﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditSchedulingReportNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                BindLegalEntityData();
                BindFinancialYear();
            }
        }

        public int GetBranchID()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            return CustomerBranchId;
        }


        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            cleardatasource(); cleardatasource1();
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["ISAHQMPReport"] = null;
            ViewState["customerbranchidReport"] = null;
            ViewState["PhaseCountReport"] = null;
            int customerbranchid = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {

                            customerbranchid = Convert.ToInt32(GetBranchID());
                            var ISAHQMPDetails = GetSchedulingReportISAHQMP_ResultProcedure(customerbranchid, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlVerticalID.SelectedValue));
                            if (ISAHQMPDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                                {
                                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                                    ViewState["VerticalID"] = VerticalID;
                                    if (ISAHQMPDetails.ISAHQMP == "A")
                                    {
                                        ViewState["ISAHQMPReport"] = "A";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "A");
                                        EnableDisable2(0, "A");
                                        BindAuditSchedule("A", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("A", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "H")
                                    {
                                        ViewState["ISAHQMPReport"] = "H";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "H");
                                        EnableDisable2(0, "H");
                                        BindAuditSchedule("H", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("H", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "Q")
                                    {
                                        ViewState["ISAHQMPReport"] = "Q";

                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "Q");
                                        EnableDisable2(0, "Q");
                                        BindAuditSchedule("Q", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("Q", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "M")
                                    {
                                        ViewState["ISAHQMPReport"] = "M";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "M");
                                        EnableDisable2(0, "M");
                                        BindAuditSchedule("M", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("M", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "P")
                                    {
                                        ViewState["ISAHQMPReport"] = "P";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        ViewState["PhaseCountReport"] = ISAHQMPDetails.PhaseCount;
                                        EnableDisable(Convert.ToInt32(ISAHQMPDetails.PhaseCount), "P");
                                        EnableDisable2(Convert.ToInt32(ISAHQMPDetails.PhaseCount), "P");
                                        BindAuditSchedule("P", Convert.ToInt32(ISAHQMPDetails.PhaseCount), customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("P", Convert.ToInt32(ISAHQMPDetails.PhaseCount), customerbranchid, VerticalID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Legal Entity", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }

        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                int customerID = -1;
                customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("All", "-1"));
            }
        }
        public SchedulingReportISAHQMP_Result GetSchedulingReportISAHQMP_ResultProcedure(int branchid, string FinancialYear,int VerticalID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SchedulingReportISAHQMP(branchid, FinancialYear, VerticalID).FirstOrDefault();
                return complianceReminders;
            }

        }

        public List<SchedulingReport_Result> GetSchedulingReport_ResultProcedure(int branchid, int ProcessId, string FinancialYear, int VerticalID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SchedulingReport(branchid, ProcessId, FinancialYear, VerticalID).ToList();
                return complianceReminders;
            }

        }
        #region Annually        
        public static List<AnnualyReportView> GetSPAnnualyReportwiseDisplayNEW(int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AnnualyReportView> transactionsQuery = new List<AnnualyReportView>();
                transactionsQuery = (from row in entities.AnnualyReportViews
                                     where row.CustomerBranchId == Branchid
                                     select row).ToList();

                return transactionsQuery;
            }
        }
        public static List<SP_AnnualyReport_Result> GetSPAnnualyReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_AnnualyReport(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdAnnually_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkAnnualy1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkAnnualy2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkAnnualy3");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Annually")
                                            {
                                                bf.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Annually")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Annually")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Annually";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Annually";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Annually";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdAnnually_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdAnnually.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("A", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Haly Yearly
        public static List<SP_HalfYearlyReport_Result> GetSPHalfYearlywiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_HalfYearlyReport(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdHalfYearly_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                bool IsDeleted = true;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            GridViewRow gvRow = e.Row;
                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 2;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 2;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 2;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell1 = e.Row.Cells[5];
                                otherCell1.Visible = false;
                                TableCell otherCell2 = e.Row.Cells[6];
                                otherCell2.Visible = false;
                                TableCell otherCell3 = e.Row.Cells[7];
                                otherCell3.Visible = false;

                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkHalfyearly1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkHalfyearly2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkHalfyearly3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkHalfyearly4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkHalfyearly5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkHalfyearly6");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Apr-Sep")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Oct-Mar")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Apr-Sep")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termNameSecond == "Oct-Mar")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Apr-Sep")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameThird == "Oct-Mar")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr-Sep";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Oct-Mar";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Apr-Sep";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Oct-Mar";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Apr-Sep";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Oct-Mar";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        protected void grdHalfYearly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdHalfYearly.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("H", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Quarterly
        public static List<SP_QuarterlyReport_Result> GetSPQuarterwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_QuarterlyReport(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 4;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 4;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 4;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell3 = e.Row.Cells[13];
                                otherCell3.Visible = false;

                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                //first financial Year
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkQuarter1");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkQuarter2");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkQuarter3");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkQuarter4");
                                //Second financial Year
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkQuarter5");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkQuarter6");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkQuarter7");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkQuarter8");
                                //Third financial Year
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkQuarter9");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkQuarter10");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkQuarter11");
                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkQuarter12");


                                List<SchedulingReport_Result> schresult = new List<SchedulingReport_Result>();
                                schresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                var remindersummary = schresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Apr-Jun")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Jul-Sep")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Oct-Dec")
                                        {
                                            bf3.Checked = true;
                                        }
                                        else if (termName == "Jan-Mar")
                                        {
                                            bf4.Checked = true;
                                        }
                                    }
                                }

                                #region Header Data
                                if (lblProcessID.Text == "0")
                                {
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr-Jun";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Jul-Sep";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Oct-Dec";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Jan-Mar";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Apr-Jun";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Jul-Sep";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Oct-Dec";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Jan-Mar";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Apr-Jun";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Jul-Sep";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Oct-Dec";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Jan-Mar";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdQuarterly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdQuarterly.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("Q", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }

        }
        #endregion
        #region Monthly
        public static List<SP_MonthlyReport_Result> GetSPMonthlyReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_MonthlyReport(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 12;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 12;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 12;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;

                                TableCell otherCell14 = e.Row.Cells[14];
                                otherCell14.Visible = false;

                                TableCell otherCell15 = e.Row.Cells[15];
                                otherCell15.Visible = false;

                                TableCell otherCell16 = e.Row.Cells[16];
                                otherCell16.Visible = false;

                                TableCell otherCell17 = e.Row.Cells[17];
                                otherCell17.Visible = false;

                                TableCell otherCell18 = e.Row.Cells[18];
                                otherCell18.Visible = false;

                                TableCell otherCell19 = e.Row.Cells[19];
                                otherCell19.Visible = false;

                                TableCell otherCell20 = e.Row.Cells[20];
                                otherCell20.Visible = false;

                                TableCell otherCell21 = e.Row.Cells[21];
                                otherCell21.Visible = false;

                                TableCell otherCell22 = e.Row.Cells[22];
                                otherCell22.Visible = false;

                                TableCell otherCell23 = e.Row.Cells[23];
                                otherCell23.Visible = false;

                                TableCell otherCell24 = e.Row.Cells[24];
                                otherCell24.Visible = false;

                                TableCell otherCell25 = e.Row.Cells[25];
                                otherCell25.Visible = false;

                                TableCell otherCell26 = e.Row.Cells[26];
                                otherCell26.Visible = false;

                                TableCell otherCell27 = e.Row.Cells[27];
                                otherCell27.Visible = false;

                                TableCell otherCell28 = e.Row.Cells[28];
                                otherCell28.Visible = false;

                                TableCell otherCell29 = e.Row.Cells[29];
                                otherCell29.Visible = false;

                                TableCell otherCell30 = e.Row.Cells[30];
                                otherCell30.Visible = false;


                                TableCell otherCell31 = e.Row.Cells[31];
                                otherCell31.Visible = false;

                                TableCell otherCell32 = e.Row.Cells[32];
                                otherCell32.Visible = false;

                                TableCell otherCell33 = e.Row.Cells[33];
                                otherCell33.Visible = false;

                                TableCell otherCell34 = e.Row.Cells[34];
                                otherCell34.Visible = false;

                                TableCell otherCell35 = e.Row.Cells[35];
                                otherCell35.Visible = false;

                                TableCell otherCell36 = e.Row.Cells[36];
                                otherCell36.Visible = false;

                                TableCell otherCell37 = e.Row.Cells[37];
                                otherCell37.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                //first financial Year
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkMonthly1");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkMonthly2");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkMonthly3");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkMonthly4");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkMonthly5");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkMonthly6");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkMonthly7");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkMonthly8");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkMonthly9");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkMonthly10");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkMonthly11");
                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkMonthly12");
                                //Second financial Year
                                CheckBox bf13 = (CheckBox) e.Row.FindControl("chkMonthly13");
                                CheckBox bf14 = (CheckBox) e.Row.FindControl("chkMonthly14");
                                CheckBox bf15 = (CheckBox) e.Row.FindControl("chkMonthly15");
                                CheckBox bf16 = (CheckBox) e.Row.FindControl("chkMonthly16");
                                CheckBox bf17 = (CheckBox) e.Row.FindControl("chkMonthly17");
                                CheckBox bf18 = (CheckBox) e.Row.FindControl("chkMonthly18");
                                CheckBox bf19 = (CheckBox) e.Row.FindControl("chkMonthly19");
                                CheckBox bf20 = (CheckBox) e.Row.FindControl("chkMonthly20");
                                CheckBox bf21 = (CheckBox) e.Row.FindControl("chkMonthly21");
                                CheckBox bf22 = (CheckBox) e.Row.FindControl("chkMonthly22");
                                CheckBox bf23 = (CheckBox) e.Row.FindControl("chkMonthly23");
                                CheckBox bf24 = (CheckBox) e.Row.FindControl("chkMonthly24");
                                //Third financial Year
                                CheckBox bf25 = (CheckBox) e.Row.FindControl("chkMonthly25");
                                CheckBox bf26 = (CheckBox) e.Row.FindControl("chkMonthly26");
                                CheckBox bf27 = (CheckBox) e.Row.FindControl("chkMonthly27");
                                CheckBox bf28 = (CheckBox) e.Row.FindControl("chkMonthly28");
                                CheckBox bf29 = (CheckBox) e.Row.FindControl("chkMonthly29");
                                CheckBox bf30 = (CheckBox) e.Row.FindControl("chkMonthly30");
                                CheckBox bf31 = (CheckBox) e.Row.FindControl("chkMonthly31");
                                CheckBox bf32 = (CheckBox) e.Row.FindControl("chkMonthly32");
                                CheckBox bf33 = (CheckBox) e.Row.FindControl("chkMonthly33");
                                CheckBox bf34 = (CheckBox) e.Row.FindControl("chkMonthly34");
                                CheckBox bf35 = (CheckBox) e.Row.FindControl("chkMonthly35");
                                CheckBox bf36 = (CheckBox) e.Row.FindControl("chkMonthly36");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Apr")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "May")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Jun")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termName == "Jul")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termName == "Aug")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termName == "Sep")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termName == "Oct")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termName == "Nov")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termName == "Dec")
                                            {
                                                bf9.Checked = true;
                                            }
                                            else if (termName == "Jan")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termName == "Feb")
                                            {
                                                bf11.Checked = true;
                                            }
                                            else if (termName == "Mar")
                                            {
                                                bf12.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Apr")
                                            {
                                                bf13.Checked = true;
                                            }
                                            else if (termNameSecond == "May")
                                            {
                                                bf14.Checked = true;
                                            }
                                            else if (termNameSecond == "Jun")
                                            {
                                                bf15.Checked = true;
                                            }
                                            else if (termNameSecond == "Jul")
                                            {
                                                bf16.Checked = true;
                                            }
                                            else if (termNameSecond == "Aug")
                                            {
                                                bf17.Checked = true;
                                            }
                                            else if (termNameSecond == "Sep")
                                            {
                                                bf18.Checked = true;
                                            }
                                            else if (termNameSecond == "Oct")
                                            {
                                                bf19.Checked = true;
                                            }
                                            else if (termNameSecond == "Nov")
                                            {
                                                bf20.Checked = true;
                                            }
                                            else if (termNameSecond == "Dec")
                                            {
                                                bf21.Checked = true;
                                            }
                                            else if (termNameSecond == "Jan")
                                            {
                                                bf22.Checked = true;
                                            }
                                            else if (termNameSecond == "Feb")
                                            {
                                                bf23.Checked = true;
                                            }
                                            else if (termNameSecond == "Mar")
                                            {
                                                bf24.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Apr")
                                            {
                                                bf25.Checked = true;
                                            }
                                            else if (termNameThird == "May")
                                            {
                                                bf26.Checked = true;
                                            }
                                            else if (termNameThird == "Jun")
                                            {
                                                bf27.Checked = true;
                                            }
                                            else if (termNameThird == "Jul")
                                            {
                                                bf28.Checked = true;
                                            }
                                            else if (termNameThird == "Aug")
                                            {
                                                bf29.Checked = true;
                                            }
                                            else if (termNameThird == "Sep")
                                            {
                                                bf30.Checked = true;
                                            }
                                            else if (termNameThird == "Oct")
                                            {
                                                bf31.Checked = true;
                                            }
                                            else if (termNameThird == "Nov")
                                            {
                                                bf32.Checked = true;
                                            }
                                            else if (termNameThird == "Dec")
                                            {
                                                bf33.Checked = true;
                                            }
                                            else if (termNameThird == "Jan")
                                            {
                                                bf34.Checked = true;
                                            }
                                            else if (termNameThird == "Feb")
                                            {
                                                bf35.Checked = true;
                                            }
                                            else if (termNameThird == "Mar")
                                            {
                                                bf36.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    //first financial Year
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;
                                    //Second financial Year
                                    bf13.Visible = false;
                                    bf14.Visible = false;
                                    bf15.Visible = false;
                                    bf16.Visible = false;
                                    bf17.Visible = false;
                                    bf18.Visible = false;
                                    bf19.Visible = false;
                                    bf20.Visible = false;
                                    bf21.Visible = false;
                                    bf22.Visible = false;
                                    bf23.Visible = false;
                                    bf24.Visible = false;
                                    //Third financial Year
                                    bf25.Visible = false;
                                    bf26.Visible = false;
                                    bf27.Visible = false;
                                    bf28.Visible = false;
                                    bf29.Visible = false;
                                    bf30.Visible = false;
                                    bf31.Visible = false;
                                    bf32.Visible = false;
                                    bf33.Visible = false;
                                    bf34.Visible = false;
                                    bf35.Visible = false;
                                    bf36.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "May";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Jun";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Jul";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Aug";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Sep";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Oct";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Nov";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Dec";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Jan";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Feb";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Mar";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell13 = e.Row.Cells[14];
                                    cell13.ColumnSpan = 1;
                                    cell13.Text = "Apr";
                                    cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell13.BorderColor = Color.White;
                                    cell13.ForeColor = Color.White;
                                    cell13.Font.Bold = true;
                                    cell13.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell14 = e.Row.Cells[15];
                                    cell14.ColumnSpan = 1;
                                    cell14.Text = "May";
                                    cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell14.BorderColor = Color.White;
                                    cell14.ForeColor = Color.White;
                                    cell14.Font.Bold = true;
                                    cell14.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell15 = e.Row.Cells[16];
                                    cell15.ColumnSpan = 1;
                                    cell15.Text = "Jun";
                                    cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell15.BorderColor = Color.White;
                                    cell15.ForeColor = Color.White;
                                    cell15.Font.Bold = true;
                                    cell15.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell16 = e.Row.Cells[17];
                                    cell16.ColumnSpan = 1;
                                    cell16.Text = "Jul";
                                    cell16.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell16.BorderColor = Color.White;
                                    cell16.ForeColor = Color.White;
                                    cell16.Font.Bold = true;
                                    cell16.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell17 = e.Row.Cells[18];
                                    cell17.ColumnSpan = 1;
                                    cell17.Text = "Aug";
                                    cell17.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell17.BorderColor = Color.White;
                                    cell17.ForeColor = Color.White;
                                    cell17.Font.Bold = true;
                                    cell17.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell18 = e.Row.Cells[19];
                                    cell18.ColumnSpan = 1;
                                    cell18.Text = "Sep";
                                    cell18.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell18.BorderColor = Color.White;
                                    cell18.ForeColor = Color.White;
                                    cell18.Font.Bold = true;
                                    cell18.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell19 = e.Row.Cells[20];
                                    cell19.ColumnSpan = 1;
                                    cell19.Text = "Oct";
                                    cell19.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell19.BorderColor = Color.White;
                                    cell19.ForeColor = Color.White;
                                    cell19.Font.Bold = true;
                                    cell19.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell20 = e.Row.Cells[21];
                                    cell20.ColumnSpan = 1;
                                    cell20.Text = "Nov";
                                    cell20.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell20.BorderColor = Color.White;
                                    cell20.ForeColor = Color.White;
                                    cell20.Font.Bold = true;
                                    cell20.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell21 = e.Row.Cells[22];
                                    cell21.ColumnSpan = 1;
                                    cell21.Text = "Dec";
                                    cell21.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell21.BorderColor = Color.White;
                                    cell21.ForeColor = Color.White;
                                    cell21.Font.Bold = true;
                                    cell21.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell22 = e.Row.Cells[23];
                                    cell22.ColumnSpan = 1;
                                    cell22.Text = "Jan";
                                    cell22.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell22.BorderColor = Color.White;
                                    cell22.ForeColor = Color.White;
                                    cell22.Font.Bold = true;
                                    cell22.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell23 = e.Row.Cells[24];
                                    cell23.ColumnSpan = 1;
                                    cell23.Text = "Feb";
                                    cell23.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell23.BorderColor = Color.White;
                                    cell23.ForeColor = Color.White;
                                    cell23.Font.Bold = true;
                                    cell23.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell24 = e.Row.Cells[25];
                                    cell24.ColumnSpan = 1;
                                    cell24.Text = "Mar";
                                    cell24.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell24.BorderColor = Color.White;
                                    cell24.ForeColor = Color.White;
                                    cell24.Font.Bold = true;
                                    cell24.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell25 = e.Row.Cells[26];
                                    cell25.ColumnSpan = 1;
                                    cell25.Text = "Apr";
                                    cell25.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell25.BorderColor = Color.White;
                                    cell25.ForeColor = Color.White;
                                    cell25.Font.Bold = true;
                                    cell25.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell26 = e.Row.Cells[27];
                                    cell26.ColumnSpan = 1;
                                    cell26.Text = "May";
                                    cell26.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell26.BorderColor = Color.White;
                                    cell26.ForeColor = Color.White;
                                    cell26.Font.Bold = true;
                                    cell26.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell27 = e.Row.Cells[28];
                                    cell27.ColumnSpan = 1;
                                    cell27.Text = "Jun";
                                    cell27.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell27.BorderColor = Color.White;
                                    cell27.ForeColor = Color.White;
                                    cell27.Font.Bold = true;
                                    cell27.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell28 = e.Row.Cells[29];
                                    cell28.ColumnSpan = 1;
                                    cell28.Text = "Jul";
                                    cell28.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell28.BorderColor = Color.White;
                                    cell28.ForeColor = Color.White;
                                    cell28.Font.Bold = true;
                                    cell28.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell29 = e.Row.Cells[30];
                                    cell29.ColumnSpan = 1;
                                    cell29.Text = "Aug";
                                    cell29.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell29.BorderColor = Color.White;
                                    cell29.ForeColor = Color.White;
                                    cell29.Font.Bold = true;
                                    cell29.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell30 = e.Row.Cells[31];
                                    cell30.ColumnSpan = 1;
                                    cell30.Text = "Sep";
                                    cell30.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell30.BorderColor = Color.White;
                                    cell30.ForeColor = Color.White;
                                    cell30.Font.Bold = true;
                                    cell30.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell31 = e.Row.Cells[32];
                                    cell31.ColumnSpan = 1;
                                    cell31.Text = "Oct";
                                    cell31.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell31.BorderColor = Color.White;
                                    cell31.ForeColor = Color.White;
                                    cell31.Font.Bold = true;
                                    cell31.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell32 = e.Row.Cells[33];
                                    cell32.ColumnSpan = 1;
                                    cell32.Text = "Nov";
                                    cell32.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell32.BorderColor = Color.White;
                                    cell32.ForeColor = Color.White;
                                    cell32.Font.Bold = true;
                                    cell32.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell33 = e.Row.Cells[34];
                                    cell33.ColumnSpan = 1;
                                    cell33.Text = "Dec";
                                    cell33.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell33.BorderColor = Color.White;
                                    cell33.ForeColor = Color.White;
                                    cell33.Font.Bold = true;
                                    cell33.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell34 = e.Row.Cells[35];
                                    cell34.ColumnSpan = 1;
                                    cell34.Text = "Jan";
                                    cell34.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell34.BorderColor = Color.White;
                                    cell34.ForeColor = Color.White;
                                    cell34.Font.Bold = true;
                                    cell34.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell35 = e.Row.Cells[36];
                                    cell35.ColumnSpan = 1;
                                    cell35.Text = "Feb";
                                    cell35.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell35.BorderColor = Color.White;
                                    cell35.ForeColor = Color.White;
                                    cell35.Font.Bold = true;
                                    cell35.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell36 = e.Row.Cells[37];
                                    cell36.ColumnSpan = 1;
                                    cell36.Text = "Mar";
                                    cell36.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell36.BorderColor = Color.White;
                                    cell36.ForeColor = Color.White;
                                    cell36.Font.Bold = true;
                                    cell36.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }            
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        protected void grdMonthly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdMonthly.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("M", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Phase1
        public static List<SP_Phase1Report_Result> GetSPPhase1ReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase1Report(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdphase1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase1.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("P", 1, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Phase 2
        public static List<SP_Phase2Report_Result> GetSPPhase2ReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase2Report(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdphase2_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 2;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 2;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 2;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell1 = e.Row.Cells[5];
                                otherCell1.Visible = false;

                                TableCell otherCell2 = e.Row.Cells[6];
                                otherCell2.Visible = false;

                                TableCell otherCell3 = e.Row.Cells[7];
                                otherCell3.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 1";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 2";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 1";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 2";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase2.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("P", 2, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Phase 3
        public static List<SP_Phase3Report_Result> GetSPPhase3ReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase3Report(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdphase3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 3;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 3;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 3;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf8.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;


                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 1";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 2";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 3";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 1";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 2";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 3";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase3.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("P", 3, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Phase 4
        public static List<SP_Phase4Report_Result> GetSPPhase4ReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase4Report(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdphase4_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 4;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 4;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 4;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkPhase10");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkPhase11");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkPhase12");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Phase4")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase4")
                                            {
                                                bf7.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf9.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termNameThird == "Phase4")
                                            {
                                                bf11.Checked = true;
                                            }

                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 4";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 1";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 2";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 3";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 4";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 1";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Phase 2";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Phase 3";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Phase 4";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase4.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("P", 4, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        #region Phase 5
        public static List<SP_Phase5Report_Result> GetSPPhase5ReportwiseDisplay(int Branchid, bool IsDeleted, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase5Report(Branchid, "", Verticalid).ToList();
                return auditdsplay;
            }
        }
        protected void grdphase5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {

                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 5;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 5;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 5;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;

                                TableCell otherCell14 = e.Row.Cells[14];
                                otherCell14.Visible = false;

                                TableCell otherCell15 = e.Row.Cells[15];
                                otherCell15.Visible = false;

                                TableCell otherCell16 = e.Row.Cells[16];
                                otherCell16.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkPhase10");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkPhase11");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkPhase12");

                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkPhase12");
                                CheckBox bf13 = (CheckBox) e.Row.FindControl("chkPhase13");
                                CheckBox bf14 = (CheckBox) e.Row.FindControl("chkPhase14");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Phase4")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termName == "Phase5")
                                            {
                                                bf4.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase4")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase5")
                                            {
                                                bf9.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf11.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf12.Checked = true;
                                            }
                                            else if (termNameThird == "Phase4")
                                            {
                                                bf13.Checked = true;
                                            }
                                            else if (termNameThird == "Phase5")
                                            {
                                                bf14.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;
                                    bf13.Visible = false;
                                    bf14.Visible = false;


                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 4";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 5";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 1";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 2";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 3";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 4";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Phase 5";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Phase 1";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Phase 2";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell13 = e.Row.Cells[14];
                                    cell13.ColumnSpan = 1;
                                    cell13.Text = "Phase 3";
                                    cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell13.BorderColor = Color.White;
                                    cell13.ForeColor = Color.White;
                                    cell13.Font.Bold = true;
                                    cell13.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell14 = e.Row.Cells[15];
                                    cell14.ColumnSpan = 1;
                                    cell14.Text = "Phase 4";
                                    cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell14.BorderColor = Color.White;
                                    cell14.ForeColor = Color.White;
                                    cell14.Font.Bold = true;
                                    cell14.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell15 = e.Row.Cells[16];
                                    cell15.ColumnSpan = 1;
                                    cell15.Text = "Phase 5";
                                    cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell15.BorderColor = Color.White;
                                    cell15.ForeColor = Color.White;
                                    cell15.Font.Bold = true;
                                    cell15.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase5.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditSchedule("P", 5, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }
        #endregion
        public void cleardatasource()
        {
            grdAnnually.DataSource = null;
            grdAnnually.DataBind();
            grdHalfYearly.DataSource = null;
            grdHalfYearly.DataBind();
            grdQuarterly.DataSource = null;
            grdQuarterly.DataBind();
            grdMonthly.DataSource = null;
            grdMonthly.DataBind();
            grdphase1.DataSource = null;
            grdphase1.DataBind();
            grdphase2.DataSource = null;
            grdphase2.DataBind();
            grdphase3.DataSource = null;
            grdphase3.DataBind();
            grdphase4.DataSource = null;
            grdphase4.DataBind();
            grdphase5.DataSource = null;
            grdphase5.DataBind();
        }

        public void cleardatasource1()
        {
            grdAnnually2.DataSource = null;
            grdAnnually2.DataBind();
            grdHalfYearly2.DataSource = null;
            grdHalfYearly2.DataBind();
            grdQuarterly2.DataSource = null;
            grdQuarterly2.DataBind();
            grdMonthly2.DataSource = null;
            grdMonthly2.DataBind();
            grdphase12.DataSource = null;
            grdphase12.DataBind();
            grdphase22.DataSource = null;
            grdphase22.DataBind();
            grdphase32.DataSource = null;
            grdphase32.DataBind();
            grdphase42.DataSource = null;
            grdphase42.DataBind();
            grdphase52.DataSource = null;
            grdphase52.DataBind();
        }
        public void EnableDisable(int noofphases, string Flag)
        {
            if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
            {
                if (Flag == "A")
                {
                    pnlAnnually.Visible = true;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    pnlAnnuallyPrevious.Visible = true;
                    pnlHalfYearlyPrevious.Visible = false;
                    pnlQuarterlyPrevious.Visible = false;
                    pnlMonthlyPrevious.Visible = false;
                    pnlphase1Previous.Visible = false;
                    pnlphase2Previous.Visible = false;
                    pnlphase3Previous.Visible = false;
                    pnlphase4Previous.Visible = false;
                    pnlphase5Previous.Visible = false;

                    //Divnophase.Visible = false;
                }
                else if (Flag == "H")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = true;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    pnlAnnuallyPrevious.Visible = false;
                    pnlHalfYearlyPrevious.Visible = true;
                    pnlQuarterlyPrevious.Visible = false;
                    pnlMonthlyPrevious.Visible = false;
                    pnlphase1Previous.Visible = false;
                    pnlphase2Previous.Visible = false;
                    pnlphase3Previous.Visible = false;
                    pnlphase4Previous.Visible = false;
                    pnlphase5Previous.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "Q")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = true;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    pnlAnnuallyPrevious.Visible = false;
                    pnlHalfYearlyPrevious.Visible = false;
                    pnlQuarterlyPrevious.Visible = true;
                    pnlMonthlyPrevious.Visible = false;
                    pnlphase1Previous.Visible = false;
                    pnlphase2Previous.Visible = false;
                    pnlphase3Previous.Visible = false;
                    pnlphase4Previous.Visible = false;
                    pnlphase5Previous.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "M")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = true;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    pnlAnnuallyPrevious.Visible = false;
                    pnlHalfYearlyPrevious.Visible = false;
                    pnlQuarterlyPrevious.Visible = false;
                    pnlMonthlyPrevious.Visible = true;
                    pnlphase1Previous.Visible = false;
                    pnlphase2Previous.Visible = false;
                    pnlphase3Previous.Visible = false;
                    pnlphase4Previous.Visible = false;
                    pnlphase5Previous.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "P")
                {
                    if (noofphases == 1)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = true;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        pnlAnnuallyPrevious.Visible = false;
                        pnlHalfYearlyPrevious.Visible = false;
                        pnlQuarterlyPrevious.Visible = false;
                        pnlMonthlyPrevious.Visible = false;
                        pnlphase1Previous.Visible = true;
                        pnlphase2Previous.Visible = false;
                        pnlphase3Previous.Visible = false;
                        pnlphase4Previous.Visible = false;
                        pnlphase5Previous.Visible = false;
                        //Divnophase.Visible = true;
                    }
                    else if (noofphases == 2)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = true;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        pnlAnnuallyPrevious.Visible = false;
                        pnlHalfYearlyPrevious.Visible = false;
                        pnlQuarterlyPrevious.Visible = false;
                        pnlMonthlyPrevious.Visible = false;
                        pnlphase1Previous.Visible = false;
                        pnlphase2Previous.Visible = true;
                        pnlphase3Previous.Visible = false;
                        pnlphase4Previous.Visible = false;
                        pnlphase5Previous.Visible = false;
                    }
                    else if (noofphases == 3)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = true;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                    }
                    else if (noofphases == 4)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = true;
                        pnlphase5.Visible = false;
                        pnlAnnuallyPrevious.Visible = false;
                        pnlHalfYearlyPrevious.Visible = false;
                        pnlQuarterlyPrevious.Visible = false;
                        pnlMonthlyPrevious.Visible = false;
                        pnlphase1Previous.Visible = false;
                        pnlphase2Previous.Visible = false;
                        pnlphase3Previous.Visible = true;
                        pnlphase4Previous.Visible = false;
                        pnlphase5Previous.Visible = false;
                    }
                    else if (noofphases == 5)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = true;
                        pnlAnnuallyPrevious.Visible = false;
                        pnlHalfYearlyPrevious.Visible = false;
                        pnlQuarterlyPrevious.Visible = false;
                        pnlMonthlyPrevious.Visible = false;
                        pnlphase1Previous.Visible = false;
                        pnlphase2Previous.Visible = false;
                        pnlphase3Previous.Visible = false;
                        pnlphase4Previous.Visible = false;
                        pnlphase5Previous.Visible = true;
                    }
                }
            }
        }

        public void EnableDisable2(int noofphases, string Flag)
        {
            if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
            {
                if (Flag == "A")
                {
                    pnlAnnually2.Visible = true;
                    pnlHalfYearly2.Visible = false;
                    pnlQuarterly2.Visible = false;
                    pnlMonthly2.Visible = false;
                    pnlphase12.Visible = false;
                    pnlphase22.Visible = false;
                    pnlphase32.Visible = false;
                    pnlphase42.Visible = false;
                    pnlphase52.Visible = false;
                    pnlAnnually2Current.Visible = true;
                    pnlHalfYearly2Current.Visible = false;
                    pnlQuarterly2Current.Visible = false;
                    pnlMonthly2Current.Visible = false;
                    pnlphase12Current.Visible = false;
                    pnlphase22Current.Visible = false;
                    pnlphase32Current.Visible = false;
                    pnlphase42Current.Visible = false;
                    pnlphase52Current.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "H")
                {
                    pnlAnnually2.Visible = false;
                    pnlHalfYearly2.Visible = true;
                    pnlQuarterly2.Visible = false;
                    pnlMonthly2.Visible = false;
                    pnlphase12.Visible = false;
                    pnlphase22.Visible = false;
                    pnlphase32.Visible = false;
                    pnlphase42.Visible = false;
                    pnlphase52.Visible = false;
                    pnlAnnually2Current.Visible = false;
                    pnlHalfYearly2Current.Visible = true;
                    pnlQuarterly2Current.Visible = false;
                    pnlMonthly2Current.Visible = false;
                    pnlphase12Current.Visible = false;
                    pnlphase22Current.Visible = false;
                    pnlphase32Current.Visible = false;
                    pnlphase42Current.Visible = false;
                    pnlphase52Current.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "Q")
                {
                    pnlAnnually2.Visible = false;
                    pnlHalfYearly2.Visible = false;
                    pnlQuarterly2.Visible = true;
                    pnlMonthly2.Visible = false;
                    pnlphase12.Visible = false;
                    pnlphase22.Visible = false;
                    pnlphase32.Visible = false;
                    pnlphase42.Visible = false;
                    pnlphase52.Visible = false;
                    pnlAnnually2Current.Visible = false;
                    pnlHalfYearly2Current.Visible = false;
                    pnlQuarterly2Current.Visible = true;
                    pnlMonthly2Current.Visible = false;
                    pnlphase12Current.Visible = false;
                    pnlphase22Current.Visible = false;
                    pnlphase32Current.Visible = false;
                    pnlphase42Current.Visible = false;
                    pnlphase52Current.Visible = false;

                    //Divnophase.Visible = false;
                }
                else if (Flag == "M")
                {
                    pnlAnnually2.Visible = false;
                    pnlHalfYearly2.Visible = false;
                    pnlQuarterly2.Visible = false;
                    pnlMonthly2.Visible = true;
                    pnlphase12.Visible = false;
                    pnlphase22.Visible = false;
                    pnlphase32.Visible = false;
                    pnlphase42.Visible = false;
                    pnlphase52.Visible = false;
                    pnlAnnually2Current.Visible = false;
                    pnlHalfYearly2Current.Visible = false;
                    pnlQuarterly2Current.Visible = false;
                    pnlMonthly2Current.Visible = true;
                    pnlphase12Current.Visible = false;
                    pnlphase22Current.Visible = false;
                    pnlphase32Current.Visible = false;
                    pnlphase42Current.Visible = false;
                    pnlphase52Current.Visible = false;
                    //Divnophase.Visible = false;
                }
                else if (Flag == "P")
                {
                    if (noofphases == 1)
                    {
                        pnlAnnually2.Visible = false;
                        pnlHalfYearly2.Visible = false;
                        pnlQuarterly2.Visible = false;
                        pnlMonthly2.Visible = false;
                        pnlphase12.Visible = true;
                        pnlphase22.Visible = false;
                        pnlphase32.Visible = false;
                        pnlphase42.Visible = false;
                        pnlphase52.Visible = false;
                        pnlAnnually2Current.Visible = false;
                        pnlHalfYearly2Current.Visible = false;
                        pnlQuarterly2Current.Visible = false;
                        pnlMonthly2Current.Visible = false;
                        pnlphase12Current.Visible = true;
                        pnlphase22Current.Visible = false;
                        pnlphase32Current.Visible = false;
                        pnlphase42Current.Visible = false;
                        pnlphase52Current.Visible = false;
                        //Divnophase.Visible = true;
                    }
                    else if (noofphases == 2)
                    {
                        pnlAnnually2.Visible = false;
                        pnlHalfYearly2.Visible = false;
                        pnlQuarterly2.Visible = false;
                        pnlMonthly2.Visible = false;
                        pnlphase12.Visible = false;
                        pnlphase22.Visible = true;
                        pnlphase32.Visible = false;
                        pnlphase42.Visible = false;
                        pnlphase52.Visible = false;
                        pnlAnnually2Current.Visible = false;
                        pnlHalfYearly2Current.Visible = false;
                        pnlQuarterly2Current.Visible = false;
                        pnlMonthly2Current.Visible = false;
                        pnlphase12Current.Visible = false;
                        pnlphase22Current.Visible = true;
                        pnlphase32Current.Visible = false;
                        pnlphase42Current.Visible = false;
                        pnlphase52Current.Visible = false;
                    }
                    else if (noofphases == 3)
                    {
                        pnlAnnually2.Visible = false;
                        pnlHalfYearly2.Visible = false;
                        pnlQuarterly2.Visible = false;
                        pnlMonthly2.Visible = false;
                        pnlphase12.Visible = false;
                        pnlphase22.Visible = false;
                        pnlphase32.Visible = true;
                        pnlphase42.Visible = false;
                        pnlphase52.Visible = false;
                        pnlAnnually2Current.Visible = false;
                        pnlHalfYearly2Current.Visible = false;
                        pnlQuarterly2Current.Visible = false;
                        pnlMonthly2Current.Visible = false;
                        pnlphase12Current.Visible = false;
                        pnlphase22Current.Visible = false;
                        pnlphase32Current.Visible = true;
                        pnlphase42Current.Visible = false;
                        pnlphase52Current.Visible = false;
                    }
                    else if (noofphases == 4)
                    {
                        pnlAnnually2.Visible = false;
                        pnlHalfYearly2.Visible = false;
                        pnlQuarterly2.Visible = false;
                        pnlMonthly2.Visible = false;
                        pnlphase12.Visible = false;
                        pnlphase22.Visible = false;
                        pnlphase32.Visible = false;
                        pnlphase42.Visible = true;
                        pnlphase52.Visible = false;
                        pnlAnnually2Current.Visible = false;
                        pnlHalfYearly2Current.Visible = false;
                        pnlQuarterly2Current.Visible = false;
                        pnlMonthly2Current.Visible = false;
                        pnlphase12Current.Visible = false;
                        pnlphase22Current.Visible = false;
                        pnlphase32Current.Visible = false;
                        pnlphase42Current.Visible = true;
                        pnlphase52Current.Visible = false;
                    }
                    else if (noofphases == 5)
                    {
                        pnlAnnually2.Visible = false;
                        pnlHalfYearly2.Visible = false;
                        pnlQuarterly2.Visible = false;
                        pnlMonthly2.Visible = false;
                        pnlphase12.Visible = false;
                        pnlphase22.Visible = false;
                        pnlphase32.Visible = false;
                        pnlphase42.Visible = false;
                        pnlphase52.Visible = true;
                        pnlAnnually2Current.Visible = false;
                        pnlHalfYearly2Current.Visible = false;
                        pnlQuarterly2Current.Visible = false;
                        pnlMonthly2Current.Visible = false;
                        pnlphase12Current.Visible = false;
                        pnlphase22Current.Visible = false;
                        pnlphase32Current.Visible = false;
                        pnlphase42Current.Visible = false;
                        pnlphase52Current.Visible = true;
                    }
                }
            }
        }

        public void BindAuditScheduleNew(string flag, int count, int Branchid, int Verticalid)
        {
            try
            {
                bool IsDeleted = true;
                if (flag == "A")
                {
                    #region Annualy
                    cleardatasource();
                    //AnnualyReportView
                    grdAnnually.DataSource = GetSPAnnualyReportwiseDisplayNEW(Branchid); //GetSPAnnualyReportwiseDisplay(Branchid);
                    grdAnnually.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<AnnualyReportView> r = new List<AnnualyReportView>();
                        r = GetSPAnnualyReportwiseDisplayNEW(Branchid);
                        DataTable dt = new DataTable();
                        dt = (grdAnnually.DataSource as List<AnnualyReportView>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Annualy1");
                        dt1.Columns.Add("Annualy2");
                        dt1.Columns.Add("Annualy3");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "";
                        dr1["Annualy1"] = f1;
                        dr1["Annualy2"] = f2;
                        dr1["Annualy3"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdAnnually.DataSource = dt1;
                        grdAnnually.DataBind();
                    }
                    #endregion
                }
                else if (flag == "H")
                {
                    #region Half Yearly
                    cleardatasource();
                    grdHalfYearly.DataSource = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, Verticalid);
                    grdHalfYearly.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_HalfYearlyReport_Result> r = new List<SP_HalfYearlyReport_Result>();
                        r = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, Verticalid);
                        DataTable dt = new DataTable();
                        dt = (grdHalfYearly.DataSource as List<SP_HalfYearlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Halfyearly1");
                        dt1.Columns.Add("Halfyearly2");
                        dt1.Columns.Add("Halfyearly3");
                        dt1.Columns.Add("Halfyearly4");
                        dt1.Columns.Add("Halfyearly5");
                        dt1.Columns.Add("Halfyearly6");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Halfyearly1"] = "";
                        dr1["Halfyearly2"] = f1;
                        dr1["Halfyearly3"] = "";
                        dr1["Halfyearly4"] = f2;
                        dr1["Halfyearly5"] = "";
                        dr1["Halfyearly6"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdHalfYearly.DataSource = dt1;
                        grdHalfYearly.DataBind();
                    }
                    #endregion
                }
                else if (flag == "Q")
                {
                    #region Quarterly
                    pnlQuarterly.Visible = true;
                    cleardatasource();

                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                        grdQuarterly.DataSource = GetSPQuarterwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdQuarterly.DataBind();

                        List<SP_QuarterlyReport_Result> r = new List<SP_QuarterlyReport_Result>();
                        r = GetSPQuarterwiseDisplay(Branchid, IsDeleted, Verticalid);
                        DataTable dt = new DataTable();
                        dt = (grdQuarterly.DataSource as List<SP_QuarterlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Quarter1");
                        dt1.Columns.Add("Quarter2");
                        dt1.Columns.Add("Quarter3");
                        dt1.Columns.Add("Quarter4");
                        dt1.Columns.Add("Quarter5");
                        dt1.Columns.Add("Quarter6");
                        dt1.Columns.Add("Quarter7");
                        dt1.Columns.Add("Quarter8");
                        dt1.Columns.Add("Quarter9");
                        dt1.Columns.Add("Quarter10");
                        dt1.Columns.Add("Quarter11");
                        dt1.Columns.Add("Quarter12");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Quarter1"] = "";
                        dr1["Quarter2"] = "";
                        dr1["Quarter3"] = "";
                        dr1["Quarter4"] = f1;
                        dr1["Quarter5"] = "";
                        dr1["Quarter6"] = "";
                        dr1["Quarter7"] = "";
                        dr1["Quarter8"] = f2;
                        dr1["Quarter9"] = "";
                        dr1["Quarter10"] = "";
                        dr1["Quarter11"] = "";
                        dr1["Quarter12"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdQuarterly.DataSource = dt1;
                        grdQuarterly.DataBind();
                    }
                    #endregion
                }
                else if (flag == "M")
                {
                    #region Monthly
                    cleardatasource();
                    grdMonthly.DataSource = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                    grdMonthly.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_MonthlyReport_Result> r = new List<SP_MonthlyReport_Result>();
                        r = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        DataTable dt = new DataTable();
                        dt = (grdMonthly.DataSource as List<SP_MonthlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Monthly1");
                        dt1.Columns.Add("Monthly2");
                        dt1.Columns.Add("Monthly3");
                        dt1.Columns.Add("Monthly4");
                        dt1.Columns.Add("Monthly5");
                        dt1.Columns.Add("Monthly6");
                        dt1.Columns.Add("Monthly7");
                        dt1.Columns.Add("Monthly8");
                        dt1.Columns.Add("Monthly9");
                        dt1.Columns.Add("Monthly10");
                        dt1.Columns.Add("Monthly11");
                        dt1.Columns.Add("Monthly12");
                        dt1.Columns.Add("Monthly13");
                        dt1.Columns.Add("Monthly14");
                        dt1.Columns.Add("Monthly15");
                        dt1.Columns.Add("Monthly16");
                        dt1.Columns.Add("Monthly17");
                        dt1.Columns.Add("Monthly18");
                        dt1.Columns.Add("Monthly19");
                        dt1.Columns.Add("Monthly20");
                        dt1.Columns.Add("Monthly21");
                        dt1.Columns.Add("Monthly22");
                        dt1.Columns.Add("Monthly23");
                        dt1.Columns.Add("Monthly24");
                        dt1.Columns.Add("Monthly25");
                        dt1.Columns.Add("Monthly26");
                        dt1.Columns.Add("Monthly27");
                        dt1.Columns.Add("Monthly28");
                        dt1.Columns.Add("Monthly29");
                        dt1.Columns.Add("Monthly30");
                        dt1.Columns.Add("Monthly31");
                        dt1.Columns.Add("Monthly32");
                        dt1.Columns.Add("Monthly33");
                        dt1.Columns.Add("Monthly34");
                        dt1.Columns.Add("Monthly35");
                        dt1.Columns.Add("Monthly36");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Monthly1"] = "";
                        dr1["Monthly2"] = "";
                        dr1["Monthly3"] = "";
                        dr1["Monthly4"] = "";
                        dr1["Monthly5"] = "";
                        dr1["Monthly6"] = "";
                        dr1["Monthly7"] = "";
                        dr1["Monthly8"] = "";
                        dr1["Monthly9"] = "";
                        dr1["Monthly10"] = "";
                        dr1["Monthly11"] = "";
                        dr1["Monthly12"] = f1;
                        dr1["Monthly13"] = "";
                        dr1["Monthly14"] = "";
                        dr1["Monthly15"] = "";
                        dr1["Monthly16"] = "";
                        dr1["Monthly17"] = "";
                        dr1["Monthly18"] = "";
                        dr1["Monthly19"] = "";
                        dr1["Monthly20"] = "";
                        dr1["Monthly21"] = "";
                        dr1["Monthly22"] = "";
                        dr1["Monthly23"] = "";
                        dr1["Monthly24"] = f2;
                        dr1["Monthly25"] = "";
                        dr1["Monthly26"] = "";
                        dr1["Monthly27"] = "";
                        dr1["Monthly28"] = "";
                        dr1["Monthly29"] = "";
                        dr1["Monthly30"] = "";
                        dr1["Monthly31"] = "";
                        dr1["Monthly32"] = "";
                        dr1["Monthly33"] = "";
                        dr1["Monthly34"] = "";
                        dr1["Monthly35"] = "";
                        dr1["Monthly36"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdMonthly.DataSource = dt1;
                        grdMonthly.DataBind();
                    }
                    #endregion
                }
                else
                {
                    if (count == 1)
                    {
                        #region Phase1
                        cleardatasource();
                        grdphase1.DataSource = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdphase1.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase1Report_Result> r = new List<SP_Phase1Report_Result>();
                            r = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                            DataTable dt = new DataTable();
                            dt = (grdphase1.DataSource as List<SP_Phase1Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = f1;
                            dr1["Phase2"] = f2;
                            dr1["Phase3"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase1.DataSource = dt1;
                            grdphase1.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 2)
                    {
                        #region Phase2
                        cleardatasource();
                        grdphase2.DataSource = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdphase2.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase2Report_Result> r = new List<SP_Phase2Report_Result>();
                            r = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                            DataTable dt = new DataTable();
                            dt = (grdphase2.DataSource as List<SP_Phase2Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = f1;
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f2;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase2.DataSource = dt1;
                            grdphase2.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 3)
                    {
                        #region Phase3
                        cleardatasource();
                        grdphase3.DataSource = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdphase3.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase3Report_Result> r = new List<SP_Phase3Report_Result>();
                            r = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                            DataTable dt = new DataTable();
                            dt = (grdphase3.DataSource as List<SP_Phase3Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = f1;
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f2;
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase3.DataSource = dt1;
                            grdphase3.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 4)
                    {
                        #region Phase4
                        cleardatasource();
                        grdphase4.DataSource = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdphase4.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase4Report_Result> r = new List<SP_Phase4Report_Result>();
                            r = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                            DataTable dt = new DataTable();
                            dt = (grdphase4.DataSource as List<SP_Phase4Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f1;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = f2;
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = "";
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase4.DataSource = dt1;
                            grdphase4.DataBind();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Phase5
                        cleardatasource();
                        grdphase5.DataSource = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                        grdphase5.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase5Report_Result> r = new List<SP_Phase5Report_Result>();
                            r = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, Verticalid);
                            DataTable dt = new DataTable();
                            dt = (grdphase5.DataSource as List<SP_Phase5Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("Phase13");
                            dt1.Columns.Add("Phase14");
                            dt1.Columns.Add("Phase15");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = f1;
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = f2;
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = "";
                            dr1["Phase13"] = "";
                            dr1["Phase14"] = "";
                            dr1["Phase15"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase5.DataSource = dt1;
                            grdphase5.DataBind();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindAuditSchedule(string flag, int count, int Branchid, int verticalId)
        {
            try
            {
                bool IsDeleted = true;

                if (flag == "A")
                {
                    #region Annualy
                    cleardatasource();
                    grdAnnually.DataSource = GetSPAnnualyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                    grdAnnually.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != "Select Financial Year")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_AnnualyReport_Result> r = new List<SP_AnnualyReport_Result>();

                        r = GetSPAnnualyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdAnnually.DataSource as List<SP_AnnualyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Annualy1");
                        dt1.Columns.Add("Annualy2");
                        dt1.Columns.Add("Annualy3");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "";
                        dr1["Annualy1"] = f1;
                        dr1["Annualy2"] = f2;
                        dr1["Annualy3"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdAnnually.DataSource = dt1;
                        grdAnnually.DataBind();
                    }
                    #endregion
                }
                else if (flag == "H")
                {
                    #region Half Yearly
                    cleardatasource();
                    grdHalfYearly.DataSource = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, verticalId);
                    grdHalfYearly.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_HalfYearlyReport_Result> r = new List<SP_HalfYearlyReport_Result>();
                        r = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdHalfYearly.DataSource as List<SP_HalfYearlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Halfyearly1");
                        dt1.Columns.Add("Halfyearly2");
                        dt1.Columns.Add("Halfyearly3");
                        dt1.Columns.Add("Halfyearly4");
                        dt1.Columns.Add("Halfyearly5");
                        dt1.Columns.Add("Halfyearly6");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Halfyearly1"] = "";
                        dr1["Halfyearly2"] = f1;
                        dr1["Halfyearly3"] = "";
                        dr1["Halfyearly4"] = f2;
                        dr1["Halfyearly5"] = "";
                        dr1["Halfyearly6"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdHalfYearly.DataSource = dt1;
                        grdHalfYearly.DataBind();
                    }
                    #endregion
                }
                else if (flag == "Q")
                {
                    #region Quarterly
                    pnlQuarterly.Visible = true;
                    cleardatasource();

                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                        grdQuarterly.DataSource = GetSPQuarterwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdQuarterly.DataBind();

                        List<SP_QuarterlyReport_Result> r = new List<SP_QuarterlyReport_Result>();
                        r = GetSPQuarterwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdQuarterly.DataSource as List<SP_QuarterlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Quarter1");
                        dt1.Columns.Add("Quarter2");
                        dt1.Columns.Add("Quarter3");
                        dt1.Columns.Add("Quarter4");
                        dt1.Columns.Add("Quarter5");
                        dt1.Columns.Add("Quarter6");
                        dt1.Columns.Add("Quarter7");
                        dt1.Columns.Add("Quarter8");
                        dt1.Columns.Add("Quarter9");
                        dt1.Columns.Add("Quarter10");
                        dt1.Columns.Add("Quarter11");
                        dt1.Columns.Add("Quarter12");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Quarter1"] = "";
                        dr1["Quarter2"] = "";
                        dr1["Quarter3"] = "";
                        dr1["Quarter4"] = f1;
                        dr1["Quarter5"] = "";
                        dr1["Quarter6"] = "";
                        dr1["Quarter7"] = "";
                        dr1["Quarter8"] = f2;
                        dr1["Quarter9"] = "";
                        dr1["Quarter10"] = "";
                        dr1["Quarter11"] = "";
                        dr1["Quarter12"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdQuarterly.DataSource = dt1;
                        grdQuarterly.DataBind();
                    }
                    #endregion
                }
                else if (flag == "M")
                {
                    #region Monthly
                    cleardatasource();
                    grdMonthly.DataSource = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                    grdMonthly.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_MonthlyReport_Result> r = new List<SP_MonthlyReport_Result>();
                        r = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdMonthly.DataSource as List<SP_MonthlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Monthly1");
                        dt1.Columns.Add("Monthly2");
                        dt1.Columns.Add("Monthly3");
                        dt1.Columns.Add("Monthly4");
                        dt1.Columns.Add("Monthly5");
                        dt1.Columns.Add("Monthly6");
                        dt1.Columns.Add("Monthly7");
                        dt1.Columns.Add("Monthly8");
                        dt1.Columns.Add("Monthly9");
                        dt1.Columns.Add("Monthly10");
                        dt1.Columns.Add("Monthly11");
                        dt1.Columns.Add("Monthly12");
                        dt1.Columns.Add("Monthly13");
                        dt1.Columns.Add("Monthly14");
                        dt1.Columns.Add("Monthly15");
                        dt1.Columns.Add("Monthly16");
                        dt1.Columns.Add("Monthly17");
                        dt1.Columns.Add("Monthly18");
                        dt1.Columns.Add("Monthly19");
                        dt1.Columns.Add("Monthly20");
                        dt1.Columns.Add("Monthly21");
                        dt1.Columns.Add("Monthly22");
                        dt1.Columns.Add("Monthly23");
                        dt1.Columns.Add("Monthly24");
                        dt1.Columns.Add("Monthly25");
                        dt1.Columns.Add("Monthly26");
                        dt1.Columns.Add("Monthly27");
                        dt1.Columns.Add("Monthly28");
                        dt1.Columns.Add("Monthly29");
                        dt1.Columns.Add("Monthly30");
                        dt1.Columns.Add("Monthly31");
                        dt1.Columns.Add("Monthly32");
                        dt1.Columns.Add("Monthly33");
                        dt1.Columns.Add("Monthly34");
                        dt1.Columns.Add("Monthly35");
                        dt1.Columns.Add("Monthly36");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Monthly1"] = "";
                        dr1["Monthly2"] = "";
                        dr1["Monthly3"] = "";
                        dr1["Monthly4"] = "";
                        dr1["Monthly5"] = "";
                        dr1["Monthly6"] = "";
                        dr1["Monthly7"] = "";
                        dr1["Monthly8"] = "";
                        dr1["Monthly9"] = "";
                        dr1["Monthly10"] = "";
                        dr1["Monthly11"] = "";
                        dr1["Monthly12"] = f1;
                        dr1["Monthly13"] = "";
                        dr1["Monthly14"] = "";
                        dr1["Monthly15"] = "";
                        dr1["Monthly16"] = "";
                        dr1["Monthly17"] = "";
                        dr1["Monthly18"] = "";
                        dr1["Monthly19"] = "";
                        dr1["Monthly20"] = "";
                        dr1["Monthly21"] = "";
                        dr1["Monthly22"] = "";
                        dr1["Monthly23"] = "";
                        dr1["Monthly24"] = f2;
                        dr1["Monthly25"] = "";
                        dr1["Monthly26"] = "";
                        dr1["Monthly27"] = "";
                        dr1["Monthly28"] = "";
                        dr1["Monthly29"] = "";
                        dr1["Monthly30"] = "";
                        dr1["Monthly31"] = "";
                        dr1["Monthly32"] = "";
                        dr1["Monthly33"] = "";
                        dr1["Monthly34"] = "";
                        dr1["Monthly35"] = "";
                        dr1["Monthly36"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdMonthly.DataSource = dt1;
                        grdMonthly.DataBind();
                    }
                    #endregion
                }
                else
                {
                    if (count == 1)
                    {
                        #region Phase1
                        cleardatasource();
                        grdphase1.DataSource = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase1.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase1Report_Result> r = new List<SP_Phase1Report_Result>();
                            r = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase1.DataSource as List<SP_Phase1Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = f1;
                            dr1["Phase2"] = f2;
                            dr1["Phase3"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase1.DataSource = dt1;
                            grdphase1.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 2)
                    {
                        #region Phase2
                        cleardatasource();
                        grdphase2.DataSource = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase2.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase2Report_Result> r = new List<SP_Phase2Report_Result>();
                            r = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase2.DataSource as List<SP_Phase2Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = f1;
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f2;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase2.DataSource = dt1;
                            grdphase2.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 3)
                    {
                        #region Phase3
                        cleardatasource();
                        grdphase3.DataSource = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase3.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase3Report_Result> r = new List<SP_Phase3Report_Result>();
                            r = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase3.DataSource as List<SP_Phase3Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = f1;
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f2;
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase3.DataSource = dt1;
                            grdphase3.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 4)
                    {
                        #region Phase4
                        cleardatasource();
                        grdphase4.DataSource = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase4.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase4Report_Result> r = new List<SP_Phase4Report_Result>();
                            r = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase4.DataSource as List<SP_Phase4Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f1;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = f2;
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = "";
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase4.DataSource = dt1;
                            grdphase4.DataBind();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Phase5
                        cleardatasource();
                        //Added by hardik Sapara for paging.

                        /////////////////// OLD Code ////////
                        //grdphase5.DataSource = GetSPPhase5ReportwiseDisplay(Branchid);
                        //grdphase5.DataBind();

                        // New Code 
                        var Phase5AuditDetails = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase5.DataSource = Phase5AuditDetails;
                        Session["TotalRows"] = Phase5AuditDetails.Count;
                        grdphase5.DataBind();


                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase5Report_Result> r = new List<SP_Phase5Report_Result>();
                            r = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase5.DataSource as List<SP_Phase5Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("Phase13");
                            dt1.Columns.Add("Phase14");
                            dt1.Columns.Add("Phase15");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = f1;
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = f2;
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = "";
                            dr1["Phase13"] = "";
                            dr1["Phase14"] = "";
                            dr1["Phase15"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase5.DataSource = dt1;
                            grdphase5.DataBind();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindAuditScheduleShowOldData(string flag, int count, int Branchid, int verticalId)
        {
            try
            {
                bool IsDeleted = false;

                if (flag == "A")
                {
                    #region Annualy
                    cleardatasource1();
                    grdAnnually2.DataSource = GetSPAnnualyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                    grdAnnually2.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != "Select Financial Year")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_AnnualyReport_Result> r = new List<SP_AnnualyReport_Result>();

                        r = GetSPAnnualyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdAnnually2.DataSource as List<SP_AnnualyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Annualy1");
                        dt1.Columns.Add("Annualy2");
                        dt1.Columns.Add("Annualy3");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "";
                        dr1["Annualy1"] = f1;
                        dr1["Annualy2"] = f2;
                        dr1["Annualy3"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdAnnually2.DataSource = dt1;
                        grdAnnually2.DataBind();
                    }
                    #endregion
                }
                else if (flag == "H")
                {
                    #region Half Yearly
                    cleardatasource1();
                    grdHalfYearly2.DataSource = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, verticalId);
                    grdHalfYearly2.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_HalfYearlyReport_Result> r = new List<SP_HalfYearlyReport_Result>();
                        r = GetSPHalfYearlywiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdHalfYearly2.DataSource as List<SP_HalfYearlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Halfyearly1");
                        dt1.Columns.Add("Halfyearly2");
                        dt1.Columns.Add("Halfyearly3");
                        dt1.Columns.Add("Halfyearly4");
                        dt1.Columns.Add("Halfyearly5");
                        dt1.Columns.Add("Halfyearly6");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Halfyearly1"] = "";
                        dr1["Halfyearly2"] = f1;
                        dr1["Halfyearly3"] = "";
                        dr1["Halfyearly4"] = f2;
                        dr1["Halfyearly5"] = "";
                        dr1["Halfyearly6"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdHalfYearly2.DataSource = dt1;
                        grdHalfYearly2.DataBind();
                    }
                    #endregion
                }
                else if (flag == "Q")
                {
                    #region Quarterly
                    pnlQuarterly.Visible = true;
                    cleardatasource1();

                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                        grdQuarterly2.DataSource = GetSPQuarterwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdQuarterly2.DataBind();

                        List<SP_QuarterlyReport_Result> r = new List<SP_QuarterlyReport_Result>();
                        r = GetSPQuarterwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdQuarterly2.DataSource as List<SP_QuarterlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Quarter1");
                        dt1.Columns.Add("Quarter2");
                        dt1.Columns.Add("Quarter3");
                        dt1.Columns.Add("Quarter4");
                        dt1.Columns.Add("Quarter5");
                        dt1.Columns.Add("Quarter6");
                        dt1.Columns.Add("Quarter7");
                        dt1.Columns.Add("Quarter8");
                        dt1.Columns.Add("Quarter9");
                        dt1.Columns.Add("Quarter10");
                        dt1.Columns.Add("Quarter11");
                        dt1.Columns.Add("Quarter12");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Quarter1"] = "";
                        dr1["Quarter2"] = "";
                        dr1["Quarter3"] = "";
                        dr1["Quarter4"] = f1;
                        dr1["Quarter5"] = "";
                        dr1["Quarter6"] = "";
                        dr1["Quarter7"] = "";
                        dr1["Quarter8"] = f2;
                        dr1["Quarter9"] = "";
                        dr1["Quarter10"] = "";
                        dr1["Quarter11"] = "";
                        dr1["Quarter12"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdQuarterly2.DataSource = dt1;
                        grdQuarterly2.DataBind();
                    }
                    #endregion
                }
                else if (flag == "M")
                {
                    #region Monthly
                    cleardatasource1();
                    grdMonthly2.DataSource = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                    grdMonthly2.DataBind();
                    if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_MonthlyReport_Result> r = new List<SP_MonthlyReport_Result>();
                        r = GetSPMonthlyReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        DataTable dt = new DataTable();
                        dt = (grdMonthly2.DataSource as List<SP_MonthlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Monthly1");
                        dt1.Columns.Add("Monthly2");
                        dt1.Columns.Add("Monthly3");
                        dt1.Columns.Add("Monthly4");
                        dt1.Columns.Add("Monthly5");
                        dt1.Columns.Add("Monthly6");
                        dt1.Columns.Add("Monthly7");
                        dt1.Columns.Add("Monthly8");
                        dt1.Columns.Add("Monthly9");
                        dt1.Columns.Add("Monthly10");
                        dt1.Columns.Add("Monthly11");
                        dt1.Columns.Add("Monthly12");
                        dt1.Columns.Add("Monthly13");
                        dt1.Columns.Add("Monthly14");
                        dt1.Columns.Add("Monthly15");
                        dt1.Columns.Add("Monthly16");
                        dt1.Columns.Add("Monthly17");
                        dt1.Columns.Add("Monthly18");
                        dt1.Columns.Add("Monthly19");
                        dt1.Columns.Add("Monthly20");
                        dt1.Columns.Add("Monthly21");
                        dt1.Columns.Add("Monthly22");
                        dt1.Columns.Add("Monthly23");
                        dt1.Columns.Add("Monthly24");
                        dt1.Columns.Add("Monthly25");
                        dt1.Columns.Add("Monthly26");
                        dt1.Columns.Add("Monthly27");
                        dt1.Columns.Add("Monthly28");
                        dt1.Columns.Add("Monthly29");
                        dt1.Columns.Add("Monthly30");
                        dt1.Columns.Add("Monthly31");
                        dt1.Columns.Add("Monthly32");
                        dt1.Columns.Add("Monthly33");
                        dt1.Columns.Add("Monthly34");
                        dt1.Columns.Add("Monthly35");
                        dt1.Columns.Add("Monthly36");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Monthly1"] = "";
                        dr1["Monthly2"] = "";
                        dr1["Monthly3"] = "";
                        dr1["Monthly4"] = "";
                        dr1["Monthly5"] = "";
                        dr1["Monthly6"] = "";
                        dr1["Monthly7"] = "";
                        dr1["Monthly8"] = "";
                        dr1["Monthly9"] = "";
                        dr1["Monthly10"] = "";
                        dr1["Monthly11"] = "";
                        dr1["Monthly12"] = f1;
                        dr1["Monthly13"] = "";
                        dr1["Monthly14"] = "";
                        dr1["Monthly15"] = "";
                        dr1["Monthly16"] = "";
                        dr1["Monthly17"] = "";
                        dr1["Monthly18"] = "";
                        dr1["Monthly19"] = "";
                        dr1["Monthly20"] = "";
                        dr1["Monthly21"] = "";
                        dr1["Monthly22"] = "";
                        dr1["Monthly23"] = "";
                        dr1["Monthly24"] = f2;
                        dr1["Monthly25"] = "";
                        dr1["Monthly26"] = "";
                        dr1["Monthly27"] = "";
                        dr1["Monthly28"] = "";
                        dr1["Monthly29"] = "";
                        dr1["Monthly30"] = "";
                        dr1["Monthly31"] = "";
                        dr1["Monthly32"] = "";
                        dr1["Monthly33"] = "";
                        dr1["Monthly34"] = "";
                        dr1["Monthly35"] = "";
                        dr1["Monthly36"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdMonthly2.DataSource = dt1;
                        grdMonthly2.DataBind();
                    }
                    #endregion
                }
                else
                {
                    if (count == 1)
                    {
                        #region Phase1
                        cleardatasource1();
                        grdphase12.DataSource = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase12.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase1Report_Result> r = new List<SP_Phase1Report_Result>();
                            r = GetSPPhase1ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase12.DataSource as List<SP_Phase1Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = f1;
                            dr1["Phase2"] = f2;
                            dr1["Phase3"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase12.DataSource = dt1;
                            grdphase12.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 2)
                    {
                        #region Phase2
                        cleardatasource1();
                        grdphase22.DataSource = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase22.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase2Report_Result> r = new List<SP_Phase2Report_Result>();
                            r = GetSPPhase2ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase22.DataSource as List<SP_Phase2Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = f1;
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f2;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase22.DataSource = dt1;
                            grdphase22.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 3)
                    {
                        #region Phase3
                        cleardatasource1();
                        grdphase32.DataSource = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase32.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase3Report_Result> r = new List<SP_Phase3Report_Result>();
                            r = GetSPPhase3ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase32.DataSource as List<SP_Phase3Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = f1;
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = f2;
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase32.DataSource = dt1;
                            grdphase32.DataBind();
                        }
                        #endregion
                    }
                    else if (count == 4)
                    {
                        #region Phase4
                        cleardatasource1();
                        grdphase42.DataSource = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase42.DataBind();
                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase4Report_Result> r = new List<SP_Phase4Report_Result>();
                            r = GetSPPhase4ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase42.DataSource as List<SP_Phase4Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f1;
                            dr1["Phase5"] = "";
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = f2;
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = "";
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase42.DataSource = dt1;
                            grdphase42.DataBind();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Phase5
                        cleardatasource1();
                        //Added by hardik Sapara for paging.

                        /////////////////// OLD Code ////////
                        //grdphase5.DataSource = GetSPPhase5ReportwiseDisplay(Branchid);
                        //grdphase5.DataBind();

                        // New Code 
                        var Phase5AuditDetails = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                        grdphase52.DataSource = Phase5AuditDetails;
                        Session["TotalRows"] = Phase5AuditDetails.Count;
                        grdphase52.DataBind();


                        if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase5Report_Result> r = new List<SP_Phase5Report_Result>();
                            r = GetSPPhase5ReportwiseDisplay(Branchid, IsDeleted, verticalId);
                            DataTable dt = new DataTable();
                            dt = (grdphase5.DataSource as List<SP_Phase5Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("Phase7");
                            dt1.Columns.Add("Phase8");
                            dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("Phase10");
                            dt1.Columns.Add("Phase11");
                            dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("Phase13");
                            dt1.Columns.Add("Phase14");
                            dt1.Columns.Add("Phase15");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = f1;
                            dr1["Phase6"] = "";
                            dr1["Phase7"] = "";
                            dr1["Phase8"] = "";
                            dr1["Phase9"] = "";
                            dr1["Phase10"] = f2;
                            dr1["Phase11"] = "";
                            dr1["Phase12"] = "";
                            dr1["Phase13"] = "";
                            dr1["Phase14"] = "";
                            dr1["Phase15"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase52.DataSource = dt1;
                            grdphase52.DataBind();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }


        /// <summary>
        /// /////////////
        /// </summary> Code for Second Gridview
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void grdAnnually2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkAnnualy1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkAnnualy2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkAnnualy3");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Annually")
                                            {
                                                bf.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Annually")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Annually")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Annually";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Annually";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Annually";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdAnnually2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdAnnually2.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("A", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }

            }
        }

        protected void grdHalfYearly2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            GridViewRow gvRow = e.Row;
                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 2;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 2;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 2;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell1 = e.Row.Cells[5];
                                otherCell1.Visible = false;
                                TableCell otherCell2 = e.Row.Cells[6];
                                otherCell2.Visible = false;
                                TableCell otherCell3 = e.Row.Cells[7];
                                otherCell3.Visible = false;

                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkHalfyearly1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkHalfyearly2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkHalfyearly3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkHalfyearly4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkHalfyearly5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkHalfyearly6");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Apr-Sep")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Oct-Mar")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Apr-Sep")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termNameSecond == "Oct-Mar")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Apr-Sep")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameThird == "Oct-Mar")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr-Sep";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Oct-Mar";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Apr-Sep";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Oct-Mar";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Apr-Sep";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Oct-Mar";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdHalfYearly2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdHalfYearly2.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("H", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdQuarterly2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 4;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 4;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 4;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell3 = e.Row.Cells[13];
                                otherCell3.Visible = false;

                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                //first financial Year
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkQuarter1");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkQuarter2");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkQuarter3");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkQuarter4");
                                //Second financial Year
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkQuarter5");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkQuarter6");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkQuarter7");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkQuarter8");
                                //Third financial Year
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkQuarter9");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkQuarter10");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkQuarter11");
                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkQuarter12");


                                List<SchedulingReport_Result> schresult = new List<SchedulingReport_Result>();
                                schresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                var remindersummary = schresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Apr-Jun")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Jul-Sep")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Oct-Dec")
                                        {
                                            bf3.Checked = true;
                                        }
                                        else if (termName == "Jan-Mar")
                                        {
                                            bf4.Checked = true;
                                        }
                                    }
                                }

                                #region Header Data
                                if (lblProcessID.Text == "0")
                                {
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr-Jun";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Jul-Sep";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Oct-Dec";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Jan-Mar";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Apr-Jun";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Jul-Sep";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Oct-Dec";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Jan-Mar";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Apr-Jun";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Jul-Sep";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Oct-Dec";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Jan-Mar";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdQuarterly2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdQuarterly2.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("Q", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdMonthly2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 12;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 12;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 12;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;

                                TableCell otherCell14 = e.Row.Cells[14];
                                otherCell14.Visible = false;

                                TableCell otherCell15 = e.Row.Cells[15];
                                otherCell15.Visible = false;

                                TableCell otherCell16 = e.Row.Cells[16];
                                otherCell16.Visible = false;

                                TableCell otherCell17 = e.Row.Cells[17];
                                otherCell17.Visible = false;

                                TableCell otherCell18 = e.Row.Cells[18];
                                otherCell18.Visible = false;

                                TableCell otherCell19 = e.Row.Cells[19];
                                otherCell19.Visible = false;

                                TableCell otherCell20 = e.Row.Cells[20];
                                otherCell20.Visible = false;

                                TableCell otherCell21 = e.Row.Cells[21];
                                otherCell21.Visible = false;

                                TableCell otherCell22 = e.Row.Cells[22];
                                otherCell22.Visible = false;

                                TableCell otherCell23 = e.Row.Cells[23];
                                otherCell23.Visible = false;

                                TableCell otherCell24 = e.Row.Cells[24];
                                otherCell24.Visible = false;

                                TableCell otherCell25 = e.Row.Cells[25];
                                otherCell25.Visible = false;

                                TableCell otherCell26 = e.Row.Cells[26];
                                otherCell26.Visible = false;

                                TableCell otherCell27 = e.Row.Cells[27];
                                otherCell27.Visible = false;

                                TableCell otherCell28 = e.Row.Cells[28];
                                otherCell28.Visible = false;

                                TableCell otherCell29 = e.Row.Cells[29];
                                otherCell29.Visible = false;

                                TableCell otherCell30 = e.Row.Cells[30];
                                otherCell30.Visible = false;


                                TableCell otherCell31 = e.Row.Cells[31];
                                otherCell31.Visible = false;

                                TableCell otherCell32 = e.Row.Cells[32];
                                otherCell32.Visible = false;

                                TableCell otherCell33 = e.Row.Cells[33];
                                otherCell33.Visible = false;

                                TableCell otherCell34 = e.Row.Cells[34];
                                otherCell34.Visible = false;

                                TableCell otherCell35 = e.Row.Cells[35];
                                otherCell35.Visible = false;

                                TableCell otherCell36 = e.Row.Cells[36];
                                otherCell36.Visible = false;

                                TableCell otherCell37 = e.Row.Cells[37];
                                otherCell37.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                //first financial Year
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkMonthly1");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkMonthly2");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkMonthly3");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkMonthly4");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkMonthly5");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkMonthly6");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkMonthly7");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkMonthly8");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkMonthly9");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkMonthly10");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkMonthly11");
                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkMonthly12");
                                //Second financial Year
                                CheckBox bf13 = (CheckBox) e.Row.FindControl("chkMonthly13");
                                CheckBox bf14 = (CheckBox) e.Row.FindControl("chkMonthly14");
                                CheckBox bf15 = (CheckBox) e.Row.FindControl("chkMonthly15");
                                CheckBox bf16 = (CheckBox) e.Row.FindControl("chkMonthly16");
                                CheckBox bf17 = (CheckBox) e.Row.FindControl("chkMonthly17");
                                CheckBox bf18 = (CheckBox) e.Row.FindControl("chkMonthly18");
                                CheckBox bf19 = (CheckBox) e.Row.FindControl("chkMonthly19");
                                CheckBox bf20 = (CheckBox) e.Row.FindControl("chkMonthly20");
                                CheckBox bf21 = (CheckBox) e.Row.FindControl("chkMonthly21");
                                CheckBox bf22 = (CheckBox) e.Row.FindControl("chkMonthly22");
                                CheckBox bf23 = (CheckBox) e.Row.FindControl("chkMonthly23");
                                CheckBox bf24 = (CheckBox) e.Row.FindControl("chkMonthly24");
                                //Third financial Year
                                CheckBox bf25 = (CheckBox) e.Row.FindControl("chkMonthly25");
                                CheckBox bf26 = (CheckBox) e.Row.FindControl("chkMonthly26");
                                CheckBox bf27 = (CheckBox) e.Row.FindControl("chkMonthly27");
                                CheckBox bf28 = (CheckBox) e.Row.FindControl("chkMonthly28");
                                CheckBox bf29 = (CheckBox) e.Row.FindControl("chkMonthly29");
                                CheckBox bf30 = (CheckBox) e.Row.FindControl("chkMonthly30");
                                CheckBox bf31 = (CheckBox) e.Row.FindControl("chkMonthly31");
                                CheckBox bf32 = (CheckBox) e.Row.FindControl("chkMonthly32");
                                CheckBox bf33 = (CheckBox) e.Row.FindControl("chkMonthly33");
                                CheckBox bf34 = (CheckBox) e.Row.FindControl("chkMonthly34");
                                CheckBox bf35 = (CheckBox) e.Row.FindControl("chkMonthly35");
                                CheckBox bf36 = (CheckBox) e.Row.FindControl("chkMonthly36");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Apr")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "May")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Jun")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termName == "Jul")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termName == "Aug")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termName == "Sep")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termName == "Oct")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termName == "Nov")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termName == "Dec")
                                            {
                                                bf9.Checked = true;
                                            }
                                            else if (termName == "Jan")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termName == "Feb")
                                            {
                                                bf11.Checked = true;
                                            }
                                            else if (termName == "Mar")
                                            {
                                                bf12.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Apr")
                                            {
                                                bf13.Checked = true;
                                            }
                                            else if (termNameSecond == "May")
                                            {
                                                bf14.Checked = true;
                                            }
                                            else if (termNameSecond == "Jun")
                                            {
                                                bf15.Checked = true;
                                            }
                                            else if (termNameSecond == "Jul")
                                            {
                                                bf16.Checked = true;
                                            }
                                            else if (termNameSecond == "Aug")
                                            {
                                                bf17.Checked = true;
                                            }
                                            else if (termNameSecond == "Sep")
                                            {
                                                bf18.Checked = true;
                                            }
                                            else if (termNameSecond == "Oct")
                                            {
                                                bf19.Checked = true;
                                            }
                                            else if (termNameSecond == "Nov")
                                            {
                                                bf20.Checked = true;
                                            }
                                            else if (termNameSecond == "Dec")
                                            {
                                                bf21.Checked = true;
                                            }
                                            else if (termNameSecond == "Jan")
                                            {
                                                bf22.Checked = true;
                                            }
                                            else if (termNameSecond == "Feb")
                                            {
                                                bf23.Checked = true;
                                            }
                                            else if (termNameSecond == "Mar")
                                            {
                                                bf24.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Apr")
                                            {
                                                bf25.Checked = true;
                                            }
                                            else if (termNameThird == "May")
                                            {
                                                bf26.Checked = true;
                                            }
                                            else if (termNameThird == "Jun")
                                            {
                                                bf27.Checked = true;
                                            }
                                            else if (termNameThird == "Jul")
                                            {
                                                bf28.Checked = true;
                                            }
                                            else if (termNameThird == "Aug")
                                            {
                                                bf29.Checked = true;
                                            }
                                            else if (termNameThird == "Sep")
                                            {
                                                bf30.Checked = true;
                                            }
                                            else if (termNameThird == "Oct")
                                            {
                                                bf31.Checked = true;
                                            }
                                            else if (termNameThird == "Nov")
                                            {
                                                bf32.Checked = true;
                                            }
                                            else if (termNameThird == "Dec")
                                            {
                                                bf33.Checked = true;
                                            }
                                            else if (termNameThird == "Jan")
                                            {
                                                bf34.Checked = true;
                                            }
                                            else if (termNameThird == "Feb")
                                            {
                                                bf35.Checked = true;
                                            }
                                            else if (termNameThird == "Mar")
                                            {
                                                bf36.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    //first financial Year
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;
                                    //Second financial Year
                                    bf13.Visible = false;
                                    bf14.Visible = false;
                                    bf15.Visible = false;
                                    bf16.Visible = false;
                                    bf17.Visible = false;
                                    bf18.Visible = false;
                                    bf19.Visible = false;
                                    bf20.Visible = false;
                                    bf21.Visible = false;
                                    bf22.Visible = false;
                                    bf23.Visible = false;
                                    bf24.Visible = false;
                                    //Third financial Year
                                    bf25.Visible = false;
                                    bf26.Visible = false;
                                    bf27.Visible = false;
                                    bf28.Visible = false;
                                    bf29.Visible = false;
                                    bf30.Visible = false;
                                    bf31.Visible = false;
                                    bf32.Visible = false;
                                    bf33.Visible = false;
                                    bf34.Visible = false;
                                    bf35.Visible = false;
                                    bf36.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Apr";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "May";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Jun";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Jul";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Aug";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Sep";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Oct";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Nov";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Dec";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Jan";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Feb";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Mar";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell13 = e.Row.Cells[14];
                                    cell13.ColumnSpan = 1;
                                    cell13.Text = "Apr";
                                    cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell13.BorderColor = Color.White;
                                    cell13.ForeColor = Color.White;
                                    cell13.Font.Bold = true;
                                    cell13.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell14 = e.Row.Cells[15];
                                    cell14.ColumnSpan = 1;
                                    cell14.Text = "May";
                                    cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell14.BorderColor = Color.White;
                                    cell14.ForeColor = Color.White;
                                    cell14.Font.Bold = true;
                                    cell14.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell15 = e.Row.Cells[16];
                                    cell15.ColumnSpan = 1;
                                    cell15.Text = "Jun";
                                    cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell15.BorderColor = Color.White;
                                    cell15.ForeColor = Color.White;
                                    cell15.Font.Bold = true;
                                    cell15.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell16 = e.Row.Cells[17];
                                    cell16.ColumnSpan = 1;
                                    cell16.Text = "Jul";
                                    cell16.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell16.BorderColor = Color.White;
                                    cell16.ForeColor = Color.White;
                                    cell16.Font.Bold = true;
                                    cell16.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell17 = e.Row.Cells[18];
                                    cell17.ColumnSpan = 1;
                                    cell17.Text = "Aug";
                                    cell17.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell17.BorderColor = Color.White;
                                    cell17.ForeColor = Color.White;
                                    cell17.Font.Bold = true;
                                    cell17.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell18 = e.Row.Cells[19];
                                    cell18.ColumnSpan = 1;
                                    cell18.Text = "Sep";
                                    cell18.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell18.BorderColor = Color.White;
                                    cell18.ForeColor = Color.White;
                                    cell18.Font.Bold = true;
                                    cell18.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell19 = e.Row.Cells[20];
                                    cell19.ColumnSpan = 1;
                                    cell19.Text = "Oct";
                                    cell19.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell19.BorderColor = Color.White;
                                    cell19.ForeColor = Color.White;
                                    cell19.Font.Bold = true;
                                    cell19.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell20 = e.Row.Cells[21];
                                    cell20.ColumnSpan = 1;
                                    cell20.Text = "Nov";
                                    cell20.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell20.BorderColor = Color.White;
                                    cell20.ForeColor = Color.White;
                                    cell20.Font.Bold = true;
                                    cell20.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell21 = e.Row.Cells[22];
                                    cell21.ColumnSpan = 1;
                                    cell21.Text = "Dec";
                                    cell21.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell21.BorderColor = Color.White;
                                    cell21.ForeColor = Color.White;
                                    cell21.Font.Bold = true;
                                    cell21.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell22 = e.Row.Cells[23];
                                    cell22.ColumnSpan = 1;
                                    cell22.Text = "Jan";
                                    cell22.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell22.BorderColor = Color.White;
                                    cell22.ForeColor = Color.White;
                                    cell22.Font.Bold = true;
                                    cell22.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell23 = e.Row.Cells[24];
                                    cell23.ColumnSpan = 1;
                                    cell23.Text = "Feb";
                                    cell23.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell23.BorderColor = Color.White;
                                    cell23.ForeColor = Color.White;
                                    cell23.Font.Bold = true;
                                    cell23.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell24 = e.Row.Cells[25];
                                    cell24.ColumnSpan = 1;
                                    cell24.Text = "Mar";
                                    cell24.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell24.BorderColor = Color.White;
                                    cell24.ForeColor = Color.White;
                                    cell24.Font.Bold = true;
                                    cell24.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell25 = e.Row.Cells[26];
                                    cell25.ColumnSpan = 1;
                                    cell25.Text = "Apr";
                                    cell25.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell25.BorderColor = Color.White;
                                    cell25.ForeColor = Color.White;
                                    cell25.Font.Bold = true;
                                    cell25.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell26 = e.Row.Cells[27];
                                    cell26.ColumnSpan = 1;
                                    cell26.Text = "May";
                                    cell26.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell26.BorderColor = Color.White;
                                    cell26.ForeColor = Color.White;
                                    cell26.Font.Bold = true;
                                    cell26.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell27 = e.Row.Cells[28];
                                    cell27.ColumnSpan = 1;
                                    cell27.Text = "Jun";
                                    cell27.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell27.BorderColor = Color.White;
                                    cell27.ForeColor = Color.White;
                                    cell27.Font.Bold = true;
                                    cell27.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell28 = e.Row.Cells[29];
                                    cell28.ColumnSpan = 1;
                                    cell28.Text = "Jul";
                                    cell28.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell28.BorderColor = Color.White;
                                    cell28.ForeColor = Color.White;
                                    cell28.Font.Bold = true;
                                    cell28.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell29 = e.Row.Cells[30];
                                    cell29.ColumnSpan = 1;
                                    cell29.Text = "Aug";
                                    cell29.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell29.BorderColor = Color.White;
                                    cell29.ForeColor = Color.White;
                                    cell29.Font.Bold = true;
                                    cell29.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell30 = e.Row.Cells[31];
                                    cell30.ColumnSpan = 1;
                                    cell30.Text = "Sep";
                                    cell30.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell30.BorderColor = Color.White;
                                    cell30.ForeColor = Color.White;
                                    cell30.Font.Bold = true;
                                    cell30.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell31 = e.Row.Cells[32];
                                    cell31.ColumnSpan = 1;
                                    cell31.Text = "Oct";
                                    cell31.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell31.BorderColor = Color.White;
                                    cell31.ForeColor = Color.White;
                                    cell31.Font.Bold = true;
                                    cell31.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell32 = e.Row.Cells[33];
                                    cell32.ColumnSpan = 1;
                                    cell32.Text = "Nov";
                                    cell32.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell32.BorderColor = Color.White;
                                    cell32.ForeColor = Color.White;
                                    cell32.Font.Bold = true;
                                    cell32.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell33 = e.Row.Cells[34];
                                    cell33.ColumnSpan = 1;
                                    cell33.Text = "Dec";
                                    cell33.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell33.BorderColor = Color.White;
                                    cell33.ForeColor = Color.White;
                                    cell33.Font.Bold = true;
                                    cell33.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell34 = e.Row.Cells[35];
                                    cell34.ColumnSpan = 1;
                                    cell34.Text = "Jan";
                                    cell34.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell34.BorderColor = Color.White;
                                    cell34.ForeColor = Color.White;
                                    cell34.Font.Bold = true;
                                    cell34.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell35 = e.Row.Cells[36];
                                    cell35.ColumnSpan = 1;
                                    cell35.Text = "Feb";
                                    cell35.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell35.BorderColor = Color.White;
                                    cell35.ForeColor = Color.White;
                                    cell35.Font.Bold = true;
                                    cell35.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell36 = e.Row.Cells[37];
                                    cell36.ColumnSpan = 1;
                                    cell36.Text = "Mar";
                                    cell36.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell36.BorderColor = Color.White;
                                    cell36.ForeColor = Color.White;
                                    cell36.Font.Bold = true;
                                    cell36.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMonthly2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdMonthly2.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("M", 0, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdphase12_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdphase12_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase12.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("P", 1, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdphase22_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = true;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 2;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 2;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 2;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell1 = e.Row.Cells[5];
                                otherCell1.Visible = false;

                                TableCell otherCell2 = e.Row.Cells[6];
                                otherCell2.Visible = false;

                                TableCell otherCell3 = e.Row.Cells[7];
                                otherCell3.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }

                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 1";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 2";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 1";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 2";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdphase22_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase22.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("P", 2, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdphase32_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 3;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 3;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 3;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf5.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf8.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;


                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 1";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 2";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 3";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 1";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 2";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 3";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdphase32_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase32.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("P", 3, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdphase42_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 4;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 4;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 4;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkPhase10");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkPhase11");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkPhase12");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Phase4")
                                            {
                                                bf3.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf4.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase4")
                                            {
                                                bf7.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf9.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termNameThird == "Phase4")
                                            {
                                                bf11.Checked = true;
                                            }

                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;

                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 4";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 1";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 2";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 3";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 4";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 1";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Phase 2";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Phase 3";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Phase 4";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdphase42_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase42.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("P", 4, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void grdphase52_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                bool IsDeleted = false;
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            string financialyear = ddlFinancialYear.SelectedItem.Text;
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            if (gvRow.RowType == DataControlRowType.Header)
                            {
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 5;
                                cell1.Text = f1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 5;
                                cell2.Text = f2;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Center;

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 5;
                                cell3.Text = f3;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Center;

                                TableCell otherCell5 = e.Row.Cells[5];
                                otherCell5.Visible = false;

                                TableCell otherCell6 = e.Row.Cells[6];
                                otherCell6.Visible = false;

                                TableCell otherCell7 = e.Row.Cells[7];
                                otherCell7.Visible = false;

                                TableCell otherCell8 = e.Row.Cells[8];
                                otherCell8.Visible = false;

                                TableCell otherCell9 = e.Row.Cells[9];
                                otherCell9.Visible = false;

                                TableCell otherCell10 = e.Row.Cells[10];
                                otherCell10.Visible = false;

                                TableCell otherCell11 = e.Row.Cells[11];
                                otherCell11.Visible = false;

                                TableCell otherCell12 = e.Row.Cells[12];
                                otherCell12.Visible = false;

                                TableCell otherCell13 = e.Row.Cells[13];
                                otherCell13.Visible = false;

                                TableCell otherCell14 = e.Row.Cells[14];
                                otherCell14.Visible = false;

                                TableCell otherCell15 = e.Row.Cells[15];
                                otherCell15.Visible = false;

                                TableCell otherCell16 = e.Row.Cells[16];
                                otherCell16.Visible = false;
                            }
                            if (gvRow.RowType == DataControlRowType.DataRow)
                            {
                                Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                                CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                                CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                                CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                                CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                                CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");
                                CheckBox bf5 = (CheckBox) e.Row.FindControl("chkPhase6");
                                CheckBox bf6 = (CheckBox) e.Row.FindControl("chkPhase7");
                                CheckBox bf7 = (CheckBox) e.Row.FindControl("chkPhase8");
                                CheckBox bf8 = (CheckBox) e.Row.FindControl("chkPhase9");
                                CheckBox bf9 = (CheckBox) e.Row.FindControl("chkPhase10");
                                CheckBox bf10 = (CheckBox) e.Row.FindControl("chkPhase11");
                                CheckBox bf11 = (CheckBox) e.Row.FindControl("chkPhase12");

                                CheckBox bf12 = (CheckBox) e.Row.FindControl("chkPhase12");
                                CheckBox bf13 = (CheckBox) e.Row.FindControl("chkPhase13");
                                CheckBox bf14 = (CheckBox) e.Row.FindControl("chkPhase14");

                                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                                {
                                    List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                    schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                    string termName = string.Empty;
                                    if (remindersummary.Count > 0)
                                    {
                                        foreach (var row in remindersummary)
                                        {
                                            termName = row.TermName;
                                            if (termName == "Phase1")
                                            {
                                                bf.Checked = true;
                                            }
                                            else if (termName == "Phase2")
                                            {
                                                bf1.Checked = true;
                                            }
                                            else if (termName == "Phase3")
                                            {
                                                bf2.Checked = true;
                                            }
                                            else if (termName == "Phase4")
                                            {
                                                bf3.Checked = true;
                                            }
                                            else if (termName == "Phase5")
                                            {
                                                bf4.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schSecondresult = new List<SchedulingReport_Result>();
                                    schSecondresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f2, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummarySecond = schSecondresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameSecond = string.Empty;
                                    if (remindersummarySecond.Count > 0)
                                    {
                                        foreach (var row in remindersummarySecond)
                                        {
                                            termNameSecond = row.TermName;
                                            if (termNameSecond == "Phase1")
                                            {
                                                bf5.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase2")
                                            {
                                                bf6.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase3")
                                            {
                                                bf7.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase4")
                                            {
                                                bf8.Checked = true;
                                            }
                                            else if (termNameSecond == "Phase5")
                                            {
                                                bf9.Checked = true;
                                            }
                                        }
                                    }
                                    List<SchedulingReport_Result> schThirdresult = new List<SchedulingReport_Result>();
                                    schThirdresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(GetBranchID()), Convert.ToInt32(lblProcessID.Text), f3, Convert.ToInt32(ddlVerticalID.SelectedValue)).ToList();
                                    var remindersummaryThird = schThirdresult.OrderBy(entry => entry.TermName).ToList();
                                    string termNameThird = string.Empty;
                                    if (remindersummaryThird.Count > 0)
                                    {
                                        foreach (var row in remindersummaryThird)
                                        {
                                            termNameThird = row.TermName;
                                            if (termNameThird == "Phase1")
                                            {
                                                bf10.Checked = true;
                                            }
                                            else if (termNameThird == "Phase2")
                                            {
                                                bf11.Checked = true;
                                            }
                                            else if (termNameThird == "Phase3")
                                            {
                                                bf12.Checked = true;
                                            }
                                            else if (termNameThird == "Phase4")
                                            {
                                                bf13.Checked = true;
                                            }
                                            else if (termNameThird == "Phase5")
                                            {
                                                bf14.Checked = true;
                                            }
                                        }
                                    }
                                }
                                if (lblProcessID.Text == "0")
                                {
                                    bf.Visible = false;
                                    bf1.Visible = false;
                                    bf2.Visible = false;
                                    bf3.Visible = false;
                                    bf4.Visible = false;
                                    bf5.Visible = false;
                                    bf6.Visible = false;
                                    bf7.Visible = false;
                                    bf8.Visible = false;
                                    bf9.Visible = false;
                                    bf10.Visible = false;
                                    bf11.Visible = false;
                                    bf12.Visible = false;
                                    bf13.Visible = false;
                                    bf14.Visible = false;


                                    TableCell cell = e.Row.Cells[1];
                                    cell.ColumnSpan = 1;
                                    cell.Text = "Process";
                                    cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell.BorderColor = Color.White;
                                    cell.ForeColor = Color.White;
                                    cell.Font.Bold = true;
                                    cell.HorizontalAlign = HorizontalAlign.Left;

                                    TableCell cell1 = e.Row.Cells[2];
                                    cell1.ColumnSpan = 1;
                                    cell1.Text = "Phase 1";
                                    cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell1.BorderColor = Color.White;
                                    cell1.ForeColor = Color.White;
                                    cell1.Font.Bold = true;
                                    cell1.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell2 = e.Row.Cells[3];
                                    cell2.ColumnSpan = 1;
                                    cell2.Text = "Phase 2";
                                    cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell2.BorderColor = Color.White;
                                    cell2.ForeColor = Color.White;
                                    cell2.Font.Bold = true;
                                    cell2.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell3 = e.Row.Cells[4];
                                    cell3.ColumnSpan = 1;
                                    cell3.Text = "Phase 3";
                                    cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell3.BorderColor = Color.White;
                                    cell3.ForeColor = Color.White;
                                    cell3.Font.Bold = true;
                                    cell3.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell4 = e.Row.Cells[5];
                                    cell4.ColumnSpan = 1;
                                    cell4.Text = "Phase 4";
                                    cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell4.BorderColor = Color.White;
                                    cell4.ForeColor = Color.White;
                                    cell4.Font.Bold = true;
                                    cell4.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell5 = e.Row.Cells[6];
                                    cell5.ColumnSpan = 1;
                                    cell5.Text = "Phase 5";
                                    cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell5.BorderColor = Color.White;
                                    cell5.ForeColor = Color.White;
                                    cell5.Font.Bold = true;
                                    cell5.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell6 = e.Row.Cells[7];
                                    cell6.ColumnSpan = 1;
                                    cell6.Text = "Phase 1";
                                    cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell6.BorderColor = Color.White;
                                    cell6.ForeColor = Color.White;
                                    cell6.Font.Bold = true;
                                    cell6.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell7 = e.Row.Cells[8];
                                    cell7.ColumnSpan = 1;
                                    cell7.Text = "Phase 2";
                                    cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell7.BorderColor = Color.White;
                                    cell7.ForeColor = Color.White;
                                    cell7.Font.Bold = true;
                                    cell7.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell8 = e.Row.Cells[9];
                                    cell8.ColumnSpan = 1;
                                    cell8.Text = "Phase 3";
                                    cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell8.BorderColor = Color.White;
                                    cell8.ForeColor = Color.White;
                                    cell8.Font.Bold = true;
                                    cell8.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell9 = e.Row.Cells[10];
                                    cell9.ColumnSpan = 1;
                                    cell9.Text = "Phase 4";
                                    cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell9.BorderColor = Color.White;
                                    cell9.ForeColor = Color.White;
                                    cell9.Font.Bold = true;
                                    cell9.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell10 = e.Row.Cells[11];
                                    cell10.ColumnSpan = 1;
                                    cell10.Text = "Phase 5";
                                    cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell10.BorderColor = Color.White;
                                    cell10.ForeColor = Color.White;
                                    cell10.Font.Bold = true;
                                    cell10.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell11 = e.Row.Cells[12];
                                    cell11.ColumnSpan = 1;
                                    cell11.Text = "Phase 1";
                                    cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell11.BorderColor = Color.White;
                                    cell11.ForeColor = Color.White;
                                    cell11.Font.Bold = true;
                                    cell11.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell12 = e.Row.Cells[13];
                                    cell12.ColumnSpan = 1;
                                    cell12.Text = "Phase 2";
                                    cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell12.BorderColor = Color.White;
                                    cell12.ForeColor = Color.White;
                                    cell12.Font.Bold = true;
                                    cell12.HorizontalAlign = HorizontalAlign.Center;


                                    TableCell cell13 = e.Row.Cells[14];
                                    cell13.ColumnSpan = 1;
                                    cell13.Text = "Phase 3";
                                    cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell13.BorderColor = Color.White;
                                    cell13.ForeColor = Color.White;
                                    cell13.Font.Bold = true;
                                    cell13.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell14 = e.Row.Cells[15];
                                    cell14.ColumnSpan = 1;
                                    cell14.Text = "Phase 4";
                                    cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell14.BorderColor = Color.White;
                                    cell14.ForeColor = Color.White;
                                    cell14.Font.Bold = true;
                                    cell14.HorizontalAlign = HorizontalAlign.Center;

                                    TableCell cell15 = e.Row.Cells[16];
                                    cell15.ColumnSpan = 1;
                                    cell15.Text = "Phase 5";
                                    cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                    cell15.BorderColor = Color.White;
                                    cell15.ForeColor = Color.White;
                                    cell15.Font.Bold = true;
                                    cell15.HorizontalAlign = HorizontalAlign.Center;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdphase52_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string drpValue = Convert.ToString(GetBranchID());
            grdphase52.PageIndex = e.NewPageIndex;
            if (drpValue != "-1")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                {
                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    BindAuditScheduleShowOldData("P", 5, Convert.ToInt32(GetBranchID()), VerticalID);
                }
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")

                    BindVerticalID(Convert.ToInt32(ddlSubEntity4.SelectedValue));
            }
        }
    

        ////////////Export to Excel////////////////

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        public void ExportListFromGridViewNeW()
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Internal Financial Control");
                    DataTable ExcelData = null;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ISAHQMPReport"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["customerbranchidReport"])))
                        {

                            if (ViewState["ISAHQMPReport"].ToString() == "A")
                            {
                                BindAuditSchedule("A", 0, Convert.ToInt32(ViewState["customerbranchidReport"]),Convert.ToInt32(ViewState["VerticalID"]));
                                //grdAnnually.RenderControl(htw);
                                DataView view = new System.Data.DataView((grdAnnually.DataSource as List<AnnualyReportView>).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "Name", "Annualy1", "Annualy2", "Annualy3");

                            }
                            else if (ViewState["ISAHQMPReport"].ToString() == "H")
                            {
                                BindAuditSchedule("H", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                //grdHalfYearly.RenderControl(htw);
                                DataView view = new System.Data.DataView((grdHalfYearly.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                            }
                            else if (ViewState["ISAHQMPReport"].ToString() == "Q")
                            {
                                BindAuditSchedule("Q", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                //grdQuarterly.RenderControl(htw);
                                DataView view = new System.Data.DataView((grdQuarterly.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                            }
                            else if (ViewState["ISAHQMPReport"].ToString() == "M")
                            {
                                BindAuditSchedule("M", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                //grdMonthly.RenderControl(htw);
                                DataView view = new System.Data.DataView((grdMonthly.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                            }
                            else if (ViewState["ISAHQMPReport"].ToString() == "P")
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["PhaseCountReport"])))
                                {
                                    if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 1)
                                    {
                                        BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                        //grdphase1.RenderControl(htw);
                                        DataView view = new System.Data.DataView((grdphase1.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                                    }
                                    else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 2)
                                    {
                                        BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                        //grdphase2.RenderControl(htw);
                                        DataView view = new System.Data.DataView((grdphase2.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                                    }
                                    else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 3)
                                    {
                                        BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                        //grdphase3.RenderControl(htw);
                                        DataView view = new System.Data.DataView((grdphase3.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                                    }
                                    else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 4)
                                    {
                                        BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                        //grdphase4.RenderControl(htw);
                                        DataView view = new System.Data.DataView((grdphase4.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                                    }
                                    else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 5)
                                    {
                                        BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), Convert.ToInt32(ViewState["VerticalID"]));
                                        //grdphase5.RenderControl(htw);
                                        DataView view = new System.Data.DataView((grdphase5.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                                    }
                                }
                            }
                        }
                    }
                    
                    var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    exWorkSheet.Cells["A2"].Value = customer.Name;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A3"].Value = ddlLegalEntity.SelectedItem.Text;
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A4"].Value = ddlFinancialYear.SelectedItem.Text;
                    exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A4"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A8"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A6"].Value = "Internal Financial Control";
                    exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A6"].Style.Font.Size = 12;


                    exWorkSheet.Cells["A8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A8"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B8"].Value = "Sub Process";
                    exWorkSheet.Cells["B8"].Style.Font.Size = 12;

                    exWorkSheet.Cells["C8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C8"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C8"].Value = "Risk Description";


                    exWorkSheet.Cells["D8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D8"].Value = "Control Description";
                    exWorkSheet.Cells["D8"].Style.Font.Size = 12;


                    exWorkSheet.Cells["E8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E8"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E8"].Value = "Key /NonKey";

                    exWorkSheet.Cells["F8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F8"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F8"].Value = "TOD (Pass or Fail)";


                    exWorkSheet.Cells["G8"].Value = "TOE (Pass or Fail)";
                    exWorkSheet.Cells["G8"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G8"].Style.Font.Size = 12;

                    using (ExcelRange col = exWorkSheet.Cells[2, 1, 8 + ExcelData.Rows.Count, 8])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.AutoFitColumns();
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=TestingStatusReport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                }
                catch (Exception)
                {
                }
            }
        }
        public void ExportListFromGridView()
        {
            string DropDownListName = string.Empty;
            int verticalId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    DropDownListName = ddlLegalEntity.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    DropDownListName = ddlSubEntity1.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    DropDownListName = ddlSubEntity2.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    DropDownListName = ddlSubEntity3.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    DropDownListName = ddlSubEntity4.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
            {
                if (ddlVerticalID.SelectedValue != "-1")
                {
                    verticalId = Convert.ToInt32(ddlVerticalID.SelectedValue);
                }
            }
            System.Web.UI.HtmlControls.HtmlGenericControl previousText = new System.Web.UI.HtmlControls.HtmlGenericControl("p");
            previousText.InnerText = "Previous";
            System.Web.UI.HtmlControls.HtmlGenericControl CurrentText = new System.Web.UI.HtmlControls.HtmlGenericControl("p");
            CurrentText.InnerText = "Current";
            string drpValue = Convert.ToString(GetBranchID());
            if (drpValue != "-1")
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    string FileName = string.Empty;
                    FileName = DropDownListName + "-" + ddlFinancialYear.SelectedItem.Text;
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xls");
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    using (System.IO.StringWriter sw = new System.IO.StringWriter())
                    {
                        using (System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw))
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ISAHQMPReport"])))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["customerbranchidReport"])))
                                {

                                    if (ViewState["ISAHQMPReport"].ToString() == "A")
                                    {
                                        BindAuditSchedule("A", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        BindAuditScheduleShowOldData("A", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        grdAnnually.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        grdAnnually2.RenderControl(htw);
                                    }
                                    else if (ViewState["ISAHQMPReport"].ToString() == "H")
                                    {
                                        BindAuditSchedule("H", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        BindAuditScheduleShowOldData("H", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        previousText.RenderControl(htw);
                                        grdHalfYearly.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        CurrentText.RenderControl(htw);
                                        grdHalfYearly2.RenderControl(htw);
                                    }
                                    else if (ViewState["ISAHQMPReport"].ToString() == "Q")
                                    {
                                        BindAuditSchedule("Q", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        BindAuditScheduleShowOldData("Q", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        grdQuarterly.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        grdQuarterly2.RenderControl(htw);
                                    }
                                    else if (ViewState["ISAHQMPReport"].ToString() == "M")
                                    {
                                        BindAuditSchedule("M", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        BindAuditScheduleShowOldData("M", 0, Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                        grdMonthly.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        newlinebreak.RenderControl(htw);
                                        grdMonthly2.RenderControl(htw);

                                    }
                                    else if (ViewState["ISAHQMPReport"].ToString() == "P")
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["PhaseCountReport"])))
                                        {
                                            if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 1)
                                            {
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]),verticalId);
                                                grdphase1.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                grdphase12.RenderControl(htw);
                                            }
                                            else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 2)
                                            {
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                BindAuditScheduleShowOldData("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                grdphase2.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                grdphase22.RenderControl(htw);
                                            }
                                            else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 3)
                                            {
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                BindAuditScheduleShowOldData("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                grdphase3.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                grdphase32.RenderControl(htw);
                                            }
                                            else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 4)
                                            {
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                BindAuditScheduleShowOldData("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                grdphase4.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                grdphase42.RenderControl(htw);
                                            }
                                            else if (Convert.ToInt32(ViewState["PhaseCountReport"]) == 5)
                                            {
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                BindAuditSchedule("P", Convert.ToInt32(ViewState["PhaseCountReport"]), Convert.ToInt32(ViewState["customerbranchidReport"]), verticalId);
                                                
                                                grdphase5.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                newlinebreak.RenderControl(htw);
                                                grdphase52.RenderControl(htw);
                                            }
                                        }
                                    }
                                }
                            }
                            Response.Write(sw.ToString());
                        }
                    }
                    Response.End();
                }
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string drpValue = Convert.ToString(GetBranchID());
                if (!string.IsNullOrEmpty(drpValue))
                {
                    ExportListFromGridView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["ISAHQMPReport"] = null;
            ViewState["customerbranchidReport"] = null;
            ViewState["PhaseCountReport"] = null;
            int customerbranchid = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            customerbranchid = Convert.ToInt32(GetBranchID());
                            var ISAHQMPDetails = GetSchedulingReportISAHQMP_ResultProcedure(customerbranchid, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlVerticalID.SelectedValue));
                            if (ISAHQMPDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ddlVerticalID.SelectedValue)))
                                {
                                    int VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                                    ViewState["VerticalID"] = VerticalID;
                                    if (ISAHQMPDetails.ISAHQMP == "A")
                                    {
                                        ViewState["ISAHQMPReport"] = "A";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "A");
                                        EnableDisable2(0, "A");
                                        BindAuditSchedule("A", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("A", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "H")
                                    {
                                        ViewState["ISAHQMPReport"] = "H";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "H");
                                        EnableDisable2(0, "H");
                                        BindAuditSchedule("H", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("H", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "Q")
                                    {
                                        ViewState["ISAHQMPReport"] = "Q";

                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "Q");
                                        EnableDisable2(0, "Q");
                                        BindAuditSchedule("Q", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("Q", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "M")
                                    {
                                        ViewState["ISAHQMPReport"] = "M";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        EnableDisable(0, "M");
                                        EnableDisable2(0, "M");
                                        BindAuditSchedule("M", 0, customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("M", 0, customerbranchid, VerticalID);
                                    }
                                    else if (ISAHQMPDetails.ISAHQMP == "P")
                                    {
                                        ViewState["ISAHQMPReport"] = "P";
                                        ViewState["customerbranchidReport"] = customerbranchid;
                                        ViewState["PhaseCountReport"] = ISAHQMPDetails.PhaseCount;
                                        EnableDisable(Convert.ToInt32(ISAHQMPDetails.PhaseCount), "P");
                                        EnableDisable2(Convert.ToInt32(ISAHQMPDetails.PhaseCount), "P");
                                        BindAuditSchedule("P", Convert.ToInt32(ISAHQMPDetails.PhaseCount), customerbranchid, VerticalID);
                                        BindAuditScheduleShowOldData("P", Convert.ToInt32(ISAHQMPDetails.PhaseCount), customerbranchid, VerticalID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}