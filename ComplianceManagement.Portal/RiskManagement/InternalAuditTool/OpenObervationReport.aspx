﻿<%@ Page Title="Open Obervation Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
    CodeBehind="OpenObervationReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.OpenObervationReport" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>


    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>


    <script type="text/javascript">
        $(document).ready(function () {
            //setactivemenu('leftreportsmenu');
               <%if (roles.Contains(3) || roles.Contains(4))%>
               <%{%>
               <% if (AuditHeadOrManagerReport == null)%>
                    <%{%>
               fhead('My Reports');
                    <%}
        else
        {%>
               fhead('Open Observation Report');
               <%}%>
               <%}%>
               <%else%>
               <%{%>
               fhead('Open Observation Report');
               <%}%>
           });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">


                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel"> 
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs" style="margin: -10px -2px -11px 0;">
                                           <%if (roles.Contains(3) || roles.Contains(4))%>
                                           <%{%>

                                                    <%if (roles.Contains(3))%>
                                                    <%{%>                                                              
                                                        <li class="active" id="liPerformer" runat="server">
                                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                        </li>
                                                    <%}%>
                                                    <%if (roles.Contains(4))%>
                                                    <%{%>      
                                                        <li class=""  id="liReviewer" runat="server">
                                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                        </li> 
                                                    <%}%>
                                                    <% if (AuditHeadOrManagerReport == null)%>
                                                    <%{%>
                                                    <li style="float: right;">   
                                                        <div>
                                                            <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="../AuditTool/MyReportAudit.aspx">Final Audit</a></li>
                                                                    <li><a href="../InternalAuditTool/AuditObservationDraft.aspx">Draft Observation</a></li>                                                                    
                                                                    <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation-Word</a></li>                                                                    
                                                                    <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                    <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li>
                                                                    <li><a href="../InternalAuditTool/PastAuditUpload.aspx">Past Audit Reports</a></li>
                                                                                                                        
                                                                </ul>
                                                        </div>
                                                    </li>
                                                    <%}%> 
                                           <%}%>                                         
                                            </ul>
                                </header>
                            <div class="clearfix"></div>     
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              
                                <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Unit" 
                                    OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                          
                                 <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                             
                                 <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              
                                 <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                             
                                 <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                         
                                  <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="90%" 
                                      Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" 
                                      DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged1">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            
                                 <asp:DropDownListChosen ID="ddlSchedulingType" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Scheduling Type" 
                                    OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                             

                                  <asp:DropDownListChosen ID="ddlPeriod" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Select Period" >
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                         

                                  <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Select Process" 
                                    OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                             <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                             
                                 <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" DataPlaceHolder="Vertical" 
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                       OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged"
                                     class="form-control m-bot15" Width="90%" Height="32px">
                                </asp:DropDownListChosen>
                            </div>
                              <%}%>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float: right;">
                                
                                <div style="float: right; margin-right: 10%;">
                                    <asp:Button ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click" class="btn btn-search" runat="server" Text="Export to Excel"></asp:Button>                                    
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div>
                            &nbsp;
                            <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                                Font-Size="12px">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  HeaderText="Location">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>' ></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     
                                        <asp:TemplateField  HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;"> 
                                                <asp:Label ID="lblFyear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         <asp:TemplateField  HeaderText="Period">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;"> 
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                         <asp:TemplateField  HeaderText="Observation">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 150px;"> 
                                                <asp:Label ID="lblObservation" runat="server" Text='<%# Eval("Observation") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Observation") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField  HeaderText="Recomendation">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblRecomendation" runat="server" Text='<%# Eval("Recomendation") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Recomendation") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField  HeaderText="Management&nbsp;Response">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblManagementResponse" runat="server" Text='<%# Eval("ManagementResponse") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ManagementResponse") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Time Line" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTimeline" runat="server" Text='<%# Eval("TimeLine")!=null?Convert.ToDateTime(Eval("TimeLine")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />  
                                      
                    <PagerTemplate>
                      </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                               <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-top: -34px;margin-right: 11%;">                                    
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
