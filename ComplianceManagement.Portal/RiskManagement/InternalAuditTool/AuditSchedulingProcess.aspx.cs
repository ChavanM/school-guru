﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditSchedulingProcess : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            int BranchId = -1;
            if (!IsPostBack)
            {
                BindSchedulingType();
                BindFinancialYear();
                BindFinancialYearFilter();
                BindLegalEntityDataPop();

                BindMainGrid();
                bindPageNumber();
                BindLegalEntityData();
                BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditScheduling.PageIndex = chkSelectedPage - 1;

            grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindMainGrid();
        }

        public void BindProcessList(int BranchID)
        {
            var details = UserManagementRisk.FillProcessList(Portal.Common.AuthenticationHelper.CustomerID, BranchID);
            ddlProcessList.DataTextField = "Name";
            ddlProcessList.DataValueField = "Id";
            ddlProcessList.Items.Clear();
            ddlProcessList.DataSource = details;
            ddlProcessList.DataBind();

            ddlProcessListMain.DataTextField = "Name";
            ddlProcessListMain.DataValueField = "Id";
            ddlProcessListMain.Items.Clear();
            ddlProcessListMain.DataSource = details;
            ddlProcessListMain.DataBind();
        }
        public void BindVerticalIDPOPup(int CustomerID)
        {
            try
            {
                //var details = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                var details = UserManagementRisk.FillAllVerticalForSchedule(CustomerID);
                ddlVerticalListPopup.DataTextField = "VerticalName";
                ddlVerticalListPopup.DataValueField = "VerticalsId";
                ddlVerticalListPopup.Items.Clear();
                ddlVerticalListPopup.DataSource = details;
                ddlVerticalListPopup.DataBind();
                ddlVerticalListPopup.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindVerticalID(int CustomerID)
        {
            if (CustomerID != null)
            {
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                //ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(Brachid);
                ddlVerticalID.DataSource = UserManagementRisk.FillAllVerticalForSchedule(CustomerID);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }

        public void BindFinancialYearFilter()
        {
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            cleardatasource();
            BindSchedulingType();
        }
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMainGrid();
        }


        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                int Branchid = -1;
                int verticalId = -1;
                ViewState["CustomerBranchId"] = Branchid;
                ViewState["VerticalId"] = verticalId;

                if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                {
                    if (ddlLegalEntityPop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                {
                    if (ddlSubEntity1Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                {
                    if (ddlSubEntity2Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                {
                    if (ddlSubEntity3Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                {
                    if (ddlSubEntity4Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        verticalId = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                    {
                        if (ddlVerticalListPopup.SelectedValue != "-1")
                        {
                            verticalId = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                        }
                    }
                }

                List<long> ProcessList = new List<long>();
                ProcessList.Clear();
                if (ddlProcessList.Items.Count > 0)
                {
                    for (int i = 0; i < ddlProcessList.Items.Count; i++)
                    {
                        if (ddlProcessList.Items[i].Selected)
                        {
                            ProcessList.Add(Convert.ToInt32(ddlProcessList.Items[i].Value));
                        }
                    }
                }

                ViewState["CustomerBranchId"] = Branchid;
                ViewState["VerticalId"] = verticalId;

                if (verticalId != -1)
                {
                    if (ProcessList.Count > 0)
                    {
                        if (flag == "A")
                        {
                            #region Annualy
                            cleardatasource();
                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;

                                    List<SP_AnnualyReport_ScheduleFirst_Result> r = new List<SP_AnnualyReport_ScheduleFirst_Result>();
                                    r = GetSPAnnualyDisplay(Branchid, f1, verticalId, "A");

                                    if (r.Count > 0)
                                    {
                                        r = r.OrderBy(entry => entry.ProcessName).ToList();
                                        r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                    }

                                    grdAnnually.DataSource = r;/*ProcessManagement.GetSPAnnualyDisplay(Branchid, f1, verticalId);*/
                                    grdAnnually.DataBind();

                                    DataTable dt = new DataTable();
                                    dt = (grdAnnually.DataSource as List<SP_AnnualyReport_ScheduleFirst_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("ProcessName");
                                    dt1.Columns.Add("SubProcessName");
                                    dt1.Columns.Add("Annualy1");
                                    dt1.Columns.Add("ProcessID", typeof(long));
                                    dt1.Columns.Add("SubProcessID", typeof(long));
                                    //dt1.Columns.Add("AuditID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["ProcessName"] = "";
                                    dr1["SubProcessName"] = "";
                                    dr1["Annualy1"] = f1;
                                    dr1["ProcessID"] = 0;
                                    dr1["SubProcessID"] = 0;
                                    //dr1["AuditID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdAnnually.DataSource = dt1;
                                    grdAnnually.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (flag == "H")
                        {
                            #region Half Yearly
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;

                                    List<SP_HalfYearlyReport_ScheduleFirst_Result> r = new List<SP_HalfYearlyReport_ScheduleFirst_Result>();
                                    r = GetSPHalfYearlyDisplay(Branchid, f1, verticalId, "H");

                                    if (r.Count > 0)
                                    {
                                        r = r.OrderBy(entry => entry.ProcessName).ToList();
                                        r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                    }

                                    grdHalfYearly.DataSource = r; /* ProcessManagement.GetSPHalfYearlyDisplay(Branchid, f1, verticalId);*/
                                    grdHalfYearly.DataBind();

                                    DataTable dt = new DataTable();
                                    dt = (grdHalfYearly.DataSource as List<SP_HalfYearlyReport_ScheduleFirst_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;

                                    dt1.Columns.Add("ProcessName");
                                    dt1.Columns.Add("SubProcessName");
                                    dt1.Columns.Add("Halfyearly1");
                                    dt1.Columns.Add("Halfyearly2");
                                    dt1.Columns.Add("ProcessID", typeof(long));
                                    dt1.Columns.Add("SubProcessID", typeof(long));
                                    // dt1.Columns.Add("AuditID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["ProcessName"] = "";
                                    dr1["SubProcessName"] = "";
                                    dr1["Halfyearly1"] = "";
                                    dr1["Halfyearly2"] = f1;
                                    dr1["ProcessID"] = 0;
                                    dr1["SubProcessID"] = 0;
                                    //dr1["AuditID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdHalfYearly.DataSource = dt1;
                                    grdHalfYearly.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (flag == "Q")
                        {
                            #region Quarterly
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;

                                    List<SP_QuarterlyReport_ScheduleFirst_Result> r = new List<SP_QuarterlyReport_ScheduleFirst_Result>();
                                    r = GetSPQuarterwiseDisplay(Branchid, f1, verticalId, "Q");

                                    if (r.Count > 0)
                                    {
                                        r = r.OrderBy(entry => entry.ProcessName).ToList();
                                        r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                    }

                                    grdQuarterly.DataSource = r;/*ProcessManagement.GetSPQuarterwiseDisplay(Branchid, f1, verticalId);*/
                                    grdQuarterly.DataBind();

                                    DataTable dt = new DataTable();
                                    dt = (grdQuarterly.DataSource as List<SP_QuarterlyReport_ScheduleFirst_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;

                                    dt1.Columns.Add("ProcessName");
                                    dt1.Columns.Add("SubProcessName");
                                    dt1.Columns.Add("Quarter1");
                                    dt1.Columns.Add("Quarter2");
                                    dt1.Columns.Add("Quarter3");
                                    dt1.Columns.Add("Quarter4");
                                    dt1.Columns.Add("ProcessID", typeof(long));
                                    dt1.Columns.Add("SubProcessID", typeof(long));
                                    // dt1.Columns.Add("AuditID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["ProcessName"] = "";
                                    dr1["SubProcessName"] = "";
                                    dr1["Quarter1"] = "";
                                    dr1["Quarter2"] = "";
                                    dr1["Quarter3"] = "";
                                    dr1["Quarter4"] = f1;
                                    dr1["ProcessID"] = 0;
                                    dr1["SubProcessID"] = 0;
                                    //dr1["AuditID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdQuarterly.DataSource = dt1;
                                    grdQuarterly.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (flag == "M")
                        {
                            #region Monthly
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;

                                    List<SP_MonthlyReport_ScheduleFirst_Result> r = new List<SP_MonthlyReport_ScheduleFirst_Result>();
                                    r = GetSPMonthlyDisplay(Branchid, f1, verticalId, "M");

                                    if (r.Count > 0)
                                    {
                                        r = r.OrderBy(entry => entry.ProcessName).ToList();
                                        r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                    }

                                    grdMonthly.DataSource = r; /*ProcessManagement.GetSPMonthlyDisplay(Branchid, f1, verticalId);*/
                                    grdMonthly.DataBind();

                                    DataTable dt = new DataTable();
                                    dt = (grdMonthly.DataSource as List<SP_MonthlyReport_ScheduleFirst_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;

                                    dt1.Columns.Add("ProcessName");
                                    dt1.Columns.Add("SubProcessName");
                                    dt1.Columns.Add("Monthly1");
                                    dt1.Columns.Add("Monthly2");
                                    dt1.Columns.Add("Monthly3");
                                    dt1.Columns.Add("Monthly4");
                                    dt1.Columns.Add("Monthly5");
                                    dt1.Columns.Add("Monthly6");
                                    dt1.Columns.Add("Monthly7");
                                    dt1.Columns.Add("Monthly8");
                                    dt1.Columns.Add("Monthly9");
                                    dt1.Columns.Add("Monthly10");
                                    dt1.Columns.Add("Monthly11");
                                    dt1.Columns.Add("Monthly12");
                                    dt1.Columns.Add("ProcessID", typeof(long));
                                    dt1.Columns.Add("SubProcessID", typeof(long));
                                    //dt1.Columns.Add("AuditID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["ProcessName"] = "";
                                    dr1["SubProcessName"] = "";
                                    dr1["Monthly1"] = "";
                                    dr1["Monthly2"] = "";
                                    dr1["Monthly3"] = "";
                                    dr1["Monthly4"] = "";
                                    dr1["Monthly5"] = "";
                                    dr1["Monthly6"] = "";
                                    dr1["Monthly7"] = "";
                                    dr1["Monthly8"] = "";
                                    dr1["Monthly9"] = "";
                                    dr1["Monthly10"] = "";
                                    dr1["Monthly11"] = "";
                                    dr1["Monthly12"] = f1;
                                    dr1["ProcessID"] = 0;
                                    dr1["SubProcessID"] = 0;
                                    //dr1["AuditID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdMonthly.DataSource = dt1;
                                    grdMonthly.DataBind();
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            if (count == 1)
                            {
                                #region Phase1
                                cleardatasource();

                                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                {
                                    if (ddlFinancialYear.SelectedValue != "-1")
                                    {
                                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                                        string[] a = financialyear.Split('-');
                                        string aaa = a[0];
                                        string bbb = a[1];
                                        string f1 = aaa + "-" + bbb;

                                        List<SP_Phase1Report_ScheduleFirst_Result> r = new List<SP_Phase1Report_ScheduleFirst_Result>();
                                        r = GetSP_Phase1Display(Branchid, f1, verticalId, "P");

                                        if (r.Count > 0)
                                        {
                                            r = r.OrderBy(entry => entry.ProcessName).ToList();
                                            r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                        }

                                        grdphase1.DataSource = r; /*ProcessManagement.GetSP_Phase1Display(Branchid, f1, verticalId);*/
                                        grdphase1.DataBind();

                                        DataTable dt = new DataTable();
                                        dt = (grdphase1.DataSource as List<SP_Phase1Report_ScheduleFirst_Result>).ToDataTable();
                                        DataTable dt1 = new DataTable();
                                        dt1.Clear();
                                        DataRow dr1 = null;

                                        dt1.Columns.Add("ProcessName");
                                        dt1.Columns.Add("SubProcessName");
                                        dt1.Columns.Add("Phase1");
                                        dt1.Columns.Add("ProcessID", typeof(long));
                                        dt1.Columns.Add("SubProcessID", typeof(long));
                                        // dt1.Columns.Add("AuditID", typeof(long));
                                        dr1 = dt1.NewRow();

                                        dr1["ProcessName"] = "Process";
                                        dr1["SubProcessName"] = "SubProcessName";
                                        dr1["Phase1"] = f1;
                                        dr1["ProcessID"] = 0;
                                        dr1["SubProcessID"] = 0;
                                        // dr1["AuditID"] = 0;
                                        dt1.Rows.Add(dr1);
                                        dt1.Merge(dt);

                                        grdphase1.DataSource = dt1;
                                        grdphase1.DataBind();
                                    }
                                }
                                #endregion
                            }
                            else if (count == 2)
                            {
                                #region Phase2
                                cleardatasource();

                                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                {
                                    if (ddlFinancialYear.SelectedValue != "-1")
                                    {
                                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                                        string[] a = financialyear.Split('-');
                                        string aaa = a[0];
                                        string bbb = a[1];
                                        string f1 = aaa + "-" + bbb;

                                        List<SP_Phase2Report_ScheduleFirst_Result> r = new List<SP_Phase2Report_ScheduleFirst_Result>();
                                        r = GetSP_Phase2Display(Branchid, f1, verticalId, "P");

                                        if (r.Count > 0)
                                        {
                                            r = r.OrderBy(entry => entry.ProcessName).ToList();
                                            r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                        }

                                        grdphase2.DataSource = r;/*ProcessManagement.GetSP_Phase2Display(Branchid, f1, verticalId);*/
                                        grdphase2.DataBind();

                                        DataTable dt = new DataTable();
                                        dt = (grdphase2.DataSource as List<SP_Phase2Report_ScheduleFirst_Result>).ToDataTable();
                                        DataTable dt1 = new DataTable();
                                        dt1.Clear();
                                        DataRow dr1 = null;

                                        dt1.Columns.Add("ProcessName");
                                        dt1.Columns.Add("SubProcessName");
                                        dt1.Columns.Add("Phase1");
                                        dt1.Columns.Add("Phase2");
                                        dt1.Columns.Add("ProcessID", typeof(long));
                                        dt1.Columns.Add("SubProcessID", typeof(long));
                                        //dt1.Columns.Add("AuditID", typeof(long));
                                        dr1 = dt1.NewRow();

                                        dr1["ProcessName"] = "";
                                        dr1["SubProcessName"] = "";
                                        dr1["Phase1"] = "";
                                        dr1["Phase2"] = f1;
                                        dr1["ProcessID"] = 0;
                                        dr1["SubProcessID"] = 0;
                                        //dr1["AuditID"] = 0;
                                        dt1.Rows.Add(dr1);
                                        dt1.Merge(dt);

                                        grdphase2.DataSource = dt1;
                                        grdphase2.DataBind();
                                    }
                                }
                                #endregion
                            }
                            else if (count == 3)
                            {
                                #region Phase3
                                cleardatasource();

                                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                {
                                    if (ddlFinancialYear.SelectedValue != "-1")
                                    {
                                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                                        string[] a = financialyear.Split('-');
                                        string aaa = a[0];
                                        string bbb = a[1];
                                        string f1 = aaa + "-" + bbb;

                                        List<SP_Phase3Report_ScheduleFirst_Result> r = new List<SP_Phase3Report_ScheduleFirst_Result>();
                                        r = GetSP_Phase3Display(Branchid, f1, verticalId, "P");

                                        if (r.Count > 0)
                                        {
                                            r = r.OrderBy(entry => entry.ProcessName).ToList();
                                            r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                        }

                                        grdphase3.DataSource = r; /*ProcessManagement.GetSP_Phase3Display(Branchid, f1, verticalId);*/
                                        grdphase3.DataBind();

                                        DataTable dt = new DataTable();
                                        dt = (grdphase3.DataSource as List<SP_Phase3Report_ScheduleFirst_Result>).ToDataTable();
                                        DataTable dt1 = new DataTable();
                                        dt1.Clear();
                                        DataRow dr1 = null;
                                        dt1.Columns.Add("ProcessName");
                                        dt1.Columns.Add("SubProcessName");
                                        dt1.Columns.Add("Phase1");
                                        dt1.Columns.Add("Phase2");
                                        dt1.Columns.Add("Phase3");
                                        dt1.Columns.Add("ProcessID", typeof(long));
                                        dt1.Columns.Add("SubProcessID", typeof(long));
                                        // dt1.Columns.Add("AuditID", typeof(long));
                                        dr1 = dt1.NewRow();

                                        dr1["ProcessName"] = "";
                                        dr1["SubProcessName"] = "";
                                        dr1["Phase1"] = "";
                                        dr1["Phase2"] = "";
                                        dr1["Phase3"] = f1;
                                        dr1["ProcessID"] = 0;
                                        dr1["SubProcessID"] = 0;
                                        //dr1["AuditID"] = 0;
                                        dt1.Rows.Add(dr1);
                                        dt1.Merge(dt);
                                        grdphase3.DataSource = dt1;
                                        grdphase3.DataBind();
                                    }
                                }
                                #endregion
                            }
                            else if (count == 4)
                            {
                                #region Phase4
                                cleardatasource();

                                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                {
                                    if (ddlFinancialYear.SelectedValue != "-1")
                                    {
                                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                                        string[] a = financialyear.Split('-');
                                        string aaa = a[0];
                                        string bbb = a[1];
                                        string f1 = aaa + "-" + bbb;
                                        List<SP_Phase4Report_ScheduleFirst_Result> r = new List<SP_Phase4Report_ScheduleFirst_Result>();
                                        r = GetSP_Phase4Display(Branchid, f1, verticalId, "P");
                                        if (r.Count > 0)
                                        {
                                            r = r.OrderBy(entry => entry.ProcessName).ToList();
                                            r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                        }

                                        grdphase4.DataSource = r;/*ProcessManagement.GetSP_Phase4Display(Branchid, f1, verticalId);*/
                                        grdphase4.DataBind();

                                        DataTable dt = new DataTable();
                                        dt = (grdphase4.DataSource as List<SP_Phase4Report_ScheduleFirst_Result>).ToDataTable();
                                        DataTable dt1 = new DataTable();
                                        dt1.Clear();
                                        DataRow dr1 = null;
                                        dt1.Columns.Add("ProcessName");
                                        dt1.Columns.Add("SubProcessName");
                                        dt1.Columns.Add("Phase1");
                                        dt1.Columns.Add("Phase2");
                                        dt1.Columns.Add("Phase3");
                                        dt1.Columns.Add("Phase4");
                                        dt1.Columns.Add("ProcessID", typeof(long));
                                        dt1.Columns.Add("SubProcessID", typeof(long));
                                        //dt1.Columns.Add("AuditID", typeof(long));
                                        dr1 = dt1.NewRow();

                                        dr1["ProcessName"] = "";
                                        dr1["SubProcessName"] = "";
                                        dr1["Phase1"] = "";
                                        dr1["Phase2"] = "";
                                        dr1["Phase3"] = "";
                                        dr1["Phase4"] = f1;
                                        dr1["ProcessID"] = 0;
                                        dr1["SubProcessID"] = 0;
                                        //dr1["AuditID"] = 0;
                                        dt1.Rows.Add(dr1);
                                        dt1.Merge(dt);

                                        grdphase4.DataSource = dt1;
                                        grdphase4.DataBind();
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Phase5
                                cleardatasource();

                                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                {
                                    if (ddlFinancialYear.SelectedValue != "-1")
                                    {
                                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                                        string[] a = financialyear.Split('-');
                                        string aaa = a[0];
                                        string bbb = a[1];
                                        string f1 = aaa + "-" + bbb;

                                        List<SP_Phase5Report_ScheduleFirst_Result> r = new List<SP_Phase5Report_ScheduleFirst_Result>();
                                        r = GetSP_Phase5Display(Branchid, f1, verticalId, "P");

                                        if (r.Count > 0)
                                        {
                                            r = r.OrderBy(entry => entry.ProcessName).ToList();
                                            r = r.Where(entry => ProcessList.Contains(entry.ProcessID)).ToList();
                                        }

                                        grdphase5.DataSource = r;/*ProcessManagement.GetSP_Phase5Display(Branchid, f1, verticalId);*/
                                        grdphase5.DataBind();

                                        DataTable dt = new DataTable();
                                        dt = (grdphase5.DataSource as List<SP_Phase5Report_ScheduleFirst_Result>).ToDataTable();
                                        DataTable dt1 = new DataTable();
                                        dt1.Clear();
                                        DataRow dr1 = null;
                                        dt1.Columns.Add("ProcessName");
                                        dt1.Columns.Add("SubProcessName");
                                        dt1.Columns.Add("Phase1");
                                        dt1.Columns.Add("Phase2");
                                        dt1.Columns.Add("Phase3");
                                        dt1.Columns.Add("Phase4");
                                        dt1.Columns.Add("Phase5");
                                        dt1.Columns.Add("ProcessID", typeof(long));
                                        dt1.Columns.Add("SubProcessID", typeof(long));
                                        //dt1.Columns.Add("AuditID", typeof(long));
                                        dr1 = dt1.NewRow();

                                        dr1["ProcessName"] = "";
                                        dr1["SubProcessName"] = "";
                                        dr1["Phase1"] = "";
                                        dr1["Phase2"] = "";
                                        dr1["Phase3"] = "";
                                        dr1["Phase4"] = "";
                                        dr1["Phase5"] = f1;
                                        dr1["ProcessID"] = 0;
                                        dr1["SubProcessID"] = 0;
                                        // dr1["AuditID"] = 0;
                                        dt1.Rows.Add(dr1);
                                        dt1.Merge(dt);

                                        grdphase5.DataSource = dt1;
                                        grdphase5.DataBind();
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Vertical";
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
        }
        public void BindSchedulingType()
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.Items.Clear();
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingType();
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        public void EnableDisable(int noofphases)
        {
            if (ddlFinancialYear.SelectedItem.Text != "Financial Year")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    pnlAnnually.Visible = true;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;

                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = true;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = true;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = true;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    if (noofphases == 1)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = true;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 2)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = true;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 3)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = true;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 4)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = true;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 5)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = true;
                        Divnophase.Visible = true;
                    }
                }
                else
                {
                    Divnophase.Visible = false;
                }
            }
        }

        public string ShowVerticalName(int customerid, int verticalid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.CustomerID == customerid
                             && row.ID == verticalid
                             select row.VerticalName).FirstOrDefault();

                processnonprocess = query;
            }
            return processnonprocess;
        }

        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                if (query != null)
                {
                    processnonprocess = (query.Name).Trim(',');
                }
            }
            return processnonprocess;
        }
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        public string ShowProcessName(long Processid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false
                             && row.Id == Processid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    processnonprocess = query.Name;
                }
            }
            return processnonprocess.Trim(',');
        }

        public void clearselection()
        {
            if (ddlVerticalListPopup.Items.Count > 0)
            {
                ddlVerticalListPopup.ClearSelection();
            }
            if (ddlFinancialYear.Items.Count > 0)
            {
                ddlFinancialYear.ClearSelection();
            }
            if (ddlSchedulingType.Items.Count > 0)
            {
                ddlSchedulingType.ClearSelection();
            }
        }
        public void cleardatasource()
        {
            grdAnnually.DataSource = null;
            grdAnnually.DataBind();
            grdHalfYearly.DataSource = null;
            grdHalfYearly.DataBind();
            grdQuarterly.DataSource = null;
            grdQuarterly.DataBind();
            grdMonthly.DataSource = null;
            grdMonthly.DataBind();
            grdphase1.DataSource = null;
            grdphase1.DataBind();
            grdphase2.DataSource = null;
            grdphase2.DataBind();
            grdphase3.DataSource = null;
            grdphase3.DataBind();
            grdphase4.DataSource = null;
            grdphase4.DataBind();
            grdphase5.DataSource = null;
            grdphase5.DataBind();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
            {
                if (ddlSchedulingType.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (ddlFinancialYear.SelectedValue != "-1")
                        {
                            if (ddlSchedulingType.SelectedItem.Text == "Annually")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("A", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("H", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("Q", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("M", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                            {
                                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                                {
                                    Divnophase.Visible = true;
                                }
                                else
                                {
                                    Divnophase.Visible = false;
                                }
                                cleardatasource();
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Financial Year";
                        }
                    }
                }
                else
                {

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Scheduling Type";
                    cleardatasource();
                }
            }
        }
        protected void txtNoOfPhases_TextChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                {
                    EnableDisable(Convert.ToInt32(txtNoOfPhases.Text));
                    BindAuditSchedule("P", Convert.ToInt32(txtNoOfPhases.Text));

                }
                else
                {
                    cleardatasource();
                }
            }
        }
        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                ddlFinancialYear.SelectedValue = "-1";
                ddlSchedulingType.SelectedValue = "-1";
                ddlLegalEntityPop.SelectedValue = "-1";
                ddlSubEntity1Pop.SelectedValue = "-1";
                ddlSubEntity2Pop.SelectedValue = "-1";
                ddlSubEntity3Pop.SelectedValue = "-1";
                ddlSubEntity4Pop.SelectedValue = "-1";
                txtStartDatePop.Text = string.Empty;
                txtEndDatePop.Text = string.Empty;
                ddlFinancialYear.SelectedValue = "-1";
                ddlSchedulingType.SelectedValue = "-1";
                cleardatasource();
                EnableDisable(0);
                Divnophase.Visible = false;
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;
                txtNoOfPhases.Text = string.Empty;
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again1.";
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindMainGrid()
        {
            string FinancialYear = string.Empty;
            int CustomerBranchId = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            int VerticalID = -1;
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }
            List<int> ProcessList = new List<int>();
            ProcessList.Clear();
            if (ddlProcessList.Items.Count > 0)
            {
                for (int i = 0; i < ddlProcessList.Items.Count; i++)
                {
                    if (ddlProcessList.Items[i].Selected)
                    {
                        ProcessList.Add(Convert.ToInt32(ddlProcessList.Items[i].Value));
                    }
                }
            }
            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
            var Branchlistloop = Branchlist.ToList();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditSchedulingNewbind> query = new List<AuditSchedulingNewbind>();
                //if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                //{
                query = (from row in entities.InternalAuditSchedulings
                         join MCB in entities.mst_CustomerBranch
                         on row.CustomerBranchId equals MCB.ID
                         join row1 in entities.mst_Vertical
                         on row.VerticalID equals row1.ID
                         where row.TermStatus == true
                         && row.IsDeleted == false && MCB.CustomerID == customerID
                         select new AuditSchedulingNewbind()
                         {
                             CustomerId = MCB.CustomerID,
                             FinancialYear = row.FinancialYear,
                             CustomerBranchId = row.CustomerBranchId,
                             CustomerBranchName = MCB.Name,
                             ISAHQMP = row.ISAHQMP,
                             TermName = row.TermName,
                             PhaseCount = row.PhaseCount,
                             VerticalName = row1.VerticalName,
                             VerticalID = row1.ID,
                             AuditID = row.AuditID,
                         }).Distinct().ToList();
                //}
                //else
                //{
                //    query = (from row in entities.InternalAuditSchedulings
                //             join MCB in entities.mst_CustomerBranch
                //             on row.CustomerBranchId equals MCB.ID
                //             join row1 in entities.mst_Vertical
                //             on row.VerticalID equals row1.ID
                //             join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                //             on row.Process equals EAAR.ProcessId
                //             where row.TermStatus == true
                //             && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                //             && row.IsDeleted == false && MCB.CustomerID == customerID
                //             && EAAR.ISACTIVE == true && EAAR.BranchID == row.CustomerBranchId
                //             select new AuditSchedulingNewbind()
                //             {
                //                 CustomerId = MCB.CustomerID,
                //                 FinancialYear = row.FinancialYear,
                //                 CustomerBranchId = row.CustomerBranchId,
                //                 CustomerBranchName = MCB.Name,
                //                 ISAHQMP = row.ISAHQMP,
                //                 TermName = row.TermName,
                //                 PhaseCount = row.PhaseCount,
                //                 VerticalName = row1.VerticalName,
                //                 VerticalID = row1.ID,
                //                 AuditID = row.AuditID,
                //             }).Distinct().ToList();
                //}



                if (Branchlist.Count > 0)
                {
                    query = query.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                }
                if (FinancialYear != "")
                {
                    query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                if (VerticalID != -1)
                {
                    query = query.Where(entry => entry.VerticalID == VerticalID).ToList();
                }

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.CustomerBranchName).ThenBy(entry => entry.VerticalName).ToList();

                grdAuditScheduling.DataSource = null;
                grdAuditScheduling.DataBind();

                if (query != null)
                {
                    grdAuditScheduling.DataSource = query;
                    Session["TotalRows"] = query.Count;
                    grdAuditScheduling.DataBind();
                    Branchlistloop.Clear();
                    Branchlist.Clear();

                }
            }
        }

        public void BindGridForNextpreviouse()
        {
            BindMainGrid();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            BindMainGrid();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        int verticalId = -1;
                        int customerID = -1;
                        customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                        long UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                        int CustomerBranchId = -1;
                        if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                        {
                            if (ddlLegalEntityPop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                        {
                            if (ddlSubEntity1Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                        {
                            if (ddlSubEntity2Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                        {
                            if (ddlSubEntity3Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                        {
                            if (ddlSubEntity4Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                            }
                        }
                        if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                        {
                            int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                            if (vid != -1)
                            {
                                verticalId = vid;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                            {
                                if (ddlVerticalListPopup.SelectedValue != "-1")
                                {
                                    verticalId = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                                }
                            }
                        }
                        List<int> ProcessList = new List<int>();
                        ProcessList.Clear();
                        if (ddlProcessList.Items.Count > 0)
                        {
                            for (int i = 0; i < ddlProcessList.Items.Count; i++)
                            {
                                if (ddlProcessList.Items[i].Selected)
                                {
                                    ProcessList.Add(Convert.ToInt32(ddlProcessList.Items[i].Value));
                                }
                            }
                        }

                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] fsplit = financialyear.Split('-');
                        string fyear = fsplit[0];
                        string syear = fsplit[1];
                        string f1 = fyear + "-" + syear;
                        int ScheduleTypeValue = -1;
                        if (verticalId != -1)
                        {
                            if (ProcessList.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(txtStartDatePop.Text.Trim()))
                                {
                                    if (!string.IsNullOrEmpty(txtEndDatePop.Text.Trim()))
                                    {
                                        DateTime Startdate = new DateTime();
                                        DateTime EndDate = new DateTime();
                                        Startdate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                        EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                        if (Startdate > EndDate)
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "StartDate should not be greather than EndDate.";
                                            return;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
                                {
                                    if (ddlSchedulingType.SelectedValue != "-1")
                                    {
                                        ScheduleTypeValue = Convert.ToInt32(ddlSchedulingType.SelectedValue);
                                    }
                                }

                                if (ScheduleTypeValue != -1)
                                {
                                    #region Annually Save
                                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                                    {
                                        string Period = "Annually";
                                        bool CheckAnnually = false;
                                        bool result = false;
                                        int InternalAuditScheduleOnID = 0;
                                        long AnnuallyAuditID = 0;
                                        for (int chkauidcreate = 0; chkauidcreate < grdAnnually.Rows.Count; chkauidcreate++)
                                        {
                                            GridViewRow row = grdAnnually.Rows[chkauidcreate];
                                            CheckBox bf = (CheckBox)row.FindControl("chkAnnualy1");
                                            if (bf.Checked && bf.Enabled)
                                            {
                                                CheckAnnually = true;
                                            }
                                        }
                                        if (CheckAnnually)
                                        {
                                            AnnuallyAuditID = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, Period, customerID);
                                        }
                                        AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                        int processid = -1;
                                        int subprocessid = -1;
                                        int OldProcessID = -1;
                                        for (int i = 0; i < grdAnnually.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdAnnually.Rows[i];
                                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                            Label lblSubProcessId = (Label)row.FindControl("lblSubProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                if (!string.IsNullOrEmpty(lblSubProcessId.Text))
                                                {
                                                    subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                                }
                                                CheckBox bf = (CheckBox)row.FindControl("chkAnnualy1");
                                                if (bf.Checked && bf.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AnnuallyAuditID, subprocessid, "Annually"))
                                                    {
                                                        if (processid != OldProcessID)
                                                        {
                                                            OldProcessID = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Annually";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "A";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AnnuallyAuditID;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AnnuallyAuditID != 0)
                                                            {
                                                                InternalAuditScheduleOnID = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AnnuallyAuditID;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnID;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AnnuallyAuditID != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AnnuallyAuditID;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnID;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AnnuallyAuditID != 0 && subprocessid != 0 && InternalAuditScheduleOnID != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (result)
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        }
                                    }
                                    #endregion

                                    #region Half Yearly Save
                                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                                    {
                                        // List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                        int processid = -1;
                                        int OldProcessIDHalfyearly1 = -1;
                                        int OldProcessIDHalfyearly2 = -1;
                                        bool result = false;
                                        int InternalAuditScheduleOnIDHalfyearly1 = 0;
                                        int InternalAuditScheduleOnIDHalfyearly2 = 0;
                                        int subprocessid = -1;
                                        bool CheckAprSep = false;
                                        bool CheckOctMar = false;
                                        long AuditIDAprSep = 0;
                                        long AuditIDOctMar = 0;
                                        for (int i = 0; i < grdHalfYearly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdHalfYearly.Rows[i];
                                            CheckBox bf = (CheckBox)row.FindControl("chkHalfyearly1");
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkHalfyearly2");
                                            if (bf.Checked && bf.Enabled)
                                            {
                                                CheckAprSep = true;
                                            }
                                            if (bf1.Checked && bf1.Enabled)
                                            {
                                                CheckOctMar = true;
                                            }
                                        }
                                        if (CheckAprSep)
                                        {
                                            string PeriodAprSep = "Apr-Sep";
                                            AuditIDAprSep = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodAprSep, customerID);
                                        }
                                        if (CheckOctMar)
                                        {
                                            string PeriodOctMar = "Oct-Mar";
                                            AuditIDOctMar = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodOctMar, customerID);
                                        }

                                        for (int i = 0; i < grdHalfYearly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdHalfYearly.Rows[i];
                                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                            Label lblSubProcessId = (Label)row.FindControl("lblSubProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                if (!string.IsNullOrEmpty(lblSubProcessId.Text))
                                                {
                                                    subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                                }
                                                CheckBox bf = (CheckBox)row.FindControl("chkHalfyearly1");
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkHalfyearly2");
                                                //first financial Year
                                                if (bf.Checked && bf.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDAprSep, subprocessid, "Apr-Sep"))
                                                    {
                                                        if (processid != OldProcessIDHalfyearly1)
                                                        {
                                                            OldProcessIDHalfyearly1 = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Apr-Sep";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "H";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDAprSep;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDAprSep != 0)
                                                            {
                                                                InternalAuditScheduleOnIDHalfyearly1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAprSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly1;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAprSep != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAprSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly1;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAprSep != 0 && subprocessid != 0 && InternalAuditScheduleOnIDHalfyearly1 != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                                if (bf1.Checked && bf1.Enabled)
                                                {

                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDOctMar, subprocessid, "Oct-Mar"))
                                                    {
                                                        if (processid != OldProcessIDHalfyearly2)
                                                        {
                                                            OldProcessIDHalfyearly2 = Convert.ToInt32(lblProcessId.Text);
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Oct-Mar";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "H";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDOctMar;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDOctMar != 0)
                                                            {
                                                                InternalAuditScheduleOnIDHalfyearly2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOctMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly2;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOctMar != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOctMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDHalfyearly2;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOctMar != 0 && subprocessid != 0 && InternalAuditScheduleOnIDHalfyearly2 != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }

                                                }
                                            }
                                        }
                                        if (result)
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                        }
                                    }
                                    #endregion

                                    #region Quarterly Save
                                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                                    {
                                        long AuditIDAprJun = 0;
                                        long AuditIDJulSep = 0;
                                        long AuditIDOctDec = 0;
                                        long AuditIDJanMar = 0;
                                        bool CheckAprJun = false;
                                        bool CheckJulSep = false;
                                        bool CheckOctDec = false;
                                        bool CheckJanMar = false;
                                        bool result = false;

                                        int OldProcessIDAprJun = -1;
                                        int InternalAuditScheduleOnIDAprJun = 0;
                                        int OldProcessIDJulSep = -1;
                                        int InternalAuditScheduleOnIDJulSep = 0;
                                        int OldProcessIDOctDec = -1;
                                        int InternalAuditScheduleOnIDOctDec = 0;
                                        int OldProcessIDJanMar = -1;
                                        int InternalAuditScheduleOnIDJanMar = 0;
                                        int subprocessid = -1;
                                        for (int i = 0; i < grdQuarterly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdQuarterly.Rows[i];
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkQuarter1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkQuarter2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkQuarter3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkQuarter4");
                                            if (bf1.Checked && bf1.Enabled)
                                            {
                                                CheckAprJun = true;
                                            }
                                            if (bf2.Checked && bf2.Enabled)
                                            {
                                                CheckJulSep = true;
                                            }
                                            if (bf3.Checked && bf3.Enabled)
                                            {
                                                CheckOctDec = true;
                                            }
                                            if (bf4.Checked && bf4.Enabled)
                                            {
                                                CheckJanMar = true;
                                            }
                                        }
                                        int processid = -1;
                                        if (CheckAprJun)
                                        {
                                            string PeriodAprJun = "Apr-Jun";
                                            AuditIDAprJun = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodAprJun, customerID);
                                        }
                                        if (CheckJulSep)
                                        {
                                            string PeriodJulSep = "Jul-Sep";
                                            AuditIDJulSep = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodJulSep, customerID);
                                        }
                                        if (CheckOctDec)
                                        {
                                            string PeriodOctDec = "Oct-Dec";
                                            AuditIDOctDec = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodOctDec, customerID);
                                        }
                                        if (CheckJanMar)
                                        {
                                            string PeriodJanMar = "Jan-Mar";
                                            AuditIDJanMar = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodJanMar, customerID);
                                        }
                                        AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                        //List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        for (int i = 0; i < grdQuarterly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdQuarterly.Rows[i];
                                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                            Label lblSubProcessId = (Label)row.FindControl("lblSubProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                if (!string.IsNullOrEmpty(lblSubProcessId.Text))
                                                {
                                                    subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                                }
                                                //first financial Year
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkQuarter1");
                                                CheckBox bf2 = (CheckBox)row.FindControl("chkQuarter2");
                                                CheckBox bf3 = (CheckBox)row.FindControl("chkQuarter3");
                                                CheckBox bf4 = (CheckBox)row.FindControl("chkQuarter4");
                                                //first financial Year
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDAprJun, subprocessid, "Apr-Jun"))
                                                    {
                                                        if (processid != OldProcessIDAprJun)
                                                        {
                                                            OldProcessIDAprJun = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Apr-Jun";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "Q";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDAprJun;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDAprJun != 0)
                                                            {
                                                                InternalAuditScheduleOnIDAprJun = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAprJun;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAprJun;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAprJun != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAprJun;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAprJun;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAprJun != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                                if (bf2.Checked && bf2.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDJulSep, subprocessid, "Jul-Sep"))
                                                    {
                                                        if (processid != OldProcessIDJulSep)
                                                        {
                                                            OldProcessIDJulSep = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Jul-Sep";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "Q";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDJulSep;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDJulSep != 0)
                                                            {
                                                                InternalAuditScheduleOnIDJulSep = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJulSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJulSep;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJulSep != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJulSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJulSep;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJulSep != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                                if (bf3.Checked && bf3.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDOctDec, subprocessid, "Oct-Dec"))
                                                    {
                                                        if (processid != OldProcessIDOctDec)
                                                        {
                                                            OldProcessIDOctDec = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Oct-Dec";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "Q";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDOctDec;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDOctDec != 0)
                                                            {
                                                                InternalAuditScheduleOnIDOctDec = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOctDec;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOctDec;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOctDec != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOctDec;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOctDec;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOctDec != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }

                                                }
                                                if (bf4.Checked && bf4.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDJanMar, subprocessid, "Jan-Mar"))
                                                    {
                                                        if (processid != OldProcessIDJanMar)
                                                        {
                                                            OldProcessIDJanMar = Convert.ToInt32(lblProcessId.Text);
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Jan-Mar";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "Q";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDJanMar;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDJanMar != 0)
                                                            {
                                                                InternalAuditScheduleOnIDJanMar = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJanMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJanMar;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJanMar != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJanMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJanMar;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJanMar != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (result)
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                            // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                        }
                                    }
                                    #endregion

                                    #region Monthly Save
                                    else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                                    {
                                        bool CheckAnnually = false;
                                        bool result = false;
                                        bool CheckApr = false;
                                        bool CheckMay = false;
                                        bool CheckJun = false;
                                        bool CheckJul = false;
                                        bool CheckAug = false;
                                        bool CheckSep = false;
                                        bool CheckOct = false;
                                        bool CheckNov = false;
                                        bool CheckDec = false;
                                        bool CheckJan = false;
                                        bool CheckFeb = false;
                                        bool CheckMar = false;

                                        int InternalAuditScheduleOnIDApr = 0;
                                        int InternalAuditScheduleOnIDMay = 0;
                                        int InternalAuditScheduleOnIDJun = 0;
                                        int InternalAuditScheduleOnIDJul = 0;
                                        int InternalAuditScheduleOnIDAug = 0;
                                        int InternalAuditScheduleOnIDSep = 0;
                                        int InternalAuditScheduleOnIDOct = 0;
                                        int InternalAuditScheduleOnIDNov = 0;
                                        int InternalAuditScheduleOnIDDec = 0;
                                        int InternalAuditScheduleOnIDJan = 0;
                                        int InternalAuditScheduleOnIDFeb = 0;
                                        int InternalAuditScheduleOnIDMar = 0;

                                        long AuditIDApr = 0;
                                        long AuditIDMay = 0;
                                        long AuditIDJun = 0;
                                        long AuditIDJul = 0;
                                        long AuditIDAug = 0;
                                        long AuditIDSep = 0;
                                        long AuditIDOct = 0;
                                        long AuditIDNov = 0;
                                        long AuditIDDec = 0;
                                        long AuditIDJan = 0;
                                        long AuditIDFeb = 0;
                                        long AuditIDMar = 0;

                                        int OldProcessIDApr = 0;
                                        int OldProcessIDMay = 0;
                                        int OldProcessIDJun = 0;
                                        int OldProcessIDJul = 0;
                                        int OldProcessIDAug = 0;
                                        int OldProcessIDSep = 0;
                                        int OldProcessIDOct = 0;
                                        int OldProcessIDNov = 0;
                                        int OldProcessIDDec = 0;
                                        int OldProcessIDJan = 0;
                                        int OldProcessIDFeb = 0;
                                        int OldProcessIDMar = 0;

                                        for (int i = 0; i < grdMonthly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdMonthly.Rows[i];
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkMonthly1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkMonthly2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkMonthly3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkMonthly4");
                                            CheckBox bf5 = (CheckBox)row.FindControl("chkMonthly5");
                                            CheckBox bf6 = (CheckBox)row.FindControl("chkMonthly6");
                                            CheckBox bf7 = (CheckBox)row.FindControl("chkMonthly7");
                                            CheckBox bf8 = (CheckBox)row.FindControl("chkMonthly8");
                                            CheckBox bf9 = (CheckBox)row.FindControl("chkMonthly9");
                                            CheckBox bf10 = (CheckBox)row.FindControl("chkMonthly10");
                                            CheckBox bf11 = (CheckBox)row.FindControl("chkMonthly11");
                                            CheckBox bf12 = (CheckBox)row.FindControl("chkMonthly12");
                                            if (bf1.Checked && bf1.Enabled)
                                            {
                                                CheckApr = true;
                                            }
                                            if (bf2.Checked && bf2.Enabled)
                                            {
                                                CheckMay = true;
                                            }
                                            if (bf3.Checked && bf3.Enabled)
                                            {
                                                CheckJun = true;
                                            }
                                            if (bf4.Checked && bf4.Enabled)
                                            {
                                                CheckJul = true;
                                            }
                                            if (bf5.Checked && bf5.Enabled)
                                            {
                                                CheckAug = true;
                                            }
                                            if (bf6.Checked && bf6.Enabled)
                                            {
                                                CheckSep = true;
                                            }
                                            if (bf7.Checked && bf7.Enabled)
                                            {
                                                CheckOct = true;
                                            }
                                            if (bf8.Checked && bf8.Enabled)
                                            {
                                                CheckNov = true;
                                            }
                                            if (bf9.Checked && bf9.Enabled)
                                            {
                                                CheckDec = true;
                                            }
                                            if (bf10.Checked && bf10.Enabled)
                                            {
                                                CheckJan = true;
                                            }
                                            if (bf11.Checked && bf11.Enabled)
                                            {
                                                CheckFeb = true;
                                            }
                                            if (bf12.Checked && bf12.Enabled)
                                            {
                                                CheckMar = true;
                                            }
                                        }
                                        if (CheckApr)
                                        {
                                            string PeriodApr = "Apr";
                                            AuditIDApr = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodApr, customerID);
                                        }
                                        if (CheckMay)
                                        {
                                            string PeriodMay = "May";
                                            AuditIDMay = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodMay, customerID);
                                        }
                                        if (CheckJun)
                                        {
                                            string PeriodJun = "Jun";
                                            AuditIDJun = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodJun, customerID);
                                        }
                                        if (CheckJul)
                                        {
                                            string PeriodJul = "Jul";
                                            AuditIDJul = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodJul, customerID);
                                        }
                                        if (CheckAug)
                                        {
                                            string PeriodAug = "Aug";
                                            AuditIDAug = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodAug, customerID);
                                        }
                                        if (CheckSep)
                                        {
                                            string PeriodSep = "Sep";
                                            AuditIDSep = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodSep, customerID);
                                        }
                                        if (CheckOct)
                                        {
                                            string PeriodOct = "Oct";
                                            AuditIDOct = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodOct, customerID);
                                        }
                                        if (CheckNov)
                                        {
                                            string PeriodNov = "Nov";
                                            AuditIDNov = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodNov, customerID);
                                        }
                                        if (CheckDec)
                                        {
                                            string PeriodDec = "Dec";
                                            AuditIDDec = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodDec, customerID);
                                        }
                                        if (CheckJan)
                                        {
                                            string PeriodJan = "Jan";
                                            AuditIDJan = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodJan, customerID);
                                        }
                                        if (CheckFeb)
                                        {
                                            string PeriodFeb = "Feb";
                                            AuditIDFeb = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodFeb, customerID);
                                        }
                                        if (CheckMar)
                                        {
                                            string PeriodMar = "Mar";
                                            AuditIDMar = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodMar, customerID);
                                        }

                                        int processid = -1;
                                        int subprocessid = -1;

                                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                        for (int i = 0; i < grdMonthly.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdMonthly.Rows[i];
                                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                            Label lblSubProcessId = (Label)row.FindControl("lblSubProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                if (!string.IsNullOrEmpty(lblSubProcessId.Text))
                                                {
                                                    subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                                }
                                                //first financial Year
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkMonthly1");
                                                CheckBox bf2 = (CheckBox)row.FindControl("chkMonthly2");
                                                CheckBox bf3 = (CheckBox)row.FindControl("chkMonthly3");
                                                CheckBox bf4 = (CheckBox)row.FindControl("chkMonthly4");
                                                CheckBox bf5 = (CheckBox)row.FindControl("chkMonthly5");
                                                CheckBox bf6 = (CheckBox)row.FindControl("chkMonthly6");
                                                CheckBox bf7 = (CheckBox)row.FindControl("chkMonthly7");
                                                CheckBox bf8 = (CheckBox)row.FindControl("chkMonthly8");
                                                CheckBox bf9 = (CheckBox)row.FindControl("chkMonthly9");
                                                CheckBox bf10 = (CheckBox)row.FindControl("chkMonthly10");
                                                CheckBox bf11 = (CheckBox)row.FindControl("chkMonthly11");
                                                CheckBox bf12 = (CheckBox)row.FindControl("chkMonthly12");

                                                //first financial Year
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDApr, subprocessid, "Apr"))
                                                    {
                                                        if (processid != OldProcessIDApr)
                                                        {
                                                            OldProcessIDApr = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Apr";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDApr;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDApr != 0)
                                                            {
                                                                InternalAuditScheduleOnIDApr = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDApr;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDApr;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDApr != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDApr;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDApr;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDApr != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf2.Checked && bf2.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDMay, subprocessid, "May"))
                                                    {
                                                        if (processid != OldProcessIDMay)
                                                        {
                                                            OldProcessIDMay = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "May";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDMay;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDMay != 0)
                                                            {
                                                                InternalAuditScheduleOnIDMay = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDMay;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMay;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDMay != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDMay;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMay;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDMay != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf3.Checked && bf3.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDJun, subprocessid, "Jun"))
                                                    {
                                                        if (processid != OldProcessIDJun)
                                                        {
                                                            OldProcessIDJun = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Jun";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDJun;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDJun != 0)
                                                            {
                                                                InternalAuditScheduleOnIDJun = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJun;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJun;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJun != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJun;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJun;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJun != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf4.Checked && bf4.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDJul, subprocessid, "Jul"))
                                                    {
                                                        if (processid != OldProcessIDJul)
                                                        {
                                                            OldProcessIDJul = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Jul";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDJul;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDJul != 0)
                                                            {
                                                                InternalAuditScheduleOnIDJul = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJul;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJul;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJul != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJul;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJul;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJul != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }

                                                }

                                                if (bf5.Checked && bf5.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDAug, subprocessid, "Aug"))
                                                    {
                                                        if (processid != OldProcessIDAug)
                                                        {
                                                            OldProcessIDAug = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Aug";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDAug;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDAug != 0)
                                                            {
                                                                InternalAuditScheduleOnIDAug = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAug;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAug;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAug != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDAug;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDAug;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDAug != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf6.Checked && bf6.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDSep, subprocessid, "Sep"))
                                                    {
                                                        if (processid != OldProcessIDSep)
                                                        {
                                                            OldProcessIDSep = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Sep";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDSep;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDSep != 0)
                                                            {
                                                                InternalAuditScheduleOnIDSep = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDSep;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDSep != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDSep;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDSep;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDSep != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf7.Checked && bf7.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDOct, subprocessid, "Oct"))
                                                    {
                                                        if (processid != OldProcessIDOct)
                                                        {
                                                            OldProcessIDOct = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Oct";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDOct;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDOct != 0)
                                                            {
                                                                InternalAuditScheduleOnIDOct = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOct;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOct;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOct != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDOct;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDOct;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDOct != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf8.Checked && bf8.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDNov, subprocessid, "Nov"))
                                                    {
                                                        if (processid != OldProcessIDNov)
                                                        {
                                                            OldProcessIDNov = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Nov";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDNov;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDNov != 0)
                                                            {
                                                                InternalAuditScheduleOnIDNov = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDNov;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDNov;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDNov != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDNov;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDNov;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDNov != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf9.Checked && bf9.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDDec, subprocessid, "Dec"))
                                                    {
                                                        if (processid != OldProcessIDDec)
                                                        {
                                                            OldProcessIDDec = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Dec";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDDec;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDDec != 0)
                                                            {
                                                                InternalAuditScheduleOnIDDec = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDDec;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDDec;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDDec != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDDec;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDDec;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDDec != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf10.Checked && bf10.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDJan, subprocessid, "Jan"))
                                                    {
                                                        if (processid != OldProcessIDJan)
                                                        {
                                                            OldProcessIDJan = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Jan";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDJan;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDJan != 0)
                                                            {
                                                                InternalAuditScheduleOnIDJan = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJan;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJan;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJan != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDJan;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDJan;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDJan != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }

                                                if (bf11.Checked && bf11.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDFeb, subprocessid, "Feb"))
                                                    {
                                                        if (processid != OldProcessIDFeb)
                                                        {
                                                            OldProcessIDFeb = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Feb";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDFeb;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDFeb != 0)
                                                            {
                                                                InternalAuditScheduleOnIDFeb = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDFeb;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDFeb;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDFeb != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDFeb;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDFeb;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDFeb != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }

                                                }

                                                if (bf12.Checked && bf12.Enabled)
                                                {
                                                    if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDMar, subprocessid, "Mar"))
                                                    {
                                                        if (processid != OldProcessIDMar)
                                                        {
                                                            OldProcessIDMar = Convert.ToInt32(lblProcessId.Text);

                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Mar";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "M";
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                            Internalauditscheduling.AuditID = AuditIDMar;
                                                            Internalauditscheduling.Createdby = UserID;
                                                            Internalauditscheduling.CreatedOn = DateTime.Now;
                                                            if (processid != 0 && AuditIDMar != 0)
                                                            {
                                                                InternalAuditScheduleOnIDMar = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                            }
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMar;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDMar != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                        else
                                                        {
                                                            AuditScheduling_SubProcessMapping.Process = processid;
                                                            AuditScheduling_SubProcessMapping.AuditID = AuditIDMar;
                                                            AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                            AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                            AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                            AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDMar;
                                                            AuditScheduling_SubProcessMapping.IsActive = true;
                                                            AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                            if (processid != 0 && AuditIDMar != 0 && subprocessid != 0)
                                                            {
                                                                UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                            }
                                                            result = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (result)
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                        }
                                    }
                                    #endregion

                                    #region Phase Save
                                    else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                                    {
                                        AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                        int processid = -1;
                                        int subprocessid = -1;
                                        int noofphases = -1;

                                        if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                                        {
                                            noofphases = (Convert.ToInt32(txtNoOfPhases.Text));
                                        }
                                        #region Phase 1
                                        if (noofphases == 1)
                                        {
                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            string Period = "Phase1";
                                            bool CheckPhase1 = false;
                                            long Phase1AuditID = 0;
                                            bool result = false;
                                            int InternalAuditScheduleOnIDPhase1 = 0;
                                            int OldProcessIDPhase1 = -1;
                                            for (int chkauidcreate = 0; chkauidcreate < grdphase1.Rows.Count; chkauidcreate++)
                                            {
                                                GridViewRow row = grdphase1.Rows[chkauidcreate];
                                                CheckBox bf = (CheckBox)row.FindControl("chkPhase1");
                                                if (bf.Checked && bf.Enabled)
                                                {
                                                    CheckPhase1 = true;
                                                }
                                            }
                                            if (CheckPhase1)
                                            {
                                                Phase1AuditID = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, Period, customerID);
                                            }

                                            for (int i = 0; i < grdphase1.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase1.Rows[i];
                                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                                Label lblSubProcessId = (Label)row.FindControl("lblSubProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    if (!string.IsNullOrEmpty(lblSubProcessId.Text))
                                                    {
                                                        subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                                    }
                                                    CheckBox bf = (CheckBox)row.FindControl("chkPhase1");

                                                    if (bf.Checked && bf.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, Phase1AuditID, subprocessid, "Phase1", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase1)
                                                            {
                                                                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase1";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = Phase1AuditID;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && Phase1AuditID != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = Phase1AuditID;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && Phase1AuditID != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = Phase1AuditID;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && Phase1AuditID != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (result)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                            }
                                        }
                                        #endregion
                                        #region Phase 2
                                        else if (noofphases == 2)
                                        {
                                            bool CheckPhase1 = false;
                                            bool CheckPhase2 = false;
                                            long AuditIDPhase1 = 0;
                                            long AuditIDPhase2 = 0;
                                            bool result = false;
                                            int InternalAuditScheduleOnIDPhase1 = 0;
                                            int OldProcessIDPhase1 = -1;
                                            int InternalAuditScheduleOnIDPhase2 = 0;
                                            int OldProcessIDPhase2 = -1;
                                            for (int i = 0; i < grdphase2.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase2.Rows[i];
                                                CheckBox bf = (CheckBox)row.FindControl("chkPhase1");
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkPhase2");
                                                if (bf.Checked && bf.Enabled)
                                                {
                                                    CheckPhase1 = true;
                                                }
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    CheckPhase2 = true;
                                                }
                                            }
                                            if (CheckPhase1)
                                            {
                                                string PeriodPhase1 = "Phase1";
                                                AuditIDPhase1 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase1, customerID);
                                            }
                                            if (CheckPhase2)
                                            {
                                                string PeriodPhase2 = "Phase2";
                                                AuditIDPhase2 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase2, customerID);
                                            }
                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase2.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase2.Rows[i];
                                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                                Label lblSubProcessID = (Label)row.FindControl("lblSubProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    if (!string.IsNullOrEmpty(lblSubProcessID.Text))
                                                    {
                                                        subprocessid = Convert.ToInt32(lblSubProcessID.Text);
                                                    }
                                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                    if (bf1.Checked && bf1.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase1, subprocessid, "Phase1", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase1)
                                                            {
                                                                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase1";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase1;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase1 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                    if (bf2.Checked && bf2.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase2, subprocessid, "Phase2", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase2)
                                                            {
                                                                OldProcessIDPhase2 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase2";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase2;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase2 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (result)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                            }
                                        }
                                        #endregion
                                        #region Phase 3
                                        else if (noofphases == 3)
                                        {
                                            bool CheckPhase1 = false;
                                            bool CheckPhase2 = false;
                                            bool CheckPhase3 = false;
                                            long AuditIDPhase1 = 0;
                                            long AuditIDPhase2 = 0;
                                            long AuditIDPhase3 = 0;
                                            bool result = false;
                                            int InternalAuditScheduleOnIDPhase1 = 0;
                                            int OldProcessIDPhase1 = -1;
                                            int InternalAuditScheduleOnIDPhase2 = 0;
                                            int OldProcessIDPhase2 = -1;
                                            int InternalAuditScheduleOnIDPhase3 = 0;
                                            int OldProcessIDPhase3 = -1;
                                            for (int i = 0; i < grdphase3.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase3.Rows[i];
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    CheckPhase1 = true;
                                                }
                                                if (bf2.Checked && bf2.Enabled)
                                                {
                                                    CheckPhase2 = true;
                                                }
                                                if (bf3.Checked && bf3.Enabled)
                                                {
                                                    CheckPhase3 = true;
                                                }
                                            }
                                            if (CheckPhase1)
                                            {
                                                string PeriodPhase1 = "Phase1";
                                                AuditIDPhase1 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase1, customerID);
                                            }
                                            if (CheckPhase2)
                                            {
                                                string PeriodPhase2 = "Phase2";
                                                AuditIDPhase2 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase2, customerID);
                                            }
                                            if (CheckPhase3)
                                            {
                                                string PeriodPhase3 = "Phase3";
                                                AuditIDPhase3 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase3, customerID);
                                            }
                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase3.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase3.Rows[i];
                                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                                Label lblSubProcessID = (Label)row.FindControl("lblSubProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    if (!string.IsNullOrEmpty(lblSubProcessID.Text))
                                                    {
                                                        subprocessid = Convert.ToInt32(lblSubProcessID.Text);
                                                    }
                                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                    if (bf1.Checked && bf1.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase1, subprocessid, "Phase1", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase1)
                                                            {
                                                                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase1";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase1;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase1 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf2.Checked && bf2.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase2, subprocessid, "Phase2", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase2)
                                                            {
                                                                OldProcessIDPhase2 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase2";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase2;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase2 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf3.Checked && bf3.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase3, subprocessid, "Phase3", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase3)
                                                            {
                                                                OldProcessIDPhase3 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase3";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase3;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase3 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase3 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (result)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                            }
                                        }
                                        #endregion
                                        #region Phase 4
                                        else if (noofphases == 4)
                                        {
                                            bool CheckPhase1 = false;
                                            bool CheckPhase2 = false;
                                            bool CheckPhase3 = false;
                                            bool CheckPhase4 = false;
                                            long AuditIDPhase1 = 0;
                                            long AuditIDPhase2 = 0;
                                            long AuditIDPhase3 = 0;
                                            long AuditIDPhase4 = 0;
                                            bool result = false;
                                            int InternalAuditScheduleOnIDPhase1 = 0;
                                            int OldProcessIDPhase1 = -1;
                                            int InternalAuditScheduleOnIDPhase2 = 0;
                                            int OldProcessIDPhase2 = -1;
                                            int InternalAuditScheduleOnIDPhase3 = 0;
                                            int OldProcessIDPhase3 = -1;
                                            int InternalAuditScheduleOnIDPhase4 = 0;
                                            int OldProcessIDPhase4 = -1;
                                            for (int i = 0; i < grdphase4.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase4.Rows[i];
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    CheckPhase1 = true;
                                                }
                                                if (bf2.Checked && bf2.Enabled)
                                                {
                                                    CheckPhase2 = true;
                                                }
                                                if (bf3.Checked && bf3.Enabled)
                                                {
                                                    CheckPhase3 = true;
                                                }
                                                if (bf4.Checked && bf4.Enabled)
                                                {
                                                    CheckPhase4 = true;
                                                }
                                            }
                                            if (CheckPhase1)
                                            {
                                                string PeriodPhase1 = "Phase1";
                                                AuditIDPhase1 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase1, customerID);
                                            }
                                            if (CheckPhase2)
                                            {
                                                string PeriodPhase2 = "Phase2";
                                                AuditIDPhase2 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase2, customerID);
                                            }
                                            if (CheckPhase3)
                                            {
                                                string PeriodPhase3 = "Phase3";
                                                AuditIDPhase3 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase3, customerID);
                                            }
                                            if (CheckPhase4)
                                            {
                                                string PeriodPhase4 = "Phase4";
                                                AuditIDPhase4 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase4, customerID);
                                            }

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase4.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase4.Rows[i];
                                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                                Label lblSubProcessID = (Label)row.FindControl("lblSubProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    if (!string.IsNullOrEmpty(lblSubProcessID.Text))
                                                    {
                                                        subprocessid = Convert.ToInt32(lblSubProcessID.Text);
                                                    }
                                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                                    if (bf1.Checked && bf1.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase1, subprocessid, "Phase1", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase1)
                                                            {
                                                                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase1";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase1;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase1 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf2.Checked && bf2.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase2, subprocessid, "Phase2", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase2)
                                                            {
                                                                OldProcessIDPhase2 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase2";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase2;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase2 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf3.Checked && bf3.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase3, subprocessid, "Phase3", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase3)
                                                            {
                                                                OldProcessIDPhase3 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase3";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase3;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase3 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase3 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf4.Checked && bf4.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase4, subprocessid, "Phase4", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase4)
                                                            {
                                                                OldProcessIDPhase4 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase4";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase4;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase4 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase4 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase4;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase4;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase4 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase4;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase4;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase4 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (result)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                            }
                                        }
                                        #endregion
                                        #region Phase 5
                                        else if (noofphases == 5)
                                        {
                                            bool CheckPhase1 = false;
                                            bool CheckPhase2 = false;
                                            bool CheckPhase3 = false;
                                            bool CheckPhase4 = false;
                                            bool CheckPhase5 = false;
                                            long AuditIDPhase1 = 0;
                                            long AuditIDPhase2 = 0;
                                            long AuditIDPhase3 = 0;
                                            long AuditIDPhase4 = 0;
                                            long AuditIDPhase5 = 0;
                                            bool result = false;
                                            int InternalAuditScheduleOnIDPhase1 = 0;
                                            int OldProcessIDPhase1 = -1;
                                            int InternalAuditScheduleOnIDPhase2 = 0;
                                            int OldProcessIDPhase2 = -1;
                                            int InternalAuditScheduleOnIDPhase3 = 0;
                                            int OldProcessIDPhase3 = -1;
                                            int InternalAuditScheduleOnIDPhase4 = 0;
                                            int OldProcessIDPhase4 = -1;
                                            int InternalAuditScheduleOnIDPhase5 = 0;
                                            int OldProcessIDPhase5 = -1;
                                            for (int i = 0; i < grdphase5.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase5.Rows[i];
                                                CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                                CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                                if (bf1.Checked && bf1.Enabled)
                                                {
                                                    CheckPhase1 = true;
                                                }
                                                if (bf2.Checked && bf2.Enabled)
                                                {
                                                    CheckPhase2 = true;
                                                }
                                                if (bf3.Checked && bf3.Enabled)
                                                {
                                                    CheckPhase3 = true;
                                                }
                                                if (bf4.Checked && bf4.Enabled)
                                                {
                                                    CheckPhase4 = true;
                                                }
                                                if (bf5.Checked && bf5.Enabled)
                                                {
                                                    CheckPhase5 = true;
                                                }
                                            }
                                            if (CheckPhase1)
                                            {
                                                string PeriodPhase1 = "Phase1";
                                                AuditIDPhase1 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase1, customerID);
                                            }
                                            if (CheckPhase2)
                                            {
                                                string PeriodPhase2 = "Phase2";
                                                AuditIDPhase2 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase2, customerID);
                                            }
                                            if (CheckPhase3)
                                            {
                                                string PeriodPhase3 = "Phase3";
                                                AuditIDPhase3 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase3, customerID);
                                            }
                                            if (CheckPhase4)
                                            {
                                                string PeriodPhase4 = "Phase4";
                                                AuditIDPhase4 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase4, customerID);
                                            }
                                            if (CheckPhase5)
                                            {
                                                string PeriodPhase5 = "Phase5";
                                                AuditIDPhase5 = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, financialyear, PeriodPhase5, customerID);
                                            }
                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase5.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase5.Rows[i];
                                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                                Label lblSubProcessID = (Label)row.FindControl("lblSubProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    if (!string.IsNullOrEmpty(lblSubProcessID.Text))
                                                    {
                                                        subprocessid = Convert.ToInt32(lblSubProcessID.Text);
                                                    }
                                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                                    CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                                    if (bf1.Checked && bf1.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase1, subprocessid, "Phase1", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase1)
                                                            {
                                                                OldProcessIDPhase1 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase1";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase1;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase1 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase1 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase1;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase1;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase1 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf2.Checked && bf2.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase2, subprocessid, "Phase2", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase2)
                                                            {
                                                                OldProcessIDPhase2 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase2";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase2;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase2 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase2 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase2;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase2;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase2 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf3.Checked && bf3.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase3, subprocessid, "Phase3", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase3)
                                                            {
                                                                OldProcessIDPhase3 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase3";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase3;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase3 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase3 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase3;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase3;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase3 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf4.Checked && bf4.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase4, subprocessid, "Phase4", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase4)
                                                            {
                                                                OldProcessIDPhase4 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase4";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase4;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase4 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase4 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase4;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase4;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase4 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase4;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase4;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase4 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }

                                                    if (bf5.Checked && bf5.Enabled)
                                                    {
                                                        if (UserManagementRisk.IsExistAuditScheduling_ScheduleFirst(CustomerBranchId, verticalId, processid, f1, AuditIDPhase5, subprocessid, "Phase5", noofphases))
                                                        {
                                                            if (processid != OldProcessIDPhase5)
                                                            {
                                                                OldProcessIDPhase5 = Convert.ToInt32(lblProcessId.Text);

                                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                                Internalauditscheduling.FinancialYear = f1;
                                                                Internalauditscheduling.TermName = "Phase5";
                                                                Internalauditscheduling.TermStatus = true;
                                                                Internalauditscheduling.Process = processid;
                                                                Internalauditscheduling.ISAHQMP = "P";
                                                                Internalauditscheduling.PhaseCount = noofphases;
                                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                                Internalauditscheduling.AuditID = AuditIDPhase5;
                                                                Internalauditscheduling.Createdby = UserID;
                                                                Internalauditscheduling.CreatedOn = DateTime.Now;
                                                                if (processid != 0 && AuditIDPhase5 != 0)
                                                                {
                                                                    InternalAuditScheduleOnIDPhase5 = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                                                }
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase5;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase5;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase5 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                            else
                                                            {
                                                                AuditScheduling_SubProcessMapping.Process = processid;
                                                                AuditScheduling_SubProcessMapping.AuditID = AuditIDPhase5;
                                                                AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                                AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                                AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                                AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnIDPhase5;
                                                                AuditScheduling_SubProcessMapping.IsActive = true;
                                                                AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                                if (processid != 0 && AuditIDPhase5 != 0 && subprocessid != 0)
                                                                {
                                                                    UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                                                }
                                                                result = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (result)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                            }
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Scheduling Type";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Process";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Vertical";
                        }
                        BindMainGrid();
                        cleardatasource();
                        BindGridpopData();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Financial Year";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                for (int i = 0; i < grdAuditScheduleStartEndDate.Rows.Count; i++)
                {
                    GridViewRow row = grdAuditScheduleStartEndDate.Rows[i];
                    Label lblID = (Label)row.FindControl("lblID");
                    Label lblAuditID = (Label)row.FindControl("lblAuditID");
                    TextBox txtStartDate = (TextBox)row.FindControl("txtExpectedStartDate");
                    TextBox txtEndDate = (TextBox)row.FindControl("txtExpectedEndDate");

                    if (txtStartDate.Text != "" && txtEndDate.Text != "")
                    {
                        if (!string.IsNullOrEmpty(txtStartDate.Text.Trim()))
                        {
                            if (!string.IsNullOrEmpty(txtEndDate.Text.Trim()))
                            {
                                DateTime Startdate = new DateTime();
                                DateTime EndDate = new DateTime();
                                Startdate = Convert.ToDateTime(txtStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                EndDate = Convert.ToDateTime(txtEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                if (Startdate > EndDate)
                                {
                                    cvDuplicateEntry23.IsValid = false;
                                    cvDuplicateEntry23.ErrorMessage = "StartDate should not be greather than EndDate.";
                                    return;
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(lblID.Text))
                        {
                            DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                            Internalauditscheduling.Id = Convert.ToInt32(lblID.Text);
                            Internalauditscheduling.StartDate = GetDate(a.ToString("dd/MM/yyyy"));
                            Internalauditscheduling.EndDate = GetDate(b.ToString("dd/MM/yyyy"));

                            int ScheduleID = Convert.ToInt32(lblID.Text);
                            var CheckLogData = ProcessManagement.GetScheduleonDataByID(ScheduleID);
                            //if (!string.IsNullOrEmpty(Convert.ToString(CheckLogData.StartDate)) && !string.IsNullOrEmpty(Convert.ToString(CheckLogData.EndDate)))
                            //{ }
                            if (a != CheckLogData.StartDate || b != CheckLogData.EndDate)
                            {
                                InternalAuditSchedulingReportLog Objlog = new InternalAuditSchedulingReportLog
                                {
                                    Process = CheckLogData.Process,
                                    CustomerBranchId = CheckLogData.CustomerBranchId,
                                    ISAHQMP = CheckLogData.ISAHQMP,
                                    FinancialYear = CheckLogData.FinancialYear,
                                    TermName = CheckLogData.TermName,
                                    TermStatus = CheckLogData.TermStatus,
                                    PhaseCount = CheckLogData.PhaseCount,
                                    StartDate = a,
                                    EndDate = b,
                                    IsDeleted = CheckLogData.IsDeleted,
                                    VerticalID = CheckLogData.VerticalID,
                                    StartDateOld = CheckLogData.StartDate,
                                    EndDateOld = CheckLogData.EndDate,
                                    AuditId = Convert.ToInt64(lblAuditID.Text),
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                ProcessManagement.SaveInternalAuditSchedulingReportLogData(Objlog);
                            }
                            ProcessManagement.UpdateInternalAuditorScheduling(Internalauditscheduling);
                            cvDuplicateEntry23.IsValid = false;
                            cvDuplicateEntry23.ErrorMessage = "Data updated successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry23.IsValid = false;
                cvDuplicateEntry23.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool IsValidDateFormat(string dateFormat)
        {
            try
            {
                String dts = DateTime.Now.ToString(dateFormat);
                DateTime.ParseExact(dts, dateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        #region Annually       
        protected void grdAnnually_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkAnnualy1");
                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Annually")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                }
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                //cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell2 = e.Row.Cells[2];
                                //cell2.ColumnSpan = 1;
                                cell2.Text = "SubProcess";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                // cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Annually12 = new CheckBox();
                                chk_Annually12.Text = "Annually";
                                chk_Annually12.ID = "Annually1_Id";
                                chk_Annually12.Attributes.Add("OnClick", "return func();");
                                cell1.Controls.Add(chk_Annually12);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdAnnually_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAnnually.PageIndex = e.NewPageIndex;
            BindAuditSchedule("A", 0);
        }
        #endregion
        #region Haly Yearly
        protected void grdHalfYearly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        GridViewRow gvRow = e.Row;
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            //cell1.HorizontalAlign = HorizontalAlign.Center;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.CssClass = "alignCenter";

                            TableCell otherCell1 = e.Row.Cells[4];
                            otherCell1.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkHalfyearly1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkHalfyearly2");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Apr-Sep")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                    else if (termName == "Oct-Mar")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell3 = e.Row.Cells[2];
                                cell3.ColumnSpan = 1;
                                cell3.Text = "SubProcess";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                //cell1.CssClass = "alignCenter";
                                CheckBox chk_Half1 = new CheckBox();
                                chk_Half1.Text = "Apr-Sep";
                                chk_Half1.ID = "Half1_Id";
                                chk_Half1.Attributes.Add("OnClick", "return funHalf1();");
                                cell1.Controls.Add(chk_Half1);

                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                //cell2.CssClass = "alignCenter";
                                CheckBox chk_Half2 = new CheckBox();
                                chk_Half2.Text = "Oct-Mar";
                                chk_Half2.ID = "Half2_Id";
                                chk_Half2.Attributes.Add("OnClick", "return funHalf2();");
                                cell2.Controls.Add(chk_Half2);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        protected void grdHalfYearly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHalfYearly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("H", 0);
        }
        #endregion
        #region Quarterly
        protected void grdQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.CssClass = "alignCenter";

                            TableCell otherCell5 = e.Row.Cells[4];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[5];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[6];
                            otherCell7.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkQuarter1");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkQuarter2");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkQuarter3");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkQuarter4");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Apr-Jun")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                    else if (termName == "Jul-Sep")
                                    {
                                        bf2.Checked = true;
                                        bf2.Enabled = false;
                                    }
                                    else if (termName == "Oct-Dec")
                                    {
                                        bf3.Checked = true;
                                        bf3.Enabled = false;
                                    }
                                    else if (termName == "Jan-Mar")
                                    {
                                        bf4.Checked = true;
                                        bf4.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell5 = e.Row.Cells[2];
                                cell5.ColumnSpan = 1;
                                cell5.Text = "SubProcess";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly1 = new CheckBox();
                                chk_Quarterly1.Text = "Apr-Jun";
                                chk_Quarterly1.ID = "Quarterly1_Id";
                                chk_Quarterly1.Attributes.Add("OnClick", "return funcQuarterly1();");
                                cell1.Controls.Add(chk_Quarterly1);

                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly2 = new CheckBox();
                                chk_Quarterly2.Text = "Jul-Sep";
                                chk_Quarterly2.ID = "Quarterly2_Id";
                                chk_Quarterly2.Attributes.Add("OnClick", "return funcQuarterly2();");
                                cell2.Controls.Add(chk_Quarterly2);

                                TableCell cell3 = e.Row.Cells[5];
                                cell3.ColumnSpan = 1;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly3 = new CheckBox();
                                chk_Quarterly3.Text = "Oct-Dec";
                                chk_Quarterly3.ID = "Quarterly3_Id";
                                chk_Quarterly3.Attributes.Add("OnClick", "return funcQuarterly3();");
                                cell3.Controls.Add(chk_Quarterly3);


                                TableCell cell4 = e.Row.Cells[6];
                                cell4.ColumnSpan = 1;
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly4 = new CheckBox();
                                chk_Quarterly4.Text = "Jan-Mar";
                                chk_Quarterly4.ID = "Quarterly4_Id";
                                chk_Quarterly4.Attributes.Add("OnClick", "return funcQuarterly4();");
                                cell4.Controls.Add(chk_Quarterly4);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdQuarterly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdQuarterly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("Q", 0);

        }
        #endregion
        #region Monthly
        protected void grdMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {

                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 12;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.CssClass = "alignCenter";
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;

                            TableCell otherCell5 = e.Row.Cells[4];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[5];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[6];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[7];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[8];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[9];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[10];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[11];
                            otherCell12.Visible = false;

                            TableCell otherCell13 = e.Row.Cells[12];
                            otherCell13.Visible = false;

                            TableCell otherCell14 = e.Row.Cells[13];
                            otherCell14.Visible = false;

                            TableCell otherCell15 = e.Row.Cells[14];
                            otherCell15.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            //first financial Year
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkMonthly1");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkMonthly2");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkMonthly3");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkMonthly4");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkMonthly5");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkMonthly6");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkMonthly7");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkMonthly8");
                            CheckBox bf9 = (CheckBox)e.Row.FindControl("chkMonthly9");
                            CheckBox bf10 = (CheckBox)e.Row.FindControl("chkMonthly10");
                            CheckBox bf11 = (CheckBox)e.Row.FindControl("chkMonthly11");
                            CheckBox bf12 = (CheckBox)e.Row.FindControl("chkMonthly12");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Apr")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                    else if (termName == "May")
                                    {
                                        bf2.Checked = true;
                                        bf2.Enabled = false;
                                    }
                                    else if (termName == "Jun")
                                    {
                                        bf3.Checked = true;
                                        bf3.Enabled = false;
                                    }
                                    else if (termName == "Jul")
                                    {
                                        bf4.Checked = true;
                                        bf4.Enabled = false;
                                    }
                                    else if (termName == "Aug")
                                    {
                                        bf5.Checked = true;
                                        bf5.Enabled = false;
                                    }
                                    else if (termName == "Sep")
                                    {
                                        bf6.Checked = true;
                                        bf6.Enabled = false;
                                    }
                                    else if (termName == "Oct")
                                    {
                                        bf7.Checked = true;
                                        bf7.Enabled = false;
                                    }
                                    else if (termName == "Nov")
                                    {
                                        bf8.Checked = true;
                                        bf8.Enabled = false;
                                    }
                                    else if (termName == "Dec")
                                    {
                                        bf9.Checked = true;
                                        bf9.Enabled = false;
                                    }
                                    else if (termName == "Jan")
                                    {
                                        bf10.Checked = true;
                                        bf10.Enabled = false;
                                    }
                                    else if (termName == "Feb")
                                    {
                                        bf11.Checked = true;
                                        bf11.Enabled = false;
                                    }
                                    else if (termName == "Mar")
                                    {
                                        bf12.Checked = true;
                                        bf12.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;
                                bf12.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cells1 = e.Row.Cells[2];
                                cells1.ColumnSpan = 1;
                                cells1.Text = "SubProcess";
                                cells1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cells1.BorderColor = Color.White;
                                cells1.ForeColor = Color.White;
                                cells1.Font.Bold = true;
                                cells1.HorizontalAlign = HorizontalAlign.Left;


                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly1 = new CheckBox();
                                chk_grdMonthly1.Text = "Apr";
                                chk_grdMonthly1.ID = "grdMonthly1_Id";
                                chk_grdMonthly1.Attributes.Add("OnClick", "return funMonthly1();");
                                cell1.Controls.Add(chk_grdMonthly1);

                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly2 = new CheckBox();
                                chk_grdMonthly2.Text = "May";
                                chk_grdMonthly2.ID = "grdMonthly2_Id";
                                chk_grdMonthly2.Attributes.Add("OnClick", "return funMonthly2();");
                                cell2.Controls.Add(chk_grdMonthly2);

                                TableCell cell3 = e.Row.Cells[5];
                                cell3.ColumnSpan = 1;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly3 = new CheckBox();
                                chk_grdMonthly3.Text = "Jun";
                                chk_grdMonthly3.ID = "grdMonthly3_Id";
                                chk_grdMonthly3.Attributes.Add("OnClick", "return funMonthly3();");
                                cell3.Controls.Add(chk_grdMonthly3);


                                TableCell cell4 = e.Row.Cells[6];
                                cell4.ColumnSpan = 1;
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly4 = new CheckBox();
                                chk_grdMonthly4.Text = "Jul";
                                chk_grdMonthly4.ID = "grdMonthly4_Id";
                                chk_grdMonthly4.Attributes.Add("OnClick", "return funMonthly4();");
                                cell4.Controls.Add(chk_grdMonthly4);


                                TableCell cell5 = e.Row.Cells[7];
                                cell5.ColumnSpan = 1;
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly5 = new CheckBox();
                                chk_grdMonthly5.Text = "Aug";
                                chk_grdMonthly5.ID = "grdMonthly5_Id";
                                chk_grdMonthly5.Attributes.Add("OnClick", "return funMonthly5();");
                                cell5.Controls.Add(chk_grdMonthly5);


                                TableCell cell6 = e.Row.Cells[8];
                                cell6.ColumnSpan = 1;
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly6 = new CheckBox();
                                chk_grdMonthly6.Text = "Sep";
                                chk_grdMonthly6.ID = "grdMonthly6_Id";
                                chk_grdMonthly6.Attributes.Add("OnClick", "return funMonthly6();");
                                cell6.Controls.Add(chk_grdMonthly6);

                                TableCell cell7 = e.Row.Cells[9];
                                cell7.ColumnSpan = 1;
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly7 = new CheckBox();
                                chk_grdMonthly7.Text = "Oct";
                                chk_grdMonthly7.ID = "grdMonthly7_Id";
                                chk_grdMonthly7.Attributes.Add("OnClick", "return funMonthly7();");
                                cell7.Controls.Add(chk_grdMonthly7);

                                TableCell cell8 = e.Row.Cells[10];
                                cell8.ColumnSpan = 1;
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly8 = new CheckBox();
                                chk_grdMonthly8.Text = "Nov";
                                chk_grdMonthly8.ID = "grdMonthly8_Id";
                                chk_grdMonthly8.Attributes.Add("OnClick", "return funMonthly8();");
                                cell8.Controls.Add(chk_grdMonthly8);

                                TableCell cell9 = e.Row.Cells[11];
                                cell9.ColumnSpan = 1;
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly9 = new CheckBox();
                                chk_grdMonthly9.Text = "Dec";
                                chk_grdMonthly9.ID = "grdMonthly9_Id";
                                chk_grdMonthly9.Attributes.Add("OnClick", "return funMonthly9();");
                                cell9.Controls.Add(chk_grdMonthly9);

                                TableCell cell10 = e.Row.Cells[12];
                                cell10.ColumnSpan = 1;
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly10 = new CheckBox();
                                chk_grdMonthly10.Text = "Jan";
                                chk_grdMonthly10.ID = "grdMonthly10_Id";
                                chk_grdMonthly10.Attributes.Add("OnClick", "return funMonthly10();");
                                cell10.Controls.Add(chk_grdMonthly10);

                                TableCell cell11 = e.Row.Cells[13];
                                cell11.ColumnSpan = 1;
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly11 = new CheckBox();
                                chk_grdMonthly11.Text = "Feb";
                                chk_grdMonthly11.ID = "grdMonthly11_Id";
                                chk_grdMonthly11.Attributes.Add("OnClick", "return funMonthly11();");
                                cell11.Controls.Add(chk_grdMonthly11);

                                TableCell cell12 = e.Row.Cells[14];
                                cell12.ColumnSpan = 1;
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly12 = new CheckBox();
                                chk_grdMonthly12.Text = "Mar";
                                chk_grdMonthly12.ID = "grdMonthly12_Id";
                                chk_grdMonthly12.Attributes.Add("OnClick", "return funMonthly12();");
                                cell12.Controls.Add(chk_grdMonthly12);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        protected void grdMonthly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMonthly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("M", 0);

        }
        #endregion
        #region Phase1
        protected void grdphase1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Phase1")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell2 = e.Row.Cells[2];
                                cell2.ColumnSpan = 1;
                                cell2.Text = "SubProcess";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase1 = new CheckBox();
                                chk_Phase1.Text = "Phase1";
                                chk_Phase1.ID = "Phase1_Phase1_Id";
                                chk_Phase1.Attributes.Add("OnClick", "return funcphase1();");
                                cell1.Controls.Add(chk_Phase1);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase1.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 1);

        }
        #endregion
        #region Phase 2
        protected void grdphase2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.CssClass = "alignCenter";
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;

                            TableCell otherCell1 = e.Row.Cells[4];
                            otherCell1.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Phase1")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                    else if (termName == "Phase2")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell3 = e.Row.Cells[2];
                                cell3.ColumnSpan = 1;
                                cell3.Text = "SubProcess";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2_Phase1 = new CheckBox();
                                chk_Phase2_Phase1.Text = "Phase1";
                                chk_Phase2_Phase1.ID = "Phase2_Phase1_Id";
                                chk_Phase2_Phase1.Attributes.Add("OnClick", "return funcphase21();");
                                cell1.Controls.Add(chk_Phase2_Phase1);


                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2_Phase2 = new CheckBox();
                                chk_Phase2_Phase2.Text = "Phase2";
                                chk_Phase2_Phase2.ID = "Phase2_Phase2_Id";
                                chk_Phase2_Phase2.Attributes.Add("OnClick", "return funcphase22();");
                                cell2.Controls.Add(chk_Phase2_Phase2);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase2.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 2);
        }
        #endregion
        #region Phase 3
        protected void grdphase3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[2];
                            cell2.ColumnSpan = 1;
                            cell2.Text = "SubProcess";
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 3;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.CssClass = "alignCenter";
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;

                            TableCell otherCell5 = e.Row.Cells[4];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[5];
                            otherCell6.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Phase1")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                    else if (termName == "Phase2")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                    else if (termName == "Phase3")
                                    {
                                        bf2.Checked = true;
                                        bf2.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cells = e.Row.Cells[2];
                                cells.ColumnSpan = 1;
                                cells.Text = "SubProcess";
                                cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cells.BorderColor = Color.White;
                                cells.ForeColor = Color.White;
                                cells.Font.Bold = true;
                                cells.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase1 = new CheckBox();
                                chk_Phase3_Phase1.Text = "Phase1";
                                chk_Phase3_Phase1.ID = "Phase3_Phase1_Id";
                                chk_Phase3_Phase1.Attributes.Add("OnClick", "return funcphase31();");
                                cell1.Controls.Add(chk_Phase3_Phase1);

                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase2 = new CheckBox();
                                chk_Phase3_Phase2.Text = "Phase2";
                                chk_Phase3_Phase2.ID = "Phase3_Phase2_Id";
                                chk_Phase3_Phase2.Attributes.Add("OnClick", "return funcphase32();");
                                cell2.Controls.Add(chk_Phase3_Phase2);


                                TableCell cell3 = e.Row.Cells[5];
                                cell3.ColumnSpan = 1;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase3 = new CheckBox();
                                chk_Phase3_Phase3.Text = "Phase3";
                                chk_Phase3_Phase3.ID = "Phase3_Phase3_Id";
                                chk_Phase3_Phase3.Attributes.Add("OnClick", "return funcphase33();");
                                cell3.Controls.Add(chk_Phase3_Phase3);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase3.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 3);
        }
        #endregion
        #region Phase 4
        protected void grdphase4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cells = e.Row.Cells[2];
                            cells.ColumnSpan = 1;
                            cells.Text = "SubProcess";
                            cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cells.BorderColor = Color.White;
                            cells.ForeColor = Color.White;
                            cells.Font.Bold = true;
                            cells.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.CssClass = "alignCenter";
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;

                            TableCell otherCell5 = e.Row.Cells[4];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[5];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[6];
                            otherCell7.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Phase1")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                    else if (termName == "Phase2")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                    else if (termName == "Phase3")
                                    {
                                        bf2.Checked = true;
                                        bf2.Enabled = false;
                                    }
                                    else if (termName == "Phase4")
                                    {
                                        bf3.Checked = true;
                                        bf3.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cells = e.Row.Cells[2];
                                cells.ColumnSpan = 1;
                                cells.Text = "SubProcess";
                                cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cells.BorderColor = Color.White;
                                cells.ForeColor = Color.White;
                                cells.Font.Bold = true;
                                cells.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase1 = new CheckBox();
                                chk_Phase4_Phase1.Text = "Phase1";
                                chk_Phase4_Phase1.ID = "Phase4_Phase1_Id";
                                chk_Phase4_Phase1.Attributes.Add("OnClick", "return funcphase41();");
                                cell1.Controls.Add(chk_Phase4_Phase1);


                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase2 = new CheckBox();
                                chk_Phase4_Phase2.Text = "Phase2";
                                chk_Phase4_Phase2.ID = "Phase4_Phase2_Id";
                                chk_Phase4_Phase2.Attributes.Add("OnClick", "return funcphase42();");
                                cell2.Controls.Add(chk_Phase4_Phase2);


                                TableCell cell3 = e.Row.Cells[5];
                                cell3.ColumnSpan = 1;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase3 = new CheckBox();
                                chk_Phase4_Phase3.Text = "Phase3";
                                chk_Phase4_Phase3.ID = "Phase4_Phase3_Id";
                                chk_Phase4_Phase3.Attributes.Add("OnClick", "return funcphase43();");
                                cell3.Controls.Add(chk_Phase4_Phase3);


                                TableCell cell4 = e.Row.Cells[6];
                                cell4.ColumnSpan = 1;
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase4 = new CheckBox();
                                chk_Phase4_Phase4.Text = "Phase4";
                                chk_Phase4_Phase4.ID = "Phase4_Phase4_Id";
                                chk_Phase4_Phase4.Attributes.Add("OnClick", "return funcphase44();");
                                cell4.Controls.Add(chk_Phase4_Phase4);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase4.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 4);
        }
        #endregion
        #region Phase 5
        protected void grdphase5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cells = e.Row.Cells[2];
                            cells.ColumnSpan = 1;
                            cells.Text = "SubProcess";
                            cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cells.BorderColor = Color.White;
                            cells.ForeColor = Color.White;
                            cells.Font.Bold = true;
                            cells.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 5;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.CssClass = "alignCenter";
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;

                            TableCell otherCell5 = e.Row.Cells[4];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[5];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[6];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[7];
                            otherCell8.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            //Label lblAuditID = e.Row.FindControl("lblAuditID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkPhase5");

                            List<SchedulingReport_ScheduleFirst_Result> schFirstresult = new List<SchedulingReport_ScheduleFirst_Result>();
                            schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["CustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["VerticalId"]), Convert.ToInt32(lblSubProcessID.Text)).ToList();
                            var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                            string termName = string.Empty;
                            if (remindersummary.Count > 0)
                            {
                                foreach (var row in remindersummary)
                                {
                                    termName = row.TermName;
                                    if (termName == "Phase1")
                                    {
                                        bf.Checked = true;
                                        bf.Enabled = false;
                                    }
                                    else if (termName == "Phase2")
                                    {
                                        bf1.Checked = true;
                                        bf1.Enabled = false;
                                    }
                                    else if (termName == "Phase3")
                                    {
                                        bf2.Checked = true;
                                        bf2.Enabled = false;
                                    }
                                    else if (termName == "Phase4")
                                    {
                                        bf3.Checked = true;
                                        bf3.Enabled = false;
                                    }
                                    else if (termName == "Phase5")
                                    {
                                        bf4.Checked = true;
                                        bf4.Enabled = false;
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cells = e.Row.Cells[2];
                                cells.ColumnSpan = 1;
                                cells.Text = "SubProcess";
                                cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cells.BorderColor = Color.White;
                                cells.ForeColor = Color.White;
                                cells.Font.Bold = true;

                                cells.HorizontalAlign = HorizontalAlign.Left;
                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase1 = new CheckBox();
                                chk_Phase5_Phase1.Text = "Phase1";
                                chk_Phase5_Phase1.ID = "Phase5_Phase1_Id";
                                chk_Phase5_Phase1.Attributes.Add("OnClick", "return funcphase51();");
                                cell1.Controls.Add(chk_Phase5_Phase1);


                                TableCell cell2 = e.Row.Cells[4];
                                cell2.ColumnSpan = 1;
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase2 = new CheckBox();
                                chk_Phase5_Phase2.Text = "Phase2";
                                chk_Phase5_Phase2.ID = "Phase5_Phase2_Id";
                                chk_Phase5_Phase2.Attributes.Add("OnClick", "return funcphase52();");
                                cell2.Controls.Add(chk_Phase5_Phase2);


                                TableCell cell3 = e.Row.Cells[5];
                                cell3.ColumnSpan = 1;
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase3 = new CheckBox();
                                chk_Phase5_Phase3.Text = "Phase3";
                                chk_Phase5_Phase3.ID = "Phase5_Phase3_Id";
                                chk_Phase5_Phase3.Attributes.Add("OnClick", "return funcphase53();");
                                cell3.Controls.Add(chk_Phase5_Phase3);


                                TableCell cell4 = e.Row.Cells[6];
                                cell4.ColumnSpan = 1;
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase4 = new CheckBox();
                                chk_Phase5_Phase4.Text = "Phase4";
                                chk_Phase5_Phase4.ID = "Phase5_Phase4_Id";
                                chk_Phase5_Phase4.Attributes.Add("OnClick", "return funcphase54();");
                                cell4.Controls.Add(chk_Phase5_Phase4);

                                TableCell cell5 = e.Row.Cells[7];
                                cell5.ColumnSpan = 1;
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase5 = new CheckBox();
                                chk_Phase5_Phase5.Text = "Phase5";
                                chk_Phase5_Phase5.ID = "Phase5_Phase5_Id";
                                chk_Phase5_Phase5.Attributes.Add("OnClick", "return funcphase55();");
                                cell5.Controls.Add(chk_Phase5_Phase5);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase5.PageIndex = e.NewPageIndex;
            BindAuditSchedule("P", 5);
        }
        #endregion
        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {

        }
        protected bool ViewSchedule(long? EventID, object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (EventID != null)
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void grdAuditScheduling_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void grdAuditScheduling_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdAuditScheduling_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;
                    int AuditID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[4]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            AuditID = Convert.ToInt32(commandArgs[5]);
                        }

                        OpenScheduleInformation(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid, AuditID);
                    }
                }
                if (e.CommandName.Equals("DELETE_SCHEDULE"))
                {
                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;
                    long AuditID = 0;

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                            lblEDITLocation.Text = ShowCustomerBranchName(CustomerBranchId);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[4]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[5]))
                        {
                            AuditID = Convert.ToInt32(commandArgs[5]);
                        }
                        ActivtInactiveInternalAuditScheduling(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid, AuditID);
                        AuditClosureDelete(isAHQMP, FinancialYear, Termname, CustomerBranchId, Verticalid, AuditID);
                        BindMainGrid();
                    }
                }
                if (e.CommandName.Equals("EDIT_SCHEDULE"))
                {
                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;
                    int PhaseCount = 0;
                    long AuditID = 0;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                            lblEDITFinancialyear.Text = FinancialYear;
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                            lblEDITLocation.Text = ShowCustomerBranchName(CustomerBranchId);
                            lblEDITLocationID.Text = Convert.ToString(CustomerBranchId);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                            lblEDITTermName.Text = Termname;
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            PhaseCount = Convert.ToInt32(commandArgs[4]);
                            lblEDITPhaseCount.Text = Convert.ToString(PhaseCount);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[5]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[5]);
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            lblEDITVerticalID.Text = Convert.ToString(Verticalid);
                            lblVerticalName.Text = ShowVerticalName(customerID, Verticalid);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[6]))
                        {
                            AuditID = Convert.ToInt64(commandArgs[6]);
                        }
                        ViewState["EDITISAHQMP"] = null;
                        ViewState["AuditID"] = null;
                        ViewState["EDITFinancialYear"] = null;
                        ViewState["EDITCustomerBranchId"] = null;
                        ViewState["EDITTermname"] = null;
                        ViewState["EDITVerticalId"] = null;
                        ViewState["EDITISAHQMP"] = isAHQMP;
                        ViewState["EDITFinancialYear"] = FinancialYear;
                        ViewState["EDITCustomerBranchId"] = CustomerBranchId;
                        ViewState["EDITTermname"] = Termname;
                        ViewState["EDITVerticalId"] = Verticalid;
                        ViewState["AuditID"] = AuditID;
                        if (isAHQMP == "A")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("A");
                            BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                        }
                        else if (isAHQMP == "H")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("H");
                            BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                        }
                        else if (isAHQMP == "Q")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("Q");
                            BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                        }
                        else if (isAHQMP == "M")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("M");
                            BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                        }
                        else
                        {
                            if (PhaseCount == 1)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                            }
                            else if (PhaseCount == 2)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                            }
                            else if (PhaseCount == 3)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                            }
                            else if (PhaseCount == 4)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                            }
                            else if (PhaseCount == 5)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                BindAuditScheduleEDIT(Convert.ToInt32(CustomerBranchId), Verticalid, AuditID);
                            }
                        }
                        upEDITComplianceDetails.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckProcessAsignedOrNotExists(string FinancialYear, string ForPeriod, long CustomerBranchId, int Verticalid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             join row1 in entities.InternalAuditScheduleOns
                             on row.ID equals row1.InternalAuditInstance
                             where row.ProcessId == row1.ProcessId && row1.FinancialYear == FinancialYear
                             && row1.ForMonth == ForPeriod && row.CustomerBranchID == CustomerBranchId && row.VerticalID == Verticalid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool ActivtInactiveInternalAuditScheduling(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname, int Verticalid, long AuditID)
        {
            try
            {
                bool isdeletesucess = false;
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    if (CheckProcessAsignedOrNotExists(FinancialYear, Termname, CustomerBranchId, Verticalid, AuditID) == false)
                    {
                        var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                                     where row.FinancialYear == FinancialYear && row.IsDeleted == false
                                                     && row.ISAHQMP == isAHQMP && row.CustomerBranchId == CustomerBranchId
                                                     && row.TermName == Termname && row.VerticalID == Verticalid
                                                     && row.AuditID == AuditID
                                                     select row.Id).ToList();
                        AuditorMastertoDelete.ForEach(entry =>
                       {
                           InternalAuditScheduling prevmappedids = (from row in entities.InternalAuditSchedulings
                                                                    where row.Id == entry
                                                                    select row).FirstOrDefault();
                           prevmappedids.IsDeleted = true;
                           prevmappedids.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                           prevmappedids.UpdatedOn = DateTime.Now;

                           var ScheduleList = (from row in entities.AuditScheduling_SubProcessMapping
                                               where row.ScheduleOnID == prevmappedids.Id
                                               select row.ID).ToList();
                           ScheduleList.ForEach(entrys =>
                           {
                               AuditScheduling_SubProcessMapping Schedule = (from row in entities.AuditScheduling_SubProcessMapping
                                                                             where row.ID == entrys
                                                                             select row).FirstOrDefault();
                               Schedule.IsDeleted = true;
                               Schedule.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                               Schedule.UpdaetdOn = DateTime.Now;

                           });
                       });
                        entities.SaveChanges();
                        isdeletesucess = true;
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Audit already Kicked-off. So, Audit Schedule can not be Edit/Delete...!";
                        isdeletesucess = false;
                    }
                    return isdeletesucess;
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        public void AuditClosureDelete(string isAHQMP, string FinancialYear, string ForPeriod, long CustomerBranchId, int Verticalid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                             where row.FinancialYear == FinancialYear && row.IsDeleted == false
                                             && row.ISAHQMP == isAHQMP && row.CustomerBranchId == CustomerBranchId
                                             && row.TermName == ForPeriod && row.VerticalID == Verticalid
                                             && row.AuditID == AuditID
                                             select row.Id).ToList();
                if (AuditorMastertoDelete.Count == 0)
                {
                    var ACD = (from row in entities.AuditClosureDetails
                               where row.ID == AuditID
                               select row).FirstOrDefault();

                    ACD.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
        }
        protected void grdAuditScheduling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        private void OpenScheduleInformation(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname, int Verticalid, int AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<AuditExpectedStartEndDate_Result> a = new List<AuditExpectedStartEndDate_Result>();
                    a = ProcessManagement.GetAuditExpectedStartEndDate_ResultProcedure(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid, AuditID).ToList();
                    var remindersummary = a.OrderBy(entry => entry.ProcessName).ToList();
                    grdAuditScheduleStartEndDate.DataSource = null;
                    grdAuditScheduleStartEndDate.DataBind();
                    grdAuditScheduleStartEndDate.DataSource = remindersummary;
                    grdAuditScheduleStartEndDate.DataBind();
                    upComplianceScheduleDialog.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region EDIT Record
        public List<SchedulingReport_ScheduleFirst_Result> GetSchedulingReport_ResultProcedure(int branchid, int ProcessId, string FinancialYear, int verticalID, int SubProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SchedulingReport_ScheduleFirst(branchid, ProcessId, FinancialYear, verticalID, SubProcessId).ToList();
                return complianceReminders;
            }
        }
        public void cleardatasourceEDIT()
        {
            grdAuditSchedulingEDIT.DataSource = null;
            grdAuditSchedulingEDIT.DataBind();
        }
        public static List<SP_EditAuditScheduling_ScheduleFirst_Result> GetSPEditAuditSchedulingDisplay(int Branchid, string Financialyear, int Verticalid, int AuditID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_EditAuditScheduling_ScheduleFirst(Branchid, Financialyear, Verticalid, AuditID).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_AnnualyReport_ScheduleFirst_Result> GetSPAnnualyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_AnnualyReport_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_HalfYearlyReport_ScheduleFirst_Result> GetSPHalfYearlyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_HalfYearlyReport_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_QuarterlyReport_ScheduleFirst_Result> GetSPQuarterwiseDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_QuarterlyReport_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_MonthlyReport_ScheduleFirst_Result> GetSPMonthlyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_MonthlyReport_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase1Report_ScheduleFirst_Result> GetSP_Phase1Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase1Report_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase2Report_ScheduleFirst_Result> GetSP_Phase2Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase2Report_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase3Report_ScheduleFirst_Result> GetSP_Phase3Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase3Report_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase4Report_ScheduleFirst_Result> GetSP_Phase4Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase4Report_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase5Report_ScheduleFirst_Result> GetSP_Phase5Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase5Report_ScheduleFirst(Branchid, Financialyear, Verticalid, SchedulingType).ToList();
                return auditdsplay;
            }
        }

        public void BindAuditScheduleEDIT(int Branchid, int Verticalid, long AuditID)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITFinancialYear"])))
                {
                    string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                    cleardatasourceEDIT();

                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;

                    List<SP_EditAuditScheduling_ScheduleFirst_Result> r = new List<SP_EditAuditScheduling_ScheduleFirst_Result>();
                    r = GetSPEditAuditSchedulingDisplay(Branchid, f1, Verticalid, Convert.ToInt32(AuditID));

                    if (r.Count > 0)
                        r = r.OrderBy(entry => entry.ProcessName).ToList();

                    grdAuditSchedulingEDIT.DataSource = r;
                    grdAuditSchedulingEDIT.DataBind();

                    DataTable dt = new DataTable();
                    dt = (grdAuditSchedulingEDIT.DataSource as List<SP_EditAuditScheduling_ScheduleFirst_Result>).ToDataTable();
                    DataTable dt1 = new DataTable();
                    dt1.Clear();
                    DataRow dr1 = null;
                    dt1.Columns.Add("ProcessName");
                    dt1.Columns.Add("SubProcessName");
                    dt1.Columns.Add("Annualy1");
                    dt1.Columns.Add("ProcessID", typeof(long));
                    dt1.Columns.Add("SubProcessID", typeof(long));
                    dr1 = dt1.NewRow();

                    dr1["ProcessName"] = "";
                    dr1["SubProcessName"] = "";
                    dr1["Annualy1"] = f1;
                    dr1["ProcessID"] = 0;
                    dr1["SubProcessID"] = 0;
                    dt1.Rows.Add(dr1);
                    dt1.Merge(dt);

                    grdAuditSchedulingEDIT.DataSource = dt1;
                    grdAuditSchedulingEDIT.DataBind();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Annually


        private List<sp_GetIASEditGridData_Result> GetEditGridBindData(int BranchID, int ProcessID, string FYear, int VerticalID, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var FetchData = entities.sp_GetIASEditGridData(BranchID, ProcessID, FYear, VerticalID, auditID).ToList();
                return FetchData;
            }
        }
        protected void grdAuditSchedulingEDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cells = e.Row.Cells[2];
                            cells.ColumnSpan = 1;
                            cells.Text = "SubProcess";
                            cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cells.BorderColor = Color.White;
                            cells.Font.Bold = true;
                            cells.ForeColor = Color.White;
                            cells.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[3];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            string termName = string.Empty;
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            Label lblSubProcessID = e.Row.FindControl("lblSubProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkAnnualy1");

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                long AuditID = 0;
                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                                {
                                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                                }

                                List<sp_GetIASEditGridData_Result> schFirstresult = new List<sp_GetIASEditGridData_Result>();
                                schFirstresult = GetEditGridBindData(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"]), AuditID).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();

                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Annually")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Apr-Sep")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Oct-Mar")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Oct-Dec")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Apr-Jun")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Jul-Sep")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Jan-Mar")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Apr")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "May")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Jun")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Jul")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Aug")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Sep")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Oct")
                                        {
                                            bf.Checked = true;
                                        }

                                        if (termName == "Nov")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Dec")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Jan")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Feb")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Mar")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Phase2")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Phase3")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Phase4")
                                        {
                                            bf.Checked = true;
                                        }
                                        if (termName == "Phase5")
                                        {
                                            bf.Checked = true;
                                        }
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cells = e.Row.Cells[2];
                                cells.ColumnSpan = 1;
                                cells.Text = "SubProcess";
                                cells.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cells.BorderColor = Color.White;
                                cells.ForeColor = Color.White;
                                cells.Font.Bold = true;
                                cells.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[3];
                                cell1.ColumnSpan = 1;
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Annually12 = new CheckBox();
                                string termName1 = string.Empty;
                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITTermname"])))
                                {
                                    termName1 = Convert.ToString(ViewState["EDITTermname"]);
                                }

                                if (termName1 == "Annually")
                                {
                                    Editchk_Annually12.Text = "Annually";
                                }
                                if (termName1 == "Apr-Sep")
                                {
                                    Editchk_Annually12.Text = "Apr-Sep";
                                }
                                if (termName1 == "Oct-Mar")
                                {
                                    Editchk_Annually12.Text = "Oct-Mar";
                                }
                                if (termName1 == "Oct-Dec")
                                {
                                    Editchk_Annually12.Text = "Oct-Dec";
                                }
                                if (termName1 == "Apr-Jun")
                                {
                                    Editchk_Annually12.Text = "Apr-Jun";
                                }
                                if (termName1 == "Jul-Sep")
                                {
                                    Editchk_Annually12.Text = "Jul-Sep";
                                }
                                if (termName1 == "Jan-Mar")
                                {
                                    Editchk_Annually12.Text = "Jan-Mar";
                                }
                                if (termName1 == "Apr")
                                {
                                    Editchk_Annually12.Text = "Apr";
                                }
                                if (termName1 == "May")
                                {
                                    Editchk_Annually12.Text = "May";
                                }
                                if (termName1 == "Jun")
                                {
                                    Editchk_Annually12.Text = "Jun";
                                }
                                if (termName1 == "Jul")
                                {
                                    Editchk_Annually12.Text = "Jul";
                                }
                                if (termName1 == "Aug")
                                {
                                    Editchk_Annually12.Text = "Aug";
                                }
                                if (termName1 == "Sep")
                                {
                                    Editchk_Annually12.Text = "Sep";
                                }
                                if (termName1 == "Oct")
                                {
                                    Editchk_Annually12.Text = "Oct";
                                }

                                if (termName1 == "Nov")
                                {
                                    Editchk_Annually12.Text = "Nov";
                                }
                                if (termName1 == "Dec")
                                {
                                    Editchk_Annually12.Text = "Dec";
                                }
                                if (termName1 == "Jan")
                                {
                                    Editchk_Annually12.Text = "Jan";
                                }
                                if (termName1 == "Feb")
                                {
                                    Editchk_Annually12.Text = "Feb";
                                }
                                if (termName1 == "Mar")
                                {
                                    Editchk_Annually12.Text = "Mar";
                                }
                                if (termName1 == "Phase1")
                                {
                                    Editchk_Annually12.Text = "Phase1";
                                }
                                if (termName1 == "Phase2")
                                {
                                    Editchk_Annually12.Text = "Phase2";
                                }
                                if (termName1 == "Phase3")
                                {
                                    Editchk_Annually12.Text = "Phase3";
                                }
                                if (termName1 == "Phase4")
                                {
                                    Editchk_Annually12.Text = "Phase4";
                                }
                                if (termName1 == "Phase5")
                                {
                                    Editchk_Annually12.Text = "Phase5";
                                }
                                Editchk_Annually12.ID = "EditAnnually1_Id";
                                Editchk_Annually12.Attributes.Add("OnClick", "return Editfunc();");
                                cell1.Controls.Add(Editchk_Annually12);
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "EditSchedule", "javascript:CheckEditSchedule()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdAuditSchedulingEDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAuditSchedulingEDIT.PageIndex = e.NewPageIndex;

            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                    {
                        BindAuditScheduleEDIT(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]), Convert.ToInt32(ViewState["AuditID"]));
                    }
                }
            }
        }

        #endregion

        protected void btnEDITSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblEDITFinancialyear.Text.Trim() != "")
                {
                    if (lblEDITLocationID.Text.Trim() != "")
                    {
                        if (lblEDITVerticalID.Text.Trim() != "")
                        {
                            bool successrahulf1 = false;
                            string financialyear = lblEDITFinancialyear.Text;
                            string[] fsplit = financialyear.Split('-');
                            string fyear = fsplit[0];
                            string syear = fsplit[1];
                            string f1 = fyear + "-" + syear;
                            string TermName = string.Empty;
                            string processnonprocess = string.Empty;
                            long AuditID = 0;
                            long UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                            {
                                AuditID = Convert.ToInt64(ViewState["AuditID"]);
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITTermname"])))
                            {
                                TermName = Convert.ToString(ViewState["EDITTermname"]);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITISAHQMP"])))
                            {
                                processnonprocess = Convert.ToString(ViewState["EDITISAHQMP"]);
                            }
                            #region All Save
                            if (!string.IsNullOrEmpty(TermName))
                            {
                                if (!string.IsNullOrEmpty(processnonprocess))
                                {
                                    successrahulf1 = ActivtInactiveInternalAuditScheduling(processnonprocess, f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text), AuditID);
                                    int processid = -1;
                                    int subprocessid = -1;
                                    int noofphases = -1;
                                    bool result = false;
                                    if (!string.IsNullOrEmpty(lblEDITPhaseCount.Text))
                                    {
                                        noofphases = (Convert.ToInt32(lblEDITPhaseCount.Text));
                                    }
                                    for (int i = 0; i < grdAuditSchedulingEDIT.Rows.Count; i++)
                                    {
                                        GridViewRow row = grdAuditSchedulingEDIT.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        Label lblSubProcessId = (Label)row.FindControl("lblSubProcessId");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            subprocessid = Convert.ToInt32(lblSubProcessId.Text);
                                            CheckBox bf = (CheckBox)row.FindControl("chkAnnualy1");
                                            if (successrahulf1)
                                            {
                                                if (bf.Checked)
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = TermName;
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = processnonprocess;
                                                    if (noofphases != -1)
                                                    {
                                                        Internalauditscheduling.PhaseCount = noofphases;
                                                    }
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    Internalauditscheduling.UpdatedBy = UserID;
                                                    Internalauditscheduling.UpdatedOn = DateTime.Now;
                                                    Internalauditscheduling.AuditID = AuditID;

                                                    AuditScheduling_SubProcessMapping.Process = processid;
                                                    AuditScheduling_SubProcessMapping.AuditID = AuditID;
                                                    AuditScheduling_SubProcessMapping.SubProcessID = subprocessid;
                                                    AuditScheduling_SubProcessMapping.CreatedBy = UserID;
                                                    AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                                    AuditScheduling_SubProcessMapping.IsActive = true;
                                                    AuditScheduling_SubProcessMapping.IsDeleted = false;
                                                    if (processid != 0)
                                                    {
                                                        UserManagementRisk.UpdateDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling, AuditScheduling_SubProcessMapping);
                                                    }
                                                    result = true;
                                                }
                                            }
                                        }
                                    }
                                    if (result)
                                    {
                                        cvDuplicateEntry1.IsValid = false;
                                        cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                    }
                                    else
                                    {
                                        AuditClosureDelete(processnonprocess, f1, lblEDITTermName.Text, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text), AuditID);
                                    }
                                    BindAuditScheduleEDIT(Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text), AuditID);
                                }
                            }
                            #endregion
                            BindMainGrid();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindMainGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditScheduling.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        ////////////////////////////Sushant Code///////////////

        #region Dropdown code Start
        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindLegalEntityDataPop()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntityPop.DataTextField = "Name";
            ddlLegalEntityPop.DataValueField = "ID";
            ddlLegalEntityPop.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntityPop.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //ddlLegalEntityPop.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntityPop.DataBind();
            ddlLegalEntityPop.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityDataPop(DropDownList DRP, int ParentId)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                DRP.DataTextField = "Name";
                DRP.DataValueField = "ID";
                DRP.Items.Clear();
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
                DRP.DataBind();
                DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion End Code
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));

                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindVerticalID(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }

        protected void btnSaveMainGrid_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                int ID = -1;
                for (int i = 0; i < grdAuditScheduling.Rows.Count; i++)
                {
                    GridViewRow row = grdAuditScheduling.Rows[i];
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtStartDate = (TextBox)row.FindControl("txtStartDateGrid");
                    TextBox txtEndDate = (TextBox)row.FindControl("txtEndDateGrid");

                    if (txtStartDate.Text != "" && txtEndDate.Text != "")
                    {
                        if (!string.IsNullOrEmpty(lblID.Text))
                        {
                            DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                            Internalauditscheduling.Id = Convert.ToInt32(lblID.Text);
                            Internalauditscheduling.StartDate = GetDate(a.ToString("dd/MM/yyyy"));
                            Internalauditscheduling.EndDate = GetDate(b.ToString("dd/MM/yyyy"));
                            ProcessManagement.UpdateInternalAuditorScheduling(Internalauditscheduling);
                        }
                    }
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindddlBranchApplyto()
        {

            int Branchid = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
            {
                if (ddlLegalEntityPop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
            {
                if (ddlSubEntity1Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
            {
                if (ddlSubEntity2Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
            {
                if (ddlSubEntity3Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
            {
                if (ddlSubEntity4Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                }
            }

            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlBranchList.Items.Clear();
            var details = AuditKickOff_NewDetails.FillSubEntityDataScheduleingApplyTO(Branchid, customerID);

            ddlBranchList.DataSource = details;
            ddlBranchList.DataTextField = "Name";
            ddlBranchList.DataValueField = "ID";
            ddlBranchList.DataBind();
            updateApplyToPopUp.Update();

        }

        protected void btnSaveApplyto_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                int Branchid = -1;
                int VerticalID = -1;
                bool SaveSuccess = false;
                if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                {
                    if (ddlLegalEntityPop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                {
                    if (ddlSubEntity1Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                {
                    if (ddlSubEntity2Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                {
                    if (ddlSubEntity3Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                {
                    if (ddlSubEntity4Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                    {
                        if (ddlVerticalListPopup.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                        }
                    }
                }
                if (VerticalID != -1)
                {
                    var schedulingList = UserManagementRisk.GetDataFromPopUpSave(Branchid, VerticalID);
                    if (schedulingList.Count > 0)
                    {
                        foreach (var item in schedulingList)
                        {
                            InternalauditschedulingList.Clear();
                            for (int i = 0; i < ddlBranchList.Items.Count; i++)
                            {
                                if (ddlBranchList.Items[i].Selected == true)
                                {
                                    var verticalIDList = UserManagementRisk.FillVerticalListFromRiskActTrasa(Convert.ToInt32(ddlBranchList.Items[i].Value), item.VerticalID);
                                    if (verticalIDList.Count > 0)
                                    {
                                        foreach (var Items in verticalIDList)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(Convert.ToInt32(ddlBranchList.Items[i].Value), Convert.ToInt32(Items.VerticalsId), item.FinancialYear, item.TermName))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlBranchList.Items[i].Value);
                                                Internalauditscheduling.FinancialYear = item.FinancialYear;
                                                Internalauditscheduling.TermName = item.TermName;
                                                Internalauditscheduling.TermStatus = item.TermStatus;
                                                Internalauditscheduling.Process = item.Process;
                                                Internalauditscheduling.ISAHQMP = item.ISAHQMP;
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(item.StartDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(item.EndDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = (int)Items.VerticalsId;

                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                            }
                                        }
                                    }
                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                SaveSuccess = true;
                            }
                        }

                        if (SaveSuccess)
                        {
                            cvDuplicateEntryApplyPop.IsValid = false;
                            cvDuplicateEntryApplyPop.ErrorMessage = "Audit Schedule Save Successfully.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller1()", true);
                            ddlBranchList.ClearSelection();
                        }
                    }
                    else
                    {
                        cvDuplicateEntryApplyPop.IsValid = false;
                        cvDuplicateEntryApplyPop.ErrorMessage = "No Previous Audit Schedule Record found for current selection.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryApplyPop.IsValid = false;
                cvDuplicateEntryApplyPop.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// //////////////////////Pop Up code Sushant//////////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLegalEntityPop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
            {
                if (ddlLegalEntityPop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity1Pop, Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                    BindProcessList(Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                }

                if (ddlSubEntity2Pop.Items.Count > 0)
                    ddlSubEntity2Pop.Items.Clear();

                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();

                cleardatasource();
                BindGridpopData();
            }
        }

        protected void ddlSubEntity1Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
            {
                if (ddlSubEntity1Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity2Pop, Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                    BindProcessList(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                }
                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();

                cleardatasource();
                clearselection();
                BindGridpopData();
            }
        }

        protected void ddlSubEntity2Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
            {
                if (ddlSubEntity2Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity3Pop, Convert.ToInt32(ddlSubEntity2Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                    BindProcessList(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                }
                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();
                cleardatasource();
                clearselection();
                BindGridpopData();
            }
        }

        protected void ddlSubEntity3Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
            {
                if (ddlSubEntity3Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity4Pop, Convert.ToInt32(ddlSubEntity3Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                    BindProcessList(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4Pop.Items.Count > 0)
                        ddlSubEntity4Pop.Items.Clear();
                }
                cleardatasource();
                clearselection();
                BindGridpopData();
            }
        }

        protected void ddlSubEntity4Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
            {
                cleardatasource();
                clearselection();
                BindGridpopData();
                BindVerticalIDPOPup(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                BindProcessList(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
            }
        }
        public void BindGridpopData()
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("A", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("H", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("Q", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("M", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (ddlSchedulingType.SelectedItem.Text == "Phase")
                        {
                            Divnophase.Visible = true;
                        }
                        else
                        {
                            Divnophase.Visible = false;
                        }
                        cleardatasource();
                    }

                }
            }
            else
            {
                cleardatasource();
            }
        }
        protected void btnapply_Click(object sender, EventArgs e)
        {
            BindddlBranchApplyto();
        }

        protected void ddlProcessList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSchedulingType.SelectedValue = "-1";
            //List<int> ProcessList = new List<int>();
            //ProcessList.Clear();
            //if (ddlProcessList.Items.Count > 0)
            //{
            //    for (int i = 0; i < ddlProcessList.Items.Count; i++)
            //    {
            //        if (ddlProcessList.Items[i].Selected)
            //        {
            //            ProcessList.Add(Convert.ToInt32(ddlProcessList.Items[i].Value));
            //        }
            //    }
            //}
            //if (ProcessList.Count > 0)
            //{
            //    if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
            //    {
            //        if (ddlSchedulingType.SelectedValue != "-1")
            //        {
            //            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            //            {
            //                if (ddlFinancialYear.SelectedValue != "-1")
            //                {
            //                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
            //                    {
            //                        EnableDisable(0);
            //                        BindAuditSchedule("A", 0);
            //                    }
            //                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            //                    {
            //                        EnableDisable(0);
            //                        BindAuditSchedule("H", 0);
            //                    }
            //                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            //                    {
            //                        EnableDisable(0);
            //                        BindAuditSchedule("Q", 0);
            //                    }
            //                    else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            //                    {
            //                        EnableDisable(0);
            //                        BindAuditSchedule("M", 0);
            //                    }
            //                    else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            //                    {
            //                        if (ddlSchedulingType.SelectedItem.Text == "Phase")
            //                        {
            //                            Divnophase.Visible = true;
            //                        }
            //                        else
            //                        {
            //                            Divnophase.Visible = false;
            //                        }
            //                        cleardatasource();
            //                    }
            //                }
            //                else
            //                {
            //                    cvDuplicateEntry.IsValid = false;
            //                    cvDuplicateEntry.ErrorMessage = "Please Select Financial Year";
            //                }
            //            }
            //        }
            //        else
            //        {

            //            cvDuplicateEntry.IsValid = false;
            //            cvDuplicateEntry.ErrorMessage = "Please Select Scheduling Type";
            //            cleardatasource();
            //        }
            //    }
            //}
            //else
            //{
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Please Select Process";
            //}
        }
    }
}