﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="AuditKickOffNEW_old.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditKickOffNEW_old" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridView1 {
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .accordionContent1 {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 92%;
        }

        .accordionHeaderSelected1 {
            background-color: #5078B3;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .accordionHeader1 {
            background-color: #2E4D7B;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .href1 {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }

        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.            
            $(function () {
                $('input[id*=tbxLoanStartDate]').datepicker(
                    { dateFormat: 'dd-mm-yy' });
            });
            $(function () {
                $('input[id*=tbxLoanEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>

    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="ComplianceValidationGroup1" />
                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroup1" Display="None" />
                <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
            </div>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <div id="FilterLocationdiv" runat="server" style="margin-bottom: 7px; position: relative; display: inline-block;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Select  Location</label>
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 210px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Location."
                                    ControlToValidate="tbxFilterLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />-
                            </div>
                        </div>
                    </td>
                    <td style="width: 10%">Financial Year</td>
                    <td style="width: 23%">
                        <asp:DropDownList runat="server" ID="ddlFilterFinancialYear" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="saaaaaaaaaaaa" runat="server">
        <ContentTemplate>
            <ajaxToolkit:Accordion ID="UserAccordion1" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader1"
                HeaderSelectedCssClass="accordionHeaderSelected1" ContentCssClass="accordionContent1" FadeTransitions="true"
                SuppressHeaderPostbacks="true" TransitionDuration="250" FramesPerSecond="40" RequireOpenedPane="false" AutoSize="None">
                <Panes>
                    <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                        <Header><a href="#" class="href1">Audit KickOff Details</a></Header>
                        <Content>

                            <asp:Panel ID="LoanEntry" runat="server">
                                <div style="text-align: Left; margin-left: 0%; font-weight: bold; margin-bottom: 10px; color: black;">
                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                </div>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="vertical-align: top; width: 50%;">
                                            <div style="margin-top: 10px">
                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="25" Width="100%"
                                                            Font-Size="12px" DataKeyNames="ProcessID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="200px" SortExpression="CustomerBranchId" HeaderStyle-VerticalAlign="Middle">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                            <asp:Label runat="server" ID="lblLocation" Text='<%# ShowCustomerBranchName((int)Eval("CustomerBranchId")) %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Financial Year" ItemStyle-Width="100px" SortExpression="FinancialYear" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label runat="server" ID="lblFinancialYear" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Term Name" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Assigned To" ItemStyle-Width="10px" SortExpression="AssignedTo" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                            <asp:Label runat="server" ID="lblAssignedTo" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="EAId" ItemStyle-Width="10px" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                            <asp:Label runat="server" ID="lblExternalAuditorId" Text='<%# Eval("ExternalAuditorId") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="PhaseCount" ItemStyle-Width="10px" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                            <asp:Label runat="server" ID="lblPhaseCount" Text='<%# Eval("PhaseCount") %>' CssClass="label"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") %>' CausesValidation="false"><img src="../../Images/edit_icon.png" alt="Edit Compliance" title="Edit Compliance" /></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>
                                                                    </HeaderTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                            <PagerSettings Position="Top" />
                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                            <EmptyDataTemplate>
                                                                No Records Found.
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </Content>
                    </ajaxToolkit:AccordionPane>

                    <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                        <Header><a href="#" class="href1">Process</a> </Header>
                        <Content>
                            <asp:Panel ID="Panel2" runat="server">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                   
                                </div>
                                <div style="margin: 5px 5px 20px;">

                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Location</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList Enabled="false" runat="server" ID="ddlLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" />
                                                <asp:CompareValidator ID="CompareValidator14" ErrorMessage="Please Select Location."
                                                    ControlToValidate="ddlLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            </td>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Internal /External
                                                </label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlInternalExternal" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlInternalExternal_SelectedIndexChanged" Enabled="false">
                                                    <asp:ListItem Text="Select">Select Internal/External</asp:ListItem>
                                                    <asp:ListItem Text="Internal">Internal</asp:ListItem>
                                                    <asp:ListItem Text="Internal">External</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator17" ErrorMessage="Please Select Internal /External."
                                                    ControlToValidate="ddlInternalExternal" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Financial Year</label></td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlFinancialYear" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" Enabled="false" /></td>
                                            <div style="margin-bottom: 7px" runat="server" id="DivIsExternal" visible="false">
                                                <td>
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        External Auditor</label></td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlAuditor" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" Enabled="false"
                                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                            </div>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Scheduling Type</label></td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlSchedulingType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" Enabled="false" /></td>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Select For Period</label></td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlPeriod" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" Enabled="false" />
                                                <asp:RequiredFieldValidator ID="RFVPeriod" ErrorMessage="Select Period." ControlToValidate="ddlPeriod"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="Select Period" Display="None" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Actual Start Date</label></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbxLoanStartDate" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Actual Start Date can not be empty."
                                                    ControlToValidate="tbxLoanStartDate"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" /></td>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Expected End Date
                                                </label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbxLoanEndDate" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ErrorMessage="Expected End Date can not be empty."
                                                    ControlToValidate="tbxLoanEndDate"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 210px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Audit Planning Document Upload</label></td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                                                <asp:FileUpload runat="server" ID="fuSampleFile" /></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="margin-top:20px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="left">Auditor Details                                                                                                                                                                                                                                                                 
                                            </td>
                                        </tr>
                                         <tr>
                                            <td colspan="4" style="margin-top:20px;"> 
                                                <asp:Label runat="server" ID="Label3" Font-Bold="true" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="Vertical"
                                                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%" OnRowDataBound="grdRiskActivityMatrix_RowDataBound"
                                                            Font-Size="12px" DataKeyNames="ProcessId" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateID" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Process" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlPerformer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlReviewer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" Height="25px" Width="60px"
                                                                            ValidationGroup="ComplianceValidationGroup" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                            <PagerSettings Position="Top" />
                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdActualAssignmentSave" AutoGenerateColumns="false" GridLines="Vertical"
                                                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%"
                                                            Font-Size="12px" DataKeyNames="ProcessId">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateID" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                                                        <asp:Label ID="lblPerformerUserID" runat="server" Text='<%# Eval("PerformerUserID") %>'></asp:Label>
                                                                        <asp:Label ID="lblReviewerUserID" runat="server" Text='<%# Eval("ReviewerUserID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowPerformerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowReviewerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                            <PagerSettings Position="Top" />
                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button" /></td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdActualAssignmentSaved" AutoGenerateColumns="false" GridLines="Vertical"
                                                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%"
                                                            Font-Size="12px" DataKeyNames="ProcessId">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Process" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowPerformerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowReviewerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                            <PagerSettings Position="Top" />
                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </Content>
                    </ajaxToolkit:AccordionPane>
                    <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                        <Header><a href="#" class="href1">Implementation Status</a> </Header>
                        <Content>
                            <asp:Panel ID="Panel1" runat="server">
                                <table style="width: 100%">
                                    <tr style="display:none;">
                                        <td style="width: 25%" >
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                Financial Year</label>

                                        </td>
                                        <td style="width: 25%">
                                            <asp:DropDownList runat="server" ID="ddlFinancialYearImplementation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                OnSelectedIndexChanged="ddlFinancialYearImplementation_SelectedIndexChanged"
                                                CssClass="txtbox" />
                                        </td>
                                        <td style="width: 25%"></td>
                                        <td style="width: 25%"></td>
                                    </tr>

                                     <tr>
                                            <td colspan="4" style="margin-top:20px;"> 
                                                <asp:Label runat="server" ID="Label4" Font-Bold="true" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView runat="server" ID="grdImplementationStatus" AutoGenerateColumns="false" GridLines="Vertical"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%"
                                                        OnRowDataBound="grdImplementationStatus_RowDataBound" OnRowCommand="grdImplementationStatus_RowCommand"
                                                        Font-Size="12px" OnPageIndexChanging="grdImplementationStatus_PageIndexChanging">
                                                        <Columns>
                                                             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Financial Year" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>                                                                     
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Period" ItemStyle-Width="200px" >
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlPerformer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlReviewer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="30px">
                                                                <ItemTemplate >
                                                                    <asp:Button ID="btnAddNew" runat="server" Text="Add"
                                                                        CommandName="ADDNEW" Height="25px" Width="60px" CausesValidation="false"
                                                                        ValidationGroup="ComplianceValidationGroup" />                                                                    
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                        <PagerSettings Position="Top" />
                                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView runat="server" ID="grdImplementationStatusSave" AutoGenerateColumns="false" GridLines="Vertical"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%"
                                                        Font-Size="12px">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Financial Year" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>                                                                     
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Period" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                     <asp:Label ID="lblPerformerUserID" runat="server" Text='<%# Eval("PerformerUserID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblReviewerUserID" runat="server" Text='<%# Eval("ReviewerUserID") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowIMPlementationPerformerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowIMPlementationReviewerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                        <PagerSettings Position="Top" />
                                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveNew" OnClick="btnSaveNew_Click" CssClass="button" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView runat="server" ID="grdImplementationStatusSaved" AutoGenerateColumns="false" GridLines="Vertical"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%"
                                                        Font-Size="12px">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Financial Year" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>                                                                     
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Period" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowIMPlementationPerformerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer" ItemStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowIMPlementationReviewerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                        <PagerSettings Position="Top" />
                                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </Content>
                    </ajaxToolkit:AccordionPane>
                </Panes>
            </ajaxToolkit:Accordion>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
