﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AdditionalRiskCreation : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindProcess("P");
                BindVertical();
                BindDataFirstTime("P");
                bindPageNumber();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;

            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N"); 
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindVertical()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlVertical.DataTextField = "VerticalName";
            ddlVertical.DataValueField = "ID";
            ddlVertical.Items.Clear();
            ddlVertical.DataSource = UserManagementRisk.FillVerticalList(customerID); //FillVerticalList(int customerID)
            ddlVertical.DataBind();            
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        private void BindProcess(string flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                int CustomerBranchId = -1;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P",customerID, CustomerBranchId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("N", customerID, CustomerBranchId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("P");                    
                }
                else
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("N");                    
                }

                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));               
            }
            else
            {                
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("P");                    
                }
                else
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("N");                    
                }

                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {                
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("P");                    
                }
                else
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("N");                   
                }

                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {               
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("P");                   
                }
                else
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("N");                   
                }

                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("P");                    
                }
                else
                {
                    grdRiskActivityMatrix.DataSource = null;
                    grdRiskActivityMatrix.DataBind();
                    BindProcess("N");                   
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");               
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");               
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void btnPopupSave_Click(object sender, EventArgs e)
        {
            try
            {
                long auditStepMasterID = -1;
                int riskcreationid = -1;
                int RATBDMID = -1;
                long riskactivityid = -1;
                long verticalid = -1;
                long BranchId = -1;
                
                if (ViewState["RiskCreationId"] !=null)
                {
                    riskcreationid = Convert.ToInt32(ViewState["RiskCreationId"]);
                }
                if (ViewState["RiskActivityId"] != null)
                {
                    riskactivityid = Convert.ToInt64(ViewState["RiskActivityId"]);
                }
                if (ViewState["VerticalId"] != null)
                {
                    verticalid = Convert.ToInt64(ViewState["VerticalId"]);
                }                
                if (ViewState["RATBDID"] != null)
                {
                    RATBDMID = Convert.ToInt32(ViewState["RATBDID"]);
                }
                if (ViewState["BranchId"] != null)
                {
                    BranchId = Convert.ToInt64(ViewState["BranchId"]);
                }
                                
                if (riskcreationid != -1)
                {
                    if (riskactivityid != -1)
                    {
                        if (verticalid != -1)
                        {
                            if (RATBDMID != -1)
                            {
                                if (BranchId != -1)
                                {
                                    
                                RiskCategoryCreation RCC = RiskCategoryManagement.RiskGetByID(riskcreationid);
                                    if (rdCD.SelectedValue == "Same")
                                    {
                                        if ((int)ViewState["Mode"] == 0)// Create
                                        {
                                            auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(txtATBD.Text.Trim());
                                            if (auditStepMasterID == 0)
                                            {
                                                AuditStepMaster newASM = new AuditStepMaster()
                                                {
                                                    AuditStep = txtATBD.Text.Trim()
                                                };

                                                if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                    auditStepMasterID = newASM.ID;
                                            }

                                            RiskActivityToBeDoneMapping riskactivitytoBeDoneMapping = new RiskActivityToBeDoneMapping()
                                            {
                                                RiskCategoryCreationId = riskcreationid,
                                                ActivityTobeDone = txtATBD.Text,
                                                ProcessId = RCC.ProcessId,
                                                SubProcessId = RCC.SubProcessId,
                                                RiskActivityId = riskactivityid,
                                                CustomerBranchID = BranchId,
                                                VerticalID = verticalid,
                                                IsActive = true,
                                                Rating = Convert.ToInt32(ddlRatingItemTemplate.SelectedValue),
                                                AuditStepMasterID = auditStepMasterID,

                                            };
                                            if ((ProcessManagement.RiskActivityToBeDoneMappingGetByName(txtATBD.Text, riskcreationid, riskactivityid)) == 0)
                                            {
                                                using (AuditControlEntities entities = new AuditControlEntities())
                                                {
                                                    entities.RiskActivityToBeDoneMappings.Add(riskactivitytoBeDoneMapping);
                                                    entities.SaveChanges();
                                                }
                                            }
                                        }

                                        if ((int)ViewState["Mode"] == 1) //Update
                                        {
                                            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation();
                                            riskcategorycreation.Id = riskcreationid;

                                            riskcategorycreation.ProcessId = RCC.ProcessId;
                                            riskcategorycreation.SubProcessId = RCC.SubProcessId;
                                            RiskCategoryManagement.UpdateRiskCategoryCreation(riskcategorycreation);

                                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                            riskactivitytransaction.Id = (Int32)riskactivityid;
                                            riskactivitytransaction.RiskCreationId = riskcreationid;
                                            riskactivitytransaction.ControlDescription = txtCDEC.Text;
                                            riskactivitytransaction.ProcessId = RCC.ProcessId;
                                            riskactivitytransaction.SubProcessId = RCC.SubProcessId;
                                            riskactivitytransaction.VerticalsId = verticalid;

                                            RiskCategoryManagement.UpdateRiskActivityTransaction(riskactivitytransaction);
                                            if (RiskCategoryManagement.ExistsRATBDMByID(RATBDMID))
                                            {
                                                auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(txtATBD.Text.Trim());
                                                if (auditStepMasterID == 0)
                                                {
                                                    AuditStepMaster newASM = new AuditStepMaster()
                                                    {
                                                        AuditStep = txtATBD.Text.Trim()
                                                    };

                                                    if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                        auditStepMasterID = newASM.ID;
                                                }

                                                if (auditStepMasterID != 0)
                                                {
                                                    RiskActivityToBeDoneMapping RATBDEdit = new RiskActivityToBeDoneMapping();
                                                    RATBDEdit.RiskCategoryCreationId = riskcreationid;
                                                    RATBDEdit.RiskActivityId = riskactivityid;
                                                    RATBDEdit.ProcessId = RCC.ProcessId;
                                                    RATBDEdit.SubProcessId = RCC.SubProcessId;
                                                    RATBDEdit.ActivityTobeDone = txtATBD.Text.Trim();
                                                    RATBDEdit.Rating = Convert.ToInt64(ddlRatingItemTemplate.SelectedValue);
                                                    RATBDEdit.CustomerBranchID = BranchId;
                                                    RATBDEdit.VerticalID = verticalid;
                                                    RATBDEdit.IsActive = true;
                                                    RATBDEdit.AuditStepMasterID = auditStepMasterID;
                                                    RiskCategoryManagement.UpdateRiskActivityTBDMappings(RATBDEdit, RATBDMID);
                                                }
                                            }
                                        }
                                        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                                        {
                                            grdRiskActivityMatrix.DataSource = null;
                                            grdRiskActivityMatrix.DataBind();
                                            ddlPageSize_SelectedIndexChanged(sender, e);
                                        }
                                        else
                                        {
                                            grdRiskActivityMatrix.DataSource = null;
                                            grdRiskActivityMatrix.DataBind();
                                            ddlPageSize_SelectedIndexChanged(sender, e);
                                        }
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                                    }
                                    else if (rdCD.SelectedValue == "New")
                                    {
                                        RiskActivityTransaction RAT = RiskCategoryManagement.RiskActivityTransactionGetByID((Int32)riskactivityid, riskcreationid);
                                        RiskActivityTransaction RiskActivityTransactionr = new RiskActivityTransaction()
                                        {
                                            RiskCreationId = riskcreationid,
                                            ControlDescription = txtCDEC.Text,
                                            ProcessId = RCC.ProcessId,
                                            SubProcessId = RCC.SubProcessId,
                                            IsDeleted = false,
                                            Assertion = RAT.Assertion,
                                            MControlDescription = RAT.MControlDescription,
                                            PersonResponsible = RAT.PersonResponsible,
                                            EffectiveDate = RAT.EffectiveDate,
                                            Key_Value = RAT.Key_Value,
                                            PrevationControl = RAT.PrevationControl,
                                            AutomatedControl = RAT.AutomatedControl,
                                            Frequency = RAT.Frequency,
                                            CustomerBranchId = RAT.CustomerBranchId,
                                            VerticalsId = verticalid,
                                            RCMType="ARS"
                                        };

                                        if ((ProcessManagement.RiskRiskActivityTransactionGetByName(txtCDEC.Text.Trim()) == 0))
                                        {
                                            using (AuditControlEntities entities = new AuditControlEntities())
                                            {
                                                entities.RiskActivityTransactions.Add(RiskActivityTransactionr);
                                                entities.SaveChanges();

                                                if (RiskCategoryManagement.ExistsRATBDMByID(RATBDMID))
                                                {
                                                    auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(txtATBD.Text.Trim());
                                                    if (auditStepMasterID == 0)
                                                    {
                                                        AuditStepMaster newASM = new AuditStepMaster()
                                                        {
                                                            AuditStep = txtATBD.Text.Trim()
                                                        };

                                                        if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                            auditStepMasterID = newASM.ID;
                                                    }

                                                    if (auditStepMasterID != 0)
                                                    {
                                                        RiskActivityToBeDoneMapping riskactivitytoBeDoneMapping = new RiskActivityToBeDoneMapping()
                                                        {
                                                            RiskCategoryCreationId = riskcreationid,
                                                            ActivityTobeDone = txtATBD.Text,
                                                            ProcessId = RCC.ProcessId,
                                                            SubProcessId = RCC.SubProcessId,
                                                            RiskActivityId = RiskActivityTransactionr.Id,
                                                            CustomerBranchID = BranchId,
                                                            VerticalID = verticalid,
                                                            IsActive = true,
                                                            Rating = Convert.ToInt32(ddlRatingItemTemplate.SelectedValue),
                                                            AuditStepMasterID = auditStepMasterID,
                                                        };

                                                        if ((ProcessManagement.RiskActivityToBeDoneMappingGetByName(txtATBD.Text, riskcreationid, riskactivityid)) == 0)
                                                        {
                                                            using (AuditControlEntities entities1 = new AuditControlEntities())
                                                            {
                                                                entities1.RiskActivityToBeDoneMappings.Add(riskactivitytoBeDoneMapping);
                                                                entities1.SaveChanges();
                                                            }
                                                        }
                                                    }
                                                }
                                                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                                                {
                                                    grdRiskActivityMatrix.DataSource = null;
                                                    grdRiskActivityMatrix.DataBind();
                                                    ddlPageSize_SelectedIndexChanged(sender, e);
                                                }
                                                else
                                                {
                                                    grdRiskActivityMatrix.DataSource = null;
                                                    grdRiskActivityMatrix.DataBind();
                                                    ddlPageSize_SelectedIndexChanged(sender, e);
                                                }
                                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        public string ShowRating(string RiskRating)
        {            
            string processnonprocess = "";
            if (RiskRating=="1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }  

        protected void txtFilter_TextChanged(object sender, EventArgs e)
            {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
        }

        private void BindDataFirstTime(string Flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int processid = -1;
                int subprocessid = -1;
                int CustomerBranchId = -1;
                String FilterText = String.Empty;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }

                if (txtFilter.Text != "")
                    FilterText = txtFilter.Text;

                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();

                if (Flag == "P")
                {
                    var Riskcategorymanagementlist = GetAllRiskControlMatrixFirstTime(customerID, Branchlist.ToList(), processid, subprocessid, "P", FilterText);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<SP_AdditionalRiskCreationFillView_Result>).ToDataTable();
                }
                else
                {
                    var Riskcategorymanagementlist = GetAllRiskControlMatrixFirstTime(customerID, Branchlist.ToList(), processid, subprocessid, "N", FilterText);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<SP_AdditionalRiskCreationFillView_Result>).ToDataTable();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindData(string Flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                int processid = -1;
                int subprocessid = -1;
                int verticalid = -1;
                int CustomerBranchId = -1;
                String FilterText = String.Empty;
                              
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        Session["BranchList"] = 0;
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                else
                {
                    List<long> branchs = new List<long>();
                    for (int i = 0; i < ddlLegalEntity.Items.Count; i++)
                    {
                        if (ddlLegalEntity.Items[i].Value != "-1")
                        {
                            if (ddlLegalEntity.Items[i].Value != "")
                            {
                                branchs.Add(Convert.ToInt64(ddlLegalEntity.Items[i].Value));
                                Session["BranchList"] = branchs;
                            }
                        }

                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }              
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        verticalid = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                if (txtFilter.Text != "")
                    FilterText = txtFilter.Text;

                Branchlist.Clear();
                List<long> branch = new List<long>();
                branch = Session["BranchList"] as List<long>;
                if (branch != null)
                {
                    for (int i = 0; i < branch.Count; i++)
                    {
                        CustomerBranchId = Convert.ToInt32(branch[i]);
                        GetAllHierarchy(customerID, CustomerBranchId);
                    }
                }
                else
                {
                    var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                }
                //var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();

                if (Flag == "P")
                {
                    var Riskcategorymanagementlist = Business.AdditionalRiskCreation.GetAuditStepExportForUpdateAdditional(customerID, Branchlist, processid, subprocessid, verticalid, FilterText, "P");

                    //var Riskcategorymanagementlist = GetAllAuditStepExport(customerID, Branchlist.ToList(), processid, subprocessid, "P", FilterText);
                    //var Riskcategorymanagementlist = GetAllRiskControlMatrix(customerID, Branchlist.ToList(), processid, subprocessid,"P", FilterText);

                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<AuditStepExportFillView>).ToDataTable();
                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<SP_AdditionalRiskCreationFillView_Result>).ToDataTable();

                    //Riskcategorymanagementlist.Clear();
                    //int identificador = GC.GetGeneration(Riskcategorymanagementlist);
                    //GC.Collect(identificador, GCCollectionMode.Forced);

                    Riskcategorymanagementlist.Clear();
                    Riskcategorymanagementlist = null;
                    //GC.Collect();
                }
                else
                {
                    var Riskcategorymanagementlist = Business.AdditionalRiskCreation.GetAuditStepExportForUpdateAdditional(customerID, Branchlist, processid, subprocessid, verticalid, FilterText, "P");
                   
                    //var Riskcategorymanagementlist = GetAllAuditStepExport(customerID, Branchlist.ToList(), processid, subprocessid, "N", FilterText);
                    //var Riskcategorymanagementlist =  GetAllRiskControlMatrix(customerID, Branchlist.ToList(), processid, subprocessid, "N", FilterText);

                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;                    
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<AuditStepExportFillView>).ToDataTable();
                    //Session["grdFilterAuditSteps"] = (grdRiskActivityMatrix.DataSource as List<SP_AdditionalRiskCreationFillView_Result>).ToDataTable(); 

                    //Riskcategorymanagementlist.Clear();
                    //int identificador = GC.GetGeneration(Riskcategorymanagementlist);
                    //GC.Collect(identificador, GCCollectionMode.Forced);

                    Riskcategorymanagementlist.Clear();
                    Riskcategorymanagementlist = null;
                    //GC.Collect();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<sp_AuditStepExport_New_Result> GetDataExport(string Flag)
        {
            int CustomerBranchId = -1;
            int processid = -1;
            int subprocessid = -1;
            int verticalid = -1;
            string FilterText = String.Empty;

            int customerID = -1;            
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            List<sp_AuditStepExport_New_Result> AuditStepList = new List<sp_AuditStepExport_New_Result>();
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (ddlProcess.SelectedValue != "")
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
            {
                if (ddlSubProcess.SelectedValue != "-1")
                {
                    subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                }
            }

            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
            }

            if (txtFilter.Text != "")
                FilterText = txtFilter.Text;            

            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);
            Branchlist.ToList();

            if (Flag == "L")
            {                
                AuditStepList = Business.AdditionalRiskCreation.GetAuditStepExportForUpdateAdditional(customerID, Branchlist, processid, subprocessid, verticalid, FilterText, "P");
                //AuditStepList = GetAllAuditStepExport(customerID, Branchlist.ToList(), processid, subprocessid, "P", FilterText);
                //AuditStepList = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");                              
            }
            else if (Flag == "P")
            {
                
                AuditStepList = Business.AdditionalRiskCreation.GetAuditStepExportForUpdateAdditional(customerID, Branchlist, processid, subprocessid, verticalid, FilterText, "P");
                //AuditStepList = GetAllAuditStepExport(customerID, Branchlist.ToList(), processid, subprocessid, "N", FilterText);
                //AuditStepList = AdditionalRiskCreation.GetAllRiskCategoryCreationExport(customerID, Branchlist, processid, verticalid, "P");
            }

            return AuditStepList;
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditStepExportForUpdate");
                    DataTable ExcelData = null;

                    //var AuditStepList = GetDataExport("P");

                    DataView view = new System.Data.DataView(GetDataExport("P").ToDataTable());

                    ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "ATBDId");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    view.Dispose();
                    view = null;                   

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Location";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(20);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(20);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Control No";
                    exWorkSheet.Cells["E1"].AutoFitColumns(15);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Risk Description";
                    exWorkSheet.Cells["F1"].AutoFitColumns(50);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Control Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(50);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Risk Rating";
                    exWorkSheet.Cells["H1"].AutoFitColumns(15);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Audit Methodology";
                    exWorkSheet.Cells["I1"].AutoFitColumns(50);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Audit Steps";
                    exWorkSheet.Cells["J1"].AutoFitColumns(50);

                    //exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    //exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    //exWorkSheet.Cells["K1"].Value = "Audit Step Rating";
                    //exWorkSheet.Cells["K1"].AutoFitColumns(15);

                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "ID";
                    exWorkSheet.Cells["K1"].AutoFitColumns(10);

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 11])
                    {
                        col.Style.WrapText = true;
                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.AutoFitColumns();

                        //Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    ExcelData.Dispose();
                    ExcelData = null;

                    //exWorkSheet.Protection.IsProtected = true;
                    //exWorkSheet.Protection.AllowSort = true;
                    //exWorkSheet.Protection.AllowAutoFilter = true;
                    //exWorkSheet.Protection.AllowDeleteRows = true;

                    //exWorkSheet.Cells["A:G"].Style.Locked = false;
                    //exWorkSheet.Cells["A:G"].AutoFilter = true;
                    //exWorkSheet.Cells["K:L"].Style.Locked = false;
                    //exWorkSheet.Cells["K:L"].AutoFilter = true;

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditStepExportForUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<AuditStepExportFillView> GetAllAuditStepExport(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditStepExportFillView> AuditStepList = new List<AuditStepExportFillView>();

                if (Flag == "P")
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)                    
                    //    riskcategorycreations=(List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                    entities.Database.CommandTimeout = 300;
                    AuditStepList = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                     where row.CustomerID == Customerid
                                     && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                     select row).ToList();

                    //    HttpContext.Current.Cache.Insert("MasterListAuditSteps", riskcategorycreations,null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);     
                    //}

                    if (BranchID.Count > 0)
                        AuditStepList = AuditStepList.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        AuditStepList = AuditStepList.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        AuditStepList = AuditStepList.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        AuditStepList = AuditStepList
                                                 .Where(entry => entry.ControlNo.Contains(filter)
                                                 || entry.BranchName.Contains(filter)
                                                 || entry.VerticalName.Contains(filter)
                                                 || entry.ActivityTobeDone.Contains(filter)).ToList();
                }
                else
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)
                    //    riskcategorycreations = (List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                    entities.Database.CommandTimeout = 300;
                    AuditStepList = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                     where row.CustomerID == Customerid
                                             && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                     select row).ToList();

                    //    HttpContext.Current.Cache["MasterListAuditSteps"] = riskcategorycreations;
                    //}

                    if (BranchID.Count > 0)
                        AuditStepList = AuditStepList.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        AuditStepList = AuditStepList.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        AuditStepList = AuditStepList.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        AuditStepList = AuditStepList
                                                   .Where(entry => entry.ControlNo.Contains(filter)
                                                   || entry.BranchName.Contains(filter)
                                                   || entry.VerticalName.Contains(filter)
                                                   || entry.ActivityTobeDone.Contains(filter)).ToList();
                }

                if (AuditStepList.Count > 0)
                {
                    AuditStepList = AuditStepList.OrderBy(entry => entry.ControlNo)
                                                                 .ThenBy(entry => entry.ProcessId)
                                                                 .ThenBy(entry => entry.SubProcessId)
                                                                .ThenBy(entry => entry.ActivityTobeDone).ToList();
                }

                return AuditStepList;
            }
        }


        public static List<SP_AdditionalRiskCreationFillView_Result> GetAllRiskControlMatrix(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_AdditionalRiskCreationFillView_Result> riskcategorycreations = new List<SP_AdditionalRiskCreationFillView_Result>();

                if (Flag == "P")
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)                    
                    //    riskcategorycreations=(List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                        entities.Database.CommandTimeout = 300;
                        riskcategorycreations = (from row in entities.SP_AdditionalRiskCreationFillView()
                                                 where row.CustomerID == Customerid
                                                 && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                                 //&& row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();

                    //    HttpContext.Current.Cache.Insert("MasterListAuditSteps", riskcategorycreations,null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);     
                    //}

                    if (BranchID.Count>0)                    
                        riskcategorycreations= riskcategorycreations.Where(entry=> BranchID.Contains((long)entry.BranchId)).ToList();
                    
                    if (ProcessId != -1)                   
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId== ProcessId).ToList();
                    
                    if (SubprocessId != -1)                    
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();                    

                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                 .Where(entry => entry.ControlNo.Contains(filter)
                                                 || entry.BranchName.Contains(filter)
                                                 || entry.VerticalName.Contains(filter)                                                
                                                 ||entry.ActivityTobeDone.Contains(filter)).ToList();
                }
                else
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)
                    //    riskcategorycreations = (List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                        entities.Database.CommandTimeout = 300;
                        riskcategorycreations = (from row in entities.SP_AdditionalRiskCreationFillView()
                                                 where row.CustomerID == Customerid
                                                 && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                                 //&& row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();

                    //    HttpContext.Current.Cache["MasterListAuditSteps"] = riskcategorycreations;
                    //}

                    if (BranchID.Count > 0)                    
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long) entry.BranchId)).ToList();
                    
                    if (ProcessId != -1)                    
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();
                    
                    if (SubprocessId != -1)                    
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                    
                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                   .Where(entry => entry.ControlNo.Contains(filter)
                                                   || entry.BranchName.Contains(filter)
                                                   || entry.VerticalName.Contains(filter)
                                                   || entry.ActivityTobeDone.Contains(filter)).ToList();
                }

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ControlNo)
                                                                 .ThenBy(entry => entry.ProcessId)
                                                                 .ThenBy(entry => entry.SubProcessId)
                                                                .ThenBy(entry => entry.ActivityTobeDone).ToList();
                }

                return riskcategorycreations;
            }
        }

        public static List<SP_TOP100_AdditionalRiskCreationFillView_Result> GetAllRiskControlMatrixFirstTime(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_TOP100_AdditionalRiskCreationFillView_Result> riskcategorycreations = new List<SP_TOP100_AdditionalRiskCreationFillView_Result>();

                if (Flag == "P")
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)                    
                    //    riskcategorycreations=(List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                    entities.Database.CommandTimeout = 300;
                    riskcategorycreations = (from row in entities.SP_TOP100_AdditionalRiskCreationFillView(Customerid)
                                             where  !string.IsNullOrEmpty(row.ActivityTobeDone)
                                             //&& row.IsProcessNonProcess == "P"
                                             orderby row.RiskCreationId
                                             select row).ToList();

                    //    HttpContext.Current.Cache.Insert("MasterListAuditSteps", riskcategorycreations,null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);     
                    //}

                    if (BranchID.Count > 0)
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                 .Where(entry => entry.ControlNo.Contains(filter)
                                                 || entry.BranchName.Contains(filter)
                                                 || entry.VerticalName.Contains(filter)
                                                 || entry.ActivityTobeDone.Contains(filter)).ToList();
                }
                else
                {
                    //if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)
                    //    riskcategorycreations = (List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    //else
                    //{
                    entities.Database.CommandTimeout = 300;
                    riskcategorycreations = (from row in entities.SP_TOP100_AdditionalRiskCreationFillView(Customerid)
                                             where  !string.IsNullOrEmpty(row.ActivityTobeDone)
                                             //&& row.IsProcessNonProcess == "P"
                                             orderby row.RiskCreationId
                                             select row).ToList();

                    //    HttpContext.Current.Cache["MasterListAuditSteps"] = riskcategorycreations;
                    //}

                    if (BranchID.Count > 0)
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                   .Where(entry => entry.ControlNo.Contains(filter)
                                                   || entry.BranchName.Contains(filter)
                                                   || entry.VerticalName.Contains(filter)
                                                   || entry.ActivityTobeDone.Contains(filter)).ToList();
                }

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ControlNo)
                                                                 .ThenBy(entry => entry.ProcessId)
                                                                 .ThenBy(entry => entry.SubProcessId)
                                                                .ThenBy(entry => entry.ActivityTobeDone).ToList();
                }

                return riskcategorycreations;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
            }
            else
            {
                if (ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }                 

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

        }        

        protected void upCompliance_Load(object sender, EventArgs e)
        {           
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {
            
        }

        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            { 
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (commandArgs.Length > 1)
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    ViewState["VerticalId"] = null;
                    Label lblATBD = (Label)grdRiskActivityMatrix.Rows[RowIndex].FindControl("lblActivityToBeDone");
                    ViewState["ATBD"] = lblATBD.Text;
                   
                    Label lblctrlno = (Label)grdRiskActivityMatrix.Rows[RowIndex].FindControl("lblControlNo");
                    ViewState["ControlNo"] = lblctrlno.Text;
                    Label lblVerticalIdItemTemplate = (Label)grdRiskActivityMatrix.Rows[RowIndex].FindControl("lblVerticalIdItemTemplate");
                    ViewState["VerticalId"] = lblVerticalIdItemTemplate.Text;
                    int RiskCreationId = Convert.ToInt32(commandArgs[0]);
                    int RiskActivityId = Convert.ToInt32(commandArgs[1]);
                   
                    Label lblATBDId = (Label)grdRiskActivityMatrix.Rows[RowIndex].FindControl("lblATBDIdItemTemplate");
                    ViewState["RATBDID"] = null;
                    ViewState["RATBDID"] = lblATBDId.Text;

                    Label lblBranchIdItemTemplate = (Label)grdRiskActivityMatrix.Rows[RowIndex].FindControl("lblBranchIdItemTemplate");
                    ViewState["BranchId"] = null;
                    ViewState["BranchId"] = lblBranchIdItemTemplate.Text;
                    

                    ViewState["RiskCreationId"] = RiskCreationId;
                    ViewState["RiskActivityId"] = null;
                    ViewState["RiskActivityId"] = RiskActivityId;

                    RiskCategoryCreation RCC = RiskCategoryManagement.RiskGetByID(RiskCreationId);
                    RiskActivityTransaction RAT = RiskCategoryManagement.RiskActivityTransactionGetByID(RiskActivityId, RiskCreationId);

                    if (RAT != null)
                    {
                        txtCDEC.Text = RAT.ControlDescription;
                    }

                    txtFName.Text = RCC.ActivityDescription;
                    ViewState["ControlDescription"] = RAT.ControlDescription;



                    //if (rdCD.SelectedItem.Text == "Same")
                    //{
                    //    txtCDEC.Enabled = false;
                    //    txtCDEC.Text = ViewState["ControlDescription"].ToString();
                    //}
                    //else if (rdCD.SelectedItem.Text == "New")
                    //{
                    //    txtCDEC.Enabled = true;
                    //    txtATBD.Text = string.Empty;
                    //    //CObj = txtCDEC.Text;
                    //    txtCDEC.Text = "";
                    //}

                    if (e.CommandName.Equals("New_AdditionalRiskCreation"))
                    {
                        rdCD.SelectedValue = "Same";
                        ViewState["Mode"] = 0;
                        txtATBD.Text = string.Empty;
                        txtCDEC.Enabled = false;
                        txtCDEC.Text = ViewState["ControlDescription"].ToString();

                    }
                    if (e.CommandName.Equals("EDIT_AdditionalRiskCreation"))
                    {
                        rdCD.SelectedValue = "Same";
                        ViewState["Mode"] = 1;                       
                        txtATBD.Text = ViewState["ATBD"].ToString();
                        txtCDEC.Enabled = false;
                        txtCDEC.Text = ViewState["ControlDescription"].ToString();
                    }
                    upPromotor.Update();                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdCD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdCD.SelectedItem.Text == "Same")
            {
                txtCDEC.Enabled = false;
                txtCDEC.Text = ViewState["ControlDescription"].ToString();
            }
            else if (rdCD.SelectedItem.Text == "New")
            {
                txtCDEC.Enabled = true;
                txtATBD.Text = string.Empty;                
                //CObj = txtCDEC.Text;
                txtCDEC.Text = "";
            }
        }     
                  
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        } 
             
        protected void grdRiskActivityMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
      
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P");
                }
                else
                {
                    BindData("N");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}       
                        
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")               
                    BindData("P");                
                else                
                    BindData("N");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {               
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }

        //        //Reload the Grid
        //        if (rdRiskActivityProcess.SelectedItem.Text == "Process")                
        //            BindData("P");                
        //        else                
        //            BindData("N");               
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //        {
        //            BindData("P");
        //        }
        //        else
        //        {
        //            BindData("N");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}
    }
}