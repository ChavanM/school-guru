﻿<%@ Page Title="" Language="C#" Async="true" EnableEventValidation="false" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="InternalAuditStatusUI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.InternalAuditStatusUI" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
     <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {                
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ ' + filterbytype);
            }
        });
    </script>
       <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style> 

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusUI" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                           <header class="panel-heading tab-bg-primary">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                            <li class="active" id="liPerformer" runat="server">
                                                <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                            </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                            <li class=""  id="liReviewer" runat="server">
                                                <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                            </li>
                                          <%}%>                                                                                                                  
                                    </ul>
                                </header> 
                             
                                 <div class="clearfix"></div> 


                            <div class="panel-body">

                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" Selected="True"/>
                                    </asp:DropDownList>
                                    </div>  
                                    <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 24%; display:none;" >
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                        RepeatLayout="Flow"> <%--OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged"--%>
                                        <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                        <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                </div>                       
                               

                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit" 
                                          class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div> 
                                      </div>

                                <div class="clearfix"></div>

                                      <div class="col-md-12 colpadding0">                                                           
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit"
                                         class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>

                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Sub Unit"
                                        OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" 
                                             Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div> 
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" DataPlaceHolder="Financial Year"
                                            class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                        OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" DataPlaceHolder="Period"
                                        OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">                                           
                                                <asp:ListItem Value="-1">Period</asp:ListItem>
                                                <asp:ListItem Value="1">Apr - Jun</asp:ListItem>
                                                <asp:ListItem Value="2">Jul - Sep</asp:ListItem>
                                                <asp:ListItem Value="3">Oct - Dec</asp:ListItem>
                                                <asp:ListItem Value="4">Jan - Mar</asp:ListItem>
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>  

                             <div class="clearfix"></div> 
                                                                                                
                                <div style="margin-bottom: 4px">                                       
                                      <asp:GridView runat="server" ID="grdInternalAudits" AutoGenerateColumns="false" 
                                  PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowHeaderWhenEmpty="true"
                                          GridLines="None" Width="100%" AllowSorting="true" ShowFooter="true" 
                                          OnRowCommand="grdInternalAudits_RowCommand"
                                          OnRowDataBound="grdInternalAudits_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("UserName") %>'  data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                         </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>    
                                        
                                        <%-- <asp:TemplateField HeaderText="Reviewer">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("UserName") %>'  data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                         </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>     --%>                                                        
                                     
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>                                            
                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID") %>'
                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" alt="View Audit Details" title="View Audit Details" /></asp:LinkButton> <%--CommandArgument='<%# Eval("InternalAuditInstanceID") %>' --%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" /> 
                                    <PagerSettings Visible="false" />                         
                                    <PagerTemplate>
                                   
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                          <div style="float: right;">
                  
                </div>
                                </div>
                              
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                              <div class="table-paging" style="margin-bottom: 10px;float:right;color: #999;">                               
                                        <p>Page
                                       <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                            class="form-control m-bot15"   Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>  
                                        </p>
                                    </div>
                                                                 
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>           
                       </section>
                    </div>
                </div>
            </div>       
          </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
