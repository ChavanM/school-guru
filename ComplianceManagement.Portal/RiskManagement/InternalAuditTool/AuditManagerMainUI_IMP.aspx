﻿<%@ Page Title="My Workspace" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditManagerMainUI_IMP.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditManagerMainUI_IMP" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');

            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace / Implementation / ' + filterbytype);
                $('#pagetype').css('font-size', '20px');
            }
        });
        function ShowIMPDialog(Resultid, BID, FinYear, ForMonth, SID, VerticalID, scheduledonid, AuditID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditManager.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + Resultid + "&SID=" + SID + "&VID=" + VerticalID + "&scheduledonid=" + scheduledonid + "&AuditID=" + AuditID);
        };

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }


        function BindControls() {
            // JQUERY DATE PICKER.
            var now = new Date();
            var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var diffDays = now.getDate() - firstDayPrevMonth.getDate();
            if (diffDays > 15) {
                var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 1);
            }

            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    minDate: firstDayPrevMonth,
                });
            });

        }
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                                                            
                             <div class="panel-body">
                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                              <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>   
                                       
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                  <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true"
                                CssClass="form-control m-bot15" Visible="false">
                                <asp:ListItem Value="-1">Select Status</asp:ListItem>
                                <asp:ListItem Value="1">Open</asp:ListItem>
                                <asp:ListItem Value="2">Submited</asp:ListItem>
                                <asp:ListItem Value="4">Team Review</asp:ListItem>
                                <asp:ListItem Value="5">Final Review</asp:ListItem>
                                <asp:ListItem Value="3">Closed</asp:ListItem>
                                <asp:ListItem Value="6">Auditee Review</asp:ListItem> 
                                </asp:DropDownList>
                                      </div>    
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"></div>                                                  
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;text-align: right;">
                                    <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                                  </div>
                                  
                                </div>

                                 <div class="clearfix"></div>
                                <div style="margin-bottom: 4px">
                                    &nbsp;
                                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverageIMP" AutoGenerateColumns="false" 
                                        AllowSorting="true" CssClass="table" GridLines="None" AllowPaging="true" PageSize="5" Width="100%" 
                                        OnRowDataBound="grdSummaryDetailsAuditCoverageIMP_RowDataBound" ShowHeaderWhenEmpty="true" DataKeyNames="ResultID"> 
                                    <Columns>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" onclick = "Check_Click(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField  Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResultID" runat="server" Text='<%# Eval("ResultID") %>'></asp:Label>
                                            <asp:Label ID="lblVerticalId" runat="server" Text='<%# Eval("VerticalId") %>'></asp:Label>
                                            <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                            <asp:Label ID="lblCustomerBranchID" runat="server" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                            <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observation" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                <asp:Label ID="lblObservation" runat="server" Text='<%# Eval("Observation") %>' ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    <asp:TemplateField HeaderText="Management Response" ItemStyle-Width="20%">
                                        <ItemTemplate>                                            
                                            <asp:TextBox ID="tbxMgmResponce" runat="server" class="form-control"                                            
                                            Width="150px"></asp:TextBox>                                            
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Person Responsible" ItemStyle-Width="20%">
                                        <ItemTemplate>                                        
                                            <asp:DropDownList runat="server" ID="ddlPersonResponsible"                                            
                                            class="form-control m-bot15" Style="width: 150px;">                                                                               
                                            </asp:DropDownList>                                        
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="TimeLine" ItemStyle-Width="20%">
                                        <ItemTemplate>                                            
                                                <asp:TextBox ID="txtStartDate" class="form-control" style="width: 120px; text-align: center;" runat="server"></asp:TextBox>                                            
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="20%">
                                        <ItemTemplate>                                                                        
                                            <asp:DropDownList runat="server" ID="ddlIMPStatus" class="form-control m-bot15" Style="width: 150px;">    
                                            <asp:ListItem Value="-1">Select Status</asp:ListItem>
                                            <asp:ListItem Value="2">Due & Partial Implemented</asp:ListItem>
                                            <asp:ListItem Value="3">Due But Not Implemented</asp:ListItem>
                                            <asp:ListItem Value="4">Not Feasible</asp:ListItem>
                                            <asp:ListItem Value="5">Due & Closed</asp:ListItem>                                                                           
                                            </asp:DropDownList>                                    
                                        </ItemTemplate>
                                    </asp:TemplateField>                                                                                                                                
                                    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnChangeStatus" runat="server" OnClick="btnChangeStatusIMP_Click"
                                            CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID")+","+Eval("VerticalId")+","+Eval("ScheduledOnID")+","+Eval("AuditID")  %>'>                                                         
                                            <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerSettings Visible="false" />                            
                                    <PagerTemplate>                                    
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                    No Records Found.
                                    </EmptyDataTemplate>                                         
                                    </asp:GridView>
                                    <div style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>  
                                    </div>                     
                                </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0">
                                        <div class="table-Selecteddownload">
                                            <div class="table-Selecteddownload-text">
                                                <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                            </div>                                   
                                        </div>
                                    </div>
                                    <div class="col-md-6 colpadding0" style="float:right;">
                                        <div class="table-paging" style="margin-bottom: 10px;">                                    
                                            <div class="table-paging-text" style="float: right;">
                                                <p>Page                                           
                                                </p>
                                            </div>                                    
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                         </div>
                                    </div>
                                </div> 
                                 <div>
                                    <asp:LinkButton runat="server" ID="lbkSave" Text="Save" CssClass="btn btn-primary" style="float: left;display:none;" OnClick="lbkSave_Click"/>                                    
                                        <asp:DropDownList runat="server" ID="ddlSaveStatus" AutoPostBack="true"
                                        CssClass="form-control m-bot15" Style="width: 160px;margin-top: -33px;margin-left: 69px;display:none;" >                             
                                        </asp:DropDownList>
                                </div>                                         
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe id="showdetails" src="about:blank" width="1100px" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
