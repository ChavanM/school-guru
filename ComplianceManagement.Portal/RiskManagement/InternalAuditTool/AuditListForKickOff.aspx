﻿<%@ Page Title="Audit Kickoff" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditListForKickOff.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditListForKickOff" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            /*height: 3px !important;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .table>tbody>tr>th, .table>tbody>tr>th>a
        {
                border-bottom: 6px solid #ddd;
        }
        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding-bottom: 8px;
            padding-left:unset;
            padding-right:unset;
            padding-top:8px;
        }
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
        .Check {
            width: 119px !important;
            height: 34px !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            fhead('Audit Kickoff (List of Audits)');
        });

        function openModal() {
            $('#divReAssign').modal('show');
            return true;
        }

        function closeModal() {
            $('#divReAssign').modal('hide');
            window.location.reload();
        };

        function ConfirmEvent() {

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_Event_value";
            var oldUser = $("#<%=ddlUser.ClientID%> option:selected").text();
            var newUser = $("#<%=ddlREAssignUser.ClientID%> option:selected").text();
            var type = $("#<%=rbtEventReAssignment.ClientID%> input:checked").val();

            if (type == "Process") {
                if (newUser != "Select User") {
                    if (confirm("Are you sure you want re-assign selected Process Audits of " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
            } else {
                if (confirm("Are you sure you want re-assign selected Implementation Audits of " + oldUser + "?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
            }

            document.forms[0].appendChild(confirm_value);
        }

        function CloseModelDelete() {
            $('#ShowDeleteModel').modal('hide');
            location.reload();
        }

        function OpenModelDeleteAssignment() {
            $('#ShowDeleteModel').modal('show');
            $('#ContentPlaceHolder1_IframeDeleteAssignment').attr('src', "../../RiskManagement/InternalAuditTool/DeleteKickOff.aspx");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upAuditKickoffList">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upAuditKickoffList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <%--<div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">--%>
                        <section class="panel">
                             <header class="panel-heading tab-bg-primary">
                                 <div class="col-md-12 colpadding0" >                               
                                     <div class="col-md-6 colpadding0" style="text-align: right;">
                                     </div>
                                    <div class="col-md-6 colpadding0" style="">
                                        <div style="width:95%;text-align: right;">
                                        <asp:Button Text="Re-Assign" runat="server" ID="btnReAssign" OnClick="btnReAssign_Click" CssClass="btn btn-advanceSearch"/>
                                        <asp:Button ID="btnDeleteAssigned" runat="server" Text="Delete Assigment" Width="130px" CssClass="btn btn-primary" OnClientClick="OpenModelDeleteAssignment();"/>             
                                        </div>
                                </div>
                             </header>  
                            
                            <div class="clearfix" style="margin-bottom:15px;"></div>
                                           
                            <div class="col-md-12 colpadding0">
                                 <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in" 
                                     ValidationGroup="ComplianceValidationGroup" /> 
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>

                            <div class="clearfix"></div>

                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">
                                    <div class="col-md-2 colpadding0" style="margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px;margin-right: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />                                        
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" 
                                        Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                     DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Branch." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator1"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                    
                                </div>

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15"
                                         Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                     DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>

                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" 
                                        Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                      DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Unit." ControlToValidate="ddlSubEntity2" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>

                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px; width:20%">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" 
                                         class="form-control m-bot15" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                       DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>
                              </div>

                             <div class="clearfix"></div> 

                             <div class="col-md-12 colpadding0">                             
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" 
                                        class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>                                  
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" 
                                        Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                  DataPlaceHolder="Financial Year"  AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                                   <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                   <%{%>                                  
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Vertical" AutoPostBack="true" 
                                            OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged" class="form-control m-bot15 select_location"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                       </div>
                                  <%}%>
                                 <div class="col-md-3 colpadding0 entrycount"style="margin-top: 5px;width:20%;display:none;">                             
                                    <asp:DropDownListChosen runat="server" ID="ddlPerformerFilter" class="form-control m-bot15" 
                                         Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" DataPlaceHolder="Performer" OnSelectedIndexChanged="ddlPerformerFilter_SelectedIndexChanged">
                                    </asp:DropDownListChosen>  
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Performer"
                                    ControlToValidate="ddlPerformerFilter" runat="server" ValidationGroup="ComplianceValidationGroup1"
                                    Display="None"/>  
                                    <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Performer"
                                            ControlToValidate="ddlPerformerFilter" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                            ValidationGroup="ComplianceValidationGroup1" Display="None" />                     
                                </div> 

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;display:none;">                               
                                    <asp:DropDownListChosen runat="server" ID="ddlReviewerFilter" class="form-control m-bot15"  Width="90%" Height="32px" 
                                     AutoPostBack="true" DataPlaceHolder="Reviewer" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                         OnSelectedIndexChanged="ddlReviewerFilter_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Reviewer"
                                        ControlToValidate="ddlReviewerFilter" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None"/>
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Reviewer"
                                    ControlToValidate="ddlReviewerFilter" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />                         
                                </div>

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;display:none;">                               
                                    <asp:DropDownListChosen runat="server" ID="ddlReviewerFilter2" class="form-control m-bot15"  Width="90%" Height="32px" 
                                     AutoPostBack="true" DataPlaceHolder="Reviewer2" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                         OnSelectedIndexChanged="ddlReviewerFilter2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Please Select Reviewer2"
                                        ControlToValidate="ddlReviewerFilter2" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None"/>
                                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Reviewer2"
                                    ControlToValidate="ddlReviewerFilter2" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />                         
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">                                                                                             
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">                                                                                        
                                </div>
                            </div>                                                                                                                              
                                
                            <div class="clearfix"></div> 
                            
                            <div class="col-md-12 colpadding0" style="margin-top: 5px;">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"> 
                                    </div> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"> 
                                    </div> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;"> 
                                    <asp:DropDownListChosen runat="server" ID="ddlInternalExternal"  Width="90%" Height="32px" class="form-control m-bot15"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlInternalExternal_SelectedIndexChanged" 
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        DataPlaceHolder="Internal / External">                                                 
                                    <asp:ListItem Text="Internal">Internal</asp:ListItem>
                                    <asp:ListItem Text="External">External</asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div> 
                                  
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">                                                                
                                    <asp:DropDownListChosen runat="server" ID="ddlAuditor" class="form-control m-bot15" 
                                         Width="90%" Height="32px" Enabled="false"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged" DataPlaceHolder="Auditor">
                                    </asp:DropDownListChosen>                                                         
                                </div> 
                            </div>
                            
                            <div class="clearfix"></div>                                                        
                        
                            <div id="dvProcessDetails" runat="server" style="margin-bottom: 4px;margin-top: 10px;">
                            <asp:GridView runat="server" ID="grdProcessAudit" AutoGenerateColumns="false" GridLines="None" PageSize="20" 
                                AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" OnRowDataBound="grdProcessAudit_RowDataBound"
                                OnRowCommand="grdProcessAudit_RowCommand" ShowHeaderWhenEmpty="true">
                                <Columns>          
                                    <asp:TemplateField HeaderText="AuditID" Visible="false">
                                        <ItemTemplate>                                        
                                            <asp:Label runat="server" ID="lblAuditID"  Style="display:none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblVerticalName" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("VerticalName") %>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblVerticalId"  Style="display:none;" Text='<%#Eval("VerticalId") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblFinancialYear" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                            <asp:Label  runat="server" ID="lblTermName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("TermName") %>' ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblLocation" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblLocationId"  Style="display:none;" Text='<%#Eval("CustomerBranchId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    <asp:TemplateField HeaderText="Audit Name">
                                        <ItemTemplate>
                                              <div class="text_NlinesusingCSS" style="width: 130px;">
                                                <asp:Label  runat="server" ID="lblAuditName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditName") %>' ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                   <div class="text_NlinesusingCSS" style="width: 100px;">
                                                        <asp:Label runat="server" ID="lblProcessName" data-toggle="tooltip" data-placement="top" Text='<%# GetProcessName(Convert.ToInt32(Eval("AuditID"))) %>' ToolTip='<%# GetProcessName(Convert.ToInt32(Eval("AuditID"))) %>'></asp:Label>
                                                        <%--<asp:Label runat="server" ID="lblProcessId" Style="display: none;" Text='<%#Eval("ProcessId") %>'></asp:Label>--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>   
                                    <asp:TemplateField HeaderText="SubProcess">
                                                <ItemTemplate>
                                                   <div class="text_NlinesusingCSS" style="width: 100px;">
                                                        <asp:Label runat="server" ID="lblSubProcessName" data-toggle="tooltip" data-placement="top" Text='<%# GetSubProcessName(Convert.ToInt32(Eval("AuditID"))) %>' ToolTip='<%# GetSubProcessName(Convert.ToInt32(Eval("AuditID"))) %>'></asp:Label>
                                                       <%-- <asp:Label runat="server" ID="lblSubProcessId" Style="display: none;" Text='<%#Eval("SubProcessId") %>'></asp:Label>--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Audit" HeaderStyle-CssClass="text-center">
                                        <HeaderTemplate>
                                             <div>Audit</div>
                                              <div style="clear:both;height:5px;"></div>
                                             <div style="text-overflow: ellipsis; white-space: nowrap; width: 380px;">
                                                <div class="col-md-12" style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                            <asp:DropDownList ID="ddlPerformerAuditHeader" class="form-control m-bot15" Width="120px"
                                                DataPlaceHolder="Performer" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPerformerAuditHeader_SelectedIndexChanged"></asp:DropDownList> 
                                                        <%--<asp1:DropDownCheckBoxes ID="ddlPerformerAuditHeader" runat="server" AutoPostBack="true" Visible="true" OnSelectedIndexChanged="ddlPerformerAuditHeader_SelectedIndexChanged"
                                                    CssClass="form-control m-bot15 Check" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                            </asp1:DropDownCheckBoxes>--%>
                                                <asp:Label ID="lblPerformerNameAuditHeader" runat="server" Visible="false"></asp:Label>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewerAuditHeader" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer" AutoPostBack="true" OnSelectedIndexChanged="ddlReviewerAuditHeader_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:Label ID="lblReviewerNameAuditHeader"  runat="server" Visible="false"></asp:Label>
                                               </div>
                                                    <div class="col-md-4"> <asp:DropDownList ID="ddlReviewer2AuditHeader" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2" AutoPostBack="true" OnSelectedIndexChanged="ddlReviewer2AuditHeader_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:Label ID="lblReviewerName2AuditHeader" runat="server" Visible="false" ></asp:Label>
                                                </div>
                                                </div>
                                                </div>
                                         </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-overflow: ellipsis; white-space: nowrap; width: 380px;">
                                                <div class="col-md-12" style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                            <asp:DropDownList ID="ddlPerformer" class="form-control m-bot15" Width="120px"
                                                DataPlaceHolder="Performer" runat="server"></asp:DropDownList> 
                                                        <%--<asp1:DropDownCheckBoxes ID="ddlPerformer" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15 Check" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                            </asp1:DropDownCheckBoxes>--%>
                                                <%--<asp:Label ID="lblPerformerName" runat="server" Visible="false"></asp:Label>--%>
                                                        <asp:TextBox ID="lblPerformerName" TextMode="MultiLine" CssClass="text_NlinesusingCSS" style="width:121px; border:none"  runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewer" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer"></asp:DropDownList>
                                                <%--<asp:Label ID="lblReviewerName" runat="server" Visible="false"></asp:Label>--%>
                                                        <asp:TextBox ID="lblReviewerName" TextMode="MultiLine" CssClass="text_NlinesusingCSS" style="width:121px; border:none" runat="server" Visible="false"></asp:TextBox>
                                               </div>
                                                    <div class="col-md-4"> <asp:DropDownList ID="ddlReviewer2" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2"></asp:DropDownList>
                                            <%--<asp:Label ID="lblReviewerName2" runat="server" Visible="false"></asp:Label>--%>
                                                        <asp:TextBox ID="lblReviewerName2" TextMode="MultiLine" CssClass="text_NlinesusingCSS" style="width:121px; border:none" runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                                </div>
                                                </div>
                                             </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField  HeaderText="Implementation Review" HeaderStyle-CssClass="text-center">
                                          <HeaderTemplate >
                                              <div>Implementation Review</div>
                                              <div style="clear:both;height:5px;"></div>
                                             <div style="text-overflow: ellipsis; white-space: nowrap; width: 380px;">
                                                <div class="col-md-12" style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                            <asp:DropDownList ID="ddlPerformerImplemetationHeader" class="form-control m-bot15" Width="120px"
                                                DataPlaceHolder="Performer" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPerformerImplemetationHeader_SelectedIndexChanged"></asp:DropDownList> 
                                                        <%--<asp1:DropDownCheckBoxes ID="ddlPerformerImplemetationHeader" runat="server" AutoPostBack="true" Visible="true" OnSelectedIndexChanged="ddlPerformerImplemetationHeader_SelectedIndexChanged"
                                                    CssClass="form-control m-bot15 Check" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                            </asp1:DropDownCheckBoxes>--%>
                                                <asp:Label ID="lblPerformerNameImplemetationHeader" CssClass="text_NlinesusingCSS" runat="server" Visible="false"></asp:Label>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewerImplemetationHeader" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer" AutoPostBack="true"  OnSelectedIndexChanged="ddlReviewerImplemetationHeader_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:Label ID="lblReviewerNameImplemetationHeader" CssClass="text_NlinesusingCSS" runat="server" Visible="false"></asp:Label>
                                               </div>
                                                    <div class="col-md-4"> <asp:DropDownList ID="ddlReviewer2ImplemetationHeader" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2" AutoPostBack="true"  OnSelectedIndexChanged="ddlReviewer2ImplemetationHeader_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:Label ID="lblReviewerName2ImplemetationHeader" CssClass="text_NlinesusingCSS" runat="server" Visible="false"></asp:Label>
                                                </div>
                                                </div>
                                                </div>
  
                                         </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-overflow: ellipsis; white-space: nowrap; width: 380px;">
                                            <div class="col-md-12"  style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPerformerIMP" class="form-control m-bot15" Width="120px"
                                                DataPlaceHolder="Performer" runat="server"></asp:DropDownList> 
                                                        <%--<asp1:DropDownCheckBoxes ID="ddlPerformerIMP" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15 Check" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                            </asp1:DropDownCheckBoxes>--%>
                                                <%--<asp:Label ID="lblPerformerNameIMP" CssClass="text_NlinesusingCSS"  runat="server" Visible="false"></asp:Label>--%>
                                                        <asp:TextBox ID="lblPerformerNameIMP" CssClass="text_NlinesusingCSS" TextMode="MultiLine" style="width:121px; border:none" runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewerIMP" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer"></asp:DropDownList>
                                                <%--<asp:Label ID="lblReviewerNameIMP" CssClass="text_NlinesusingCSS" runat="server" Visible="false"></asp:Label>--%>
                                                        <asp:TextBox ID="lblReviewerNameIMP" CssClass="text_NlinesusingCSS" TextMode="MultiLine"  style="width:121px; border:none" runat="server" Visible="false"></asp:TextBox>
                                               </div>
                                                    <div class="col-md-4">
                                                         <asp:DropDownList ID="ddlReviewerIMP2" runat="server" Width="120px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2"></asp:DropDownList>
                                            <asp:TextBox ID="lblReviewerNameIMP2" CssClass="text_NlinesusingCSS" TextMode="MultiLine" style="width:121px; border:none" runat="server" Visible="false"></asp:TextBox>
                                                </div> </div> </div>                                 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField ItemStyle-Width="8%" HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("CustomerBranchId") +"," +Eval("VerticalID")+"," + Eval("FinancialYear")+"," +  Eval("TermName")+","+ Eval("ISAHQMP") +","+Eval("AuditID") %>'>
                                                <img src="../../Images/view-icon-new.png" data-toggle="tooltip" data-placement="top"  title="View Process Wise Details" /></asp:LinkButton>                                           
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                            <div class="clearfix"></div> 
                             <div class="clearfix"></div>
                              <div style="float: right;">
                                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                  </asp:DropDownListChosen>  
                                </div>

                              <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0" style="text-align: right;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" ValidationGroup="ComplianceValidationGroup" CssClass="btn btn-primary"/>
                                </div>                            
                                <div class="col-md-5 colpadding0" style="float: right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                        
                                        <div class="table-paging-text" style="float: right;">
                                            <p>Page
                                             
                                            </p>
                                        </div>                                       
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       </section>
                    <%--</div>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divReAssign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 1150px; height: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">×</button>
                </div>

                <div class="modal-body">
                    <asp:UpdatePanel ID="ReAssign" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 ">
                                <section class="panel">  
                                    <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="VSReassignSummary" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ReAssignValidationGroup" />
                                        <asp:CustomValidator ID="cvReAssignAudit" runat="server" EnableClientScript="False" 
                                            ValidationGroup="ReAssignValidationGroup" Display="None" />                          
                                    </div>

                                   
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 colpadding0">                                   
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>

                                            <asp:DropDownList runat="server" ID="ddlPageSizeReAssign" class="form-control m-bot15" Style="width: 70px; float: left"
                                                OnSelectedIndexChanged="ddlPageSizeReAssign_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="5" Selected="True"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                            </asp:DropDownList> 
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;"> 
                                            <asp:DropDownListChosen runat="server" ID="ddlUser" Width="90%" Height="32px" AutoPostBack="true"
                                                 DataPlaceHolder="User" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" CssClass="form-control m-bot15" />
                                            <asp:Label ID="lblReAssign" Visible="false" runat="server" style="color: #333;"/>
                                        </div>

                                         <div class="col-md-6 colpadding0">
                                              <asp:RadioButtonList ID="rbtEventReAssignment" runat="server" AutoPostBack="true" 
                                                 OnSelectedIndexChanged="rbtEventReAssignment_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Table">
                                                <asp:ListItem Text="Audit" Value="Audit" Selected="True" style="margin: 5px; margin-left: 0.5em;"></asp:ListItem>
                                                <asp:ListItem Text="Implementation" Value="Implementation"  style="margin: 5px; margin-left: 0.5em;"></asp:ListItem>                                                
                                                </asp:RadioButtonList>
                                             </div>

                                    </div>

                                    
                                         <div class="clearfix"></div>

                                         <div class="col-md-12 colpadding0">                                       

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">  
                                            <asp:DropDownListChosen ID="ddlLegalEntityPopup" runat="server" AutoPostBack="true"
                                                 class="form-control m-bot15" Width="90%"  AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                Height="32px" DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntityPopUp_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator4"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />                                    
                                        </div>

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen ID="ddlSubEntity1Popup" runat="server" AutoPostBack="true"
                                                 class="form-control m-bot15" Width="90%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator5"
                                                runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />
                                        </div>

                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen ID="ddlSubEntity2Popup" runat="server" AutoPostBack="true" 
                                                class="form-control m-bot15" Width="90%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                              DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity2Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Unit." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator6"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />  
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                             <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Popup" AutoPostBack="true"
                                                  class="form-control m-bot15" Width="90%" Height="32px"
                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"
                                               DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3Popup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                         </div>
                            
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLocationPopup" AutoPostBack="true"
                                                 class="form-control m-bot15" Width="90%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlLocationPopup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>                                  
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFinYearPopup" class="form-control m-bot15"
                                                 Width="90%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                          DataPlaceHolder="Financial Year"  AutoPostBack="true" OnSelectedIndexChanged="ddlFinYearPopup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                       <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                       <%{%>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                                <asp:DropDownListChosen runat="server" ID="ddlVerticalPopup" DataPlaceHolder="Vertical" 
                                                    AutoPostBack="true"  AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    OnSelectedIndexChanged="ddlVerticalPopup_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>
                                            </div>                                        
                                        <%}%>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;"> 
                                            <asp:DropDownListChosen runat="server" ID="ddlRole"  Width="90%" Height="32px"
                                                 class="form-control m-bot15" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" DataPlaceHolder="Select Role" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">                                                 
                                            <asp:ListItem Text="Performer" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Reviewer" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Reviewer2" Value="5"></asp:ListItem>
                                            </asp:DropDownListChosen> 
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;"> 
                                            <asp:DropDownListChosen runat="server" ID="ddlAuditStatus" Width="90%" Height="32px" 
                                                class="form-control m-bot15" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" DataPlaceHolder="Select Role" OnSelectedIndexChanged="ddlAuditStatus_SelectedIndexChanged">                                                 
                                            <asp:ListItem Text="Open Audits" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Close Audits" Value="3"></asp:ListItem>
                                            </asp:DropDownListChosen> 
                                        </div>
                                             
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                            
                                            <asp:DropDownListChosen runat="server" ID="ddlREAssignUser" Width="90%" AutoPostBack="true"
                                            CssClass="form-control m-bot15" DataPlaceHolder="New User to Assign"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                 OnSelectedIndexChanged="ddlREAssignUser_SelectedIndexChanged"/>
                                            <asp:CompareValidator  ErrorMessage="Please Select New User to Assign." ControlToValidate="ddlREAssignUser"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ReAssignValidationGroup"
                                            Display="None" />
                                         </div> 
                                             
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"> 
                                             <asp:TextBox runat="server" ID="txtReAssignFilter" Width="90%" Height="32px" AutoPostBack="true" 
                                                 CssClass="form-control" OnTextChanged="txtReAssignFilter_TextChanged" Visible="false"/>
                                         </div>  
                                             
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                             <asp:DropDownList runat="server" ID="ddlProcess" Width="90%" Height="32px" AutoPostBack="true" DataPlaceHolder="Process"
                                                 OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" CssClass="form-control m-bot15" Visible="false"/>
                                        </div>                                       
                                   </div>
                            </div>


                            <div runat="server" id="div2" style="margin-bottom: 7px; width: 100%; height: 300px; overflow-y: auto;">
                                <asp:GridView runat="server" ID="grdReassign" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" ShowHeaderWhenEmpty="true"
                                    GridLines="None" AllowSorting="true" Width="100%" CssClass="table" ShowFooter="false" OnRowDataBound="grdReassign_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CustomerBranchID" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label ID="lblBranchID" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBranchID") %>' ToolTip='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                    <asp:Label ID="lblAuditID" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditID") %>' ToolTip='<%# Eval("AuditID") %>'></asp:Label>
                                                    <asp:Label ID="lblProcessId" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessId") %>' ToolTip='<%# Eval("ProcessId") %>'></asp:Label>
                                                    <asp:Label ID="lblSubProcessId" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcessId") %>' ToolTip='<%# Eval("SubProcessId") %>'></asp:Label>
                                                    <asp:Label ID="lblATBDID" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ATBDID") %>' ToolTip='<%# Eval("ATBDID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblVertical" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top"
                                                        ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblVerticalId" Style="display: none;" Text='<%#Eval("VerticalId") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                    <asp:Label ID="lblFY" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>'
                                                        ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                    <asp:Label ID="lblPeriod" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ForMonth") %>'
                                                        ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                    <asp:Label ID="lblRole" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Role") %>'
                                                        ToolTip='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label ID="lblprocess" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SubProcess">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label ID="lblSubprocess" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="New User">
                                            <ItemTemplate>
                                                <asp:DropDownListChosen runat="server" ID="ddlGridReAssignUser" Width="90%" AutoPostBack="false"
                                                    CssClass="form-control m-bot15" DataPlaceHolder="New User to Assign" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                     <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>


                            <div class="clearfix"></div>

                            <div id="divSavePageSummary" runat="server" style="display: none;">
                                <div class="col-md-12 colpadding0" style="margin: 5px; margin-bottom: 40px;">
                                    <div class="col-md-6 colpadding0" style="margin: auto; text-align: right;">
                                        <asp:Button Text="Save" runat="server" ID="btnReassignEvent" OnClick="btnReassignAuditSave_Click"
                                            CssClass="btn btn-primary" ValidationGroup="ReAssignValidationSummary" OnClientClick="ConfirmEvent();" />
                                        <asp:Button Text="Close" runat="server" ID="btnReassignClose" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="closeModal()" />
                                    </div>

                                    <div class="col-md-6 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:ImageButton ID="lBPreviousReAssign" CssClass="table-paging-left" runat="server"
                                                ImageUrl="~/img/paging-left-active.png" OnClick="lBPreviousReAssign_Click" />
                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNoReAssign" runat="server" Text=""></asp:Label>/
                                                     <asp:Label ID="lTotalCountReAssign" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNextReAssign" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png"
                                                OnClick="lBNextReAssign_Click" />
                                            <asp:HiddenField ID="TotalRowsReAssign" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </section>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ShowDeleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 85%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Delete KickOff</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDeleteAssignment" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
