﻿<%@ Page Title="Audit Closure" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditClosure_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditClosure_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 0px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        div.dd_chk_drop {
            top: 29px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .dot {
            height: 25px;
            width: 25px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .dd_chk_select {
            height: 30px !important;
            text-align: center;
            border-radius: 5px;
        }

        div.dd_chk_select div#caption {
            margin-top: 5px;
        }

        td.locked, th.locked {
            position: relative;
            left: expression((this.parentElement.parentElement.parentElement.parentElement.scrollLeft-2)+'px');
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Report Generation');
        });
    </script>

    <script type="text/javascript">
        function Confirm() {

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to save data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <script type="text/javascript">

        function fvalidateProcess() {

            for (var i = 0; i < $('.ProcessSequence').length ; i++) {
                var ctrl = $('.ProcessSequence')[i];
                if ($(ctrl).val() == "") {
                    alert("Process Order can not be empty.");
                    $(ctrl).focus();
                    return false;
                }
            }

            for (var i = 0; i < $('.ObservationNumbers').length ; i++) {
                var ctrl = $('.ObservationNumbers')[i];
                if ($(ctrl).val() == "") {
                    alert("Observation Number can not be empty.");
                    $(ctrl).focus();
                    return false;
                }
            }
            //var retVal = confirm(" Alert: Before audit closure, if you wish to edit/delete the observation, please go to 'Draft obervation' button otherwise it will be disable.");
            //if (retVal == true) {
            //    return true;
            //} else {
            //    return false;
            //}
            //getConfirmation();
            //confirm(" Alert: Before audit closure, if you wish to edit/delete the observation, please go to 'Draft obervation' button otherwise it will be disable.");
            return true;
        }

        function getConfirmation() {
            var totalvalue = $('#ContentPlaceHolder1_HiddenObjValue').attr("value");
            if (totalvalue == '0') {
                var retVal = confirm(" Alert: Before audit closure, if you wish to edit/delete the observation, please go to 'Draft obervation' button otherwise it will be disable.");
                if (retVal == true) {
                    $("#ContentPlaceHolder1_HiddenObjValue").val("2");
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                                                       
                            <header class="panel-heading tab-bg-primary">
                                <ul id="rblRole1" class="nav nav-tabs">
                                    <li class="dummyval">
                                        <asp:LinkButton ID="LBTAuditDetails" OnClick="LBTAuditDetails_Click"   runat="server">Audit Details</asp:LinkButton>
                                    </li>
                                    <li class="dummyval">
                                        <asp:LinkButton ID="LBTObservationDetails" OnClick="LBTObservationDetails_Click" runat="server">Observation</asp:LinkButton>
                                    </li>                                                             
                                </ul>
                            </header>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>                                                                                    
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />                                  
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                                            
                            <div class="tab-content" style="margin-top:10px;">
                                <div runat="server" id="dvAuditDetails" class="tab-pane active">
                                    <div class="col-md-12 colpadding0">
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <div class="col-md-2 colpadding0">
                                                <p style="color: #999; margin-top: 5px;">Show </p>
                                            </div>
                                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                                <asp:ListItem Text="50" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="5" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            </div> 
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            </div> 
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float:right">                                      
                                            </div>  
                                    </div>
                                    <div class="clearfix"></div> 
                                    <div class="col-md-12 colpadding0"> 
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                            <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                            DataPlaceHolder="Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                                                               
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                            <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                            DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                           
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                            <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                             DataPlaceHolder="Sub Unit"  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                       
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                             DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>                  
                                    <div class="col-md-12 colpadding0"> 
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged"  DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>                                  
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Financial Year"  AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div> 
                                           <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                          <%{%> 
                                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                                <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true" 
                                               AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged"
                                                class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>
                                             </div>
                                          <%}%> 
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                            <asp:DropDownListChosen runat="server" ID="ddlPeriod" DataPlaceHolder="Period" AutoPostBack="true" 
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                            class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                         </div> 
                                    </div>                                                                                                                              
                                    <div class="clearfix"></div>                                                           
                                    <div class="clearfix"></div>
                                    <div style="margin-bottom: 4px">
                                        &nbsp;
                                    <asp:GridView runat="server" ID="grdAuditScheduling" AutoGenerateColumns="false" 
                                        OnRowDataBound="grdAuditScheduling_RowDataBound" 
                                        ShowHeaderWhenEmpty="true"
                                    OnSorting="grdAuditScheduling_Sorting" GridLines="None"  PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdAuditScheduling_PageIndexChanging" OnRowCommand="grdAuditScheduling_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>                                           
                                                <asp:Label ID="lblLocationID" runat="server" Text='<%# Eval("BranchId") %>'></asp:Label>                                        
                                                <asp:Label ID="lblVerticalID" runat="server" Text='<%# Eval("VerticalID") %>'></asp:Label>    
                                                <asp:Label ID="lblPStatus" runat="server" Text='<%# Eval("Status") %>' Width="25px" Height="23px"></asp:Label>   
                                                <asp:Label ID="lblAuditId" runat="server" Text='<%# Eval("AuditID") %>' Width="25px" Height="23px"></asp:Label>
                                                <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>' Width="25px" Height="23px"></asp:Label>                                                                                     
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                                      
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">       
                                                <asp:Label ID="lblLocation" data-toggle="tooltip" data-placement="top" runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>                                        
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Vertical">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:Label ID="lblVertical" data-toggle="tooltip" data-placement="top" runat="server" Text='<%# Eval("VerticalName") %>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">                                              
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">                                              
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">                                              
                                                <asp:Label runat="server" Text='<%# Eval("Total") %>' ></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closed">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">                                              
                                                <asp:Label runat="server" Text='<%# Eval("Closed") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closure Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsClosed" runat="server" Text="" Width="25px" Height="23px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text="" Width="25px" Height="25px" style="border-radius:50px">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>                                               
                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditDetial" CommandArgument='<%# Eval("AuditID")  + "," + Eval("BranchId") + "," + Eval("ForMonth")+ "," + Eval("VerticalName")%>'
                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" alt="View Audit Details" data-toggle="tooltip" data-placement="top" title="View Audit Details" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                                                      
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                        <HeaderStyle BackColor="#ECF0F1" />

                                    <PagerSettings Visible="false" />       
                                    <PagerTemplate>                             
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                    <div style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>  
                                    </div>
                                    </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0" style="text-align:right;">
                                           <asp:LinkButton Text="Next" runat="server" ID="btnAddCompliance" CssClass="btn btn-primary" 
                                        OnClick="btnAddCompliance_Click" Visible= "false" />
                                        </div>
                                        <div class="col-md-6 colpadding0" style="float: right;">
                                            <div class="table-paging" style="margin-bottom: 10px;">
                                                <div class="table-paging-text" style="float: right;">
                                                    <p></p>
                                                </div>                                    
                                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float:right">
                                        
                                    </div>  
                                </div>

                                <div runat="server" id="dvObservationDetails" class="tab-pane">   
                                    
                                <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ObservationValidationGroup" />                                  
                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                ValidationGroup="ObservationValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                </div>
                                                                      
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <div class="col-md-2 colpadding0">
                                                <p style="color: #999; margin-top: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSizeObservation" class="form-control m-bot15" Style="width: 70px; float: left"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSizeObservation_SelectedIndexChanged" >                        
                                            <asp:ListItem Text="50" Selected="True" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="5" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-6 colpadding0 entrycount" style="text-align: Left; margin-top: 10px; margin-left: 0%; font-weight: bold; color: teal;">
                                            <asp:Label ID="lblObservationSelectionDetails" runat="server" Text=""></asp:Label>
                                        </div> 
                                        <div class="col-md-2" style="float:right">
                                            <asp:Button runat="server" ID="lbtObservationList" Visible="false" Text="Draft Observation Listing" class="btn btn-primary" style="width: 190px;" OnClick="lbtObservationList_Click"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>                                                                                                          
                                    <div class="clearfix"></div>
                                    <div style="margin-bottom: 4px">
                                        <asp:GridView runat="server" ID="GridObservationDetails" AutoGenerateColumns="false" 
                                             ShowHeaderWhenEmpty="true" OnRowDataBound="GridObservationDetails_RowDataBound"
                                        GridLines="None" PageSize="50" AllowPaging="true" CssClass="table" Width="100%">
                                        <Columns>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField> 

                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>                                                      
                                                     <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessId") %>' Width="25px" Height="23px"></asp:Label>   
                                                     <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>' Width="25px" Height="23px"></asp:Label>
                                                    <asp:Label ID="lblATBDID" runat="server" Text='<%# Eval("ATBDID") %>'></asp:Label>                                                                                        
                                                    <asp:Label ID="lblObservationCategoryID" runat="server" Text='<%# Eval("ObservationCategory") %>'></asp:Label>                                        
                                                    <asp:Label ID="lblObservationSubCategoryID" runat="server" Text='<%# Eval("ObservationSubCategory") %>'></asp:Label>                                                       
                                                    <asp:Label ID="lblProcessRatingName" runat="server" Text='<%# Eval("ProcessRatingName") %>' Width="25px" Height="23px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            
                                             <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px">                                              
                                                        <asp:Label ID="lblProcessName" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                            
                                             <asp:TemplateField HeaderText="Observation">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 320px;">                                               
                                                        <asp:Label ID="lblObservation" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Observation") %>' ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                                                 
                                            <asp:TemplateField HeaderText="Category">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 100px;">        
                                                    <asp:Label ID="lblLocation" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ObservationCategoryName") %>' ToolTip='<%# Eval("ObservationCategoryName") %>'></asp:Label>                                        
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField  HeaderText="Sub Category">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 100px;">  
                                                        <asp:Label ID="lblVertical" runat="server" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("ObservationSubCategoryName") %>' ToolTip='<%# Eval("ObservationSubCategoryName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period" Visible="false">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">                                              
                                                        <asp:Label runat="server" ID="lblPeriod" data-toggle="tooltip" data-placement="top" 
                                                            Text='<%# Eval("ForPeriod") %>' ToolTip='<%# Eval("ForPeriod") %>'>

                                                        </asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Location" Visible="false">
                                                <ItemTemplate>                                                     
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">                                              
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="top" 
                                                            Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>                                                       
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                                                                   
                                               <asp:TemplateField HeaderText="Process Order">
                                                <ItemTemplate>                                                      
                                                    <asp:TextBox ID="txtProcessSequence" Width="80px" runat="server" TextMode="Number" Text='<%# Eval("ProcessOrder") %>' 
                                                         CssClass="form-control ProcessSequence" style="text-align: center;">
                                                    </asp:TextBox>                                                                                  
                                                </ItemTemplate>
                                            </asp:TemplateField>   
                                            
                                            <asp:TemplateField HeaderText="Observation Number">
                                                <ItemTemplate>                                                      
                                                    <asp:TextBox ID="txtObservationNumber" Width="80px" runat="server" Text='<%# Eval("ObservationNumber")%>' 
                                                         CssClass="form-control ObservationNumbers" style="text-align: center;">
                                                    </asp:TextBox>                                                                                     
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                              <asp:TemplateField ItemStyle-Width="25%" HeaderText="Process Rating" HeaderStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownListChosen runat="server" ID="ddlProcess" class="form-control" Width="100%" 
                                                                AllowSingleDeselect="false" DisableSearchThreshold="5">                                                  
                                                            </asp:DropDownListChosen>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="ddlProcess" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                </asp:TemplateField> 
                                                                                                                                                                                                             
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                         <HeaderStyle BackColor="#ECF0F1" />
                                        <PagerSettings Visible="false" />       
                                        <PagerTemplate>                             
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                        </asp:GridView>

                                        
                                    </div>
                                    <%--<div class="clearfix"></div>--%>
                                      <div class="col-md-12 colpadding0">
                                          <table style="margin-left: 1%;">
                                              <tr>
                                                  <td>
                                                      <%-- <span style="color:#666"> Final Annexure Document</span>--%>
                                                       <span style="color:#666">Feedback Form</span>
                                                  </td>
                                                  <td>                                                      
                                                        <asp:FileUpload ID="FeedbackFileUpload" Style="width: 200px;color: #666;" runat="server"/>                                                 
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="2">
                                                      <div class="clearfix" style="height:10px;"></div>
                                                  </td>                                                 
                                              </tr>
                                              <tr>
                                                  <td>
                                                       <span style="color:#666"> Major Observation / Remarks</span>
                                                  </td>
                                                  <td style="width:80%;">
                                                        <asp:TextBox ID="txtRemark"  runat="server" CssClass="form-control" TextMode="MultiLine" Rows="4"/>
                                                  </td>
                                              </tr>
                                          </table>                             
                                          </div>

                                     <div class="clearfix" style="height:10px;"></div>
                                    <div class="col-md-12 colpadding0">                                                                                                                  
                                        <div class="col-md-5 colpadding0">
                                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">                                     
                                                </div>
                                            <div class="table-paging" style="margin-bottom: 10px;">                                                
                                                <div class="table-paging-text" >
                                                    
                                                </div>                                                
                                                <asp:HiddenField ID="TotalRowsObservation" runat="server" Value="0" />
                                            </div>
                                        </div>                                                                               
                                        <div class="col-md-3 colpadding0 entrycount">                                            
                                        <asp:LinkButton Text="Save" runat="server" ID="btnObservation" 
                                             CssClass="btn btn-primary"  OnClientClick="javascript:return fvalidateProcess();"  
                                         Visible="false" OnClick="btnObservation_Click" />       
                                         <asp:LinkButton Text="Draft" runat="server" ID="btnDraft" CssClass="btn btn-primary" 
                                             Visible="false" OnClick="btnDraft_Click"/>
                                                             
                                        <asp:LinkButton Text="Generate Report" runat="server" ID="btnGenrateReport" OnClientClick="javascript:return getConfirmation();"
                                             CssClass="btn btn-primary" OnClick="btnGenrateReport_Click" Visible="false" /> 
                                       </div> 

                                       <div class="col-md-4 colpadding0 text-right">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListObservation" AutoPostBack="true"
                                            class="form-control m-bot15"  
                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                  OnSelectedIndexChanged="DropDownListObservation_SelectedIndexChanged">                                   
                                            </asp:DropDownListChosen>  
                                           <asp:HiddenField ID="HiddenObjValue" runat="server" />
                                        </div>
                                    </div>                         
                                </div>                            
                                <div style="display:none;">
                                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" GridLines="None"
                                    AllowSorting="true"
                                    CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="50" Width="100%"
                                    Font-Size="12px">
                                    <Columns>                                  
                                    <asp:BoundField DataField="Branch" HeaderText="Location" />
                                    <asp:BoundField DataField="FinancialYear" HeaderText="Financial Year" />
                                    <asp:BoundField DataField="ForMonth" HeaderText="Period" />
                                    <asp:BoundField DataField="Observation" HeaderText="Observation" />                                                                  
                                    <asp:TemplateField>                                  
                                    <ItemTemplate>
                                    <asp:Label ID="lblTimeline" runat="server" Text='<%# Eval("TimeLine")!=null?Convert.ToDateTime(Eval("TimeLine")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>                               
                                    </asp:GridView>
                                </div>
                             </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnObservation" />
            <asp:PostBackTrigger ControlID="btnGenrateReport" />
            <asp:PostBackTrigger ControlID="btnDraft" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
