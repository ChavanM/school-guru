﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteKickOff.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.DeleteKickOff" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Delete Audit Kick Off</title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>            
        <script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>
    --%>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 30px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .dd_chk_drop {
            top: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>

    <script type="text/javascript">
        function CloseModel() {
            window.parent.CloseModelDelete();
        }
    </script>

    <script type="text/javascript">
        function checkAll(chkHeader, gridName) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkContract = document.getElementById("<%=btnDelete.ClientID %>");
                var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
                if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                        divSendDocCount.style.display = "block";
                    }
                    else {
                        lblTotalContractSelected.innerHTML = "";;
                        divSendDocCount.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox               
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkContract = document.getElementById("<%=btnDelete.ClientID %>");
            var lblTotalContractSelected = document.getElementById("<%=lblTotalDocumentSelected.ClientID %>");
            if ((btnSaveLinkContract != null || btnSaveLinkContract != undefined) && (lblTotalContractSelected != null || lblTotalContractSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalContractSelected.innerHTML = selectedRowCount + " Selected";
                    divSendDocCount.style.display = "block";
                }
                else {
                    lblTotalContractSelected.innerHTML = "";;
                    divSendDocCount.style.display = "none";
                }
            }
        }

        function unCheckAll(gridName) {
            var grid = document.getElementById(gridName);
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>
</head>
<body style="overflow-y: hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceTypeList">
            <progresstemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                </div>
            </progresstemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <div class="row Dashboard-white-widget" style="height: 490px; overflow-y: scroll;">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel">
                                <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ForeColor="Red"
                                        ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="float: left; width: 75px; margin-bottom: 0px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" Selected="True" />
                                            <asp:ListItem Text="100" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                             Style="background: none;" 
                                            OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ErrorMessage="Select Location" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlLegalEntity"
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                        <asp:CompareValidator ErrorMessage="Select Location" ControlToValidate="ddlLegalEntity"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Location" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Vertical" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true" class="form-control m-bot15"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Financial Year" Width="90%" Height="32px"
                                            OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                                        </asp:DropDownListChosen>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ErrorMessage="Select Financial Year" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlFilterFinancial"
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                        <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                            Display="None" />
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                           <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownListChosen ID="ddlSchedulingType" runat="server" AutoPostBack="true" Width="90%" 
                                            CssClass="form-control m-bot15"  DataPlaceHolder="Scheduling Type"
                                             OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"></asp:DropDownListChosen>                                                                            
                                    </div>

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                          <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"    DataPlaceHolder="Period"
                                           class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ErrorMessage="Select Period" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlPeriod"
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                        <asp:CompareValidator ErrorMessage="Select Period" ControlToValidate="ddlPeriod"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                            Display="None" />

                                    </div>                                   

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;"></div>                                    

                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;"></div>  
                                </div>
                                
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 4px; margin-top:10px;">
                                    <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        DataKeyNames="AuditID" PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" 
                                        GridLines="None" AllowSorting="true" Width="100%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField  Visible="false">
                                                  <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAuditID"  Text='<%# Eval("AuditID")%>' ></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                         
                                               <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%"> 
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeaderTaskDocument" runat="server" onclick="javascript:checkAll(this)" /> <%--checkAll_MailDocument--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRowTaskDocument" runat="server" onclick="javascript:checkUncheckRow(this)" />   <%--checkUncheckRow_MailDocument--%>                                                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Branch">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                        <asp:Label runat="server" ID="lblBranch" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BrachName")%>' ToolTip='<%# Eval("BrachName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Financial Year">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 75px;"> 
                                                        <asp:Label runat="server" ID="lblFinancialYear" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear")%>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 75px;"> 
                                                        <asp:Label runat="server" ID="lblPeriod" data-toggle="tooltip" data-placement="top"
                                                            Text='<%# Eval("ForMonth")%>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 175px;"> 
                                                        <asp:Label ID="lblprocess" runat="server" data-toggle="tooltip"
                                                            data-placement="top" Text='<%# Eval("CProcessName")%>'
                                                            ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SubProcess">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 175px;"> 
                                                        <asp:Label ID="lblSubprocess" runat="server" data-toggle="tooltip"
                                                            data-placement="top" Text='<%# Eval("SubProcess")%>'
                                                            ToolTip='<%# Eval("SubProcess") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          <%--  <asp:TemplateField HeaderText="Vertical">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 175px;">
                                                        <asp:Label ID="lblControlObjective" runat="server" data-toggle="tooltip"
                                                            data-placement="left" Text='<%# Eval("VerticalName")%>'
                                                            ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <HeaderStyle BackColor="#ECF0F1" /> 
                                        <PagerTemplate></PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>

                                <div id="divSendDocCount" class="row col-md-12 plr0" style="display: none;"> 
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalDocumentSelected" Text="" style=" color:#999;" CssClass="control-label"></asp:Label>
                                        </div>                                        
                                    </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0" style="text-align: right;">
                                        <asp:Button Text="Delete" runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Width="90px"
                                            ValidationGroup="ComplianceInstanceValidationGroup" CausesValidation="true" />
                                         <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn btn-primary" Width="90px"
                                           OnClientClick="CloseModel();" />

                                    </div>
                                    <div class="col-md-6 colpadding0" style="float: right;">
                                        <div class="table-paging" style="margin-bottom: 10px; float: right; color: #999;">
                                            <p>
                                                Page
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                                class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            </p>
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
