﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Spire.Presentation;
using Spire.Presentation.Drawing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmObservationReportPowerPoint : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                BindLegalEntityData();
                BindVertical();
                BindFinancialYear();

                BindData();

                populateDdlMultiColor();
                colorManipulation();

                bindPageNumber();
                //GetPageDisplaySummary();  
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAudits.PageIndex = chkSelectedPage - 1;
                        
            grdAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            
            BindData();

        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFinancialYear.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int customerbranchid = -1;
            int VerticalID = -1;
            string FinancialYear = String.Empty;
            string Period = String.Empty;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    customerbranchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    customerbranchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    customerbranchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    customerbranchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    customerbranchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    FinancialYear = ddlFinancialYear.SelectedItem.Text;
                }
            }

            if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
            {
                if (ddlPeriod.SelectedValue != "-1")
                {
                    Period = ddlPeriod.SelectedItem.Text;
                }
            }

            if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
            {
                if (ddlVertical.SelectedValue != "-1")
                {
                    VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(customerID, customerbranchid);

            List<long> BranchAssigned = AssignEntityManagementRisk.CheckAuditManagerLocation(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditRecords = (from row in entities.AuditClosure_DisplayViewNew
                                    where row.CustomerID == customerID
                                    && BranchAssigned.Contains(row.CustomerBranchId)
                                    && row.ACCStatus == 1
                                    select new AuditKickOffDetails()
                                    {
                                        BranchName = row.BranchName,
                                        TermName = row.ForPeriod,
                                        CustomerBranchId = row.CustomerBranchId,
                                        ISAHQMP = row.ISAHQMP,
                                        FinancialYear = row.FinancialYear,
                                        Period = row.ForPeriod,
                                        //AssignedTo = row.AssignedTo,
                                        //ExternalAuditorId = row.ExternalAuditorId,
                                        PhaseCount = row.PhaseCount,
                                        VerticalID = row.VerticalID,
                                        VerticalName = row.VerticalName
                                    }).Distinct().ToList();

                if (Branchlist.Count > 0)
                    AuditRecords = AuditRecords.Where(Entry => Branchlist.Contains(Entry.CustomerBranchId)).ToList();

                if (VerticalID != -1)
                    AuditRecords = AuditRecords.Where(entry => entry.VerticalID == VerticalID).ToList();

                if (!string.IsNullOrEmpty(FinancialYear))
                    AuditRecords = AuditRecords.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (!string.IsNullOrEmpty(Period))
                    AuditRecords = AuditRecords.Where(entry => entry.Period == Period).ToList();

                grdAudits.DataSource = null;
                grdAudits.DataBind();

                grdAudits.DataSource = AuditRecords.ToList();
                Session["TotalRows"] = AuditRecords.Count;
                grdAudits.DataBind();

                //bindPageNumber();
                //GetPageDisplaySummary();

                grdAudits.Visible = true;
                
            }
        }

        public int GetBranchID()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            return CustomerBranchId;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlFilterLocation.Items.Count > 0)
                        ddlFilterLocation.Items.Clear();
                }
            }

            BindVertical();
            BindSchedulingType();

            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlFilterLocation.Items.Count > 0)
                        ddlFilterLocation.Items.Clear();
                }
            }

            BindVertical();
            BindSchedulingType();

            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlFilterLocation.Items.Count > 0)
                        ddlFilterLocation.Items.Clear();
                }
            }

            BindVertical();
            BindSchedulingType();

            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlFilterLocation.Items.Count > 0)
                        ddlFilterLocation.Items.Clear();
                }
            }

            BindVertical();
            BindSchedulingType();

            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedItem.Text))
            {
                BindVertical();
                BindSchedulingType();

                BindData();
                //GetPageDisplaySummary();
                bindPageNumber();
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateDdlMultiColor();
            colorManipulation();

            BindData();
           // GetPageDisplaySummary();
            bindPageNumber();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted==false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }

        public string GetUserName(int userid)
        {
            string username = "";
            if (userid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {  
                    mst_User mstuser = (from row in entities.mst_User
                                        where row.ID == userid 
                                        && row.IsDeleted == false 
                                        && row.IsActive==true
                                        select row).FirstOrDefault();

                    if (mstuser != null)
                        username = mstuser.FirstName + " " + mstuser.LastName;
                }
            }
            return username;
        }
                     
        protected void ddlMultiColor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlMultiColor.BackColor = Color.FromName(ddlMultiColor.SelectedItem.Text);
            colorManipulation();
            ddlMultiColor.Items.FindByValue(ddlMultiColor.SelectedValue).Selected = true;
            //msgColor.Attributes.Add("style", "background:" + ddlMultiColor.SelectedItem.Value + ";width:30px;height:25px;");
            //ddlMultiColor.BackColor = Color.FromName(ddlMultiColor.SelectedItem.Text);
            //colorManipulation();
            //ddlMultiColor.Items.FindByValue(ddlMultiColor.SelectedValue).Selected = true;
            //msgColor.Attributes.Add("style", "background:" + ddlMultiColor.SelectedItem.Value + ";width: 30px; height: 25px;");
        }

        public static List<InternalControlAuditResult> TempInternalControlAuditResult(int branchid, string Financialyear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.CustomerBranchId == branchid && row.FinancialYear == Financialyear
                             && row.AStatusId == 3
                             select row).ToList();
                return query;
            }
        }


        #region Grid Summary Details

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}

                //Reload the Grid             
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAudits.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        //BindProcessSubType();
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        //BindProcessSubType();
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}

        #endregion


        #region private method

        private void colorManipulation()
        {
            int row;
            for (row = 0; row < ddlMultiColor.Items.Count - 1; row++)
            {
                ddlMultiColor.Items[row].Attributes.Add("style", "background-color:" + ddlMultiColor.Items[row].Value);
            }
            ddlMultiColor.BackColor = Color.FromName(ddlMultiColor.SelectedItem.Text);
        }

        private List<string> finalColorList()
        {

            string[] allColors = Enum.GetNames(typeof(System.Drawing.KnownColor));
            string[] systemEnvironmentColors = new string[(typeof(System.Drawing.SystemColors)).GetProperties().Length];

            int index = 0;

            foreach (MemberInfo member in (typeof(System.Drawing.SystemColors)).GetProperties())
            {
                systemEnvironmentColors[index++] = member.Name;
            }

            List<string> finalColorList = new List<string>();

            foreach (string color in allColors)
            {
                if (Array.IndexOf(systemEnvironmentColors, color) < 0)
                {                   
                    //if (color != "Transparent")
                    //{
                    //    if (color != "Azure")
                    //    {
                    //        if (color != "Beige")
                    //        {
                    //            if (color != "Cornsilk")
                    //            {
                    //                if (color != "AliceBlue")
                    //                {
                    //                    if (color != "FloralWhite")
                    //                    {
                    //                        if (color != "GhostWhite")
                    //                        {
                    //                            if (color != "White")
                    //                            {
                                                    finalColorList.Add(color);
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                }
            }

            return finalColorList;

        }

        private void populateDdlMultiColor()
        {
            ddlMultiColor.DataSource = finalColorList();
            ddlMultiColor.DataBind();
        }

        #endregion

        protected void btnExportDoc_Click(object sender, EventArgs e)
        {
            try
            {
                int customerbranchid = -1;
                int VerticalID = -1;

                string Period = string.Empty;
                string financialyear = string.Empty;
                string BranchName = string.Empty;

                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                if (commandArgs.Length > 1)
                {
                    if (Convert.ToString(commandArgs[0]) != null || Convert.ToString(commandArgs[0]) != "")
                    {
                        Period = Convert.ToString(commandArgs[0]);
                    }
                    if (Convert.ToString(commandArgs[1]) != null || Convert.ToString(commandArgs[1]) != "")
                    {
                        customerbranchid = Convert.ToInt32(commandArgs[1]);
                    }
                    if (Convert.ToString(commandArgs[2]) != null || Convert.ToString(commandArgs[2]) != "")
                    {
                        financialyear = Convert.ToString(commandArgs[2]);
                    }
                    if (Convert.ToString(commandArgs[3]) != null || Convert.ToString(commandArgs[3]) != "")
                    {
                        VerticalID = Convert.ToInt32(commandArgs[3]);
                    }
                    if (Convert.ToString(commandArgs[4]) != null || Convert.ToString(commandArgs[4]) != "")
                    {
                        BranchName = Convert.ToString(commandArgs[4]);
                    }

                    var details = GetAuditWiseObservationDetailList(customerbranchid, VerticalID, financialyear, Period);

                    if (details.Count > 0)
                    {
                        var ProcessList = GetProcessListOrdered(details);

                        float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                        float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;

                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        String PPTName = "Draft Internal Audit Report - " + Period;

                        string CustomerName = GetByID(customerID).Name;
                        string colorname = String.Empty;

                        if (ddlMultiColor.SelectedItem.Text != "")
                            colorname = ddlMultiColor.SelectedItem.Text;
                        else
                            colorname = "Chocolate";

                        CreateDymanicHeadingFirstSlide(SlideCounter, xpointsection1, 10, 700, 520, "", 32, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 80, 700, 100, CustomerName, 32, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 150, 700, 100, BranchName, 30, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 200, 700, 60, PPTName, 33, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 250, 700, 60, "Year- " + financialyear + "", 24, "L", colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        PPTName=PPTName+ " " + "(Year " + financialyear + ")";

                        CreateDymanicPara(SlideCounter, xpointsection1, 200, 700, 205, "Ignore – to be replaced by cover letter", 22);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 100, "Table of Content", 32, "L", colorname);
                        DynamicTableOfContent(SlideCounter, xpointsection1, 120, 700, 300, "", 24, colorname, ProcessList);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", "White");
                        SetSlideColor(SlideCounter, colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;
                        
                        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", colorname);

                        //Add New Blank Slide 
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        int ProcessID = -1;

                        int Count = 0;
                        int ProcessNumber = 0;
                        int ObservationNumber = 0;

                        String ObservationNo = string.Empty;

                       details.ForEach(entry =>
                        {
                            String SectionHeading = "Section " + intToRoman(Count + 2) + "";

                            ObservationNumber++;

                            //if (ProcessID == -1)
                            //{
                            //    ProcessID = Convert.ToInt32(entry.ProcessId);
                            //    ProcessNumber = 1;

                            //    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, SectionHeading, 32, "L", "White");
                            //    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 100, ProcessList.ElementAt(Count), 32, "L", "White");
                            //    SetSlideColor(SlideCounter, colorname);
                            //    presentationdynamic.Slides.Append();
                            //    SlideCounter++;
                            //    Count++;
                            //}
                            //else if (ProcessID != entry.ProcessId)

                            if (ProcessID == -1 || ProcessID != entry.ProcessId)
                            {
                                if (Count < ProcessList.Count)
                                {
                                    ProcessID = Convert.ToInt32(entry.ProcessId);
                                    ProcessNumber++;
                                    ObservationNumber = 1;

                                    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 50, SectionHeading, 32, "L", "White");
                                    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 50, ProcessList.ElementAt(Count), 32, "L", "White");
                                    SetSlideColor(SlideCounter, colorname);
                                    presentationdynamic.Slides.Append();
                                    SlideCounter++;
                                    Count++;
                                }
                                else
                                {

                                }
                            }                           

                            int checkvalueobservation = 0;
                            int checkvalueRisk = 0;
                            int checkvalueRecomentdation = 0;
                            int checkvalueManagementresponse = 0;
                            int checkvalueTimeLine = 0;

                            string[] arrayobservation = entry.Observation.Split(' ');
                            int arrobservationLength = arrayobservation.Length;
                            checkvalueobservation = (2 * arrobservationLength);

                            string[] arrayrisk = entry.Risk.Split(' ');
                            int arrLengthrisk = arrayrisk.Length;
                            checkvalueRisk = (2 * arrLengthrisk);

                            string[] arrayRecommendations = entry.Recomendation.Split(' ');
                            int arrLengthRecommendations = arrayRecommendations.Length;
                            checkvalueRecomentdation = (2 * arrLengthRecommendations);

                            string[] arrayManagementResponse = entry.ManagementResponse.Split(' ');
                            int arrLengthManagementResponsek = arrayManagementResponse.Length;
                            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

                            string[] arrayPersonresponsible = entry.ManagementResponse.Split(' ');
                            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
                            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

                            //add footer
                            presentationdynamic.SetFooterText(PPTName);
                            
                            //set the footer visible
                            presentationdynamic.SetFooterVisible(true);

                            //set the page number visible
                            presentationdynamic.SetSlideNoVisible(true);

                            //set the date visible
                            presentationdynamic.SetDateTimeVisible(true);

                            if (checkvalueobservation <= 200 && checkvalueRisk <= 200 && checkvalueRecomentdation <= 150
                                && checkvalueManagementresponse <= 150 && checkvalueTimeLine <= 100)
                            {
                                ObservationNo = ProcessNumber + "." + ObservationNumber + " ";
                                PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                               //PrintDynamicObservationSlideFix(SlideCounter, ObservationNo, entry.ObservationTitle, entry.Observation, entry.Risk, entry.Recomendation, entry.ManagementResponse, Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                            }
                            else
                            {
                                // PrintDynamicObservationSlide(SlideCounter, entry.ObservationNumber, entry.ObservationTitle, entry.Observation, entry.Risk, entry.Recomendation, entry.ManagementResponse, Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                                PrintDynamicObservationSlide(SlideCounter, Regex.Replace(entry.ObservationNumber, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                            }
                        });

                        CreateDymanicHeading(SlideCounter, xpointsection1 + 100, 200, 300, 100, "Thank You", 32, "L", "White");
                        SetSlideColor(SlideCounter, colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        string dt = DateTime.Now.ToString("dd-MMM-yy");

                        if ((BranchName.Split(' ').First() + "-" + PPTName + "-" + dt).Length < 218)
                            PPTName = BranchName.Split(' ').FirstOrDefault() + "-" + PPTName + "-" + dt;

                        string path = Server.MapPath("~//PPTDownload//" + PPTName + ".pptx");

                        presentationdynamic.DocumentProperty["_MarkAsFinal"] = true;
                        presentationdynamic.SaveToFile(path, FileFormat.Pptx2010);                       

                        Response.ContentType = "application/octet-stream";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + PPTName + ".pptx");
                        Response.TransmitFile(path);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }


        public static string intToRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            // starting with the highest place (for 3046, it would be the thousands
            // place, or 3), get the roman numeral representation for that place
            // and add it to the final roman numeral string
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

        public int SlideCounter = 0;

        Presentation presentationdynamic = new Presentation();

        public void SetSlideColor(int slidenumber, string colorName)
        {
            presentationdynamic.Slides[slidenumber].SlideBackground.Type = BackgroundType.Custom;
            presentationdynamic.Slides[slidenumber].SlideBackground.Fill.FillType = FillFormatType.Solid;          

            switch (colorName)
            {
                #region A
                case "AliceBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
        }

        public void CreateDymanicHeadingFirstSlide(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);

            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gray;
                    break;
                case "Green":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Peru;
                    break;
                case "Pink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Pink;
                    break;
                case "Plum":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tan;
                    break;
                case "Teal":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            //switch (colorname)
            //{
            //    case "Navy":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
            //        break;
            //    case "OrangeRed":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
            //        break;
            //    case "RoyalBlue":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
            //        break;
            //    case "Red":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
            //        break;
            //    case "Green":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
            //        break;
            //    case "Sienna":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
            //        break;
            //    case "Purple":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
            //        break;
            //    case "Maroon":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
            //        break;
            //    default:
            //        break;
            //}

            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();

            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }


        public void CreateDymanicFilledHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            
            switch (colorname)
            {
                case "Red":
                    shape_Secc.Fill.SolidColor.Color = Color.Red;
                    break;
                case "Chocolate":
                    shape_Secc.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Green":
                    shape_Secc.Fill.SolidColor.Color = Color.Green;
                    break;
                default:
                    break;
            }

            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;
                        
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;
                case "White":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;
                    break;
                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }
        
        public void CreateDymanicSecondHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                default:
                    break;
            }
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicSecondTitle(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;
            para_Secc.Alignment = TextAlignmentType.Left;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                default:
                    break;
            }
            //para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicTitle(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;
            para_Secc.Alignment = TextAlignmentType.Left;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            if (Contain.Contains('\n'))
            {
                string ss = Contain.Replace("\r\n", "");
                para_Secc.Text = ss;
            }
            else
            {
                para_Secc.Text = Contain;
            }
            para_Secc.Alignment = TextAlignmentType.Justify;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.MarginTop = 0;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicLoopPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, string printsection, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > 200)
            {
                do
                {
                    if (arrLength > 200)
                    {
                        int cnt = (arrLength / 200) + 1;

                        for (int j = 200; j > 0; j--)
                        {
                            if (array[j].Contains("."))
                            {
                                indextoSplit = j;
                                j = 0;
                                break;
                            }
                        }
                        for (int j = 0; j <= indextoSplit; j++)
                        {
                            if (!String.IsNullOrEmpty(Firstpara))
                                Firstpara = Firstpara + " " + array[j];
                            else
                                Firstpara = Firstpara + array[j];
                            Lntcnt += 1;
                        }
                        CreateDymanicPara(slidenumber, x, y, width, height, Firstpara, fontSize);
                        Firstpara = "";
                        Nextpara = "";
                        for (int k = Lntcnt; k < arrLength; k++)
                        {
                            if (!String.IsNullOrEmpty(Nextpara))
                                Nextpara = Nextpara + " " + array[k];
                            else
                                Nextpara = Nextpara + array[k];
                        }
                        arrLength = 0;
                        array = Nextpara.Split(' ');
                        arrLength = array.Length;
                        Lntcnt = 0;
                    }
                    else
                    {
                        if (printsection == "S")
                        {
                            presentationdynamic.Slides.Append();
                            SlideCounter++;
                            yposition = (2 * arrLength);
                            CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 20, "1.1  Explore possibility of payment of Ocean Freight bills in USD", 21, "L", colorname);
                            CreateDymanicPara(SlideCounter, xpointsection1, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "F";
                        }
                        else
                        {
                            yposition = (2 * arrLength);
                            CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "S";
                        }

                    }
                } while (arrLength > 0);
            }
            else
            {
                if (printsection == "S")
                {
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    yposition = (2 * arrLength);
                    CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";
                }
                else
                {
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    CreateDymanicPara(slidenumber, x, y, 350, dynamicheight, Contain, fontSize);
                    yposition = dynamicheight;
                    section = "F";
                }
            }
        }

        public void CreateDymanicLoopParaDynamicArrayLength(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, int dymnamicchar, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > dymnamicchar)
            {
                if (arrLength > dymnamicchar)
                {
                    int cnt = (arrLength / dymnamicchar) + 1;

                    for (int j = dymnamicchar; j > 0; j--)
                    {
                        if (array[j].Contains("."))
                        {
                            indextoSplit = j;
                            j = 0;
                            break;
                        }
                    }
                    for (int j = 0; j <= indextoSplit; j++)
                    {
                        if (!String.IsNullOrEmpty(Firstpara))
                            Firstpara = Firstpara + " " + array[j];
                        else
                            Firstpara = Firstpara + array[j];
                        Lntcnt += 1;
                    }
                    CreateDymanicPara(0, x, y, width, height, Firstpara, fontSize);
                    Firstpara = "";
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    arrLength = 0;
                    array = Nextpara.Split(' ');
                    arrLength = array.Length;
                    Lntcnt = 0;
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    section = "S";
                    yposition = (2 * arrLength);
                    CreateDymanicLoopPara(0, xpointsection2, 70, 350, dynamicheight, Contain, fontSize, arrLength, array, section, colorname, out yposition, out section);

                }
                else
                {

                    yposition = (2 * arrLength);
                    CreateDymanicPara(0, xpointsection2, 70, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";

                }
            }
            else
            {
                int dynamicheight = 0;
                dynamicheight = (2 * arrLength);
                CreateDymanicPara(0, x, y, 350, dynamicheight, Contain, fontSize);
                yposition = dynamicheight;
                section = "S";

            }
        }

        public static List<InternalControlAuditResult> GetAllInternalControlAuditResultList(int CustomerBranchId,int verticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (verticalID != -1)
                {
                    var AuditorUserMasterList = (from row in entities.InternalControlAuditResults
                                                 where row.AStatusId == 3
                                                 && row.CustomerBranchId == CustomerBranchId 
                                                 && row.VerticalID == verticalID
                                                 select row);
                    AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);

                    return AuditorUserMasterList.ToList();
                }
                else
                {
                    var AuditorUserMasterList = (from row in entities.InternalControlAuditResults
                                                 where row.AStatusId == 3
                                                 && row.CustomerBranchId == CustomerBranchId 
                                                 select row);

                    AuditorUserMasterList = AuditorUserMasterList.OrderBy(entry => entry.ID);
                    return AuditorUserMasterList.ToList();
                }               
            }
        }

        public static List<Business.DataRisk.AuditClosure> GetAuditWiseObservationDetailList(int CustomerBranchId, int verticalID, String FinYear, String Period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in entities.AuditClosures
                                                where row.CustomerbranchId == CustomerBranchId
                                                && row.VerticalID == verticalID
                                                && row.FinancialYear == FinYear
                                                && row.ForPeriod == Period
                                                && row.ACStatus == 1
                                                select row).ToList();

                AuditWiseObservationList = AuditWiseObservationList
                    .OrderBy(entry => entry.ProcessOrder)
                    .ThenBy(entry => entry.ObservationNumber).ToList();

                return AuditWiseObservationList.ToList();
            }
        }

        public static List<String> GetProcessListOrdered(List<Business.DataRisk.AuditClosure> ACC)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in ACC
                                                join MP in entities.Mst_Process
                                                on row.ProcessId equals MP.Id
                                                orderby row.ProcessOrder 
                                                select MP.Name).Distinct().ToList();               

                return AuditWiseObservationList.ToList();
            }
        }

        public static mst_Customer GetByID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_Customer
                                where row.ID == customerID && row.IsDeleted == false
                                select row).SingleOrDefault();

                return customer;
            }
        }

        public void PrintDynamicObservationSlideFix(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, int? ObsRating, string colorname)
        {
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;

            int checkvalueobservation = 0;
            int checkvalueRisk = 0;
            int checkvalueRecomentdation = 0;
            int checkvalueManagementresponse = 0;
            int checkvalueTimeLine = 0;

            string[] arrayobservation = paraobservation.Split(' ');
            int arrobservationLength = arrayobservation.Length;
            checkvalueobservation = (2 * arrobservationLength);

            string[] arrayrisk = pararisk.Split(' ');
            int arrLengthrisk = arrayrisk.Length;
            checkvalueRisk = (2 * arrLengthrisk);

            string[] arrayRecommendations = pararRecommendations.Split(' ');
            int arrLengthRecommendations = arrayRecommendations.Length;
            checkvalueRecomentdation = (2 * arrLengthRecommendations);

            string[] arrayManagementResponse = paraManagementResponse.Split(' ');
            int arrLengthManagementResponsek = arrayManagementResponse.Length;
            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

            string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

            String ObservationRating = String.Empty;
            String ObsRatingColor = String.Empty;


            if(ObsRating!=null)
            {
                if(ObsRating==1)
                {
                    ObservationRating = "High";
                    ObsRatingColor = "Red";
                }
                else if(ObsRating==2)
                {
                    ObservationRating = "Medium";
                    ObsRatingColor = "Chocolate";
                }
                else if(ObsRating==3)
                {
                    ObservationRating = "Low";
                    ObsRatingColor = "Green";
                }
            }

            CreateDymanicHeading(slidecounter, xpointsection1, 30, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
            CreateDymanicFilledHeading(slidecounter, 615, 5, 100, 20, ObservationRating, 15, "C", ObsRatingColor);
            CreateDymanicTitle(slidecounter, xpointsection1, 70, 350, 10, "Observations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 100, 350, 205, paraobservation, 13);

            CreateDymanicTitle(slidecounter, xpointsection1, 250, 350, 10, "Risk:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 280, 350, 205, pararisk, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 100, 350, 150, pararRecommendations, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 200, 350, 10, "Management Response:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 230, 350, 150, paraManagementResponse, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 350, 350, 10, "Responsibility and Timeline:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 380, 350, 100, paraPersonresponsible, 13);
        }

        public void PrintDynamicObservationSlide(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, string colorname)
        {
            try
            {
                string printstartfrom = "";
                int dynamicwordlength = 0;
                int dynamicheight = 0;
                int totalheight = 400;
                String Firstpara = String.Empty;
                String Nextpara = String.Empty;
                string section = "F";
                int yposition = 0;
                int newvalue = 0;
                string sectionvalue = "A";
                float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
                float ypoint = 40;
                int thirdval = 0;

                //int checkvalueobservation = 0;
                //int checkvalueRisk = 0;
                //int checkvalueRecomentdation = 0;
                //int checkvalueManagementresponse = 0;
                //int checkvalueTimeLine = 0;

                string[] arrayobservation = paraobservation.Split(' ');
                int arrobservationLength = arrayobservation.Length;

                string[] arrayrisk = pararisk.Split(' ');
                int arrLengthrisk = arrayrisk.Length;

                string[] arrayRecommendations = pararRecommendations.Split(' ');
                int arrLengthRecommendations = arrayRecommendations.Length;

                string[] arrayManagementResponse = paraManagementResponse.Split(' ');
                int arrLengthManagementResponsek = arrayManagementResponse.Length;

                string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
                int arrLengthPersonresponsible = arrayPersonresponsible.Length;

                CreateDymanicHeading(slidecounter, xpointsection1, ypoint, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
                CreateDymanicTitle(slidecounter, xpointsection1, ypoint + 30, 350, 10, "Observations:", 13, colorname);
                CreateDymanicLoopPara(slidecounter, xpointsection1, ypoint + 70, 350, totalheight, paraobservation, 13, arrobservationLength, arrayobservation, section, colorname, out yposition, out section);

                newvalue = yposition;
                sectionvalue = section;
                if (sectionvalue == "F")//observation First Section Value
                {
                    dynamicheight = (2 * arrLengthrisk);
                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                    CreateDymanicTitle(slidecounter, xpointsection1, (ypoint + 70 + newvalue), 350, 10, "Risk:", 13, colorname);
                    if (dynamicheight > totalheight - newvalue)
                    {
                        printstartfrom = "MID";
                        CreateDymanicLoopParaDynamicArrayLength(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20,
                        pararisk, 13, arrLengthrisk, arrayrisk, dynamicwordlength, colorname, out yposition, out section);
                        thirdval += yposition;
                    }
                    else
                    {
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicLoopPara(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20, pararisk, 13,
                            arrLengthrisk, arrayrisk, section, colorname, out yposition, out section);
                        printstartfrom = "START";
                    }//risk Print Complete                       
                    newvalue = yposition;
                    sectionvalue = section;
                    if (sectionvalue == "F")//Risk  First Section Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval = newvalue;
                        }
                        else
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += newvalue;
                        }
                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                            newvalue = yposition;
                            sectionvalue = section;
                            if (sectionvalue == "F")
                            {
                                thirdval += newvalue;
                                dynamicheight = (2 * arrLengthPersonresponsible);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 110 + thirdval), 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 120 + thirdval + 20), 350, totalheight - thirdval + 20, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                            }
                            else if (sectionvalue == "S")//Recomendation Second Section Value
                            {
                                if (newvalue > 300)
                                {
                                    presentationdynamic.Slides.Append();
                                    SlideCounter++;
                                    dynamicheight = (2 * arrLengthRecommendations);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                                    newvalue = yposition;
                                    sectionvalue = section;
                                    if (sectionvalue == "F")
                                    {
                                        dynamicheight = (2 * arrLengthPersonresponsible);
                                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                                    }
                                }
                            }
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            if (newvalue > 300)
                            {
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                                dynamicheight = (2 * arrLengthRecommendations);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Recommendations:", 13, colorname);
                                CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);

                                newvalue = yposition;
                                sectionvalue = section;
                                if (sectionvalue == "F")
                                {
                                    dynamicheight = (2 * arrLengthManagementResponsek);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                                }
                            }
                        }
                    }
                    else if (sectionvalue == "S")//Risk Section Second Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                        else
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += yposition;
                        }
                        newvalue = yposition;
                        sectionvalue = section;

                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - thirdval - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, thirdval + 70, 350, 10, "Management Response:", 13, colorname);
                        }
                    }
                }
                else if (sectionvalue == "S") //observation Second Section Value
                {
                    if (newvalue > 300)
                    {
                        presentationdynamic.Slides.Append();
                        SlideCounter++;
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Risk:", 13, colorname);
                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararisk, 13, arrLengthrisk, arrayrisk, sectionvalue, colorname, out yposition, out section);

                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")
                        {
                            dynamicheight = (2 * arrLengthRecommendations);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recomendtions:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void DynamicTableOfContent(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname, List<string> ProcessList)
        {
            RectangleF rec_bullet = new RectangleF(x, y, 700, height);
            IAutoShape shape_bullet = presentationdynamic.Slides[SlideCounter].Shapes.AppendShape(ShapeType.Rectangle, rec_bullet);
            shape_bullet.ShapeStyle.LineColor.Color = Color.White;
            shape_bullet.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            shape_bullet.TextFrame.AnchoringType = TextAnchorType.Top;

            string[] str = new string[] { "Executive Summary", "Detailed Report" };

            foreach (string txt in str)
            {
                TextParagraph textParagraph = new TextParagraph();
                textParagraph.Text = txt;
                textParagraph.Alignment = TextAlignmentType.Left;
                textParagraph.Indent = 35;

                //set the Bullets
                textParagraph.BulletType = TextBulletType.Symbol;
                textParagraph.BulletStyle = NumberedBulletStyle.BulletRomanLCPeriod;
                textParagraph.LineSpacing = 150;
                shape_bullet.TextFrame.Paragraphs.Append(textParagraph);

                if (txt == "Detailed Report")
                {
                    string[] str1 = ProcessList.ToArray();
                    
                    foreach (string txt1 in str1)
                    {
                        TextParagraph textinnerParagraph = new TextParagraph();
                        textinnerParagraph.Text = txt1;
                        textinnerParagraph.Alignment = TextAlignmentType.Left;
                        textinnerParagraph.Indent = 24;
                        textinnerParagraph.LeftMargin = 50;
                        textinnerParagraph.LineSpacing = 150;
                        //set the Bullets
                        textinnerParagraph.BulletType = TextBulletType.Numbered;
                        textinnerParagraph.BulletStyle = NumberedBulletStyle.BulletArabicPeriod;
                        shape_bullet.TextFrame.Paragraphs.Append(textinnerParagraph);
                    }
                }
            }

            ////set the font and fill style
            foreach (TextParagraph paragraph in shape_bullet.TextFrame.Paragraphs)
            {
                paragraph.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
                paragraph.TextRanges[0].FontHeight = 24;
                paragraph.TextRanges[0].Fill.FillType = FillFormatType.Solid;
                switch (colorname)
                {
                    #region A
                    case "AliceBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                        break;
                    case "AntiqueWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                        break;
                    case "Aqua":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                        break;
                    case "Aquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                        break;
                    case "Azure":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                        break;
                    #endregion

                    #region B
                    case "Black":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                        break;
                    case "BlanchedAlmond":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                        break;
                    case "Blue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                        break;
                    case "BlueViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                        break;
                    case "Brown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                        break;
                    case "BurlyWood":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                        break;
                    #endregion

                    #region C
                    case "CadetBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                        break;
                    case "Chartreuse":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                        break;
                    case "Chocolate":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                        break;
                    case "Coral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                        break;
                    case "CornflowerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                        break;
                    case "Crimson":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                        break;
                    case "Cornsilk":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                        break;
                    case "Cyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                        break;
                    #endregion

                    #region D
                    case "DarkBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                        break;
                    case "DarkCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                        break;
                    case "DarkGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                        break;
                    case "DarkGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                        break;
                    case "DarkGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                        break;
                    case "DarkKhaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                        break;
                    case "DarkMagenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                        break;
                    case "DarkOliveGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                        break;
                    case "DarkOrange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                        break;
                    case "DarkOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                        break;
                    case "DarkRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                        break;
                    case "DarkSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                        break;
                    case "DarkSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                        break;
                    case "DarkSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                        break;
                    case "DarkSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                        break;
                    case "DarkTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                        break;
                    case "DarkViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                        break;
                    case "DeepPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                        break;
                    case "DeepSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                        break;
                    case "DimGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                        break;
                    case "DodgerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                        break;
                    #endregion

                    #region F
                    case "Firebrick":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                        break;
                    case "FloralWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                        break;
                    case "Fuchsia":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                        break;
                    case "ForestGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                        break;
                    #endregion

                    #region G
                    case "Gainsboro":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                        break;
                    case "GhostWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                        break;
                    case "Gold":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                        break;
                    case "Goldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                        break;
                    case "Gray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                        break;
                    case "Green":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                        break;
                    case "GreenYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                        break;
                    #endregion

                    #region H
                    case "Honeydew":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                        break;
                    case "HotPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                        break;
                    #endregion

                    #region I
                    case "IndianRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                        break;
                    case "Indigo":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                        break;
                    case "Ivory":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                        break;
                    #endregion

                    #region K
                    case "Khaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                        break;
                    #endregion

                    #region L
                    case "Lavender":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                        break;
                    case "LavenderBlush":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                        break;
                    case "LawnGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                        break;
                    case "LemonChiffon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                        break;
                    case "LightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                        break;
                    case "LightCoral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                        break;
                    case "LightCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                        break;
                    case "LightGoldenrodYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                        break;
                    case "LightGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                        break;
                    case "LightGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                        break;
                    case "LightPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                        break;
                    case "LightSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                        break;
                    case "LightSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                        break;
                    case "LightSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                        break;
                    case "LightSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                        break;
                    case "LightSteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                        break;
                    case "LightYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                        break;
                    case "Lime":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                        break;
                    case "LimeGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                        break;
                    case "Linen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                        break;
                    #endregion

                    #region M
                    case "Magenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                        break;
                    case "MediumAquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                        break;
                    case "Maroon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                        break;
                    case "MediumBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                        break;
                    case "MediumOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                        break;
                    case "MediumPurple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                        break;
                    case "MediumSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                        break;
                    case "MediumSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                        break;
                    case "MediumSpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                        break;
                    case "MediumTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                        break;
                    case "MediumVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                        break;
                    case "MidnightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                        break;
                    case "MintCream":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                        break;
                    case "MistyRose":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                        break;
                    case "Moccasin":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                        break;
                    #endregion

                    #region N
                    case "Navy":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                        break;
                    case "NavajoWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                        break;
                    #endregion

                    #region O
                    case "OldLace":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                        break;
                    case "Olive":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                        break;
                    case "OliveDrab":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                        break;
                    case "Orange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                        break;
                    case "OrangeRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                        break;
                    case "Orchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                        break;

                    #endregion

                    #region P
                    case "PaleGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                        break;
                    case "PaleGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                        break;
                    case "PaleTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                        break;
                    case "PaleVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                        break;
                    case "PapayaWhip":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                        break;
                    case "PeachPuff":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                        break;
                    case "Peru":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                        break;
                    case "Pink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                        break;
                    case "Plum":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                        break;
                    case "PowderBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                        break;
                    case "Purple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                        break;
                    #endregion

                    #region R
                    case "RoyalBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                        break;
                    case "Red":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                        break;
                    case "RosyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                        break;
                    #endregion

                    #region S
                    case "SaddleBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                        break;
                    case "Salmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                        break;
                    case "SandyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                        break;
                    case "SeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                        break;
                    case "SeaShell":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                        break;
                    case "Sienna":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                        break;
                    case "Silver":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                        break;
                    case "SkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                        break;
                    case "SlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                        break;
                    case "SlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                        break;
                    case "Snow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                        break;
                    case "SpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                        break;
                    case "SteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                        break;
                    #endregion

                    #region T
                    case "Tan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                        break;
                    case "Teal":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                        break;
                    case "Thistle":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                        break;
                    case "Tomato":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                        break;
                    case "Turquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                        break;
                    #endregion

                    #region V
                    case "Violet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                        break;

                    #endregion

                    #region W
                    case "Wheat":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                        break;
                    case "WhiteSmoke":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                        break;

                    #endregion

                    #region Y
                    case "YellowGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                        break;
                    case "Yellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                        break;
                    #endregion
                    default:
                        break;
                }
            }
        }

        protected void btnProcessOrderSave_Click(object sender, EventArgs e)
        {
            try
            {
                
                int customerbranchid = -1;
                string FinancialYear = String.Empty;
                string Period = String.Empty;
                int VerticalID = -1;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (ViewState["CustBranchID"] != null)
                    customerbranchid = Convert.ToInt32(ViewState["CustBranchID"]);

                if (ViewState["FinYear"] != null)
                    FinancialYear = Convert.ToString(ViewState["FinYear"]);

                if (ViewState["Period"] != null)
                    Period = Convert.ToString(ViewState["Period"]);

                if (ViewState["VerticalID"] != null)
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);

                if(grdProcessOrder.Rows.Count>0)
                {
                    bool Success = true;
                    for (int RowIndex = 0; RowIndex < grdProcessOrder.Rows.Count; RowIndex++)
                    {
                        TextBox txtProcessOrder = (TextBox)grdProcessOrder.Rows[RowIndex].FindControl("txtProcessOrder");

                        if(txtProcessOrder!=null)
                            if(txtProcessOrder.Text=="")
                            {
                                Success = false;
                                cvProcessOrderPopUp.IsValid = false;
                                cvProcessOrderPopUp.ErrorMessage = "Please Provide Process Order";
                                return;
                                break;                                
                            }
                    }

                    if(Success)
                    {                         
                        foreach (GridViewRow gvrow in grdProcessOrder.Rows)
                        {
                            TextBox txtProcessOrder = (TextBox)gvrow.FindControl("txtProcessOrder");
                            Label lblProcessID = (Label)gvrow.FindControl("lblProcessID");

                            if (txtProcessOrder != null)
                                if(lblProcessID!=null)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        Business.DataRisk.AuditClosure ACClose = new Business.DataRisk.AuditClosure()
                                        {   
                                            CustomerbranchId = customerbranchid,
                                            FinancialYear = FinancialYear,
                                            ForPeriod = Period,
                                            VerticalID = VerticalID,
                                            ProcessId=Convert.ToInt32(lblProcessID.Text),
                                            ProcessOrder=Convert.ToInt32(txtProcessOrder.Text)
                                        };

                                        if (RiskCategoryManagement.AuditClosureRecordExists(ACClose))
                                            Success = RiskCategoryManagement.UpdateProcessOrderAuditClosure(ACClose);
                                        else
                                            Success = false;

                                    }
                                }
                        }
                    }

                    if (Success)
                    {
                        cvProcessOrderPopUp.IsValid = false;
                        cvProcessOrderPopUp.ErrorMessage = "Process Order Save Successfully.";
                    }
                    else
                    {
                        cvProcessOrderPopUp.IsValid = false;
                        cvProcessOrderPopUp.ErrorMessage = "Something went wrong....Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvProcessOrderPopUp.IsValid = false;
                cvProcessOrderPopUp.ErrorMessage = "Something went wrong....Please try again.";
            }
        }

        protected void btnProcessOrder_Click(object sender, EventArgs e)
        {
            try
            {               
                int customerbranchid = -1;
                int VerticalID = -1;
                string Period = string.Empty;
                string financialyear = string.Empty;                

                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
              
                if (commandArgs.Length > 1)
                {
                    if (Convert.ToString(commandArgs[0]) != null || Convert.ToString(commandArgs[0]) != "")
                    {
                        Period = Convert.ToString(commandArgs[0]);
                    }

                    if (Convert.ToString(commandArgs[1]) != null || Convert.ToString(commandArgs[1]) != "")
                    {
                        customerbranchid = Convert.ToInt32(commandArgs[1]);
                    }

                    if (Convert.ToString(commandArgs[2]) != null || Convert.ToString(commandArgs[2]) != "")
                    {
                        financialyear = Convert.ToString(commandArgs[2]);
                    }
                   
                    if (Convert.ToString(commandArgs[3]) != null || Convert.ToString(commandArgs[3]) != "")
                    {
                        VerticalID = Convert.ToInt32(commandArgs[3]);
                    }

                    ViewState["CustBranchID"] = null;
                    ViewState["Period"] = null;
                    ViewState["FinYear"] = null;
                    ViewState["VerticalID"] = null;

                    ViewState["CustBranchID"] = customerbranchid;
                    ViewState["Period"] = Period;
                    ViewState["FinYear"] = financialyear;
                    ViewState["VerticalID"] = VerticalID;                   

                    BindProcess(customerbranchid, Period, financialyear, VerticalID);

                    upComplianceDetails.Update();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindProcess(int CustomerBranchId, string ForPerid, string Financialyear, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchId != -1 && VerticalID != -1 && !string.IsNullOrEmpty(ForPerid) && !string.IsNullOrEmpty(Financialyear))
                {
                    var ProcessRecords = (from row in entities.ClosedAuditObservationDisplayViews
                                          where row.CustomerBranchId == CustomerBranchId
                                          && row.ForPeriod == ForPerid
                                          && row.FinancialYear == Financialyear
                                          && row.VerticalID == VerticalID
                                          select new
                                          {
                                              ProcessId = row.ProcessId,
                                              ProcessOrder=row.ProcessOrder,
                                              ProcessName = row.ProcessName,
                                              CustomerBranchId = row.CustomerBranchId,
                                              FinancialYear = row.FinancialYear,
                                              TermName = row.ForPeriod,
                                              VerticalID = row.VerticalID,
                                              VerticalName = row.VerticalName,
                                          }).Distinct().ToList();

                    grdProcessOrder.DataSource = null;
                    grdProcessOrder.DataBind();

                    grdProcessOrder.DataSource = null;
                    grdProcessOrder.DataBind();

                    grdProcessOrder.DataSource = ProcessRecords.ToList();
                    grdProcessOrder.DataBind();
                }
            }
        }

        public bool CheckEnable(int CustomerBranchId, string ForPerid, string Financialyear, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchId != -1 && VerticalID!=-1 && !string.IsNullOrEmpty(ForPerid) && !string.IsNullOrEmpty(Financialyear))
                {
                    var ProcessRecords = (from row in entities.ClosedAuditObservationDisplayViews
                                          where row.CustomerBranchId == CustomerBranchId
                                          && row.ForPeriod == ForPerid
                                          && row.FinancialYear == Financialyear
                                          && row.VerticalID == VerticalID
                                          select new
                                          {
                                              ProcessId = row.ProcessId,
                                              ProcessOrder = row.ProcessOrder,
                                              ProcessName = row.ProcessName,
                                              CustomerBranchId = row.CustomerBranchId,
                                              FinancialYear = row.FinancialYear,
                                              TermName = row.ForPeriod,
                                              VerticalID = row.VerticalID,
                                              VerticalName = row.VerticalName,
                                          }).Distinct().ToList();

                    var ProcessOrder = ProcessRecords.Where(entry => entry.ProcessOrder == null).ToList();

                    if (ProcessOrder.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }

    }
}