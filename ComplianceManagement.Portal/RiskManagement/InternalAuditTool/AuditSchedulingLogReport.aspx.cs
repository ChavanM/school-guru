﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditSchedulingLogReport : System.Web.UI.Page
    {
        public static List<int?> Branchlist = new List<int?>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool auditHeadFlag;
        protected int CustomerId = 0;
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFnancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
        }

        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if(role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
             
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerID, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerID, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerID, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerID, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType(); BindVertical();
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }


        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                int RevisedDateListCount = 0; // Added By Sagar More 26-11-2019
                try
                {
                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    string ForPeriod = "";
                    string FnancialYear = "";
                    int CustomerBranchId = -1;
                    string CustomerBranchName = "";
                    int VerticalID = -1;
                    string departmentheadFlag = string.Empty;
                    if (DepartmentHead)
                    {
                        departmentheadFlag = "DH";
                    }
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                            CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                            CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                            CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                            CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                            CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                    {
                        if (ddlFilterFinancial.SelectedValue != "-1")
                        {
                            FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                        }
                    }
                    else
                    {
                        FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    }
                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                        if (vid != -1)
                        {
                            VerticalID = vid;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                        {
                            if (ddlVertical.SelectedValue != "-1")
                            {
                                VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                            CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                        }
                    }
                    else
                    {
                        Branchlist.Clear();
                        GetAllHierarchy(CustomerId, CustomerBranchId);
                        Branchlist.ToList();
                    }



                    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            ForPeriod = ddlPeriod.SelectedItem.Text;
                        }
                    }
                    if (AuditHeadOrManagerReport == null)
                    {
                        AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
                    }
                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));
                    List<sp_InernalAuditScheduleReport_Result> masterquery = new List<sp_InernalAuditScheduleReport_Result>();
                    List<sp_InernalAuditScheduleReport_Result> table1 = new List<sp_InernalAuditScheduleReport_Result>();

                    if (string.IsNullOrEmpty(CustomerBranchName))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        return;
                    }
                    if (string.IsNullOrEmpty(ForPeriod))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Period.";
                        return;
                    }
                    if (ddlSchedulingType.SelectedValue == "-1")
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Scheduling Type.";
                        return;
                    }
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        masterquery = (from row in entities.sp_InernalAuditScheduleReport(CustomerId)
                                       select row).ToList();

                        if (Branchlist.Count > 0)
                        {
                            masterquery = masterquery.Where(Entry => Branchlist.Contains((int)Entry.CustomerBranchId)).ToList();
                        }
                        if (CustomerManagementRisk.CheckIsManagement(Portal.Common.AuthenticationHelper.UserID) == 8)
                        {
                            table1 = (from row in masterquery
                                      join EAAR in entities.EntitiesAssignmentManagementRisks
                                      on row.CustomerBranchId equals (int)EAAR.BranchID
                                      where EAAR.ProcessId == row.Process
                                      && EAAR.CustomerID == CustomerId
                                      && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                      && EAAR.ISACTIVE == true
                                      select row).Distinct().ToList();
                        }
                        else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            table1 = (from row in masterquery
                                      join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                      on row.CustomerBranchId equals (int)EAAR.BranchID
                                      where EAAR.ProcessId == row.Process
                                      && EAAR.CustomerID == CustomerId
                                      && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                      && EAAR.ISACTIVE == true
                                      select row).Distinct().ToList();
                        }
                        else if (departmentheadFlag == "DH")
                        {
                            table1 = (from row in masterquery
                                      join ICAA in entities.InternalControlAuditAssignments
                                      on row.AuditId equals ICAA.AuditID
                                      join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                      on row.CustomerBranchId equals EAAMR.BranchID
                                      join MSP in entities.Mst_Process
                                      on ICAA.ProcessId equals MSP.Id
                                      where EAAMR.DepartmentID == MSP.DepartmentID
                                      && EAAMR.CustomerID == CustomerId
                                      && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                      && EAAMR.ISACTIVE == true
                                      select row).Distinct().ToList();
                        }
                        else
                        {
                            table1 = (from row in masterquery
                                      join ICAA in entities.InternalControlAuditAssignments
                                      on row.CustomerBranchId equals (int)ICAA.CustomerBranchID
                                      where ICAA.AuditID == row.AuditId
                                      && ICAA.ProcessId == row.Process
                                      && ICAA.UserID == Portal.Common.AuthenticationHelper.UserID
                                      select row).Distinct().ToList();
                        }
                        if (VerticalID != -1)
                        {
                            table1 = table1.Where(entry => entry.VerticalID == VerticalID).ToList();
                        }

                        if (FnancialYear != "")
                        {
                            table1 = table1.Where(entry => entry.FinancialYear == FnancialYear).ToList();
                        }

                        if (ForPeriod != "")
                        {
                            table1 = table1.Where(entry => entry.TermName == ForPeriod).ToList();
                        }

                        var test = table1.GroupBy(aa => aa.Process).Select(grp => grp.ToList()).ToList();// 17-12-2019

                        DataTable table = new DataTable();
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Process", typeof(string));
                        table.Columns.Add("SubProcess", typeof(string));
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("OriginalDate", typeof(string));

                        string coulnName = string.Empty;
                        int RowCount2 = 0;
                        string strProcessName = string.Empty;
                        string strSubProcessName = string.Empty;
                        string strOldProcessName = string.Empty;
                        string strOldSubProcessName = string.Empty;


                        if (table1.Count > 0)
                        {

                            // get max count for revised Date
                            int cnt = 1;

                            foreach (var itemCount in table1)
                            {
                                string processNameCnt = string.Empty;
                                string subProcessNameCnt = string.Empty;
                                processNameCnt = itemCount.ProcessName;
                                subProcessNameCnt = itemCount.SubProcess;

                                int count = (from cs in table1
                                             where cs.ProcessName == processNameCnt
                                             && cs.SubProcess == subProcessNameCnt
                                             select cs).Count();
                                if (count > cnt)
                                {
                                    cnt = count;
                                }
                            }

                            // create revised date column
                            int revisedDateCnt = 1;
                            while (revisedDateCnt <= cnt)
                            {
                                coulnName = "RevisedDate" + Convert.ToString(revisedDateCnt);
                                Boolean columnExists = table.Columns.Contains(coulnName);
                                if (columnExists == false)
                                {
                                    table.Columns.Add(coulnName, typeof(string));
                                }
                                revisedDateCnt++;
                            }
                            long AuditId = 0;
                            long oldAuditId = 0;
                            int rowsCount = 0;
                            foreach (var item in table1)
                            {
                                strProcessName = item.ProcessName;
                                strSubProcessName = item.SubProcess;

                                AuditId = Convert.ToInt32(item.AuditId);
                                List<sp_InernalAuditScheduleReport_Result> table2 = (from cs in table1
                                                                                     where cs.ProcessName == item.ProcessName
                                                                                     && cs.SubProcess == item.SubProcess
                                                                                     && cs.AuditId == AuditId
                                                                                     select cs).ToList();

                                List<DateTime> startDateList = new List<DateTime>();
                                List<DateTime> endDateList = new List<DateTime>();
                                List<DateTime> oldStartDateList = new List<DateTime>();
                                List<DateTime> oldEndDateList = new List<DateTime>();

                                foreach (var itemtable2 in table2)
                                {
                                    startDateList.Add(Convert.ToDateTime(itemtable2.StartDate));
                                    endDateList.Add(Convert.ToDateTime(itemtable2.EndDate));
                                    oldStartDateList.Add(Convert.ToDateTime(itemtable2.StartDateOld));
                                    oldEndDateList.Add(Convert.ToDateTime(itemtable2.EndDateOld));
                                }


                                int tableColCnt = 5;
                                //while(tableColCnt < table.Columns.Count)
                                //{
                                if (strProcessName != strOldProcessName && strSubProcessName != strOldSubProcessName || string.IsNullOrEmpty(strOldProcessName) && string.IsNullOrEmpty(strOldSubProcessName))
                                {
                                    foreach (var itemoldEndDate in oldEndDateList)
                                    {
                                        foreach (var itemoldStartDate in oldStartDateList)
                                        {
                                            foreach (var itemendDate in endDateList)
                                            {
                                                foreach (var itemstartDate in startDateList)
                                                {
                                                    if (startDateList.Count == 1)
                                                    {
                                                        table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "Start Date", itemoldStartDate);
                                                        table.Rows[rowsCount][tableColCnt] = itemstartDate;
                                                        rowsCount++;
                                                    }

                                                    if (endDateList.Count == 1)
                                                    {
                                                        table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "End Date", item.EndDateOld);
                                                        table.Rows[rowsCount][tableColCnt] = itemendDate;
                                                        rowsCount++;
                                                        tableColCnt++;
                                                    }

                                                    if (startDateList.Count > 1)
                                                    {
                                                        string tempSdate = string.Empty;
                                                        string temp1SDate = string.Empty;
                                                        foreach (var itemSDate in startDateList)
                                                        {
                                                            if (string.IsNullOrEmpty(tempSdate))
                                                            {
                                                                table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "Start Date", itemoldStartDate);
                                                                table.Rows[rowsCount][tableColCnt] = itemSDate;
                                                                tempSdate = itemSDate.ToShortDateString();
                                                            }
                                                            else
                                                            {
                                                                tableColCnt++;
                                                                table.Rows[rowsCount][tableColCnt] = itemSDate;
                                                            }
                                                        }
                                                        rowsCount++;
                                                    }
                                                    if (endDateList.Count > 1)
                                                    {
                                                        string tempEdate = string.Empty;
                                                        tableColCnt = 5;
                                                        foreach (var itemEDate in endDateList)
                                                        {
                                                            if (string.IsNullOrEmpty(tempEdate))
                                                            {
                                                                table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "End Date", itemoldEndDate);
                                                                table.Rows[rowsCount][tableColCnt] = itemEDate;
                                                                tempEdate = itemEDate.ToShortDateString();
                                                            }
                                                            else
                                                            {
                                                                tableColCnt++;
                                                                table.Rows[rowsCount][tableColCnt] = itemEDate;
                                                            }
                                                        }
                                                        rowsCount++;
                                                    }
                                                    if (startDateList.Count > 1)
                                                    {
                                                        oldEndDateList = new List<DateTime>();
                                                        oldStartDateList = new List<DateTime>();
                                                        endDateList = new List<DateTime>();
                                                        startDateList = new List<DateTime>();
                                                    }
                                                    if (endDateList.Count > 1)
                                                    {
                                                        oldEndDateList = new List<DateTime>();
                                                        oldStartDateList = new List<DateTime>();
                                                        endDateList = new List<DateTime>();
                                                        startDateList = new List<DateTime>();
                                                    }
                                                    strOldProcessName = strProcessName;
                                                    strOldSubProcessName = strSubProcessName;
                                                    oldAuditId = AuditId;
                                                }
                                            }
                                        }
                                    }
                                }
                                //else
                                //{
                                //    foreach (var itemoldEndDate in oldEndDateList)
                                //    {
                                //        foreach (var itemoldStartDate in oldStartDateList)
                                //        {
                                //            foreach (var itemendDate in endDateList)
                                //            {
                                //                foreach (var itemstartDate in startDateList)
                                //                {
                                //                    if (startDateList.Count == 1)
                                //                    {
                                //                        table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "Start Date", itemoldStartDate);
                                //                        table.Rows[rowsCount][tableColCnt] = itemstartDate;
                                //                        rowsCount++;
                                //                    }

                                //                    if (endDateList.Count == 1)
                                //                    {
                                //                        table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "End Date", item.EndDateOld);
                                //                        table.Rows[rowsCount][tableColCnt] = itemendDate;
                                //                        rowsCount++;
                                //                        tableColCnt++;
                                //                    }

                                //                    if (startDateList.Count > 1)
                                //                    {
                                //                        string tempSdate = string.Empty;
                                //                        string temp1SDate = string.Empty;
                                //                        foreach (var itemSDate in startDateList)
                                //                        {
                                //                            if (string.IsNullOrEmpty(tempSdate))
                                //                            {
                                //                                table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "Start Date", itemoldStartDate);
                                //                                table.Rows[rowsCount][tableColCnt] = itemSDate;
                                //                                tempSdate = itemSDate.ToShortDateString();
                                //                            }
                                //                            else
                                //                            {
                                //                                tableColCnt++;
                                //                                table.Rows[rowsCount][tableColCnt] = itemSDate;
                                //                            }
                                //                        }
                                //                        rowsCount++;
                                //                    }
                                //                    if (endDateList.Count > 1)
                                //                    {
                                //                        string tempEdate = string.Empty;
                                //                        tableColCnt = 5;
                                //                        foreach (var itemEDate in endDateList)
                                //                        {
                                //                            if (string.IsNullOrEmpty(tempEdate))
                                //                            {
                                //                                table.Rows.Add(item.BrachName, item.ProcessName, item.SubProcess, "End Date", itemoldEndDate);
                                //                                table.Rows[rowsCount][tableColCnt] = itemEDate;
                                //                                tempEdate = itemEDate.ToShortDateString();
                                //                            }
                                //                            else
                                //                            {
                                //                                tableColCnt++;
                                //                                table.Rows[rowsCount][tableColCnt] = itemEDate;
                                //                            }
                                //                        }
                                //                        rowsCount++;
                                //                    }
                                //                    if (startDateList.Count > 1)
                                //                    {
                                //                        oldEndDateList = new List<DateTime>();
                                //                        oldStartDateList = new List<DateTime>();
                                //                        endDateList = new List<DateTime>();
                                //                        startDateList = new List<DateTime>();
                                //                    }
                                //                    if (endDateList.Count > 1)
                                //                    {
                                //                        oldEndDateList = new List<DateTime>();
                                //                        oldStartDateList = new List<DateTime>();
                                //                        endDateList = new List<DateTime>();
                                //                        startDateList = new List<DateTime>();
                                //                    }
                                //                    strOldProcessName = strProcessName;
                                //                    strOldSubProcessName = strSubProcessName;
                                //                    oldAuditId = AuditId;
                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                                //}
                            }


                            #region comment
                            //string processNameConst = string.Empty;
                            //string subProcessNameConst = string.Empty;
                            //string startDateConst = string.Empty;
                            //string endDateConst = string.Empty;
                            //string startOldDateValue = string.Empty;
                            //string endOldDateValue = string.Empty;

                            //foreach (var item in table1)
                            //{
                            //    //string processName = string.Empty;
                            //string subProcessName = string.Empty;
                            //string newStartDateValue = string.Empty;
                            //string newEndDateValue = string.Empty;



                            //processName = item.ProcessName;
                            //subProcessName = item.SubProcess;


                            //table1.Where(entry => entry.ProcessName == processName).ToList();
                            //if (!string.IsNullOrEmpty(newStartDateValue) && !string.IsNullOrEmpty(newEndDateValue))
                            //{
                            //if (item.ProcessName != processNameConst && item.SubProcess != subProcessNameConst)
                            //{
                            //    newStartDateValue = item.StartDate.ToString();
                            //    newEndDateValue = item.EndDate.ToString();
                            //    coulnName = "RevisedDate" + Convert.ToString(1);
                            //    Boolean columnExists = table.Columns.Contains(coulnName);
                            //    if (columnExists == false)
                            //    {
                            //        table.Columns.Add(coulnName, typeof(string));
                            //    }
                            //    table.Rows.Add(item.ProcessName, item.SubProcess, "Start Date", item.StartDateOld, newStartDateValue);
                            //    RowCount2++;
                            //    table.Rows.Add(item.ProcessName, item.SubProcess, "End Date", item.EndDateOld, newEndDateValue);
                            //    RowCount2++;
                            //    processNameConst = processName;
                            //    subProcessNameConst = subProcessName;
                            //    startDateConst = item.StartDate.ToString();
                            //    endDateConst = item.EndDate.ToString();
                            //    startOldDateValue = item.StartDateOld.ToString();
                            //    endOldDateValue = item.EndDateOld.ToString();
                            //}
                            //else if (item.ProcessName == processNameConst && item.SubProcess == subProcessNameConst)
                            //{
                            //    RowCount2++;
                            //    int coulmnCount = table.Columns.Count + 1;
                            //    coulnName = "RevisedDate" + Convert.ToString(2);
                            //    Boolean columnExists = table.Columns.Contains(coulnName);
                            //    if (columnExists == false)
                            //    {
                            //        table.Columns.Add(coulnName, typeof(string));
                            //    }
                            //    if (startDateConst != item.StartDate.ToString())
                            //    {
                            //        table.Rows.Add(item.ProcessName, item.SubProcess, "Start Date", startOldDateValue, newStartDateValue, item.StartDate);
                            //    }
                            //    else
                            //    {
                            //        table.Rows.Add(item.ProcessName, item.SubProcess, "Start Date", startOldDateValue, " ");
                            //    }
                            //    RowCount2++;
                            //    if (endDateConst != item.EndDate.ToString())
                            //    {
                            //        table.Rows.Add(item.ProcessName, item.SubProcess, "End Date", endOldDateValue, newEndDateValue, item.EndDate);
                            //    }
                            //    else
                            //    {
                            //        table.Rows[RowCount2][coulmnCount] = table.Rows.Add(item.ProcessName, item.SubProcess, "End Date", endOldDateValue, " ");
                            //    }
                            //    RowCount2++;
                            //}


                            //}



                            //for (int p = 4; p < table.Columns.Count; p++)
                            //{

                            //    if (p < table.Columns.Count)
                            //    {
                            //        table.Rows[RowCount2][p] = item.EndDate;
                            //    }

                            //}
                            //RowCount2++;

                            //} 
                            #endregion
                        }

                        #region MyRegion
                        //for (var i = 0; i < table1.Count; i++) //17-11
                        //{
                        //    string processName = string.Empty;
                        //    string subProcessName = string.Empty;
                        //    string startDateValue = string.Empty;
                        //    string endDateValue = string.Empty;
                        //    string checkProcessWalkThrough = string.Empty;
                        //    List<string> listEndDate = new List<string>();
                        //    List<string> listStartDate = new List<string>();
                        //    List<string> subProcessList = new List<string>();
                        //    string subProcessNameTemp = string.Empty;
                        //    int id = 1;




                        //    //foreach (var item in table1)
                        //    //{
                        //    //    if (string.IsNullOrEmpty(CustomerBranchName))
                        //    //    {
                        //    //        CustomerBranchName = item.BrachName;
                        //    //    }

                        //    //    if (string.IsNullOrEmpty(ForPeriod))
                        //    //    {
                        //    //        ForPeriod = item.TermName;
                        //    //    }

                        //    //    coulnName = "RevisedDate" + Convert.ToString(id);
                        //    //    Boolean columnExists = table.Columns.Contains(coulnName);
                        //    //    if (columnExists == false)
                        //    //    {
                        //    //        table.Columns.Add(coulnName, typeof(string));
                        //    //        id++;
                        //    //    }


                        //    //    #region MyRegion


                        //    //    if (processName != item.ProcessName && subProcessName != item.SubProcess)
                        //    //    {
                        //    //        subProcessNameTemp = subProcessName;
                        //    //        processName = item.ProcessName;
                        //    //        string checkprocessnamevalue = string.Empty;
                        //    //        checkprocessnamevalue = item.ProcessName;
                        //    //        if (!item.StartDateOld.Equals(item.StartDate))
                        //    //        {
                        //    //            subProcessName = item.SubProcess;
                        //    //            subProcessNameTemp = subProcessName;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "Start Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");
                        //    //            if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            {
                        //    //                table.Rows.Add(checkprocessnamevalue, subProcessName, "Start Date", item.StartDateOld);
                        //    //            }
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        if (!item.EndDateOld.Equals(item.EndDate))
                        //    //        {
                        //    //            subProcessName = item.SubProcess;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "End Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");
                        //    //            //if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            //{
                        //    //            table.Rows.Add(checkprocessnamevalue, subProcessName, "End Date", item.EndDateOld);
                        //    //            //}
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //        }
                        //    //        subProcessList.Add(subProcessName);
                        //    //    }
                        //    //    else if (processName == item.ProcessName && subProcessName != item.SubProcess)
                        //    //    {
                        //    //        processName = item.ProcessName;
                        //    //        string checkprocessnamevalue = string.Empty;
                        //    //        checkprocessnamevalue = item.ProcessName;
                        //    //        if (!item.StartDateOld.Equals(item.StartDate))
                        //    //        {
                        //    //            subProcessName = item.SubProcess;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "Start Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");
                        //    //            if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            {
                        //    //                table.Rows.Add(checkprocessnamevalue, subProcessName, "Start Date", item.StartDateOld);
                        //    //            }
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        if (!item.EndDateOld.Equals(item.EndDate))
                        //    //        {
                        //    //            subProcessName = item.SubProcess;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "End Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");
                        //    //            //if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            //{
                        //    //            table.Rows.Add(checkprocessnamevalue, subProcessName, "End Date", item.EndDateOld);
                        //    //            //}
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //        }
                        //    //        subProcessList.Add(subProcessName);
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        processName = item.ProcessName;
                        //    //        string checkprocessnamevalue = item.ProcessName;
                        //    //        if (!item.StartDateOld.Equals(item.StartDate))
                        //    //        {
                        //    //            string tempString = string.Empty;
                        //    //            tempString = subProcessName;
                        //    //            subProcessName = item.SubProcess;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "Start Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");
                        //    //            if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            {
                        //    //                table.Rows.Add(checkprocessnamevalue, subProcessName, "Start Date", item.StartDateOld);
                        //    //            }
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listStartDate.Add(item.StartDate.ToString());
                        //    //        }
                        //    //        if (!item.EndDateOld.Equals(item.EndDate))
                        //    //        {
                        //    //            string tempString = string.Empty;
                        //    //            tempString = subProcessName;
                        //    //            subProcessName = item.SubProcess;
                        //    //            DataRow[] foundProcess = table.Select("Process = '" + checkprocessnamevalue + "'");
                        //    //            DataRow[] foundTitle = table.Select("Title = '" + "End Date" + "'");
                        //    //            DataRow[] foundSubProcess = table.Select("SubProcess = '" + item.SubProcess + "'");

                        //    //            #region need to discuss with sushant - change by sagar on 21-06-2020
                        //    //            if (foundProcess.Length == 0 || foundSubProcess.Length == 0 || foundTitle.Length == 0)
                        //    //            {
                        //    //                table.Rows.Add(checkprocessnamevalue, subProcessName, "End Date", item.EndDateOld);
                        //    //            }
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //            #endregion
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            listEndDate.Add(item.EndDate.ToString());
                        //    //        }
                        //    //        if (subProcessNameTemp != subProcessName)
                        //    //        {
                        //    //            subProcessList.Add(subProcessName);
                        //    //        }

                        //    //    }
                        //    //    #region Added by Sagar more on 21-06-2020. need to discuss with sushant
                        //    //    //int listStartDateCount = 0;
                        //    //    //if (RowCount2 != 0)
                        //    //    //{
                        //    //    //    RowCount2++;
                        //    //    //}
                        //    //    //for (int p = 4; p < table.Columns.Count;)
                        //    //    //{
                        //    //    //    foreach (var listStartDateItem in listStartDate)
                        //    //    //    {
                        //    //    //        if (listStartDateCount != listStartDate.Count)
                        //    //    //        {
                        //    //    //            if (p < table.Columns.Count)
                        //    //    //            {
                        //    //    //                table.Rows[RowCount2][p] = listStartDateItem;
                        //    //    //                p++;
                        //    //    //                listStartDateCount++;
                        //    //    //            }
                        //    //    //        }
                        //    //    //        else
                        //    //    //        {
                        //    //    //            p++;
                        //    //    //        }
                        //    //    //    }
                        //    //    //}
                        //    //    //RowCount2++;
                        //    //    //for (int q = 4; q < table.Columns.Count;)
                        //    //    //{
                        //    //    //    foreach (var listEndDateItem in listEndDate)
                        //    //    //    {
                        //    //    //        if (q < table.Columns.Count)
                        //    //    //        {
                        //    //    //            table.Rows[RowCount2][q] = listEndDateItem;
                        //    //    //            q++;
                        //    //    //        }
                        //    //    //    }
                        //    //    //    break;
                        //    //    //}
                        //    //    //RowCount2++;
                        //    //    #endregion
                        //    //    #endregion
                        //    //}

                        //    #region
                        //    //if (subProcessList.Count > 1)

                        //    //#region comment by sagar on 21-06-2020
                        //    //int listStartDateCount = 0;
                        //    //for (int p = 4; p < table.Columns.Count;)
                        //    //{
                        //    //    foreach (var item in listStartDate)
                        //    //    {
                        //    //        if (listStartDateCount != listStartDate.Count)
                        //    //        {
                        //    //            if (p < table.Columns.Count)
                        //    //            {
                        //    //                table.Rows[RowCount2][p] = item;
                        //    //                p++;
                        //    //                listStartDateCount++;
                        //    //            }
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            p++;
                        //    //        }
                        //    //    }
                        //    //}
                        //    //RowCount2++;
                        //    //for (int q = 4; q < table.Columns.Count;)
                        //    //{
                        //    //    foreach (var item in listEndDate)
                        //    //    {
                        //    //        if (q < table.Columns.Count)
                        //    //        {
                        //    //            table.Rows[RowCount2][q] = item;
                        //    //            q++;
                        //    //        }
                        //    //    }
                        //    //    break;
                        //    //}
                        //    //RowCount2++;
                        //    //#endregion
                        //    #endregion
                        //} 
                        #endregion

                        List<FetchDataAuditSchedulingReport> tabletest = new List<FetchDataAuditSchedulingReport>();
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            string Date1 = string.Empty;
                            string Date2 = string.Empty;
                            int cnt = 1; string columnName = string.Empty;
                            List<string> Date2List = new List<string>();
                            FetchDataAuditSchedulingReport obj = new FetchDataAuditSchedulingReport();
                            obj.Location = table.Rows[i]["Location"].ToString();
                            obj.Process = table.Rows[i]["Process"].ToString();
                            obj.SubProcess = table.Rows[i]["SubProcess"].ToString();
                            obj.Title = table.Rows[i]["Title"].ToString();
                            Date1 = table.Rows[i]["OriginalDate"].ToString();
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                coulnName = "RevisedDate" + Convert.ToString(cnt);
                                Date2List.Add(table.Rows[i][coulnName].ToString());
                                cnt++;
                            }
                            Date2 = table.Rows[i]["RevisedDate1"].ToString();// Change By Sagar More 26-11-2019 
                            if (!string.IsNullOrEmpty(Date1))
                            {
                                obj.OriginalDate = Convert.ToDateTime(Date1).ToString("dd/MM/yyyy");
                            }
                            if (!string.IsNullOrEmpty(Date2))
                            {
                                obj.RevisedDate = Convert.ToDateTime(Date2).ToString("dd/MM/yyyy");
                            }
                            if (Date2List.Count > 0)
                            {
                                obj.RevisedDateList = new List<string>();
                                foreach (string date in Date2List)
                                {
                                    if (date != "")
                                    {
                                        string rDate = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                                        obj.RevisedDateList.Add(rDate);
                                    }
                                    else
                                    {
                                        obj.RevisedDateList.Add(date);
                                    }
                                }
                            }
                            if (obj.RevisedDateList.Count > RevisedDateListCount)
                            {
                                RevisedDateListCount = obj.RevisedDateList.Count;
                            }
                            tabletest.Add(obj);
                        }
                        //tabletest = tabletest.OrderBy(x => x.Process).ToList(); //.ThenBy(x => x.Title)
                        List<FetchDataAuditSchedulingReport> tabletestfinal = new List<FetchDataAuditSchedulingReport>();
                        string prcname = string.Empty;
                        string subprcname = string.Empty;
                        string auditstepname = string.Empty;
                        string checktitle = string.Empty;
                        foreach (var item in tabletest)
                        {
                            //if (String.IsNullOrEmpty(CustomerBranchName))
                            //{
                            //    CustomerBranchName = item.bra
                            //}
                            if (!prcname.Equals(item.Process))
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                checktitle = item.Title;
                                //item.Location = CustomerBranchName;// Added By Sagar More 26-11-2019 
                                tabletestfinal.Add(item);
                            }
                            //else
                            //if (!subprcname.Equals(item.SubProcess))
                            //{
                            //    prcname = item.Process;
                            //    subprcname = item.SubProcess;
                            //    checktitle = item.Title;
                            //    item.Location = "";
                            //    item.Process = "";
                            //    tabletestfinal.Add(item);
                            //}
                            else
                            {
                                prcname = item.Process;
                                subprcname = item.SubProcess;
                                checktitle = item.Title;
                                item.Process = "";
                                item.SubProcess = "";
                                item.Location = "";
                                tabletestfinal.Add(item);
                            }
                        }
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Scheduling Deviation Report");
                        DataTable ExcelData = null;
                        DataView view1 = new System.Data.DataView((tabletestfinal as List<FetchDataAuditSchedulingReport>).ToDataTable());

                        List<string> strList = new List<string>();
                        ExcelData = view1.ToTable("Selected", false, "Location", "Process", "SubProcess", "Title", "OriginalDate");
                        int RowCnt1 = 0;
                        foreach (DataRowView rowView in view1)
                        {
                            DataRow row = rowView.Row;
                            strList = row["RevisedDateList"] as List<string>;
                            for (int i = 1; i <= strList.Count; i++)
                            {
                                coulnName = "RevisedDate" + Convert.ToString(i);
                                bool b = ExcelData.Columns.Contains(coulnName);
                                DataColumnCollection columns = ExcelData.Columns;
                                if (!columns.Contains(coulnName))
                                {
                                    ExcelData.Columns.Add("RevisedDate" + Convert.ToString(i), typeof(string));
                                }
                            }
                            for (int j = 5; j < ExcelData.Columns.Count;)
                            {
                                foreach (var item in strList)
                                {
                                    ExcelData.Rows[RowCnt1][j] = item;
                                    j++;
                                }
                            }
                            RowCnt1++;
                        }
                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].AutoFitColumns(50);
                        exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].AutoFitColumns(50);
                        exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["A2"].Value = "Name of Group:";
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A2"].AutoFitColumns(50);
                        exWorkSheet.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["B2"].Value = customer;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B2"].AutoFitColumns(50);
                        exWorkSheet.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["E6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["A3"].Value = "Financial Year:";
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet.Cells["B3"].Value = FnancialYear;
                        exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B3"].AutoFitColumns(50);

                        exWorkSheet.Cells["A4"].Value = "Period:";
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].AutoFitColumns(50);

                        exWorkSheet.Cells["B4"].Value = ForPeriod;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].AutoFitColumns(50);

                        exWorkSheet.Cells["A5"].Value = "Type Of Report:";
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].AutoFitColumns(50);

                        exWorkSheet.Cells["B5"].Value = "Audit Scheduling Deviation Report";
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].AutoFitColumns(50);

                        exWorkSheet.Cells["A8"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet.Cells["A8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A8"].Value = "Location";
                        exWorkSheet.Cells["A8"].AutoFitColumns(25);

                        exWorkSheet.Cells["B8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B8"].Value = "Process";
                        exWorkSheet.Cells["B8"].AutoFitColumns(25);

                        exWorkSheet.Cells["C8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C8"].Value = "SubProcess";
                        exWorkSheet.Cells["C8"].AutoFitColumns(45);

                        exWorkSheet.Cells["D8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D8"].Value = "Title";
                        exWorkSheet.Cells["D8"].AutoFitColumns(15);

                        exWorkSheet.Cells["E8"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E8"].Value = "Original Date";
                        exWorkSheet.Cells["E8"].AutoFitColumns(15);

                        int coloumncreationcount = 70;
                        char lastSVal = 'E';
                        foreach (DataColumn dataColumn in ExcelData.Columns)
                        {
                            char lastcategoryprintcount = Convert.ToChar(coloumncreationcount);
                            char sval = Convert.ToChar(coloumncreationcount);
                            lastSVal = sval;
                            string columnName = dataColumn.ColumnName;
                            string strRevisedDateColumn = "RevisedDate";
                            if (columnName.Contains(strRevisedDateColumn))
                            {

                                exWorkSheet.Cells["" + sval + "8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["" + sval + "8"].Value = columnName;
                                exWorkSheet.Cells["" + sval + "8"].AutoFitColumns(15);
                                lastcategoryprintcount = sval;
                                coloumncreationcount++;
                            }
                        }

                        if (ExcelData.Rows.Count > 0)
                        {
                            int columnCount = 5 + RevisedDateListCount;
                            using (ExcelRange col = exWorkSheet.Cells[8, 1, 8 + ExcelData.Rows.Count, columnCount])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                //exWorkSheet.Cells["A8:E8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet.Cells["A8:E8"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet.Cells["A8:" + lastSVal + "8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet.Cells["A8:" + lastSVal + "8"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            CustomerBranchName = CustomerBranchName.Replace(',', ' ');
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("content-disposition", "attachment;filename=SchedulingDeviationReport_" + CustomerBranchName + ".xlsx");
                            Response.BinaryWrite(fileBytes);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Record Found.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }

        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            /*
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            */
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            bool DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
    }
}