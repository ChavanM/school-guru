﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class PersonResponsibleMainUI_IMP : System.Web.UI.Page
    {
        public string mgmtResponse = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["FinYear"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                        {
                                            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                            {
                                                ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["FinYear"] + ";" + Request.QueryString["Period"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["AuditID"];
                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                //GetPageDisplaySummary();
                                                bindPageNumber();
                                            }
                                        }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());


                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static ImplementationAuditResult GetImplementationAuditResultbyID(long AuditScheduleOnID, int ResultID, long CustomerBranchId, int Verticalid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.ImplementationAuditResults
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ResultID == ResultID
                                     && row.CustomerBranchId == CustomerBranchId && row.VerticalID == Verticalid
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        protected void grdSummaryDetailsAuditCoverageIMP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var details = RiskCategoryManagement.FillUsers(customerID);
            string statusidasd = Request.QueryString["Status"].ToString();

            if (statusidasd.Equals("AuditeeReview"))
            {
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    chkBx.Enabled = true;
                    Label lblResultID = (Label)rw.FindControl("lblResultID");
                    Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                    Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                    Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                    Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                    var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                    if (implementationdetails != null)
                    {
                        Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                        if (implementationdetails.ManagementResponse != "")
                        {
                            tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                            //mgmtResponse = implementationdetails.ManagementResponse;
                        }
                        tbxMgmResponce.Enabled = true;

                        DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                        ddlPersonResponsible.Enabled = false;
                        if (ddlPersonResponsible != null)
                        {
                            ddlPersonResponsible.DataTextField = "Name";
                            ddlPersonResponsible.DataValueField = "ID";
                            ddlPersonResponsible.DataSource = details;
                            ddlPersonResponsible.DataBind();
                            ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                        {
                            ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                        }

                        TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                        txtStartDate.Enabled = true;
                        txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                        DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                        ddlIMPStatus.Enabled = true;
                        if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                        {
                            ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                        }
                    }
                }
            }
            else
            {
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    chkBx.Enabled = false;
                    Label lblResultID = (Label)rw.FindControl("lblResultID");
                    Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                    Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                    Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                    Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                    var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                    if (implementationdetails != null)
                    {
                        Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                        if (implementationdetails.ManagementResponse != "")
                        {
                            tbxMgmResponce.Text= implementationdetails.ManagementResponse;
                            //mgmtResponse = implementationdetails.ManagementResponse;
                        }
                        tbxMgmResponce.Enabled = false;

                        DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                        ddlPersonResponsible.Enabled = false;
                        if (ddlPersonResponsible != null)
                        {
                            ddlPersonResponsible.DataTextField = "Name";
                            ddlPersonResponsible.DataValueField = "ID";
                            ddlPersonResponsible.DataSource = details;
                            ddlPersonResponsible.DataBind();
                            ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                        {
                            ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                        }

                        TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                        txtStartDate.Enabled = false;
                        txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                        DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                        ddlIMPStatus.Enabled = false;
                        if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                        {
                            ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                        }
                    }
                }
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverageIMP.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindDetailView(ViewState["Arguments"].ToString(), "P");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {          
            string url4 = "";
            string AuditID = "";
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Request.QueryString["AuditID"];
            }
            string FinYear = "";
            if (!String.IsNullOrEmpty(Request.QueryString["FinYear"]))
            {
                FinYear = Request.QueryString["FinYear"];
            }

            string Type = "";
            if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
            {
                Type = Request.QueryString["Type"];
            }

            string tag = "";
            if (!String.IsNullOrEmpty(Request.QueryString["tag"]))
            {
                tag = Request.QueryString["tag"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            {                            
                url4 =  Request.QueryString["ReturnUrl1"].Replace("@", "=");
            }            
            Response.Redirect("~/RiskManagement/AuditTool/PersonResponsibleStatusUI.aspx?Type="+ Type + "&" + url4 + "&tag="+ tag + "&FinYear="+ FinYear + "&AuditID=" + AuditID);           
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        {
            try
            {
                int ScheduledonID = -1;
                int ResultID = -1;
                int AuditStatusID = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                long AuditID = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    ResultID = Convert.ToInt32(commandArgs[0]);
                    ViewState["ResultID"] = null;
                    ViewState["ResultID"] = ResultID;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[5]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[5]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                if (!string.IsNullOrEmpty(commandArgs[6]))
                {
                    ScheduledonID = Convert.ToInt32(commandArgs[6]);
                    ViewState["ScheduledonID"] = null;
                    ViewState["ScheduledonID"] = ScheduledonID;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    AuditStatusID = Convert.ToInt32(commandArgs[4]);
                    ViewState["AuditStatusID"] = null;
                    ViewState["AuditStatusID"] = AuditStatusID;
                }
                if (!string.IsNullOrEmpty(commandArgs[7]))
                {
                    AuditID = Convert.ToInt64(commandArgs[7]);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialog(" + ResultID + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "'," + VerticalId + "," + ScheduledonID + "," + AuditStatusID + "," + AuditID + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LinkButton btn = (LinkButton)(sender);
        //        String Args = btn.CommandArgument.ToString();

        //        if (Args != "")
        //        {
        //            string[] arg = Args.ToString().Split(',');

        //            if (arg[4] == "")
        //                arg[4] = "1";

        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + ");", true);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }

        public bool ATBDVisibleorNot(int? StatusID)
        {
            if (ViewState["roleid"] != null)
            {
                if (StatusID == null && Convert.ToInt32(ViewState["roleid"]) == 4)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private void BindDetailView(string Arguments, String IFlag)
        {
            int BranchID = -1;
            string financialyear = String.Empty;
            string period = String.Empty;
            string filter = String.Empty;
            int VerticalID = -1;
            int userid = -1;
            long AuditID = -1;
            userid = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            string tag = string.Empty;
            string AudResponse = string.Empty;
            List<string> AudResponseList = new List<string>();
            if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["tag"])))
            {
                tag = Convert.ToString(Request.QueryString["tag"]);
            }
            if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["Audittee"])))
            {
                AudResponse = Convert.ToString(Request.QueryString["Audittee"]);
                if (AudResponse== "AS")
                {
                    AudResponseList.Add("AS");
                    AudResponseList.Add("R2");
                    tag = null;
                }
                else if(AudResponse== "PS")
                {
                    AudResponseList.Add("PS");
                    AudResponseList.Add("R3");
                    AudResponseList.Add("2R");
                    tag = null;
                }
                else if(AudResponse=="PA")
                {
                    AudResponseList.Add("PA");
                    tag = null;
                }
                else if (AudResponse == "AR")
                {
                    AudResponseList.Add("AR");
                    tag = null;
                }
                else if (AudResponse == "RS")
                {
                    AudResponseList.Add("RS");
                    tag = null;
                }
                else if (AudResponse == "AC")
                {
                    AudResponseList.Add("AC");
                    tag = null;
                }
                else
                {
                    AudResponseList.Add(null);
                }
            }
            else
            {
                AudResponseList.Add(null);
            }
            int ScheduledonID = -1;
            string[] arg = Arguments.ToString().Split(';');

            if (arg.Length > 0)
            {
                ScheduledonID = Convert.ToInt32(arg[2]);
                BranchID = Convert.ToInt32(arg[3]);
                financialyear = Convert.ToString(arg[4]);
                period = Convert.ToString(arg[5]);
                VerticalID = Convert.ToInt32(arg[6]);
                AuditID = Convert.ToInt64(arg[7]);
                List<int> statusIds = new List<int>();
                List<int?> statusNullableIds = new List<int?>();

                filter = arg[0].Trim();

                if (arg[0].Trim().Equals("NotDone"))
                {
                    statusIds.Add(-1);
                }
                else if (arg[0].Trim().Equals("Submited"))
                {
                    statusIds.Add(2);
                }
                else if (arg[0].Trim().Equals("TeamReview"))
                {
                    statusIds.Add(4);
                }
                else if (arg[0].Trim().Equals("Closed"))
                {
                    statusIds.Add(3);
                }
                else if (arg[0].Trim().Equals("AuditeeReview"))
                {
                    statusIds.Add(6);
                }
                else if (arg[0].Trim().Equals("FinalReview"))
                {
                    statusIds.Add(5);
                }
                else
                {
                    statusIds.Add(1);
                    statusIds.Add(4);
                    statusIds.Add(2);
                    statusIds.Add(3);
                    statusIds.Add(5);
                    statusIds.Add(6);
                }

                if (IFlag == "P")
                {
                    var detailView = InternalControlManagementDashboardRisk.GetPersonResponsibleAuditStepsIMP(customerID, BranchID, VerticalID, financialyear, ScheduledonID, period, userid, 3, statusIds, statusNullableIds, filter, AuditID, tag, AudResponseList);
                    Session["TotalRows"] = detailView.Count;
                    grdSummaryDetailsAuditCoverageIMP.DataSource = detailView;
                    grdSummaryDetailsAuditCoverageIMP.DataBind();
                }
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverageIMP.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                //GetPageDisplaySummary();

            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnAllsave_click(object sender, EventArgs e)
        {
            try
            {
                string statusidasd = Request.QueryString["Status"].ToString();

                if (statusidasd.Equals("AuditeeReview"))
                {
                    int chkValidationflag = 0;
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        if (chkBx != null && chkBx.Checked)
                        {
                            chkValidationflag = chkValidationflag + 1;
                        }
                    }
                    if (chkValidationflag > 0)
                    {
                        string ManagementResponse = string.Empty;
                        string TimeLineValue = null;
                        string PersonResponsible = "-1";
                        string FinalStatusIMP = "-1";

                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                            if (chkBx != null && chkBx.Checked)
                            {
                                long roleid = 4;
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                ManagementResponse = ((TextBox)rw.FindControl("tbxMgmResponce")).Text;
                                TimeLineValue = ((TextBox)rw.FindControl("txtStartDate")).Text;
                                PersonResponsible = ((DropDownList)rw.FindControl("ddlPersonResponsible")).SelectedValue;
                                FinalStatusIMP = ((DropDownList)rw.FindControl("ddlIMPStatus")).SelectedValue;

                                if (FinalStatusIMP != "" && FinalStatusIMP != "-1")
                                {
                                    LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatusIMP");
                                    String Args = chklnkbutton.CommandArgument.ToString();
                                    string[] arg = Args.ToString().Split(',');

                                    long ResultID = Convert.ToInt32(arg[0]);
                                    long CustomerbranchID = Convert.ToInt32(arg[1]);
                                    string FinancialYear = arg[2].ToString();
                                    string ForMonth = arg[3].ToString();
                                    long VerticalID = Convert.ToInt32(arg[4]);
                                    long Scheduledonid = Convert.ToInt32(arg[5]);
                                    long AuditID = Convert.ToInt32(arg[7]);
                                    //CommandArgument = '<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "
                                    //," + Eval("ForMonth") +","+Eval("VerticalID")+","+Eval("ScheduledOnID")+","+Eval("AuditStatusID") %>' >
                                    var getImpAuditScheduleOnByDetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinancialYear, ForMonth, Convert.ToInt32(CustomerbranchID), Convert.ToInt32(VerticalID), Convert.ToInt32(Scheduledonid), AuditID);
                                    if (getImpAuditScheduleOnByDetails != null)
                                    {

                                        #region
                                        ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                                        {
                                            AuditScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = getImpAuditScheduleOnByDetails.FinancialYear,
                                            ForPeriod = getImpAuditScheduleOnByDetails.ForMonth,
                                            CustomerBranchId = CustomerbranchID,
                                            IsDeleted = false,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };
                                        AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                                        {
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                            ImplementationScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = FinancialYear,
                                            CustomerBranchId = CustomerbranchID,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            ForPeriod = ForMonth,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };

                                        if (ManagementResponse.Trim() == "")
                                            MstRiskResult.ManagementResponse = "";
                                        else
                                            MstRiskResult.ManagementResponse = ManagementResponse.Trim();

                                        if (PersonResponsible == "-1")
                                        {
                                            MstRiskResult.PersonResponsible = null;
                                            transaction.PersonResponsible = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                            transaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                        }
                                        if (FinalStatusIMP == "-1")
                                        {
                                            MstRiskResult.ImplementationStatus = null;
                                            transaction.ImplementationStatusId = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.ImplementationStatus = Convert.ToInt32(FinalStatusIMP);
                                            transaction.ImplementationStatusId = Convert.ToInt32(FinalStatusIMP);
                                        }
                                        DateTime dt = new DateTime();
                                        if (!string.IsNullOrEmpty(TimeLineValue.Trim()))
                                        {
                                            dt = DateTime.ParseExact(TimeLineValue.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            MstRiskResult.TimeLine = dt.Date;
                                        }
                                        else
                                            MstRiskResult.TimeLine = null;

                                        string remark = string.Empty;


                                        transaction.StatusId = 6;
                                        MstRiskResult.Status = 6;
                                        remark = "Implementation Status Under Auditee Review.";

                                        transaction.Remarks = remark.Trim();
                                        #endregion

                                        var MstRiskResultchk = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(Convert.ToInt32(ResultID), Convert.ToInt32(VerticalID), Convert.ToInt32(getImpAuditScheduleOnByDetails.Id), AuditID);
                                        if (MstRiskResultchk != null)
                                        {
                                            #region
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                            {
                                                MstRiskResult.ProcessWalkthrough = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ActualWorkDone))
                                            {
                                                MstRiskResult.ActualWorkDone = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ActualWorkDone = MstRiskResultchk.ActualWorkDone;
                                            }

                                            if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                            {
                                                MstRiskResult.Population = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Population = MstRiskResultchk.Population;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                            {
                                                MstRiskResult.Sample = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Sample = MstRiskResultchk.Sample;
                                            }

                                            if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                            {
                                                MstRiskResult.FixRemark = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;
                                            }
                                            #endregion
                                        }
                                        bool Success1 = false;
                                        bool Success2 = false;
                                        bool isvalidate = true;
                                        string msg = string.Empty;
                                        if (string.IsNullOrEmpty(MstRiskResult.ManagementResponse))
                                        {
                                            isvalidate = false;
                                            msg += "Management Response,";
                                        }
                                        if (MstRiskResult.PersonResponsible == null)
                                        {
                                            isvalidate = false;
                                            msg += "Person Responsible,";
                                        }
                                        if (MstRiskResult.TimeLine == null)
                                        {
                                            isvalidate = false;
                                            msg += "Time Line";
                                        }
                                        if (MstRiskResult.ImplementationStatus == null)
                                        {
                                            isvalidate = false;
                                            msg += "Implementation Status";
                                        }
                                        if (isvalidate)
                                        {
                                            if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                            }
                                            else
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                            }

                                            if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                                            {
                                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.UpdatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                            }
                                            else
                                            {
                                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.UpdatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                            }
                                            if (Success1 == true && Success2 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Implementation Steps Submitted Successfully";
                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Provide " + msg.Trim(',') + " Before Save";
                                            //cvDuplicateEntry.ErrorMessage = "Please Provide Implementation Status,Person Responsible,Time Line and Management Reponse Before Save";
                                        }
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Implementation Status";
                                    break;
                                }
                            }
                        } //For Each Loop End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnOldAudit_Click(object sender, EventArgs e)
        {
            try
            {
                int ScheduledonID = -1;
                int ResultID = -1;
                int AuditStatusID = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                long AuditID = -1;
                int ProcessID = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    ResultID = Convert.ToInt32(commandArgs[0]);
                    ViewState["ResultID"] = null;
                    ViewState["ResultID"] = ResultID;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[5]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[5]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                if (!string.IsNullOrEmpty(commandArgs[6]))
                {
                    ScheduledonID = Convert.ToInt32(commandArgs[6]);
                    ViewState["ScheduledonID"] = null;
                    ViewState["ScheduledonID"] = ScheduledonID;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    AuditStatusID = Convert.ToInt32(commandArgs[4]);
                    ViewState["AuditStatusID"] = null;
                    ViewState["AuditStatusID"] = AuditStatusID;
                }
                if (!string.IsNullOrEmpty(commandArgs[7]))
                {
                    AuditID = Convert.ToInt64(commandArgs[7]);
                }
                if (!string.IsNullOrEmpty(commandArgs[8]))
                {
                    ProcessID = Convert.ToInt32(commandArgs[8]);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialogAuditReviwer(" + ResultID + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "'," + VerticalId + "," + ScheduledonID + "," + AuditStatusID + "," + AuditID + "," + ProcessID + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}