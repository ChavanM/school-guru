﻿<%@ Page Title="Observation Report (Word)" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FrmObservationReportWord.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmObservationReportWord" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //setactivemenu('Observation Report');
            fhead('Observation Report');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 ">
                  <section class="panel"> 
                      <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3) || roles.Contains(4))%>
                                           <%{%>
                                                 <% if (AuditHeadOrManagerReport == null)%>
                                                 <%{%>
                                                <li style="float: right;">   
                                                    <div>
                                                        <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                        <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                            <ul class="dropdown-menu">                                                                                                                                                                                                                                                                  
                                                                <li><a href="../AuditTool/MyReportAudit.aspx">Final Audit</a></li>
                                                                <li><a href="../InternalAuditTool/AuditObservationDraft.aspx">Draft Observation</a></li>                                                                                                                                    
                                                                <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation-Excel</a></li>
                                                                <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li> 
                                                                <li><a href="../InternalAuditTool/PastAuditUpload.aspx">Past Audit Reports</a></li>                                       
                                                                 <%--  <li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>
                                                                 <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                  
                                                                <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li>  --%>                                                      
                                                            </ul>
                                                    </div>
                                                </li>   
                                                <%}%>
                                           <%}%>                                                 
                                    </ul>
                                </header>
                      </section>
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" CssClass="vdsummary"
                                ValidationGroup="EventValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="EventValidationGroup" Display="None" />
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">                            
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                              <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" DataPlaceHolder="Vertical" 
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" Width="90%" Height="32px">
                                </asp:DropDownListChosen>
                            </div>
                              <%}%>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged1">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div style="width: 90%; text-align: right;">
                                    <asp:Button ID="btnExportDoc" runat="server" Text="Export to Word" OnClick="btnExportDoc_Click" CssClass="btn btn-primary" ValidationGroup="EventValidationGroup" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportDoc" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


</asp:Content>
