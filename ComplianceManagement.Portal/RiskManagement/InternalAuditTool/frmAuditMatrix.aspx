﻿<%@ Page Title="Audit Matrix" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="frmAuditMatrix.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.frmAuditMatrix" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
              .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
  
    </style>
    <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">
        function fopenpopup() {
            $('#divAuditFrequencyDialog').modal('show');
        };
        function CloseEvent() {
            $('#divAuditFrequencyDialog').modal('hide');
            window.location.reload();
        }
    </script>

    <script type="text/javascript">
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        };
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Matrix');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0" style="width: 30%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                                   
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>                 
                               

                                <div style="text-align:right">
                                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddPromotor" CssClass="btn btn-primary" OnClientClick="fopenpopup()" OnClick="btnAddPromotor_Click" /></td>
                                </div>
                              </div>
                              <div class="clearfix"></div>    
                            <div class="col-md-12 colpadding0">  
                                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                </div>
                                                                                            
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>

                                 <div class="col-md-3 colpadding0" style="margin-top: 5px;">                        
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" DataPlaceHolder="Locations">
                                    </asp:DropDownListChosen>
                                </div> 
                            </div>
                            <div class="clearfix"></div>

                            <div style="margin-bottom: 4px">                 
                                <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" 
                                 DataKeyNames="CustomerBranchID" OnRowCommand="grdAuditor_RowCommand"
                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"  Width="100%"  AllowSorting="true"
                                OnPageIndexChanging="grdAuditor_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField  HeaderText="Sr" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClient" runat="server" Text='<%# GetCustomerBranchName((long)Eval("CustomerBranchID")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                               
                                        <asp:TemplateField  HeaderText="Action">
                                            <ItemTemplate>
                                              <%--  <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_PROMOTER" OnClientClick="fopenpopup()"
                                                CommandArgument='<%# Eval("CustomerBranchID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Audit Matrix" title="Edit Audit Matrix Details" /></asp:LinkButton>--%>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_PROMOTER"
                                                CommandArgument='<%# Eval("CustomerBranchID") %>' OnClientClick="return confirm('Are you certain you want to delete this Audit Matrix Details?');"><img src="../../Images/delete_icon_new.png" alt="Delete Audit Matrix Details" title="Delete Audit Matrix Details" /></asp:LinkButton>
                                            </ItemTemplate>                          
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />            
                                                  <PagerSettings Visible="false" />        
                    <PagerTemplate>
                        <%--<table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>--%>
                        </PagerTemplate>
                                </asp:GridView>
                            </div>
                          <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">
                                        <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                        <div class="table-paging-text" style="float: right;">
                                            <p>Page
                                           <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                            </p>
                                        </div>
                                        <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>                                       
                      </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
 
    <div class="modal fade" id="divAuditFrequencyDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="margin-left: 13%;">
            <div class="modal-content" style="width: 1000px; height:600px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAuditorDialog">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntityPopPup" DataPlaceHolder="Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width: 90%;"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntityPopPup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1PopPup" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1PopPup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2PopPup" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2PopPup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3PopPup" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3PopPup_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                      <div class="clearfix"></div>
                                     <div class="col-md-12 colpadding0">                                  
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListchosen runat="server" ID="ddlFilterLocationPopPup" DataPlaceHolder="Location" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocationPopPup_SelectedIndexChanged" />                                     
                                             <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Location"
                                                ControlToValidate="ddlFilterLocationPopPup" runat="server" ValidationGroup="PromotorValidationGroup"
                                                Display="None"/>  
                                                <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Location"
                                                ControlToValidate="ddlFilterLocationPopPup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="PromotorValidationGroup" Display="None" />  --%> 
                                        </div>
                                      </div>



                                    <div runat="server" id="divLocationEdit" visible="false" style="margin-bottom: 7px">
                                        <asp:Label ID="lblLocationEdit" runat="server" Font-Bold="true" Text=""></asp:Label>
                                    </div>
                                    <div runat="server" style="margin-bottom: 7px">
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="None" PageSize="5" AllowPaging="true"
                                                    AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" DataKeyNames="Value">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                    <asp:Label runat="server" ID="lblProcessName" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblValue" Text='<%# Eval("Value") %>' Visible="false"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" SortExpression="Lessthan">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtlessthan" runat="server" Width="70px" CssClass="form-control" onpaste="return false;"  onkeypress="return allowOnlyNumber(event);" Text='<%# Eval("Lessthan") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter Less Than Value."
                                                                    ControlToValidate="txtlessthan" runat="server" ValidationGroup="PromotorValidationGroup"
                                                                    Display="None" />
                                                                <asp:CompareValidator ID="cv" runat="server" ControlToValidate="txtlessthan" Type="Integer" Operator="DataTypeCheck"
                                                                    ErrorMessage="Shares must be NUMERIC! or Do not use comma seperated values" ValidationGroup="PromotorValidationGroup" Display="None" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblTotal" Text='<%# Eval("Bet") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtgreaterthan" runat="server" Width="70px" CssClass="form-control" onpaste="return false;"  onkeypress="return allowOnlyNumber(event);" Text='<%# Eval("Greaterthan") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Enter Greater Than Value."
                                                                    ControlToValidate="txtgreaterthan" runat="server" ValidationGroup="PromotorValidationGroup"
                                                                    Display="None" />
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtgreaterthan" Type="Integer" Operator="DataTypeCheck"
                                                                    ErrorMessage="Shares must be NUMERIC! or Do not use comma seperated values" ValidationGroup="PromotorValidationGroup" Display="None" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                   
                                    <div align="center" style="margin-bottom: 7px; margin-top: 15px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                   <%-- <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>--%>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
