﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadingPastAuditDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.UploadingPastAuditDocument" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->

    <!-- bootstrap theme -->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script>

        $(document).ready(function () {
            BindControls();
        });
        function CloseEvent() {
            window.parent.CloseEvent();
        }
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });

        function OpenPopup() {
            $('#AddAuditModelPopup').modal('show');
        }
        function ConfirmtestViewREV(ink) {
            $('#DocumentPopUpREV').modal();
            $('#docViewerAllREV').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUpREV').modal('hide');
            });
        });

        function BindControls() {
            $(function () {
                $('input[id*=tbxReportDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" style="background-color: white;">
        <%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
        <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12 ">
                            <div class="col-md-12 colpadding0">
                                <section class="panel">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <div runat="server" id="DivTopFilter">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntityPopPup" DataPlaceHolder="Unit"
                                                            class="form-control m-bot15" Width="95%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntityPopPup_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Unit"
                                                            ControlToValidate="ddlLegalEntityPopPup" runat="server" ValidationGroup="PromotorValidationGroup"
                                                            Display="None" />
                                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Unit"
                                                            ControlToValidate="ddlLegalEntityPopPup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                            ValidationGroup="PromotorValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1PopPup" DataPlaceHolder="Sub Unit"
                                                            class="form-control m-bot15" Width="95%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1PopPup_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2PopPup" DataPlaceHolder="Sub Unit"
                                                            class="form-control m-bot15" Width="95%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2PopPup_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3PopPup" DataPlaceHolder="Sub Unit"
                                                            class="form-control m-bot15" Width="95%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3PopPup_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocationPopPup" DataPlaceHolder="Location"
                                                            class="form-control m-bot15" Width="95%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocationPopPup_SelectedIndexChanged" />
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialPopPup" AutoPostBack="true"
                                                            DataPlaceHolder="Financial Year" class="form-control m-bot15" Width="95%" Height="32px"
                                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                            OnSelectedIndexChanged="ddlFilterFinancialPopPup_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Financial Year"
                                                            ControlToValidate="ddlFilterFinancialPopPup" runat="server" ValidationGroup="PromotorValidationGroup"
                                                            Display="None" />
                                                        <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year"
                                                            ControlToValidate="ddlFilterFinancialPopPup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                            ValidationGroup="PromotorValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <asp:DropDownListChosen ID="ddlAuditBackground" runat="server" class="form-control m-bot15" Width="82%" Height="32px"
                                                            AutoPostBack="true" Style="background: none;" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlAuditBackground_SelectedIndexChanged"
                                                            DataPlaceHolder="Audit Background">
                                                        </asp:DropDownListChosen>
                                                        <img id="lnkAddNewAB" runat="server" style="float: right;" src="../../Images/add_icon_new.png"
                                                            onclick="OpenPopup()" data-toggle="tooltip" data-placement="top" alt="Add New Audit Background" title="Add New Audit Background" />

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Audit Background"
                                                            ControlToValidate="ddlAuditBackground" runat="server" ValidationGroup="PromotorValidationGroup"
                                                            Display="None" />
                                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Audit Background"
                                                            ControlToValidate="ddlAuditBackground" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                            ValidationGroup="PromotorValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:TextBox runat="server" ID="tbxReportDate" CssClass="form-control" MaxLength="50" PlaceHolder="Select Report Generation Date" AutoComplete="off" AutoPostBack="false" Width="95%" />
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                                    <%{%>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:DropDownCheckBoxes ID="ddlVertical" runat="server" Width="40%" Height="30px" AutoPostBack="true"
                                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True">
                                                            <Style SelectBoxWidth="90%" DropDownBoxBoxWidth="800" DropDownBoxBoxHeight="250"></Style>
                                                            <Texts SelectBoxCaption="Select Vertical" />
                                                        </asp:DropDownCheckBoxes>
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                    </div>
                                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%"></div>
                                                    <%}%>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        Report Issued By
                                                    </div>
                                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:TextBox runat="server" ID="tbxIssueBy" TextMode="MultiLine" CssClass="form-control" MaxLength="50" AutoComplete="off" AutoPostBack="false" Width="95%" />
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        Report Title
                                                    </div>
                                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:TextBox runat="server" ID="tbxTitle" TextMode="MultiLine" CssClass="form-control" MaxLength="50" AutoComplete="off" AutoPostBack="false" Width="95%" />
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        Remarks
                                                    </div>
                                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <asp:TextBox runat="server" ID="tbxRemark" TextMode="MultiLine" CssClass="form-control" MaxLength="50" AutoComplete="off" AutoPostBack="false" Width="95%" />
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 colpadding0">
                                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <div runat="server" id="divuploadlbl"><label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        Upload File </div>                                                      
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;">                                                        
                                                        <asp:FileUpload ID="FileUploadObservation" runat="server" AllowMultiple="true" Style="width: 95%; padding-left: 11px;" />
                                                        <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="FileUploadObservation"
                                                            runat="server" ID="rfvFile" ValidationGroup="PromotorValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                                        <asp:Button Text="Save" runat="server" ID="btnSave" Style="float: left" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                                            ValidationGroup="PromotorValidationGroup" />
                                                        <asp:Button Text="Close" runat="server" ID="btnCancel" Style="float: left; margin-left: 10px" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseEvent()" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="clearfix" style="height: 10px;"></div>
                                    <asp:GridView runat="server" ID="grdVersionDisplayDownload" AutoGenerateColumns="false" PageSize="20" AllowPaging="true"
                                        CssClass="table" GridLines="none" Width="100%" AllowSorting="true" ShowHeaderWhenEmpty="true" OnRowCommand="grdVersionDisplayDownload_RowCommand">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.no">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranch")%>'></asp:Label>
                                                    <asp:Label ID="lblVerticalID" runat="server" Text='<%# Eval("VerticalID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unit">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBrachName")%>' ToolTip='<%# Eval("CustomerBrachName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vertical" Visible="false">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("VerticalName")%>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Financial&nbsp;Year">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                        <asp:Label ID="lblFinancialYear" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear")%>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Audit Background">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                        <asp:Label ID="lblAuditBackground" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditBackground")%>' ToolTip='<%# Eval("AuditBackground") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="File Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 280px;">
                                                        <asp:Label ID="lblAuditBackgroundfileName" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FileName")%>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle">
                                                <ItemTemplate>
                                                    <div style="width: 100px">
                                                        <asp:UpdatePanel runat="server" ID="upDownloadfile" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="lblDownLoadfile" runat="server" CommandName="grdVersionDisplayDownload" CommandArgument='<%# Eval("FileID") %>'
                                                                    data-toggle="tooltip" data-placement="top" ToolTip="Download Document"
                                                                    CausesValidation="false"><img src="../../Images/download_icon_new.png" /></asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="grdVersionDisplayView" CommandArgument='<%# Eval("FileID") %>'
                                                                    data-toggle="tooltip" data-placement="top" ToolTip="View Document"
                                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" /></asp:LinkButton>
                                                                <% if (RoleId == 8)%>
                                                                <% { %>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="grdVersionDisplayDelete" CommandArgument='<%# Eval("FileID") %>'
                                                                    data-toggle="tooltip" data-placement="top" ToolTip="Delete Document"
                                                                    CausesValidation="false"><img src="../../Images/delete_icon_new.png" /></asp:LinkButton>
                                                                <% } %>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <HeaderStyle BackColor="#ECF0F1" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                   
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>

        <div class="modal fade" id="AddAuditModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog p0" style="width: 50%; overflow: hidden;">
                <div class="modal-content">
                    <div class="modal-header" style="padding: 30px !important;">
                        <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <label class="modal-header-custom" id="Usermmodel">
                            Add Audit Background</label>
                    </div>
                    <div class="modal-body Dashboard-white-widget" style="width: 100%; margin-top: -2px;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="valSummaryABack" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="valAuditBackgroup" />
                                    <asp:CustomValidator ID="cvAuditBackground" runat="server" EnableClientScript="False"
                                        ValidationGroup="valAuditBackgroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div>
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="display: block; float: left; font-size: 13px;">Audit Background</label>
                                    <asp:TextBox runat="server" ID="tbxAuditBackground" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Enter Audit Background."
                                        ControlToValidate="tbxAuditBackground" runat="server" ValidationGroup="cvAuditBackground"
                                        Display="None" />
                                    <br />
                                    <asp:Button runat="server" Text="Save" ID="btnAddAuditBackground" CssClass="btn btn-primary" OnClick="btnAddAuditBackground_Click" />
                                    <asp:Button runat="server" Visible="false" Text="Close" ID="Button1" class="btn btn-primary" OnClientClick="this.close()" />
                                </div>
                            </ContentTemplate>
                            <%--<Triggers>
                                <asp:PostBackTrigger ControlID="btnAddAuditBackground" />
                            </Triggers>--%>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DocumentPopUpREV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; display: none; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="docViewerAllREV" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
