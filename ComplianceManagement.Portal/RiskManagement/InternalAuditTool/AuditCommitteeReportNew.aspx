﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditCommitteeReportNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditCommitteeReportNew" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //setactivemenu('Audit Committee Report');
            fhead('Audit Committee Report');
        });
    </script>
    <script type="text/javascript">
       

            function initializeConfirmDatePicker(date) {
                var startDate = new Date();
                $('#<%= txtfromMonthDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                //maxDate: startDate,
                //numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            <%--if (date != null) {
                $("#<%= txtfromMonthDate.ClientID %>").datepicker("option", "defaultDate", date);
            }--%>
        }

        function initializeConfirmDatePicker1(date) {
            var startDate = new Date();
            $('#<%= txttoMonthDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                //maxDate: startDate,
                //numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

              //$(function () {
              //    $('input[id*=txtfromMonthDate]').datepicker(
              //        {
              //            dateFormat: 'dd-mm-yy'
              //        });
              //});
              //$(function () {
              //    $('input[id*=txttoMonthDate]').datepicker(
              //        {
              //            dateFormat: 'dd-mm-yy'
              //        });
              //});
              //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

              //function EndRequestHandler(sender, args) {
              //    $(function () {
              //        $('input[id*=txtfromMonthDate]').datepicker({
              //            changeMonth: true,
              //            changeYear: true,
              //            //dateFormat: 'MM yy',
              //            onClose: function (dateText, inst) {

              //                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
              //                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
              //                $(this).datepicker('setDate', new Date(year, month, 1));
              //            },
              //            beforeShow: function (input, inst) {

              //                if ((datestr = $(this).val()).length > 0) {
              //                    year = datestr.substring(datestr.length - 4, datestr.length);
              //                    month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
              //                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
              //                    $(this).datepicker('setDate', new Date(year, month, 1));
              //                }
              //            }
              //        });


              //    });
              //    $(function () {
              //        $('input[id*=txttoMonthDate]').datepicker({
              //            changeMonth: true,
              //            changeYear: true,
              //            //dateFormat: 'MM yy',
              //            onClose: function (dateText, inst) {

              //                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
              //                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
              //                $(this).datepicker('setDate', new Date(year, month, 1));
              //            },
              //            beforeShow: function (input, inst) {

              //                if ((datestr = $(this).val()).length > 0) {
              //                    year = datestr.substring(datestr.length - 4, datestr.length);
              //                    month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
              //                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
              //                    $(this).datepicker('setDate', new Date(year, month, 1));
              //                }
              //            }
              //        });


              //    });




              //}
              //$(function () {
              //    $('input[id*=txtfromMonthDate]').datepicker({
              //        changeMonth: true,
              //        changeYear: true,
              //        dateFormat: 'MM yy',
              //        onClose: function (dateText, inst) {

              //            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
              //            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
              //            $(this).datepicker('setDate', new Date(year, month, 1));
              //        },
              //        beforeShow: function (input, inst) {

              //            if ((datestr = $(this).val()).length > 0) {
              //                year = datestr.substring(datestr.length - 4, datestr.length);
              //                month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
              //                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
              //                $(this).datepicker('setDate', new Date(year, month, 1));
              //            }
              //        }
              //    });


              //});
              //$(function () {
              //    $('input[id*=txttoMonthDate]').datepicker({
              //        changeMonth: true,
              //        changeYear: true,
              //        dateFormat: 'MM yy',
              //        onClose: function (dateText, inst) {

              //            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
              //            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
              //            $(this).datepicker('setDate', new Date(year, month, 1));
              //        },
              //        beforeShow: function (input, inst) {

              //            if ((datestr = $(this).val()).length > 0) {
              //                year = datestr.substring(datestr.length - 4, datestr.length);
              //                month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
              //                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
              //                $(this).datepicker('setDate', new Date(year, month, 1));
              //            }
              //        }
              //    });


              //});
          
    </script>
    <%--<style>
        .ui-datepicker-calendar {
            display: none;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel"> 
                                                          
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs" style="margin: -10px -1px -11px 0;">
                                           <%if (roles.Contains(3) || roles.Contains(4))%>
                                           <%{%>
                                                    <%if (roles.Contains(3))%>
                                                    <%{%>                                                              
                                                        <li class="active" id="liPerformer" runat="server">
                                                        <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                        </li>
                                                    <%}%>
                                                    <%if (roles.Contains(4))%>
                                                    <%{%>      
                                                        <li class=""  id="liReviewer" runat="server">
                                                             <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                        </li>
                                                    <%}%>
                                                    <% if (AuditHeadOrManagerReport == null)%>
                                                    <%{%>
                                                        <li style="float: right;">   
                                                        <div>
                                                        <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                        <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li>
                                                                <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation</a></li>
                                                                <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation</a></li>                                                    
                                                                <%-- <li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>                                                  
                                                                <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                      
                                                                <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li> --%>                                                     
                                                            </ul>
                                                        </div>
                                                    </li>   
                                                     <%}%>    
                                           <%}%>           
                                    </ul>
                                </header>
                            <div class="clearfix"></div>     
                        <div class="col-md-12 colpadding0">                 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true" Style="background:none;" 
                                DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                               <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true"   DataPlaceHolder="Sub Unit" 
                                      OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                  <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true"   DataPlaceHolder="Sub Unit"
                                        OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                 <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true"   DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">                           
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                               
                                <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" 
                                            class="form-control m-bot15" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"    DataPlaceHolder="Sub Unit" 
                                      OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                        </asp:DropDownListChosen> 
                            </div> 
                            <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%> 
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                         
                                   <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"    
                                     OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged"  
                                      class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical">
                                        </asp:DropDownListChosen>

                            </div>
                              <%}%>   
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                  <asp:TextBox runat="server" ID="txtfromMonthDate" class="form-control" AutoComplete="off" Style="width: 90%;background-color: white;cursor:pointer" placeholder="Audit Period From Month" />
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select audit period from month."
                                                    ControlToValidate="txtfromMonthDate"
                                                    runat="server" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                 </div>
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                 <asp:TextBox runat="server" ID="txttoMonthDate" class="form-control" AutoComplete="off" Style="width: 90%;background-color: white;cursor:pointer" placeholder="Audit Period To Month" />
                                 
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Please select audit period to month."
                                                    ControlToValidate="txttoMonthDate"
                                                    runat="server" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />

                                  </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                             
                                 <asp:DropDownListChosen runat="server" ID="ddlFinancialYear"  AutoPostBack="true"                                        
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"     class="form-control m-bot15" Width="90%" DataPlaceHolder="Financial year">
                                        </asp:DropDownListChosen>
                                <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Select Financial Year" ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"  ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlSchedulingType" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                    class="form-control m-bot15 select_location" Style="float: left; width: 90%;">                                   
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlPeriod" AutoPostBack="true" Visible="false"
                                    class="form-control m-bot15 select_location" Style="float: left; width: 90%;">
                                </asp:DropDownList>
                            </div>                                              
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float: right;">                            
                                <div style="float: right; margin-right: 10%;">                                   
                                    <asp:Button ID="lbtnExportExcelTest" OnClick="lbtnExportExcelTest_Click" class="btn btn-search" runat="server"  ValidationGroup="ComplianceInstanceValidationGroup" Text="Export To Excel"></asp:Button>                                    
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    

                        <div class="clearfix"></div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="lbtnExportExcelTest" />

        </Triggers>
    </asp:UpdatePanel>


</asp:Content>


