﻿<%@ Page Title="Audit Main UI" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditMainUI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditMainUI" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');

            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed';
                }
                fhead('My Workspace / ' + filterbytype);
            }
        });

        function Displays() {
            $('#updateProgress').show();
            setTimeout(function () { $("#updateProgress").hide(); $("#updateProgress").html(); }, 3000);
            return true;
        };

        function ShowDialog(ATBDid, BID, FinYear, ForMonth, SID, PID, VID, AuditID) {
            Displays();
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditStatusTransaction.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ATBDID=" + ATBDid + "&SID=" + SID + "&PID=" + PID + "&VID=" + VID + "&AuditID=" + AuditID);
        };

        function ShowReviewerDialog(ATBDid, BID, FinYear, ForMonth, SID, PID, VID, AuditID) {
            Displays();
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditReviewerStatus.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ATBDID=" + ATBDid + "&SID=" + SID + "&PID=" + PID + "&VID=" + VID + "&AuditID=" + AuditID);
        };   
        function ShowAdditionalDialog(BID, VID, PID, SPID, AUID) {
            Displays();
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '90%');
            $('#showdetails').attr('height', '400px');
            $('.modal-dialog').css('width', '50%');
            $('#showdetails').attr('src', "../AuditTool/AdditionalAuditCheckList.aspx?BID=" + BID +"&VID="+VID+"&PID="+PID+"&SPID="+SPID+"&AUID="+AUID);
        };
        function redirectback() {
            location.href = document.referrer;
        }
    </script>

    <script type="text/javascript">
        function checkAll(objRef) {
            
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {

                        inputList[i].checked = true;
                    }
                    else {

                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                             <div class="clearfix"></div>                           
                             <div class="panel-body">                                 
                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>                       
                                    <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true"
                                        CssClass="form-control m-bot15" Visible="false">
                                        <asp:ListItem Value="-1">Select Status</asp:ListItem>
                                        <asp:ListItem Value="1">Open</asp:ListItem>
                                        <asp:ListItem Value="2">Submited</asp:ListItem>
                                        <asp:ListItem Value="4">Team Review</asp:ListItem>
                                        <asp:ListItem Value="5">Final Review</asp:ListItem>
                                        <asp:ListItem Value="3">Closed</asp:ListItem>
                                        <asp:ListItem Value="6">Auditee Review</asp:ListItem> 
                                    </asp:DropDownList>

                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                                              
                                            <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" Width="90%" Height="32px" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                            </asp:DropDownListChosen>                  
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                                              
                                            <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" Width="90%" Height="32px" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                            </asp:DropDownListChosen>                                     
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; text-align:right;">                                             
                                            <asp:Button ID="btnAddChecklist" runat="server"   CssClass="btn btn-primary" Text="Add New" OnClick="btnAddChecklist_Click"></asp:Button> &nbsp; &nbsp;
                                            <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                                        </div>
                                </div>                                                               
                                <div class="clearfix"></div>  
                                 <div class="col-md-12 colpadding0">
                                     <div runat="server" id="LblPageDetails" style="color:#666"></div>                                      
                                     </div>                                                            
                                <div class="clearfix"></div>                                                   
                                <div class="clearfix"></div>
                                  
                                <div style="margin-bottom: 4px">
                                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage"
                                         AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" AllowPaging="true" 
                                        onrowdatabound="grdSummaryDetailsAuditCoverage_RowDataBound"
                                         PageSize="5" Width="100%" DataKeyNames="ATBDID">              
                                    <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" onclick = "Check_Click(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField>                    
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="ControlNo">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNO") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ControlNO") %>'></asp:Label>
                                            <asp:Label ID="lblATBDID" Visible="false" runat="server" Text='<%# Eval("ATBDID") %>'></asp:Label>
                                            <asp:Label ID="lblScheduledOnID" Visible="false" runat="server" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Audit Steps">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                            <asp:Label ID="LActivityTobeDone" runat="server" Text='<%# Eval("ActivityTobeDone") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ActivityTobeDone") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual Work Done">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtWorkDone" runat="server" Text='<%# ShowWorkDone((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>' CssClass="form-control" style="text-align: left;">
                                        </asp:TextBox>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observation">
                                        <ItemTemplate>      
                                            <div class="text_NlinesusingCSS" style="width: 200px;">                      
                                                <asp:Label ID="LblObservation" runat="server" Text='<%# ShowObservation((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>'
                                                     data-toggle="tooltip" data-placement="top" ToolTip='<%# ShowObservation((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                      
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRemark" runat="server" Text='<%# ShowRemark((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>' CssClass="form-control" style="text-align: left;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        
                                    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnChangeStatus" runat="server" OnClick="btnChangeStatus_Click" 
                                            CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ATBDID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID")+ "," + Eval("ProcessId")+","+Eval("VerticalId")+","+Eval("AuditID") %>'>
                                            <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" data-toggle="tooltip" data-placement="top"/></asp:LinkButton>
                                        </ItemTemplate> 
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" /> 
                                    <HeaderStyle BackColor="#ECF0F1" /> 
                                    <PagerSettings Visible="false" />                      
                                    <PagerTemplate>                        
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                    No Records Found.
                                    </EmptyDataTemplate> 
                                    </asp:GridView>
                                    <div style="float: right;padding-bottom:15px">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>  
                                    </div>                     
                                </div>
                                <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                    
                                        <div class="table-paging-text" style="float: right;">
                                            <p>Page                                          
                                            </p>
                                        </div>                                    
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                                    <br />                            
                                </div>  
                                 <div>
                                     <asp:Button Text="Save" runat="server" ID="btnAllSavechk" OnClick="btnAllsave_click" CssClass="btn btn-search" />
                                     <asp:DropDownList runat="server" ID="DropDownListPersonResponsible" AutoPostBack="true"
                                        CssClass="form-control m-bot15" Visible="false"  Style="width: 160px;margin-top: -33px;margin-left: 69px;" >                                                                           
                                     </asp:DropDownList>                                
                                </div>                                
                            </div>               
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>    
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
                    <iframe id="showdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
