﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Spire.Presentation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Spire.Presentation.Drawing;
using System.Text.RegularExpressions;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditStatusSummary : System.Web.UI.Page
    {
        protected decimal totalPrice = 0M;
        protected decimal totalNotDone = 0M;
        protected decimal totalsubmited = 0M;
        protected decimal totalteamreview = 0M;
        protected decimal totalAuditReview = 0M;
        protected decimal totalFinalreview = 0M;
        protected decimal totalclosed = 0M;
        protected int AuditID;
        protected static int CustomerId = 0;
        bool suucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                {
                    ViewState["RoleId"] = Convert.ToInt32(Request.QueryString["Role"]);
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        ViewState["returnUrl1"] = Request.QueryString["ReturnUrl1"];
                        ViewState["ProcessId"] = Convert.ToString(Request.QueryString["PID"]);
                        ViewState["SubProcessId"] = Convert.ToString(Request.QueryString["SPID"]);
                    }
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        ViewState["AuditID"] = AuditID;
                    }
                    if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]) && !String.IsNullOrEmpty(Request.QueryString["gridpagesize"]) && !String.IsNullOrEmpty(Request.QueryString["BoPSize"]))
                    {
                        ddlPageSize.SelectedIndex = Convert.ToInt32(Request.QueryString["PageSize"]);
                        int chkSelectedPage = Convert.ToInt32(Request.QueryString["BoPSize"]);
                        grdAuditStatus.PageIndex = chkSelectedPage - 1;
                        grdAuditStatus.PageSize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
                        BindProcess("P", CustomerId, AuditID);
                        BindData();
                        DropDownListPageNo.SelectedValue = Request.QueryString["BoPSize"];
                    }
                    else
                    {
                        BindProcess("P", CustomerId, AuditID);
                        BindData();
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                {
                    if (((Request.QueryString["ReturnUrl1"]).ToString()) == "Status@Open")
                    {
                        lbtObservationList.Visible = true;
                        #region                     
                        var details = UserManagementRisk.GetDataAuditClosureDetails(AuditID);
                        if (details != null)
                        {
                            var roles = UserManagementRisk.SP_GETAssigned_AsPerformerORReviewerProcedure(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID), details.FinancialYear, details.ForMonth, Convert.ToInt32(details.BranchId), Convert.ToInt32(details.VerticalId), AuditID);
                            if (roles != null)
                            {
                                if (roles.Contains(3))
                                {
                                    if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                                    {
                                        if (Convert.ToInt32(Request.QueryString["Role"]) == 3)
                                        {
                                            var UploadOptionDisplayOrNot = UserManagementRisk.Sp_CheckStatusIdSubmittedCountProcedure(Convert.ToInt32(details.BranchId), Convert.ToInt32(details.VerticalId), details.FinancialYear, details.ForMonth, AuditID);
                                            btnUploadObservation.Visible = true;
                                            fnUploadObservation.Visible = true;
                                            lbluploadobservation.Visible = true;
                                            if (UploadOptionDisplayOrNot != null)
                                            {
                                                if (UploadOptionDisplayOrNot.TotalCount >= UploadOptionDisplayOrNot.savecount)
                                                {
                                                    btnUploadObservation.Visible = false;
                                                    fnUploadObservation.Visible = false;
                                                    lbluploadobservation.Visible = false;
                                                }
                                            }
                                            if (details.Total - details.Closed == 0)
                                            {
                                                btnDraftClosure.Visible = true;
                                            }
                                            else
                                            {
                                                btnDraftClosure.Visible = false;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    btnDraftClosure.Visible = false;
                                    btnUploadObservation.Visible = false;
                                    fnUploadObservation.Visible = false;
                                    lbluploadobservation.Visible = false;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        lbtObservationList.Visible = false;
                    }
                }
                if (AuditID > 0)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var Data = (from row in entities.AuditClosureCloses
                                    where row.AuditId == AuditID
                                    select row).ToList();

                        if (Data.Count > 0)
                        {
                            ObjUploadDiv.Visible = false;
                        }
                        else
                        {
                            ObjUploadDiv.Visible = true;
                        }
                    }
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string FinancialYear = string.Empty;
            if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
            {
                FinancialYear = Convert.ToString(Request.QueryString["FY"]);
            }
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            {
                url = Request.QueryString["ReturnUrl1"];
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditStatusUI.aspx?FY=" + FinancialYear + "&" + url.Replace("@", "="));
                }
                else
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditStatusUI.aspx?" + url.Replace("@", "="));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditStatusUI.aspx?FY=" + FinancialYear + "&status=open");
                }
                else
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditStatusUI.aspx?status=open");
                }
            }
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdAuditStatus.DataSource = null;
                grdAuditStatus.DataBind();
                BindProcess("P", CustomerId, AuditID);
            }
            else
            {
                grdAuditStatus.DataSource = null;
                grdAuditStatus.DataBind();
                BindProcess("N", CustomerId, AuditID);
            }
        }



        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        private void BindProcess(string flag, int CustomerID, int AuditID)
        {
            try
            {
                int PID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    PID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                else
                {
                    PID = Convert.ToInt32(ViewState["ProcessId"]);
                }

                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdown("P", CustomerID, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));

                    if (PID > 0)
                    {
                        ddlProcess.SelectedValue = PID.ToString();
                    }

                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdown("N", CustomerID, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                    if (PID > 0)
                    {
                        ddlProcess.SelectedValue = PID.ToString();
                    }
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                int SpID = 0;
                if (Processid > 0)
                {
                    int Pid = Convert.ToInt32(Processid);

                    if (Convert.ToInt32(ViewState["ProcessId"]) == Pid)
                    {
                        if (ViewState["SubProcessId"] != null)
                        {
                            SpID = Convert.ToInt32(ViewState["SubProcessId"]);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }

                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcessUserWise(Processid, flag, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem(" Select Sub Process ", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcessUserWise(Processid, flag, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem(" Select Sub Process ", "-1"));
                }
                if (SpID > 0)
                {
                    ddlSubProcess.SelectedValue = SpID.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindData()
        {
            try
            {
                int roleid = -1;
                int Processid = -1;
                int SubProcessID = -1;
                int CustBranchID = -1;
                int? VerticalID = -1;
                string finYear = string.Empty;
                string period = string.Empty;

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                var AuditDetail = UserManagementRisk.GetAuditDatail(AuditID);
                if (AuditDetail != null)
                {
                    CustBranchID = Convert.ToInt32(AuditDetail.BranchId);
                    VerticalID = Convert.ToInt32(AuditDetail.VerticalId);
                    finYear = AuditDetail.FinancialYear;
                    period = AuditDetail.ForMonth;
                    if (!string.IsNullOrEmpty(Request.QueryString["Role"]))
                    {
                        roleid = Convert.ToInt32(Request.QueryString["Role"]);
                    }
                    else
                    {
                        roleid = Convert.ToInt32(ViewState["RoleId"]);
                    }
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        if (ddlProcess.SelectedValue != "-1")
                        {
                            Processid = Convert.ToInt32(ddlProcess.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                    {
                        if (ddlSubProcess.SelectedValue != "-1")
                        {
                            SubProcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                        }
                    }

                    if (AuditID != -1 && AuditID != 0)
                    {
                        var AuditLists = InternalControlManagementDashboardRisk.GetAuditStatusProcessSubProcessUserWise(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, roleid, CustBranchID, VerticalID, Processid, SubProcessID, CustomerId, AuditID);
                        grdAuditStatus.DataSource = AuditLists;
                        Session["TotalRows"] = AuditLists.Rows.Count;
                        grdAuditStatus.DataBind();

                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdAuditStatus.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                String AuditDetail = String.Empty;

                BindData();

                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                var fetchdetailsProcess = InternalControlManagementDashboardRisk.GetAuditinstanceData(AuditID);
                if (fetchdetailsProcess != null)
                {
                    AuditDetail = fetchdetailsProcess.location + "/" + fetchdetailsProcess.FinancialYear + "/" + fetchdetailsProcess.period + "/" + fetchdetailsProcess.verticalName;
                }

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Audit Reporting System");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView(grdAuditStatus.DataSource as DataTable);
                        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "Total", "NotDone", "Submited", "TeamReview", "AuditeeReview", "FinalReview", "Closed");
                        var customername = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));
                        FileName = "Audit Status Summary";
                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A2"].Value = customername;
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;


                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = FileName;
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);


                        exWorkSheet.Cells["A4"].Value = "Audit:";
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B4"].Value = AuditDetail;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;


                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A6"].Value = "Process";
                        exWorkSheet.Cells["A6"].AutoFitColumns(15);


                        exWorkSheet.Cells["B6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].Value = "Sub Process";
                        exWorkSheet.Cells["B6"].AutoFitColumns(25);

                        exWorkSheet.Cells["C6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C6"].Value = "Total";
                        exWorkSheet.Cells["C6"].AutoFitColumns(25);

                        exWorkSheet.Cells["D6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D6"].Value = "NotDone";
                        exWorkSheet.Cells["D6"].AutoFitColumns(25);

                        exWorkSheet.Cells["E6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E6"].Value = "Submited";
                        exWorkSheet.Cells["E6"].AutoFitColumns(50);

                        exWorkSheet.Cells["F6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F6"].Value = "TeamReview";
                        exWorkSheet.Cells["F6"].AutoFitColumns(50);


                        exWorkSheet.Cells["G6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G6"].Value = "AuditeeReview";
                        exWorkSheet.Cells["G6"].AutoFitColumns(50);

                        exWorkSheet.Cells["H6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H6"].Value = "FinalReview";
                        exWorkSheet.Cells["H6"].AutoFitColumns(20);

                        exWorkSheet.Cells["I6"].Value = "Closed";
                        exWorkSheet.Cells["I6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I6"].AutoFitColumns(20);

                        using (ExcelRange col = exWorkSheet.Cells[6, 1, 6 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }


                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AuditStatusSummary.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcess.SelectedItem.Text != " Select Process " || ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != "")
            {
                BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
            }
            BindData();

        }

        protected void ddlProcessNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlProcess.SelectedItem.Text != " Select Process " || ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != "")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
            }
            else
            {
                if (ddlProcess.SelectedItem.Text != " Select Non Process " || ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != "")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                }
            }
        }

        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {

        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }


        protected void grdAuditStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label chklabl = (Label)e.Row.FindControl("lblProcess");
                string chk = chklabl.Text.ToString();
                if (chk.Equals("Total"))
                {
                    e.Row.Font.Bold = true;
                    e.Row.Cells[0].Text = "";
                }
            }
        }


        protected void grdAuditStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = e.CommandArgument.ToString();

                    int PageSize = Convert.ToInt32(ddlPageSize.SelectedIndex);
                    int gridpagesize = Convert.ToInt32(grdAuditStatus.PageSize);
                    int BoPSize = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                    string[] arg = Args.ToString().Split(';');
                    String URLStr = "";
                    int PrpcessID = Convert.ToInt32(arg[3]);
                    if (PrpcessID == -1)
                    {
                        if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            if (ddlProcess.SelectedValue != "-1")
                            {
                                PrpcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                            }
                        }
                    }
                    int SubPrpcessID = Convert.ToInt32(arg[4]);
                    if (SubPrpcessID == -1)
                    {
                        if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                        {
                            if (ddlSubProcess.SelectedValue != "-1")
                            {
                                SubPrpcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                            }
                        }
                    }

                    string url = "";
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                    }
                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;
                    if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditMainUI.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&PID=" + PrpcessID + "&SPID=" + SubPrpcessID + "&CustBranchID=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&peroid=" + arg[8] + "&FY=" + arg[9] + "&chkPID=" + ddlProcess.SelectedValue + "&chkSPID=" + ddlSubProcess.SelectedValue + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&AuditID=" + Request.QueryString["AuditID"].ToString() + "&returnUrl2=Role@" + Request.QueryString["Role"].ToString() + url;
                    }
                    else
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditMainUI.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&PID=" + PrpcessID + "&SPID=" + SubPrpcessID + "&CustBranchID=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&peroid=" + arg[8] + "&FY=" + arg[9] + "&chkPID=" + ddlProcess.SelectedValue + "&chkSPID=" + ddlSubProcess.SelectedValue + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&AuditID=" + Request.QueryString["AuditID"].ToString();
                    }
                    if (Args != "")
                        Response.Redirect(URLStr, false);

                }
            }
            catch (Exception ex)
            {
                Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdAuditStatus.PageIndex = chkSelectedPage - 1;
                grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData();
            }
        }

        protected void lbtObservationList_Click(object sender, EventArgs e)
        {
            string status = string.Empty;
            int role = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
            {
                role = Convert.ToInt32(Request.QueryString["Role"]);
            }
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            {
                url = Request.QueryString["ReturnUrl1"];
            }
            Response.Redirect("../InternalAuditTool/ObservationDraftList.aspx?AOD=" + "ASU" + "&AuditID=" + AuditID + "&Role=" + role + "&ReturnUrl1=" + url);
        }


        protected void btnDraftClosure_Click(object sender, EventArgs e)
        {
            try
            {
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string PeriodName = string.Empty;
                int VerticalID = -1;
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                #region Added by Sagar More on 23-Jun-2020
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                }

                List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                if (FinalDeliverableDocument != null)
                {
                    using (ZipFile AuditZip = new ZipFile())
                    {
                        FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID);
                        if (FinalDeliverableDocument.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in FinalDeliverableDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {

                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                    if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            AuditZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=DraftReport.zip");
                            Response.BinaryWrite(Filedata);
                            Response.Flush();
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                        }
                    }
                }
                #endregion

                #region code comment by sagar on 23-Jun-2020
                //var AuditDetail = UserManagementRisk.GetAuditDatail(AuditID);
                //if (AuditDetail != null)
                //{
                //    CustomerBranchId = (int)AuditDetail.BranchId;
                //    FinancialYear = AuditDetail.FinancialYear;
                //    PeriodName = AuditDetail.ForMonth;
                //    VerticalID = (int)AuditDetail.VerticalId;

                //    var customerName = UserManagementRisk.GetCustomerName(CustomerId);
                //    var VerticalName = UserManagementRisk.GetVerticalName(CustomerId, VerticalID);
                //    var CustomerBranchName = UserManagementRisk.GetCustomerBranchName(CustomerId, CustomerBranchId);
                //    #region Word Report             
                //    System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                //    System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                //    using (AuditControlEntities entities = new AuditControlEntities())
                //    {
                //        var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                //                        select row).ToList();
                //        if (itemlist.Count > 0)
                //        {
                //            #region first Report
                //            stringbuilderTOP.Append(@"<style>.break { page-break-before: always;}</style>");

                //            stringbuilderTOP.Append(@"<table style ='width:100%;font-family:Calibri;'>" +
                //           "<tr>" +
                //           "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                //            "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                //            "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                //            "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                //            "<td style ='width:40%;text-align:left;'>" + VerticalName + "</td></tr></table>");

                //            stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                //            "<tr>" +
                //            "<th style='width:15%;border: 1px solid black;'><b>Control No.</b></th>" +
                //            "<th style='width:25%;border: 1px solid black;'><b>Process/Sub Process/ Control Objective & Control Activity</b></th>" +
                //            "<th style='width:25%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                //            "<th style='width:20%;border: 1px solid black;'><b>Action Plan/Due Date</b></th>" +
                //            "<th style='width:15%;border: 1px solid black;'><b>Risk Rating</b></th>" +
                //            "</tr>");


                //            foreach (var cat in itemlist)
                //            {
                //                string ControlNo = string.Empty;
                //                string Observation = string.Empty;
                //                string ManagementResponse = string.Empty;
                //                DateTime TimeLine = new DateTime();
                //                string ProcessName = string.Empty;
                //                string SubProcessName = string.Empty;
                //                string ControlObjective = string.Empty;
                //                string ControlDescription = string.Empty;
                //                string RootCause = string.Empty;
                //                string Branchname = string.Empty;
                //                string RiskRating = string.Empty;
                //                string PSPCOCD = string.Empty;
                //                string ORC = string.Empty;
                //                string APT = string.Empty;

                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                //                {
                //                    ControlNo = cat.ControlNo;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                //                {
                //                    Observation = cat.Observation;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                //                {
                //                    ManagementResponse = cat.ManagementResponse;

                //                }
                //                if (cat.TimeLine != null)
                //                {
                //                    TimeLine = Convert.ToDateTime(cat.TimeLine);
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                //                {
                //                    ProcessName = cat.ProcessName;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                //                {
                //                    SubProcessName = cat.SubProcessName;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                //                {
                //                    ControlObjective = cat.ControlObjective;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                //                {
                //                    ControlDescription = cat.ControlDescription;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                //                {
                //                    RootCause = cat.RootCost;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                //                {
                //                    Branchname = cat.Branch;
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                //                {
                //                    FinancialYear = Convert.ToString(cat.FinancialYear);
                //                }
                //                if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                //                {
                //                    RiskRating = Convert.ToString(cat.RiskRatingName);
                //                }

                //                PSPCOCD = ProcessName + " /" + SubProcessName;

                //                ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                //                APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                //                string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                //                     "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                //                     "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                //                string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                //                stringbuilderTOP.Append(@"<tr>" +
                //                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                //                "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                //                "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                //                "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                //                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                //                "</tr>");
                //            }

                //            stringbuilderTOP.Append(@"</table>");
                //            ProcessRequest_AsPerClientFirst(stringbuilderTOP.ToString(), CustomerId, customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);

                //            #endregion

                //            #region second report
                //            stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");

                //            stringbuilderTOPSecond.Append(@"<table style ='width:100%;font-family:Calibri;'>" +
                //           "<tr>" +
                //           "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                //            "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                //            "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                //            "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                //            "<td style ='width:40%;text-align:left;'>" + VerticalName + "</td></tr></table>");


                //            var RiskRatinglist = (from MCRR in entities.mst_Risk_ControlRating
                //                                  where MCRR.IsRiskControl == "R"
                //                                  select MCRR).OrderByDescending(entry => entry.Value).ToList();

                //            if (RiskRatinglist.Count > 0)
                //            {
                //                foreach (var item in RiskRatinglist)
                //                {
                //                    var ItemDetails = itemlist.Where(a => a.RiskRating == item.Value).ToList();
                //                    int rcnt = 1;
                //                    if (ItemDetails.Count > 0)
                //                    {
                //                        //stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");
                //                        stringbuilderTOPSecond.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                //                        "<tr>" +
                //                        "<td colspan ='5'><b>" + item.Name.Trim() + " Risk </b></td></tr>" +
                //                        "<tr>" +
                //                        "<th style='width:5%;border: 1px solid black;'><b>Sr No.</b></th>" +
                //                        "<th style='width:30%;border: 1px solid black;'><b>Process/Sub Process</b></th>" +
                //                        "<th style='width:35%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                //                        "<th style='width:15%;border: 1px solid black;'><b>Location</b></th>" +
                //                        "<th style='width:15%;border: 1px solid black;'><b>Action Plan</b></th>" +
                //                        "</tr>");
                //                        foreach (var cat in ItemDetails)
                //                        {
                //                            string ControlNo = string.Empty;
                //                            string Observation = string.Empty;
                //                            string ManagementResponse = string.Empty;
                //                            DateTime TimeLine = new DateTime();
                //                            string ProcessName = string.Empty;
                //                            string SubProcessName = string.Empty;
                //                            string ControlObjective = string.Empty;
                //                            string ControlDescription = string.Empty;
                //                            string RootCause = string.Empty;
                //                            string Branchname = string.Empty;
                //                            string RiskRating = string.Empty;
                //                            string PSPCOCD = string.Empty;
                //                            string ORC = string.Empty;
                //                            string APT = string.Empty;

                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                //                            {
                //                                ControlNo = cat.ControlNo;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                //                            {
                //                                Observation = cat.Observation;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                //                            {
                //                                ManagementResponse = cat.ManagementResponse;

                //                            }
                //                            if (cat.TimeLine != null)
                //                            {
                //                                TimeLine = Convert.ToDateTime(cat.TimeLine);
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                //                            {
                //                                ProcessName = cat.ProcessName;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                //                            {
                //                                SubProcessName = cat.SubProcessName;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                //                            {
                //                                ControlObjective = cat.ControlObjective;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                //                            {
                //                                ControlDescription = cat.ControlDescription;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                //                            {
                //                                RootCause = cat.RootCost;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                //                            {
                //                                Branchname = cat.Branch;
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                //                            {
                //                                FinancialYear = Convert.ToString(cat.FinancialYear);
                //                            }
                //                            if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                //                            {
                //                                RiskRating = Convert.ToString(cat.RiskRatingName);
                //                            }

                //                            PSPCOCD = ProcessName + " /" + SubProcessName;

                //                            ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                //                            APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                //                            string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                //                                 "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                //                                 "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                //                            string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                //                            stringbuilderTOP.Append(@"<tr>" +
                //                            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                //                            "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                //                            "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                //                            "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                //                            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                //                            "</tr>");

                //                            PSPCOCD = string.Empty;
                //                            ORC = string.Empty;
                //                            APT = string.Empty;

                //                            PSPCOCD = ProcessName + " /" + SubProcessName;
                //                            ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";
                //                            APT = ManagementResponse;
                //                            stringbuilderTOPSecond.Append(@"<tr>" +
                //                            "<td style='width:5%;border: 1px solid black;vertical-align: top;'>" + rcnt + "</td>" +
                //                            "<td style='width:30%;border: 1px solid black;vertical-align: top;'>" + PSPCOCD.Trim() + "</td>" +
                //                            "<td style='width:35%;border: 1px solid black;vertical-align: top;'>" + ORC.Trim() + "</td>" +
                //                            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + Branchname.Trim() + "</td>" +
                //                            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ManagementResponse.Trim() + "</td>" +
                //                            "</tr>");
                //                            rcnt++;
                //                        }//itemdetails foreach end
                //                        stringbuilderTOPSecond.Append(@"</table>");
                //                    }
                //                }//Riskrating details foreach end
                //            }
                //            ProcessRequest_AsPerClientSECOND(stringbuilderTOPSecond.ToString(), CustomerId, customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);
                //            #endregion
                //        }
                //    }
                //    #endregion

                //    #region Excel Report
                //    using (AuditControlEntities entities = new AuditControlEntities())
                //    {
                //        var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                //                        select row).ToList();
                //        if (itemlist.Count > 0)
                //        {

                //            using (ExcelPackage exportPackge = new ExcelPackage())
                //            {
                //                try
                //                {
                //                    String FileName = String.Empty;
                //                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Observation Report");
                //                    DataTable ExcelData = null;
                //                    DataView view = new System.Data.DataView((itemlist as List<SPGETObservationDetails_AsPerClient_Result>).ToDataTable());

                //                    ExcelData = view.ToTable("Selected", false, "ProcessName", "SubProcessName", "Risk", "ControlObjective", "ControlDescription", "Observation",
                //                    "RootCost", "ManagementResponse", "PersonResponsibleName", "TimeLine", "Branch");

                //                    foreach (DataRow item in ExcelData.Rows)
                //                    {
                //                        if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value)
                //                            item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                //                    }

                //                    FileName = "Observation Report";
                //                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                //                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                //                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["A2"].Value = customerName;
                //                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["A3"].Value = FileName;
                //                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["A4"].Value = "Location";
                //                    exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A4"].Style.Font.Size = 12;


                //                    exWorkSheet.Cells["B4"].Value = CustomerBranchName;
                //                    exWorkSheet.Cells["B4"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["A5"].Value = "Vertical";
                //                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["B5"].Value = VerticalName;//ddlVerticalID.SelectedItem.Text.Trim();
                //                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;

                //                    exWorkSheet.Cells["A7"].LoadFromDataTable(ExcelData, true);

                //                    exWorkSheet.Cells["A7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["A7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["A7"].Value = "Process";
                //                    exWorkSheet.Cells["A7"].AutoFitColumns(35);

                //                    exWorkSheet.Cells["B7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["B7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["B7"].Value = "Sub Process";
                //                    exWorkSheet.Cells["B7"].AutoFitColumns(25);

                //                    exWorkSheet.Cells["C7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["C7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["C7"].Value = "Risk";
                //                    exWorkSheet.Cells["C7"].AutoFitColumns(25);

                //                    exWorkSheet.Cells["D7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["D7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["D7"].Value = "Control Objective";
                //                    exWorkSheet.Cells["D7"].AutoFitColumns(50);

                //                    exWorkSheet.Cells["E7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["E7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["E7"].Value = "Entity Control";
                //                    exWorkSheet.Cells["E7"].AutoFitColumns(50);

                //                    exWorkSheet.Cells["F7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["F7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["F7"].Value = "Observation";
                //                    exWorkSheet.Cells["F7"].AutoFitColumns(30);

                //                    exWorkSheet.Cells["G7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["G7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["G7"].Value = "Root Cause";
                //                    exWorkSheet.Cells["G7"].AutoFitColumns(30);

                //                    exWorkSheet.Cells["H7"].Value = "Management Action Plan";
                //                    exWorkSheet.Cells["H7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["H7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["H7"].AutoFitColumns(50);

                //                    exWorkSheet.Cells["I7"].Value = "Process Owner";
                //                    exWorkSheet.Cells["I7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["I7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["I7"].AutoFitColumns(30);

                //                    exWorkSheet.Cells["J7"].Value = "Due Date";
                //                    exWorkSheet.Cells["J7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["J7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["J7"].AutoFitColumns(20);

                //                    exWorkSheet.Cells["K7"].Value = "Location";
                //                    exWorkSheet.Cells["K7"].Style.Font.Bold = true;
                //                    exWorkSheet.Cells["K7"].Style.Font.Size = 12;
                //                    exWorkSheet.Cells["K7"].AutoFitColumns(30);

                //                    using (ExcelRange col = exWorkSheet.Cells[7, 1, 7 + ExcelData.Rows.Count, 11])
                //                    {
                //                        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                //                        col.Style.WrapText = true;
                //                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                //                        // Assign borders
                //                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                //                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                //                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                //                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //                    }

                //                    Byte[] fileBytes = exportPackge.GetAsByteArray();

                //                    #region Save Code
                //                    if (string.IsNullOrEmpty(PeriodName))
                //                    {
                //                        var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                //                        if (peridodetails != null)
                //                        {
                //                            if (peridodetails.Count > 0)
                //                            {
                //                                foreach (var ForPeriodName in peridodetails)
                //                                {
                //                                    string fileName = "Draft Closure Observation Report" + FinancialYear + ForPeriodName + ".xlsx";
                //                                    Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                //                                    {
                //                                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                                        FinancialYear = Convert.ToString(FinancialYear),
                //                                        Period = Convert.ToString(ForPeriodName),
                //                                        CustomerId = CustomerId,
                //                                        VerticalID = Convert.ToInt32(VerticalID),
                //                                        FileName = fileName,
                //                                        AuditID = AuditID,
                //                                    };
                //                                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //                                    string directoryPath1 = "";
                //                                    bool Success1 = false;
                //                                    if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                //                                    {
                //                                        directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + CustomerId + "/"
                //                                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //                                       + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                //                                       + "/1.0");
                //                                        if (!Directory.Exists(directoryPath1))
                //                                        {
                //                                            DocumentManagement.CreateDirectory(directoryPath1);
                //                                        }
                //                                        Guid fileKey1 = Guid.NewGuid();
                //                                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                                        DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                                        DraftClosureUpload.FileKey = fileKey1.ToString();
                //                                        DraftClosureUpload.Version = "1.0";
                //                                        DraftClosureUpload.VersionDate = DateTime.Today.Date;
                //                                        DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                //                                        DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                //                                        Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                    else
                //                    {
                //                        string fileName = "Draft Closure Observation Report" + FinancialYear + PeriodName + ".xlsx";
                //                        Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                //                        {
                //                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                            FinancialYear = Convert.ToString(FinancialYear),
                //                            Period = Convert.ToString(PeriodName),
                //                            CustomerId = CustomerId,
                //                            VerticalID = Convert.ToInt32(VerticalID),
                //                            FileName = fileName,
                //                            AuditID = AuditID,
                //                        };
                //                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //                        string directoryPath1 = "";

                //                        bool Success1 = false;
                //                        if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                //                        {
                //                            directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + CustomerId + "/"
                //                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //                           + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                //                           + "/1.0");
                //                            if (!Directory.Exists(directoryPath1))
                //                            {
                //                                DocumentManagement.CreateDirectory(directoryPath1);
                //                            }

                //                            Guid fileKey1 = Guid.NewGuid();
                //                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                            DraftClosureUpload.FileName = fileName;
                //                            DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                            DraftClosureUpload.FileKey = fileKey1.ToString();
                //                            DraftClosureUpload.Version = "1.0";
                //                            DraftClosureUpload.VersionDate = DateTime.Today.Date;
                //                            DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                //                            DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                //                            Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                //                        }
                //                    }
                //                    #endregion

                //                    #region Excel Download Code
                //                    //Response.ClearContent();
                //                    //Response.Buffer = true;
                //                    //string fileName = string.Empty;
                //                    //fileName = "asd.xlsx";
                //                    //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                //                    //Response.Charset = "";
                //                    //Response.ContentType = "application/vnd.ms-excel";
                //                    //StringWriter sw = new StringWriter();
                //                    //Response.BinaryWrite(fileBytes);
                //                    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //                    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //                    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                //                    #endregion

                //                }
                //                catch (Exception ex)
                //                {
                //                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //                }
                //            }
                //        }
                //    }
                //    #endregion

                //    #region PowerPoint Report               
                //    //using (AuditControlEntities entities = new AuditControlEntities())
                //    //{
                //    //    var details = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName)
                //    //                   select row).ToList();


                //    //    string BranchName = string.Empty;
                //    //    if (details.Count > 0)
                //    //    {
                //    //        var ProcessList = GetProcessListOrdered(details);

                //    //        float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                //    //        float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;


                //    //        String PPTName = "Draft Internal Audit Report - " + PeriodName;

                //    //        string CustomerName = GetByID(customerID).Name;
                //    //        string colorname = String.Empty;


                //    //        colorname = "Chocolate";

                //    //        CreateDymanicHeadingFirstSlide(SlideCounter, xpointsection1, 10, 700, 520, "", 32, "L", colorname);
                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 80, 700, 100, CustomerName, 32, "L", colorname);
                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 150, 700, 100, BranchName, 30, "L", colorname);
                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 200, 700, 60, PPTName, 33, "L", colorname);
                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 250, 700, 60, "Year- " + FinancialYear + "", 24, "L", colorname);
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        PPTName = PPTName + " " + "(Year " + FinancialYear + ")";

                //    //        CreateDymanicPara(SlideCounter, xpointsection1, 200, 700, 205, "Ignore – to be replaced by cover letter", 22);
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 100, "Table of Content", 32, "L", colorname);
                //    //        DynamicTableOfContent(SlideCounter, xpointsection1, 120, 700, 300, "", 24, colorname, ProcessList);
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", "White");
                //    //        SetSlideColor(SlideCounter, colorname);
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", colorname);

                //    //        //Add New Blank Slide 
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        int ProcessID = -1;

                //    //        int Count = 0;
                //    //        int ProcessNumber = 0;
                //    //        int ObservationNumber = 0;

                //    //        String ObservationNo = string.Empty;

                //    //        details.ForEach(entry =>
                //    //        {
                //    //            String SectionHeading = "Section " + intToRoman(Count + 2) + "";

                //    //            ObservationNumber++;

                //    //            if (ProcessID == -1 || ProcessID != entry.ProcessId)
                //    //            {
                //    //                if (Count < ProcessList.Count)
                //    //                {
                //    //                    ProcessID = Convert.ToInt32(entry.ProcessId);
                //    //                    ProcessNumber++;
                //    //                    ObservationNumber = 1;

                //    //                    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 50, SectionHeading, 32, "L", "White");
                //    //                    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 50, ProcessList.ElementAt(Count), 32, "L", "White");
                //    //                    SetSlideColor(SlideCounter, colorname);
                //    //                    presentationdynamic.Slides.Append();
                //    //                    SlideCounter++;
                //    //                    Count++;
                //    //                }
                //    //                else
                //    //                {

                //    //                }
                //    //            }

                //    //            int checkvalueobservation = 0;
                //    //            int checkvalueRisk = 0;
                //    //            int checkvalueRecomentdation = 0;
                //    //            int checkvalueManagementresponse = 0;
                //    //            int checkvalueTimeLine = 0;

                //    //            string[] arrayobservation = entry.Observation.Split(' ');
                //    //            int arrobservationLength = arrayobservation.Length;
                //    //            checkvalueobservation = (2 * arrobservationLength);

                //    //            string[] arrayrisk = entry.Risk.Split(' ');
                //    //            int arrLengthrisk = arrayrisk.Length;
                //    //            checkvalueRisk = (2 * arrLengthrisk);

                //    //            string[] arrayRecommendations = entry.Recomendation.Split(' ');
                //    //            int arrLengthRecommendations = arrayRecommendations.Length;
                //    //            checkvalueRecomentdation = (2 * arrLengthRecommendations);

                //    //            string[] arrayManagementResponse = entry.ManagementResponse.Split(' ');
                //    //            int arrLengthManagementResponsek = arrayManagementResponse.Length;
                //    //            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

                //    //            string[] arrayPersonresponsible = entry.ManagementResponse.Split(' ');
                //    //            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
                //    //            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

                //    //            //add footer
                //    //            presentationdynamic.SetFooterText(PPTName);

                //    //            //set the footer visible
                //    //            presentationdynamic.SetFooterVisible(true);

                //    //            //set the page number visible
                //    //            presentationdynamic.SetSlideNoVisible(true);

                //    //            //set the date visible
                //    //            presentationdynamic.SetDateTimeVisible(true);

                //    //            if (checkvalueobservation <= 200 && checkvalueRisk <= 200 && checkvalueRecomentdation <= 150
                //    //                && checkvalueManagementresponse <= 150 && checkvalueTimeLine <= 100)
                //    //            {
                //    //                ObservationNo = ProcessNumber + "." + ObservationNumber + " ";
                //    //                //Observation Title by default null
                //    //                //PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                //    //                PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                //    //                presentationdynamic.Slides.Append();
                //    //                SlideCounter++;
                //    //            }
                //    //            else
                //    //            {
                //    //                //Observation Title by default null
                //    //                PrintDynamicObservationSlide(SlideCounter, Regex.Replace(entry.ObservationNumber, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                //    //                presentationdynamic.Slides.Append();
                //    //                SlideCounter++;
                //    //            }
                //    //        });

                //    //        CreateDymanicHeading(SlideCounter, xpointsection1 + 100, 200, 300, 100, "Thank You", 32, "L", "White");
                //    //        SetSlideColor(SlideCounter, colorname);
                //    //        presentationdynamic.Slides.Append();
                //    //        SlideCounter++;

                //    //        string dt = DateTime.Now.ToString("dd-MMM-yy");
                //    //        if ((BranchName.Split(' ').First() + "-" + PPTName + "-" + dt).Length < 218)
                //    //            PPTName = BranchName.Split(' ').FirstOrDefault() + "-" + PPTName + "-" + dt;

                //    //        string path = Server.MapPath("~//PPTDownload//" + PPTName + ".pptx");

                //    //        presentationdynamic.DocumentProperty["_MarkAsFinal"] = true;
                //    //        presentationdynamic.SaveToFile(path, FileFormat.Pptx2010);


                //    //        Byte[] fileBytes = presentationdynamic.GetBytes();
                //    //        if (string.IsNullOrEmpty(PeriodName))
                //    //        {
                //    //            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear);
                //    //            if (peridodetails != null)
                //    //            {
                //    //                if (peridodetails.Count > 0)
                //    //                {
                //    //                    foreach (var ForPeriodName in peridodetails)
                //    //                    {
                //    //                        string fileName = "Draft Closure Observation Report" + FinancialYear + ForPeriodName + ".pptx";
                //    //                        Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                //    //                        {
                //    //                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //    //                            FinancialYear = Convert.ToString(FinancialYear),
                //    //                            Period = Convert.ToString(ForPeriodName),
                //    //                            CustomerId = customerID,
                //    //                            VerticalID = Convert.ToInt32(VerticalID),
                //    //                            FileName = fileName,
                //    //                        };
                //    //                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //    //                        string directoryPath1 = "";
                //    //                        bool Success1 = false;
                //    //                        if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                //    //                        {
                //    //                            directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                //    //                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //    //                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                //    //                           + "/1.0");
                //    //                            if (!Directory.Exists(directoryPath1))
                //    //                            {
                //    //                                DocumentManagement.CreateDirectory(directoryPath1);
                //    //                            }
                //    //                            Guid fileKey1 = Guid.NewGuid();
                //    //                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //    //                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //    //                            DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //    //                            DraftClosureUpload.FileKey = fileKey1.ToString();
                //    //                            DraftClosureUpload.Version = "1.0";
                //    //                            DraftClosureUpload.VersionDate = DateTime.Today.Date;
                //    //                            DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                //    //                            DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //    //                            DocumentManagement.SaveDocFiles(Filelist1);
                //    //                            Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                //    //                        }

                //    //                    }
                //    //                }
                //    //            }
                //    //        }
                //    //        else
                //    //        {
                //    //            string fileName = "Draft Closure Observation Report" + FinancialYear + PeriodName + ".pptx";
                //    //            Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                //    //            {
                //    //                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //    //                FinancialYear = Convert.ToString(FinancialYear),
                //    //                Period = Convert.ToString(PeriodName),
                //    //                CustomerId = customerID,
                //    //                VerticalID = Convert.ToInt32(VerticalID),
                //    //                FileName = fileName,
                //    //            };
                //    //            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //    //            string directoryPath1 = "";

                //    //            bool Success1 = false;
                //    //            if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                //    //            {
                //    //                directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                //    //               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //    //               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                //    //               + "/1.0");
                //    //                if (!Directory.Exists(directoryPath1))
                //    //                {
                //    //                    DocumentManagement.CreateDirectory(directoryPath1);
                //    //                }

                //    //                Guid fileKey1 = Guid.NewGuid();
                //    //                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //    //                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //    //                DraftClosureUpload.FileName = fileName;
                //    //                DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //    //                DraftClosureUpload.FileKey = fileKey1.ToString();
                //    //                DraftClosureUpload.Version = "1.0";
                //    //                DraftClosureUpload.VersionDate = DateTime.Today.Date;
                //    //                DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                //    //                DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //    //                DocumentManagement.SaveDocFiles(Filelist1);
                //    //                Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                //    //            }

                //    //        }
                //    //    }
                //    //    #region PPT Download Code
                //    //    //Response.ContentType = "application/octet-stream";
                //    //    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + PPTName + ".pptx");
                //    //    //Response.TransmitFile(path);
                //    //    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //    //    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //    //    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                //    //    #endregion
                //    //}
                //    #endregion

                //    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure AuditClosureResult = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure()
                //    {
                //        FinancialYear = FinancialYear,
                //        ForPeriod = PeriodName,
                //        CustomerbranchId = Convert.ToInt32(CustomerBranchId),
                //        VerticalID = Convert.ToInt32(VerticalID),
                //        DraftClosureDate = DateTime.Now,
                //        AuditID = AuditID,
                //    };
                //    bool sucess = RiskCategoryManagement.UpdateAuditClosure(AuditClosureResult);
                //    if (sucess)
                //    {
                //        #region
                //        List<Tran_DraftClosureUpload> DraftClosureDocument = new List<Tran_DraftClosureUpload>();
                //        if (DraftClosureDocument != null)
                //        {
                //            using (ZipFile AuditZip = new ZipFile())
                //            {
                //                if (!string.IsNullOrEmpty(PeriodName))
                //                {
                //                    DraftClosureDocument = RiskCategoryManagement.GetFileDataDraftClosureDocument(AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                //                }
                //                else
                //                {
                //                    DraftClosureDocument = RiskCategoryManagement.GetFileDataDraftClosureDocument(AuditID).ToList();//.Where(entry => entry.Version.Equals("1.0")).ToList();
                //                }
                //                AuditZip.AddDirectoryByName(FinancialYear);
                //                if (DraftClosureDocument.Count > 0)
                //                {
                //                    int i = 0;
                //                    foreach (var file in DraftClosureDocument)
                //                    {
                //                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                //                        if (file.FilePath != null && File.Exists(filePath))
                //                        {
                //                            string[] filename = file.FileName.Split('.');
                //                            string str = filename[0] + "." + filename[1];
                //                            if (file.EnType == "M")
                //                            {
                //                                AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                //                            }
                //                            else
                //                            {
                //                                AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                //                            }
                //                            i++;
                //                        }
                //                    }
                //                    var zipMs = new MemoryStream();
                //                    AuditZip.Save(zipMs);
                //                    zipMs.Position = 0;
                //                    byte[] Filedata = zipMs.ToArray();
                //                    Response.Buffer = true;
                //                    Response.ClearContent();
                //                    Response.ClearHeaders();
                //                    Response.Clear();
                //                    Response.ContentType = "application/zip";
                //                    Response.AddHeader("content-disposition", "attachment; filename=FinalDeliverableDocuments.zip");
                //                    Response.BinaryWrite(Filedata);
                //                    Response.Flush();
                //                    //Response.End();
                //                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                //                }
                //                else
                //                {
                //                    cvDuplicateEntry.IsValid = false;
                //                    cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                //                }
                //            }
                //        }
                //        #endregion
                //    }
                //}//(AuditDetail != null) end 

                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region Word Report Funcation
        public void ProcessRequest_AsPerClientFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Calibri;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Consolidated Location Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    DraftClosureUpload.FileKey = fileKey1.ToString();
                                    DraftClosureUpload.Version = "1.0";
                                    DraftClosureUpload.VersionDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Consolidated Location Report" + FinancialYear + PeriodName + ".doc";
                    Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                    {
                        directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        DraftClosureUpload.FileName = fileName;
                        DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        DraftClosureUpload.FileKey = fileKey1.ToString();
                        DraftClosureUpload.Version = "1.0";
                        DraftClosureUpload.VersionDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest_AsPerClientSECOND(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Calibri;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Draft Audit Committee Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    DraftClosureUpload.FileKey = fileKey1.ToString();
                                    DraftClosureUpload.Version = "1.0";
                                    DraftClosureUpload.VersionDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Draft Audit Committee Report" + FinancialYear + PeriodName + ".doc";
                    Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                    {
                        directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        DraftClosureUpload.FileName = fileName;
                        DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        DraftClosureUpload.FileKey = fileKey1.ToString();
                        DraftClosureUpload.Version = "1.0";
                        DraftClosureUpload.VersionDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest(string context, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                string details = "Audit Report for the (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "ObservationReport" + FinancialYear + ForPeriodName + ".doc";
                                Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    DraftClosureUpload.FileKey = fileKey1.ToString();
                                    DraftClosureUpload.Version = "1.0";
                                    DraftClosureUpload.VersionDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                                    DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "ObservationReport" + FinancialYear + PeriodName + ".doc";
                    Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
                    {
                        directoryPath1 = Server.MapPath("~/DraftClosureDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        DraftClosureUpload.FileName = fileName;
                        DraftClosureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        DraftClosureUpload.FileKey = fileKey1.ToString();
                        DraftClosureUpload.Version = "1.0";
                        DraftClosureUpload.VersionDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedDate = DateTime.Today.Date;
                        DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);
                    }
                }
                #region Word Download Code
                //string NameVertical = VerticalName.VerticalName;
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + companyname + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }
        public string GetUserName(int userid)
        {
            string processnonprocess = "";
            if (userid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(userid);
                    mst_User mstuser = (from row in entities.mst_User
                                        where row.ID == userid && row.IsDeleted == false && row.IsActive == true
                                        select row).FirstOrDefault();

                    processnonprocess = mstuser.FirstName + " " + mstuser.LastName;
                }
            }
            return processnonprocess;
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure> TempAuditClosureResult(int branchid, string Financialyear, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.CustomerbranchId == branchid && row.VerticalID == VerticalID && row.FinancialYear == Financialyear
                             && row.ACStatus == 1
                             select row).ToList();
                return query.ToList();
            }
        }
        #endregion
        #region Power Point Report Function
        public int SlideCounter = 0;

        Presentation presentationdynamic = new Presentation();
        public static mst_Customer GetByID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_Customer
                                where row.ID == customerID && row.IsDeleted == false
                                select row).SingleOrDefault();

                return customer;
            }
        }
        public static List<String> GetProcessListOrdered(List<Business.DataRisk.AuditClosure> ACC)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in ACC
                                                join MP in entities.Mst_Process
                                                on row.ProcessId equals MP.Id
                                                orderby row.ProcessOrder
                                                select MP.Name).Distinct().ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public static List<String> GetProcessListOrdered(List<Business.DataRisk.SPGETObservationDetails_Result> ACC)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in ACC
                                                join row1 in entities.AuditClosures
                                                on row.ProcessId equals row1.ProcessId
                                                join MP in entities.Mst_Process
                                                on row.ProcessId equals MP.Id
                                                orderby row1.ProcessOrder
                                                select MP.Name).Distinct().ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public static List<Business.DataRisk.AuditClosure> GetAuditWiseObservationDetailList(List<long> BranchList, int verticalID, String FinYear, String Period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in entities.AuditClosures
                                                where BranchList.Contains(row.CustomerbranchId)
                                                && row.VerticalID == verticalID
                                                && row.FinancialYear == FinYear
                                                //&& row.ForPeriod == Period
                                                && row.ACStatus == 1
                                                select row).ToList();

                AuditWiseObservationList = AuditWiseObservationList
                .OrderBy(entry => entry.ProcessOrder)
                .ThenBy(entry => entry.ObservationNumber).ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public void SetSlideColor(int slidenumber, string colorName)
        {
            presentationdynamic.Slides[slidenumber].SlideBackground.Type = BackgroundType.Custom;
            presentationdynamic.Slides[slidenumber].SlideBackground.Fill.FillType = FillFormatType.Solid;

            switch (colorName)
            {
                #region A
                case "AliceBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
        }
        public void CreateDymanicPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            if (Contain.Contains('\n'))
            {
                string ss = Contain.Replace("\r\n", "");
                para_Secc.Text = ss;
            }
            else
            {
                para_Secc.Text = Contain;
            }
            para_Secc.Alignment = TextAlignmentType.Justify;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.MarginTop = 0;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }
        public void CreateDymanicHeadingFirstSlide(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);

            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gray;
                    break;
                case "Green":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Peru;
                    break;
                case "Pink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Pink;
                    break;
                case "Plum":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tan;
                    break;
                case "Teal":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            //switch (colorname)
            //{
            //    case "Navy":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
            //        break;
            //    case "OrangeRed":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
            //        break;
            //    case "RoyalBlue":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
            //        break;
            //    case "Red":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
            //        break;
            //    case "Green":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
            //        break;
            //    case "Sienna":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
            //        break;
            //    case "Purple":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
            //        break;
            //    case "Maroon":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
            //        break;
            //    default:
            //        break;
            //}

            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();

            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }


        public void PrintDynamicObservationSlideFix(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, int? ObsRating, string colorname)
        {
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;

            int checkvalueobservation = 0;
            int checkvalueRisk = 0;
            int checkvalueRecomentdation = 0;
            int checkvalueManagementresponse = 0;
            int checkvalueTimeLine = 0;

            string[] arrayobservation = paraobservation.Split(' ');
            int arrobservationLength = arrayobservation.Length;
            checkvalueobservation = (2 * arrobservationLength);

            string[] arrayrisk = pararisk.Split(' ');
            int arrLengthrisk = arrayrisk.Length;
            checkvalueRisk = (2 * arrLengthrisk);

            string[] arrayRecommendations = pararRecommendations.Split(' ');
            int arrLengthRecommendations = arrayRecommendations.Length;
            checkvalueRecomentdation = (2 * arrLengthRecommendations);

            string[] arrayManagementResponse = paraManagementResponse.Split(' ');
            int arrLengthManagementResponsek = arrayManagementResponse.Length;
            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

            string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

            String ObservationRating = String.Empty;
            String ObsRatingColor = String.Empty;


            if (ObsRating != null)
            {
                if (ObsRating == 1)
                {
                    ObservationRating = "High";
                    ObsRatingColor = "Red";
                }
                else if (ObsRating == 2)
                {
                    ObservationRating = "Medium";
                    ObsRatingColor = "Chocolate";
                }
                else if (ObsRating == 3)
                {
                    ObservationRating = "Low";
                    ObsRatingColor = "Green";
                }
            }

            CreateDymanicHeading(slidecounter, xpointsection1, 30, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
            CreateDymanicFilledHeading(slidecounter, 615, 5, 100, 20, ObservationRating, 15, "C", ObsRatingColor);
            CreateDymanicTitle(slidecounter, xpointsection1, 70, 350, 10, "Observations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 100, 350, 205, paraobservation, 13);

            CreateDymanicTitle(slidecounter, xpointsection1, 250, 350, 10, "Risk:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 280, 350, 205, pararisk, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 100, 350, 150, pararRecommendations, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 200, 350, 10, "Management Response:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 230, 350, 150, paraManagementResponse, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 350, 350, 10, "Responsibility and Timeline:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 380, 350, 100, paraPersonresponsible, 13);
        }

        public void PrintDynamicObservationSlide(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, string colorname)
        {
            try
            {
                string printstartfrom = "";
                int dynamicwordlength = 0;
                int dynamicheight = 0;
                int totalheight = 400;
                String Firstpara = String.Empty;
                String Nextpara = String.Empty;
                string section = "F";
                int yposition = 0;
                int newvalue = 0;
                string sectionvalue = "A";
                float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
                float ypoint = 40;
                int thirdval = 0;

                //int checkvalueobservation = 0;
                //int checkvalueRisk = 0;
                //int checkvalueRecomentdation = 0;
                //int checkvalueManagementresponse = 0;
                //int checkvalueTimeLine = 0;

                string[] arrayobservation = paraobservation.Split(' ');
                int arrobservationLength = arrayobservation.Length;

                string[] arrayrisk = pararisk.Split(' ');
                int arrLengthrisk = arrayrisk.Length;

                string[] arrayRecommendations = pararRecommendations.Split(' ');
                int arrLengthRecommendations = arrayRecommendations.Length;

                string[] arrayManagementResponse = paraManagementResponse.Split(' ');
                int arrLengthManagementResponsek = arrayManagementResponse.Length;

                string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
                int arrLengthPersonresponsible = arrayPersonresponsible.Length;

                CreateDymanicHeading(slidecounter, xpointsection1, ypoint, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
                CreateDymanicTitle(slidecounter, xpointsection1, ypoint + 30, 350, 10, "Observations:", 13, colorname);
                CreateDymanicLoopPara(slidecounter, xpointsection1, ypoint + 70, 350, totalheight, paraobservation, 13, arrobservationLength, arrayobservation, section, colorname, out yposition, out section);

                newvalue = yposition;
                sectionvalue = section;
                if (sectionvalue == "F")//observation First Section Value
                {
                    dynamicheight = (2 * arrLengthrisk);
                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                    CreateDymanicTitle(slidecounter, xpointsection1, (ypoint + 70 + newvalue), 350, 10, "Risk:", 13, colorname);
                    if (dynamicheight > totalheight - newvalue)
                    {
                        printstartfrom = "MID";
                        CreateDymanicLoopParaDynamicArrayLength(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20,
                        pararisk, 13, arrLengthrisk, arrayrisk, dynamicwordlength, colorname, out yposition, out section);
                        thirdval += yposition;
                    }
                    else
                    {
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicLoopPara(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20, pararisk, 13,
                            arrLengthrisk, arrayrisk, section, colorname, out yposition, out section);
                        printstartfrom = "START";
                    }//risk Print Complete                       
                    newvalue = yposition;
                    sectionvalue = section;
                    if (sectionvalue == "F")//Risk  First Section Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval = newvalue;
                        }
                        else
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += newvalue;
                        }
                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                            newvalue = yposition;
                            sectionvalue = section;
                            if (sectionvalue == "F")
                            {
                                thirdval += newvalue;
                                dynamicheight = (2 * arrLengthPersonresponsible);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 110 + thirdval), 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 120 + thirdval + 20), 350, totalheight - thirdval + 20, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                            }
                            else if (sectionvalue == "S")//Recomendation Second Section Value
                            {
                                if (newvalue > 300)
                                {
                                    presentationdynamic.Slides.Append();
                                    SlideCounter++;
                                    dynamicheight = (2 * arrLengthRecommendations);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                                    newvalue = yposition;
                                    sectionvalue = section;
                                    if (sectionvalue == "F")
                                    {
                                        dynamicheight = (2 * arrLengthPersonresponsible);
                                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                                    }
                                }
                            }
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            if (newvalue > 300)
                            {
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                                dynamicheight = (2 * arrLengthRecommendations);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Recommendations:", 13, colorname);
                                CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);

                                newvalue = yposition;
                                sectionvalue = section;
                                if (sectionvalue == "F")
                                {
                                    dynamicheight = (2 * arrLengthManagementResponsek);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                                }
                            }
                        }
                    }
                    else if (sectionvalue == "S")//Risk Section Second Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                        else
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += yposition;
                        }
                        newvalue = yposition;
                        sectionvalue = section;

                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - thirdval - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, thirdval + 70, 350, 10, "Management Response:", 13, colorname);
                        }
                    }
                }
                else if (sectionvalue == "S") //observation Second Section Value
                {
                    if (newvalue > 300)
                    {
                        presentationdynamic.Slides.Append();
                        SlideCounter++;
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Risk:", 13, colorname);
                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararisk, 13, arrLengthrisk, arrayrisk, sectionvalue, colorname, out yposition, out section);

                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")
                        {
                            dynamicheight = (2 * arrLengthRecommendations);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recomendtions:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void CreateDymanicLoopParaDynamicArrayLength(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, int dymnamicchar, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > dymnamicchar)
            {
                if (arrLength > dymnamicchar)
                {
                    int cnt = (arrLength / dymnamicchar) + 1;

                    for (int j = dymnamicchar; j > 0; j--)
                    {
                        if (array[j].Contains("."))
                        {
                            indextoSplit = j;
                            j = 0;
                            break;
                        }
                    }
                    for (int j = 0; j <= indextoSplit; j++)
                    {
                        if (!String.IsNullOrEmpty(Firstpara))
                            Firstpara = Firstpara + " " + array[j];
                        else
                            Firstpara = Firstpara + array[j];
                        Lntcnt += 1;
                    }
                    CreateDymanicPara(0, x, y, width, height, Firstpara, fontSize);
                    Firstpara = "";
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    arrLength = 0;
                    array = Nextpara.Split(' ');
                    arrLength = array.Length;
                    Lntcnt = 0;
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    section = "S";
                    yposition = (2 * arrLength);
                    CreateDymanicLoopPara(0, xpointsection2, 70, 350, dynamicheight, Contain, fontSize, arrLength, array, section, colorname, out yposition, out section);

                }
                else
                {

                    yposition = (2 * arrLength);
                    CreateDymanicPara(0, xpointsection2, 70, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";

                }
            }
            else
            {
                int dynamicheight = 0;
                dynamicheight = (2 * arrLength);
                CreateDymanicPara(0, x, y, 350, dynamicheight, Contain, fontSize);
                yposition = dynamicheight;
                section = "S";

            }
        }
        public void CreateDymanicLoopPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, string printsection, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > 200)
            {
                do
                {
                    if (arrLength > 200)
                    {
                        int cnt = (arrLength / 200) + 1;

                        for (int j = 200; j > 0; j--)
                        {
                            if (array[j].Contains("."))
                            {
                                indextoSplit = j;
                                j = 0;
                                break;
                            }
                        }
                        for (int j = 0; j <= indextoSplit; j++)
                        {
                            if (!String.IsNullOrEmpty(Firstpara))
                                Firstpara = Firstpara + " " + array[j];
                            else
                                Firstpara = Firstpara + array[j];
                            Lntcnt += 1;
                        }
                        CreateDymanicPara(slidenumber, x, y, width, height, Firstpara, fontSize);
                        Firstpara = "";
                        Nextpara = "";
                        for (int k = Lntcnt; k < arrLength; k++)
                        {
                            if (!String.IsNullOrEmpty(Nextpara))
                                Nextpara = Nextpara + " " + array[k];
                            else
                                Nextpara = Nextpara + array[k];
                        }
                        arrLength = 0;
                        array = Nextpara.Split(' ');
                        arrLength = array.Length;
                        Lntcnt = 0;
                    }
                    else
                    {
                        if (printsection == "S")
                        {
                            presentationdynamic.Slides.Append();
                            SlideCounter++;
                            yposition = (2 * arrLength);
                            CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 20, "1.1  Explore possibility of payment of Ocean Freight bills in USD", 21, "L", colorname);
                            CreateDymanicPara(SlideCounter, xpointsection1, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "F";
                        }
                        else
                        {
                            yposition = (2 * arrLength);
                            CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "S";
                        }

                    }
                } while (arrLength > 0);
            }
            else
            {
                if (printsection == "S")
                {
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    yposition = (2 * arrLength);
                    CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";
                }
                else
                {
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    CreateDymanicPara(slidenumber, x, y, 350, dynamicheight, Contain, fontSize);
                    yposition = dynamicheight;
                    section = "F";
                }
            }
        }
        public void CreateDymanicTitle(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;
            para_Secc.Alignment = TextAlignmentType.Left;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }
        public void DynamicTableOfContent(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname, List<string> ProcessList)
        {
            RectangleF rec_bullet = new RectangleF(x, y, 700, height);
            IAutoShape shape_bullet = presentationdynamic.Slides[SlideCounter].Shapes.AppendShape(ShapeType.Rectangle, rec_bullet);
            shape_bullet.ShapeStyle.LineColor.Color = Color.White;
            shape_bullet.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            shape_bullet.TextFrame.AnchoringType = TextAnchorType.Top;

            string[] str = new string[] { "Executive Summary", "Detailed Report" };

            foreach (string txt in str)
            {
                TextParagraph textParagraph = new TextParagraph();
                textParagraph.Text = txt;
                textParagraph.Alignment = TextAlignmentType.Left;
                textParagraph.Indent = 35;

                //set the Bullets
                textParagraph.BulletType = TextBulletType.Symbol;
                textParagraph.BulletStyle = NumberedBulletStyle.BulletRomanLCPeriod;
                textParagraph.LineSpacing = 150;
                shape_bullet.TextFrame.Paragraphs.Append(textParagraph);

                if (txt == "Detailed Report")
                {
                    string[] str1 = ProcessList.ToArray();

                    foreach (string txt1 in str1)
                    {
                        TextParagraph textinnerParagraph = new TextParagraph();
                        textinnerParagraph.Text = txt1;
                        textinnerParagraph.Alignment = TextAlignmentType.Left;
                        textinnerParagraph.Indent = 24;
                        textinnerParagraph.LeftMargin = 50;
                        textinnerParagraph.LineSpacing = 150;
                        //set the Bullets
                        textinnerParagraph.BulletType = TextBulletType.Numbered;
                        textinnerParagraph.BulletStyle = NumberedBulletStyle.BulletArabicPeriod;
                        shape_bullet.TextFrame.Paragraphs.Append(textinnerParagraph);
                    }
                }
            }

            ////set the font and fill style
            foreach (TextParagraph paragraph in shape_bullet.TextFrame.Paragraphs)
            {
                paragraph.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
                paragraph.TextRanges[0].FontHeight = 24;
                paragraph.TextRanges[0].Fill.FillType = FillFormatType.Solid;
                switch (colorname)
                {
                    #region A
                    case "AliceBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                        break;
                    case "AntiqueWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                        break;
                    case "Aqua":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                        break;
                    case "Aquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                        break;
                    case "Azure":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                        break;
                    #endregion

                    #region B
                    case "Black":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                        break;
                    case "BlanchedAlmond":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                        break;
                    case "Blue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                        break;
                    case "BlueViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                        break;
                    case "Brown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                        break;
                    case "BurlyWood":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                        break;
                    #endregion

                    #region C
                    case "CadetBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                        break;
                    case "Chartreuse":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                        break;
                    case "Chocolate":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                        break;
                    case "Coral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                        break;
                    case "CornflowerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                        break;
                    case "Crimson":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                        break;
                    case "Cornsilk":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                        break;
                    case "Cyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                        break;
                    #endregion

                    #region D
                    case "DarkBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                        break;
                    case "DarkCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                        break;
                    case "DarkGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                        break;
                    case "DarkGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                        break;
                    case "DarkGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                        break;
                    case "DarkKhaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                        break;
                    case "DarkMagenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                        break;
                    case "DarkOliveGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                        break;
                    case "DarkOrange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                        break;
                    case "DarkOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                        break;
                    case "DarkRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                        break;
                    case "DarkSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                        break;
                    case "DarkSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                        break;
                    case "DarkSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                        break;
                    case "DarkSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                        break;
                    case "DarkTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                        break;
                    case "DarkViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                        break;
                    case "DeepPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                        break;
                    case "DeepSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                        break;
                    case "DimGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                        break;
                    case "DodgerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                        break;
                    #endregion

                    #region F
                    case "Firebrick":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                        break;
                    case "FloralWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                        break;
                    case "Fuchsia":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                        break;
                    case "ForestGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                        break;
                    #endregion

                    #region G
                    case "Gainsboro":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                        break;
                    case "GhostWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                        break;
                    case "Gold":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                        break;
                    case "Goldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                        break;
                    case "Gray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                        break;
                    case "Green":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                        break;
                    case "GreenYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                        break;
                    #endregion

                    #region H
                    case "Honeydew":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                        break;
                    case "HotPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                        break;
                    #endregion

                    #region I
                    case "IndianRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                        break;
                    case "Indigo":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                        break;
                    case "Ivory":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                        break;
                    #endregion

                    #region K
                    case "Khaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                        break;
                    #endregion

                    #region L
                    case "Lavender":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                        break;
                    case "LavenderBlush":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                        break;
                    case "LawnGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                        break;
                    case "LemonChiffon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                        break;
                    case "LightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                        break;
                    case "LightCoral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                        break;
                    case "LightCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                        break;
                    case "LightGoldenrodYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                        break;
                    case "LightGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                        break;
                    case "LightGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                        break;
                    case "LightPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                        break;
                    case "LightSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                        break;
                    case "LightSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                        break;
                    case "LightSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                        break;
                    case "LightSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                        break;
                    case "LightSteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                        break;
                    case "LightYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                        break;
                    case "Lime":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                        break;
                    case "LimeGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                        break;
                    case "Linen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                        break;
                    #endregion

                    #region M
                    case "Magenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                        break;
                    case "MediumAquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                        break;
                    case "Maroon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                        break;
                    case "MediumBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                        break;
                    case "MediumOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                        break;
                    case "MediumPurple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                        break;
                    case "MediumSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                        break;
                    case "MediumSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                        break;
                    case "MediumSpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                        break;
                    case "MediumTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                        break;
                    case "MediumVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                        break;
                    case "MidnightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                        break;
                    case "MintCream":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                        break;
                    case "MistyRose":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                        break;
                    case "Moccasin":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                        break;
                    #endregion

                    #region N
                    case "Navy":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                        break;
                    case "NavajoWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                        break;
                    #endregion

                    #region O
                    case "OldLace":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                        break;
                    case "Olive":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                        break;
                    case "OliveDrab":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                        break;
                    case "Orange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                        break;
                    case "OrangeRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                        break;
                    case "Orchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                        break;

                    #endregion

                    #region P
                    case "PaleGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                        break;
                    case "PaleGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                        break;
                    case "PaleTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                        break;
                    case "PaleVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                        break;
                    case "PapayaWhip":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                        break;
                    case "PeachPuff":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                        break;
                    case "Peru":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                        break;
                    case "Pink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                        break;
                    case "Plum":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                        break;
                    case "PowderBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                        break;
                    case "Purple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                        break;
                    #endregion

                    #region R
                    case "RoyalBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                        break;
                    case "Red":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                        break;
                    case "RosyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                        break;
                    #endregion

                    #region S
                    case "SaddleBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                        break;
                    case "Salmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                        break;
                    case "SandyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                        break;
                    case "SeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                        break;
                    case "SeaShell":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                        break;
                    case "Sienna":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                        break;
                    case "Silver":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                        break;
                    case "SkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                        break;
                    case "SlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                        break;
                    case "SlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                        break;
                    case "Snow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                        break;
                    case "SpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                        break;
                    case "SteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                        break;
                    #endregion

                    #region T
                    case "Tan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                        break;
                    case "Teal":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                        break;
                    case "Thistle":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                        break;
                    case "Tomato":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                        break;
                    case "Turquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                        break;
                    #endregion

                    #region V
                    case "Violet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                        break;

                    #endregion

                    #region W
                    case "Wheat":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                        break;
                    case "WhiteSmoke":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                        break;

                    #endregion

                    #region Y
                    case "YellowGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                        break;
                    case "Yellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                        break;
                    #endregion
                    default:
                        break;
                }
            }
        }

        public void CreateDymanicFilledHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;

            switch (colorname)
            {
                case "Red":
                    shape_Secc.Fill.SolidColor.Color = Color.Red;
                    break;
                case "Chocolate":
                    shape_Secc.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Green":
                    shape_Secc.Fill.SolidColor.Color = Color.Green;
                    break;
                default:
                    break;
            }

            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;

            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Calibri (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;
                case "White":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;
                    break;
                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }


        public static string intToRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }
        #endregion

        private bool UpdateAuditStepSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ExternalAuditorUpload"))
                    {
                        if (sheet.Name.Trim().Equals("ExternalAuditorUpload"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }

        private bool ProcessAuditStepUpdate(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ExternalAuditorUpload"];

                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<InternalControlAuditResult> ICARList = new List<InternalControlAuditResult>();
                    List<InternalControlAuditResult> ICARList1 = new List<InternalControlAuditResult>();
                    List<InternalAuditTransaction> IATList = new List<InternalAuditTransaction>();
                    List<InternalAuditTransaction> IATList1 = new List<InternalAuditTransaction>();
                    List<ObservationHistory> OHList = new List<ObservationHistory>();
                    List<ObservationHistory> OHList1 = new List<ObservationHistory>();
                    List<InternalReviewHistory> IRHList = new List<InternalReviewHistory>();
                    List<InternalReviewHistory> IRHList1 = new List<InternalReviewHistory>();

                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string AuditSteps = string.Empty;
                    string ProcessWalkthrough = string.Empty;
                    string ActualWorkDone = string.Empty;
                    string Population = string.Empty;
                    string Sample = string.Empty;
                    string Remark = string.Empty;

                    string ObservationTitle = string.Empty;
                    string Observation = string.Empty;
                    string BriefObservation = string.Empty;
                    string ObservationBackground = string.Empty;
                    string ObservationReport = string.Empty;
                    string BusinessImplication = string.Empty;
                    string RootCause = string.Empty;
                    string FinancialImpact = string.Empty;
                    string Recomendation = string.Empty;
                    //added by rahul on 6 DEC 2017
                    string stringTimeLine = string.Empty;
                    string ManagementResponse = string.Empty;
                    int Personresponsible = -1;
                    int Owner = -1;
                    DateTime timeline = new DateTime();

                    string ObservationRating = string.Empty;
                    int ObservationCategoryID = -1;
                    int ObservationSubCategoryID = -1;

                    int ATBDID = 0;
                    int VID = 0;
                    int CBID = 0;
                    int AUID = 0;
                    int ObservationRatingID = -1;
                    int ObservationReportID = -1;
                    //Verify Data in Excel Document
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        ProcessWalkthrough = string.Empty;
                        ActualWorkDone = string.Empty;
                        Population = string.Empty;
                        Sample = string.Empty;
                        Remark = string.Empty;
                        ObservationTitle = string.Empty;
                        Observation = string.Empty;
                        BriefObservation = string.Empty;
                        ObservationBackground = string.Empty;
                        ObservationReport = string.Empty;
                        BusinessImplication = string.Empty;
                        RootCause = string.Empty;
                        FinancialImpact = string.Empty;
                        Recomendation = string.Empty;
                        ObservationRating = string.Empty;
                        stringTimeLine = string.Empty;
                        ObservationCategoryID = -1;
                        ObservationSubCategoryID = -1;
                        Owner = -1;
                        ATBDID = 0;
                        VID = 0;
                        CBID = 0;
                        AUID = 0;
                        ObservationRatingID = -1;
                        ObservationReportID = -1;
                        ManagementResponse = string.Empty;
                        Personresponsible = -1;
                        timeline = new DateTime();

                        //31 ATBDID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text))
                            ATBDID = Convert.ToInt32(xlWorksheet.Cells[i, 31].Value.ToString());
                        if (ATBDID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the ID at row - " + i + " or ID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;
                        //32 CBID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text))
                            CBID = Convert.ToInt32(xlWorksheet.Cells[i, 32].Value.ToString());
                        if (CBID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the CBID at row - " + i + " or CBID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //33 VID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text))
                            VID = Convert.ToInt32(xlWorksheet.Cells[i, 33].Value.ToString());
                        if (VID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the VID at row - " + i + " or VID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;
                        //34 AUID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text))
                            AUID = Convert.ToInt32(xlWorksheet.Cells[i, 34].Value.ToString());
                        if (AUID == 0)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the AUID at row - " + i + " or AUID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //14 Observation
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text))
                            Observation = Convert.ToString(xlWorksheet.Cells[i, 14].Text).Trim();

                        if (!string.IsNullOrEmpty(Observation))
                        {
                            #region If Observation

                            //13 Observation Title
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                                ObservationTitle = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim();

                            if (ObservationTitle == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Observation Title at row - " + i + " or Observation Title can not be empty.";
                                break;
                            }
                            else
                                suucess = true;

                            //17 Observation Report                           
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text))
                            {
                                ObservationReport = Convert.ToString(xlWorksheet.Cells[i, 17].Text).Trim();
                            }
                            if (ObservationReport == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Observation Report at row - " + i + " or Observation Report can not be empty.";
                                break;
                            }
                            else
                            {
                                suucess = true;
                                if (ObservationReport.ToUpper() == "Audit Commitee".ToUpper())
                                {
                                    ObservationReportID = 1;
                                }
                                //else if (ObservationReport.ToUpper() == "MIS".ToUpper())
                                //{
                                //    ObservationReportID = 0;
                                //}
                                //else if (ObservationReport.ToUpper() == "Both".ToUpper())
                                //{
                                //    ObservationReportID = 2;
                                //}
                                else if (ObservationReport.ToUpper() == "None of the Above".ToUpper())
                                {
                                    ObservationReportID = -1;
                                }
                                else
                                {
                                    ObservationReportID = -1;
                                }
                            }

                            //18 Business Implication
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text))
                                BusinessImplication = Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim();

                            if (BusinessImplication == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Business Implication at row - " + i + " or Business Implication can not be empty.";
                                break;
                            }
                            else
                                suucess = true;

                            //19 Root Cause
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                                RootCause = Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim();

                            if (RootCause == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Root Cause at row - " + i + " or Root Cause can not be empty.";
                                break;
                            }
                            else
                                suucess = true;

                            //20 Financial Impact
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                                FinancialImpact = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();


                            if (FinancialImpact == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Financial Impact (It should be numeric) at row - " + i + " or Financial Impact can not be empty.";
                                break;
                            }
                            else
                            {
                                try
                                {
                                    double n;
                                    var isNumeric = double.TryParse(FinancialImpact, out n);
                                    if (isNumeric)
                                    {
                                        suucess = true;
                                    }
                                    else
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Check the Financial Impact at row - " + i + " or Financial Impact can not be empty.";
                                        break;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Financial Impact at row - " + i + " or Financial Impact can not be empty.";
                                    break;
                                }
                            }
                            //21 Recomendation
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                                Recomendation = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();

                            if (Recomendation == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Recomendation at row - " + i + " or Recomendation can not be empty.";
                                break;
                            }
                            else
                                suucess = true;


                            //22 Management Response
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                                ManagementResponse = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();

                            if (ManagementResponse == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Management Response at row - " + i + " or Management Response can not be empty.";
                                break;
                            }
                            else
                                suucess = true;

                            // 23 Time Line                            
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                                stringTimeLine = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();

                            if (stringTimeLine == "")
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Time Line at row - " + i + " or Time Line can not be empty.";
                                break;
                            }
                            else
                            {
                                try
                                {
                                    bool check = CheckDate(stringTimeLine);
                                    if (check)
                                    {
                                        suucess = true;
                                    }
                                    else
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Check the Time Line at row - " + i + " or Time Line can not be empty.";
                                        break;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Time Line at row - " + i + " or Time Line can not be empty.";
                                    break;
                                }
                            }

                            //24 Personresponsible Name                            
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                            {   //25 Personresponsible email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                {
                                    Personresponsible = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 24].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 25].Text.ToString().Trim());
                                }
                            }

                            if (Personresponsible == 0 || Personresponsible == -1)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Correct the Person Responsible Name/Email at row number - " + i + " or Person Responsible not Defined in the System.";
                                break;
                            }
                            else
                                suucess = true;

                            //26 Observation Rating    
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text))
                            {
                                ObservationRating = xlWorksheet.Cells[i, 26].Text.Trim();
                            }
                            if (ObservationRating.ToUpper() == "MAJOR")
                            {
                                ObservationRatingID = 1;
                            }
                            else if (ObservationRating.ToUpper() == "MODERATE")
                            {
                                ObservationRatingID = 2;
                            }
                            else if (ObservationRating.ToUpper() == "MINOR")
                            {
                                ObservationRatingID = 3;
                            }
                            else
                            {
                                ObservationRatingID = 0;
                            }
                            if (ObservationRatingID == 0)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Correct the Observation Rating  at row - " + i + " or Observation Rating  can not left blank.";
                                break;
                            }

                            //27 Observation Category
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                            {
                                ObservationCategoryID = ObservationSubcategory.GetObservationCategoryByName(Regex.Replace(xlWorksheet.Cells[i, 27].Text.ToString().Trim(), @"\t|\n|\r", ""), customerID);
                            }
                            if (ObservationCategoryID == 0 || ObservationCategoryID == -1)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Observation Category at row - " + i + " or Observation Category can not be empty.";
                                break;
                            }
                            else
                            {
                                suucess = true;
                            }

                            //29 Owner Name                            
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                            {   //30 Owner email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                                {
                                    Owner = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 29].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 30].Text.ToString().Trim());
                                }
                            }

                            if (Owner == 0 || Owner == -1)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Correct the Owner Name/Email at row number - " + i + " or Owner not Defined in the System.";
                                break;
                            }
                            else
                                suucess = true;
                            #endregion
                        }
                        else
                        {
                            #region IF No Observation
                            //20 Financial Impact
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                            {
                                FinancialImpact = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();

                                try
                                {
                                    double n;
                                    var isNumeric = double.TryParse(FinancialImpact, out n);
                                    if (isNumeric)
                                    {
                                        suucess = true;
                                    }
                                    else
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Check the Financial Impact at row - " + i + " or Financial Impact can not be empty.";
                                        break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Financial Impact at row - " + i + " or Financial Impact can not be empty.";
                                    break;
                                }
                            }
                            // 23 Time Line                            
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text))
                            {
                                stringTimeLine = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                                try
                                {
                                    bool check = CheckDate(stringTimeLine);
                                    if (check)
                                    {
                                        suucess = true;
                                    }
                                    else
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Check the Time Line at row - " + i + " or Time Line can not be empty.";
                                        break;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Time Line at row - " + i + " or Time Line can not be empty.";
                                    break;
                                }

                            }
                            //24 Personresponsible Name                            
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                            {   //25 Personresponsible email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                {
                                    Personresponsible = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 24].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 25].Text.ToString().Trim());
                                    if (Personresponsible == 0 || Personresponsible == -1)
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Person Responsible Name/Email at row number - " + i + " or Person Responsible not Defined in the System.";
                                        break;
                                    }
                                    else
                                        suucess = true;
                                }
                            }

                            //26 Observation Rating    
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text))
                            {
                                ObservationRating = xlWorksheet.Cells[i, 26].Text.Trim();
                                if (ObservationRating.ToUpper() == "MAJOR")
                                {
                                    ObservationRatingID = 1;
                                }
                                else if (ObservationRating.ToUpper() == "MODERATE")
                                {
                                    ObservationRatingID = 2;
                                }
                                else if (ObservationRating.ToUpper() == "MINOR")
                                {
                                    ObservationRatingID = 3;
                                }
                                else
                                {
                                    ObservationRatingID = 0;
                                }
                                if (ObservationRatingID == 0)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Correct the Observation Rating  at row - " + i + " or Observation Rating  can not left blank.";
                                    break;
                                }
                            }
                            //27 Observation Category
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                            {
                                ObservationCategoryID = ObservationSubcategory.GetObservationCategoryByName(Regex.Replace(xlWorksheet.Cells[i, 27].Text.ToString().Trim(), @"\t|\n|\r", ""), customerID);
                                if (ObservationCategoryID == 0 || ObservationCategoryID == -1)
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Observation Category at row - " + i + " or Observation Category can not be empty.";
                                    break;
                                }
                                else
                                {
                                    suucess = true;
                                }
                            }

                            //29 Owner Name                            
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                            {   //30 Owner email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                                {
                                    Owner = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 29].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 30].Text.ToString().Trim());
                                }
                            }

                            if (Owner == 0 || Owner == -1)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Correct the Owner Name/Email at row number - " + i + " or Owner not Defined in the System.";
                                break;
                            }
                            else
                                suucess = true;
                            #endregion
                        }



                        //28 Observation Sub Category                      
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                        {
                            ObservationSubCategoryID = ObservationSubcategory.GetObservationSubCategoryByName(Regex.Replace(xlWorksheet.Cells[i, 28].Text.ToString().Trim(), @"\t|\n|\r", ""), ObservationCategoryID, customerID);
                            if (ObservationSubCategoryID == 0 || ObservationSubCategoryID == -1)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Observation Sub Category at row - " + i + " or Observation Sub Category can not be empty.";
                                break;
                            }
                            else
                            {
                                suucess = true;
                            }
                        }
                        else
                        {
                            suucess = true;
                        }


                        //7 AuditSteps
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                            AuditSteps = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                        //8 Process Walkthrough
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                            ProcessWalkthrough = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();

                        //9 Actual Work Done
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text))
                            ActualWorkDone = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();

                        //10 Population
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                            Population = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();

                        //11 Sample
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text))
                            Sample = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim();

                        //12 Remark
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text))
                            Remark = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();

                        //15 Brief Observation
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                            BriefObservation = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();

                        //17 Observation Background
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text))
                            ObservationBackground = Convert.ToString(xlWorksheet.Cells[i, 16].Text).Trim();

                    }

                    if (suucess)
                    {
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            ProcessWalkthrough = string.Empty;
                            ActualWorkDone = string.Empty;
                            Population = string.Empty;
                            Sample = string.Empty;
                            Remark = string.Empty;
                            ObservationTitle = string.Empty;
                            Observation = string.Empty;
                            ObservationReport = string.Empty;
                            BusinessImplication = string.Empty;
                            RootCause = string.Empty;
                            FinancialImpact = string.Empty;
                            Recomendation = string.Empty;
                            ObservationRating = string.Empty;
                            ObservationCategoryID = -1;
                            ObservationSubCategoryID = -1;
                            ATBDID = 0;
                            VID = 0;
                            CBID = 0;
                            AUID = 0;
                            ObservationRatingID = -1;
                            ObservationReportID = -1;
                            ManagementResponse = string.Empty;
                            stringTimeLine = string.Empty;
                            Personresponsible = -1;
                            timeline = new DateTime();

                            //7 AuditSteps
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text))
                                AuditSteps = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                            //8 Process Walkthrough
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text))
                                ProcessWalkthrough = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();

                            //9 Actual Work Done
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text))
                                ActualWorkDone = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();

                            //10 Population
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                                Population = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();

                            //11 Sample
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text))
                                Sample = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim();

                            //12 Remark
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text))
                                Remark = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();


                            //13 Observation Title
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text))
                                ObservationTitle = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim();

                            //14 Observation
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text))
                                Observation = Convert.ToString(xlWorksheet.Cells[i, 14].Text).Trim();

                            //15 Brief Observation
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                                BriefObservation = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();

                            //16 Observation Background
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text))
                                ObservationBackground = Convert.ToString(xlWorksheet.Cells[i, 16].Text).Trim();

                            //17 Observation Report                           
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text))
                                ObservationReport = Convert.ToString(xlWorksheet.Cells[i, 17].Text).Trim();

                            if (ObservationReport.ToUpper() == "Audit Commitee".ToUpper())
                            {
                                ObservationReportID = 1;
                            }
                            //else if (ObservationReport.ToUpper() == "MIS".ToUpper())
                            //{
                            //    ObservationReportID = 0;
                            //}
                            //else if (ObservationReport.ToUpper() == "Both".ToUpper())
                            //{
                            //    ObservationReportID = 2;
                            //}
                            else if (ObservationReport.ToUpper() == "None of the Above".ToUpper())
                            {
                                ObservationReportID = -1;
                            }
                            else
                            {
                                ObservationReportID = -1;
                            }

                            //18 Business Implication
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text))
                                BusinessImplication = Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim();

                            //19 Root Cause
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text))
                                RootCause = Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim();

                            //20 Financial Impact
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text))
                                FinancialImpact = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();

                            //21 Recomendation
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text))
                                Recomendation = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();


                            //22 Management Response
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text))
                                ManagementResponse = Convert.ToString(xlWorksheet.Cells[i, 22].Text).Trim();

                            // 23 Time Line                           
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                            {
                                stringTimeLine = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                                string c = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                                DateTime? aaaa = CleanDateField(c);
                                DateTime dt1 = Convert.ToDateTime(aaaa);
                                timeline = GetDate(dt1.ToString("dd/MM/yyyy"));
                            }


                            //24 25 Personresponsibal
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                            {   //25 Personresponsible email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                {
                                    Personresponsible = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 24].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 25].Text.ToString().Trim());
                                }
                            }


                            //26 Observation Rating    
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text))
                            {
                                ObservationRating = xlWorksheet.Cells[i, 26].Text.Trim();
                            }
                            if (ObservationRating.ToUpper() == "MAJOR")
                            {
                                ObservationRatingID = 1;
                            }
                            else if (ObservationRating.ToUpper() == "MODERATE")
                            {
                                ObservationRatingID = 2;
                            }
                            else if (ObservationRating.ToUpper() == "MINOR")
                            {
                                ObservationRatingID = 3;
                            }
                            else
                            {
                                ObservationRatingID = 0;
                            }
                            //27 Observation Category
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                                ObservationCategoryID = ObservationSubcategory.GetObservationCategoryByName(Regex.Replace(xlWorksheet.Cells[i, 27].Text.ToString().Trim(), @"\t|\n|\r", ""), customerID);

                            //28 Observation Sub Category                      
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                                ObservationSubCategoryID = ObservationSubcategory.GetObservationSubCategoryByName(Regex.Replace(xlWorksheet.Cells[i, 28].Text.ToString().Trim(), @"\t|\n|\r", ""), ObservationCategoryID, customerID);

                            //29 30 Personresponsibal
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                            {   //30 Personresponsible email
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 39].Text.ToString().Trim()))
                                {
                                    Owner = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 29].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 30].Text.ToString().Trim());
                                }
                            }

                            //31 ATBDID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text))
                                ATBDID = Convert.ToInt32(xlWorksheet.Cells[i, 31].Value.ToString());

                            //32 CBID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text))
                                CBID = Convert.ToInt32(xlWorksheet.Cells[i, 32].Value.ToString());


                            //33 VID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text))
                                VID = Convert.ToInt32(xlWorksheet.Cells[i, 33].Value.ToString());

                            //34 AUID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text))
                                AUID = Convert.ToInt32(xlWorksheet.Cells[i, 34].Value.ToString());

                            var details = UserManagementRisk.GetDataAuditClosureDetails(AUID);
                            if (details != null)
                            {
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                long userid = UserManagementRisk.InternalControlAuditAssignmentsPerformerID(CBID, VID, details.FinancialYear, details.ForMonth, AUID);
                                if (userid != -1)
                                {
                                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(Convert.ToInt32(userid), 3, details.FinancialYear, details.ForMonth, CBID, ATBDID, VID, AUID);
                                    if (getScheduleonDetails != null)
                                    {
                                        #region  InternalControlAuditResult
                                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                        {
                                            AuditScheduleOnID = getScheduleonDetails.ID,
                                            ProcessId = getScheduleonDetails.ProcessId,
                                            FinancialYear = details.FinancialYear,
                                            ForPerid = details.ForMonth,
                                            CustomerBranchId = CBID,
                                            IsDeleted = false,
                                            AuditObjective = "",
                                            AuditSteps = AuditSteps.Trim(),
                                            AnalysisToBePerofrmed = "",
                                            ProcessWalkthrough = ProcessWalkthrough,
                                            ActivityToBeDone = ActualWorkDone.Trim(),
                                            Population = Population.Trim(),
                                            Sample = Sample.Trim(),
                                            ObservationNumber = "",
                                            ObservationTitle = ObservationTitle.Trim(),
                                            Observation = Observation.Trim(),
                                            BriefObservation = BriefObservation.Trim(),
                                            ObjBackground = ObservationBackground.Trim(),
                                            Risk = BusinessImplication.Trim(),
                                            RootCost = RootCause.Trim(),
                                            Recomendation = Recomendation.Trim(),
                                            ObservationRating = Convert.ToInt32(ObservationRatingID),
                                            ObservationCategory = Convert.ToInt32(ObservationCategoryID),
                                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                            ATBDId = ATBDID,
                                            UserID = userid,
                                            RoleID = 3,
                                            FixRemark = Remark.Trim(),
                                            AStatusId = 2,
                                            ObservationSubCategory = Convert.ToInt32(ObservationSubCategoryID),
                                            VerticalID = VID,
                                            AuditScores = 0,
                                            ISACPORMIS = ObservationReportID,
                                            Dated = DateTime.Now,
                                            ManagementResponse = ManagementResponse,
                                            //TimeLine = timeline.Date,
                                            PersonResponsible = Personresponsible,
                                            Owner = Owner,
                                            AuditID = AUID,
                                        };
                                        if (FinancialImpact.Trim() == "")
                                            MstRiskResult.FinancialImpact = null;
                                        else
                                            MstRiskResult.FinancialImpact = FinancialImpact;// changed by sagar more on 24-01-2020

                                        if (stringTimeLine.Trim() == "")
                                            MstRiskResult.TimeLine = null;
                                        else
                                            MstRiskResult.TimeLine = timeline.Date;

                                        ICARList.Add(MstRiskResult);
                                        #endregion

                                        #region InternalAuditTransaction
                                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                                        {
                                            StatusId = 2,
                                            Remarks = "Audit Steps Submited.",
                                            Dated = DateTime.Now,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                            AuditScheduleOnID = getScheduleonDetails.ID,
                                            FinancialYear = details.FinancialYear,
                                            CustomerBranchId = Convert.ToInt32(CBID),
                                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                            ProcessId = getScheduleonDetails.ProcessId,
                                            ObservatioRating = Convert.ToInt32(ObservationRatingID),
                                            ObservationCategory = Convert.ToInt32(ObservationCategoryID),
                                            BriefObservation = BriefObservation.Trim(),
                                            ForPeriod = details.ForMonth,
                                            ATBDId = Convert.ToInt32(ATBDID),
                                            UserID = userid,
                                            RoleID = 3,
                                            ObservationSubCategory = Convert.ToInt32(ObservationSubCategoryID),
                                            VerticalID = VID,
                                            PersonResponsible = Personresponsible,
                                            Owner = Owner,
                                            AuditID = AUID,
                                        };
                                        IATList.Add(transaction);

                                        #endregion

                                        #region ObservationHistory
                                        ObservationHistory objHistory = new ObservationHistory()
                                        {
                                            UserID = userid,
                                            RoleID = 3,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            ATBTID = Convert.ToInt32(ATBDID),
                                            ISACPORMIS = ObservationReportID,
                                            ProcessWalkthrough = ProcessWalkthrough,
                                            ActualWorkDone = ActualWorkDone.Trim(),
                                            Population = Population.Trim(),
                                            Sample = Sample.Trim(),
                                            ObservationTitle = ObservationTitle.Trim(),
                                            Observation = Observation.Trim(),
                                            Risk = BusinessImplication.Trim(),
                                            RootCause = RootCause.Trim(),
                                            ManagementResponse = ManagementResponse.Trim(),
                                            PersonResponsible = Personresponsible,
                                            Owner = Owner,
                                            Score = "0",
                                            Recommendation = Recomendation.Trim(),
                                            ObservationCategory = Convert.ToInt32(ObservationCategoryID),
                                            ObservationSubCategory = Convert.ToInt32(ObservationSubCategoryID),
                                            ObservationRating = Convert.ToInt32(ObservationRatingID),
                                            Remarks = Remark,
                                            AuditId = AUID,
                                            ISACPORMISOld = null,
                                            UserOld = null,
                                            ObservationSubCategoryOld = null,
                                            ObservationCategoryOld = null,
                                            ObservationRatingOld = null,
                                            ObservationOld = "",
                                            ProcessWalkthroughOld = "",
                                            ActualWorkDoneOld = "",
                                            PopulationOld = "",
                                            SampleOld = "",
                                            ObservationTitleOld = "",
                                            RiskOld = "",
                                            RootCauseOld = "",
                                            FinancialImpactOld = "",
                                            RecommendationOld = "",
                                            ManagementResponseOld = "",
                                            RemarksOld = "",
                                            ScoreOld = "",
                                            TimeLineOld = null,
                                            PersonResponsibleOld = null,
                                            OwnerOld = null,
                                        };
                                        if (FinancialImpact.Trim() == "")
                                            objHistory.FinancialImpact = null;
                                        else
                                            objHistory.FinancialImpact = FinancialImpact;

                                        if (stringTimeLine.Trim() == "")
                                            objHistory.TimeLine = null;
                                        else
                                            objHistory.TimeLine = timeline.Date;

                                        OHList.Add(objHistory);

                                        #endregion

                                        #region InternalReviewHistory
                                        InternalReviewHistory RH = new InternalReviewHistory()
                                        {
                                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            Dated = DateTime.Now,
                                            Remarks = "NA",
                                            AuditScheduleOnID = getScheduleonDetails.ID,
                                            FinancialYear = details.FinancialYear,
                                            CustomerBranchId = Convert.ToInt32(CBID),
                                            ATBDId = Convert.ToInt32(ATBDID),
                                            FixRemark = Remark,
                                            VerticalID = Convert.ToInt32(VID),
                                            AuditID = AUID,
                                        };
                                        IRHList.Add(RH);
                                        #endregion

                                    }//getScheduleonDetails != null end
                                }//userid != -1 end
                            }//details != null end
                        }//  for End
                        #region
                        ICARList1 = ICARList.Where(entry => entry.CustomerBranchId == 0 && entry.VerticalID == 0 && entry.FinancialYear == "" && entry.ForPerid == "").ToList();
                        IATList1 = IATList.Where(entry => entry.CustomerBranchId == 0 && entry.VerticalID == 0 && entry.FinancialYear == "").ToList();
                        OHList1 = OHList.Where(entry => entry.UserID == 0 && entry.AuditId == 0).ToList();
                        IRHList1 = IRHList.Where(entry => entry.CustomerBranchId == 0 && entry.VerticalID == 0 && entry.FinancialYear == "" && entry.ForMonth == "").ToList();
                        if (ICARList1.Count == 0)
                        {
                            if (IATList1.Count == 0)
                            {
                                if (OHList1.Count == 0)
                                {
                                    if (IRHList1.Count == 0)
                                    {
                                        suucess = RiskCategoryManagement.BulkInternalControlAuditResultInsert(ICARList);

                                        if (suucess)
                                        {
                                            suucess = RiskCategoryManagement.BulkInternalAuditTransactionInsert(IATList);
                                            if (suucess)
                                            {
                                                suucess = RiskCategoryManagement.BulkObservationHistoryInsert(OHList);
                                                if (suucess)
                                                {
                                                    suucess = RiskCategoryManagement.BulkInternalReviewHistoryInsert(IRHList);
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again(ObservationHistory).";
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again(InternalAuditTransaction).";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again(InternalControlAuditResult).";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document before uploading.";
                                        suucess = false;
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document before uploading.";
                                    suucess = false;
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document before uploading.";
                                suucess = false;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check excel document before uploading.";
                            suucess = false;
                        }
                        #endregion
                        return true;
                    }//suucess end
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected void btnUploadObservation_Click(object sender, EventArgs e)
        {
            if (fnUploadObservation.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(fnUploadObservation.FileName);
                    fnUploadObservation.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = UpdateAuditStepSheetsExitsts(xlWorkbook, "ExternalAuditorUpload");
                            if (flag == true)
                            {
                                suucess = ProcessAuditStepUpdate(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ExternalAuditorUpload'.";
                            }


                            if (suucess)
                            {
                                cvDuplicateEntry.IsValid = false;
                                //cvDuplicateEntry.ForeColor = Color.Green;
                                BindData();
                                cvDuplicateEntry.ErrorMessage = "Data Uploaded Successfully.";
                            }
                            //else
                            //{
                            //    cvDuplicateEntry.IsValid = false;
                            //    cvDuplicateEntry.ErrorMessage = "Data Uploaded Successfully.";
                            //}
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }

        }
    }
}