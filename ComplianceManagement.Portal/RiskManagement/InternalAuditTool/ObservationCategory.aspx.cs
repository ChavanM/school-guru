﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ObservationCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindAuditorList();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindAuditorList();
           // bindPageNumber();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        private void BindAuditorList()
        {
            try
            {
               
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var AuditorMasterList = ProcessManagement.GetAllObservationCategory(customerID);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                              
                Mst_ObservationCategory mstauditormaster = new Mst_ObservationCategory()
                {
                    Name = txtFName.Text,
                    IsActive = false,
                    CustomerID = customerID,
                };
                
                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.ObservationCategoryExists(customerID,mstauditormaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Observation Category Already Exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        ProcessManagement.CreateObservationCategory(mstauditormaster);
                        cvDuplicateEntry.ErrorMessage = "Observation Category Save Successfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ProcessManagement.ObservationCategoryExists(customerID, mstauditormaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Observation Category Already Exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        mstauditormaster.ID = Convert.ToInt32(ViewState["ObservationID"]);
                        ProcessManagement.UpdateObservationCategory(mstauditormaster);
                        cvDuplicateEntry.ErrorMessage = "Observation Category Updated Successfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorDialog\").dialog('close')", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                BindAuditorList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;                
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ObservationID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ObservationID"] = ObservationID;

                    Mst_ObservationCategory RPD = ProcessManagement.ObservationCategoryGetByID(ObservationID);

                    if (RPD != null)
                        txtFName.Text = RPD.Name;

                    upPromotor.Update();                  
                }
                else if (e.CommandName.Equals("DELETE_PROMOTER"))
                {
                    ProcessManagement.DeleteObservationCategory(ObservationID);
                    BindAuditorList();
                }
                else if (e.CommandName.Equals("VIEW_Category"))
                {
                    //GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //int RowIndex = gvr.RowIndex;
                    Session["ObservationID"] = ObservationID;
                    Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationCategoryList.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditor.PageIndex = e.NewPageIndex;
                BindAuditorList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void grdAuditor_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}
        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        //BindGrid();
        //        BindAuditorList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        //BindGrid();
        //        BindAuditorList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                //BindGrid();
                BindAuditorList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                return true;
                //if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                //{
                //    SelectedPageNo.Text = "1";
                //    return false;
                //}
                //else if (!IsNumeric(SelectedPageNo.Text))
                //{
                //    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}
            }
            catch (FormatException)
            {
                return false;
            }
        }


    }
}