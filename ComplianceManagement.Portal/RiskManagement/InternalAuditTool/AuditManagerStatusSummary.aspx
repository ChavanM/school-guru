﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditManagerStatusSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditManagerStatusSummary" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }

        <style type="text/css" > span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Manager Status Summary');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                                                           
                             <div class="panel-body">
                                <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width:13%;">
                                        <div class="col-md-3 colpadding0" style="margin-right: 8px;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" Selected="True" />
                                        </asp:DropDownList>
                                    </div> 
                                                                                                                
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%">                                                           
                                        <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" Width="90%" Height="32px" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                 
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%" >                                                           
                                          <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>                                          
                                    </div> 

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:47%" > <%--text-align: right; --%>    
                                        <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" OnClick="lbtnExportExcel_Click" style="width: 126px;"  runat="server"/> &nbsp;                                     
                                        <asp:Button runat="server" ID="lbtObservationList" Text="Draft Observation Listing" class="btn btn-primary" style="width: 170px;" OnClick="lbtObservationList_Click"/>                                                                                       
                                        <asp:Button runat="server" ID="btnDraftClosure" Text="Draft Closure"  class="btn btn-primary" style="width: 126px;" OnClick="btnDraftClosure_Click"/>                                         
                                        <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary"  OnClick="btnBack_Click" /> 
                                        <asp:Label id="lblFromDashboardReach" runat="server" Text="" Visible="false"></asp:Label>
                                    </div> 
                                                          
                                </div>                                                              
                                <div class="clearfix"></div> 
                                 <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 20%; display:none;">
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                    RepeatLayout="Flow">
                                    <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                    <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                    </div>                                                              
                                <div class="clearfix"></div>                                
                                <div style="margin-bottom: 4px">
                                    &nbsp;
                                    <asp:GridView runat="server" ID="grdAuditStatus" AutoGenerateColumns="false" 
                                        PageSize="50" AllowPaging="true" AutoPostBack="true" OnRowCommand="grdAuditStatus_RowCommand" onrowdatabound="grdAuditStatus_RowDataBound"
                                        CssClass="table" GridLines="None" Width="100%" AllowSorting="true" RowStyle-HorizontalAlign="Center"
                                        OnPageIndexChanging="grdAuditStatus_PageIndexChanging" ShowFooter="true">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                             <ItemTemplate>
                                                 <%#Container.DataItemIndex+1 %>
                                             </ItemTemplate>
                                             </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Process" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="left" FooterStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblProcess" runat="server" Text='<%#Eval("Process") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Process") %>' />
                                                         </div>
                                                </ItemTemplate>                           
                                            </asp:TemplateField>

                                             <asp:TemplateField HeaderText="Sub Process" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="left" FooterStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblSubProcess" runat="server" Text='<%#Eval("SubProcess") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcess") %>' />
                                                         </div>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRiskTotal" runat="server"
                                                        Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Not&nbsp;Done" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRiskNotDone" runat="server"                                                         
                                                        Text='<%# Eval("NotDone") %>' CausesValidation="false"></asp:LinkButton>


                                                    <%--CommandName="DRILLDOWN" 
                                                        CommandArgument='<%# "NotDone;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'--%>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Submitted" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                        Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                                </ItemTemplate>                           
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Team&nbsp;Review" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRiskTeamReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "TeamReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                        Text='<%# Eval("TeamReview") %>' CausesValidation="false"></asp:LinkButton>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Auditee&nbsp;Review" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblAuditeeReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                        Text='<%# Eval("AuditeeReview") %>' CausesValidation="false"></asp:LinkButton>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Final&nbsp;Review" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblFinalReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "FinalReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear") %>'
                                                        Text='<%# Eval("FinalReview") %>' CausesValidation="false"></asp:LinkButton>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                        Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                                </ItemTemplate>                            
                                            </asp:TemplateField>   
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />  
                                        <HeaderStyle BackColor="#ECF0F1" />   
                                         <PagerSettings Visible="false" />                 
                                        <PagerTemplate>
                                       
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                              No Records Found.
                                         </EmptyDataTemplate> 
                                    </asp:GridView>
                                    <div style="float: right;">
                                          <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                              class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                          </asp:DropDownListChosen>  
                                    </div>
                                 </div>
                                <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                <div class="col-md-6 colpadding0" runat="server" id="ObjUploadDiv">
                                     <div>                                      
                                         <label runat="server" id="lbluploadobservation" style="width: 148px; display: block; float: left; font-size: 14px; color: #8e8e93; margin-bottom: 10px;">Upload Observations</label>
                                    </div> 
                                    <div  style="float: left;">
                                      <asp:FileUpload ID="fnUploadObservation" runat="server" ForeColor="Black"></asp:FileUpload>
                                    </div>
                                    <div style="float: right;">     
                                      <asp:Button ID="btnUploadObservation" runat="server"   class="btn btn-primary" Text="Upload" OnClick="btnUploadObservation_Click"></asp:Button>
                                    </div>
                                </div>                             
                               </div>
                                <div class="col-md-5 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                    
                                        <div class="table-paging-text" style="float: right;">
                                            <p>Page</p>
                                        </div>                                    
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                              </div>     
                              </div>      
                       </section>
                    </div>
                </div>
            </div>            
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
             <asp:PostBackTrigger ControlID="btnDraftClosure" />
               <asp:PostBackTrigger ControlID="btnUploadObservation" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
