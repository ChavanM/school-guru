﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditListForKickOff : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindVertical();
                BindFinancialYearFilter();
                BindPerformerFilter();
                BindReviewerFilter();
                BindReviewerFilter2();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancialYear.ClearSelection();
                    ddlFilterFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                liProcess_Click(sender, e);
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            grdProcessAudit.PageIndex = chkSelectedPage - 1;
            BindProcessAudits();
        }
        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }
        public void BindVertical()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlVerticalID.DataTextField = "VerticalName";
            ddlVerticalID.DataValueField = "ID";
            ddlVerticalID.Items.Clear();
            ddlVerticalID.DataSource = UserManagementRisk.FillVerticalList(customerID); //FillVerticalList(int customerID)
            ddlVerticalID.DataBind();
            ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
        }
        public void BindFinancialYearFilter()
        {
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        public void BindPerformerFilter()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlPerformerFilter.Items.Clear();
            ddlPerformerFilter.DataTextField = "Name";
            ddlPerformerFilter.DataValueField = "ID";
            ddlPerformerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
            ddlPerformerFilter.DataBind();
            // ddlPerformerFilter.Items.Insert(0, new ListItem("Performer ", "-1"));

        }
        public void BindReviewerFilter()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlReviewerFilter.Items.Clear();
            ddlReviewerFilter.DataTextField = "Name";
            ddlReviewerFilter.DataValueField = "ID";
            ddlReviewerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
            ddlReviewerFilter.DataBind();
            ddlReviewerFilter.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
        }
        public void BindReviewerFilter2()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlReviewerFilter2.Items.Clear();
            ddlReviewerFilter2.DataTextField = "Name";
            ddlReviewerFilter2.DataValueField = "ID";
            ddlReviewerFilter2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
            ddlReviewerFilter2.DataBind();
            ddlReviewerFilter2.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
        }
        protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.Items.Insert(0, new ListItem("Auditor", "-1"));
                ddlAuditor.Enabled = false;
                BindPerformerFilter();
                BindReviewerFilter();
                BindReviewerFilter2();
                BindProcessAudits();
            }
            else
            {
                ddlAuditor.Enabled = true;
                BindAuditor();
                if (ddlPerformerFilter.Items.Count > 0)
                    ddlPerformerFilter.Items.Clear();

                if (ddlReviewerFilter.Items.Count > 0)
                    ddlReviewerFilter.Items.Clear();

                if (ddlReviewerFilter2.Items.Count > 0)
                    ddlReviewerFilter2.Items.Clear();
            }
        }
        private void BindAuditor()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                ddlAuditor.Items.Clear();
                ddlAuditor.DataTextField = "Name";
                ddlAuditor.DataValueField = "ID";
                ddlAuditor.DataSource = ProcessManagement.GetAllAuditorMaster(customerID);
                ddlAuditor.DataBind();
                ddlAuditor.Items.Insert(0, new ListItem("Auditor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveFlag = false;
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Portal.Common.AuthenticationHelper.UserID;
                string userName = string.Empty;
                userName = Portal.Common.AuthenticationHelper.User;
                List<int> distinctprocess = new List<int>();
                List<int> distinctprocessImp = new List<int>();
                #region Process

                List<InternalControlAuditAssignment> tempAssignmentList = new List<InternalControlAuditAssignment>();
                List<InternalAuditTransaction> tempTransactionList = new List<InternalAuditTransaction>();
                List<RiskActivityToBeDoneMapping> ActiveAuditStepIDList = new List<RiskActivityToBeDoneMapping>();
                string listNotSaveList = string.Empty;
                tempAssignmentList.Clear();
                tempTransactionList.Clear();
                ActiveAuditStepIDList.Clear();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ActiveAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                             where RATBDM.IsActive == true
                                             && RATBDM.RCMType != "IFC"
                                             select RATBDM).Distinct().ToList();
                    foreach (GridViewRow g1 in grdProcessAudit.Rows)
                    {
                        #region
                        DropDownList ddlper = (g1.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                        DropDownList ddlrev2 = (g1.FindControl("ddlReviewer2") as DropDownList);
                        DropDownList ddlPerImp = (g1.FindControl("ddlPerformerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP = (g1.FindControl("ddlReviewerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP2 = (g1.FindControl("ddlReviewerIMP2") as DropDownList);
                        List<int> Auditstepdetails = new List<int>();
                        string lblLocation = (g1.FindControl("lblLocation") as Label).Text;
                        string lblLocationId = (g1.FindControl("lblLocationId") as Label).Text;
                        string lblVerticalId = (g1.FindControl("lblVerticalId") as Label).Text;
                        string lblVerticalName = (g1.FindControl("lblVerticalName") as Label).Text;
                        string lblAuditID = (g1.FindControl("lblAuditID") as Label).Text;
                        List<string> PerformerList = new List<string>();
                        List<string> PerformerListImplem = new List<string>();
                        string lblItemTemplateFinancialYear = (g1.FindControl("lblFinancialYear") as Label).Text;
                        string lblItemTemplateForPeriod = (g1.FindControl("lblTermName") as Label).Text;

                        string lblPerformerUserID = (g1.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                        string lblReviewerUserID = (g1.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                        string lblReviewerUserID2 = (g1.FindControl("ddlReviewer2") as DropDownList).SelectedItem.Value;

                        string lblPerformerIMPUserID = (g1.FindControl("ddlPerformerIMP") as DropDownList).SelectedItem.Value;
                        string lblReviewerIMPUserID = (g1.FindControl("ddlReviewerIMP") as DropDownList).SelectedItem.Value;
                        string lblReviewerIMPUserID2 = (g1.FindControl("ddlReviewerIMP2") as DropDownList).SelectedItem.Value;
                        //string lblPerformerUserID = string.Empty;
                        //string lblPerformerIMPUserID = string.Empty;
                        //for (int i = 0; i < ddlper.Items.Count; i++)
                        //{
                        //    if (ddlper.Items[i].Selected)
                        //    {
                        //        PerformerList.Add(ddlper.Items[i].Value);
                        //        lblPerformerUserID = ddlper.Items[i].Value;
                        //    }
                        //}
                        //for (int i = 0; i < ddlPerImp.Items.Count; i++)
                        //{
                        //    if (ddlPerImp.Items[i].Selected)
                        //    {
                        //        PerformerListImplem.Add(ddlPerImp.Items[i].Value);
                        //        lblPerformerIMPUserID = ddlPerImp.Items[i].Value;
                        //    }
                        //}

                        if (lblReviewerUserID == lblReviewerUserID2 && lblReviewerUserID != "-1" && lblReviewerUserID2 != "-1")
                        {
                            listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                        }
                        else if (lblReviewerIMPUserID == lblReviewerIMPUserID2 && lblReviewerIMPUserID != "-1" && lblReviewerIMPUserID2 != "-1")
                        {
                            listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                        }
                        else
                        {
                            if (lblLocationId != "" && lblVerticalId != "" && lblItemTemplateFinancialYear != "" && lblItemTemplateForPeriod != "" && lblAuditID != "")
                            {
                                #region Audit
                                Auditstepdetails = UserManagementRisk.GetAuditStepDetailsCount(ActiveAuditStepIDList, Convert.ToInt32(lblLocationId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), userID);
                                if (Auditstepdetails != null)
                                {
                                    if (Auditstepdetails.Count > 0)
                                    {
                                        //Get Scheduled Processes in each Audit
                                        var ScheduledProcessList = UserManagementRisk.GetProcessListAuditScheduled(Convert.ToInt32(lblLocationId), Convert.ToInt32(lblVerticalId), lblItemTemplateFinancialYear, lblItemTemplateForPeriod, Convert.ToInt32(lblAuditID), userID);
                                        #region Audit
                                        if (lblPerformerUserID != "-1" && lblPerformerUserID != "" && ddlper.Enabled && ddlrev.Enabled && lblReviewerUserID != "-1" && lblReviewerUserID != "")
                                        {
                                            string AssignedTo = "";
                                            if (ddlInternalExternal.SelectedItem.Text == "Internal")
                                                AssignedTo = "I";
                                            else
                                                AssignedTo = "E";

                                            if (ScheduledProcessList.Count > 0)
                                            {
                                                tempAssignmentList.Clear();
                                                tempTransactionList.Clear();
                                                InternalAuditInstance riskinstance = new InternalAuditInstance();
                                                InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                                                InternalAuditTransaction transaction = new InternalAuditTransaction();

                                                ScheduledProcessList.ForEach(EachProcess =>
                                                {
                                                    #region Instance Save
                                                    riskinstance.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstance.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstance.ProcessId = EachProcess.Process;
                                                    riskinstance.SubProcessId = -1;
                                                    riskinstance.IsDeleted = false;
                                                    riskinstance.CreatedOn = DateTime.Now;
                                                    riskinstance.CreatedBy = userID;
                                                    riskinstance.AuditID = Convert.ToInt64(lblAuditID);

                                                    auditScheduleon.FinancialYear = lblItemTemplateFinancialYear; //FinancialYear;
                                                    auditScheduleon.ForMonth = lblItemTemplateForPeriod;//Period;
                                                    auditScheduleon.ProcessId = EachProcess.Process;
                                                    auditScheduleon.CreatedOn = DateTime.Now;
                                                    auditScheduleon.CreatedBy = userID;
                                                    auditScheduleon.AuditID = Convert.ToInt64(lblAuditID);

                                                    transaction.AuditScheduleOnID = auditScheduleon.ID;
                                                    transaction.CustomerBranchId = Convert.ToInt32(lblLocationId); //branchid,
                                                    transaction.VerticalID = Convert.ToInt32(lblVerticalId); //VerticalID,
                                                    transaction.ProcessId = EachProcess.Process; //Processid,
                                                    transaction.SubProcessId = -1;
                                                    transaction.CreatedByText = userName;
                                                    transaction.StatusId = 1;
                                                    transaction.Remarks = "New Audit Steps Assigned.";
                                                    transaction.CreatedOn = DateTime.Now;
                                                    transaction.CreatedBy = userID;
                                                    transaction.AuditID = Convert.ToInt64(lblAuditID);

                                                    //if (!distinctprocess.Contains((int)EachProcess.Process))
                                                    //{
                                                    UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                    auditScheduleon.InternalAuditInstance = riskinstance.ID;
                                                    transaction.InternalAuditInstance = riskinstance.ID;
                                                    UserManagementRisk.AddDetailsInternalAuditScheduleOn(auditScheduleon);
                                                    if (tempTransactionList.Count != 0)
                                                    {
                                                        CreateTransaction(transaction);
                                                    }
                                                    distinctprocess.Add((int)EachProcess.Process);
                                                    //}
                                                    #endregion
                                                    //if (PerformerList.Count > 0)
                                                    //{
                                                    //    foreach (var item in PerformerList)
                                                    //    {
                                                    if (lblPerformerUserID != null && lblPerformerUserID != "-1" && ddlper.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                                                        TempAssP.InternalAuditInstance = riskinstance.ID;
                                                        TempAssP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssP.ProcessId = EachProcess.Process;
                                                        TempAssP.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssP.RoleID = 3;   //RoleManagement.GetByCode("PERF").ID;
                                                        TempAssP.UserID = Convert.ToInt32(ddlper.SelectedValue);
                                                        TempAssP.IsActive = true;
                                                        TempAssP.ISInternalExternal = AssignedTo;
                                                        TempAssP.CreatedOn = DateTime.Now;
                                                        TempAssP.CreatedBy = userID;
                                                        TempAssP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssP.AuditPeriodStartdate = null;
                                                        TempAssP.AuditPeriodEnddate = null;
                                                        tempAssignmentList.Add(TempAssP);
                                                    }
                                                    //    }
                                                    //}

                                                    if (lblReviewerUserID != null && lblReviewerUserID != "-1" && ddlrev.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.RoleID = 4;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ProcessId = EachProcess.Process;
                                                        TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        tempAssignmentList.Add(TempAssR);
                                                    }

                                                    if (lblReviewerUserID2 != null && lblReviewerUserID2 != "-1" && ddlrev2.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.RoleID = 5;// RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID2);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ProcessId = EachProcess.Process;
                                                        TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        tempAssignmentList.Add(TempAssR);
                                                    }
                                                });

                                                if (tempAssignmentList.Count != 0)
                                                    UserManagementRisk.AddDetailsInternalControlAuditAssignment(tempAssignmentList);

                                                //Save Total Active Ateps and Count 
                                                SaveFlag = UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, customerID, Convert.ToInt32(lblLocationId), Convert.ToInt32(lblVerticalId), lblItemTemplateFinancialYear, lblItemTemplateForPeriod, Convert.ToInt64(lblAuditID));
                                            }
                                        }
                                        #endregion

                                        #region Audit Implementation                                   
                                        if (lblPerformerIMPUserID != "-1" && lblPerformerIMPUserID != "" && ddlPerImp.Enabled)
                                        {
                                            string AssignedToIMP = "";
                                            if (ddlInternalExternal.SelectedItem.Text == "Internal")
                                                AssignedToIMP = "I";
                                            else
                                                AssignedToIMP = "E";

                                            if (ScheduledProcessList.Count > 0)
                                            {
                                                tempAssignmentList.Clear();
                                                tempTransactionList.Clear();

                                                ScheduledProcessList.ForEach(EachProcess =>
                                                {
                                                    AuditImplementationInstance riskinstanceIMP = new AuditImplementationInstance();
                                                    riskinstanceIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstanceIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstanceIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                    riskinstanceIMP.ProcessId = EachProcess.Process;
                                                    riskinstanceIMP.SubProcessId = -1;
                                                    riskinstanceIMP.IsDeleted = false;
                                                    riskinstanceIMP.CreatedOn = DateTime.Now;
                                                    riskinstanceIMP.CreatedBy = userID;
                                                    riskinstanceIMP.AuditID = Convert.ToInt32(lblAuditID);

                                                    //if (!distinctprocessImp.Contains((int)EachProcess.Process))
                                                    //{
                                                    UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstanceIMP);
                                                    CreateScheduleOnImplementation(riskinstanceIMP.Id, Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User,
                                                    lblItemTemplateFinancialYear.Trim(), Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblVerticalId), Convert.ToInt32(EachProcess.Process), Convert.ToInt32(lblAuditID), -1);
                                                    distinctprocessImp.Add(Convert.ToInt32(EachProcess.Process));
                                                    //}

                                                    List<AuditImplementationAssignment> Tempassignments = new List<AuditImplementationAssignment>();
                                                    //if (PerformerListImplem.Count > 0)
                                                    //{
                                                    //    foreach (var item in PerformerListImplem)
                                                    //    {
                                                    if (lblPerformerIMPUserID != null && lblPerformerIMPUserID != "-1" && ddlPerImp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssP = new AuditImplementationAssignment();
                                                        TempAssP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssP.ProcessId = EachProcess.Process;
                                                        TempAssP.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                                        TempAssP.UserID = Convert.ToInt32(ddlPerImp.SelectedValue);
                                                        TempAssP.IsActive = true;
                                                        TempAssP.CreatedOn = DateTime.Now;
                                                        TempAssP.CreatedBy = userID;
                                                        TempAssP.AuditID = Convert.ToInt32(lblAuditID);
                                                        Tempassignments.Add(TempAssP);
                                                    }
                                                    //    }
                                                    //}

                                                    if (lblReviewerIMPUserID != null && lblReviewerIMPUserID != "-1" && ddlReviewerIMP.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerIMPUserID);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssR.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssR.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = EachProcess.Process;
                                                        TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssR.AuditID = Convert.ToInt32(lblAuditID);
                                                        TempAssR.CreatedBy = userID;
                                                        Tempassignments.Add(TempAssR);
                                                    }
                                                    if (lblReviewerIMPUserID2 != null && lblReviewerIMPUserID2 != "-1" && ddlReviewerIMP2.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.RoleID = RoleManagement.GetByCode("RVW2").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerIMPUserID2);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssR.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssR.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = EachProcess.Process;
                                                        TempAssR.SubProcessId = EachProcess.SubProcessID;
                                                        TempAssR.AuditID = Convert.ToInt32(lblAuditID);
                                                        TempAssR.CreatedBy = userID;
                                                        Tempassignments.Add(TempAssR);
                                                    }

                                                    if (Tempassignments.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsAuditImplementationAssignment(Tempassignments);
                                                    }

                                                    SaveFlag = true;
                                                });
                                            }
                                        }
                                        #endregion
                                    }//Audit Step Count
                                }//Audit setep null
                                else
                                {
                                    listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                                }
                                #endregion
                            }
                        }
                        #endregion
                    } //Process Wise Audit Performer/Reviewer foreach end
                    #region Code added by Sushant for Process Name/SubProcess
                    var DistinctAuditID = (from row in entities.ProcessCommaSaperates
                                           select row.AuditID).Distinct().ToList();

                    var SubProcessAuditList = (from row in entities.SubProcessCommaSeparates
                                               select row.AuditID).Distinct().ToList();

                    var CheckAuditID = (from row in entities.AuditClosureDetails
                                        where !DistinctAuditID.Contains(row.ID)
                                        select row.ID).Distinct().ToList();

                    var CheckAuditIDSubProcess = (from row in entities.AuditClosureDetails
                                                  where !SubProcessAuditList.Contains(row.ID)
                                                  select row.ID).Distinct().ToList();

                    foreach (var item in CheckAuditID)
                    {
                        var ProcessList = (from row in entities.InternalControlAuditAssignments
                                           join row1 in entities.Mst_Process
                                           on row.ProcessId equals row1.Id
                                           join RATDM in entities.RiskActivityToBeDoneMappings
                                           on row.CustomerBranchID equals RATDM.CustomerBranchID
                                           where row.AuditID == item
                                           && row.ProcessId == RATDM.ProcessId
                                           && row.IsActive == true
                                           select row1.Name).Distinct().ToList();

                        var ProcessNameComaSaperate = string.Empty;
                        foreach (var item1 in ProcessList)
                        {
                            ProcessNameComaSaperate += item1 + ",";
                        }
                        ProcessNameComaSaperate = ProcessNameComaSaperate.TrimEnd(',');
                        ProcessNameComaSaperate = ProcessNameComaSaperate.TrimStart(',');

                        if (!string.IsNullOrEmpty(ProcessNameComaSaperate))
                        {
                            ProcessCommaSaperate objProcess = new ProcessCommaSaperate();
                            objProcess.AuditID = item;
                            objProcess.ProcessName = ProcessNameComaSaperate;
                            objProcess.CreatedOn = DateTime.Now;
                            entities.ProcessCommaSaperates.Add(objProcess);
                            entities.SaveChanges();
                        }
                    }

                    foreach (var item in CheckAuditIDSubProcess)
                    {
                        var SubProcessList = (from row in entities.InternalControlAuditAssignments
                                              join row1 in entities.mst_Subprocess
                                              on row.SubProcessId equals row1.Id
                                              join RATDM in entities.RiskActivityToBeDoneMappings
                                              on row.CustomerBranchID equals RATDM.CustomerBranchID
                                              where row.AuditID == item
                                              && RATDM.ProcessId == row.ProcessId
                                              && RATDM.SubProcessId == row.SubProcessId
                                              && row.IsActive == true
                                              select row1.Name).Distinct().ToList();

                        var SubProcessNameComaSeparate = string.Empty;
                        foreach (var item1 in SubProcessList)
                        {
                            SubProcessNameComaSeparate += item1 + ",";
                        }

                        SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimEnd(',');
                        SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimStart(',');
                        if (!string.IsNullOrEmpty(SubProcessNameComaSeparate))
                        {
                            SubProcessCommaSeparate objSubProcess = new SubProcessCommaSeparate();
                            objSubProcess.AuditID = item;
                            objSubProcess.SubProcessName = SubProcessNameComaSeparate;
                            objSubProcess.CreatedOn = DateTime.Now;
                            entities.SubProcessCommaSeparates.Add(objSubProcess);
                            entities.SaveChanges();
                        }
                    }
                    #endregion
                    ActiveAuditStepIDList.Clear();
                    ActiveAuditStepIDList = null;
                }//using end
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                cvDuplicateEntry.IsValid = false;
                if (SaveFlag)
                {
                    if (listNotSaveList.Length <= 0)
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit Kickoff Sucessfully.";
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit Kickoff Sucessfully. The following audits " +
                                                       "- " + listNotSaveList.Trim(',') + " not kickoff due to audit steps not uploaded or Reviewer & Reviewer2 cannot be same user";
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "No Audit Kickoff due to audit steps not uploaded for following " +
                        "- " + listNotSaveList.Trim(',') + " or Reviewer & Reviewer2 cannot be same user";
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateScheduleOn(long InternalAuditInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, long Processid, int VerticalID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<InternalAuditTransaction> transactionlist = new List<InternalAuditTransaction>();

                    InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                    auditScheduleon.InternalAuditInstance = InternalAuditInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = Processid;

                    entities.InternalAuditScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        InternalAuditInstance = InternalAuditInstance,
                        AuditScheduleOnID = auditScheduleon.ID,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ProcessId = Processid,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalID,
                        Remarks = "New Audit Steps Assigned."
                    };

                    transactionlist.Add(transaction);

                    // CreateTransaction(transactionlist);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateTransaction(InternalAuditTransaction transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //transaction.ForEach(entry =>
                //{
                transaction.Dated = DateTime.Now;
                entities.InternalAuditTransactions.Add(transaction);
                //});
                entities.SaveChanges();
            }
        }
        public static void CreateScheduleOnImplementation(long ImplementationInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, int VerticalId, int ProcessId, long AuditID, int SubProcessID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<AuditImplementationTransaction> transactionlist = new List<AuditImplementationTransaction>();

                    AuditImpementationScheduleOn auditScheduleon = new AuditImpementationScheduleOn();
                    auditScheduleon.ImplementationInstance = ImplementationInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = ProcessId;
                    auditScheduleon.AuditID = AuditID;
                    entities.AuditImpementationScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        ImplementationInstance = ImplementationInstance,
                        ImplementationScheduleOnID = auditScheduleon.Id,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ForPeriod = Period,
                        FinancialYear = FinancialYear,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalId,
                        ProcessId = ProcessId,
                        SubProcessId = SubProcessID,
                        AuditID = AuditID,
                        Remarks = "New Implementation Steps Assigned."
                    };

                    transactionlist.Add(transaction);

                    CreateTransactionImplementation(transactionlist);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateTransactionImplementation(List<AuditImplementationTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(entry);
                });
                entities.SaveChanges();
            }
        }
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPerformerFilter();
            BindReviewerFilter();
            BindReviewerFilter2();
            BindProcessAudits();
        }
        protected void ddlPerformerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlPerformer");
                    DropDownList ddlPerformerIMP = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlPerformerIMP");

                    if (ddlPerformer.Enabled)
                    {
                        ddlPerformer.ClearSelection();
                        ddlPerformer.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                    if (ddlPerformerIMP.Enabled)
                    {
                        ddlPerformerIMP.ClearSelection();
                        ddlPerformerIMP.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlReviewerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer");
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewerIMP");
                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlPerformerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlPerformerAuditHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlPerformerAuditHeader");
                if (ddlPerformerAuditHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcessAudit.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerAuditHeader.Items.Count; i++)
                        {
                            DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[j].FindControl("ddlPerformer");
                            if (ddlPerformer.Items.Count > 0)
                            {
                                if (ddlPerformerAuditHeader.Items[i].Selected)
                                {
                                    string val = ddlPerformerAuditHeader.Items[i].Value;
                                    ddlPerformer.Items.FindByValue(val).Selected = true;
                                }
                                else
                                {
                                    string val = ddlPerformerAuditHeader.Items[i].Value;
                                    ddlPerformer.Items.FindByValue(val).Selected = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlReviewerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerAuditHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlReviewerAuditHeader");
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer");


                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerAuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlReviewer2AuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2AuditHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlReviewer2AuditHeader");
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer2");


                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewer2AuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlReviewer2ImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2ImplemetationHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlReviewer2ImplemetationHeader");
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewerIMP2");


                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewer2ImplemetationHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlReviewerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerImplemetationHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlReviewerImplemetationHeader");
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewerIMP");


                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerImplemetationHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPerformerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlPerformerImplemetationHeader = (DropDownList)grdProcessAudit.HeaderRow.FindControl("ddlPerformerImplemetationHeader");
                if (ddlPerformerImplemetationHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcessAudit.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerImplemetationHeader.Items.Count; i++)
                        {
                            DropDownList ddlPerformerIMP = (DropDownList)grdProcessAudit.Rows[j].FindControl("ddlPerformerIMP");
                            if (ddlPerformerIMP.Items.Count > 0)
                            {
                                if (ddlPerformerImplemetationHeader.Items[i].Selected)
                                {
                                    string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                    ddlPerformerIMP.Items.FindByValue(val).Selected = true;
                                }
                                else
                                {
                                    string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                    ddlPerformerIMP.Items.FindByValue(val).Selected = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
                //  upAuditKickoffList.Update();
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindVerticalID(Convert.ToInt32(ddlSubEntity4.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView1", "$(\"#divLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindProcessAudits()
        {
            try
            {
                int VerticalID = -1;
                int auditorid = -1;
                string flag = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;

                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    flag = "I";
                else
                    flag = "E";

                if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    if (ddlAuditor.SelectedValue != "-1")
                    {
                        auditorid = Convert.ToInt32(ddlAuditor.SelectedValue);
                    }
                }

                if (FinancialYear == "")
                    FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);


                //List<long> CommanBranchlist = new List<long>();
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.SP_AuditKickOffFillView(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                                 where row.CustomerID == customerID
                                 select row).ToList();

                    if (Branchlist.Count > 0)
                        query = query.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();

                    if (FinancialYear != "")
                        query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                    if (VerticalID != -1)
                        query = query.Where(entry => entry.Verticalid == VerticalID).ToList();

                    if (query.Count > 0)
                        query = query.OrderBy(entry => entry.CustomerBranchName).ThenBy(entry => entry.VerticalName).ToList();

                    grdProcessAudit.DataSource = null;
                    grdProcessAudit.DataBind();

                    if (query != null)
                    {
                        grdProcessAudit.DataSource = query;
                        Session["TotalRows"] = null;
                        Session["TotalRows"] = query.Count;
                        grdProcessAudit.DataBind();
                        Branchlistloop.Clear();
                        Branchlist.Clear();
                        if (query.Count > 0)
                            btnSave.Visible = true;
                        else
                            btnSave.Visible = false;
                    }

                    if (HttpContext.Current.Cache.Get("ProcessAuditAssignmentData") != null)
                        HttpContext.Current.Cache.Remove("ProcessAuditAssignmentData");

                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            BindProcessAudits();
        }
        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {

        }
        protected void grdProcessAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (e.CommandName.Equals("SHOW_SCHEDULE"))
                    {
                        String URLStr = "";
                        string url = "";
                        String Args = e.CommandArgument.ToString();

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                        int verticalid = Convert.ToInt32(commandArgs[1]);
                        string FinancialYear = Convert.ToString(commandArgs[2]);
                        string ForMonth = Convert.ToString(commandArgs[3]);
                        string ISAHQMP = Convert.ToString(commandArgs[4]);
                        int Auditid = Convert.ToInt32(commandArgs[5]);


                        var BranchAuditAssignDetails = (from row in entities.Internal_AuditAssignment
                                                        where row.IsActive == false
                                                        && row.CustomerBranchid == CustomerBranchID
                                                        && row.VerticalID == verticalid
                                                        select row).FirstOrDefault();
                        if (BranchAuditAssignDetails != null)
                        {
                            int auditorid = -1;
                            string isInternalOrexternal = "I";
                            if (BranchAuditAssignDetails.AssignedTo == "I")
                            {
                                isInternalOrexternal = "I";
                            }
                            else if (BranchAuditAssignDetails.AssignedTo == "E")
                            {
                                isInternalOrexternal = "E";
                            }
                            if (BranchAuditAssignDetails.ExternalAuditorId != -1)
                            {
                                auditorid = Convert.ToInt32(BranchAuditAssignDetails.ExternalAuditorId);
                            }
                            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                            {
                                url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                            }

                            string url2 = HttpContext.Current.Request.Url.AbsolutePath;

                            URLStr = "~/RiskManagement/InternalAuditTool/AuditKickOff_New.aspx?BID=" + CustomerBranchID + "&VID=" + verticalid + "&FY=" + FinancialYear + "&Period=" + ForMonth + "&AUID=" + Auditid + "&Type=P&ISIE=" + isInternalOrexternal + "&AUDTOR=" + auditorid;

                            if (Args != "")
                                Response.Redirect(URLStr, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdProcessAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    var obj = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                    DropDownList ddlPerformerAuditHeader = (e.Row.FindControl("ddlPerformerAuditHeader") as DropDownList);

                    ddlPerformerAuditHeader.DataTextField = "Name";
                    ddlPerformerAuditHeader.DataValueField = "ID";
                    ddlPerformerAuditHeader.DataSource = obj;
                    ddlPerformerAuditHeader.DataBind();
                    ddlPerformerAuditHeader.Items.Insert(0, new ListItem("Performer", "-1"));

                    DropDownList ddlReviewerAuditHeader = (e.Row.FindControl("ddlReviewerAuditHeader") as DropDownList);

                    ddlReviewerAuditHeader.DataTextField = "Name";
                    ddlReviewerAuditHeader.DataValueField = "ID";
                    ddlReviewerAuditHeader.DataSource = obj;
                    ddlReviewerAuditHeader.DataBind();
                    ddlReviewerAuditHeader.Items.Insert(0, new ListItem("Reviewer 1", "-1"));

                    DropDownList ddlReviewer2AuditHeader = (e.Row.FindControl("ddlReviewer2AuditHeader") as DropDownList);

                    ddlReviewer2AuditHeader.DataTextField = "Name";
                    ddlReviewer2AuditHeader.DataValueField = "ID";
                    ddlReviewer2AuditHeader.DataSource = obj;
                    ddlReviewer2AuditHeader.DataBind();
                    ddlReviewer2AuditHeader.Items.Insert(0, new ListItem("Reviewer 2", "-1"));

                    DropDownList ddlPerformerImplemetationHeader = (e.Row.FindControl("ddlPerformerImplemetationHeader") as DropDownList);

                    ddlPerformerImplemetationHeader.DataTextField = "Name";
                    ddlPerformerImplemetationHeader.DataValueField = "ID";
                    ddlPerformerImplemetationHeader.DataSource = obj;
                    ddlPerformerImplemetationHeader.DataBind();
                    ddlPerformerImplemetationHeader.Items.Insert(0, new ListItem("Performer", "-1"));

                    DropDownList ddlReviewerImplemetationHeader = (e.Row.FindControl("ddlReviewerImplemetationHeader") as DropDownList);

                    ddlReviewerImplemetationHeader.DataTextField = "Name";
                    ddlReviewerImplemetationHeader.DataValueField = "ID";
                    ddlReviewerImplemetationHeader.DataSource = obj;
                    ddlReviewerImplemetationHeader.DataBind();
                    ddlReviewerImplemetationHeader.Items.Insert(0, new ListItem("Reviewer 1", "-1"));

                    DropDownList ddlReviewer2ImplemetationHeader = (e.Row.FindControl("ddlReviewer2ImplemetationHeader") as DropDownList);

                    ddlReviewer2ImplemetationHeader.DataTextField = "Name";
                    ddlReviewer2ImplemetationHeader.DataValueField = "ID";
                    ddlReviewer2ImplemetationHeader.DataSource = obj;
                    ddlReviewer2ImplemetationHeader.DataBind();
                    ddlReviewer2ImplemetationHeader.Items.Insert(0, new ListItem("Reviewer 2", "-1"));

                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblLocationId = (e.Row.FindControl("lblLocationId") as Label);
                    Label lblFinancialYear = (e.Row.FindControl("lblFinancialYear") as Label);
                    Label lblTermName = (e.Row.FindControl("lblTermName") as Label);
                    Label lblVerticalId = (e.Row.FindControl("lblVerticalId") as Label);
                    Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);

                    TextBox lblPerformerName = (e.Row.FindControl("lblPerformerName") as TextBox);
                    TextBox lblReviewerName = (e.Row.FindControl("lblReviewerName") as TextBox);
                    TextBox lblReviewerName2 = (e.Row.FindControl("lblReviewerName2") as TextBox);

                    TextBox lblPerformerNameIMP = (e.Row.FindControl("lblPerformerNameIMP") as TextBox);
                    TextBox lblReviewerNameIMP = (e.Row.FindControl("lblReviewerNameIMP") as TextBox);
                    TextBox lblReviewerNameIMP2 = (e.Row.FindControl("lblReviewerNameIMP2") as TextBox);

                    if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    {
                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                        DropDownList ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                        #region Audit
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("Performer", "-1"));

                            if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                PerformerReviewerDropDownEnableDisable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerName, Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Reviewer", "-1"));

                            if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                PerformerReviewerDropDownEnableDisable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerName, Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewer2 != null)
                        {
                            ddlReviewer2.Items.Clear();
                            ddlReviewer2.DataTextField = "Name";
                            ddlReviewer2.DataValueField = "ID";
                            // ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewer2.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewer2.DataBind();
                            ddlReviewer2.Items.Insert(0, new ListItem("Reviewer2", "-1"));

                            if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                PerformerReviewerDropDownEnableDisable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), lblReviewerName2, Convert.ToInt32(lblAuditID.Text));
                            }
                        }
                        #endregion

                        #region Implementation
                        if (ddlPerformerIMP != null)
                        {
                            ddlPerformerIMP.Items.Clear();
                            ddlPerformerIMP.DataTextField = "Name";
                            ddlPerformerIMP.DataValueField = "ID";
                            ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformerIMP.DataBind();
                            ddlPerformerIMP.Items.Insert(0, new ListItem("Performer", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                ImplementationPerformerReviewerDropDownEnableDisable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerNameIMP, Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewerIMP != null)
                        {
                            ddlReviewerIMP.Items.Clear();
                            ddlReviewerIMP.DataTextField = "Name";
                            ddlReviewerIMP.DataValueField = "ID";
                            ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewerIMP.DataBind();
                            ddlReviewerIMP.Items.Insert(0, new ListItem("Reviewer", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerNameIMP, Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewerIMP2 != null)
                        {
                            ddlReviewerIMP2.Items.Clear();
                            ddlReviewerIMP2.DataTextField = "Name";
                            ddlReviewerIMP2.DataValueField = "ID";
                            // ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewerIMP2.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewerIMP2.DataBind();
                            ddlReviewerIMP2.Items.Insert(0, new ListItem("Reviewer2", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                            {
                                ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), lblReviewerNameIMP2, Convert.ToInt32(lblAuditID.Text));
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (Convert.ToInt32(ddlAuditor.SelectedValue) != -1 || ddlAuditor.SelectedValue != "")
                        {
                            DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                            DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                            DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                            DropDownList ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownList);
                            DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                            DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                            #region Audit
                            if (ddlPerformer != null)
                            {
                                ddlPerformer.Items.Clear();
                                ddlPerformer.DataTextField = "Name";
                                ddlPerformer.DataValueField = "ID";
                                ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformer.DataBind();
                                // ddlPerformer.Items.Insert(0, new ListItem("Performer", "-1"));

                                if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    PerformerReviewerDropDownEnableDisable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerName, Convert.ToInt32(lblAuditID.Text));
                                }
                            }

                            if (ddlReviewer != null)
                            {
                                ddlReviewer.Items.Clear();
                                ddlReviewer.DataTextField = "Name";
                                ddlReviewer.DataValueField = "ID";
                                ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewer.DataBind();
                                ddlReviewer.Items.Insert(0, new ListItem("Reviewer", "-1"));

                                if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    PerformerReviewerDropDownEnableDisable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerName, Convert.ToInt32(lblAuditID.Text));
                                }
                            }

                            if (ddlReviewer2 != null)
                            {
                                ddlReviewer2.Items.Clear();
                                ddlReviewer2.DataTextField = "Name";
                                ddlReviewer2.DataValueField = "ID";
                                // ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewer2.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewer2.DataBind();
                                ddlReviewer2.Items.Insert(0, new ListItem("Reviewer2", "-1"));

                                if (lblLocationId != null && lblVerticalId != null && lblFinancialYear != null && lblTermName != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    PerformerReviewerDropDownEnableDisable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), lblReviewerName2, Convert.ToInt32(lblAuditID.Text));
                                }
                            }
                            #endregion

                            #region Implementation
                            if (ddlPerformerIMP != null)
                            {
                                ddlPerformerIMP.Items.Clear();
                                ddlPerformerIMP.DataTextField = "Name";
                                ddlPerformerIMP.DataValueField = "ID";
                                ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformerIMP.DataBind();
                                ddlPerformerIMP.Items.Insert(0, new ListItem("Performer", "-1"));

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    ImplementationPerformerReviewerDropDownEnableDisable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerNameIMP, Convert.ToInt32(lblAuditID.Text));
                                }
                            }

                            if (ddlReviewerIMP != null)
                            {
                                ddlReviewerIMP.Items.Clear();
                                ddlReviewerIMP.DataTextField = "Name";
                                ddlReviewerIMP.DataValueField = "ID";
                                ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewerIMP.DataBind();
                                ddlReviewerIMP.Items.Insert(0, new ListItem("Reviewer", "-1"));

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerNameIMP, Convert.ToInt32(lblAuditID.Text));
                                }
                            }

                            if (ddlReviewerIMP2 != null)
                            {
                                ddlReviewerIMP2.Items.Clear();
                                ddlReviewerIMP2.DataTextField = "Name";
                                ddlReviewerIMP2.DataValueField = "ID";
                                ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewerIMP2.DataBind();
                                ddlReviewerIMP2.Items.Insert(0, new ListItem("Reviewer2", "-1"));

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && !string.IsNullOrEmpty(lblAuditID.Text))
                                {
                                    ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), lblReviewerNameIMP2, Convert.ToInt32(lblAuditID.Text));
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdImplementationAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    String URLStr = "";
                    string url = "";
                    String Args = e.CommandArgument.ToString();

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    int verticalid = Convert.ToInt32(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    string ForMonth = Convert.ToString(commandArgs[3]);
                    int Auditid = Convert.ToInt32(commandArgs[4]);

                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                    }

                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;

                    URLStr = "~/RiskManagement/InternalAuditTool/AuditKickOff_New.aspx?BID=" + CustomerBranchID + "&VID=" + verticalid + "&FY=" + FinancialYear + "&Period=" + ForMonth + "&AUID=" + Auditid + "&Type=I";

                    if (Args != "")
                        Response.Redirect(URLStr, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdImplementationAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblLocationId = (e.Row.FindControl("lblLocationId") as Label);
                    Label lblFinancialYear = (e.Row.FindControl("lblItemTemplateFinancialYear") as Label);
                    Label lblTermName = (e.Row.FindControl("lblItemTemplateForPeriod") as Label);
                    Label lblVerticalId = (e.Row.FindControl("lblVerticalId") as Label);
                    Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);

                    TextBox lblPerformerName = (e.Row.FindControl("lblPerformerName") as TextBox);
                    TextBox lblReviewerName = (e.Row.FindControl("lblReviewerName") as TextBox);

                    if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    {
                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);

                        if (ddlPerformer != null)
                        {

                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("Select Performer", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && lblAuditID != null)
                            {
                                ImplementationPerformerReviewerDropDownEnableDisable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerName, Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewer != null)
                        {

                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && lblAuditID != null)
                            {
                                ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerName, Convert.ToInt32(lblAuditID.Text));
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(ddlAuditor.SelectedValue) != -1)
                        {
                            DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                            DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                            if (ddlPerformer != null)
                            {
                                ddlPerformer.Items.Clear();
                                ddlPerformer.DataTextField = "Name";
                                ddlPerformer.DataValueField = "ID";
                                ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformer.DataBind();
                                ddlPerformer.Items.Insert(0, new ListItem("Select Performer", "-1"));
                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && lblAuditID != null)
                                {
                                    ImplementationPerformerReviewerDropDownEnableDisable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), lblPerformerName, Convert.ToInt32(lblAuditID.Text));
                                }
                            }

                            if (ddlReviewer != null)
                            {
                                ddlReviewer.Items.Clear();
                                ddlReviewer.DataTextField = "Name";
                                ddlReviewer.DataValueField = "ID";
                                ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewer.DataBind();
                                ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null && lblVerticalId != null && lblAuditID != null)
                                {
                                    ImplementationPerformerReviewerDropDownEnableDisable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), lblReviewerName, Convert.ToInt32(lblAuditID.Text));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void PerformerReviewerDropDownEnableDisable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int VerticalId, TextBox lbl, int Auditid)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(Auditid)
                           where row.CustomerID == CustID
                           select row).ToList();

                //    HttpContext.Current.Cache.Insert("ProcessAuditAssignmentData", Records, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero); // add it to cache
                //}
                //else
                //    Records = (List<Sp_InternalAuditAssignedInstancesView_Result>)HttpContext.Current.Cache["ProcessAuditAssignmentData"];

            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.VerticalID == VerticalId
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.RoleID == RoleID
                            && row.AuditID == Auditid
                            select row.UserID).Distinct().ToList();

                if (Data.Count == 1)
                {
                    if (Data.ElementAt(0) != 0)
                    {
                        ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
                        ddl.Enabled = false;
                        lbl.Visible = false;
                        //aspNetDisabled
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15 Check";
                    }
                }
                else if (Data.Count > 1)
                {
                    ddl.ClearSelection();
                    ddl.Enabled = false;
                    ddl.Visible = false;
                    ddl.Attributes.Add("class", "");
                    ddl.CssClass = "form-control m-bot15 Check";

                    if (lbl != null)
                    {
                        lbl.Text = string.Empty;
                        Data.ForEach(EachUserID =>
                        {
                            if (EachUserID != 0)
                            {
                                var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
                                lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
                            }
                        });

                        lbl.Text = lbl.Text.Trim(',');
                        lbl.ToolTip = lbl.Text.Trim(',');
                        lbl.Visible = true;
                        lbl.Enabled = false;
                    }
                }
                else
                {
                    ddl.Enabled = true;
                    lbl.Visible = false;
                    lbl.Enabled = false;
                }
            }
        }
        //public void PerformerReviewerDropDownEnableDisable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int VerticalId, TextBox lbl, int Auditid)
        //{
        //    List<InternalAuditAssignedInstancesView> Records = new List<InternalAuditAssignedInstancesView>();

        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        if (HttpContext.Current.Cache["ProcessAuditAssignmentData"] == null)
        //        {
        //            Records = (from row in entities.InternalAuditAssignedInstancesViews.AsNoTracking()
        //                       where row.CustomerID == CustID
        //                       select row).ToList();

        //            HttpContext.Current.Cache.Insert("ProcessAuditAssignmentData", Records, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero); // add it to cache
        //        }
        //        else
        //            Records = (List<InternalAuditAssignedInstancesView>)HttpContext.Current.Cache["ProcessAuditAssignmentData"];

        //    }

        //    if (Records.Count > 0)
        //    {
        //        var Data = (from row in Records
        //                    where row.CustomerBranchID == CustBranchID
        //                    && row.VerticalID == VerticalId
        //                    && row.FinancialYear == FinYear
        //                    && row.ForMonth == Period
        //                    && row.RoleID == RoleID
        //                    && row.AuditID == Auditid
        //                    select row.UserID).Distinct().ToList();

        //        if (Data.Count == 1)
        //        {
        //            if (Data.ElementAt(0) != 0)
        //            {
        //                ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
        //                ddl.Enabled = false;
        //                lbl.Visible = false;
        //                //aspNetDisabled
        //                ddl.Attributes.Add("class", "");
        //                ddl.CssClass = "form-control m-bot15";
        //            }
        //        }
        //        else if (Data.Count > 1)
        //        {
        //            ddl.ClearSelection();
        //            ddl.Enabled = false;
        //            ddl.Visible = false;
        //            ddl.Attributes.Add("class", "");
        //            ddl.CssClass = "form-control m-bot15";

        //            if (lbl != null)
        //            {
        //                lbl.Text = string.Empty;
        //                Data.ForEach(EachUserID =>
        //                {
        //                    if (EachUserID != 0)
        //                    {
        //                        var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
        //                        lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
        //                    }
        //                });

        //                lbl.Text = lbl.Text.Trim(',');
        //                lbl.ToolTip = lbl.Text.Trim(',');
        //                lbl.Visible = true;
        //                lbl.Enabled = false;
        //            }
        //        }
        //        else
        //        {
        //            ddl.Enabled = true;
        //            lbl.Visible = false;
        //            lbl.Enabled = false;
        //        }
        //    }
        //}

        public void ImplementationPerformerReviewerDropDownEnableDisable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, TextBox lbl, int Auditid)
        {
            List<Sp_ImplementationAuditAssignedInstancesView_Result> Records = new List<Sp_ImplementationAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_ImplementationAuditAssignedInstancesView(Auditid)
                               // where row.CustomerID == CustID
                           select row).ToList();
            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.VerticalID == Verticalid
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.RoleID == RoleID
                            && row.AuditID == Auditid
                            select row.UserID).Distinct().ToList();

                if (Data.Count == 1)
                {
                    if (Data.ElementAt(0) != 0)
                    {
                        ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
                        ddl.Enabled = false;
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15 Check";
                    }
                }
                else if (Data.Count > 1)
                {
                    ddl.ClearSelection();
                    ddl.Enabled = false;
                    ddl.Visible = false;
                    ddl.Attributes.Add("class", "");
                    ddl.CssClass = "form-control m-bot15 Check";

                    if (lbl != null)
                    {
                        lbl.Text = string.Empty;
                        Data.ForEach(EachUserID =>
                        {
                            if (EachUserID != 0)
                            {
                                var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
                                lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
                            }
                        });
                        lbl.Text = lbl.Text.Trim(',');
                        lbl.Visible = true;
                        lbl.Enabled = false;
                    }
                }
                else
                {
                    ddl.Enabled = true;
                    lbl.Visible = false;
                    lbl.Enabled = false;
                }
            }
        }
        //public void ImplementationPerformerReviewerDropDownEnableDisable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, TextBox lbl, int Auditid)
        //{
        //    List<ImplementationAuditAssignedInstancesView> Records = new List<ImplementationAuditAssignedInstancesView>();

        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        Records = (from row in entities.ImplementationAuditAssignedInstancesViews
        //                   where row.CustomerID == CustID
        //                   select row).ToList();
        //    }

        //    if (Records.Count > 0)
        //    {
        //        var Data = (from row in Records
        //                    where row.CustomerBranchID == CustBranchID
        //                    && row.VerticalID == Verticalid
        //                    && row.FinancialYear == FinYear
        //                    && row.ForMonth == Period
        //                    && row.RoleID == RoleID
        //                    && row.AuditID == Auditid
        //                    select row.UserID).Distinct().ToList();

        //        if (Data.Count == 1)
        //        {
        //            if (Data.ElementAt(0) != 0)
        //            {
        //                ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
        //                ddl.Enabled = false;
        //                ddl.Attributes.Add("class", "");
        //                ddl.CssClass = "form-control m-bot15";
        //            }
        //        }
        //        else if (Data.Count > 1)
        //        {
        //            ddl.ClearSelection();
        //            ddl.Enabled = false;
        //            ddl.Visible = false;
        //            ddl.Attributes.Add("class", "");
        //            ddl.CssClass = "form-control m-bot15";

        //            if (lbl != null)
        //            {
        //                lbl.Text = string.Empty;
        //                Data.ForEach(EachUserID =>
        //                {
        //                    if (EachUserID != 0)
        //                    {
        //                        var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
        //                        lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
        //                    }
        //                });
        //                lbl.Text = lbl.Text.Trim(',');
        //                lbl.Visible = true;
        //                lbl.Enabled = false;
        //            }
        //        }
        //        else
        //        {
        //            ddl.Enabled = true;
        //            lbl.Visible = false;
        //            lbl.Enabled = false;
        //        }
        //    }
        //}
        public bool CheckProcessAsignedOrNotExists(string FinancialYear, string ForPeriod, long CustomerBranchId, int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             join row1 in entities.InternalAuditScheduleOns
                             on row.ID equals row1.InternalAuditInstance
                             where row.ProcessId == row1.ProcessId && row1.FinancialYear == FinancialYear
                             && row1.ForMonth == ForPeriod && row.CustomerBranchID == CustomerBranchId && row.VerticalID == Verticalid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        protected void liProcess_Click(object sender, EventArgs e)
        {
            try
            {
                dvProcessDetails.Visible = true;
                dvProcessDetails.Attributes.Remove("class");
                dvProcessDetails.Attributes.Add("class", "tab-pane active");
                BindProcessAudits();

                int chknumber = Convert.ToInt32(grdProcessAudit.PageIndex);
                bindPageNumber();
                if (chknumber > 0)
                {
                    chknumber = chknumber + 1;
                    DropDownListPageNo.SelectedValue = (chknumber).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Re-Assign Performer/Reviewer

        protected void grdReassign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlGridReAssignUser = (e.Row.FindControl("ddlGridReAssignUser") as DropDownList);

                    if (ddlGridReAssignUser != null)
                    {
                        BindUserList(customerID, ddlGridReAssignUser);

                        if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                            ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtEventReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess();
            BindReAssingGrid();
        }

        public void BindLegalEntityDataPopUp()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            ddlLegalEntityPopup.DataTextField = "Name";
            ddlLegalEntityPopup.DataValueField = "ID";
            ddlLegalEntityPopup.Items.Clear();
            ddlLegalEntityPopup.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntityPopup.DataBind();
            ddlLegalEntityPopup.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityDataPopUp(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        private void BindUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddllist.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddllist.DataBind();

                ddllist.Items.Insert(0, new ListItem("Select User", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<AssignedUserwiseAuditList> GetProcessAuditData(string Flag)
        {
            int roleid = -1;
            int Statusid = -1;
            int branchid = -1;
            int Verticalid = -1;
            int UserID = -1;

            string FinYear = String.Empty;
            string Period = String.Empty;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            if (!String.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntityPopup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
            {
                if (ddlLocationPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLocationPopup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlAuditStatus.SelectedValue))
            {
                if (ddlAuditStatus.SelectedValue != "-1")
                {
                    Statusid = Convert.ToInt32(ddlAuditStatus.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFinYearPopup.SelectedValue))
            {
                if (ddlFinYearPopup.SelectedValue != "-1")
                {
                    FinYear = ddlFinYearPopup.SelectedItem.Text;
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    Verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalPopup.SelectedValue))
                {
                    if (ddlVerticalPopup.SelectedValue != "-1")
                    {
                        Verticalid = Convert.ToInt32(ddlVerticalPopup.SelectedValue);
                    }
                }
            }
            if (!String.IsNullOrEmpty(ddlRole.SelectedValue))
            {
                if (ddlRole.SelectedValue != "-1")
                {
                    roleid = Convert.ToInt32(ddlRole.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlUser.SelectedValue))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    UserID = Convert.ToInt32(ddlUser.SelectedValue);
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(customerID, branchid);

            List<AssignedUserwiseAuditList> AuditLists = new List<AssignedUserwiseAuditList>();

            if (Flag == "P")
            {
                AuditLists = DashboardManagementRisk.GetAuditsAssignedUser(FinYear, Period, customerID, Branchlist, UserID, Statusid, Verticalid, roleid);
            }
            else
            {
                AuditLists = DashboardManagementRisk.GetAuditsAssignedUser(FinYear, Period, customerID, Branchlist, UserID, Statusid, Verticalid, roleid);
            }

            return AuditLists;
        }

        public bool SaveReAssignProcessAuditDetails()
        {
            try
            {
                bool success = false;

                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");

                    if (ddlGridReAssignUser != null && ddlGridReAssignUser.SelectedValue != "-1" && ddlGridReAssignUser.SelectedValue != "")
                    {
                        string lblBranchID = (grdReassign.Rows[i].FindControl("lblBranchID") as Label).Text;
                        string lblVerticalId = (grdReassign.Rows[i].FindControl("lblVerticalId") as Label).Text;
                        string lblFY = (grdReassign.Rows[i].FindControl("lblFY") as Label).Text;
                        string lblPeriod = (grdReassign.Rows[i].FindControl("lblPeriod") as Label).Text;
                        string AuditID = (grdReassign.Rows[i].FindControl("lblAuditID") as Label).Text;
                        string lblProcessId = (grdReassign.Rows[i].FindControl("lblProcessId") as Label).Text;
                        string lblSubProcessId = (grdReassign.Rows[i].FindControl("lblSubProcessId") as Label).Text;
                        string lblATBDID = (grdReassign.Rows[i].FindControl("lblATBDID") as Label).Text;

                        if (lblBranchID != "" && lblVerticalId != "" && lblFY != "" && lblPeriod != "" && lblProcessId != "" && lblSubProcessId != "" && lblATBDID != "")
                        {
                            //success = UserManagementRisk.ReAssignedAudit(Convert.ToInt32(ddlUser.SelectedValue), Convert.ToInt32(ddlGridReAssignUser.SelectedValue),
                            //    Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblBranchID), Convert.ToInt32(lblVerticalId), lblFY, lblPeriod);
                            success = UserManagementRisk.ReAssignedAudit(Convert.ToInt32(ddlUser.SelectedValue), Convert.ToInt32(ddlGridReAssignUser.SelectedValue), Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt64(AuditID), Convert.ToInt32(lblProcessId), Convert.ToInt32(lblSubProcessId), Convert.ToInt32(lblBranchID), Convert.ToInt32(lblVerticalId));
                        }
                    }
                }

                return success;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private List<IMPOpenCloseAuditDetailsClass> GetImplementationAuditData(string Flag)
        {
            int roleid = -1;
            int Statusid = -1;
            int branchid = -1;
            int Verticalid = -1;
            int UserID = -1;

            string FinYear = String.Empty;
            string Period = String.Empty;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            if (!String.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntityPopup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
            {
                if (ddlLocationPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLocationPopup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlAuditStatus.SelectedValue))
            {
                if (ddlAuditStatus.SelectedValue != "-1")
                {
                    Statusid = Convert.ToInt32(ddlAuditStatus.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFinYearPopup.SelectedValue))
            {
                if (ddlFinYearPopup.SelectedValue != "-1")
                {
                    FinYear = ddlFinYearPopup.SelectedItem.Text;
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    Verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalPopup.SelectedValue))
                {
                    if (ddlVerticalPopup.SelectedValue != "-1")
                    {
                        Verticalid = Convert.ToInt32(ddlVerticalPopup.SelectedValue);
                    }
                }
            }
            if (!String.IsNullOrEmpty(ddlRole.SelectedValue))
            {
                if (ddlRole.SelectedValue != "-1")
                {
                    roleid = Convert.ToInt32(ddlRole.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlUser.SelectedValue))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    UserID = Convert.ToInt32(ddlUser.SelectedValue);
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(customerID, branchid);

            List<IMPOpenCloseAuditDetailsClass> AuditLists = new List<IMPOpenCloseAuditDetailsClass>();

            if (Flag == "P")
            {
                AuditLists = DashboardManagementRisk.IMPGetAudits(FinYear, Period, customerID, Branchlist, UserID, Statusid, Verticalid, roleid);
            }
            else
            {
                AuditLists = DashboardManagementRisk.IMPGetAudits(FinYear, Period, customerID, Branchlist, UserID, Statusid, Verticalid, roleid);
            }

            return AuditLists;
        }

        public bool SaveReAssignImplementationAuditDetails()
        {
            try
            {
                bool success = false;
                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");

                    if (ddlGridReAssignUser != null && ddlGridReAssignUser.SelectedValue != "-1" && ddlGridReAssignUser.SelectedValue != "")
                    {
                        string lblBranchID = (grdReassign.Rows[i].FindControl("lblBranchID") as Label).Text;
                        string lblVerticalId = (grdReassign.Rows[i].FindControl("lblVerticalId") as Label).Text;
                        string lblFY = (grdReassign.Rows[i].FindControl("lblFY") as Label).Text;
                        string lblPeriod = (grdReassign.Rows[i].FindControl("lblPeriod") as Label).Text;
                        string AuditID = (grdReassign.Rows[i].FindControl("lblAuditID") as Label).Text;

                        if (lblBranchID != "" && lblVerticalId != "" && lblFY != "" && lblPeriod != "")
                        {
                            success = UserManagementRisk.ReAssignedIMPAudit(Convert.ToInt32(ddlUser.SelectedValue), Convert.ToInt32(ddlGridReAssignUser.SelectedValue),
                                Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblBranchID), Convert.ToInt32(lblVerticalId), lblFY, lblPeriod, Convert.ToInt64(AuditID), CustomerID);
                        }
                    }
                }

                return success;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void BindReAssingGrid()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                int processid = -1;
                string processName = string.Empty;

                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);

                if (action.Equals("Audit"))
                {
                    if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }

                    Session["TotalRowsReAssging"] = null;
                    var AuditList = GetProcessAuditData("P");
                    grdReassign.DataSource = AuditList;
                    Session["TotalRowsReAssging"] = AuditList.Count;
                    grdReassign.DataBind();

                    if (AuditList.Count > 0)
                        divSavePageSummary.Attributes.Add("style", "display:block;");
                    else
                        divSavePageSummary.Attributes.Add("style", "display:none;");
                }
                if (action.Equals("Implementation"))
                {
                    if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }

                    Session["TotalRowsReAssging"] = null;
                    var AuditList = GetImplementationAuditData("P");

                    grdReassign.DataSource = AuditList;
                    Session["TotalRowsReAssging"] = AuditList.Count;
                    grdReassign.DataBind();

                    if (AuditList.Count > 0)
                        divSavePageSummary.Attributes.Add("style", "display:block;");
                    else
                        divSavePageSummary.Attributes.Add("style", "display:none;");
                }

                if (action.Equals("ICFR"))
                {
                    if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                    Session["TotalRowsReAssging"] = null;
                    grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                    Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                    grdReassign.DataBind();
                }

                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindVerticalPopUp(int? BranchId)
        {
            if (BranchId != null)
            {
                ddlVerticalPopup.DataTextField = "VerticalName";
                ddlVerticalPopup.DataValueField = "VerticalsId";
                ddlVerticalPopup.Items.Clear();
                ddlVerticalPopup.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId); //FillVerticalList(int customerID)
                ddlVerticalPopup.DataBind();
                ddlVerticalPopup.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }

        public void BindVerticalPopUp()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlVerticalPopup.DataTextField = "VerticalName";
            ddlVerticalPopup.DataValueField = "ID";
            ddlVerticalPopup.Items.Clear();
            ddlVerticalPopup.DataSource = UserManagementRisk.FillVerticalList(customerID); //FillVerticalList(int customerID)
            ddlVerticalPopup.DataBind();
            ddlVerticalPopup.Items.Insert(0, new ListItem("Select Vertical", "-1"));
        }

        protected void ddlLegalEntityPopUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity1Popup, Convert.ToInt32(ddlLegalEntityPopup.SelectedValue));
                    //BindVerticalPopUp(Convert.ToInt32(ddlLegalEntityPopup.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1Popup.Items.Count > 0)
                        ddlSubEntity1Popup.Items.Clear();

                    if (ddlSubEntity2Popup.Items.Count > 0)
                        ddlSubEntity2Popup.Items.Clear();

                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }

                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity1Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity2Popup, Convert.ToInt32(ddlSubEntity1Popup.SelectedValue));
                    //BindVerticalPopUp(Convert.ToInt32(ddlSubEntity1Popup.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2Popup.Items.Count > 0)
                        ddlSubEntity2Popup.Items.Clear();

                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }

                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity2Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlSubEntity3Popup, Convert.ToInt32(ddlSubEntity2Popup.SelectedValue));
                    BindVerticalPopUp(Convert.ToInt32(ddlSubEntity2Popup.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3Popup.Items.Count > 0)
                        ddlSubEntity3Popup.Items.Clear();

                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }

                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity3Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    BindSubEntityDataPopUp(ddlLocationPopup, Convert.ToInt32(ddlSubEntity3Popup.SelectedValue));
                    //BindVerticalPopUp(Convert.ToInt32(ddlSubEntity3Popup.SelectedValue));                  
                }
                else
                {
                    if (ddlLocationPopup.Items.Count > 0)
                        ddlLocationPopup.Items.Clear();
                }

                ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlLocationPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
            {
                if (ddlLocationPopup.SelectedValue != "-1")
                {
                    //BindVerticalPopUp(Convert.ToInt32(ddlLocationPopup.SelectedValue));                    
                }
            }

            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlFinYearPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
            ///*BindProcessAudits*/();
        }

        protected void ddlVerticalPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlAuditStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void txtReAssignFilter_TextChanged(object sender, EventArgs e)
        {
            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            int processid = -1;
            string processName = string.Empty;
            processName = txtReAssignFilter.Text;
            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
            if (action.Equals("Audit"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignData(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignData(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("Implementation"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("ICFR"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }

                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
        }

        public void BindProcess()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                if (action.Equals("Audit"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataProc(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("Implementation"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataImp(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("ICFR"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataICFR(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYearPopup()
        {
            ddlFinYearPopup.DataTextField = "Name";
            ddlFinYearPopup.DataValueField = "ID";
            ddlFinYearPopup.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinYearPopup.DataBind();
            ddlFinYearPopup.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindReAssingGrid();
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ViewState["ReAssignID"] = null;

            if (ddlUser.SelectedValue != "" || ddlUser.SelectedValue != "-1")
            {
                BindUserList(customerID, ddlREAssignUser);

                if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                    ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));

                ViewState["ReAssignID"] = Convert.ToInt32(ddlUser.SelectedValue);
            }
            BindVerticalPopUp();
            BindFinancialYearPopup();
            BindLegalEntityDataPopUp();
            BindReAssingGrid();
        }

        protected void ddlREAssignUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");

                    if (ddlGridReAssignUser.Enabled)
                    {
                        ddlGridReAssignUser.ClearSelection();
                        ddlGridReAssignUser.SelectedValue = ddlREAssignUser.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSizeReAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNoReAssign.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo <= GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }

                //Reload the Grid
                BindReAssingGrid();

            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnReAssign_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                lblReAssign.Text = string.Empty;
                ViewState["ReAssignID"] = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID; //userID;

                User user = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                lblReAssign.Text = user.FirstName + " " + user.LastName;

                BindUserList(customerID, ddlUser);

                ReAssign.Update();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenModel", "javascript:openModal()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ReAssignCheckBoxValueSaved()
        {
            List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            int ProceID = -1;
            foreach (GridViewRow gvrow in grdReassign.Rows)
            {
                ProceID = Convert.ToInt32(grdReassign.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkEvent")).Checked;
                int BranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblBrachID")).Text);
                if (ViewState["ReAssignedEventID"] != null)
                    chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];

                var checkedData = chkList.Where(entry => entry.Item1 == ProceID && entry.Item2 == BranchID).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, int>(ProceID, BranchID));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["ReAssignedEventID"] = chkList;
        }

        protected void btnReassignAuditSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    if (ddlUser.SelectedValue != "-1" && ddlUser.SelectedValue != "" && ddlRole.SelectedValue != "-1" && ddlRole.SelectedValue != "")
                    {
                        string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                        if (action.Equals("Audit"))
                        {
                            saveSuccess = SaveReAssignProcessAuditDetails();
                        }
                        else if (action.Equals("Implementation"))
                        {
                            saveSuccess = SaveReAssignImplementationAuditDetails();
                        }
                    }
                    else
                    {
                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Please Select User and Role.";
                    }
                    BindReAssingGrid();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                    ViewState["ReAssignedEventID"] = null;

                    if (saveSuccess)
                    {
                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Audit Re-Assign Details Save Successfully.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBPreviousReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNoReAssign.Text) > 1)
                {
                    SelectedPageNoReAssign.Text = (Convert.ToInt32(SelectedPageNoReAssign.Text) - 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindReAssingGrid();
                //GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void lBNextReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);
                if (currentPageNo < GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo + 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void GetPageDisplaySummaryReAssign()
        {
            try
            {
                lTotalCountReAssign.Text = GetTotalPagesCountReAssign().ToString();
                if (lTotalCountReAssign.Text != "0")
                {
                    if (SelectedPageNoReAssign.Text == "")
                        SelectedPageNoReAssign.Text = "1";

                    if (SelectedPageNoReAssign.Text == "0")
                        SelectedPageNoReAssign.Text = "1";
                }
                else if (lTotalCountReAssign.Text == "0")
                {
                    SelectedPageNoReAssign.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCountReAssign()
        {
            try
            {
                TotalRowsReAssign.Value = Session["TotalRowsReAssging"].ToString();
                int totalPages = Convert.ToInt32(TotalRowsReAssign.Value) / Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsReAssign.Value) % Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlReviewerFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer2");
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewerIMP2");

                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        public string GetProcessName(int AuditID)
        {
            var ProcessNameList = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.InternalAuditSchedulings
                                   join row1 in entities.Mst_Process
                                   on row.Process equals row1.Id
                                   where row.AuditID == AuditID
                                   select row1.Name).Distinct().ToList();

                if (Processlist.Count > 0)
                {
                    foreach (var item in Processlist)
                    {
                        ProcessNameList += item + ",";
                    }
                }
                ProcessNameList = ProcessNameList.TrimEnd(',');
                ProcessNameList = ProcessNameList.TrimStart(',');

                return ProcessNameList;
            }
        }

        public string GetSubProcessName(int AuditID)
        {
            var SubProcessNameList = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.AuditScheduling_SubProcessMapping
                                   join row1 in entities.mst_Subprocess
                                   on row.SubProcessID equals row1.Id
                                   join RATBD in entities.RiskActivityToBeDoneMappings
                                   on row.Process equals RATBD.ProcessId
                                   where row.AuditID == AuditID
                                   && row.SubProcessID == RATBD.SubProcessId
                                   && row.IsActive == true
                                   select row1.Name).Distinct().ToList();

                if (Processlist.Count > 0)
                {
                    foreach (var item in Processlist)
                    {
                        SubProcessNameList += item + ",";
                    }
                }
                SubProcessNameList = SubProcessNameList.TrimEnd(',');
                SubProcessNameList = SubProcessNameList.TrimStart(',');

                return SubProcessNameList;
            }
        }
    }
}