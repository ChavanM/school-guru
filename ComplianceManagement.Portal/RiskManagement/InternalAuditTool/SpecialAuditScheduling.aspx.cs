﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class SpecialAuditScheduling : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        bool suucess = false;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFinancialYear();
                BindMainGrid();
                bindPageNumber();
            }
        }
        public void BindFinancialYear()
        {
            var data = UserManagementRisk.FillFnancialYear();

            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = data;
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));

            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = data;
            ddlFilterFinancialYear.DataBind();
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            //var data = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, UserID);
            }
            //ddlLegalEntity.DataSource = data;
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityPop.DataTextField = "Name";
            ddlLegalEntityPop.DataValueField = "ID";
            ddlLegalEntityPop.Items.Clear();
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntityPop.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, UserID);
            }
            //ddlLegalEntityPop.DataSource = data;
            ddlLegalEntityPop.DataBind();
            ddlLegalEntityPop.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdAuditScheduling.PageIndex = chkSelectedPage - 1;
                grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindMainGrid();
            }
        }
        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                var data = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = data;
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("All", "-1"));

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = data;
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindMainGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditScheduling.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvSpecialAuditError.IsValid = false;
                cvSpecialAuditError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindVerticalID(Convert.ToInt32(ddlSubEntity4.SelectedValue));
                }
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMainGrid();
            bindPageNumber();
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMainGrid();
            bindPageNumber();
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divAuditSchedulingDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntityPop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
            {
                if (ddlLegalEntityPop.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1Pop, Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                }

                if (ddlSubEntity2Pop.Items.Count > 0)
                    ddlSubEntity2Pop.Items.Clear();

                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();
            }
        }

        protected void ddlSubEntity1Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
            {
                if (ddlSubEntity1Pop.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2Pop, Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                }
                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();
            }
        }

        protected void ddlSubEntity2Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
            {
                if (ddlSubEntity2Pop.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3Pop, Convert.ToInt32(ddlSubEntity2Pop.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2Pop.SelectedValue));
                }

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();
            }
        }

        protected void ddlSubEntity3Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
            {
                if (ddlSubEntity3Pop.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4Pop, Convert.ToInt32(ddlSubEntity3Pop.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3Pop.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4Pop.Items.Count > 0)
                        ddlSubEntity4Pop.Items.Clear();
                }

            }
        }

        protected void ddlSubEntity4Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
            {
                BindVerticalID(Convert.ToInt32(ddlSubEntity4Pop.SelectedValue));
            }
        }

        protected void ddlFinancialYearpop_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditSchedule("A", 0);
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                int Branchid = -1;

                if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                {
                    if (ddlLegalEntityPop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                {
                    if (ddlSubEntity1Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                {
                    if (ddlSubEntity2Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                {
                    if (ddlSubEntity3Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                {
                    if (ddlSubEntity4Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                    }
                }

                if (flag == "A")
                {
                    grdAnnually.DataSource = null;
                    grdAnnually.DataBind();

                    pnlAnnually.Visible = true;

                    if (ddlFinancialYear.SelectedItem.Text != "Financial Year")
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;

                        grdAnnually.DataSource = ProcessManagement.GetSPOneTimeDisplay(Branchid, financialyear);
                        grdAnnually.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (ddlFinancialYear.SelectedValue != "-1")
                {

                    int CustomerBranchId = -1;
                    int verticalId = -1;

                    if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                    {
                        if (ddlLegalEntityPop.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                    {
                        if (ddlSubEntity1Pop.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                    {
                        if (ddlSubEntity2Pop.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                    {
                        if (ddlSubEntity3Pop.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                    {
                        if (ddlSubEntity4Pop.SelectedValue != "-1")
                        {
                            CustomerBranchId = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            verticalId = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }

                    if (verticalId != -1)
                    {
                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] fsplit = financialyear.Split('-');
                        string fyear = fsplit[0];
                        string syear = fsplit[1];
                        string f1 = fyear + "-" + syear;

                        #region Annualy Save

                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                        int processid = -1;
                        int Subprocessid = -1;
                        int InternalAuditScheduleOnID = 0;
                        bool Result = false;
                        for (int i = 0; i < grdAnnually.Rows.Count; i++)
                        {
                            GridViewRow row = grdAnnually.Rows[i];
                            TextBox txtNewProcess = (TextBox)row.FindControl("txtNewProcess");

                            if (!string.IsNullOrEmpty(txtNewProcess.Text))
                            {
                                Mst_Process ProcessData = new Mst_Process()
                                {
                                    Name = txtNewProcess.Text,
                                    Description = txtNewProcess.Text,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedOn = DateTime.Now,
                                    IsDeleted = false,
                                    IsProcessNonProcess = "P",
                                    DepartmentID = 6,
                                    CustomerID = CustomerId,
                                };

                                //if (ProcessManagement.Exists(ProcessData.Name))
                                if (ProcessManagement.Exists(CustomerId, ProcessData.Name))
                                {
                                    cvDuplicateEntry.ErrorMessage = "Process with Same already exists.";
                                    cvDuplicateEntry.IsValid = false;
                                    return;
                                }
                                else
                                {
                                    ProcessManagement.Create(ProcessData);
                                    processid = Convert.ToInt32(ProcessData.Id);
                                    mst_Subprocess objsubProcess = new mst_Subprocess();
                                    objsubProcess.Name = txtNewProcess.Text;
                                    objsubProcess.Description = "";
                                    objsubProcess.ProcessId = processid;
                                    objsubProcess.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                    objsubProcess.CreatedOn = DateTime.Now;
                                    objsubProcess.IsDeleted = false;
                                    objsubProcess.IsProcessNonProcess = "P";
                                    objsubProcess.Scores = 0;
                                    ProcessManagement.SubprocessCreate(objsubProcess);
                                    Subprocessid = Convert.ToInt32(objsubProcess.Id);
                                }

                                CheckBox bf = (CheckBox)row.FindControl("chkOneTime");
                                string Period = "Special Audit";
                                long AuditID = UserManagementRisk.CreateAuditID(CustomerBranchId, verticalId, f1, Period, CustomerId);
                                if (bf.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Special Audit";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "S";
                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                    Internalauditscheduling.VerticalID = (int)verticalId;
                                    Internalauditscheduling.AuditID = AuditID;
                                    if (processid != 0)
                                    {
                                        InternalAuditScheduleOnID = UserManagementRisk.AddDetailsInternalAuditSchedulingTable_ScheduleFirst(Internalauditscheduling);
                                    }
                                    AuditScheduling_SubProcessMapping AuditScheduling_SubProcessMapping = new AuditScheduling_SubProcessMapping();
                                    AuditScheduling_SubProcessMapping.Process = processid;
                                    AuditScheduling_SubProcessMapping.AuditID = AuditID;
                                    AuditScheduling_SubProcessMapping.SubProcessID = Subprocessid;
                                    AuditScheduling_SubProcessMapping.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                    AuditScheduling_SubProcessMapping.CreatedOn = DateTime.Now;
                                    AuditScheduling_SubProcessMapping.ScheduleOnID = InternalAuditScheduleOnID;
                                    AuditScheduling_SubProcessMapping.IsActive = true;
                                    AuditScheduling_SubProcessMapping.IsDeleted = false;
                                    if (processid != 0 && AuditID != 0 && Subprocessid != 0)
                                    {
                                        UserManagementRisk.AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping);
                                        Result = true;
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Provide Process Name.";
                                return;
                            }
                        }
                        if (Result)
                        {
                            // UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            if (!AssignEntityAuditManager.EntitiesAssignmentAuditManagerRiskExist(CustomerId, Convert.ToInt32(CustomerBranchId), com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, processid))
                            {
                                EntitiesAssignmentAuditManagerRisk objEntitiesAssignmentrisk = new EntitiesAssignmentAuditManagerRisk();
                                objEntitiesAssignmentrisk.UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                objEntitiesAssignmentrisk.CustomerID = CustomerId;
                                objEntitiesAssignmentrisk.BranchID = Convert.ToInt32(CustomerBranchId);
                                objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                objEntitiesAssignmentrisk.ISACTIVE = true;
                                objEntitiesAssignmentrisk.ProcessId = processid;
                                objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                AssignEntityAuditManager.CreateEntitiesAssignmentAuditManagerRisklist(objEntitiesAssignmentrisk);
                            }
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Data successfully saved.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                        }

                        #endregion

                        BindMainGrid();
                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdAuditScheduling.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "Please Select Vertical.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Please Select Financial Year.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindMainGrid()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            string FinancialYear = string.Empty;
            int CustomerBranchId = -1;

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }

            int VerticalID = -1;
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }

            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
            var Branchlistloop = Branchlist.ToList();

            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.InternalAuditSchedulings
                             join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                             join row1 in entities.mst_Vertical
                             on row.VerticalID equals row1.ID
                             join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                             on row.Process equals EAAR.ProcessId
                             join MP in entities.Mst_Process
                             on row.Process equals MP.Id
                             where row.TermStatus == true
                             && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                             && row.IsDeleted == false && MCB.CustomerID == CustomerId
                             && EAAR.ISACTIVE == true && EAAR.BranchID == row.CustomerBranchId
                             && row.ISAHQMP == "S" && MP.IsDeleted == false && MCB.IsDeleted == false
                             select new AuditSpecialScheduling()
                             {
                                 CustomerId = MCB.CustomerID,
                                 FinancialYear = row.FinancialYear,
                                 CustomerBranchId = row.CustomerBranchId,
                                 ISAHQMP = row.ISAHQMP,
                                 TermName = row.TermName,
                                 VerticalName = row1.VerticalName,
                                 VerticalID = row.VerticalID,
                                 ProcessId = (int)row.Process,
                                 ProcessName = MP.Name,
                                 CustomerBranchName = MCB.Name,
                                 AuditID = row.AuditID,
                             }).Distinct().ToList();


                //var query = (from row in entities.InternalAuditSchedulings
                //             join MCB in entities.mst_CustomerBranch
                //             on row.CustomerBranchId equals MCB.ID
                //             join row1 in entities.mst_Vertical
                //             on row.VerticalID equals row1.ID
                //             join MP in entities.Mst_Process
                //             on row.Process equals MP.Id
                //             where row.TermStatus == true
                //             && row.IsDeleted == false
                //             && MCB.CustomerID == CustomerId
                //             && row.ISAHQMP == "S" && MP.IsDeleted == false
                //             select new AuditSpecialScheduling()
                //             {
                //                 CustomerId = MCB.CustomerID,
                //                 FinancialYear = row.FinancialYear,
                //                 CustomerBranchId = row.CustomerBranchId,
                //                 ISAHQMP = row.ISAHQMP,
                //                 TermName = row.TermName,
                //                 VerticalName = row1.VerticalName,
                //                 VerticalID = row.VerticalID,
                //                 ProcessId = (int)row.Process,
                //                 ProcessName = MP.Name,
                //                 CustomerBranchName = MCB.Name,
                //             }).Distinct().ToList();

                if (Branchlist.Count > 0)
                {
                    query = query.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                }
                if (FinancialYear != "")
                {
                    query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                if (VerticalID != -1)
                {
                    query = query.Where(entry => entry.VerticalID == VerticalID).ToList();
                }

                grdAuditScheduling.DataSource = null;
                Session["TotalRows"] = query.Count;
                grdAuditScheduling.DataBind();

                if (query != null)
                {
                    grdAuditScheduling.DataSource = query;
                    Session["TotalRows"] = query.Count;
                    grdAuditScheduling.DataBind();
                }
            }

        }

        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }

        public string ShowStepsUploadOrNot(int PID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var AuditSteps = (from row in entities.RiskActivityToBeDoneMappings
                                  where row.ProcessId == PID
                                  select row).ToList();

                if (AuditSteps.Count > 0)
                    return "Uploaded";
                else
                    return "Not Uploaded";
            }
        }

        protected void grdAnnually_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;

                if (ddlFinancialYear.SelectedItem.Text != "Financial Year")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        TableCell cell = e.Row.Cells[1];
                        cell.ColumnSpan = 1;
                        cell.Text = "Process";
                        cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                        cell.BorderColor = Color.White;
                        cell.Font.Bold = true;
                        cell.HorizontalAlign = HorizontalAlign.Left;

                        TableCell cell2 = e.Row.Cells[1];
                        cell2.ColumnSpan = 1;
                        cell2.Text = "SubProcess";
                        cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                        cell2.BorderColor = Color.White;
                        cell2.Font.Bold = true;
                        cell2.HorizontalAlign = HorizontalAlign.Left;

                        TableCell cell1 = e.Row.Cells[2];
                        cell1.ColumnSpan = 1;
                        cell1.Text = f1;
                        cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                        cell1.BorderColor = Color.White;
                        cell1.Font.Bold = true;
                        cell1.HorizontalAlign = HorizontalAlign.Left;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        
        protected void grdAuditScheduling_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvRow = e.Row;
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                Label lblProcessID = e.Row.FindControl("lblProcessId") as Label;

                FileUpload fuSpecialAuditSteps = e.Row.FindControl("fuSpecialAuditSteps") as FileUpload;
                Label lblDownLoadfile = e.Row.FindControl("lblDownLoadfile") as Label;
                Button btnAuditStepsSave = e.Row.FindControl("btnAuditStepsSave") as Button;

                if (ShowStepsUploadOrNot(Convert.ToInt32(lblProcessID.Text)) == "Uploaded")
                {
                    lblDownLoadfile.Text = "Uploaded";
                    btnAuditStepsSave.Enabled = false;
                    fuSpecialAuditSteps.Enabled = false;
                }
                else
                {
                    lblDownLoadfile.Text = "Not Uploaded";
                    btnAuditStepsSave.Enabled = true;
                    fuSpecialAuditSteps.Enabled = true;
                }
            }
        }
        protected void grdAuditScheduling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("UploadAuditSteps"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ProcessID = Convert.ToInt32(commandArgs[0]);
                    int verticalid = Convert.ToInt32(commandArgs[1]);
                    int CustomerBranchID = Convert.ToInt32(commandArgs[2]);
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    FileUpload fuSpecialAuditSteps = (FileUpload)gvr.FindControl("fuSpecialAuditSteps");

                    if (fuSpecialAuditSteps.HasFile)
                    {
                        try
                        {
                            string filename = System.IO.Path.GetFileName(fuSpecialAuditSteps.FileName);
                            fuSpecialAuditSteps.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    bool flag = SpecialAuditStepSheetsExitsts(xlWorkbook, "AuditSteps");

                                    if (flag)
                                        ProcessSpecialAuditSteps(xlWorkbook, ProcessID, verticalid, CustomerBranchID);
                                    else
                                    {
                                        cvSpecialAuditError.IsValid = false;
                                        cvSpecialAuditError.ErrorMessage = "No data found in file.";
                                    }

                                    if (suucess == true)
                                    {
                                        cvSpecialAuditError.IsValid = false;
                                        cvSpecialAuditError.ForeColor = Color.Green;
                                        cvSpecialAuditError.ErrorMessage = "Data Uploaded Successfully.";
                                    }

                                }
                            }
                            else
                            {
                                cvSpecialAuditError.IsValid = false;
                                cvSpecialAuditError.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            cvSpecialAuditError.IsValid = false;
                            cvSpecialAuditError.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                    }
                }
                BindMainGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private bool SpecialAuditStepSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditSteps"))
                    {
                        if (sheet.Name.Trim().Equals("AuditSteps"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        private void ProcessSpecialAuditSteps(ExcelPackage xlWorkbook, int ProcessID, int VerticalID, int CustomerBranchID)
        {
            try
            {
                List<string> RiskIdProcess = new List<string>();
                List<string> IndustryIdProcess = new List<string>();
                List<string> BranchVerticalID = new List<string>();
                List<string> AssertionsIdProcess = new List<string>();
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AuditSteps"];
                int subprocessid = -1;
                long ActivityID = -1;
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<RiskCategoryCreation> riskcategorycreationlist = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist1 = new List<RiskCategoryCreation>();
                    List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();

                    string rating = "";
                    int ratingid = -1;
                    string activitytobedone = "";

                    //Validate Excel Data
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        rating = "";
                        ratingid = -1;
                        activitytobedone = "";
                        //1 Activity To be Done
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text))
                        {
                            activitytobedone = xlWorksheet.Cells[i, 1].Text.Trim();
                        }
                        if (activitytobedone == null || activitytobedone == "")
                        {
                            suucess = false;
                            cvSpecialAuditError.IsValid = false;
                            cvSpecialAuditError.ErrorMessage = "Please Correct the Audit Step at row number - " + i + "/ Audit Step can not Empty.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //2 Rating
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                        {
                            rating = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (rating.ToUpper() == "Major".ToUpper())
                            {
                                ratingid = 1;
                            }
                            else if (rating.ToUpper() == "Moderate".ToUpper())
                            {
                                ratingid = 2;
                            }
                            else if (rating.ToUpper() == "Minor".ToUpper())
                            {
                                ratingid = 3;
                            }
                            else
                            {
                                ratingid = 0;
                            }
                            if (ratingid == 0)
                            {
                                suucess = false;
                                cvSpecialAuditError.IsValid = false;
                                cvSpecialAuditError.ErrorMessage = "Please Correct the Rating at row number - " + i + " or Rating not Defined in the System.";
                                break;
                            }
                            else
                            {
                                suucess = true;
                            }
                        }
                        else
                        {
                            suucess = false;
                            cvSpecialAuditError.IsValid = false;
                            cvSpecialAuditError.ErrorMessage = "Please Correct the Rating at row number - " + i + " or Rating not Defined in the System.";
                            break;
                        }

                        //Person Responsible
                        int personresponsibleIDcheck = -1;

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                            {
                                //4 Email
                                personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 3].Text.ToString().Trim(), CustomerId, xlWorksheet.Cells[i, 4].Text.ToString().Trim());
                            }
                        }
                        if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                        {
                            suucess = false;
                            cvSpecialAuditError.IsValid = false;
                            cvSpecialAuditError.ErrorMessage = "Please Correct the Person Responsible/Email at row number - " + (count + 1) + " or Person Responsible not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                    }

                    if (suucess)
                    {
                        if (ProcessID != 0 && ProcessID != -1)
                        {
                            String ProcessName = "";
                            ProcessName = ProcessManagement.GetProcessName(ProcessID, CustomerId);

                            if (ProcessName != "")
                            {
                                subprocessid = ProcessManagement.GetSubProcessBYName(ProcessID, ProcessName);
                                //Create New SubProcess Same Name As Process created at Special Audit Scheduling
                                //mst_Subprocess SubprocessData = new mst_Subprocess()
                                //{
                                //    Name = ProcessName,
                                //    Description = ProcessName,
                                //    ProcessId = ProcessID,
                                //    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                //    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                //    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                //    CreatedOn = DateTime.Now,
                                //    UpdatedOn = DateTime.Now,
                                //    IsProcessNonProcess = "P",
                                //    Scores = Convert.ToDecimal(0)
                                //};

                                //if (ProcessManagement.SubprocessExists(SubprocessData))
                                //{
                                //    cvDuplicateEntry.ErrorMessage = "Sub Process With Same Already Exists.";
                                //    cvDuplicateEntry.IsValid = false;
                                //    return;
                                //}
                                //else
                                //{
                                //    ProcessManagement.SubprocessCreate(SubprocessData);
                                //    subprocessid = Convert.ToInt32(SubprocessData.Id);
                                //}

                                mst_Activity subactivity = new mst_Activity()
                                {
                                    Name = ProcessName,
                                    ProcessId = Convert.ToInt32(ProcessID),
                                    SubProcessId = Convert.ToInt32(subprocessid),
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                };
                                if (ProcessManagement.ActivityExists(subactivity))
                                {
                                    cvDuplicateEntry.ErrorMessage = "Activity Name Already Exists.";
                                    cvDuplicateEntry.IsValid = false;
                                    return;
                                }
                                else
                                {
                                    ProcessManagement.SubActivityCreate(subactivity);
                                    ActivityID = subactivity.Id;
                                }

                                BranchVerticalID.Add(VerticalID.ToString());
                                string ControlDescription = string.Empty;
                                string ControlNo = "";
                                int processid = -1;
                                string ActivityDescription = "";
                                string ControlObjective = "";
                                int riskcreationId = -1;
                                int riskActivityId = -1;
                                int LocationType = 3;

                                //Control No
                                ControlNo = Convert.ToString("NA").Trim();

                                //Process
                                processid = ProcessID;

                                //Sub Process 
                                //subprocessid = ProcessManagement.GetSubProcessIDByName("Special Audit");

                                //Ris or Activity Description
                                ActivityDescription = Convert.ToString("NA").Trim();

                                //Control Objective                        
                                ControlObjective = Convert.ToString("NA").Trim();

                                //11 Control Description
                                ControlDescription = Convert.ToString("NA").Trim();

                                //Location
                                int Customerbranchid = CustomerBranchID;
                                RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation();
                                riskcategorycreation.ControlNo = ControlNo;
                                riskcategorycreation.ProcessId = processid;
                                riskcategorycreation.SubProcessId = subprocessid;
                                riskcategorycreation.ActivityDescription = ActivityDescription;
                                riskcategorycreation.ControlObjective = ControlObjective;
                                riskcategorycreation.IsDeleted = false;
                                riskcategorycreation.LocationType = LocationType;
                                riskcategorycreation.IsInternalAudit = "N";
                                riskcategorycreation.CustomerId = CustomerId;
                                riskcategorycreation.CustomerBranchId = Customerbranchid;
                                riskcategorycreationlist.Add(riskcategorycreation);

                                RiskCategoryManagement.RiskCategoryCreationMapping(riskcategorycreation);
                                riskcreationId = Convert.ToInt32(riskcategorycreation.Id);

                                string MControlDescription = string.Empty;
                                int personresponsibleID = 0;
                                int KeyID = 0;
                                int PrevationControlID = 0;
                                int AutomatedControlID = 0;
                                int Frequency = 0;
                                int KC2 = 0;
                                string GapDescription = "";
                                string Recommendations = "";
                                string ActionRemediationplan = "";
                                string IPE = "";
                                string ERPsystem = "";
                                string FRC = "";
                                string UniqueReferred = "";
                                string TestStrategy = "";
                                string DocumentsExamined = "";
                                long auditStepMasterID = -1;
                                //Control Description
                                ControlDescription = Convert.ToString("NA").Trim();
                                MControlDescription = Convert.ToString("NA").Trim();
                                //13 Person Responsible
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[2, 3].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[2, 4].Text.ToString().Trim()))
                                    {
                                        personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[2, 3].Text.ToString().Trim(), CustomerId, xlWorksheet.Cells[2, 4].Text.ToString().Trim());
                                    }
                                }
                                //15 Effective Date	                          
                                DateTime dt = GetDate(DateTime.Now.Date.ToString("dd/MM/yyyy"));

                                //16 Key
                                KeyID = 1;
                                KC2 = 1;
                                //17 Preventive Control
                                PrevationControlID = 1;
                                //18 Automated Control
                                AutomatedControlID = 1;
                                //19 Frequency
                                Frequency = RiskCategoryManagement.GetFrequencyIDByName("One Time");
                                //20 Gap Description
                                GapDescription = Convert.ToString("NA");
                                //21 Recommendations
                                Recommendations = Convert.ToString("NA");
                                //22 Action Remediation Plan
                                ActionRemediationplan = Convert.ToString("NA");
                                //23 Information Produced by Entity
                                IPE = Convert.ToString("NA");
                                //24 ERP System	
                                ERPsystem = Convert.ToString("NA");
                                //25 Fraud Risk Control
                                FRC = Convert.ToString("NA");
                                //26 Unique Referred
                                UniqueReferred = Convert.ToString("NA");
                                //27 Test Strategy
                                TestStrategy = Convert.ToString("NA");
                                //28 Documents Examined
                                DocumentsExamined = Convert.ToString("NA");
                                //30 Location Type
                                LocationType = 3;

                                //31 Risk Rating
                                int RiskRating = 3;

                                //32 Control Rating
                                int ControlRating = 1;

                                //33 Process Scores
                                int processscores = 0;

                                #region RiskCatrgory Mapping add to List  

                                string risk = "Others";
                                string[] split = risk.Split(',');

                                if (split.Length > 0)
                                {
                                    string finalriskcategoryid = "";
                                    string riskcategoryid = "";

                                    for (int rs = 0; rs < split.Length; rs++)
                                    {
                                        riskcategoryid = Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName(split[rs].ToString().Trim()));

                                        if (riskcategoryid == "0" || riskcategoryid == "-1")
                                        {
                                            suucess = false;
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Correct the Risk catrgory Name at row number - " + (count + 1) + " or Risk catrgory Name not Defined in the System.";
                                            break;
                                        }
                                        else
                                        {
                                            suucess = true;
                                        }
                                        if (split.Length > 1)
                                        {
                                            finalriskcategoryid += riskcategoryid + ",";
                                        }
                                        else
                                        {
                                            finalriskcategoryid += riskcategoryid;
                                        }
                                    }

                                    RiskIdProcess.Add(finalriskcategoryid.Trim(','));
                                }

                                #endregion

                                #region Riskcatrgory Mapping Save

                                if (RiskIdProcess.Count > 0)
                                {
                                    riskcategorycreationlist.ForEach(entry =>
                                    {
                                        for (int j = 0; j < RiskIdProcess.Count; j++)
                                        {
                                            string aItem = RiskIdProcess[j].ToString();
                                            if (aItem.ToString().Contains(","))
                                            {
                                                string[] Riskcatrgorysplit = aItem.ToString().Split(',');

                                                if (Riskcatrgorysplit.Length > 0)
                                                {
                                                    for (int rs = 0; rs < Riskcatrgorysplit.Length; rs++)
                                                    {
                                                        RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                        {
                                                            RiskCategoryCreationId = riskcreationId,
                                                            RiskCategoryId = Convert.ToInt32(Riskcatrgorysplit[rs]),
                                                            ProcessId = processid,
                                                            SubProcessId = subprocessid,
                                                            IsActive = true,
                                                        };

                                                        RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                                    }
                                                    RiskIdProcess.Remove(aItem);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (aItem != "0")
                                                {
                                                    RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                    {
                                                        RiskCategoryCreationId = riskcreationId,
                                                        RiskCategoryId = Convert.ToInt32(aItem),
                                                        ProcessId = processid,
                                                        SubProcessId = subprocessid,
                                                        IsActive = true,
                                                    };
                                                    RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                                }

                                                RiskIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                    });

                                    RiskIdProcess.Clear();
                                }
                                #endregion

                                #region Assertions Mapping add to List

                                string Assertions = "Not Applicable";
                                string[] splitAssertions = Assertions.Split(',');

                                if (splitAssertions.Length > 0)
                                {
                                    string finalAssertionsmappigid = "";
                                    string Assertionsmappigid = "";

                                    for (int rs = 0; rs < splitAssertions.Length; rs++)
                                    {
                                        Assertionsmappigid = Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName(splitAssertions[rs].ToString().Trim()));

                                        if (Assertionsmappigid == "0" || Assertionsmappigid == "-1")
                                        {
                                            suucess = false;
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Correct the Assertions at row number - " + (count + 1) + " or Assertions not Defined in the System.";
                                            break;
                                        }
                                        else
                                        {
                                            suucess = true;
                                        }

                                        if (splitAssertions.Length > 1)
                                        {
                                            finalAssertionsmappigid += Assertionsmappigid + ",";
                                        }
                                        else
                                        {
                                            finalAssertionsmappigid += Assertionsmappigid;
                                        }
                                    }

                                    AssertionsIdProcess.Add(finalAssertionsmappigid.Trim(','));
                                }

                                #endregion

                                #region Assertions Mapping Save

                                if (AssertionsIdProcess.Count > 0)
                                {
                                    riskcategorycreationlist.ForEach(entry =>
                                    {
                                        for (int i = 0; i < AssertionsIdProcess.Count; i++)
                                        {
                                            string aItem = AssertionsIdProcess[i].ToString();
                                            if (aItem.ToString().Contains(","))
                                            {
                                                string[] Assertionssplit = aItem.ToString().Split(',');
                                                if (Assertionssplit.Length > 0)
                                                {
                                                    for (int rs = 0; rs < Assertionssplit.Length; rs++)
                                                    {
                                                        AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                        {
                                                            RiskCategoryCreationId = riskcreationId,
                                                            AssertionId = Convert.ToInt32(Assertionssplit[rs]),
                                                            ProcessId = processid,
                                                            SubProcessId = subprocessid,
                                                            IsActive = true,
                                                        };
                                                        RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                                    }
                                                    AssertionsIdProcess.Remove(aItem);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (aItem != "0")
                                                {
                                                    AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                    {
                                                        RiskCategoryCreationId = riskcreationId,
                                                        AssertionId = Convert.ToInt32(aItem),
                                                        ProcessId = processid,
                                                        SubProcessId = subprocessid,
                                                        IsActive = true,
                                                    };
                                                    RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                                }
                                                AssertionsIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                    });

                                    AssertionsIdProcess.Clear();
                                }
                                #endregion

                                #region Industry Mapping add to List

                                string industry = "Other";
                                string[] splitindustry = industry.Split(',');

                                if (splitindustry.Length > 0)
                                {
                                    string finalindustrymappigid = "";
                                    string industrymappigid = "";
                                    for (int rs = 0; rs < splitindustry.Length; rs++)
                                    {
                                        industrymappigid = Convert.ToString(RiskCategoryManagement.GetIndustryIdByName(splitindustry[rs].ToString().Trim()));

                                        if (industrymappigid == "0" || industrymappigid == "-1")
                                        {
                                            suucess = false;
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Correct the Industry at row number - " + (count + 1) + " or Industry not Defined in the System.";
                                            break;
                                        }
                                        else
                                        {
                                            suucess = true;
                                        }

                                        if (splitindustry.Length > 1)
                                        {
                                            finalindustrymappigid += industrymappigid + ",";
                                        }
                                        else
                                        {
                                            finalindustrymappigid += industrymappigid;
                                        }
                                    }
                                    IndustryIdProcess.Add(finalindustrymappigid.Trim(','));
                                }

                                #endregion

                                #region Industry Mapping Save

                                if (IndustryIdProcess.Count > 0)
                                {
                                    riskcategorycreationlist.ForEach(entry =>
                                    {
                                        for (int k = 0; k < IndustryIdProcess.Count; k++)
                                        {
                                            string aItem = IndustryIdProcess[k].ToString();

                                            if (aItem.ToString().Contains(","))
                                            {
                                                string[] splitIndustry = aItem.ToString().Split(',');
                                                if (splitIndustry.Length > 0)
                                                {
                                                    for (int rs = 0; rs < splitIndustry.Length; rs++)
                                                    {
                                                        IndustryMapping IndustryMapping = new IndustryMapping()
                                                        {
                                                            RiskCategoryCreationId = riskcreationId,
                                                            IndustryID = Convert.ToInt32(splitIndustry[rs]),
                                                            IsActive = true,
                                                            EditedDate = DateTime.Now,
                                                            EditedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                            ProcessId = processid,
                                                            SubProcessId = subprocessid,
                                                        };
                                                        RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                                    }
                                                    IndustryIdProcess.Remove(aItem);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (aItem != "0")
                                                {
                                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        IndustryID = Convert.ToInt32(aItem),
                                                        IsActive = true,
                                                        EditedDate = DateTime.Now,
                                                        EditedBy = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID),
                                                        ProcessId = entry.ProcessId,
                                                        SubProcessId = entry.SubProcessId,
                                                    };
                                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                                }
                                                IndustryIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                    });

                                    IndustryIdProcess.Clear();
                                }
                                #endregion

                                //if (!(RiskCategoryManagement.Exists(processid, subprocessid, riskcreationId, Convert.ToInt32(Frequency), Customerbranchid)))

                                if (BranchVerticalID.Count > 0)
                                {
                                    for (int BV = 0; BV < BranchVerticalID.Count; BV++)
                                    {
                                        if (!(RiskCategoryManagement.Exists(processid, subprocessid, riskcreationId, Convert.ToInt32(Frequency), Customerbranchid, Convert.ToInt32(BranchVerticalID[BV]), ActivityID)))
                                        {
                                            string aItem = BranchVerticalID[BV].ToString();

                                            if (aItem.ToString().Contains(","))
                                            {
                                                string[] splitVertical = aItem.ToString().Split(',');

                                                if (splitVertical.Length > 0)
                                                {
                                                    for (int rs = 0; rs < splitVertical.Length; rs++)
                                                    {
                                                        RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                        riskactivitytransaction.ProcessId = processid;
                                                        riskactivitytransaction.SubProcessId = subprocessid;
                                                        riskactivitytransaction.RiskCreationId = riskcreationId;
                                                        riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                        riskactivitytransaction.ControlDescription = ControlDescription;
                                                        riskactivitytransaction.MControlDescription = MControlDescription;
                                                        riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                        riskactivitytransaction.EffectiveDate = dt.Date;
                                                        riskactivitytransaction.Key_Value = KeyID;
                                                        riskactivitytransaction.PrevationControl = PrevationControlID;
                                                        riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                        riskactivitytransaction.Frequency = Frequency;
                                                        riskactivitytransaction.IsDeleted = false;
                                                        riskactivitytransaction.GapDescription = GapDescription;
                                                        riskactivitytransaction.Recommendations = Recommendations;
                                                        riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                        riskactivitytransaction.IPE = IPE;
                                                        riskactivitytransaction.ERPsystem = ERPsystem;
                                                        riskactivitytransaction.FRC = FRC;
                                                        riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                        riskactivitytransaction.TestStrategy = TestStrategy;
                                                        riskactivitytransaction.RiskRating = RiskRating;
                                                        riskactivitytransaction.ControlRating = ControlRating;
                                                        riskactivitytransaction.ProcessScore = processscores;
                                                        riskactivitytransaction.VerticalsId = Convert.ToInt32(splitVertical[rs]);
                                                        riskactivitytransaction.ISICFR_Close = true;
                                                        riskactivitytransaction.KC2 = KC2;
                                                        riskactivitytransaction.ActivityID = ActivityID;
                                                        riskactivitytransaction.Primary_Secondary = 10;
                                                        riskactivitytransaction.ControlOwner = personresponsibleID;

                                                        riskactivitytransactionlist.Add(riskactivitytransaction);

                                                        RiskCategoryManagement.CreateRiskActivityTransaction(riskactivitytransaction);
                                                        riskActivityId = riskactivitytransaction.Id;
                                                    }
                                                    BranchVerticalID.Remove(aItem);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (aItem != "0")
                                                {

                                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                    riskactivitytransaction.ProcessId = processid;
                                                    riskactivitytransaction.SubProcessId = subprocessid;
                                                    riskactivitytransaction.RiskCreationId = riskcreationId;
                                                    riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                    riskactivitytransaction.ControlDescription = ControlDescription;
                                                    riskactivitytransaction.MControlDescription = MControlDescription;
                                                    riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                    riskactivitytransaction.EffectiveDate = dt.Date;
                                                    riskactivitytransaction.Key_Value = KeyID;
                                                    riskactivitytransaction.PrevationControl = PrevationControlID;
                                                    riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                    riskactivitytransaction.Frequency = Frequency;
                                                    riskactivitytransaction.IsDeleted = false;
                                                    riskactivitytransaction.GapDescription = GapDescription;
                                                    riskactivitytransaction.Recommendations = Recommendations;
                                                    riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                    riskactivitytransaction.IPE = IPE;
                                                    riskactivitytransaction.ERPsystem = ERPsystem;
                                                    riskactivitytransaction.FRC = FRC;
                                                    riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                    riskactivitytransaction.TestStrategy = TestStrategy;
                                                    riskactivitytransaction.RiskRating = RiskRating;
                                                    riskactivitytransaction.ControlRating = ControlRating;
                                                    riskactivitytransaction.ProcessScore = processscores;
                                                    riskactivitytransaction.VerticalsId = Convert.ToInt32(aItem);
                                                    riskactivitytransaction.ISICFR_Close = true;
                                                    riskactivitytransaction.KC2 = KC2;
                                                    riskactivitytransaction.ActivityID = ActivityID;
                                                    riskactivitytransaction.Primary_Secondary = 10;
                                                    riskactivitytransactionlist.Add(riskactivitytransaction);
                                                    riskactivitytransaction.ControlOwner = personresponsibleID;
                                                    RiskCategoryManagement.CreateRiskActivityTransaction(riskactivitytransaction);
                                                    riskActivityId = riskactivitytransaction.Id;
                                                }
                                                BranchVerticalID.Remove(aItem);
                                                break;
                                            }
                                        }
                                    }//exists end
                                }
                                if (suucess)
                                {
                                    List<RiskActivityToBeDoneMapping> RiskATBDM = new List<RiskActivityToBeDoneMapping>();
                                    List<RiskActivityToBeDoneMapping> RiskATBDM1 = new List<RiskActivityToBeDoneMapping>();

                                    for (int i = 2; i <= xlrow2; i++)
                                    {
                                        activitytobedone = xlWorksheet.Cells[i, 1].Text.Trim();

                                        rating = xlWorksheet.Cells[i, 2].Text.Trim();

                                        if (rating.ToUpper() == "Major".ToUpper())
                                        {
                                            ratingid = 1;
                                        }
                                        else if (rating.ToUpper() == "Moderate".ToUpper())
                                        {
                                            ratingid = 2;
                                        }
                                        else if (rating.ToUpper() == "Minor".ToUpper())
                                        {
                                            ratingid = 3;
                                        }
                                        else
                                        {
                                            ratingid = 0;
                                        }
                                        auditStepMasterID = RiskCategoryManagement.GetAuditStepMasterID(activitytobedone);
                                        if (auditStepMasterID == 0)
                                        {
                                            AuditStepMaster newASM = new AuditStepMaster()
                                            {
                                                AuditStep = activitytobedone
                                            };
                                            if (RiskCategoryManagement.CreateAuditStepMaster(newASM))
                                                auditStepMasterID = newASM.ID;
                                        }
                                        RiskActivityToBeDoneMapping RATBDM = new RiskActivityToBeDoneMapping();
                                        RATBDM.AuditStepMasterID = auditStepMasterID;
                                        RATBDM.RiskCategoryCreationId = riskcreationId;
                                        RATBDM.ProcessId = processid;
                                        RATBDM.SubProcessId = subprocessid;
                                        RATBDM.RiskActivityId = riskActivityId;
                                        RATBDM.ActivityTobeDone = activitytobedone;
                                        RATBDM.Rating = ratingid;
                                        RATBDM.CustomerBranchID = Customerbranchid;
                                        RATBDM.VerticalID = Convert.ToInt32(VerticalID);
                                        RATBDM.IsActive = true;
                                        RiskATBDM.Add(RATBDM);
                                    }
                                    RiskATBDM1 = RiskATBDM.Where(entry => entry.RiskCategoryCreationId == 0 || entry.RiskActivityId == 0).ToList();
                                    if (RiskATBDM1.Count > 1)
                                    {
                                        suucess = false;
                                        cvSpecialAuditError.IsValid = false;
                                        cvSpecialAuditError.ErrorMessage = "Server Error Occured. Please try again.";
                                    }
                                    else
                                    {
                                        suucess = RiskCategoryManagement.CreateRiskActivityTBDMapping(RiskATBDM);
                                        suucess = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}
