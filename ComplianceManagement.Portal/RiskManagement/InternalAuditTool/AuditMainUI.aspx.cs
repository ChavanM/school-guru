﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditMainUI : System.Web.UI.Page
    {
        public List<InternalAuditTransactionView> MasterInternalAuditTransactionView = new List<InternalAuditTransactionView>();
        public List<InternalControlAuditAssignment> MasterInternalControlAuditAssignment = new List<InternalControlAuditAssignment>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["PID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["SPID"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["RoleID"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                                if (!String.IsNullOrEmpty(Request.QueryString["peroid"]))
                                                    if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                                        if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
                                                        {
                                                            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                                            {
                                                                using (AuditControlEntities entities = new AuditControlEntities())
                                                                {
                                                                    MasterInternalAuditTransactionView = (from row in entities.InternalAuditTransactionViews
                                                                                                          select row).ToList();

                                                                    MasterInternalControlAuditAssignment = (from row in entities.InternalControlAuditAssignments
                                                                                                            select row).ToList();
                                                                }
                                                                int chkRole = Convert.ToInt32(Request.QueryString["RoleID"]);
                                                                int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                                ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + Request.QueryString["PID"] + ";" + Request.QueryString["SPID"] + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];
                                                                ViewState["roleid"] = Convert.ToInt32(Request.QueryString["RoleID"]);
                                                                ViewState["returnUrl2"] = Request.QueryString["returnUrl2"];

                                                                int customerID = -1;
                                                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                                                BindProcess(customerID);

                                                                if (Convert.ToInt32(Request.QueryString["PID"]) != -1)
                                                                {
                                                                    #region
                                                                    ddlProcess.ClearSelection();
                                                                    ddlProcess.SelectedValue = Request.QueryString["PID"].Trim();
                                                                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                                    {
                                                                        if (ddlProcess.SelectedValue != "-1")
                                                                        {
                                                                            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                                                                        }
                                                                    }
                                                                    if (Convert.ToInt32(Request.QueryString["SPID"]) != -1)
                                                                    {
                                                                        ddlSubProcess.ClearSelection();
                                                                        ddlSubProcess.SelectedValue = Request.QueryString["SPID"].Trim();
                                                                    }
                                                                    #endregion
                                                                }
                                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                                bindPageNumber();

                                                                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                                                                {
                                                                    #region
                                                                    string statusidasd = Request.QueryString["Status"].ToString();
                                                                    int statusFlag = 0;
                                                                    if (statusidasd.Equals("NotDone"))
                                                                    {
                                                                        statusFlag = 1;
                                                                    }
                                                                    else if (statusidasd.Equals("Submitted"))
                                                                    {
                                                                        statusFlag = 2;
                                                                    }
                                                                    else if (statusidasd.Equals("TeamReview"))
                                                                    {
                                                                        statusFlag = 4;
                                                                    }
                                                                    else if (statusidasd.Equals("AuditeeReview"))
                                                                    {
                                                                        statusFlag = 6;
                                                                    }
                                                                    else if (statusidasd.Equals("FinalReview"))
                                                                    {
                                                                        statusFlag = 5;
                                                                    }
                                                                    else if (statusidasd.Equals("Closed"))
                                                                    {
                                                                        statusFlag = 3;
                                                                    }
                                                                    if (statusFlag != 0)
                                                                    {
                                                                        ddlFilterStatus.ClearSelection();
                                                                        ddlFilterStatus.Items.FindByValue(statusFlag.ToString()).Selected = true;
                                                                    }
                                                                    if (chkRole == 3)
                                                                    {
                                                                        if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                                                                        {
                                                                            btnAllSavechk.Enabled = false;
                                                                            DropDownListPersonResponsible.Visible = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            btnAllSavechk.Enabled = true;
                                                                            DropDownListPersonResponsible.Visible = false;
                                                                        }
                                                                    }
                                                                    if (chkRole == 4)
                                                                    {
                                                                        bool flagStatus = false;
                                                                        if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                                        {
                                                                            btnAllSavechk.Enabled = false;
                                                                            DropDownListPersonResponsible.Visible = false;
                                                                            flagStatus = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            btnAllSavechk.Enabled = true;
                                                                            DropDownListPersonResponsible.Visible = true;
                                                                            flagStatus = true;
                                                                        }
                                                                        if (flagStatus == true)
                                                                        {
                                                                            bool checkPRFlag = false;
                                                                            checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CBranchID);

                                                                            var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                            var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                            if (checkPRFlag == true)
                                                                            {
                                                                                if (!(statusidasd.Equals("Closed")))
                                                                                {
                                                                                    BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                if (!(statusidasd.Equals("Closed")))
                                                                                {
                                                                                    BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (chkRole == 5)
                                                                    {
                                                                        bool flagStatus = false;
                                                                        if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("Submitted") || statusidasd.Equals("TeamReview") || statusidasd.Equals("AuditeeReview"))
                                                                        {
                                                                            btnAllSavechk.Enabled = false;
                                                                            DropDownListPersonResponsible.Visible = false;
                                                                            flagStatus = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            btnAllSavechk.Enabled = true;
                                                                            DropDownListPersonResponsible.Visible = true;
                                                                            flagStatus = true;
                                                                        }
                                                                        if (flagStatus == true)
                                                                        {
                                                                            bool checkPRFlag = false;
                                                                            checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CBranchID);

                                                                            var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                            var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                            if (checkPRFlag == true)
                                                                            {
                                                                                if (!(statusidasd.Equals("Closed")))
                                                                                {
                                                                                    BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                if (!(statusidasd.Equals("Closed")))
                                                                                {
                                                                                    BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion
                                                                }//SID ENd

                                                                //Page Title
                                                                string Pname = string.Empty;
                                                                string SPname = string.Empty;
                                                                string VName = string.Empty;
                                                                string FY = string.Empty;
                                                                string Period1 = string.Empty;
                                                                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FY"])))
                                                                {
                                                                    FY = "/" + Convert.ToString(Request.QueryString["FY"]);
                                                                }
                                                                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["peroid"])))
                                                                {
                                                                    Period1 = "/" + Convert.ToString(Request.QueryString["peroid"]);
                                                                }
                                                                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                                {
                                                                    if (ddlProcess.SelectedValue != "-1")
                                                                    {
                                                                        Pname = "/" + ddlProcess.SelectedItem.Text;
                                                                    }
                                                                }
                                                                if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                                                                {
                                                                    if (ddlSubProcess.SelectedValue != "-1")
                                                                    {
                                                                        SPname = "/" + ddlSubProcess.SelectedItem.Text;
                                                                    }
                                                                }
                                                                var BrachName = CustomerBranchManagement.GetByID(CBranchID).Name;
                                                                var VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                                                                if (string.IsNullOrEmpty(VerticalName))
                                                                {
                                                                    VName = "/" + VerticalName;
                                                                }
                                                                LblPageDetails.InnerText = BrachName + "/" + FY + Period1 + Pname + SPname + VName;

                                                                //End

                                                                var roles = UserManagementRisk.SP_GETAssigned_AsPerformerORReviewerProcedure(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID), Request.QueryString["FY"].ToString(), Request.QueryString["peroid"].ToString(), Convert.ToInt32(CBranchID), Convert.ToInt32(Request.QueryString["VID"]), Convert.ToInt32(Request.QueryString["AuditID"]));
                                                                if (roles != null)
                                                                {
                                                                    if (roles.Contains(3))
                                                                    {
                                                                        if (!RiskCategoryManagement.AuditClosureCloseExists(Convert.ToInt32(CBranchID), Convert.ToInt32(Request.QueryString["VID"]), Request.QueryString["FY"].ToString(), Request.QueryString["peroid"].ToString(), Convert.ToInt32(Request.QueryString["AuditID"])))
                                                                        {
                                                                            btnAddChecklist.Visible = true;
                                                                        }
                                                                        else
                                                                        {
                                                                            btnAddChecklist.Visible = false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        btnAddChecklist.Visible = false;
                                                                    }
                                                                }
                                                            }
                                                        }
            }
        }
        protected void grdSummaryDetailsAuditCoverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int roleId = Convert.ToInt32(ViewState["roleid"]);
            string statusidasd = Request.QueryString["Status"].ToString();
            int indexOfColumn = 4;
            if (roleId == 3)
            {
                if (statusidasd.Equals("NotDone"))
                {
                    if (e.Row.Cells.Count > indexOfColumn)
                    {
                        e.Row.Cells[indexOfColumn].Visible = false;
                    }
                }
                if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = false;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = false;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = false;
                    }
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = true;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = true;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = true;
                    }
                }
            }
            else if (roleId == 4)
            {
                if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = false;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = false;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = false;
                    }
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = true;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = true;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = true;
                    }
                }
                #region Audittee Status Enable Grid
                int rowCount = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    CheckBox checkAll = (CheckBox)grdSummaryDetailsAuditCoverage.HeaderRow.FindControl("checkAll");
                    String Args = chklnkbutton.CommandArgument.ToString();
                    string[] arg = Args.ToString().Split(',');
                    Label LblObservation = (Label)rw.FindControl("LblObservation");

                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult();

                    MstRiskResult.ATBDId = Convert.ToInt32(arg[0]);
                    MstRiskResult.CustomerBranchId = Convert.ToInt32(arg[1]);
                    MstRiskResult.FinancialYear = arg[2].ToString();
                    MstRiskResult.ForPerid = arg[3].ToString();
                    MstRiskResult.ProcessId = Convert.ToInt32(arg[5]);
                    MstRiskResult.VerticalID = Convert.ToInt32(arg[6]);
                    MstRiskResult.AuditID = Convert.ToInt32(arg[7]);

                    //var AuditteeStatusID = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    //if (AuditteeStatusID == "RS" || AuditteeStatusID == "RA")
                    //{
                    //    if (chkBx != null)
                    //    {
                    //        chkBx.Enabled = false;
                    //    }
                    //    if (checkAll != null)
                    //    {
                    //        checkAll.Enabled = false;
                    //    }
                    //    if (LblObservation != null)
                    //    {
                    //        if (string.IsNullOrEmpty(LblObservation.Text))
                    //        {
                    //            if (chkBx != null)
                    //            {
                    //                chkBx.Enabled = true;
                    //            }
                    //            rowCount++;
                    //        }
                    //    }
                    //}
                    //if (grdSummaryDetailsAuditCoverage.Rows.Count == rowCount)
                    //{
                    //    if (checkAll != null)
                    //    {
                    //        checkAll.Enabled = true;
                    //    }
                    //}
                }
                #endregion
            }
            else if (roleId == 5)
            {
                if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("Submitted") || statusidasd.Equals("AuditeeReview") || statusidasd.Equals("TeamReview"))
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = false;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = false;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = false;
                    }
                    btnAllSavechk.Enabled = false;
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = true;

                        TextBox txtWorkDone = (TextBox)rw.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = true;

                        TextBox txtRemark = (TextBox)rw.FindControl("txtRemark");
                        txtRemark.Enabled = true;
                    }
                    btnAllSavechk.Enabled = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (statusidasd == "TeamReview")
                {
                    int ScheduledOnID = 0;
                    int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    Label txtstepId = (Label)e.Row.FindControl("lblATBDID");
                    Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                    Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                    if (lblScheduledOnID != null)
                    {
                        ScheduledOnID = Convert.ToInt32(lblScheduledOnID.Text);
                    }

                    var User = (from row in MasterInternalAuditTransactionView
                                where row.AuditScheduleOnID == ScheduledOnID
                                && row.AuditID == AuditID
                                && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                    var ListPerformer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 3 && row.AuditID == AuditID select row.UserID).FirstOrDefault();
                    var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 && row.AuditID == AuditID select row.UserID).FirstOrDefault();
                    if (User != null)
                    {
                        if (ListPerformer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "black");
                        }
                        else if (ListReviewer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "red");
                        }
                    }
                }
                else if (statusidasd == "AuditeeReview")
                {
                    int ScheduledOnID = 0;
                    int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    Label txtstepId = (Label)e.Row.FindControl("lblATBDID");
                    Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                    Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                    if (lblScheduledOnID != null)
                    {
                        ScheduledOnID = Convert.ToInt32(lblScheduledOnID.Text);
                    }

                    var User = (from row in MasterInternalAuditTransactionView
                                where row.AuditScheduleOnID == ScheduledOnID
                                && row.AuditID == AuditID
                                && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                select row).OrderByDescending(el => el.Dated).FirstOrDefault();
                    var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                    if (User != null)
                    {
                        if (ListReviewer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "blue");
                        }
                        else
                        {
                            lblcolor.Style.Add("color", "black");
                        }
                    }
                }
            }
        }

        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                List<long> ProcessList = new List<long>();
                if (Convert.ToInt32(Request.QueryString["PID"]) != -1)
                {
                    ProcessList.Add(Convert.ToInt32(Request.QueryString["PID"]));
                }
                else
                {
                    ProcessList = ProcessManagement.GetAllMst_ProcessList(rolelist, Convert.ToInt32(Request.QueryString["AuditID"]), Portal.Common.AuthenticationHelper.UserID);
                }
                var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(Request.QueryString["AuditID"]), Portal.Common.AuthenticationHelper.UserID, ProcessList);
                DropDownListPersonResponsible.Items.Clear();
                DropDownListPersonResponsible.Visible = true;
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(Request.QueryString["AuditID"]), ProcessList);
                if (RoleListInAudit1.Count == 1)
                {
                    #region Sigle Role User
                    if (statusID == 7)
                    {
                        if (ddlFilterStatus.SelectedValue == "6")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (RoleListInAudit.Contains(5))
                                {
                                    if ((st.ID == 4) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if ((st.ID == 4) || (st.ID == 5) || (st.ID == 2)) //|| (st.ID == 6)
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = false;
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "5")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                int chkRole = Convert.ToInt32(Request.QueryString["RoleID"]);
                                if (chkRole == 5)
                                {
                                    if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if ((st.ID == 4) || (st.ID == 6) || (st.ID == 3) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "4")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (RoleListInAudit.Contains(5))
                                {
                                    if ((st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (st.ID == 5 || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = false;
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "2")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                //if ((st.ID == 5) || (st.ID == 3))
                                if (RoleListInAudit.Contains(5))
                                {
                                    if ((st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                    {
                                        if (RoleListInAudit1.Contains(5))
                                        {
                                            if (st.ID == 3 || (st.ID == 2))
                                            {
                                            }
                                            else
                                            {
                                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                            }
                                        }
                                        else
                                        {
                                            if (st.ID == 5 || (st.ID == 2))
                                            {
                                            }
                                            else
                                            {
                                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                            }
                                        }
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = false;
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                                {
                                    DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                                else
                                {
                                    if (!(st.ID == 5))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                            }
                        }
                    }//Personresponsible End
                    else
                    {
                        if (ddlFilterStatus.SelectedValue == "5")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (!(st.ID == 4))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                                {
                                    if ((st.ID == 4) || (st.ID == 7) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (!(st.ID == 7))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Sigle Role User
                    if (statusID == 7)
                    {
                        if (ddlFilterStatus.SelectedValue == "6")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (RoleListInAudit.Contains(4) && RoleListInAudit.Contains(5))
                                {
                                    if ((st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if ((st.ID == 4) || (st.ID == 5) || (st.ID == 2)) //|| (st.ID == 6)
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = true;
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "5")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                int chkRole = Convert.ToInt32(Request.QueryString["RoleID"]);
                                if (chkRole == 5)
                                {
                                    if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if ((st.ID == 4) || (st.ID == 6) || (st.ID == 3) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "4")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (RoleListInAudit.Contains(5) && RoleListInAudit.Contains(4))
                                {
                                    if ((st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (st.ID == 5 || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = true;
                            }
                        }
                        else if (ddlFilterStatus.SelectedValue == "2")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                //if ((st.ID == 5) || (st.ID == 3))
                                if (RoleListInAudit.Contains(5) && RoleListInAudit.Contains(4))
                                {
                                    if ((st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                    {
                                        if (RoleListInAudit1.Contains(5))
                                        {
                                            if (st.ID == 3 || (st.ID == 2))
                                            {
                                            }
                                            else
                                            {
                                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                            }
                                        }
                                        else
                                        {
                                            if (st.ID == 5 || (st.ID == 2))
                                            {
                                            }
                                            else
                                            {
                                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                            }
                                        }
                                    }
                                }
                            }
                            if (RoleListInAudit1.Contains(5))
                            {
                                btnAllSavechk.Enabled = true;
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                                {
                                    DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                                else
                                {
                                    if (!(st.ID == 5))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                            }
                        }
                    }//Personresponsible End
                    else
                    {
                        if (ddlFilterStatus.SelectedValue == "5")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (!(st.ID == 4))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                                {
                                    if ((st.ID == 4) || (st.ID == 7) || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                                else
                                {
                                    if (!(st.ID == 7))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindProcess(int CustomerID)
        {
            try
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                ddlSubProcess.Items.Clear();
                ddlProcess.Items.Clear();
                if (AuditID != -1)
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownPerformerAndReviewer(CustomerID, Portal.Common.AuthenticationHelper.UserID, AuditID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            BindDetailView(ViewState["Arguments"].ToString(), "P");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string financialyear = string.Empty; ;
            string url3 = "";
            string url4 = "";
            int PID = -1;
            int SPID = -1;
            int PageSize = -1;
            int gridpagesize = -1;
            int AuditID = 0;
            int BoPSize = 0;

            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
            {
                url3 = Request.QueryString["returnUrl2"];

                //string asd = Request.QueryString["chkPID"].ToString();

                if (!String.IsNullOrEmpty(Request.QueryString["chkPID"]))
                {
                    PID = Convert.ToInt32(Request.QueryString["chkPID"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["chkSPID"]))
                {
                    SPID = Convert.ToInt32(Request.QueryString["chkSPID"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]))
                {
                    PageSize = Convert.ToInt32(Request.QueryString["PageSize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["gridpagesize"]))
                {
                    gridpagesize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["BoPSize"]))
                {
                    BoPSize = Convert.ToInt32(Request.QueryString["BoPSize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialyear = Convert.ToString(Request.QueryString["FY"]);
                }

                if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
                {
                    url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"] + "&PID=" + PID + "&SPID=" + SPID + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&AuditID=" + AuditID + "&FY=" + financialyear;
                }

                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?" + url3.Replace("@", "=") + url4);
            }

        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    if (ViewState["roleid"] != null)
                    {
                        if (Convert.ToInt32(ViewState["roleid"]) == 3)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ");", true);
                        else if (Convert.ToInt32(ViewState["roleid"]) == 4 || Convert.ToInt32(ViewState["roleid"]) == 5)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "ShowReviewerDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }

        public bool ATBDVisibleorNot(int? StatusID)
        {
            if (ViewState["roleid"] != null)
            {
                if (StatusID == null && Convert.ToInt32(ViewState["roleid"]) == 4)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        protected void btnAllsave_click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {
                    string workDone = string.Empty;
                    string remark = string.Empty;
                    string remarkreview = string.Empty;

                    if (Convert.ToInt32(ViewState["roleid"]) == 3)
                    {
                        #region performer
                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                            if (chkBx != null && chkBx.Checked)
                            {
                                long roleid = 3;
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                workDone = ((TextBox)rw.FindControl("txtWorkDone")).Text;
                                remark = ((TextBox)rw.FindControl("txtRemark")).Text;

                                LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                String Args = chklnkbutton.CommandArgument.ToString();
                                string[] arg = Args.ToString().Split(',');

                                //string chk = "asd";

                                int ATBDid = Convert.ToInt32(arg[0]);
                                int customerbranchid = Convert.ToInt32(arg[1]);
                                string FinancialYear = arg[2].ToString();
                                string period = arg[3].ToString();
                                // int ProcessId = Convert.ToInt32(arg[4]);
                                int verticalId = Convert.ToInt32(arg[6]);
                                //string AuditStep = arg[7].ToString();
                                int auditid = Convert.ToInt32(arg[7]);
                                var aaa = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinancialYear, period, Convert.ToInt32(customerbranchid), Convert.ToInt32(ATBDid), Convert.ToInt32(verticalId), auditid);
                                if (aaa != null)
                                {
                                    var riskactobedonemappingdetails = RiskCategoryManagement.GetRiskActivityToBeDoneMappingByInstanceID(ATBDid);
                                    #region
                                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                    {
                                        AuditScheduleOnID = aaa.ID,
                                        ProcessId = aaa.ProcessId,
                                        FinancialYear = FinancialYear,
                                        ForPerid = period,
                                        CustomerBranchId = Convert.ToInt32(customerbranchid),
                                        IsDeleted = false,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        ATBDId = Convert.ToInt32(ATBDid),
                                        RoleID = roleid,
                                        InternalAuditInstance = aaa.InternalAuditInstance,
                                        VerticalID = verticalId,
                                        AuditSteps = riskactobedonemappingdetails.ActivityTobeDone,
                                        AuditObjective = riskactobedonemappingdetails.AuditObjective,
                                        AuditID = auditid,
                                        //AnnexueTitle =
                                    };

                                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                                    {
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        AuditScheduleOnID = aaa.ID,
                                        FinancialYear = FinancialYear,
                                        CustomerBranchId = Convert.ToInt32(customerbranchid),
                                        InternalAuditInstance = aaa.InternalAuditInstance,
                                        ProcessId = aaa.ProcessId,
                                        SubProcessId = -1,
                                        ForPeriod = period,
                                        ATBDId = Convert.ToInt32(ATBDid),
                                        RoleID = roleid,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        VerticalID = Convert.ToInt32(verticalId),
                                        Dated = DateTime.Now,
                                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                        AuditID = auditid,
                                    };


                                    #endregion

                                    var MstRiskResultchk = RiskCategoryManagement.GetInternalControlAuditResultbyID(aaa.ID, Convert.ToInt32(ATBDid), verticalId, auditid);
                                    if (MstRiskResultchk != null)
                                    {
                                        #region
                                        if (string.IsNullOrEmpty(MstRiskResultchk.AuditObjective))
                                        {
                                            MstRiskResult.AuditObjective = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.AuditObjective = MstRiskResultchk.AuditObjective;
                                        }

                                        if (string.IsNullOrEmpty(MstRiskResultchk.AuditSteps))
                                            MstRiskResult.AuditSteps = "";
                                        else
                                            MstRiskResult.AuditSteps = MstRiskResultchk.AuditSteps;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.AnalysisToBePerofrmed))
                                            MstRiskResult.AnalysisToBePerofrmed = "";
                                        else
                                            MstRiskResult.AnalysisToBePerofrmed = MstRiskResultchk.AnalysisToBePerofrmed;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                            MstRiskResult.ProcessWalkthrough = "";
                                        else
                                            MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ActivityToBeDone))
                                            MstRiskResult.ActivityToBeDone = "";
                                        else
                                            MstRiskResult.ActivityToBeDone = MstRiskResultchk.ActivityToBeDone;
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                            MstRiskResult.Population = "";
                                        else
                                            MstRiskResult.Population = MstRiskResultchk.Population;
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                            MstRiskResult.Sample = "";
                                        else
                                            MstRiskResult.Sample = MstRiskResultchk.Sample;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ObservationNumber))
                                        {
                                            MstRiskResult.ObservationNumber = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.ObservationTitle))
                                        {
                                            MstRiskResult.ObservationTitle = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Observation))
                                        {
                                            MstRiskResult.Observation = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Observation = MstRiskResultchk.Observation;
                                        }
                                        if (MstRiskResultchk.ISACPORMIS != null)
                                        {
                                            MstRiskResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                        }
                                        else
                                        {
                                            MstRiskResult.ISACPORMIS = null;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Risk))
                                        {
                                            MstRiskResult.Risk = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Risk = MstRiskResultchk.Risk;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.RootCost))
                                        {
                                            MstRiskResult.RootCost = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.RootCost = MstRiskResultchk.RootCost;
                                        }

                                        if (MstRiskResultchk.AuditScores == null)
                                            MstRiskResult.AuditScores = null;
                                        else
                                            MstRiskResult.AuditScores = Convert.ToDecimal(MstRiskResultchk.AuditScores);

                                        if (MstRiskResultchk.FinancialImpact == null)
                                        {
                                            MstRiskResult.FinancialImpact = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Recomendation))
                                        {
                                            MstRiskResult.Recomendation = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Recomendation = MstRiskResultchk.Recomendation;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.ManagementResponse))
                                        {
                                            MstRiskResult.ManagementResponse = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                            MstRiskResult.FixRemark = "";
                                        else
                                            MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;

                                        DateTime dt = new DateTime();
                                        if (MstRiskResultchk.TimeLine != null)
                                        {
                                            string s = MstRiskResultchk.TimeLine != null ? MstRiskResultchk.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                                            dt = DateTime.ParseExact(s, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            MstRiskResult.TimeLine = dt.Date;
                                        }
                                        else
                                        {
                                            MstRiskResult.TimeLine = null;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.PersonResponsible)))
                                        {
                                            if (MstRiskResultchk.PersonResponsible == -1)
                                            {
                                                MstRiskResult.PersonResponsible = null;
                                                transaction.PersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                transaction.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.Owner)))
                                        {
                                            if (MstRiskResultchk.Owner == -1)
                                            {
                                                MstRiskResult.Owner = null;
                                                transaction.Owner = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                transaction.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationRating)))
                                        {
                                            if (MstRiskResultchk.ObservationRating == -1)
                                            {
                                                transaction.ObservatioRating = null;
                                                MstRiskResult.ObservationRating = null;
                                            }
                                            else
                                            {
                                                transaction.ObservatioRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                MstRiskResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                            }
                                        }

                                        // added by sagar more on 10-01-2020
                                        if (string.IsNullOrEmpty(MstRiskResultchk.AnnexueTitle))
                                            MstRiskResult.AnnexueTitle = null;
                                        else
                                            MstRiskResult.AnnexueTitle = MstRiskResultchk.AnnexueTitle;

                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationCategory)))
                                        {
                                            if (MstRiskResultchk.ObservationCategory == -1)
                                            {
                                                transaction.ObservationCategory = null;
                                                MstRiskResult.ObservationCategory = null;
                                            }
                                            else
                                            {
                                                transaction.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                MstRiskResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationSubCategory)))
                                        {
                                            if (MstRiskResultchk.ObservationSubCategory == -1)
                                            {
                                                transaction.ObservationSubCategory = null;
                                                MstRiskResult.ObservationSubCategory = null;
                                            }
                                            else
                                            {
                                                transaction.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                MstRiskResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                            }
                                        }

                                        //Code added by Sushant
                                        if (MstRiskResultchk.BodyContent == "")
                                        {
                                            MstRiskResult.BodyContent = "";
                                            transaction.BodyContent = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.BodyContent = MstRiskResultchk.BodyContent;
                                            transaction.BodyContent = MstRiskResultchk.BodyContent;
                                        }
                                        if (MstRiskResultchk.UHComment == "")
                                        {
                                            MstRiskResult.UHComment = "";
                                            transaction.UHComment = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.UHComment = MstRiskResultchk.UHComment;
                                            transaction.UHComment = MstRiskResultchk.UHComment;
                                        }

                                        if (MstRiskResultchk.PRESIDENTComment == "")
                                        {
                                            MstRiskResult.PRESIDENTComment = "";
                                            transaction.PRESIDENTComment = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                            transaction.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                        }

                                        if (MstRiskResultchk.UHPersonResponsible != null)
                                        {
                                            MstRiskResult.UHPersonResponsible = null;
                                            transaction.UHPersonResponsible = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                            transaction.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                        }

                                        if (MstRiskResultchk.UHPersonResponsible != null)
                                        {
                                            MstRiskResult.PRESIDENTPersonResponsible = null;
                                            transaction.PRESIDENTPersonResponsible = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                            transaction.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                        }
                                        //End

                                        transaction.StatusId = MstRiskResultchk.AStatusId;
                                        if (MstRiskResultchk.AStatusId == 4)
                                        {
                                            transaction.StatusId = 4;
                                            MstRiskResult.AStatusId = 4;
                                            transaction.Remarks = "Audit Steps Under Team Review.";
                                            MstRiskResult.AuditeeResponse = "PT";
                                        }
                                        #endregion
                                    }
                                    else if (MstRiskResultchk == null)
                                    {
                                        using (AuditControlEntities entities = new AuditControlEntities())
                                        {
                                            var data = entities.Sp_GetPreviousAuditProcessWalkthrough(ATBDid, customerbranchid, verticalId, auditid).FirstOrDefault();
                                            if (data != null)
                                            {
                                                if (string.IsNullOrEmpty(data.ProcessWalkthrough))
                                                    MstRiskResult.ProcessWalkthrough = "";
                                                else
                                                    MstRiskResult.ProcessWalkthrough = data.ProcessWalkthrough.Trim();
                                            }
                                        }
                                        transaction.Remarks = "Audit Steps Submitted.";
                                        transaction.StatusId = 2;
                                        MstRiskResult.AStatusId = 2;
                                        MstRiskResult.AuditeeResponse = "PS";
                                    }

                                    if (workDone.Trim() == "")
                                    {
                                        MstRiskResult.ActivityToBeDone = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.ActivityToBeDone = workDone.ToString();
                                    }
                                    if (remark.Trim() == "")
                                    {
                                        MstRiskResult.FixRemark = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.FixRemark = remark.ToString();
                                    }

                                    bool Success1 = false;
                                    bool Success2 = false;
                                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    }

                                    if (RiskCategoryManagement.InternalAuditTxnExists(transaction) && Convert.ToInt32(ddlFilterStatus.SelectedValue) != 4)
                                    {
                                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        transaction.UpdatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatus(transaction);
                                    }
                                    else
                                    {
                                        transaction.CreatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                    }
                                    if (Success1 && Success2)
                                    {
                                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Step Submitted Successfully.";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                    }

                                }//aaa end
                            }
                        }
                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                        #endregion
                    }
                    else if (Convert.ToInt32(ViewState["roleid"]) == 4 || Convert.ToInt32(ViewState["roleid"]) == 5)
                    {
                        #region
                        int Count = 0;
                        string ErrorMessageList = string.Empty;
                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                            if (chkBx != null && chkBx.Checked)
                            {
                                var lblControlNo = ((Label)rw.FindControl("lblControlNo")).Text;
                                workDone = ((TextBox)rw.FindControl("txtWorkDone")).Text;
                                remarkreview = ((TextBox)rw.FindControl("txtRemark")).Text;

                                LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                String Args = chklnkbutton.CommandArgument.ToString();
                                string[] arg = Args.ToString().Split(',');

                                int ATBDid = Convert.ToInt32(arg[0]);
                                int customerbranchid = Convert.ToInt32(arg[1]);
                                string FinancialYear = arg[2].ToString();
                                string period = arg[3].ToString();
                                // int ProcessId = Convert.ToInt32(arg[4]);
                                int verticalId = Convert.ToInt32(arg[6]);
                                //string AuditStep = arg[7].ToString();
                                int auditid = Convert.ToInt32(arg[7]);
                                bool Flag = false;
                                long roleid = 4;
                                bool SubmitValue = false;
                                List<long> ProcessList = new List<long>();
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                var aaa = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinancialYear, period, Convert.ToInt32(customerbranchid), Convert.ToInt32(ATBDid), Convert.ToInt32(verticalId), auditid);
                                if (aaa != null)
                                {
                                    ProcessList.Add(aaa.ProcessId);
                                    var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(auditid, Portal.Common.AuthenticationHelper.UserID, ProcessList);
                                    var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(auditid, ProcessList);

                                    if (DropDownListPersonResponsible.SelectedValue != "")
                                    {
                                        string Currentstatus = Convert.ToString(Request.QueryString["Status"]);
                                        if (RoleListInAudit.Count() == 3 && DropDownListPersonResponsible.SelectedItem.Text == "Closed" && (Currentstatus == "Submitted" || Currentstatus == "TeamReview" || Currentstatus == "AuditeeReview"))
                                        {
                                            SubmitValue = false;
                                        }
                                        else if (RoleListInAudit.Count() == 2 && DropDownListPersonResponsible.SelectedItem.Text == "Final Review")
                                        {
                                            SubmitValue = false;
                                        }
                                        else
                                        {
                                            SubmitValue = true;
                                        }
                                        if (SubmitValue)
                                        {
                                            #region
                                            int StatusID = 0;
                                            StatusID = Convert.ToInt32(DropDownListPersonResponsible.SelectedValue);
                                            InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                            {
                                                AuditScheduleOnID = aaa.ID,
                                                ProcessId = aaa.ProcessId,
                                                FinancialYear = FinancialYear,
                                                ForPerid = period,
                                                CustomerBranchId = Convert.ToInt32(customerbranchid),
                                                IsDeleted = false,
                                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                ATBDId = Convert.ToInt32(ATBDid),
                                                RoleID = roleid,
                                                InternalAuditInstance = aaa.InternalAuditInstance,
                                                VerticalID = Convert.ToInt32(verticalId),
                                                AuditID = auditid,
                                            };
                                            InternalAuditTransaction transaction = new InternalAuditTransaction()
                                            {
                                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                                AuditScheduleOnID = aaa.ID,
                                                FinancialYear = FinancialYear,
                                                CustomerBranchId = Convert.ToInt32(customerbranchid),
                                                InternalAuditInstance = aaa.InternalAuditInstance,
                                                ProcessId = aaa.ProcessId,
                                                ForPeriod = period,
                                                ATBDId = Convert.ToInt32(ATBDid),
                                                RoleID = roleid,
                                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                VerticalID = Convert.ToInt32(verticalId),
                                                Dated = DateTime.Now,
                                                AuditID = auditid,
                                                SubProcessId = -1,
                                            };
                                            //added by Sushant
                                            ComplianceManagement.Business.DataRisk.AuditClosure AuditClosureResult = new ComplianceManagement.Business.DataRisk.AuditClosure()
                                            {
                                                ProcessId = aaa.ProcessId,
                                                FinancialYear = FinancialYear,
                                                ForPeriod = period,
                                                CustomerbranchId = Convert.ToInt32(customerbranchid),
                                                ACStatus = 1,
                                                AuditCommiteRemark = "",
                                                ATBDId = Convert.ToInt32(ATBDid),
                                                VerticalID = Convert.ToInt32(verticalId),
                                                AuditCommiteFlag = 0,
                                                AuditID = auditid,
                                            };

                                            if (DropDownListPersonResponsible.SelectedItem.Text == "Closed")
                                            {
                                                transaction.StatusId = 3;
                                                MstRiskResult.AStatusId = 3;
                                                remark = "Audit Steps Closed.";
                                                MstRiskResult.AuditeeResponse = "AC";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Team Review")
                                            {
                                                transaction.StatusId = 4;
                                                MstRiskResult.AStatusId = 4;
                                                remark = "Audit Steps Under Team Review.";
                                                MstRiskResult.AuditeeResponse = "RT";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Final Review")
                                            {
                                                transaction.StatusId = 5;
                                                MstRiskResult.AStatusId = 5;
                                                remark = "Audit Steps Under Final Review.";
                                                MstRiskResult.AuditeeResponse = "RF";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Auditee Review")
                                            {
                                                if (MstRiskResult != null)
                                                {
                                                    var AuditteeStatusID = RiskCategoryManagement.GetLatestStatusOfAudittee(MstRiskResult);
                                                    if (AuditteeStatusID == 6)
                                                    {
                                                        MstRiskResult.AuditeeResponse = "RA";
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.AuditeeResponse = "RS";
                                                    }
                                                }
                                                transaction.StatusId = 6;
                                                MstRiskResult.AStatusId = 6;
                                                remark = "Audit Steps Under Auditee Review.";
                                            }
                                            transaction.Remarks = remark.Trim();
                                            #endregion

                                            var MstRiskResultchk = RiskCategoryManagement.GetInternalControlAuditResultbyID(aaa.ID, Convert.ToInt32(ATBDid), verticalId, auditid);
                                            if (MstRiskResultchk != null)
                                            {
                                                //if (DropDownListPersonResponsible.SelectedItem.Text == "Closed")
                                                //{
                                                //    if (!string.IsNullOrEmpty(MstRiskResultchk.Observation))
                                                //    {
                                                //        if (string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.TimeLine)))
                                                //        {
                                                //            ErrorMessageList += lblControlNo + ",";
                                                //            cvDuplicateEntry.IsValid = false;
                                                //            cvDuplicateEntry.ErrorMessage = "Please Enter TimeLine.";
                                                //            break;
                                                //        }
                                                //    }
                                                //}
                                                #region
                                                if (string.IsNullOrEmpty(MstRiskResultchk.AuditObjective))
                                                    MstRiskResult.AuditObjective = null;
                                                else
                                                    MstRiskResult.AuditObjective = MstRiskResultchk.AuditObjective;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.AuditSteps))
                                                    MstRiskResult.AuditSteps = "";
                                                else
                                                    MstRiskResult.AuditSteps = MstRiskResultchk.AuditSteps;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.AnalysisToBePerofrmed))
                                                    MstRiskResult.AnalysisToBePerofrmed = "";
                                                else
                                                    MstRiskResult.AnalysisToBePerofrmed = MstRiskResultchk.AnalysisToBePerofrmed;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                                    MstRiskResult.ProcessWalkthrough = "";
                                                else
                                                    MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.ActivityToBeDone))
                                                    MstRiskResult.ActivityToBeDone = "";
                                                else
                                                    MstRiskResult.ActivityToBeDone = MstRiskResultchk.ActivityToBeDone;
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                                    MstRiskResult.Population = "";
                                                else
                                                    MstRiskResult.Population = MstRiskResultchk.Population;
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                                    MstRiskResult.Sample = "";
                                                else
                                                    MstRiskResult.Sample = MstRiskResultchk.Sample;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.ObservationNumber))
                                                {
                                                    MstRiskResult.ObservationNumber = "";
                                                    AuditClosureResult.ObservationNumber = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                                    AuditClosureResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.ObservationTitle))
                                                {
                                                    MstRiskResult.ObservationTitle = "";
                                                    AuditClosureResult.ObservationTitle = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                                    AuditClosureResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Observation))
                                                {
                                                    MstRiskResult.Observation = "";
                                                    AuditClosureResult.Observation = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.Observation = MstRiskResultchk.Observation;
                                                    AuditClosureResult.Observation = MstRiskResultchk.Observation;
                                                }
                                                if (MstRiskResultchk.ISACPORMIS != null)
                                                {
                                                    MstRiskResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                                    AuditClosureResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                                }
                                                else
                                                {
                                                    MstRiskResult.ISACPORMIS = null;
                                                    AuditClosureResult.ISACPORMIS = null;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Risk))
                                                {
                                                    MstRiskResult.Risk = "";
                                                    AuditClosureResult.Risk = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.Risk = MstRiskResultchk.Risk;
                                                    AuditClosureResult.Risk = MstRiskResultchk.Risk;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.RootCost))
                                                {
                                                    MstRiskResult.RootCost = null;
                                                    AuditClosureResult.RootCost = null;
                                                }
                                                else
                                                {
                                                    MstRiskResult.RootCost = MstRiskResultchk.RootCost;
                                                    AuditClosureResult.RootCost = MstRiskResultchk.RootCost;
                                                }

                                                if (MstRiskResultchk.AuditScores == null)
                                                    MstRiskResult.AuditScores = null;
                                                else
                                                    MstRiskResult.AuditScores = Convert.ToDecimal(MstRiskResultchk.AuditScores);

                                                if (MstRiskResultchk.FinancialImpact == null)
                                                {
                                                    MstRiskResult.FinancialImpact = null;
                                                    AuditClosureResult.FinancialImpact = null;
                                                }
                                                else
                                                {
                                                    MstRiskResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                                    AuditClosureResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Recomendation))
                                                {
                                                    MstRiskResult.Recomendation = "";
                                                    AuditClosureResult.Recomendation = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.Recomendation = MstRiskResultchk.Recomendation;
                                                    AuditClosureResult.Recomendation = MstRiskResultchk.Recomendation;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.ManagementResponse))
                                                {
                                                    MstRiskResult.ManagementResponse = "";
                                                    AuditClosureResult.ManagementResponse = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                                    AuditClosureResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                                    MstRiskResult.FixRemark = "";
                                                else
                                                    MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;

                                                if (string.IsNullOrEmpty(MstRiskResultchk.AnnexueTitle))
                                                {
                                                    MstRiskResult.AnnexueTitle = "";
                                                    AuditClosureResult.AnnexueTitle = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.AnnexueTitle = MstRiskResultchk.AnnexueTitle;
                                                    AuditClosureResult.AnnexueTitle = MstRiskResultchk.AnnexueTitle;
                                                }

                                                DateTime dt = new DateTime();
                                                if (MstRiskResultchk.TimeLine != null)
                                                {
                                                    string s = MstRiskResultchk.TimeLine != null ? MstRiskResultchk.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                                                    dt = DateTime.ParseExact(s, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    MstRiskResult.TimeLine = dt.Date;
                                                    AuditClosureResult.TimeLine = dt.Date;
                                                }
                                                else
                                                {
                                                    MstRiskResult.TimeLine = null;
                                                    AuditClosureResult.TimeLine = null;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.PersonResponsible)))
                                                {
                                                    if (MstRiskResultchk.PersonResponsible == -1)
                                                    {
                                                        MstRiskResult.PersonResponsible = null;
                                                        transaction.PersonResponsible = null;
                                                        AuditClosureResult.PersonResponsible = null;
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                        transaction.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                        AuditClosureResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.Owner)))
                                                {
                                                    if (MstRiskResultchk.Owner == -1)
                                                    {
                                                        MstRiskResult.Owner = null;
                                                        transaction.Owner = null;
                                                        AuditClosureResult.Owner = null;
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                        transaction.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                        AuditClosureResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationRating)))
                                                {
                                                    if (MstRiskResultchk.ObservationRating == -1)
                                                    {
                                                        transaction.ObservatioRating = null;
                                                        MstRiskResult.ObservationRating = null;
                                                        AuditClosureResult.ObservationRating = null;
                                                    }
                                                    else
                                                    {
                                                        transaction.ObservatioRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                        MstRiskResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                        AuditClosureResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationCategory)))
                                                {
                                                    if (MstRiskResultchk.ObservationCategory == -1)
                                                    {
                                                        transaction.ObservationCategory = null;
                                                        MstRiskResult.ObservationCategory = null;
                                                        AuditClosureResult.ObservationCategory = null;
                                                    }
                                                    else
                                                    {
                                                        transaction.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                        MstRiskResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                        AuditClosureResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationSubCategory)))
                                                {
                                                    if (MstRiskResultchk.ObservationSubCategory == -1)
                                                    {
                                                        transaction.ObservationSubCategory = null;
                                                        MstRiskResult.ObservationSubCategory = null;
                                                        AuditClosureResult.ObservationSubCategory = null;
                                                    }
                                                    else
                                                    {
                                                        transaction.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                        MstRiskResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                        AuditClosureResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                    }
                                                }
                                                #endregion
                                            }

                                            if (remarkreview.Trim() == "")
                                            {
                                                MstRiskResult.FixRemark = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.FixRemark = remarkreview.ToString();
                                            }
                                            if (workDone.Trim() == "")
                                            {
                                                MstRiskResult.ActivityToBeDone = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ActivityToBeDone = workDone.ToString();
                                            }

                                            //Code added by Sushant
                                            if (MstRiskResultchk.BodyContent == "")
                                            {
                                                MstRiskResult.BodyContent = "";
                                                transaction.BodyContent = "";
                                                AuditClosureResult.BodyContent = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.BodyContent = MstRiskResultchk.BodyContent;
                                                transaction.BodyContent = MstRiskResultchk.BodyContent;
                                                AuditClosureResult.BodyContent = MstRiskResultchk.BodyContent;
                                            }
                                            if (MstRiskResultchk.UHComment == "")
                                            {
                                                MstRiskResult.UHComment = "";
                                                transaction.UHComment = "";
                                                AuditClosureResult.UHComment = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.UHComment = MstRiskResultchk.UHComment;
                                                transaction.UHComment = MstRiskResultchk.UHComment;
                                                AuditClosureResult.UHComment = MstRiskResultchk.UHComment;
                                            }
                                            if (!string.IsNullOrEmpty(MstRiskResultchk.BriefObservation))
                                            {
                                                MstRiskResult.BriefObservation = MstRiskResultchk.BriefObservation;
                                                transaction.BriefObservation = MstRiskResultchk.BriefObservation;
                                                AuditClosureResult.BriefObservation = MstRiskResultchk.BriefObservation;
                                            }
                                            else
                                            {
                                                MstRiskResult.BriefObservation = null;
                                                transaction.BriefObservation = null;
                                                AuditClosureResult.BriefObservation = null;
                                            }
                                            if (!string.IsNullOrEmpty(MstRiskResultchk.ObjBackground))
                                            {
                                                MstRiskResult.ObjBackground = MstRiskResultchk.ObjBackground;
                                                transaction.ObjBackground = MstRiskResultchk.ObjBackground;
                                                AuditClosureResult.ObjBackground = MstRiskResultchk.ObjBackground;
                                            }
                                            else
                                            {
                                                MstRiskResult.ObjBackground = null;
                                                transaction.ObjBackground = null;
                                                AuditClosureResult.ObjBackground = null;
                                            }
                                            if (MstRiskResultchk.DefeciencyType != null)
                                            {
                                                MstRiskResult.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                                transaction.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                                AuditClosureResult.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                            }
                                            else
                                            {
                                                MstRiskResult.DefeciencyType = null;
                                                transaction.DefeciencyType = null;
                                                AuditClosureResult.DefeciencyType = null;
                                            }

                                            if (MstRiskResultchk.PRESIDENTComment == "")
                                            {
                                                MstRiskResult.PRESIDENTComment = "";
                                                transaction.PRESIDENTComment = "";
                                                AuditClosureResult.PRESIDENTComment = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                                transaction.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                                AuditClosureResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                            }

                                            if (MstRiskResultchk.UHPersonResponsible != null)
                                            {
                                                MstRiskResult.UHPersonResponsible = null;
                                                transaction.UHPersonResponsible = null;
                                                AuditClosureResult.UHPersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                                transaction.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                                AuditClosureResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                            }

                                            if (MstRiskResultchk.UHPersonResponsible != null)
                                            {
                                                MstRiskResult.PRESIDENTPersonResponsible = null;
                                                transaction.PRESIDENTPersonResponsible = null;
                                                AuditClosureResult.PRESIDENTPersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                                transaction.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                                AuditClosureResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                            }
                                            //End

                                            bool Success1 = false;
                                            bool Success2 = false;
                                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                            {
                                                if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                                                {
                                                    if (ddlFilterStatus.SelectedValue == "5")
                                                    {
                                                        if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                                        {
                                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult, 5);
                                                        }
                                                        else
                                                        {
                                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.CreatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (MstRiskResult.AStatusId == 6)
                                                        {
                                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.CreatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                        }
                                                        else
                                                        {
                                                            if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                                            {
                                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                                            }
                                                            else
                                                            {
                                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                                    {
                                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        MstRiskResult.CreatedOn = DateTime.Now;
                                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                            }
                                            if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                            {
                                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                                {
                                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.CreatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                                }
                                                else
                                                {
                                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.UpdatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewer(transaction);
                                                }
                                            }
                                            else
                                            {
                                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.CreatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                            }
                                            if (Success1 && Success2)
                                            {
                                                if (DropDownListPersonResponsible.SelectedItem.Text == "Closed")
                                                {
                                                    #region  AuditClosure 
                                                    int customerID = -1;
                                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                    RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                    Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                    MstRiskResult.ForPerid, MstRiskResult.FinancialYear, auditid);

                                                    using (AuditControlEntities entities = new AuditControlEntities())
                                                    {

                                                        var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                              where row.ProcessId == MstRiskResult.ProcessId
                                                                              && row.FinancialYear == MstRiskResult.FinancialYear
                                                                              && row.ForPerid == MstRiskResult.ForPerid
                                                                              && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                              && row.UserID == MstRiskResult.UserID
                                                                              && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                              && row.VerticalID == MstRiskResult.VerticalID
                                                                              && row.AuditID == MstRiskResult.AuditID
                                                                              select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                        if (RecordtoUpdate != null)
                                                        {
                                                            AuditClosureResult.ResultID = RecordtoUpdate;
                                                            AuditClosureResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            AuditClosureResult.CreatedOn = DateTime.Now;
                                                            RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Audit Step Submitted Successfully.";
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "You don't have access to change status into " + DropDownListPersonResponsible.SelectedItem.Text + ".";
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        //Code added by Sushant
                        #region Audit Close 
                        if (DropDownListPersonResponsible.SelectedItem.Text == "Closed")
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["peroid"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                            {
                                                if (RiskCategoryManagement.CheckAllStepClosed(Convert.ToInt64(Request.QueryString["AuditID"])))
                                                {
                                                    AuditClosureClose auditclosureclose = new AuditClosureClose();
                                                    auditclosureclose.CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                                                    auditclosureclose.CustomerBranchId = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                    auditclosureclose.FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                                                    auditclosureclose.ForPeriod = Convert.ToString(Request.QueryString["peroid"]);
                                                    auditclosureclose.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    auditclosureclose.CreatedDate = DateTime.Today.Date;
                                                    auditclosureclose.ACCStatus = 1;
                                                    auditclosureclose.VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                                                    auditclosureclose.AuditId = Convert.ToInt64(Request.QueryString["AuditID"]);
                                                    if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, Convert.ToInt32(Request.QueryString["AuditID"])))
                                                    {
                                                        RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                                    }
                                                }
                                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    //cvDuplicateEntry
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one step.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowObservation(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string Observation = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                Observation = MstRiskResult.Observation;
            }
            return Observation;
        }

        public string ShowWorkDone(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string workdone = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                workdone = MstRiskResult.ActivityToBeDone;
            }
            return workdone;
        }

        public string ShowRemark(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string remarks = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                remarks = MstRiskResult.FixRemark;
            }
            return remarks;
        }

        private void BindDetailView(string Arguments, String IFlag)
        {
            try
            {
                int ProcessId = -1;
                int Subprocessid = -1;
                int BranchID = -1;
                int roleid = -1;
                string financialyear = String.Empty;
                string filter = String.Empty;
                string period = string.Empty;
                int Veritcalid = -1;
                int InstanceID = -1;
                long AuditID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int userid = -1;
                userid = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                string[] arg = Arguments.ToString().Split(';');

                if (arg.Length > 0)
                {
                    InstanceID = Convert.ToInt32(arg[1]);
                    ProcessId = Convert.ToInt32(arg[3]);
                    Subprocessid = Convert.ToInt32(arg[4]);
                    BranchID = Convert.ToInt32(arg[5]);
                    Veritcalid = Convert.ToInt32(arg[6]);
                    period = arg[7].ToString();
                    financialyear = arg[8].ToString();


                    List<int> statusIds = new List<int>();
                    List<int?> statusNullableIds = new List<int?>();

                    filter = arg[0].Trim();

                    if (arg[0].Trim().Equals("NotDone"))
                    {
                        statusIds.Add(-1);
                    }
                    else if (arg[0].Trim().Equals("Submitted"))
                    {
                        statusIds.Add(2);
                    }
                    else if (arg[0].Trim().Equals("TeamReview"))
                    {
                        statusIds.Add(4);
                    }
                    else if (arg[0].Trim().Equals("Closed"))
                    {
                        statusIds.Add(3);
                    }
                    else if (arg[0].Trim().Equals("AuditeeReview"))
                    {
                        statusIds.Add(6);
                    }
                    else if (arg[0].Trim().Equals("FinalReview"))
                    {
                        statusIds.Add(5);
                    }
                    else
                    {
                        statusIds.Add(1);
                        statusIds.Add(4);
                        statusIds.Add(2);
                        statusIds.Add(3);
                        statusIds.Add(5);
                        statusIds.Add(6);
                    }

                    if (ViewState["roleid"] != null)
                    {
                        if (Convert.ToInt32(ViewState["roleid"]) == 3)
                            roleid = 3;
                        else if (Convert.ToInt32(ViewState["roleid"]) == 4)
                            roleid = 4;
                        else if (Convert.ToInt32(ViewState["roleid"]) == 5)
                            roleid = 5;
                    }

                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt64(Request.QueryString["AuditID"]);
                    }
                    if (AuditID != -1 && AuditID != 0)
                    {
                        if (IFlag == "P")
                        {
                            var detailView = InternalControlManagementDashboardRisk.GetAuditDetailUserWise(customerID, BranchID, financialyear, ProcessId, Subprocessid, userid, roleid, Veritcalid, InstanceID, statusIds, statusNullableIds, filter, period, AuditID);
                            grdSummaryDetailsAuditCoverage.DataSource = detailView;
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetailsAuditCoverage.DataBind();
                        }
                        else
                        {
                            var detailView = InternalControlManagementDashboardRisk.GetAuditDetailUserWise(customerID, BranchID, financialyear, ProcessId, Subprocessid, userid, roleid, Veritcalid, InstanceID, statusIds, statusNullableIds, filter, period, AuditID);
                            grdSummaryDetailsAuditCoverage.DataSource = detailView;
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetailsAuditCoverage.DataBind();
                        }
                    }
                    //GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //    //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnAddChecklist_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Arguments"] != null)
                {
                    int ProcessId = -1;
                    int Subprocessid = -1;
                    int BranchID = -1;
                    string financialyear = String.Empty;
                    string period = string.Empty;
                    int Veritcalid = -1;
                    long auditID = -1;

                    var Arguments = Convert.ToString(ViewState["Arguments"]);
                    string[] arg = Arguments.ToString().Split(';');
                    if (arg.Length > 0)
                    {
                        BranchID = Convert.ToInt32(arg[5]);
                        Veritcalid = Convert.ToInt32(arg[6]);
                        ProcessId = Convert.ToInt32(arg[3]);
                        Subprocessid = Convert.ToInt32(arg[4]);
                        period = arg[7].ToString();
                        financialyear = arg[8].ToString();
                        auditID = Convert.ToInt32(arg[9]);
                        //auditID = UserManagementRisk.GetAuditID(BranchID, Convert.ToInt32(Veritcalid), financialyear, period);
                        //BID,VID,PID,SPID,AUID                        
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAdditionalDialog(" + BranchID + "," + Veritcalid + ",'" + ProcessId + "','" + Subprocessid + "'," + auditID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}