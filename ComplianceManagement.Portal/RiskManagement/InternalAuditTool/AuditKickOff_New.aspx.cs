﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using System.Globalization;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditKickOff_New : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected string FinYear;
        protected string Period;
        protected int branchid;
        protected int atbdid;
        protected static int statusid;
        protected int processid;
        protected int VerticalID;
        protected int AuditID;
        protected string isIE;
        protected int AuditorID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    branchid = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = branchid;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    FinYear = Request.QueryString["FY"];
                    ViewState["FY"] = FinYear;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    Period = Request.QueryString["Period"];
                    ViewState["Period"] = Period;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AUID"]);
                    ViewState["AUID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                {
                    isIE = Convert.ToString(Request.QueryString["ISIE"]);
                    ViewState["ISIE"] = isIE;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                {
                    AuditorID = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    ViewState["AUDTOR"] = AuditID;
                }
                BindPerformerFilter();
                BindReviewerFilter();
                BindReviewerFilter2();
                BindProcessAudit();
                bindPageNumber();
                if (branchid != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(isIE))
                {
                    if (InternalAuditAssignmnetExists(branchid, VerticalID, FinYear, Period, AuditID))
                    {
                        var UploadOptionDisplayOrNot = UserManagementRisk.Sp_CheckStatusIdSubmittedCountProcedure(Convert.ToInt32(branchid), Convert.ToInt32(VerticalID), FinYear, Period, AuditID);
                        btnSendEmail.Visible = true;
                        if (UploadOptionDisplayOrNot != null)
                        {
                            if (UploadOptionDisplayOrNot.TotalCount >= UploadOptionDisplayOrNot.savecount)
                            {
                                btnSendEmail.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        btnSendEmail.Visible = false;
                    }
                }
                else
                {
                    btnSendEmail.Visible = false;
                }
            }
        }

        protected void ddlPerformerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlPerformerAuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlPerformerAuditHeader");
                if (ddlPerformerAuditHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcess.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerAuditHeader.Items.Count; i++)
                        {
                            DropDownList ddlPerformer = (DropDownList)grdProcess.Rows[j].FindControl("ddlPerformer");
                            if (ddlPerformer.Items.Count > 0)
                            {
                                if (ddlPerformer.Enabled)
                                {
                                    if (ddlPerformerAuditHeader.Items[i].Selected)
                                    {
                                        string val = ddlPerformerAuditHeader.Items[i].Value;
                                        ddlPerformer.Items.FindByValue(val).Selected = true;
                                    }
                                    else
                                    {
                                        string val = ddlPerformerAuditHeader.Items[i].Value;
                                        ddlPerformer.Items.FindByValue(val).Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlReviewerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerAuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewerAuditHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer");


                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerAuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewer2AuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2AuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewer2AuditHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer2");


                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewer2AuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewer2ImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2ImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewer2ImplemetationHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP2");


                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewer2ImplemetationHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewerImplemetationHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP");


                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerImplemetationHeader.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlPerformerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlPerformerImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlPerformerImplemetationHeader");
                if (ddlPerformerImplemetationHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcess.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerImplemetationHeader.Items.Count; i++)
                        {
                            DropDownList ddlPerformerIMP = (DropDownList)grdProcess.Rows[j].FindControl("ddlPerformerIMP");
                            if (ddlPerformerIMP.Enabled)
                            {
                                if (ddlPerformerIMP.Items.Count > 0)
                                {
                                    if (ddlPerformerImplemetationHeader.Items[i].Selected)
                                    {
                                        string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                        ddlPerformerIMP.Items.FindByValue(val).Selected = true;
                                    }
                                    else
                                    {
                                        string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                        ddlPerformerIMP.Items.FindByValue(val).Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static bool InternalAuditAssignmnetExists(int CustomerBranchid, int Verticalid, string FinancialYear, string ForMonth, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from ICAA in entities.InternalControlAuditAssignments
                             join IASO in entities.InternalAuditScheduleOns
                             on ICAA.InternalAuditInstance equals IASO.InternalAuditInstance
                             where ICAA.ProcessId == IASO.ProcessId
                             && IASO.FinancialYear == FinancialYear
                             && IASO.ForMonth == ForMonth
                             && ICAA.CustomerBranchID == CustomerBranchid
                             && ICAA.VerticalID == Verticalid
                             && ICAA.AuditID == AuditID
                             select ICAA).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                DropDownListPageNo.DataBind();
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcess.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            if (chkSelectedPage != 0)
            {
                grdProcess.PageIndex = chkSelectedPage - 1;
            }
            BindProcessAudit();
            bindPageNumber();
        }
        public void BindPerformerFilter()
        {
            int auditorid = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
            {
                isIE = Convert.ToString(Request.QueryString["ISIE"]);
            }
            else
            {
                isIE = Convert.ToString(ViewState["ISIE"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
            {
                auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
            }
            else
            {
                auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
            }
            if (isIE == "I")
            {
                ddlPerformerFilter.Items.Clear();
                ddlPerformerFilter.DataTextField = "Name";
                ddlPerformerFilter.DataValueField = "ID";
                ddlPerformerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddlPerformerFilter.DataBind();
                ddlPerformerFilter.Items.Insert(0, new ListItem("Select Performer ", "-1"));
            }
            else
            {
                if (auditorid != -1)
                {
                    ddlPerformerFilter.Items.Clear();
                    ddlPerformerFilter.DataTextField = "Name";
                    ddlPerformerFilter.DataValueField = "ID";
                    ddlPerformerFilter.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(auditorid));
                    ddlPerformerFilter.DataBind();
                    ddlPerformerFilter.Items.Insert(0, new ListItem("Select Performer", "-1"));
                }
            }
        }
        public void BindReviewerFilter2()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlReviewerFilter2.Items.Clear();
            ddlReviewerFilter2.DataTextField = "Name";
            ddlReviewerFilter2.DataValueField = "ID";
            ddlReviewerFilter2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
            ddlReviewerFilter2.DataBind();
            ddlReviewerFilter2.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
        }
        public void BindReviewerFilter()
        {
            int auditorid = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
            {
                isIE = Convert.ToString(Request.QueryString["ISIE"]);
            }
            else
            {
                isIE = Convert.ToString(ViewState["ISIE"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
            {
                auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
            }
            else
            {
                auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
            }
            if (isIE == "I")
            {
                ddlReviewerFilter.Items.Clear();
                ddlReviewerFilter.DataTextField = "Name";
                ddlReviewerFilter.DataValueField = "ID";
                ddlReviewerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddlReviewerFilter.DataBind();
                ddlReviewerFilter.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
            }
            else
            {
                if (auditorid != -1)
                {
                    ddlReviewerFilter.Items.Clear();
                    ddlReviewerFilter.DataTextField = "Name";
                    ddlReviewerFilter.DataValueField = "ID";
                    ddlReviewerFilter.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(auditorid));
                    ddlReviewerFilter.DataBind();
                    ddlReviewerFilter.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                }
            }
        }
        protected void ddlPerformerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlPerformer = (DropDownList)grdProcess.Rows[i].FindControl("ddlPerformer");
                    DropDownList ddlPerformerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlPerformerIMP");
                    if (ddlPerformer.Enabled)
                    {
                        ddlPerformer.ClearSelection();
                        ddlPerformer.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                    if (ddlPerformerIMP.Enabled)
                    {
                        ddlPerformerIMP.ClearSelection();
                        ddlPerformerIMP.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlReviewerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer");
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP");

                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlReviewerFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer2");
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP2");

                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdProcess.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (chkSelectedPage != 0)
                {
                    grdProcess.PageIndex = chkSelectedPage - 1;
                }
                BindProcessAudit();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcess.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {

            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }


        public void BindProcessAudit()
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int auditorid = -1;
                    int CustomerBranchId = -1;
                    string flag = string.Empty;
                    string FinancialYear = string.Empty;
                    string Period = string.Empty;
                    int VerticalID = -1;
                    int customerID = -1;
                    int Auid = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                    {
                        CustomerBranchId = Convert.ToInt32(Request.QueryString["BID"]);
                    }
                    else
                    {
                        CustomerBranchId = Convert.ToInt32(ViewState["BID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    }
                    else
                    {
                        VerticalID = Convert.ToInt32(ViewState["VID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                    {
                        FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                    }
                    else
                    {
                        FinancialYear = Convert.ToString(ViewState["FY"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                    {
                        Period = Convert.ToString(Request.QueryString["Period"]);
                    }
                    else
                    {
                        Period = Convert.ToString(ViewState["Period"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                    {
                        Auid = Convert.ToInt32(Request.QueryString["AUID"]);
                    }
                    else
                    {
                        Auid = Convert.ToInt32(ViewState["AUID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                    {
                        flag = Convert.ToString(Request.QueryString["ISIE"]);
                    }
                    else
                    {
                        flag = Convert.ToString(ViewState["ISIE"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                    {
                        auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    }
                    else
                    {
                        auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                    }
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                    var Branchlistloop = Branchlist.ToList();

                    if (flag == "I")
                    {
                        var RiskActivityToBeDoneMappingGetByName = (from row in entities.AuditKickOffFillViews
                                                                    join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                                                    on row.ProcessId equals row1.ProcessId
                                                                    where row.CustomerBranchId == row1.BranchID
                                                                    && row.CustomerID == customerID
                                                                    && row.AssignedTo == "I"
                                                                    && row.AuditID == Auid
                                                                    && row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                                                    && row1.ISACTIVE == true
                                                                    select row).Distinct().ToList();
                        if (Branchlist.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();

                        if (VerticalID != -1)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.Verticalid == VerticalID).ToList();

                        if (!string.IsNullOrEmpty(FinancialYear))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                        if (!string.IsNullOrEmpty(Period))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.TermName == Period).ToList();

                        if (RiskActivityToBeDoneMappingGetByName.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.OrderBy(entry => entry.ProcessName).ToList();

                        grdProcess.DataSource = null;
                        grdProcess.DataBind();

                        grdProcess.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                        Session["TotalRows"] = null;
                        Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                        grdProcess.DataBind();
                    }
                    else if (flag == "E")
                    {
                        var RiskActivityToBeDoneMappingGetByName = (from row in entities.AuditKickOffFillViews
                                                                    join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                                                     on row.ProcessId equals row1.ProcessId
                                                                    where row.CustomerBranchId == row1.BranchID
                                                                    && row.CustomerID == customerID
                                                                    && row.AssignedTo == "E"
                                                                    && row.AuditID == Auid
                                                                     && row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                                                    && row1.ISACTIVE == true
                                                                    select row).Distinct().ToList();

                        if (Branchlist.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();

                        if (VerticalID != -1)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.Verticalid == VerticalID).ToList();

                        if (!string.IsNullOrEmpty(FinancialYear))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                        if (ViewState["Period"] != null)
                        {
                            Period = Convert.ToString(ViewState["Period"]);
                            if (!string.IsNullOrEmpty(Period))
                                RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.TermName == Period).ToList();
                        }

                        grdProcess.DataSource = null;
                        grdProcess.DataBind();

                        grdProcess.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                        Session["TotalRows"] = null;
                        Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                        grdProcess.DataBind();
                    }
                    //CommanBranchlist.Clear();
                    //branchids.Clear();
                    Branchlistloop.Clear();
                    Branchlist.Clear();

                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int auditorid = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                {
                    isIE = Convert.ToString(Request.QueryString["ISIE"]);
                }
                else
                {
                    isIE = Convert.ToString(ViewState["ISIE"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                {
                    auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                }
                else
                {
                    auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    var obj = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                    DropDownList ddlPerformerAuditHeader = (e.Row.FindControl("ddlPerformerAuditHeader") as DropDownList);

                    ddlPerformerAuditHeader.DataTextField = "Name";
                    ddlPerformerAuditHeader.DataValueField = "ID";
                    ddlPerformerAuditHeader.DataSource = obj;
                    ddlPerformerAuditHeader.DataBind();
                    ddlPerformerAuditHeader.Items.Insert(0, new ListItem("Performer", "-1"));

                    DropDownList ddlReviewerAuditHeader = (e.Row.FindControl("ddlReviewerAuditHeader") as DropDownList);

                    ddlReviewerAuditHeader.DataTextField = "Name";
                    ddlReviewerAuditHeader.DataValueField = "ID";
                    ddlReviewerAuditHeader.DataSource = obj;
                    ddlReviewerAuditHeader.DataBind();
                    ddlReviewerAuditHeader.Items.Insert(0, new ListItem("Reviewer 1", "-1"));

                    DropDownList ddlReviewer2AuditHeader = (e.Row.FindControl("ddlReviewer2AuditHeader") as DropDownList);

                    ddlReviewer2AuditHeader.DataTextField = "Name";
                    ddlReviewer2AuditHeader.DataValueField = "ID";
                    ddlReviewer2AuditHeader.DataSource = obj;
                    ddlReviewer2AuditHeader.DataBind();
                    ddlReviewer2AuditHeader.Items.Insert(0, new ListItem("Reviewer 2", "-1"));

                    DropDownList ddlPerformerImplemetationHeader = (e.Row.FindControl("ddlPerformerImplemetationHeader") as DropDownList);

                    ddlPerformerImplemetationHeader.DataTextField = "Name";
                    ddlPerformerImplemetationHeader.DataValueField = "ID";
                    ddlPerformerImplemetationHeader.DataSource = obj;
                    ddlPerformerImplemetationHeader.DataBind();
                    ddlPerformerImplemetationHeader.Items.Insert(0, new ListItem("Performer", "-1"));

                    DropDownList ddlReviewerImplemetationHeader = (e.Row.FindControl("ddlReviewerImplemetationHeader") as DropDownList);


                    ddlReviewerImplemetationHeader.DataTextField = "Name";
                    ddlReviewerImplemetationHeader.DataValueField = "ID";
                    ddlReviewerImplemetationHeader.DataSource = obj;
                    ddlReviewerImplemetationHeader.DataBind();
                    ddlReviewerImplemetationHeader.Items.Insert(0, new ListItem("Reviewer 1", "-1"));

                    DropDownList ddlReviewer2ImplemetationHeader = (e.Row.FindControl("ddlReviewer2ImplemetationHeader") as DropDownList);

                    ddlReviewer2ImplemetationHeader.DataTextField = "Name";
                    ddlReviewer2ImplemetationHeader.DataValueField = "ID";
                    ddlReviewer2ImplemetationHeader.DataSource = obj;
                    ddlReviewer2ImplemetationHeader.DataBind();
                    ddlReviewer2ImplemetationHeader.Items.Insert(0, new ListItem("Reviewer 2", "-1"));

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblLocationId = (e.Row.FindControl("lblLocationId") as Label);
                    Label lblProcessId = (e.Row.FindControl("lblProcessId") as Label);
                    Label lblSubProcessId = (e.Row.FindControl("lblSubProcessId") as Label);
                    Label lblFinancialYear = (e.Row.FindControl("lblFinancialYear") as Label);
                    Label lblTermName = (e.Row.FindControl("lblTermName") as Label);
                    Label lblVerticalId = (e.Row.FindControl("lblVerticalId") as Label);
                    Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);
                    if (isIE == "I")
                    {
                        #region Audit
                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("Performer", "-1"));

                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                PerformerReviewerEnable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Reviewer", "-1"));

                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                PerformerReviewerEnable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewer2 != null)
                        {
                            ddlReviewer2.Items.Clear();
                            ddlReviewer2.DataTextField = "Name";
                            ddlReviewer2.DataValueField = "ID";
                            ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewer2.DataBind();
                            ddlReviewer2.Items.Insert(0, new ListItem("Reviewer2", "-1"));

                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                PerformerReviewerEnable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }
                        #endregion

                        #region Implementation
                        DropDownList ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                        if (ddlPerformerIMP != null)
                        {
                            ddlPerformerIMP.Items.Clear();
                            ddlPerformerIMP.DataTextField = "Name";
                            ddlPerformerIMP.DataValueField = "ID";
                            ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformerIMP.DataBind();
                            ddlPerformerIMP.Items.Insert(0, new ListItem("Performer", "-1"));

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpPerformerReviewerEnable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewerIMP != null)
                        {
                            ddlReviewerIMP.Items.Clear();
                            ddlReviewerIMP.DataTextField = "Name";
                            ddlReviewerIMP.DataValueField = "ID";
                            ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewerIMP.DataBind();
                            ddlReviewerIMP.Items.Insert(0, new ListItem("Reviewer", "-1"));
                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpPerformerReviewerEnable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewerIMP2 != null)
                        {
                            ddlReviewerIMP2.Items.Clear();
                            ddlReviewerIMP2.DataTextField = "Name";
                            ddlReviewerIMP2.DataValueField = "ID";
                            ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewerIMP2.DataBind();
                            ddlReviewerIMP2.Items.Insert(0, new ListItem("Reviewer2", "-1"));
                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpPerformerReviewerEnable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (auditorid != -1)
                        {
                            #region Audit
                            DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                            DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                            DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                            if (ddlPerformer != null)
                            {
                                ddlPerformer.Items.Clear();
                                ddlPerformer.DataTextField = "Name";
                                ddlPerformer.DataValueField = "ID";
                                ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformer.DataBind();
                                ddlPerformer.Items.Insert(0, new ListItem("Select Performer", "-1"));

                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewer != null)
                            {
                                ddlReviewer.Items.Clear();
                                ddlReviewer.DataTextField = "Name";
                                ddlReviewer.DataValueField = "ID";
                                ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewer.DataBind();
                                ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewer2 != null)
                            {
                                ddlReviewer2.Items.Clear();
                                ddlReviewer2.DataTextField = "Name";
                                ddlReviewer2.DataValueField = "ID";
                                ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewer2.DataBind();
                                ddlReviewer2.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }
                            #endregion

                            #region Implementation
                            DropDownList ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownList);
                            DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                            DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                            if (ddlPerformerIMP != null)
                            {
                                ddlPerformerIMP.Items.Clear();
                                ddlPerformerIMP.DataTextField = "Name";
                                ddlPerformerIMP.DataValueField = "ID";
                                ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformerIMP.DataBind();
                                ddlPerformerIMP.Items.Insert(0, new ListItem("Select Performer", "-1"));

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewerIMP != null)
                            {
                                ddlReviewerIMP.Items.Clear();
                                ddlReviewerIMP.DataTextField = "Name";
                                ddlReviewerIMP.DataValueField = "ID";
                                ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewerIMP.DataBind();
                                ddlReviewerIMP.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewerIMP2 != null)
                            {
                                ddlReviewerIMP2.Items.Clear();
                                ddlReviewerIMP2.DataTextField = "Name";
                                ddlReviewerIMP2.DataValueField = "ID";
                                ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewerIMP2.DataBind();
                                ddlReviewerIMP2.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }
                            #endregion
                        }
                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        //public void PerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        //{
        //    List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
        //                   select row).ToList();

        //        if (Records.Count > 0)
        //        {
        //            var Data = (from row in Records
        //                        where row.ProcessId == ProcessID
        //                        && row.CustomerBranchID == CustBranchID
        //                        && row.SubProcessId == SubProcessID
        //                        && row.FinancialYear == FinYear
        //                        && row.ForMonth == Period
        //                        && row.RoleID == RoleID && row.VerticalID == VerticalId
        //                        select row).FirstOrDefault();

        //            if (Data != null)
        //            {
        //                ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
        //                ddl.Enabled = false;
        //                ddl.Attributes.Add("class", "");
        //                ddl.CssClass = "form-control m-bot15 Check";
        //            }
        //            else
        //                ddl.Enabled = true;
        //        }
        //    }
        //}
        public void PerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();

                if (Records.Count > 0)
                {
                    var Data = (from row in Records
                                where row.ProcessId == ProcessID
                                && row.SubProcessId == SubProcessID
                                && row.CustomerBranchID == CustBranchID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                && row.RoleID == RoleID && row.VerticalID == VerticalId
                                select row).FirstOrDefault();

                    if (Data != null)
                    {
                        ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                        ddl.Enabled = false;
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15";
                    }
                    else
                        ddl.Enabled = true;
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            String URLStr = "~/RiskManagement/InternalAuditTool/AuditListForKickOff.aspx";
            Response.Redirect(URLStr, false);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveFlag = false;
                string listNotSaveList = string.Empty;
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Portal.Common.AuthenticationHelper.UserID;

                #region Get Branch, Vertical, FY, Period Values

                int customerBranchId = -1;
                int verticalID = -1;
                string financialYear = string.Empty;
                string period = string.Empty;
                bool CheckSameUser = false;
                int distinctprocess = -1;
                int distinctprocessImp = -1;

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    customerBranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    customerBranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    verticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    verticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialYear = Convert.ToString(Request.QueryString["FY"]);
                }
                else
                {
                    financialYear = Convert.ToString(ViewState["FY"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    period = Convert.ToString(Request.QueryString["Period"]);
                }
                else
                {
                    period = Convert.ToString(ViewState["Period"]);
                }
                #endregion
                
                int TotalRowCount = Convert.ToInt32(Session["TotalRows"]);
                int ActualRows = 0;
                foreach (GridViewRow g1 in grdProcess.Rows)
                {
                    DropDownList ddlperval = new DropDownList();
                    ddlperval = (g1.FindControl("ddlPerformer") as DropDownList);
                    if (!string.IsNullOrEmpty(ddlperval.SelectedValue))
                    {
                        if (ddlperval.SelectedValue != "-1")
                        {
                            ActualRows++;
                        }
                    }
                }
                if (TotalRowCount == ActualRows)
                {
                    CheckSameUser = true;
                }

                if (CheckSameUser)
                {
                    if (customerBranchId != -1 && verticalID != -1 && financialYear != "" && period != "")
                    {
                        List<InternalControlAuditAssignment> Tempassignments = new List<InternalControlAuditAssignment>();
                        List<RiskActivityToBeDoneMapping> ActiveAuditStepIDList = new List<RiskActivityToBeDoneMapping>();
                        InternalAuditInstance riskinstance = new InternalAuditInstance();
                        AuditImplementationInstance riskinstanceIMP = new AuditImplementationInstance();
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            int Count = 0;
                            ActiveAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                     where RATBDM.IsActive == true
                                                     && RATBDM.RCMType != "IFC"
                                                     select RATBDM).Distinct().ToList();
                            foreach (GridViewRow g1 in grdProcess.Rows)
                            {
                                Tempassignments.Clear();
                                DropDownList ddlper = new DropDownList();
                                DropDownList ddlrev = new DropDownList();
                                DropDownList ddlrev2 = new DropDownList();
                                DropDownList ddlPerImp = new DropDownList();
                                DropDownList ddlRevImp = new DropDownList();
                                DropDownList ddlRev2Imp = new DropDownList();

                                ddlper = (g1.FindControl("ddlPerformer") as DropDownList);
                                ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                                ddlrev2 = (g1.FindControl("ddlReviewer2") as DropDownList);
                                ddlPerImp = (g1.FindControl("ddlPerformerIMP") as DropDownList);
                                ddlRevImp = (g1.FindControl("ddlReviewerIMP") as DropDownList);
                                ddlRev2Imp = (g1.FindControl("ddlReviewerIMP2") as DropDownList);

                                List<int> Auditstepdetails = new List<int>();

                                string lblLocation = (g1.FindControl("lblLocation") as Label).Text;
                                string lblVerticalName = (g1.FindControl("lblVerticalName") as Label).Text;
                                string lblLocationId = (g1.FindControl("lblLocationId") as Label).Text;
                                string lblVerticalId = (g1.FindControl("lblVerticalId") as Label).Text;
                                string lblItemTemplateFinancialYear = (g1.FindControl("lblFinancialYear") as Label).Text;
                                string lblItemTemplateForPeriod = (g1.FindControl("lblTermName") as Label).Text;
                                string lblProcessId = (g1.FindControl("lblProcessId") as Label).Text;
                                string lblSubProcessId = (g1.FindControl("lblSubProcessId") as Label).Text;
                                string lblAuditID = (g1.FindControl("lblAuditID") as Label).Text;
                                //List<string> PerformerList = new List<string>();
                                //List<string> PerformerListImplem = new List<string>();
                                string lblPerformerUserID = (g1.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserID = (g1.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserID2 = (g1.FindControl("ddlReviewer2") as DropDownList).SelectedItem.Value;

                                string lblPerformerUserIDIMP = (g1.FindControl("ddlPerformerIMP") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserIDIMP = (g1.FindControl("ddlReviewerIMP") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserIDIMP2 = (g1.FindControl("ddlReviewerIMP2") as DropDownList).SelectedItem.Value;
                                
                                if (lblReviewerUserID == lblReviewerUserID2 && lblReviewerUserID != "-1" && lblReviewerUserID2 != "-1")
                                {
                                    if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                    {
                                        listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                                    }
                                }
                                else if (lblReviewerUserIDIMP == lblReviewerUserIDIMP2 && lblReviewerUserIDIMP != "-1" && lblReviewerUserIDIMP2 != "-1")
                                {
                                    if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                    {
                                        listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                                    }
                                }
                                else
                                {
                                    if (lblLocationId != "" && lblVerticalId != "" && lblItemTemplateFinancialYear != "" && lblItemTemplateForPeriod != "" && lblProcessId != "" && lblAuditID != "")
                                    {
                                        Auditstepdetails = UserManagementRisk.GetAuditStepDetailsCount(ActiveAuditStepIDList, Convert.ToInt32(lblLocationId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), userID);

                                        if (Auditstepdetails != null)
                                        {
                                            if (Auditstepdetails.Count > 0)
                                            {
                                                if (lblPerformerUserID != "-1" && lblReviewerUserID != "-1" && ddlper != null && ddlper.Enabled && ddlrev != null && ddlrev.Enabled)
                                                {
                                                    #region Audit
                                                    string AssignedTo = "";
                                                    if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                                                    {
                                                        AssignedTo = Convert.ToString(Request.QueryString["ISIE"]);
                                                    }
                                                    else
                                                    {
                                                        AssignedTo = Convert.ToString(ViewState["ISIE"]);
                                                    }

                                                    riskinstance.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstance.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstance.ProcessId = Convert.ToInt32(lblProcessId);
                                                    riskinstance.SubProcessId = -1;
                                                    riskinstance.IsDeleted = false;
                                                    riskinstance.CreatedOn = DateTime.Now;
                                                    riskinstance.CreatedBy = userID;
                                                    riskinstance.AuditID = Convert.ToInt64(lblAuditID);

                                                    //if (distinctprocess != Convert.ToInt32(lblProcessId))
                                                    //{
                                                    //    UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                    //    CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, lblItemTemplateFinancialYear, Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblProcessId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), -1);
                                                    //    distinctprocess = Convert.ToInt32(lblProcessId);
                                                    //}

                                                    if (distinctprocess != Convert.ToInt32(lblProcessId))
                                                    {
                                                        long ProcessInstanceID = UserManagementRisk.CheckProcessInstanceIDExist(riskinstance);
                                                        if (ProcessInstanceID == 0)
                                                        {
                                                            UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                            CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, lblItemTemplateFinancialYear, Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblProcessId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), -1);
                                                        }
                                                        else
                                                        {
                                                            riskinstance.ID = ProcessInstanceID;
                                                        }
                                                        distinctprocess = Convert.ToInt32(lblProcessId);
                                                    }

                                                    //if (PerformerList.Count > 0)
                                                    //{
                                                    //    foreach (var item in PerformerList)
                                                    //    {
                                                    if (lblPerformerUserID != null && lblPerformerUserID != "-1" && ddlper.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                                                        TempAssP.InternalAuditInstance = riskinstance.ID;
                                                        TempAssP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssP.RoleID = 3;   //RoleManagement.GetByCode("PERF").ID;
                                                        TempAssP.UserID = Convert.ToInt32(ddlper.SelectedValue);
                                                        TempAssP.IsActive = true;
                                                        TempAssP.ISInternalExternal = AssignedTo;
                                                        TempAssP.CreatedOn = DateTime.Now;
                                                        TempAssP.CreatedBy = userID;
                                                        TempAssP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssP.AuditPeriodStartdate = null;
                                                        TempAssP.AuditPeriodEnddate = null;
                                                        Tempassignments.Add(TempAssP);
                                                    }
                                                    //    }
                                                    //}

                                                    if (lblReviewerUserID != null && lblReviewerUserID != "-1" && ddlrev.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssR.RoleID = 4;// RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        Tempassignments.Add(TempAssR);
                                                    }

                                                    if (lblReviewerUserID2 != null && lblReviewerUserID2 != "-1" && ddlrev2.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssR.RoleID = 5;// RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID2);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        Tempassignments.Add(TempAssR);
                                                    }

                                                    if (Tempassignments.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsInternalControlAuditAssignment(Tempassignments);
                                                    }

                                                    //Save Total Active Ateps and Count 
                                                    if (Count == 0)
                                                    {
                                                        SaveFlag = UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, customerID, customerBranchId, verticalID, financialYear, period, Convert.ToInt64(lblAuditID));
                                                    }
                                                    Count++;
                                                    #endregion
                                                }
                                                if (lblPerformerUserIDIMP != "-1" && ddlPerImp != null && ddlPerImp.Enabled)
                                                {
                                                    #region Implementation
                                                    riskinstanceIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstanceIMP.IsDeleted = false;
                                                    riskinstanceIMP.CreatedOn = DateTime.Now;
                                                    riskinstanceIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                    riskinstanceIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstanceIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                    riskinstanceIMP.SubProcessId = -1;
                                                    riskinstanceIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                    riskinstanceIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                                                    if (distinctprocessImp != Convert.ToInt32(lblProcessId))
                                                    {
                                                        long InstanceID = UserManagementRisk.CheckInstanceIDExist(riskinstanceIMP);
                                                        if (InstanceID == 0)
                                                        {
                                                            UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstanceIMP);
                                                            CreateScheduleOnImplementation(riskinstanceIMP.Id,
                                                            Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User,
                                                            lblItemTemplateFinancialYear.Trim(), Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblVerticalId), Convert.ToInt32(lblProcessId), Convert.ToInt64(lblAuditID), -1);
                                                        }
                                                        else
                                                        {
                                                            riskinstanceIMP.Id = InstanceID;
                                                        }
                                                        distinctprocessImp = Convert.ToInt32(lblProcessId);
                                                    }


                                                    List<AuditImplementationAssignment> TempassignmentsIMP = new List<AuditImplementationAssignment>();
                                                    //if (PerformerListImplem.Count > 0)
                                                    //{
                                                    //    foreach (var item in PerformerListImplem)
                                                    //    {
                                                    if (lblPerformerUserIDIMP != null && lblPerformerUserIDIMP != "-1" && ddlPerImp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssPIMP = new AuditImplementationAssignment();
                                                        TempAssPIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssPIMP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                                        TempAssPIMP.UserID = Convert.ToInt32(ddlPerImp.SelectedValue);
                                                        TempAssPIMP.IsActive = true;
                                                        TempAssPIMP.CreatedOn = DateTime.Now;
                                                        TempAssPIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssPIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssPIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssPIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssPIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssPIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssPIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssPIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        TempassignmentsIMP.Add(TempAssPIMP);
                                                    }
                                                    //    }
                                                    //}
                                                    if (lblReviewerUserIDIMP != null && lblReviewerUserIDIMP != "-1" && ddlRevImp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                        TempAssRIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssRIMP.UserID = Convert.ToInt32(lblReviewerUserIDIMP);
                                                        TempAssRIMP.IsActive = true;
                                                        TempAssRIMP.CreatedOn = DateTime.Now;
                                                        TempAssRIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssRIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssRIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssRIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssRIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssRIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        TempassignmentsIMP.Add(TempAssRIMP);
                                                    }
                                                    if (lblReviewerUserIDIMP2 != null && lblReviewerUserIDIMP2 != "-1" && ddlRev2Imp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                        TempAssRIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW2").ID;
                                                        TempAssRIMP.UserID = Convert.ToInt32(lblReviewerUserIDIMP2);
                                                        TempAssRIMP.IsActive = true;
                                                        TempAssRIMP.CreatedOn = DateTime.Now;
                                                        TempAssRIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssRIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssRIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssRIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssRIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssRIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        TempassignmentsIMP.Add(TempAssRIMP);
                                                    }
                                                    if (TempassignmentsIMP.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsAuditImplementationAssignment(TempassignmentsIMP);
                                                    }
                                                    SaveFlag = true;
                                                    #endregion
                                                }

                                                if (InternalAuditAssignmnetExists(customerBranchId, verticalID, financialYear, period, Convert.ToInt32(lblAuditID)))
                                                {
                                                    btnSendEmail.Visible = true;
                                                }
                                                else
                                                {
                                                    btnSendEmail.Visible = false;
                                                }
                                            }//Auditstepdetails.Count > 0                               
                                        }
                                        else
                                        {
                                            if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                            {
                                                listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim();
                                            }
                                        }
                                    }// grd branch fy period vertical not blank end
                                }
                            }//Foreach End 

                            #region Code added by Sushant for Process Name/SubProcess
                            var DistinctAuditID = (from row in entities.ProcessCommaSaperates
                                                   select row.AuditID).Distinct().ToList();

                            var SubProcessAuditList = (from row in entities.SubProcessCommaSeparates
                                                       select row.AuditID).Distinct().ToList();

                            var CheckAuditID = (from row in entities.AuditClosureDetails
                                                where !DistinctAuditID.Contains(row.ID)
                                                select row.ID).Distinct().ToList();

                            var CheckAuditIDSubProcess = (from row in entities.AuditClosureDetails
                                                          where !SubProcessAuditList.Contains(row.ID)
                                                          select row.ID).Distinct().ToList();

                            foreach (var item in CheckAuditID)
                            {
                                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                                   join row1 in entities.Mst_Process
                                                   on row.ProcessId equals row1.Id
                                                   join RATDM in entities.RiskActivityToBeDoneMappings
                                                   on row.CustomerBranchID equals RATDM.CustomerBranchID
                                                   where row.AuditID == item
                                                   && row.ProcessId == RATDM.ProcessId
                                                   && row.IsActive == true
                                                   select row1.Name).Distinct().ToList();

                                var ProcessNameComaSaperate = string.Empty;
                                foreach (var item1 in ProcessList)
                                {
                                    ProcessNameComaSaperate += item1 + ",";
                                }
                                ProcessNameComaSaperate = ProcessNameComaSaperate.TrimEnd(',');
                                ProcessNameComaSaperate = ProcessNameComaSaperate.TrimStart(',');

                                if (!string.IsNullOrEmpty(ProcessNameComaSaperate))
                                {
                                    ProcessCommaSaperate objProcess = new ProcessCommaSaperate();
                                    objProcess.AuditID = item;
                                    objProcess.ProcessName = ProcessNameComaSaperate;
                                    objProcess.CreatedOn = DateTime.Now;
                                    entities.ProcessCommaSaperates.Add(objProcess);
                                    entities.SaveChanges();
                                }
                            }

                            foreach (var item in CheckAuditIDSubProcess)
                            {
                                var SubProcessList = (from row in entities.InternalControlAuditAssignments
                                                      join row1 in entities.mst_Subprocess
                                                      on row.SubProcessId equals row1.Id
                                                      join RATDM in entities.RiskActivityToBeDoneMappings
                                                      on row.CustomerBranchID equals RATDM.CustomerBranchID
                                                      where row.AuditID == item
                                                      && RATDM.ProcessId == row.ProcessId
                                                      && RATDM.SubProcessId == row.SubProcessId
                                                      && row.IsActive == true
                                                      select row1.Name).Distinct().ToList();

                                var SubProcessNameComaSeparate = string.Empty;
                                foreach (var item1 in SubProcessList)
                                {
                                    SubProcessNameComaSeparate += item1 + ",";
                                }

                                SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimEnd(',');
                                SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimStart(',');
                                if (!string.IsNullOrEmpty(SubProcessNameComaSeparate))
                                {
                                    SubProcessCommaSeparate objSubProcess = new SubProcessCommaSeparate();
                                    objSubProcess.AuditID = item;
                                    objSubProcess.SubProcessName = SubProcessNameComaSeparate;
                                    objSubProcess.CreatedOn = DateTime.Now;
                                    entities.SubProcessCommaSeparates.Add(objSubProcess);
                                    entities.SaveChanges();
                                }
                            }
                            #endregion
                        }//Using End
                    }//branch fy period vertical not blank end

                    cvDuplicateEntry1.IsValid = false;
                    if (SaveFlag)
                    {

                        if (listNotSaveList.Length <= 0)
                        {
                            cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Sucessfully.";
                        }
                        else
                        {
                            cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Sucessfully. The following audits " +
                                                           "- " + listNotSaveList.Trim(',') + " not kickoff due to audit steps not uploaded or Reviewer & Reviewer2 cannot be same user";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.ErrorMessage = "No Audit Kickoff due to audit steps not uploaded for following " +
                            "- " + listNotSaveList.Trim(',') + " or Reviewer & Reviewer2 cannot be same user";
                    }
                    BindProcessAudit();
                }
                else
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please select user from drop down.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool Exists(int CustomerBranchID, int ProcessId, int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchID != -1 && ProcessId != -1 && userid != -1)
                {
                    var query = (from row in entities.ProcessPerformerReminderNotifications
                                 where row.ProcessID == ProcessId
                                     && row.CustomerBranchId == CustomerBranchID
                                     && row.UserID == userid
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    return false;
                }
            }
        }

        public static void AddProcessPerformerReminderNotification(List<ProcessPerformerReminderNotification> objEscalation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    objEscalation.ForEach(entry =>
                    {
                        bool chelexists = Exists(Convert.ToInt32(entry.CustomerBranchId), Convert.ToInt32(entry.ProcessID), Convert.ToInt32(entry.UserID));
                        if (chelexists == false)
                        {
                            entities.ProcessPerformerReminderNotifications.Add(entry);
                            entities.SaveChanges();
                        }

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static PerformerSendMailView ProcessPerformerSendMailReminder(int ProcessId, int CustomerId)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var CheckListReminders = (from row in entities.PerformerSendMailViews

                                          where row.CustomerID == CustomerId && row.RoleID == 3
                                          && row.ProcessId == ProcessId
                                          select row).FirstOrDefault();

                return CheckListReminders;
            }
        }
        public static void CreateScheduleOn(long InternalAuditInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, long Processid, int VerticalID, long AuditID, int SubProcessID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int userID = -1;
                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    DateTime curruntDate = DateTime.UtcNow;

                    List<InternalAuditTransaction> transactionlist = new List<InternalAuditTransaction>();

                    InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                    auditScheduleon.InternalAuditInstance = InternalAuditInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = Processid;
                    auditScheduleon.CreatedOn = DateTime.Now;
                    auditScheduleon.CreatedBy = userID;
                    auditScheduleon.AuditID = AuditID;
                    entities.InternalAuditScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        InternalAuditInstance = InternalAuditInstance,
                        AuditScheduleOnID = auditScheduleon.ID,
                        CreatedByText = creatdByName,
                        ProcessId = Processid,
                        SubProcessId = -1,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalID,
                        CreatedOn = DateTime.Now,
                        CreatedBy = userID,
                        AuditID = AuditID,
                        Remarks = "New Audit Steps Assigned."
                    };

                    transactionlist.Add(transaction);

                    CreateTransaction(transactionlist);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void CreateTransaction(List<InternalAuditTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(entry);
                });

                entities.SaveChanges();
            }
        }

        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudit();
        }



        #region Implementation Status     

        public void ImpPerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        {
            List<Sp_ImplementationAuditAssignedInstancesView_Result> Records = new List<Sp_ImplementationAuditAssignedInstancesView_Result>();


            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_ImplementationAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();
            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.ProcessId == ProcessId
                            && row.SubProcessId == SubProcessID
                            && row.RoleID == RoleID && row.VerticalID == Verticalid
                            select row).FirstOrDefault();

                if (Data != null)
                {
                    ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                    ddl.Enabled = false;
                    ddl.Attributes.Add("class", "");
                    ddl.CssClass = "form-control m-bot15 Check";
                }
                else
                    ddl.Enabled = true;
            }
        }
        //public void ImpPerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        //{
        //    List<ImplementationAuditAssignedInstancesView> Records = new List<ImplementationAuditAssignedInstancesView>();


        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        Records = (from row in entities.ImplementationAuditAssignedInstancesViews
        //                   where row.CustomerID == CustID && row.AuditID == AuditID
        //                   select row).ToList();

        //    }

        //    if (Records.Count > 0)
        //    {
        //        var Data = (from row in Records
        //                    where row.CustomerBranchID == CustBranchID
        //                    && row.FinancialYear == FinYear
        //                    && row.ForMonth == Period
        //                    && row.ProcessId == ProcessId
        //                    && row.SubProcessId == SubProcessID
        //                    && row.RoleID == RoleID && row.VerticalID == Verticalid
        //                    select row).FirstOrDefault();

        //        if (Data != null)
        //        {
        //            ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
        //            ddl.Enabled = false;
        //            ddl.Attributes.Add("class", "");
        //            ddl.CssClass = "form-control m-bot15";
        //        }
        //        else
        //            ddl.Enabled = true;
        //    }
        //}
        public static void CreateScheduleOnImplementation(long ImplementationInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, int VerticalId, int ProcessId, long AuditID, int SubProcessID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.UtcNow;

                    List<AuditImplementationTransaction> transactionlist = new List<AuditImplementationTransaction>();

                    AuditImpementationScheduleOn auditScheduleon = new AuditImpementationScheduleOn();
                    auditScheduleon.ImplementationInstance = ImplementationInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = ProcessId;
                    //auditScheduleon.
                    auditScheduleon.AuditID = AuditID;
                    auditScheduleon.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    auditScheduleon.CreatedOn = DateTime.Now;
                    entities.AuditImpementationScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        ImplementationInstance = ImplementationInstance,
                        ImplementationScheduleOnID = auditScheduleon.Id,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ForPeriod = Period,
                        FinancialYear = FinancialYear,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalId,
                        ProcessId = ProcessId,
                        SubProcessId = SubProcessID,
                        Remarks = "New Implementation Steps assigned.",
                        CreatedOn = DateTime.Now,
                        AuditID = AuditID,
                    };

                    transactionlist.Add(transaction);

                    CreateTransactionImplementation(transactionlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        public static void CreateTransactionImplementation(List<AuditImplementationTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }

        protected void grdProcess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProcess.PageIndex = e.NewPageIndex;
            BindProcessAudit();
            bindPageNumber();
        }
        #endregion

        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudit();
            bindPageNumber();
        }
        public void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(file.Value);
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    foreach (string file in Directory.GetFiles(path))
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(path);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int auditorid = -1;
                    string financialYear = string.Empty;
                    string customerBranchName = string.Empty;
                    string verticalName = string.Empty;
                    #region
                    if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                    {
                        branchid = Convert.ToInt32(Request.QueryString["BID"]);
                        ViewState["BID"] = branchid;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                        ViewState["VID"] = VerticalID;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                    {
                        FinYear = Request.QueryString["FY"];
                        ViewState["FY"] = FinYear;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                    {
                        Period = Request.QueryString["Period"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AUID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                    {
                        auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    }
                    else
                    {
                        auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                    }

                    #endregion

                    #region Excel Report
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerBranchName = CustomerBranchManagement.GetByID(branchid).Name;
                    verticalName = UserManagementRisk.GetVerticalName(customerID, VerticalID);

                    if (branchid != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinYear) && !string.IsNullOrEmpty(Period))
                    {
                        if (AuditID != 0)
                        {
                            var itemlist = (from row in entities.Sp_ExportAuditStepForPerformer(branchid, VerticalID, FinYear, Period, Convert.ToInt32(AuditID))
                                            select row).ToList();
                            if (itemlist.Count > 0)
                            {
                                using (ExcelPackage exportPackge = new ExcelPackage())
                                {
                                    try
                                    {
                                        #region
                                        String FileName = String.Empty;
                                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ExternalAuditorUpload");
                                        DataTable ExcelData = null;
                                        DataView view = new System.Data.DataView((itemlist as List<Sp_ExportAuditStepForPerformer_Result>).ToDataTable());

                                        ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "FinancialYear",
                                        "Period", "ActivityTobeDone", "ProcessWalkthrough", "ActualWorkDone", "Population", "Sample", "Remark",
                                        "ObservationTitle", "Observation", "BriefObservation", "ObservationBackground", "ObservationReport", "BusinessImplication",
                                        "RootCause", "FinancialImpact", "Recomendation", "ManagementResponse", "TimeLine", "PersonResponsible", "Email", "ObservationRating",
                                        "ObservationCategory", "ObservationSubCategory", "Owner", "OwnerEmail", "ATBTID", "CustomerBranchID", "VerticalID", "AuditId");

                                        foreach (DataRow item in ExcelData.Rows)
                                        {
                                            if (item["BranchName"] != null && item["BranchName"] != DBNull.Value)
                                                item["BranchName"] = customerBranchName;

                                            if (item["VerticalName"] != null && item["VerticalName"] != DBNull.Value)
                                                item["VerticalName"] = verticalName;
                                        }
                                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["A1"].Value = "Branch Name";
                                        exWorkSheet.Cells["A1"].AutoFitColumns(35);

                                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["B1"].Value = "Vertical Name";
                                        exWorkSheet.Cells["B1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["C1"].Value = "Process Name";
                                        exWorkSheet.Cells["C1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["D1"].Value = "Sub Process Name";
                                        exWorkSheet.Cells["D1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["E1"].Value = "FinancialYear";
                                        exWorkSheet.Cells["E1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["F1"].Value = "Period";
                                        exWorkSheet.Cells["F1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["G1"].Value = "Audit Step";
                                        exWorkSheet.Cells["G1"].AutoFitColumns(50);

                                        exWorkSheet.Cells["H1"].Value = "Process Walkthrough";
                                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["H1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["I1"].Value = "Actual Work Done";
                                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["I1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["J1"].Value = "Population";
                                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["J1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["K1"].Value = "Sample";
                                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["K1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["L1"].Value = "Remark";
                                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["L1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["M1"].Value = "Observation Title";
                                        exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["M1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["N1"].Value = "Observation";
                                        exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["N1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["O1"].Value = "Brief Observation";
                                        exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["O1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["P1"].Value = "Observation Background";
                                        exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["P1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Q1"].Value = "Observation Report (Audit Commitee/None of the Above)";
                                        exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Q1"].AutoFitColumns(30);
                                        exWorkSheet.Cells["Q1"].Style.WrapText = true;

                                        exWorkSheet.Cells["R1"].Value = "Business Implication";
                                        exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["R1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["S1"].Value = "Root Cause";
                                        exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["S1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["T1"].Value = "Financial Impact";
                                        exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["T1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["U1"].Value = "Recomendation";
                                        exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["U1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["V1"].Value = "Management Response";
                                        exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["V1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["W1"].Value = "TimeLine";
                                        exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["W1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["X1"].Value = "Person Responsible";
                                        exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["X1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Y1"].Value = "Person Responsible Email";
                                        exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Y1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Z1"].Value = "Observation Rating";
                                        exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Z1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AA1"].Value = "Observation Category";
                                        exWorkSheet.Cells["AA1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AA1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AA1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AB1"].Value = "Observation Sub Category";
                                        exWorkSheet.Cells["AB1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AB1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AB1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AC1"].Value = "Owner";
                                        exWorkSheet.Cells["AC1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AC1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AC1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AD1"].Value = "Owner Email";
                                        exWorkSheet.Cells["AD1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AD1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AD1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AE1"].Value = "ATBTID";
                                        exWorkSheet.Cells["AE1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AE1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AE1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AF1"].Value = "CBID";
                                        exWorkSheet.Cells["AF1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AF1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AF1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AG1"].Value = "VID";
                                        exWorkSheet.Cells["AG1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AG1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AG1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AH1"].Value = "AUID";
                                        exWorkSheet.Cells["AH1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AH1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AH1"].AutoFitColumns(5);

                                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 34])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                        Byte[] fileBytes = exportPackge.GetAsByteArray();

                                        #endregion

                                        #region Save Code                                              
                                        string fileName = "AuditorUploadFile_" + customerBranchName + financialYear + ".xlsx";
                                        string CreatefileName = "AuditorUploadFile_" + customerBranchName + financialYear;
                                        CreatefileName = DocumentManagement.MakeValidFileName(CreatefileName);
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                        string directoryPath1 = "";
                                        directoryPath1 = Server.MapPath("~/AuditCheckListPerformer/" + customerID + "/"
                                            + Convert.ToInt32(branchid) + "/" + Convert.ToInt32(VerticalID) + "/"
                                            + Convert.ToString(financialYear) + "/" + Period + "");

                                        if (!Directory.Exists(directoryPath1))
                                        {
                                            DocumentManagement.CreateDirectory(directoryPath1);
                                        }
                                        else
                                        {
                                            DeleteDirectory(directoryPath1);
                                            DocumentManagement.CreateDirectory(directoryPath1);
                                        }
                                        string finalPath1 = Path.Combine(directoryPath1, CreatefileName + Path.GetExtension(fileName));
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                        SaveDocFiles(Filelist1);
                                        #endregion

                                        if (auditorid != -1)
                                        {
                                            #region External Audittor                                            
                                            var details = (from row in entities.Mst_AuditorMaster
                                                           where row.CustomerID == customerID
                                                           && row.ID == auditorid
                                                           select row).FirstOrDefault();

                                            string emailids = string.Empty;
                                            if (details != null)
                                            {
                                                if (!string.IsNullOrEmpty(details.PartnerEmail))
                                                {
                                                    emailids += details.PartnerEmail + ",";
                                                }
                                                if (!string.IsNullOrEmpty(details.AuditMEmail))
                                                {
                                                    emailids += details.AuditMEmail + ",";
                                                }
                                                emailids = emailids.Trim(',');

                                                string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;
                                                List<Attachment> attachment = new List<Attachment>();
                                                attachment.Add(new Attachment(finalPath1));


                                                //emailids = "rahul@avantis.info,ankit@avantis.info,narendra@avantis.info";
                                                string[] emailIsd = emailids.Split(',');


                                                string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                                  + "Dear Sir,<br /><br />"
                                                                  // + "You have been assigned to perform attached audit steps for audit -" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + " <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                                                  + "Kindly note that an audit has been assigned to you. The attachment below has the list of audit steps which are required to be completed during the audit.<br/><br/><br/>Kindly update the details of the audit in the attached sheet and send it back for review and system updation. <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string subject = "Audit Steps-" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + "";

                                                if (subject.Length > 75)
                                                    subject = "Audit Steps for Upload";


                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "External Auditor Upload.", message, attachment);

                                                AuditKickOffMailLog AKOML = new AuditKickOffMailLog()
                                                {
                                                    AuditId = (int)AuditID,
                                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    CustomerID = customerID
                                                };

                                                if (ProcessManagement.AuditKickOffLogExists(customerID, (int)AuditID))
                                                {
                                                    return;
                                                }
                                                else
                                                {
                                                    ProcessManagement.CreateAuditKickOffLog(AKOML);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Internal User


                                            var details = (from ICAA in entities.InternalControlAuditAssignments
                                                           join IASO in entities.InternalAuditScheduleOns
                                                           on ICAA.AuditID equals IASO.AuditID
                                                           join mu in entities.mst_User
                                                           on ICAA.UserID equals mu.ID
                                                           where ICAA.InternalAuditInstance == IASO.InternalAuditInstance
                                                           && IASO.ProcessId == ICAA.ProcessId
                                                           && IASO.FinancialYear == financialYear
                                                           && IASO.ForMonth == Period
                                                           && ICAA.CustomerBranchID == branchid
                                                           && ICAA.VerticalID == VerticalID
                                                           && mu.IsDeleted == false
                                                           && mu.IsActive == true
                                                           && ICAA.RoleID == 3
                                                           && ICAA.AuditID == AuditID
                                                           select mu.Email).Distinct().ToList();

                                            string emailids = string.Empty;
                                            if (details != null)
                                            {
                                                foreach (var item in details)
                                                {
                                                    if (!string.IsNullOrEmpty(item))
                                                    {
                                                        emailids += item + ",";
                                                    }
                                                }
                                                emailids = emailids.Trim(',');

                                                string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                                                List<Attachment> attachment = new List<Attachment>();
                                                attachment.Add(new Attachment(finalPath1));

                                                //emailids = "rahul@avantis.info,ankit@avantis.info,narendra@avantis.info";
                                                string[] emailIsd = emailids.Split(',');

                                                //string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                //+ "Dear Sir,<br /><br />"
                                                //+ "Here is Attached Audit Steps <br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                   + "Dear Sir,<br /><br />" +
                                                  //+ "You have been assigned to perform attached audit steps for audit -" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + " <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                                  "Kindly note that an audit has been assigned to you. The attachment below has the list of audit steps which are required to be completed during the audit.<br/><br/> Kindly update the details of the audit in the attached sheet and send it back for review and system updation. <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string subject = "Audit Steps-" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + "";

                                                if (subject.Length > 75)
                                                    subject = "Audit Steps for Upload";

                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Internal Auditor Upload.", message, attachment);

                                                AuditKickOffMailLog AKOML = new AuditKickOffMailLog()
                                                {
                                                    AuditId = (int)AuditID,
                                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    CustomerID = customerID
                                                };

                                                if (ProcessManagement.AuditKickOffLogExists(customerID, (int)AuditID))
                                                {
                                                    return;
                                                }
                                                else
                                                {
                                                    ProcessManagement.CreateAuditKickOffLog(AKOML);
                                                }
                                            }
                                            #endregion

                                        }
                                        #region Excel Download Code
                                        //Response.ClearContent();
                                        //Response.Buffer = true;
                                        ////string fileName = string.Empty;
                                        //fileName = "asd.xlsx";
                                        //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                        //Response.Charset = "";
                                        //Response.ContentType = "application/vnd.ms-excel";
                                        //StringWriter sw = new StringWriter();
                                        //Response.BinaryWrite(fileBytes);
                                        //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                        #endregion

                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                            }//itemlist.Count > 0 end
                        }//auditID != 0 end
                    }//customerBranchId != -1 && verticalID != -1 && !string.IsNullOrEmpty(financialYear) && !string.IsNullOrEmpty(Period) end
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public static long GetPerformerID(string ForPeriod, string FinancialYear, int CustomerBranchid, int VerticalId, int ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var reviewerid = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.InternalAuditInstance equals row1.InternalAuditInstance
                                  where row.ProcessId == row1.ProcessId && row.CustomerBranchID == CustomerBranchid &&
                                  row.VerticalID == VerticalId &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row.ProcessId == ProcessID
                                  && row.RoleID == 3
                                  select row.UserID).FirstOrDefault();

                return (long)reviewerid;
            }
        }
        private void ProcessReminderPerformer(string ForPeriod, string FinancialYear, int CustomerBranchid, int VerticalId, int ProcessID)
        {
            try
            {
                long userid = GetPerformerID(ForPeriod, FinancialYear, CustomerBranchid, VerticalId, ProcessID);
                var user = UserManagement.GetByID(Convert.ToInt32(userid));
                if (user != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

                    List<Attachment> attachment = new List<Attachment>();
                    attachment.Add(new Attachment("c:/textfile.txt"));

                    string emailids = "rahul@avantis.info,ankit@avantis.info,narendra@avantis.info";
                    string[] emailIsd = emailids.Split(',');


                    string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                     + "Dear Sir,<br /><br />"
                                     + "Here is Attached dashboard summary pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Approver Dashboards.", message, attachment);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


    }
}