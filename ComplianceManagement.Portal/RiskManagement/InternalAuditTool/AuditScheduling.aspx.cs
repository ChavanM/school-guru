﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditScheduling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                            
                BindSchedulingType();
                BindFinancialYear();                
                BindMainGrid(-1,-1);
                BindLocationFilter();
                BindLocation();
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                }
                tvLocation.CollapseAll();
                tvLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            int customerbranchid = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (tvFilterLocation.SelectedValue != "-1")
            {
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
            }
            BindMainGrid(customerID, customerbranchid);
        }
        protected void tvLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxLocation.Text = tvLocation.SelectedNode.Text;
            if (ddlSchedulingType.SelectedItem.Text != "< Select Scheduling Type >")
            {
                ddlSchedulingType_SelectedIndexChanged(sender, e);
            }
            upComplianceDetails.Update();
            ForceCloseFilterBranchesTreeView();
        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView1", "$(\"#divLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        public void BindAuditSchedule(string flag, int count,int Branchid)
        {
            try
            {
                //if (flag == "A")
                //{
                //    cleardatasource();
                //    grdAnnually.DataSource = ProcessManagement.GetSPAnnualyDisplay(Branchid);
                //    grdAnnually.DataBind();

                //    //List<SP_Annualy_Result3> r = new List<SP_Annualy_Result3>();
                //    //r= ProcessManagement.GetSPAnnualyDisplay(Branchid);
                //    //DataTable dt = new DataTable();
                //    //dt = (grdAnnually.DataSource as List<SP_Annualy_Result3>).ToDataTable();
                //    //grdAnnually.DataSource = dt;
                //    //grdAnnually.DataBind();
                //}
                //else if (flag == "H")
                //{
                //    cleardatasource();
                //    grdHalfYearly.DataSource = ProcessManagement.GetSPHalfYearlyDisplay(Branchid);
                //    grdHalfYearly.DataBind();
                //}
                //else if (flag == "Q")
                //{
                //    cleardatasource();
                //    grdQuarterly.DataSource = ProcessManagement.GetSPQuarterwiseDisplay(Branchid);
                //    grdQuarterly.DataBind();
                //}
                //else if (flag == "M")
                //{
                //    cleardatasource();
                //    grdMonthly.DataSource = ProcessManagement.GetSPMonthlyDisplay(Branchid);
                //    grdMonthly.DataBind();
                //}
                //else
                //{
                //    if (count == 1)
                //    {
                //        cleardatasource();
                //        grdphase1.DataSource = ProcessManagement.GetSP_Phase1Display(Branchid);
                //        grdphase1.DataBind();
                //    }
                //    else if (count == 2)
                //    {
                //        cleardatasource();
                //        grdphase2.DataSource = ProcessManagement.GetSP_Phase2Display(Branchid);
                //        grdphase2.DataBind();
                //    }
                //    else if (count == 3)
                //    {
                //        cleardatasource();
                //        grdphase3.DataSource = ProcessManagement.GetSP_Phase3Display(Branchid);
                //        grdphase3.DataBind();
                //    }
                //    else if (count == 4)
                //    {
                //        cleardatasource();
                //        grdphase4.DataSource = ProcessManagement.GetSP_Phase4Display(Branchid);
                //        grdphase4.DataBind();
                //    }
                //    else
                //    {
                //        cleardatasource();
                //        grdphase5.DataSource = ProcessManagement.GetSP_Phase5Display(Branchid);
                //        grdphase5.DataBind();
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divLocation');", tbxLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter1", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divLocation');", tbxLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter1", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public void BindSchedulingType()
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.Items.Clear();
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingType();
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("< Select Scheduling Type >", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("< Select Financial Year >", "-1"));
        }
        public void EnableDisable(int noofphases)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    pnlAnnually.Visible = true;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = true;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = true;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = true;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    if (noofphases == 1)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = true;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 2)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = true;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 3)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = true;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 4)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = true;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 5)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = true;
                        Divnophase.Visible = true;
                    }
                }
                else
                {
                    Divnophase.Visible = false;
                }
            }
        }
        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        public string ShowProcessName(long Processid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false
                             && row.Id == Processid
                             select row).FirstOrDefault();
                if (query !=null)
                {
                    processnonprocess = query.Name;
                }               
            }
            return processnonprocess.Trim(',');
        }
        public string ShowDate(DateTime dt)
        {
            try
            {                
                DateTime dt1 = dt;
                if (dt1 == null)
                    return String.Empty;
                else
                return dt1.ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
       
        public void cleardatasource()
        {
            grdAnnually.DataSource = null;
            grdAnnually.DataBind();
            grdHalfYearly.DataSource = null;
            grdHalfYearly.DataBind();
            grdQuarterly.DataSource = null;
            grdQuarterly.DataBind();
            grdMonthly.DataSource = null;
            grdMonthly.DataBind();
            grdphase1.DataSource = null;
            grdphase1.DataBind();
            grdphase2.DataSource = null;
            grdphase2.DataBind();
            grdphase3.DataSource = null;
            grdphase3.DataBind();
            grdphase4.DataSource = null;
            grdphase4.DataBind();
            grdphase5.DataSource = null;
            grdphase5.DataBind();
        }
       
      
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text != "< Select Scheduling Type >")
            {
                if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
                {
                    if (tvLocation.SelectedValue != "-1")
                    {
                        if (ddlSchedulingType.SelectedItem.Text == "Annually")
                        {
                            EnableDisable(0);
                            BindAuditSchedule("A", 0, Convert.ToInt32(tvLocation.SelectedValue));
                        }
                        else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                        {
                            EnableDisable(0);
                            BindAuditSchedule("H", 0, Convert.ToInt32(tvLocation.SelectedValue));
                        }
                        else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                        {
                            EnableDisable(0);
                            BindAuditSchedule("Q", 0, Convert.ToInt32(tvLocation.SelectedValue));
                        }
                        else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                        {
                            EnableDisable(0);
                            BindAuditSchedule("M", 0, Convert.ToInt32(tvLocation.SelectedValue));
                        }
                        else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                        {
                            //EnableDisable(0);
                            if (ddlSchedulingType.SelectedItem.Text == "Phase")
                            {
                                Divnophase.Visible = true;
                            }
                            else
                            {
                                Divnophase.Visible = false;
                            }
                            cleardatasource();
                        }
                        ForceCloseFilterBranchesTreeView();
                    }
                }
            }
            else
            {
                cleardatasource();
            }
        }
        protected void txtNoOfPhases_TextChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                {
                    EnableDisable(Convert.ToInt32(txtNoOfPhases.Text));
                    if (tvLocation.SelectedValue != "-1")
                    {
                        BindAuditSchedule("P", Convert.ToInt32(txtNoOfPhases.Text), Convert.ToInt32(tvLocation.SelectedValue));
                    }
                }
            }
        }
        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                tbxLocation.Text = "< All >";
                //ddlEntity.SelectedValue = "-1";
                ddlFinancialYear.SelectedValue = "-1";
                //ddlLocation.SelectedValue = "-1";
                ddlSchedulingType.SelectedValue = "-1";
                cleardatasource();
                EnableDisable(0);
                Divnophase.Visible = false;
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;
                txtNoOfPhases.Text = string.Empty;
                //BindLocation();
                //upComplianceDetails.Update();
                //
                
                ForceCloseFilterBranchesTreeView();
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divAuditSchedulingDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindMainGrid(int customerid ,int customerbranchid)
        {
            try
            {

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (customerid != -1 && customerbranchid != -1)
                    {
                        var query = (from row in entities.InternalAuditSchedulings
                                    join MCB in entities.mst_CustomerBranch
                                    on row.CustomerBranchId equals MCB.ID
                                    where row.TermStatus == true && row.IsDeleted==false && MCB.CustomerID == customerid
                                    && row.CustomerBranchId == customerbranchid 
                                select new InternalAuditSchedulingClass()
                                {
                                    CustomerId=MCB.CustomerID,
                                    FinancialYear = row.FinancialYear,
                                    CustomerBranchId = row.CustomerBranchId,
                                    ISAHQMP = row.ISAHQMP,
                                    TermName=row.TermName
                                }).Distinct().ToList();                        
                        grdAuditScheduling.DataSource = null;
                        grdAuditScheduling.DataBind();
                        if (query != null)
                        {
                            grdAuditScheduling.DataSource = query;
                            grdAuditScheduling.DataBind();
                        }
                    }
                    else if (customerid != -1)
                    {
                        var query = (from row in entities.InternalAuditSchedulings
                                     join MCB in entities.mst_CustomerBranch
                                     on row.CustomerBranchId equals MCB.ID
                                     where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
                                     select new InternalAuditSchedulingClass()
                                     {
                                         CustomerId = MCB.CustomerID,
                                         FinancialYear = row.FinancialYear,
                                         CustomerBranchId = row.CustomerBranchId,
                                         ISAHQMP = row.ISAHQMP,
                                         TermName = row.TermName
                                     }).Distinct().ToList();
                        grdAuditScheduling.DataSource = null;
                        grdAuditScheduling.DataBind();
                        if (query != null)
                        {
                            grdAuditScheduling.DataSource = query;
                            grdAuditScheduling.DataBind();
                        }
                    }
                    else
                    {
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        var query = (from row in entities.InternalAuditSchedulings
                                     join MCB in entities.mst_CustomerBranch
                                     on row.CustomerBranchId equals MCB.ID
                                     where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerID
                                     select new InternalAuditSchedulingClass()
                                     {
                                         CustomerId = MCB.CustomerID,
                                         FinancialYear = row.FinancialYear,
                                         CustomerBranchId = row.CustomerBranchId,
                                         ISAHQMP = row.ISAHQMP,
                                         TermName = row.TermName
                                     }).Distinct().ToList();
                        grdAuditScheduling.DataSource = null;
                        grdAuditScheduling.DataBind();
                        if (query != null)
                        {
                            grdAuditScheduling.DataSource = query;
                            grdAuditScheduling.DataBind();
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] fsplit = financialyear.Split('-');
                    string fyear = fsplit[0];
                    string syear = fsplit[1];
                    string f1 = fyear + "-" + syear;
                    string f2 = Convert.ToInt32(fyear) + 1 + "-" + (Convert.ToInt32(syear) + 1);
                    string f3 = (Convert.ToInt32(fyear) + 1) + 1 + "-" + ((Convert.ToInt32(syear) + 1) + 1);
                    #region Annualy Save
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                       // grdAnnually.ShowHeader = false;                          
                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                        int processid = -1;
                        //GridViewRow gvrow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        //grdAnnually.HeaderRow.Controls.RemoveAt(0);                       
                        //string a=grdAnnually.HeaderRow.Cells[0].Text;//.Controls.RemoveAt(0);    
                        // string b=grdAnnually.HeaderRow.Cells[1].Text;
                        // string c=grdAnnually.HeaderRow.Cells[2].Text;
                        // string d=grdAnnually.HeaderRow.Cells[3].Text;
                        //a1.Update();
                        //foreach (GridViewRow row in grdAnnually.Rows)
                        //{
                           
                        //    //grdAnnually.Controls[0].Controls.RemoveAt(0);
                        //    if (row.RowType == DataControlRowType.DataRow)
                        //    {                                
                        //        CheckBox bf = (row.Cells[2].FindControl("chkAnnualy1") as CheckBox);
                        //        CheckBox bf1 = (row.Cells[3].FindControl("chkAnnualy2") as CheckBox);
                        //        CheckBox bf2 = (row.Cells[4].FindControl("chkAnnualy3") as CheckBox);
                        //        string lblProcessId = (row.Cells[0].FindControl("lblProcessID") as Label).Text;
                        //        if (!string.IsNullOrEmpty(lblProcessId))
                        //        {
                        //            processid = Convert.ToInt32(lblProcessId);                                  
                        //            if (bf.Checked)
                        //            {
                        //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                        //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                        //                Internalauditscheduling.FinancialYear = f1;
                        //                Internalauditscheduling.TermName = "Annually";
                        //                Internalauditscheduling.TermStatus = true;
                        //                Internalauditscheduling.Process = processid;
                        //                Internalauditscheduling.ISAHQMP = "A";
                        //                if (processid != 0)
                        //                {
                        //                    InternalauditschedulingList.Add(Internalauditscheduling);
                        //                }
                        //            }
                        //            if (bf1.Checked)
                        //            {
                        //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                        //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                        //                Internalauditscheduling.FinancialYear = f2;
                        //                Internalauditscheduling.TermName = "Annually";
                        //                Internalauditscheduling.TermStatus = true;
                        //                Internalauditscheduling.Process = processid;
                        //                Internalauditscheduling.ISAHQMP = "A";
                        //                if (processid != 0)
                        //                {
                        //                    InternalauditschedulingList.Add(Internalauditscheduling);
                        //                }
                        //            }
                        //            if (bf2.Checked)
                        //            {
                        //                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                        //                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                        //                Internalauditscheduling.FinancialYear = f3;
                        //                Internalauditscheduling.TermName = "Annually";
                        //                Internalauditscheduling.TermStatus = true;
                        //                Internalauditscheduling.Process = processid;
                        //                Internalauditscheduling.ISAHQMP = "A";
                        //                if (processid != 0)
                        //                {
                        //                    InternalauditschedulingList.Add(Internalauditscheduling);
                        //                }
                        //            }
                        //        }                         

                        //    }
                        //}

                        for (int i = 0; i < grdAnnually.Rows.Count; i++)
                        {

                            GridViewRow row = grdAnnually.Rows[i];
                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                            {
                                processid = Convert.ToInt32(lblProcessId.Text);
                                CheckBox bf = (CheckBox)row.FindControl("chkAnnualy1");
                                CheckBox bf1 = (CheckBox)row.FindControl("chkAnnualy2");
                                CheckBox bf2 = (CheckBox)row.FindControl("chkAnnualy3");
                                if (bf.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Annually";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "A";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                if (bf1.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Annually";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "A";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                if (bf2.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Annually";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "A";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                            }
                        }
                        if (InternalauditschedulingList.Count != 0)
                        {
                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                        }
                    }
                    #endregion
                    #region Half Yearly Save
                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                        int processid = -1;
                        for (int i = 0; i < grdHalfYearly.Rows.Count; i++)
                        {
                            GridViewRow row = grdHalfYearly.Rows[i];
                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                            {
                                processid = Convert.ToInt32(lblProcessId.Text);
                                CheckBox bf = (CheckBox)row.FindControl("chkHalfyearly1");
                                CheckBox bf1 = (CheckBox)row.FindControl("chkHalfyearly2");
                                CheckBox bf2 = (CheckBox)row.FindControl("chkHalfyearly3");
                                CheckBox bf3 = (CheckBox)row.FindControl("chkHalfyearly4");
                                CheckBox bf4 = (CheckBox)row.FindControl("chkHalfyearly5");
                                CheckBox bf5 = (CheckBox)row.FindControl("chkHalfyearly6");
                                //first financial Year
                                if (bf.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Apr-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Apr-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf1.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Oct-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Oct-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}//Second financial Year
                                if (bf2.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Apr-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Apr-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf3.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Oct-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Oct-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}//Third financial Year
                                if (bf4.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Apr-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Apr-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf5.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Oct-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "H";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Oct-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "H";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                            }
                        }
                        if (InternalauditschedulingList.Count != 0)
                        {
                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                        }
                    }
                    #endregion
                    #region Quarterly Save
                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        int processid = -1;
                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                        for (int i = 0; i < grdQuarterly.Rows.Count; i++)
                        {

                            GridViewRow row = grdQuarterly.Rows[i];
                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                            {
                                processid = Convert.ToInt32(lblProcessId.Text);
                                //first financial Year
                                CheckBox bf1 = (CheckBox)row.FindControl("chkQuarter1");
                                CheckBox bf2 = (CheckBox)row.FindControl("chkQuarter2");
                                CheckBox bf3 = (CheckBox)row.FindControl("chkQuarter3");
                                CheckBox bf4 = (CheckBox)row.FindControl("chkQuarter4");
                                //Second financial Year
                                CheckBox bf5 = (CheckBox)row.FindControl("chkQuarter5");
                                CheckBox bf6 = (CheckBox)row.FindControl("chkQuarter6");
                                CheckBox bf7 = (CheckBox)row.FindControl("chkQuarter7");
                                CheckBox bf8 = (CheckBox)row.FindControl("chkQuarter8");
                                //Third financial Year
                                CheckBox bf9 = (CheckBox)row.FindControl("chkQuarter9");
                                CheckBox bf10 = (CheckBox)row.FindControl("chkQuarter10");
                                CheckBox bf11 = (CheckBox)row.FindControl("chkQuarter11");
                                CheckBox bf12 = (CheckBox)row.FindControl("chkQuarter12");
                                //first financial Year
                                if (bf1.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Apr-Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Apr-Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf2.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Jul-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Jul-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf3.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Oct-Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Oct-Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf4.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Jan-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Jan-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                //Second financial Year                          
                                if (bf5.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Apr-Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Apr-Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf6.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Jul-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Jul-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf7.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Oct-Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Oct-Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf8.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Jan-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Jan-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                //Third financial Year
                                if (bf9.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Apr-Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Apr-Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf10.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Jul-Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Jul-Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf11.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Oct-Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Oct-Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf12.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Jan-Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "Q";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Jan-Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "Q";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                            }
                        }
                        if (InternalauditschedulingList.Count != 0)
                        {
                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                        }
                    }
                    #endregion
                    #region Monthly Save
                    else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                    {
                        int processid = -1;
                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                        for (int i = 0; i < grdMonthly.Rows.Count; i++)
                        {

                            GridViewRow row = grdMonthly.Rows[i];
                            Label lblProcessId = (Label)row.FindControl("lblProcessID");
                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                            {
                                processid = Convert.ToInt32(lblProcessId.Text);
                                //first financial Year
                                CheckBox bf1 = (CheckBox)row.FindControl("chkMonthly1");
                                CheckBox bf2 = (CheckBox)row.FindControl("chkMonthly2");
                                CheckBox bf3 = (CheckBox)row.FindControl("chkMonthly3");
                                CheckBox bf4 = (CheckBox)row.FindControl("chkMonthly4");
                                CheckBox bf5 = (CheckBox)row.FindControl("chkMonthly5");
                                CheckBox bf6 = (CheckBox)row.FindControl("chkMonthly6");
                                CheckBox bf7 = (CheckBox)row.FindControl("chkMonthly7");
                                CheckBox bf8 = (CheckBox)row.FindControl("chkMonthly8");
                                CheckBox bf9 = (CheckBox)row.FindControl("chkMonthly9");
                                CheckBox bf10 = (CheckBox)row.FindControl("chkMonthly10");
                                CheckBox bf11 = (CheckBox)row.FindControl("chkMonthly11");
                                CheckBox bf12 = (CheckBox)row.FindControl("chkMonthly12");
                                //Second financial Year
                                CheckBox bf13 = (CheckBox)row.FindControl("chkMonthly13");
                                CheckBox bf14 = (CheckBox)row.FindControl("chkMonthly14");
                                CheckBox bf15 = (CheckBox)row.FindControl("chkMonthly15");
                                CheckBox bf16 = (CheckBox)row.FindControl("chkMonthly16");
                                CheckBox bf17 = (CheckBox)row.FindControl("chkMonthly17");
                                CheckBox bf18 = (CheckBox)row.FindControl("chkMonthly18");
                                CheckBox bf19 = (CheckBox)row.FindControl("chkMonthly19");
                                CheckBox bf20 = (CheckBox)row.FindControl("chkMonthly20");
                                CheckBox bf21 = (CheckBox)row.FindControl("chkMonthly21");
                                CheckBox bf22 = (CheckBox)row.FindControl("chkMonthly22");
                                CheckBox bf23 = (CheckBox)row.FindControl("chkMonthly23");
                                CheckBox bf24 = (CheckBox)row.FindControl("chkMonthly24");
                                //Third financial Year
                                CheckBox bf25 = (CheckBox)row.FindControl("chkMonthly25");
                                CheckBox bf26 = (CheckBox)row.FindControl("chkMonthly26");
                                CheckBox bf27 = (CheckBox)row.FindControl("chkMonthly27");
                                CheckBox bf28 = (CheckBox)row.FindControl("chkMonthly28");
                                CheckBox bf29 = (CheckBox)row.FindControl("chkMonthly29");
                                CheckBox bf30 = (CheckBox)row.FindControl("chkMonthly30");
                                CheckBox bf31 = (CheckBox)row.FindControl("chkMonthly31");
                                CheckBox bf32 = (CheckBox)row.FindControl("chkMonthly32");
                                CheckBox bf33 = (CheckBox)row.FindControl("chkMonthly33");
                                CheckBox bf34 = (CheckBox)row.FindControl("chkMonthly34");
                                CheckBox bf35 = (CheckBox)row.FindControl("chkMonthly35");
                                CheckBox bf36 = (CheckBox)row.FindControl("chkMonthly36");
                                //first financial Year
                                if (bf1.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Apr";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Apr";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf2.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "May";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "May";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf3.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf4.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Jul";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Jul";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf5.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Aug";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Aug";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf6.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf7.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Oct";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Oct";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf8.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Nov";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Nov";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf9.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf10.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Jan";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Jan";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf11.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Feb";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Feb";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf12.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f1;
                                    Internalauditscheduling.TermName = "Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f1;
                                //    Internalauditscheduling.TermName = "Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                //Second financial Year 
                                if (bf13.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Apr";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Apr";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf14.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "May";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "May";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf15.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf16.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Jul";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Jul";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf17.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Aug";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Aug";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf18.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf19.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Oct";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Oct";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf20.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Nov";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Nov";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf21.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf22.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Jan";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Jan";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf23.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Feb";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Feb";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf24.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f2;
                                    Internalauditscheduling.TermName = "Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f2;
                                //    Internalauditscheduling.TermName = "Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                //Third financial Year

                                if (bf25.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Apr";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Apr";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf26.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "May";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "May";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf27.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Jun";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Jun";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf28.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Jul";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Jul";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf29.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Aug";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Aug";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf30.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Sep";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Sep";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf31.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Oct";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Oct";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf32.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Nov";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Nov";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}

                                if (bf33.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Dec";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Dec";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf34.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Jan";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Jan";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf35.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Feb";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Feb";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                                if (bf36.Checked)
                                {
                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                    Internalauditscheduling.FinancialYear = f3;
                                    Internalauditscheduling.TermName = "Mar";
                                    Internalauditscheduling.TermStatus = true;
                                    Internalauditscheduling.Process = processid;
                                    Internalauditscheduling.ISAHQMP = "M";
                                    if (processid != 0)
                                    {
                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                    }
                                }
                                //else
                                //{
                                //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                //    Internalauditscheduling.FinancialYear = f3;
                                //    Internalauditscheduling.TermName = "Mar";
                                //    Internalauditscheduling.TermStatus = false;
                                //    Internalauditscheduling.Process = processid;
                                //    Internalauditscheduling.ISAHQMP = "M";
                                //    if (processid != 0)
                                //    {
                                //        InternalauditschedulingList.Add(Internalauditscheduling);
                                //    }
                                //}
                            }
                        }
                        if (InternalauditschedulingList.Count != 0)
                        {
                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                        }
                    }
                    #endregion
                    #region Phase Save
                    else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        int processid = -1;
                        int noofphases = -1;
                        if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                        {
                            noofphases = (Convert.ToInt32(txtNoOfPhases.Text));
                        }
                        #region Phase 1
                        if (noofphases == 1)
                        {
                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                            for (int i = 0; i < grdphase1.Rows.Count; i++)
                            {

                                GridViewRow row = grdphase1.Rows[i];
                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                {
                                    processid = Convert.ToInt32(lblProcessId.Text);
                                    CheckBox bf = (CheckBox)row.FindControl("chkPhase1");
                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase2");
                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase3");

                                    if (bf.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf1.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf2.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}

                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            }
                        }
                        #endregion
                        #region Phase 2
                        else if (noofphases == 2)
                        {
                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                            for (int i = 0; i < grdphase2.Rows.Count; i++)
                            {

                                GridViewRow row = grdphase2.Rows[i];
                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                {
                                    processid = Convert.ToInt32(lblProcessId.Text);
                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                    CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                    CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                    if (bf1.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf2.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Second Finacial Year
                                    if (bf3.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf4.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Third Financial Year
                                    if (bf5.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf6.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}

                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            }
                        }
                        #endregion
                        #region Phase 3
                        else if (noofphases == 3)
                        {
                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                            for (int i = 0; i < grdphase3.Rows.Count; i++)
                            {

                                GridViewRow row = grdphase3.Rows[i];
                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                {
                                    processid = Convert.ToInt32(lblProcessId.Text);
                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                    CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                    CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                    CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                    CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                    CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                    if (bf1.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf2.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf3.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Second Finacial Year                               
                                    if (bf4.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf5.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf6.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Third Financial Year                                
                                    if (bf7.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf8.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf9.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}

                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            }
                        }
                        #endregion
                        #region Phase 4
                        else if (noofphases == 4)
                        {
                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                            for (int i = 0; i < grdphase4.Rows.Count; i++)
                            {

                                GridViewRow row = grdphase4.Rows[i];
                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                {
                                    processid = Convert.ToInt32(lblProcessId.Text);
                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                    CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                    CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                    CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                    CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                    CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                    CheckBox bf10 = (CheckBox)row.FindControl("chkPhase10");
                                    CheckBox bf11 = (CheckBox)row.FindControl("chkPhase11");
                                    CheckBox bf12 = (CheckBox)row.FindControl("chkPhase12");
                                    if (bf1.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf2.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf3.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf4.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Second Finacial Year                               
                                    if (bf5.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf6.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf7.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf8.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Third Financial Year                                
                                    if (bf9.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf10.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf11.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf12.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}

                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            }
                        }
                        #endregion
                        #region Phase 5
                        else if (noofphases == 5)
                        {
                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                            for (int i = 0; i < grdphase5.Rows.Count; i++)
                            {

                                GridViewRow row = grdphase5.Rows[i];
                                Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                {
                                    processid = Convert.ToInt32(lblProcessId.Text);
                                    CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                    CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                    CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                    CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                    CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                    CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                    CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                    CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                    CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                    CheckBox bf10 = (CheckBox)row.FindControl("chkPhase10");
                                    CheckBox bf11 = (CheckBox)row.FindControl("chkPhase11");
                                    CheckBox bf12 = (CheckBox)row.FindControl("chkPhase12");
                                    CheckBox bf13 = (CheckBox)row.FindControl("chkPhase10");
                                    CheckBox bf14 = (CheckBox)row.FindControl("chkPhase11");
                                    CheckBox bf15 = (CheckBox)row.FindControl("chkPhase12");
                                    if (bf1.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf2.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf3.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf4.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf5.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f1;
                                        Internalauditscheduling.TermName = "Phase5";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f1;
                                    //    Internalauditscheduling.TermName = "Phase5";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Second Finacial Year                               
                                    if (bf6.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf7.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf8.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf9.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf10.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f2;
                                        Internalauditscheduling.TermName = "Phase5";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f2;
                                    //    Internalauditscheduling.TermName = "Phase5";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    //Third Financial Year                                
                                    if (bf11.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase1";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase1";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf12.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase2";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase2";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf13.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase3";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase3";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf14.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase4";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase4";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                    if (bf15.Checked)
                                    {
                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(tvLocation.SelectedValue);
                                        Internalauditscheduling.FinancialYear = f3;
                                        Internalauditscheduling.TermName = "Phase5";
                                        Internalauditscheduling.TermStatus = true;
                                        Internalauditscheduling.Process = processid;
                                        Internalauditscheduling.ISAHQMP = "P";
                                        Internalauditscheduling.PhaseCount = noofphases;
                                        if (processid != 0)
                                        {
                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                        }
                                    }
                                    //else
                                    //{
                                    //    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                    //    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlLocation.SelectedValue);
                                    //    Internalauditscheduling.FinancialYear = f3;
                                    //    Internalauditscheduling.TermName = "Phase5";
                                    //    Internalauditscheduling.TermStatus = false;
                                    //    Internalauditscheduling.Process = processid;
                                    //    Internalauditscheduling.ISAHQMP = "P";
                                    //    Internalauditscheduling.PhaseCount = noofphases;
                                    //    if (processid != 0)
                                    //    {
                                    //        InternalauditschedulingList.Add(Internalauditscheduling);
                                    //    }
                                    //}
                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                            }
                        }
                        #endregion
                    }
                    #endregion

            
                    int customerbranchid = -1;
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    BindMainGrid(customerID, customerbranchid);
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void btnSaveSchedule_Click(object sender, EventArgs e)
         {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                //int ID = -1;
                for (int i = 0; i < grdAuditScheduleStartEndDate.Rows.Count; i++)
                {
                    GridViewRow row = grdAuditScheduleStartEndDate.Rows[i];
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtStartDate = (TextBox)row.FindControl("txtExpectedStartDate");
                    TextBox txtEndDate = (TextBox)row.FindControl("txtExpectedEndDate");

                    if (txtStartDate.Text != "" && txtEndDate.Text != "")
                    {
                        if (!string.IsNullOrEmpty(lblID.Text))
                        {

                            //DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            //DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
               

                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                            Internalauditscheduling.Id = Convert.ToInt32(lblID.Text);
                            Internalauditscheduling.StartDate = GetDate(a.ToString("dd/MM/yyyy"));
                            Internalauditscheduling.EndDate = GetDate(b.ToString("dd/MM/yyyy"));
                            ProcessManagement.UpdateInternalAuditorScheduling(Internalauditscheduling);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool IsValidDateFormat(string dateFormat)
        {
            try
            {
                String dts = DateTime.Now.ToString(dateFormat);
                DateTime.ParseExact(dts, dateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        #region Annually
        protected void grdAnnually_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
                //{
                //    string financialyear = ddlFinancialYear.SelectedItem.Text;
                //    string[] a = financialyear.Split('-');
                //    string aaa = a[0];
                //    string bbb = a[1];
                //    string f1 = aaa + "-" + bbb;
                //    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                //    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                //    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                //    {

                //        if (e.Row.RowType == DataControlRowType.Header)
                //        {
                //            GridView HeaderGrid = (GridView)sender;
                //            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);



                //            TableCell cell0 = new TableCell();
                //            cell0.Text = "Process";
                //            cell0.HorizontalAlign = HorizontalAlign.Center;
                //            cell0.ColumnSpan = 1;

                //            TableCell cell1 = new TableCell();
                //            cell1.Text = f1;
                //            cell1.HorizontalAlign = HorizontalAlign.Center;
                //            cell1.ColumnSpan = 1;

                //            TableCell cell2 = new TableCell();
                //            cell2.Text = f2;
                //            cell2.HorizontalAlign = HorizontalAlign.Center;
                //            cell2.ColumnSpan = 1;

                //            TableCell cell3 = new TableCell();
                //            cell3.Text = f3;
                //            cell3.HorizontalAlign = HorizontalAlign.Center;
                //            cell3.ColumnSpan = 1;

                //            HeaderGridRow.Cells.Add(cell0);
                //            HeaderGridRow.Cells.Add(cell1);
                //            HeaderGridRow.Cells.Add(cell2);
                //            HeaderGridRow.Cells.Add(cell3);


                //            grdAnnually.Controls[0].Controls.AddAt(0, HeaderGridRow);

                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdAnnually_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        GridViewRow gvRow = e.Row;
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            GridViewRow gvrow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                            TableCell cell0 = new TableCell();
                            cell0.Text = "Process";
                            cell0.HorizontalAlign = HorizontalAlign.Center;
                            cell0.ColumnSpan = 1;

                            TableCell cell1 = new TableCell();
                            cell1.Text = f1;
                            cell1.HorizontalAlign = HorizontalAlign.Center;
                            cell1.ColumnSpan = 1;

                            TableCell cell2 = new TableCell();
                            cell2.Text = f2;
                            cell2.HorizontalAlign = HorizontalAlign.Center;
                            cell2.ColumnSpan = 1;

                            TableCell cell3 = new TableCell();
                            cell3.Text = f3;
                            cell3.HorizontalAlign = HorizontalAlign.Center;
                            cell3.ColumnSpan = 1;

                            gvrow.Cells.Add(cell0);
                            gvrow.Cells.Add(cell1);
                            gvrow.Cells.Add(cell2);
                            gvrow.Cells.Add(cell3);
                            grdAnnually.Controls[0].Controls.AddAt(0, gvrow);

                        }
                        else if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                                //LinkButton lbtDelete = (LinkButton)e.Row.FindControl("LinkButton2");      
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkAnnualy1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkAnnualy2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkAnnualy3");
                            if (bf.Checked)
                            {
                                string r = "1";
                            }
                            if (bf1.Checked)
                            {
                                string r1 = "2";
                            }
                            if (bf2.Checked)
                            {
                                string r2 = "3";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdAnnually_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAnnually.PageIndex = e.NewPageIndex;
            if (tvLocation.SelectedValue != "-1")
            {
                BindAuditSchedule("A", 0, Convert.ToInt32(tvLocation.SelectedValue));
            }
        }
        #endregion
        #region Haly Yearly
        protected void grdHalfYearly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 2;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 2;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 2;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdHalfYearly.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdHalfYearly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
            //this.BindData("P");
        }
        #endregion
        #region Quarterly
        protected void grdQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {

                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 4;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 4;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 4;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdQuarterly.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdQuarterly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Monthly
        protected void grdMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {

                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 12;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 12;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 12;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdMonthly.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdMonthly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Phase1
        protected void grdphase1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 1;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 1;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 1;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdphase1.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdphase1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Phase 2
        protected void grdphase2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 2;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 2;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 2;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdphase2.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdphase2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Phase 3
        protected void grdphase3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 3;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 3;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 3;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdphase3.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdphase3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Phase 4
        protected void grdphase4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 4;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 4;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 4;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdphase4.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdphase4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        #region Phase 5
        protected void grdphase5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            {
                string financialyear = ddlFinancialYear.SelectedItem.Text;
                string[] a = financialyear.Split('-');
                string aaa = a[0];
                string bbb = a[1];
                string f1 = aaa + "-" + bbb;
                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    GridViewRow gvRow = e.Row;
                    if (gvRow.RowType == DataControlRowType.Header)
                    {
                        GridViewRow gvrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                        TableCell cell0 = new TableCell();
                        cell0.Text = "Process";
                        cell0.HorizontalAlign = HorizontalAlign.Center;
                        cell0.ColumnSpan = 1;

                        TableCell cell1 = new TableCell();
                        cell1.Text = f1;
                        cell1.HorizontalAlign = HorizontalAlign.Center;
                        cell1.ColumnSpan = 5;

                        TableCell cell2 = new TableCell();
                        cell2.Text = f2;
                        cell2.HorizontalAlign = HorizontalAlign.Center;
                        cell2.ColumnSpan = 5;

                        TableCell cell3 = new TableCell();
                        cell3.Text = f3;
                        cell3.HorizontalAlign = HorizontalAlign.Center;
                        cell3.ColumnSpan = 5;

                        gvrow.Cells.Add(cell0);
                        gvrow.Cells.Add(cell1);
                        gvrow.Cells.Add(cell2);
                        gvrow.Cells.Add(cell3);

                        grdphase5.Controls[0].Controls.AddAt(1, gvrow);
                    }
                }
            }
        }
        protected void grdphase5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        #endregion
        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool ViewSchedule(long? EventID, object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {

                if (EventID != null)
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {
                        return true;

                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void grdAuditScheduling_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditScheduling_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditScheduling_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditScheduling_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("SHOW_SCHEDULE"))
            {
                string isAHQMP = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string Termname = string.Empty;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 1)
                {
                    if (!string.IsNullOrEmpty(commandArgs[0]))
                    {
                         isAHQMP = Convert.ToString(commandArgs[0]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[1]))
                    {
                         FinancialYear = Convert.ToString(commandArgs[1]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[2]))
                    {
                         CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[3]))
                    {
                         Termname = Convert.ToString(commandArgs[3]);
                    }
                    OpenScheduleInformation(isAHQMP, FinancialYear, CustomerBranchId, Termname);
                }
            }
            if (e.CommandName.Equals("DELETE_SCHEDULE"))
            {
                string isAHQMP = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string Termname = string.Empty;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 1)
                {
                    if (!string.IsNullOrEmpty(commandArgs[0]))
                    {
                        isAHQMP = Convert.ToString(commandArgs[0]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[1]))
                    {
                        FinancialYear = Convert.ToString(commandArgs[1]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[2]))
                    {
                        CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[3]))
                    {
                        Termname = Convert.ToString(commandArgs[3]);
                    }
                    DeleteInternalAuditScheduling(isAHQMP, FinancialYear, CustomerBranchId, Termname);
                    BindMainGrid(-1, -1);
                }
            }
        }
        public  bool CheckProcessAsignedOrNotExists(string FinancialYear, string ForPeriod, long CustomerBranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             join row1 in entities.InternalAuditScheduleOns
                             on row.ID equals row1.InternalAuditInstance
                             where row.ProcessId == row1.ProcessId && row1.FinancialYear == FinancialYear
                             && row1.ForMonth == ForPeriod && row.CustomerBranchID == CustomerBranchId 
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public  void DeleteInternalAuditScheduling(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    if (CheckProcessAsignedOrNotExists(FinancialYear, Termname, CustomerBranchId) == false)
                    {
                        var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                                     where row.FinancialYear == FinancialYear && row.IsDeleted == false
                                                     && row.ISAHQMP == isAHQMP && row.CustomerBranchId == CustomerBranchId
                                                     && row.TermName == Termname
                                                     select row.Id).ToList();
                        AuditorMastertoDelete.ForEach(entry =>
                        {
                            InternalAuditScheduling prevmappedids = (from row in entities.InternalAuditSchedulings
                                                                     where row.Id == entry
                                                                     select row).FirstOrDefault();
                            prevmappedids.IsDeleted = true;
                            entities.SaveChanges();
                            cvDuplicateEntry1.IsValid = false;
                            cvDuplicateEntry1.ErrorMessage = "Schedule Deleted Successfully..!";
                        });
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Process Assigned..!";
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void grdAuditScheduling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        private void OpenScheduleInformation(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname)
        {
            try
            {
                
                using (AuditControlEntities entities = new AuditControlEntities())
                {                   
                    List<AuditExpectedStartEndDate_Result> a = new List<AuditExpectedStartEndDate_Result>();
                    a = ProcessManagement.GetAuditExpectedStartEndDate_ResultProcedure(isAHQMP, FinancialYear, CustomerBranchId, Termname,-1,0).ToList();                  
                    var remindersummary = a.OrderBy(entry => entry.ProcessName).ToList();
                    grdAuditScheduleStartEndDate.DataSource = null;
                    grdAuditScheduleStartEndDate.DataBind();
                    grdAuditScheduleStartEndDate.DataSource = remindersummary;
                    grdAuditScheduleStartEndDate.DataBind();
                    upComplianceScheduleDialog.Update();
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Add(object sender, EventArgs e)
        {
            //try
            //{
            //    Control control = null;
            //    if (grdRiskActivityMatrix.FooterRow != null)
            //    {
            //        control = grdRiskActivityMatrix.FooterRow;
            //    }
            //    else
            //    {
            //        control = grdRiskActivityMatrix.Controls[0].Controls[0];
            //    }
            //    string ActivityDescription = (control.FindControl("txtRiskActivityDescription") as TextBox).Text;
            //    string controlObjective = (control.FindControl("txtControlObjective") as TextBox).Text;
            //    string locationtype = (control.FindControl("ddlLocationTypeProcess") as DropDownList).SelectedItem.Value;
            //    if (ActivityDescription == "")
            //    {
            //        lblmsg.Text = "Please Enter Activity Description";
            //        return;
            //    }
            //    if (controlObjective == "")
            //    {
            //        lblmsg.Text = "Please Enter Control Objective";
            //        return;
            //    }
            //    int customerID = -1;
            //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
            //    {
            //        customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            //    }
            //    long pid = 0;
            //    long psid = 0;
            //    if (ddlProcess.SelectedValue != "")
            //    {
            //        if (ddlProcess.SelectedValue != "< Select Process >")
            //        {
            //            pid = Convert.ToInt32(ddlProcess.SelectedValue);
            //        }
            //    }
            //    if (ddlSubProcess.SelectedValue != "")
            //    {
            //        if (ddlSubProcess.SelectedValue != "< Select Sub Process >")
            //        {
            //            psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
            //        }
            //    }
            //    if (ActivityDescription != "" || ActivityDescription != null)
            //    {
            //        if (controlObjective != "" || controlObjective != null)
            //        {
            //            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
            //            {
            //                ActivityDescription = ActivityDescription,
            //                ControlObjective = controlObjective,
            //                ProcessId = pid,
            //                SubProcessId = psid,
            //                LocationType = Convert.ToInt32(locationtype),
            //                IsInternalAudit = "N",
            //                CustomerId = customerID
            //            };
            //            if (RiskCategoryManagement.Exists(riskcategorycreation))
            //            {
            //                cvDuplicateEntry.ErrorMessage = "Risk Category already exists.";
            //                cvDuplicateEntry.IsValid = false;
            //                return;
            //            }
            //            using (AuditControlEntities entities = new AuditControlEntities())
            //            {
            //                entities.RiskCategoryCreations.Add(riskcategorycreation);
            //                entities.SaveChanges();
            //            }
            //            if (IndustryIdProcess.Count > 0)
            //            {
            //                foreach (var aItem in IndustryIdProcess)
            //                {
            //                    IndustryMapping IndustryMapping = new IndustryMapping()
            //                    {
            //                        RiskCategoryCreationId = riskcategorycreation.Id,
            //                        IndustryID = Convert.ToInt32(aItem),
            //                        IsActive = true,
            //                        EditedDate = DateTime.UtcNow,
            //                        EditedBy = Convert.ToInt32(Session["userID"]),
            //                        ProcessId = pid,
            //                        SubProcessId = psid,
            //                    };
            //                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
            //                }
            //                IndustryIdProcess.Clear();
            //            }
            //            if (RiskIdProcess.Count > 0)
            //            {
            //                foreach (var aItem in RiskIdProcess)
            //                {
            //                    RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
            //                    {
            //                        RiskCategoryCreationId = riskcategorycreation.Id,
            //                        RiskCategoryId = Convert.ToInt32(aItem),
            //                        ProcessId = pid,
            //                        SubProcessId = psid,
            //                    };
            //                    RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
            //                }
            //                RiskIdProcess.Clear();
            //            }
            //            if (ClientIdProcess.Count > 0)
            //            {
            //                foreach (var aItem in ClientIdProcess)
            //                {
            //                    CustomerBranchMapping CustomerBranchMapping = new CustomerBranchMapping()
            //                    {
            //                        RiskCategoryCreationId = riskcategorycreation.Id,
            //                        BranchId = Convert.ToInt32(aItem),
            //                        ProcessId = pid,
            //                        SubProcessId = psid,
            //                    };
            //                    RiskCategoryManagement.CreateCustomerBranchMapping(CustomerBranchMapping);
            //                }
            //                ClientIdProcess.Clear();
            //            }
            //            if (AssertionsIdProcess.Count > 0)
            //            {
            //                foreach (var aItem in AssertionsIdProcess)
            //                {
            //                    AssertionsMapping AssertionsMapping = new AssertionsMapping()
            //                    {
            //                        RiskCategoryCreationId = riskcategorycreation.Id,
            //                        AssertionId = Convert.ToInt32(aItem),
            //                        ProcessId = pid,
            //                        SubProcessId = psid,
            //                    };
            //                    RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
            //                }
            //                AssertionsIdProcess.Clear();
            //            }
            //        }
            //    }
            //    this.BindData("P");
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }
        protected void Delete(object sender, EventArgs e)
        {
            //Button btnRemove = (Button)sender;
            //GridViewRow objGvRow = (GridViewRow)btnRemove.NamingContainer;
            //string RequestId = Convert.ToString(grdRiskActivityMatrix.DataKeys[objGvRow.RowIndex].Value);
            //RiskCategoryManagement.Delete(Convert.ToInt32(RequestId));
            //this.BindData("P");
        }

        protected void grdTestQ_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkQuarter1 = (e.Row.FindControl("chkQuarter1") as CheckBox);
                CheckBox chkQuarter2 = (e.Row.FindControl("chkQuarter2") as CheckBox);
                CheckBox chkQuarter3 = (e.Row.FindControl("chkQuarter3") as CheckBox);
                CheckBox chkQuarter4 = (e.Row.FindControl("chkQuarter4") as CheckBox);
            }          
        }
       
    }
}