﻿<%@ Page Title="Master Audit Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FrmMasterAuditObservationReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmMasterAuditObservationReport" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //setactivemenu('Audit Observation Report');
            fhead('Audit Observation Report');
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel"> 
                                     <header>
                                      <ul id="Ul1" class="nav nav-tabs">                                      
                                                                           
                                    </ul>
                                </header>  
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs" style="margin: -10px -1px -11px 0;">
                                           <%if (roles.Contains(3) || roles.Contains(4))%>
                                           <%{%>
                                                    <%if (roles.Contains(3))%>
                                                    <%{%>                                                              
                                                        <li class="active" id="liPerformer" runat="server">
                                                        <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                        </li>
                                                    <%}%>
                                                    <%if (roles.Contains(4))%>
                                                    <%{%>      
                                                        <li class=""  id="liReviewer" runat="server">
                                                             <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                        </li>
                                                    <%}%>
                                                    <% if (AuditHeadOrManagerReport == null)%>
                                                    <%{%>
                                                        <li style="float: right;">   
                                                        <div>
                                                        <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                        <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li>
                                                                <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation</a></li>
                                                                <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation</a></li>                                                    
                                                                <%-- <li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>                                                   
                                                                <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                      
                                                                <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li> --%>                                                    
                                                            </ul>
                                                        </div>
                                                    </li>   
                                                     <%}%>    
                                           <%}%>           
                                    </ul>
                                </header>
                            <div class="clearfix"></div>     
                        <div class="col-md-12 colpadding0">                 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlLegalEntity" PlaceHolder="Select Unit" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlSubEntity1" PlaceHolder="Select Sub Unit" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlSubEntity2" PlaceHolder="Select Sub Unit" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlSubEntity3" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                           

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                    class="form-control m-bot15 select_location" Style="float: left; width: 90%;">
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width: 90%;">
                                    <%--OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"--%>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlSchedulingType" AutoPostBack="true" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                    class="form-control m-bot15 select_location" Style="float: left; width: 90%;">                                   
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlPeriod" AutoPostBack="true"
                                    class="form-control m-bot15 select_location" Style="float: left; width: 90%;">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                      

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlProcess" AutoPostBack="true" class="form-control m-bot15 select_location"
                                    OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" Style="float: left; width: 90%;">
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlVertical" AutoPostBack="true" class="form-control m-bot15 select_location"
                                    OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged" Style="float: left; width: 90%;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float: right;">
                                <%--<div>
                                    <asp:Button ID="btnFilter" class="btn btn-search" runat="server" Text="Apply Filter(s)"  /> 
                                </div>--%> <%--style="width: 120px;"--%>
                                <div style="float: right; margin-right: 10%;">
                                   <%-- <asp:Button ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click" class="btn btn-search" runat="server" Text="Export To Excel"></asp:Button>--%>
                                    <asp:Button ID="lbtnExportExcelTest" OnClick="lbtnExportExcelTest_Click" class="btn btn-search" runat="server" Text="Export To Excel"></asp:Button>
                                    <%--OnClick="lbtnExportExcel_Click"--%>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div>
                       
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                         

                      
                        </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="lbtnExportExcelTest" />

        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
