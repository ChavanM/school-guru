﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System.Reflection;
using System.Drawing;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using Spire.Presentation;
using Spire.Presentation.Drawing;
using System.Text;
using Ionic.Zip;
using System.Text.RegularExpressions;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditClosure_New : System.Web.UI.Page
    {
        protected int CustomerId = 0;
        protected int GridProcessID = -1;
        protected int DefaultProcessOrder = 1;
        public List<AuditClosureClose> MasterAuditClosureClose = new List<AuditClosureClose>();
        protected int totalCheckList = 0;
        protected int totalcheckListClosed = 0;
        public static string DocumentPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int AuditID = 0;
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindFinancialYear();
                BindLegalEntityData();
                bindPageNumber();
                BindVerticalID();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancialYear.ClearSelection();
                    ddlFilterFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                LBTAuditDetails.Attributes.Add("class", "active");
                LBTObservationDetails.Attributes.Add("class", "");
                ViewState["AuditID"] = null;
                Session["TotalRows"] = null;
                ViewState["brachName"] = null;
                ViewState["vertical"] = null;
                //grdAuditScheduling.DataSource = null;
                //grdAuditScheduling.DataBind();
                int ScheduledOnID = Convert.ToInt32(Request.QueryString["SID"]);
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    MasterAuditClosureClose = (from row in entities.AuditClosureCloses
                                               select row).ToList();

                }
                if (AuditID == 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                        HiddenObjValue.Value = "0";
                    }
                }

                // added by sagar more on 16-01-2020
                if (Request.QueryString["AuditIDForBack"] != null && Request.QueryString["FinancialYearForBack"] != null)
                {
                    int AuditIDForBack = Convert.ToInt32(Request.QueryString["AuditIDForBack"].ToString());
                    string financialYearForBack = Request.QueryString["FinancialYearForBack"].ToString();
                    BindMainGridObservationFromBackButton(AuditIDForBack, null, null, null, null, null, financialYearForBack, null);
                }
                else
                {
                    BindMainGrid();
                }
                //BindMainGrid();// code comment by sagar more on 16-01-2020
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public void BindFinancialYear()
        {
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
            ddlFilterFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, UserID);
            }
            //ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);

            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        public void BindVerticalID()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            var data = UserManagementRisk.FillVerticalList(CustomerId);
            ddlVerticalID.DataTextField = "VerticalName";
            ddlVerticalID.DataValueField = "ID";
            ddlVerticalID.Items.Clear();
            ddlVerticalID.DataSource = data;
            ddlVerticalID.DataBind();
            ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
        }
        public void BindPeriod()
        {
            try
            {

                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                int VerticalID = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                    else
                    {
                        FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                if (CustomerBranchId != -1 && FinancialYear != "" && VerticalID != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var query = (from AC in entities.InternalControlAuditResults
                                     join MCB in entities.mst_CustomerBranch
                                     on AC.CustomerBranchId equals MCB.ID
                                     where AC.AStatusId == 3
                                     && MCB.ID == CustomerBranchId
                                     && AC.FinancialYear == FinancialYear
                                     && AC.VerticalID == VerticalID
                                     && MCB.IsDeleted == false
                                     && MCB.Status == 1
                                     select new
                                     { AC.ForPerid }).Distinct().ToList();


                        if (query.Count > 0)
                            query = query.OrderBy(entry => entry.ForPerid).ToList();

                        ddlPeriod.DataTextField = "ForPerid";
                        ddlPeriod.DataValueField = "ForPerid";
                        ddlPeriod.Items.Clear();
                        ddlPeriod.DataSource = query;
                        ddlPeriod.DataBind();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindMainGrid()
        {
            string FinancialYear = string.Empty;
            int CustomerBranchId = -1;
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            string PeriodName = string.Empty;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            int VerticalID = -1;
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }

            if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
            {
                if (ddlPeriod.SelectedValue != "-1")
                {
                    PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                }
            }

            //  if (CustomerBranchId != -1 && FinancialYear != "" && VerticalID != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.Sp_GetAuditClosureDisplayAudit(Portal.Common.AuthenticationHelper.UserID)
                                 where row.CustomerID == CustomerId
                                 select row).ToList();

                    if (CustomerBranchId != -1)
                    {
                        query = query.Where(entry => entry.BranchId == CustomerBranchId).ToList();
                    }
                    if (FinancialYear != "")
                    {
                        query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                    }
                    else
                    {
                        FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                    }
                    if (VerticalID != -1)
                    {
                        query = query.Where(entry => entry.VerticalID == VerticalID).ToList();
                    }
                    if (PeriodName != "")
                        query = query.Where(entry => entry.ForMonth == PeriodName).ToList();

                    if (query.Count > 0)
                        query = query.OrderBy(entry => entry.BranchName).ThenBy(entry => entry.VerticalName).ToList();

                    grdAuditScheduling.DataSource = null;
                    grdAuditScheduling.DataBind();

                    if (query != null)
                    {
                        Session["TotalRows"] = null;
                        grdAuditScheduling.DataSource = query;
                        Session["TotalRows"] = query.Count;
                        grdAuditScheduling.DataBind();
                        query = query.Where(a => a.Total != a.Closed).ToList();
                        //btnAddCompliance.Visible = true;
                        //if (query.Count > 0)
                        //{
                        //    btnAddCompliance.Visible = false;
                        //}
                    }

                }
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPeriod();
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPeriod();
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRemark.Text = string.Empty;
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindMainGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditScheduling.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowStatus(string status)
        {
            string processnonprocess = "";
            if (Convert.ToString(status) == "Green")
            {
                processnonprocess = "G";
            }
            else if (Convert.ToString(status) == "Red")
            {
                processnonprocess = "R";
            }
            return processnonprocess;
        }
        protected void grdAuditScheduling_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvRow = e.Row;

            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                Label lblPStatus = (e.Row.FindControl("lblPStatus") as Label);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                Label lblIsClosed = (e.Row.FindControl("lblIsClosed") as Label);
                Label lblAuditId = (e.Row.FindControl("lblAuditId") as Label);
                if (lblPStatus != null)
                {
                    if (lblPStatus.Text == "Green")
                    {
                        lblStatus.BackColor = Color.Green;
                    }
                    else if (lblPStatus.Text == "Red")
                    {
                        lblStatus.BackColor = Color.Red;
                    }

                }
                if (MasterAuditClosureClose.Count == 0)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        MasterAuditClosureClose = (from row in entities.AuditClosureCloses

                                                   select row).ToList();

                    }
                }
                int audi = Convert.ToInt32(lblAuditId.Text);
                var close = (from row in MasterAuditClosureClose
                             where row.AuditId == audi
                             select row).FirstOrDefault();
                if (close != null)
                {
                    lblIsClosed.Text = "Closed";
                }
                else
                {
                    lblIsClosed.Text = "Open";
                }

            }
        }
        protected void grdAuditScheduling_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void GridObservationDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvRow = e.Row;
            //         protected int GridProcessID = -1;
            //protected int DefaultProcessOrder = 0;            
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                Label lblProcessID = (e.Row.FindControl("lblProcessID") as Label);
                Label lblProcessRatingName = (e.Row.FindControl("lblProcessRatingName") as Label);
                DropDownList ddlProcess = (DropDownList)e.Row.FindControl("ddlProcess");
                if (Convert.ToInt32(lblProcessID.Text) == GridProcessID)
                {
                    TextBox txtProcessSequence = (e.Row.FindControl("txtProcessSequence") as TextBox);
                    txtProcessSequence.Visible = false;
                    ddlProcess.Visible = false;
                }
                else
                {
                    TextBox txtProcessSequence = (e.Row.FindControl("txtProcessSequence") as TextBox);
                    GridProcessID = Convert.ToInt32(lblProcessID.Text);
                    if (string.IsNullOrEmpty(txtProcessSequence.Text))
                    {
                        txtProcessSequence.Text = Convert.ToString(DefaultProcessOrder++);
                    }

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ID";
                    ddlProcess.Items.Clear();
                    ddlProcess.DataSource = UserManagementRisk.GetAllProcess();
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));

                    if (!string.IsNullOrEmpty(lblProcessRatingName.Text))
                    {
                        ddlProcess.Items.FindByText(lblProcessRatingName.Text).Selected = true;
                    }
                }
            }
        }

        protected void grdAuditScheduling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        protected void grdAuditScheduling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditDetial"))
                {
                    HiddenObjValue.Value = "0";
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int AuditID = Convert.ToInt32(commandArgs[0]);
                    ViewState["AuditID"] = commandArgs[0];
                    Session["AuditID"] = commandArgs[0];// added by sagar more on 16-01-2020
                    Session["FinancialYear"] = ddlFilterFinancialYear.SelectedItem.Text; // added by sagar more on 16-01-2020
                    btnGenrateReport.Visible = false;
                    btnObservation.Visible = true;
                    btnDraft.Visible = true;// added by sagar more on 09-01-2020
                    lbtObservationList.Visible = true;
                    LBTObservationDetails_Click(sender, e);
                    BindMainGridObservation(AuditID);
                    int CustomerBranchID = -1;
                    int VerticalID = -1;
                    string FinancialYear = string.Empty;
                    string CustomerBranchName = string.Empty;
                    string VerticalName = commandArgs[3];
                    string PeriodName = string.Empty;
                    txtRemark.Text = string.Empty;
                    if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                            CustomerBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                            CustomerBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                            CustomerBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                            CustomerBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            CustomerBranchName = ddlSubEntity4.SelectedItem.Text.Trim();
                            CustomerBranchID = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }
                    if (CustomerBranchID == -1)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(commandArgs[1]))
                            {
                                CustomerBranchID = Convert.ToInt32(commandArgs[1]);
                                using (AuditControlEntities entities = new AuditControlEntities())
                                {
                                    var branchname = (from row in entities.mst_CustomerBranch
                                                      where row.ID == CustomerBranchID
                                                      select row.Name).FirstOrDefault();
                                    CustomerBranchName = branchname;
                                }
                            }
                        }
                        catch (Exception ex) { }
                    }

                    if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                    {
                        if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                        {
                            FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                        }
                    }

                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        var videtails = UserManagementRisk.getverticaldetails(Portal.Common.AuthenticationHelper.CustomerID);
                        if (videtails != null)
                        {
                            VerticalName = videtails.VerticalName;
                            VerticalID = videtails.ID;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                        {
                            if (ddlVerticalID.SelectedValue != "-1")
                            {
                                VerticalName = ddlVerticalID.SelectedItem.Text.Trim();
                                VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                        }
                    }
                    if (string.IsNullOrEmpty(PeriodName))
                    {
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            PeriodName = commandArgs[2].ToString();
                        }
                    }

                    if (string.IsNullOrEmpty(PeriodName))
                    {
                        var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchID, VerticalID, FinancialYear, AuditID);
                        if (peridodetails != null)
                        {
                            if (peridodetails.Count > 0)
                            {
                                foreach (var ForPeriodName in peridodetails)
                                {
                                    bool Success1 = false;
                                    Success1 = RiskCategoryManagement.ExistsAuditClosureCloseRecordZEE(CustomerBranchID, VerticalID, FinancialYear, ForPeriodName, AuditID);
                                    if (Success1)
                                    {
                                        btnObservation.Visible = false;
                                        lbtObservationList.Visible = false;
                                        btnGenrateReport.Visible = true;
                                        btnDraft.Visible = false;// added by sagar more on 09-01-2020
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        bool Success1 = false;
                        Success1 = RiskCategoryManagement.ExistsAuditClosureCloseRecordZEE(CustomerBranchID, VerticalID, FinancialYear, PeriodName, AuditID);
                        if (Success1)
                        {
                            btnObservation.Visible = false;
                            lbtObservationList.Visible = false;
                            btnGenrateReport.Visible = true;
                            btnDraft.Visible = false;// added by sagar more on 09-01-2020
                        }
                    }
                    var AuditDetail = UserManagementRisk.GetAuditDatail(AuditID);
                    if (AuditDetail != null)
                    {
                        totalCheckList = Convert.ToInt32(AuditDetail.Total);
                        totalcheckListClosed = Convert.ToInt32(AuditDetail.Closed);
                        if (totalCheckList != totalcheckListClosed)
                        {
                            btnObservation.Visible = false;
                            lbtObservationList.Visible = false;
                            btnDraft.Visible = false;
                        }
                    }
                    ViewState["brachName"] = CustomerBranchName;
                    ViewState["vertical"] = VerticalName;
                    lblObservationSelectionDetails.Text = string.Empty;
                    lblObservationSelectionDetails.Text = CustomerBranchName + "/" + VerticalName + "/" + FinancialYear + "/" + PeriodName;
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var result = (from row in entities.Tran_FinalDeliverableUpload
                                      where row.AuditID == AuditID
                                      select row).ToList().Count();

                        HiddenObjValue.Value = Convert.ToString(result);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string ShowLocation(long ProcessId, int ObservationCategory, long ObservationSubCategory, string FinancialYear, string Observation, string ManagementResponse, long Personresponsible)
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            string processnonprocess = "";
            var MstRiskResult = RiskCategoryManagement.GetAuditClosureLocation(Convert.ToInt32(CustomerBranchId), ProcessId, ObservationCategory, ObservationSubCategory, FinancialYear, Observation, ManagementResponse, Personresponsible);
            if (MstRiskResult != null)
            {
                var remindersummary = MstRiskResult.OrderBy(entry => entry).ToList();
                if (remindersummary.Count > 0)
                {
                    foreach (var row in remindersummary)
                    {
                        processnonprocess += row + " ,";
                    }
                }
                return processnonprocess.Trim(',');
            }
            else
            {
                return processnonprocess;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditScheduling.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindMainGrid();
        }
        protected void LBTAuditDetails_Click(object sender, EventArgs e)
        {
            try
            {
                // added by sagar more on 16-01-2020
                GridObservationDetails.DataSource = null;
                GridObservationDetails.DataBind();

                bindPageNumber();
                LBTAuditDetails.Attributes.Add("class", "active");
                LBTObservationDetails.Attributes.Add("class", "");

                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane active");

                dvObservationDetails.Attributes.Remove("class");
                dvObservationDetails.Attributes.Add("class", "tab-pane");

                //dvReportDetails.Attributes.Remove("class");
                //dvReportDetails.Attributes.Add("class", "tab-pane");

                int chknumber = grdAuditScheduling.PageIndex;
                if (chknumber > 0)
                {
                    chknumber = chknumber + 1;
                    DropDownListPageNo.SelectedValue = (chknumber).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void LBTObservationDetails_Click(object sender, EventArgs e)
        {
            try
            {
                bindObservationPageNumber();
                LBTAuditDetails.Attributes.Add("class", "");
                LBTObservationDetails.Attributes.Add("class", "active");


                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane");

                dvObservationDetails.Attributes.Remove("class");
                dvObservationDetails.Attributes.Add("class", "tab-pane active");

                if (GridObservationDetails.Rows.Count > 0)
                {
                    int chknumber = GridObservationDetails.PageIndex;
                    if (chknumber > 0)
                    {
                        chknumber = chknumber + 1;
                        DropDownListObservation.SelectedValue = (chknumber).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void LBTReportDetails_Click(object sender, EventArgs e)
        {
            try
            {
                LBTAuditDetails.Attributes.Add("class", "");
                LBTObservationDetails.Attributes.Add("class", "");

                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane");

                dvObservationDetails.Attributes.Remove("class");
                dvObservationDetails.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                if (AuditID == -1)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                    }
                }
                btnGenrateReport.Visible = false;
                btnObservation.Visible = true;
                btnDraft.Visible = true;// added by sagar more on 09-01-2020
                LBTObservationDetails_Click(sender, e);
                BindMainGridObservation(AuditID);
                int CustomerBranchID = -1;
                int VerticalID = -1;
                string FinancialYear = string.Empty;
                string CustomerBranchName = string.Empty;
                string VerticalName = string.Empty;
                string PeriodName = string.Empty;
                txtRemark.Text = string.Empty;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                        CustomerBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchName = ddlSubEntity4.SelectedItem.Text.Trim();
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    var videtails = UserManagementRisk.getverticaldetails(Portal.Common.AuthenticationHelper.CustomerID);
                    if (videtails != null)
                    {
                        VerticalName = videtails.VerticalName;
                        VerticalID = videtails.ID;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalName = ddlVerticalID.SelectedItem.Text.Trim();
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                    }
                }
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchID, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                bool Success1 = false;
                                Success1 = RiskCategoryManagement.ExistsAuditClosureCloseRecordZEE(CustomerBranchID, VerticalID, FinancialYear, ForPeriodName, AuditID);
                                if (Success1)
                                {
                                    btnObservation.Visible = false;
                                    btnGenrateReport.Visible = true;
                                    btnDraft.Visible = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool Success1 = false;
                    Success1 = RiskCategoryManagement.ExistsAuditClosureCloseRecordZEE(CustomerBranchID, VerticalID, FinancialYear, PeriodName, AuditID);
                    if (Success1)
                    {
                        btnObservation.Visible = false;
                        btnGenrateReport.Visible = true;
                    }
                }
                ViewState["brachName"] = CustomerBranchName;
                ViewState["vertical"] = VerticalName;
                lblObservationSelectionDetails.Text = string.Empty;
                lblObservationSelectionDetails.Text = CustomerBranchName + "/" + VerticalName + "/" + FinancialYear + "/" + PeriodName;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnObservation_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                string FinancialYear = string.Empty;
                string period = string.Empty;
                int CustomerBranchID = -1;
                int VerticalID = -1;
                bool Success1 = false;
                int AuditID = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        period = Convert.ToString(ddlPeriod.SelectedItem.Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
                if (AuditID == -1 || AuditID == 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                    }
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditDetails = (from row in entities.AuditClosureDetails
                                        where row.ID == AuditID
                                        select row).FirstOrDefault();
                    CustomerBranchID = Convert.ToInt32(AuditDetails.BranchId);
                    FinancialYear = AuditDetails.FinancialYear;
                    period = AuditDetails.ForMonth;
                    VerticalID = Convert.ToInt32(AuditDetails.VerticalId);

                }
                string ProcessOrder = string.Empty;
                string ObservationNumber = string.Empty;
                string Remark = string.Empty;
                string ForPeriod = string.Empty;
                int Observationcategory = -1;
                long ObservationSubCategory = -1;
                long ATBDID = -1;
                long ProcessID = -1;
                int ProcessRating = -1;
                string Observation = string.Empty;
                List<AuditClosureClose> AuditClosureCloseList = new List<AuditClosureClose>();
                if (GridObservationDetails.Rows.Count > 0)
                {
                    foreach (GridViewRow rw in GridObservationDetails.Rows)
                    {
                        AuditID = -1;
                        ATBDID = -1;
                        Observationcategory = -1;
                        ObservationSubCategory = -1;
                        Observation = string.Empty;
                        ForPeriod = string.Empty;
                        ObservationNumber = string.Empty;
                        ProcessOrder = string.Empty;
                        ProcessRating = -1;
                        ATBDID = Convert.ToInt32(((Label)rw.FindControl("lblATBDID")).Text);
                        AuditID = Convert.ToInt32(((Label)rw.FindControl("lblAuditID")).Text);
                        ProcessID = Convert.ToInt32(((Label)rw.FindControl("lblProcessID")).Text);

                        DropDownList ddlProcess = (DropDownList)rw.FindControl("ddlProcess");
                        if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            if (ddlProcess.SelectedValue != "-1")
                            {
                                ProcessRating = Convert.ToInt32(ddlProcess.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(((TextBox)rw.FindControl("txtObservationNumber")).Text))
                        {
                            ObservationNumber = ((TextBox)rw.FindControl("txtObservationNumber")).Text;
                        }
                        if (!string.IsNullOrEmpty(((TextBox)rw.FindControl("txtProcessSequence")).Text))
                        {
                            ProcessOrder = ((TextBox)rw.FindControl("txtProcessSequence")).Text;
                        }

                        if (!string.IsNullOrEmpty(((Label)rw.FindControl("lblObservationCategoryID")).Text))
                        {
                            Observationcategory = Convert.ToInt32(((Label)rw.FindControl("lblObservationCategoryID")).Text);
                        }

                        if (!string.IsNullOrEmpty(((Label)rw.FindControl("lblObservationSubCategoryID")).Text))
                        {
                            ObservationSubCategory = Convert.ToInt32(((Label)rw.FindControl("lblObservationSubCategoryID")).Text);
                        }
                        if (!string.IsNullOrEmpty(((Label)rw.FindControl("lblPeriod")).Text))
                        {
                            ForPeriod = ((Label)rw.FindControl("lblPeriod")).Text;
                        }
                        Observation = ((Label)rw.FindControl("lblObservation")).Text;
                        string RemarkText = txtRemark.Text;

                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            #region Save Process Order
                            if (ProcessOrder != "" && ((TextBox)rw.FindControl("txtProcessSequence")).Visible == true)
                            {
                                var ACs = (from AC in entities.AuditClosures
                                           where AC.ProcessId == ProcessID && AC.AuditID == AuditID
                                           select AC).ToList();

                                if (ACs.Count > 0)
                                {
                                    ACs.ForEach(entry => entry.ProcessOrder = Convert.ToInt32(ProcessOrder));
                                    entities.SaveChanges();
                                }
                            }
                            #endregion

                            #region Save ObservationNumber,FinalDate,RemarkDate,Remark
                            if (ObservationSubCategory != -1)
                            {
                                var MstRiskResult = (from AC in entities.AuditClosures
                                                     join MCB in entities.mst_CustomerBranch
                                                     on AC.CustomerbranchId equals MCB.ID
                                                     where AC.ACStatus == 1
                                                     && AC.ATBDId == ATBDID
                                                     && MCB.ID == CustomerBranchID
                                                     && AC.FinancialYear == FinancialYear
                                                     && AC.ObservationCategory == Observationcategory
                                                     && AC.ObservationSubCategory == ObservationSubCategory
                                                     && AC.VerticalID == VerticalID
                                                     && AC.ForPeriod == ForPeriod
                                                     && AC.AuditID == AuditID
                                                     && MCB.IsDeleted == false
                                                     && MCB.Status == 1
                                                     select AC).ToList();

                                if (MstRiskResult.Count > 0)
                                {
                                    MstRiskResult.ForEach(entry => entry.ProcessRatingId = ProcessRating);
                                    MstRiskResult.ForEach(entry => entry.ObservationNumber = ObservationNumber);
                                    MstRiskResult.ForEach(entry => entry.Remark = RemarkText);
                                    MstRiskResult.ForEach(entry => entry.RemarkDate = DateTime.Now);
                                    MstRiskResult.ForEach(entry => entry.FinalDate = DateTime.Now);
                                    entities.SaveChanges();
                                }
                            }
                            else
                            {
                                var MstRiskResult = (from AC in entities.AuditClosures
                                                     join MCB in entities.mst_CustomerBranch
                                                     on AC.CustomerbranchId equals MCB.ID
                                                     where AC.ACStatus == 1
                                                     && AC.ATBDId == ATBDID
                                                     && MCB.ID == CustomerBranchID
                                                     && AC.FinancialYear == FinancialYear
                                                     && AC.ObservationCategory == Observationcategory
                                                     && AC.Observation.Trim().ToUpper() == Observation.Trim().ToUpper()
                                                     && AC.VerticalID == VerticalID
                                                     && AC.ForPeriod == ForPeriod
                                                     && AC.AuditID == AuditID
                                                     && MCB.IsDeleted == false
                                                     && MCB.Status == 1
                                                     select AC).ToList();
                                if (MstRiskResult.Count > 0)
                                {
                                    MstRiskResult.ForEach(entry => entry.ProcessRatingId = ProcessRating);
                                    MstRiskResult.ForEach(entry => entry.ObservationNumber = ObservationNumber);
                                    MstRiskResult.ForEach(entry => entry.Remark = RemarkText);
                                    MstRiskResult.ForEach(entry => entry.RemarkDate = DateTime.Now);
                                    MstRiskResult.ForEach(entry => entry.FinalDate = DateTime.Now);
                                    entities.SaveChanges();
                                }
                            }
                            #endregion
                        }
                        Success1 = true;
                    }

                    if (Success1)
                    {
                        #region File upload
                        string directoryPath = "";
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        if (FeedbackFileUpload.HasFile)
                        {
                            FeedbackFormUpload objFeedbackFormUpload = new FeedbackFormUpload()
                            {
                                AuditId = Convert.ToInt64(AuditID),
                                Name = Convert.ToString(FeedbackFileUpload.FileName),
                            };

                            int count = RiskCategoryManagement.FeedBackFormFileCount(objFeedbackFormUpload);

                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                            {
                                directoryPath = Server.MapPath("~/AuditFeedBackDocument/" + CustomerId + "/"
                                          + Convert.ToInt32(CustomerBranchID) + "/" + Convert.ToInt32(VerticalID) + "/"
                                          + Convert.ToString(FinancialYear) + "/" + Convert.ToString(period) + "/" + AuditID + "/"
                                          + "/" + (count + 1) + ".0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            if (directoryPath != "")
                            {
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(FeedbackFileUpload.FileName));

                                Stream fs = FeedbackFileUpload.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (FeedbackFileUpload.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        objFeedbackFormUpload.Name = FeedbackFileUpload.PostedFile.FileName;
                                        objFeedbackFormUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        objFeedbackFormUpload.FileKey = fileKey.ToString();
                                        objFeedbackFormUpload.FileUploadedDate = DateTime.Today.Date;
                                        objFeedbackFormUpload.AuditId = AuditID;
                                        objFeedbackFormUpload.Version = (count + 1) + ".0";
                                        objFeedbackFormUpload.VersionDate = DateTime.UtcNow;
                                        if (!RiskCategoryManagement.CheckFeedbackFormExists(objFeedbackFormUpload))
                                        {
                                            Success1 = RiskCategoryManagement.SaveFeedbackFormData(objFeedbackFormUpload);
                                            SaveDocFiles(Filelist);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                        }
                        #endregion
                    }
                    if (Success1)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Save Sucessfully.";
                        btnGenrateReport.Visible = true;
                        txtRemark.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public void BindMainGridObservation(long AuditID)
        {
            string FinancialYear = string.Empty;
            string PeriodName = string.Empty;
            int CustomerBranchId = -1;
            int VerticalID = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }
            if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
            {
                if (ddlPeriod.SelectedValue != "-1")
                {
                    PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                }
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditDetails = (from row in entities.AuditClosureDetails
                                    where row.ID == (int)AuditID
                                    select row).FirstOrDefault();
                CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                FinancialYear = AuditDetails.FinancialYear;
                PeriodName = AuditDetails.ForMonth;
                VerticalID = Convert.ToInt32(AuditDetails.VerticalId);

            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SPGETObservationDetails_Result> query = new List<SPGETObservationDetails_Result>();
                if (CustomerBranchId != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinancialYear) && !string.IsNullOrEmpty(PeriodName) && AuditID != -1)
                {
                    query = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                             select row).ToList();

                    GridObservationDetails.DataSource = null;
                    GridObservationDetails.DataBind();

                    if (query != null)
                    {
                        lbtObservationList.Visible = btnObservation.Visible = btnDraft.Visible = btnGenrateReport.Visible = true; // added by sagar more on 14-01-2020
                        GridObservationDetails.DataSource = query;
                        Session["TotalRowsObservation"] = query.Count;
                        GridObservationDetails.DataBind();
                        Session["GridObservationDetails"] = null;
                        Session["GridObservationDetails"] = (GridObservationDetails.DataSource as List<SPGETObservationDetails_Result>).ToDataTable();

                        bindObservationPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCountObservation());
                        if (count > 0)
                        {
                            int gridindex = GridObservationDetails.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListObservation.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }
                else if (CustomerBranchId != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinancialYear) && AuditID != -1)
                {
                    query = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                             select row).ToList();

                    GridObservationDetails.DataSource = null;
                    GridObservationDetails.DataBind();

                    if (query != null)
                    {
                        GridObservationDetails.DataSource = query;
                        Session["TotalRowsObservation"] = query.Count;
                        GridObservationDetails.DataBind();
                        Session["GridObservationDetails"] = null;
                        Session["GridObservationDetails"] = (GridObservationDetails.DataSource as List<SPGETObservationDetails_Result>).ToDataTable();

                        bindObservationPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCountObservation());
                        if (count > 0)
                        {
                            int gridindex = GridObservationDetails.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListObservation.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }

            }
        }
        private void bindObservationPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCountObservation());

                if (DropDownListObservation.Items.Count > 0)
                {
                    DropDownListObservation.Items.Clear();
                }

                DropDownListObservation.DataTextField = "ID";
                DropDownListObservation.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListObservation.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListObservation.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListObservation.Items.Add("0");
                    DropDownListObservation.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCountObservation()
        {
            try
            {
                TotalRowsObservation.Value = Session["TotalRowsObservation"].ToString();

                int totalPages = Convert.ToInt32(TotalRowsObservation.Value) / Convert.ToInt32(ddlPageSizeObservation.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsObservation.Value) % Convert.ToInt32(ddlPageSizeObservation.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void DropDownListObservation_SelectedIndexChanged(object sender, EventArgs e)
        {
            long AuditID = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            {
                AuditID = Convert.ToInt64(ViewState["AuditID"]);
            }
            int chkSelectedPage = Convert.ToInt32(DropDownListObservation.SelectedItem.ToString());
            GridObservationDetails.PageIndex = chkSelectedPage - 1;
            GridObservationDetails.PageSize = Convert.ToInt32(ddlPageSizeObservation.SelectedValue);
            BindMainGridObservation(AuditID);
        }
        protected void ddlPageSizeObservation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                GridObservationDetails.PageSize = Convert.ToInt32(ddlPageSizeObservation.SelectedValue);
                BindMainGridObservation(AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnGenrateReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (AuditID == -1)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                        ViewState["AuditID"] = AuditID;
                    }
                }

                string CustomerBranchName = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string PeriodName = string.Empty;
                int VerticalID = -1;
                string VerticalName = string.Empty;
                string ZipFolderName = string.Empty;

                string AditHeadName = string.Empty;
                string BranchName = string.Empty;
                string Area = string.Empty;
                string location = string.Empty;
                string ReportNameAndNumber = string.Empty;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        CustomerBranchName = ddlSubEntity4.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    var videtails = UserManagementRisk.getverticaldetails(Portal.Common.AuthenticationHelper.CustomerID);
                    if (videtails != null)
                    {
                        VerticalName = videtails.VerticalName;
                        VerticalID = videtails.ID;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalName = ddlVerticalID.SelectedItem.Text.Trim();
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["brachName"])))
                {
                    CustomerBranchName = Convert.ToString(ViewState["brachName"]);
                }

                List<string> AssignedUserList = new List<string>();
                var customerName = UserManagementRisk.GetCustomerName(CustomerId);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditDetails = (from row in entities.AuditClosureDetails
                                        where row.ID == AuditID
                                        select row).FirstOrDefault();
                    CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                    FinancialYear = AuditDetails.FinancialYear;
                    PeriodName = AuditDetails.ForMonth;
                    VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                    if (string.IsNullOrEmpty(VerticalName))
                    {
                        VerticalName = (from row in entities.mst_Vertical
                                        where row.ID == VerticalID
                                        select row.VerticalName).FirstOrDefault();
                    }
                    var ReportName =  FinancialYear + "/" + VerticalName + "/";
                    ZipFolderName= FinancialYear + "_" + VerticalName;
                    int ReportNumber = RiskCategoryManagement.GetAuditReportNumber(AuditID, FinancialYear);
                    AuditDetails.ReportName = ReportName;
                    AuditDetails.ReportNumber = ReportNumber;
                    entities.SaveChanges();

                    AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                        join row1 in entities.mst_User
                                        on row.UserID equals row1.ID
                                        where row.IsActive == true
                                        && row.AuditID == AuditID
                                        select row1.FirstName + " " + row1.LastName).Distinct().ToList();

                    ReportNameAndNumber = RiskCategoryManagement.GetReportNameAndNumber(AuditID);
                }

                DateTime draftDate = (DateTime)UserManagementRisk.GetDraftReportDate(CustomerId, CustomerBranchId, VerticalID, PeriodName, FinancialYear);
                var draftReportReleaseDate = draftDate.ToString("MMM dd, yyyy");
                var draftReportReleaseTime = draftDate.ToString("HH:mm");// added by sagar more on 23-01-2020
                string draftReportReleaseDateTime = draftReportReleaseDate + " " + draftReportReleaseTime;

                DateTime releaseDate = (DateTime)UserManagementRisk.GetCreatedDateForGenerateReport(CustomerId, CustomerBranchId, PeriodName, FinancialYear);
                var finalReportReleaseDate = releaseDate.ToString("MMM dd, yyyy");
                var finalReportReleaseTime = releaseDate.ToString("HH:mm"); // added by sagar more on 23-01-2020
                string finalReportReleaseDateTime = finalReportReleaseDate + " " + finalReportReleaseTime;
                #region Word Report 1  
                {
                    System.Text.StringBuilder stringbuilderTOPFirst = new System.Text.StringBuilder();

                    // added by sagar more on 20-01-2020
                    //string SWReportImage = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg";

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                        select row).ToList();

                        var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList(); // added by sagar more on 21-01-2020

                        if (itemlist.Count > 0)
                        {
                            #region first Report
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                + " Area:" + Area + " </b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                           + " Location: " + CustomerBranchName + "</b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                           + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b><u>"
                           + "Part A: Executive Summary</u></b></p>");

                            stringbuilderTOPFirst.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                            "<table style='width:100%; font-family:Calibri;'>" +
                            "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                            "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                            "</table>" +
                            "</td>" +
                            "</tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr></table>");

                            stringbuilderTOPFirst.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 1.Executive Summary </b><br/><br/>");
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> Key findings are as under: </b>");

                            int AsciiValue = 97;
                            int SrNoCategoryTable = 97;
                            var TotalMajorCatg = 0;
                            var TotalModerateCatg = 0;
                            var TotalMinorCatg = 0;
                            var TotalCount = 0;
                            var ProcessRatingName = string.Empty;
                            foreach (var item in itemlist)
                            {
                                stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt;'>"
                                + Convert.ToChar(AsciiValue) + ") " + item.BriefObservation +
                                "<br/><b><u>Management response: </u></b>" + item.ManagementResponse + "</p>");
                                AsciiValue++;
                                if (!string.IsNullOrEmpty(item.ProcessRatingName))
                                {
                                    ProcessRatingName = item.ProcessRatingName;
                                }
                            }

                            stringbuilderTOPFirst.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 
                                A summary of findings based on risk rankings and nature of control gaps is given in the table below: </p><br/>");

                            stringbuilderTOPFirst.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>
                                                        <thead><tr> 
                                                        <th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Sr.No." + "</th>" +
                                                    "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Nature of issues reported" + "</th>" +
                                                    "<th colspan='4' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Risk Rating" + "</th>" +
                                                    "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Page reference (in the detailed report)" +
                                                    "</th></tr><tr><th style='border: 1px solid black; background-color:#FDB924; font-weight:bold'>Major</th>" +
                                                    "<th style='border: 1px solid black; background-color:#FFFF00; font-weight:bold'>Moderate</th>" +
                                                    "<th style='border: 1px solid black; background-color:#92D050; font-weight:bold'>Minor</th>" +
                                                    "<th style='border: 1px solid black; background-color:#BEEAF7; font-weight:bold'>Total</th></tr></thead><tbody>"
                                );

                            var CategoryList = itemlist.Select(entry => entry.ObservationCategory).Distinct().ToList();
                            foreach (var catgID in CategoryList)
                            {
                                var catgName = itemlist.Where(entry => entry.ObservationCategory == catgID).FirstOrDefault();
                                var MajorCatg = itemlist.Where(entry => entry.ObservationRating == 1 && entry.ObservationCategory == catgID).ToList().Count();
                                var ModerateCatg = itemlist.Where(entry => entry.ObservationRating == 2 && entry.ObservationCategory == catgID).ToList().Count();
                                var MinorCatg = itemlist.Where(entry => entry.ObservationRating == 3 && entry.ObservationCategory == catgID).ToList().Count();
                                var Total = MajorCatg + ModerateCatg + MinorCatg;

                                TotalMajorCatg = TotalMajorCatg + MajorCatg;
                                TotalModerateCatg = TotalModerateCatg + ModerateCatg;
                                TotalMinorCatg = TotalMinorCatg + MinorCatg;
                                TotalCount = TotalCount + Total;

                                stringbuilderTOPFirst.Append(@"<tr><td style='border: 1px solid black; text-align: center;'>" + Convert.ToChar(SrNoCategoryTable) + "." +
                                "</td><td style='border: 1px solid black;'>" +
                                catgName.ObservationCategoryName + "</td><td style='border: 1px solid black; text-align: center;'>" + MajorCatg
                                + "</td><td style='border: 1px solid black; text-align: center;'>" + ModerateCatg + "</td><td style='border: 1px solid black; text-align: center;'>" + MinorCatg + "</td><td style='border: 1px solid black;  text-align: center;'>" + Total + "</td><td style='border: 1px solid black;'></td></tr>"
                                );
                                SrNoCategoryTable++;
                            }
                            stringbuilderTOPFirst.Append(@"</tbody><tfoot><tr><td Colspan='2' style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'>Total</td>
                                  <td style='border: 1px solid black; background-color:#FDB924; font-weight:bold; text-align: center;'>" + TotalMajorCatg
                            + "</td><td style='border: 1px solid black; background-color:#FFFF00; font-weight:bold; text-align: center;'>" + TotalModerateCatg
                            + "</td><td style='border: 1px solid black; background-color:#92D050; font-weight:bold; text-align: center;'>" + TotalMinorCatg
                            + "</td><td style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold; text-align: center;'>" + TotalCount
                            + "</td><td style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'></td></tr>"
                            + "<tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Process Rating and Maturity Level</td>");
                            if (!string.IsNullOrEmpty(ProcessRatingName))
                            {
                                stringbuilderTOPFirst.Append(@"<td colspan='5' style='border: 1px solid black;'>" + ProcessRatingName + "</td></tr>");
                            }
                            else
                            {
                                stringbuilderTOPFirst.Append(@"<td colspan='5' style='border: 1px solid black;'></td></tr>");
                            }
                            stringbuilderTOPFirst.Append(@"< tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Overall conclusion</td><td colspan='5' style='border: 1px solid black; font-weight:bold'></td></tr>"
                             + "</tfoot></table>"
                             );

                            stringbuilderTOPFirst.Append(@"</table>");
                            ProcessRequestWordFirst(stringbuilderTOPFirst.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId, CustomerBranchName);
                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                        }
                    }
                }
                #endregion

                #region Word Report Second                 
                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                int AnnexueId = 1;
                //string SWReportImage1 = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg"; // added by sagar more on 21-01-2020
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditDetails = (from row in entities.AuditClosureDetails
                                        where row.ID == AuditID
                                        select row).FirstOrDefault();
                    CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                    FinancialYear = AuditDetails.FinancialYear;
                    PeriodName = AuditDetails.ForMonth;
                    VerticalID = Convert.ToInt32(AuditDetails.VerticalId);

                    CustomerBranchName = (from row in entities.mst_CustomerBranch
                                          where row.ID == CustomerBranchId
                                          select row.Name).FirstOrDefault().ToString();

                    var s = (from row in entities.Tran_FinalDeliverableUpload
                             where row.AuditID == AuditID
                             select row).ToList();
                    AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                        join row1 in entities.mst_User
                                        on row.UserID equals row1.ID
                                        where row.IsActive == true
                                        && row.AuditID == AuditID
                                        select row1.FirstName + " " + row1.LastName).Distinct().ToList();
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                    select row).ToList();

                    var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList(); // added by sagar more on 21-01-2020
                    if (itemlist.Count > 0)
                    {
                        #region first Report
                        stringbuilderTOP.Append(@"<p style='margin-top:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                + " Area: " + Area + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                       + " Location: " + CustomerBranchName + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                       + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b><u>"
                       + "Part B: Internal Audit report </u></b></p>");

                        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                        "<table style='width:100%; font-family:Calibri;'>" +
                        "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                        "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                        "</table>" +
                        "</td>" +
                        "</tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr></table>");

                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b> Table of Contents </b>");

                        stringbuilderTOP.Append("<ol>");
                        stringbuilderTOP.Append("<li><b>Background:......................................................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("<li><b>Audit Scope and Objective:...............................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("<li><b>Detailed Findings:..............................................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("</ol></p>");


                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 1. Background: </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 2. Audit Scope and Objective: </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-left:15px;margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> Objective(s) of the review:  </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 3. Detailed Findings: </b></p><br/><br/>");

                        int SrNo = 01;

                        foreach (var item in itemlist)
                        {
                            string ColorCode = string.Empty;
                            if (SrNo != 01)
                            {
                                stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                            }
                            stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border-top: 1px solid black;'>");
                            stringbuilderTOP.Append(@"<tr> 
                                    <td colspan='2' style='width:70%;border-top: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Issue # " + SrNo + " : " + item.ObservationTitle + "</td>");
                            if (item.ObservationRating == 1)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#FDB924; font-weight:bold'>Major/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            else if (item.ObservationRating == 2)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#FFFF00; font-weight:bold'>Moderate/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            else if (item.ObservationRating == 3)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#92D050; font-weight:bold'>Minor/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            stringbuilderTOP.Append(@"<tr><td colspan='3'>" + item.ObjBackground + "</td ></tr>" +
                            "<tr><td style='width:30%; border-top: 1px solid black; font-weight:bold'>Finding(s)</td>" +
                            "<td style='width:40%; border-top: 1px solid black;font-weight:bold'>Probable Cause(s)</td>" +
                            "<td style='width:30%; border-top: 1px solid black;font-weight:bold'>Recommendation(s)</td></tr>" +
                            "<tr><td style='width:30%; border-top: 1px solid black;'>" + item.Observation + "</td>" +
                            "<td valign='top' style='width:40%; border-top: 1px solid black;'>" + item.RootCost + "</td>" +
                            "<td style='width:30%; border-top: 1px solid black;'>" + item.Recomendation + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;font-weight:bold'>Impact/Potential Risk(s)</td>" +
                                "<td style='width:30%;font-weight:bold'>Management Action Plan(s)</td></tr>");

                            string Timeline = item.TimeLine != null ? item.TimeLine.Value.ToString("MMM dd, yyyy") : null;

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td  valign='top' style='width:40%;'>" + item.FinancialImpact + "</td>" +
                                "<td style='width:30%;'>" + item.ManagementResponse + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;'></td>" +
                                "<td style='width:30%;'>" + "<b>Responsibility</b> : " + item.PersonResponsibleName + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;'></td>" +
                                "<td style='width:30%;'>" + "<b>Timeline</b> : " + Timeline + "</td></tr>");

                            stringbuilderTOP.Append(@"</table>");
                            stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                            List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDID);
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetObservationAudioVideoList(AuditID, item.ATBDID);

                            if (!string.IsNullOrEmpty(item.BodyContent) || AllinOneDocumentList.Count > 0 || observationAudioVideoList.Count > 0)
                            {
                                stringbuilderTOP.Append(@"<label><b>Annexure:</b>" + AnnexueId + "</label><br>");
                                stringbuilderTOP.Append(@"<label><b>" + item.AnnexueTitle.Trim() + "</b></label><br/>");// changed by sagar more on 22-01-2020
                                AnnexueId++;
                            }
                            if (!string.IsNullOrEmpty(item.BodyContent))
                            {
                                stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                                stringbuilderTOP.Append(@"<P style='margin-top:1%;margin-bottom:1%; font-family:Calibri;'>" + item.BodyContent.Replace("@nbsp;", "").Replace("\r", "").Replace("\n", "") + "</p>");
                                stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                            }

                           
                            if (AllinOneDocumentList.Count > 0)
                            {
                                foreach (var ObjImageitem in AllinOneDocumentList)
                                {

                                    string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                    if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                                    {
                                        stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                                        stringbuilderTOP.Append(@"<P style='margin-top:1%;margin-bottom:1%; font-family:Calibri;''>");
                                        DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                        // DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        stringbuilderTOP.Append("<img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                                        + CustomerBranchId + "/" + VerticalID + "/" + item.ProcessId + "/"
                                        + item.FinancialYear + "/" + item.ForPeriod + "/"
                                        + AuditID + "/"
                                        + item.ATBDID + "/1.0/" + ObjImageitem.ImageName + "' alt ='d'></img >" + "<br>");
                                        stringbuilderTOP.Append(@"</p>");
                                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    }
                                }
                            }
                            int audioVideoChar = 97;
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (var audioVideoLink in observationAudioVideoList)
                                {
                                    char alpha = Convert.ToChar(audioVideoChar);
                                    stringbuilderTOP.Append(@"<label>" + alpha + ")</label>&nbsp;&nbsp;<a href=" + audioVideoLink.AudioVideoLink.Trim() + ">" + audioVideoLink.AudioVideoLink.Trim() + "</a><br/>");
                                    audioVideoChar++;
                                }
                            }
                            stringbuilderTOP.Append(@"<br clear=all style='mso-special-character:line-break;page-break-before:always'>");
                        }
                        ProcessRequestWordSecond(stringbuilderTOP.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId, CustomerBranchName);
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                    }
                }
                #endregion

                #region Word Report   
                #region  code comment
                //System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                //System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                //                    select row).ToList();
                //    if (itemlist.Count > 0)
                //    {
                //        if (string.IsNullOrEmpty(CustomerBranchName))
                //        {
                //            CustomerBranchName = Convert.ToString(ViewState["brachName"]);
                //        }
                //        if (string.IsNullOrEmpty(VerticalName))
                //        {
                //            VerticalName = Convert.ToString(ViewState["vertical"]);
                //        }
                //        #region first Report
                //        stringbuilderTOP.Append(@"<style>.break { page-break-before: always;}</style>");

                //        stringbuilderTOP.Append(@"<table style ='width:100%;font-family:Arial;'>" +
                //       "<tr>" +
                //       "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                //        "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                //        "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                //        "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                //        "<td style ='width:40%;text-align:left;'>" + VerticalName + "</td></tr></table>");

                //        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Arial; border-collapse: collapse; border: 1px solid black;'>" +
                //        "<tr>" +
                //        "<th style='width:15%;border: 1px solid black;'><b>Control No.</b></th>" +
                //        "<th style='width:25%;border: 1px solid black;'><b>Process/Sub Process/ Control Objective & Control Activity</b></th>" +
                //        "<th style='width:25%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                //        "<th style='width:20%;border: 1px solid black;'><b>Action Plan/Due Date</b></th>" +
                //        "<th style='width:15%;border: 1px solid black;'><b>Risk Rating</b></th>" +
                //        "</tr>");


                //        foreach (var cat in itemlist)
                //        {
                //            string ControlNo = string.Empty;
                //            string Observation = string.Empty;
                //            string ManagementResponse = string.Empty;
                //            DateTime TimeLine = new DateTime();
                //            string ProcessName = string.Empty;
                //            string SubProcessName = string.Empty;
                //            string ControlObjective = string.Empty;
                //            string ControlDescription = string.Empty;
                //            string RootCause = string.Empty;
                //            string Branchname = string.Empty;
                //            string RiskRating = string.Empty;
                //            string PSPCOCD = string.Empty;
                //            string ORC = string.Empty;
                //            string APT = string.Empty;

                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                //            {
                //                ControlNo = cat.ControlNo;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                //            {
                //                Observation = cat.Observation;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                //            {
                //                ManagementResponse = cat.ManagementResponse;

                //            }
                //            if (cat.TimeLine != null)
                //            {
                //                TimeLine = Convert.ToDateTime(cat.TimeLine);
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                //            {
                //                ProcessName = cat.ProcessName;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                //            {
                //                SubProcessName = cat.SubProcessName;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                //            {
                //                ControlObjective = cat.ControlObjective;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                //            {
                //                ControlDescription = cat.ControlDescription;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                //            {
                //                RootCause = cat.RootCost;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                //            {
                //                Branchname = cat.Branch;
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                //            {
                //                FinancialYear = Convert.ToString(cat.FinancialYear);
                //            }
                //            if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                //            {
                //                RiskRating = Convert.ToString(cat.RiskRatingName);
                //            }

                //            PSPCOCD = ProcessName + " /" + SubProcessName;

                //            ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                //            APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                //            string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                //                 "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                //                 "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                //            string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                //            stringbuilderTOP.Append(@"<tr>" +
                //            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                //            "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                //            "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                //            "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                //            "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                //            "</tr>");
                //        }

                //        stringbuilderTOP.Append(@"</table>");
                //        ProcessRequest_AsPerClientFirst(stringbuilderTOP.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);

                //        #endregion

                //        #region second report
                //        stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");


                //        stringbuilderTOPSecond.Append(@"<table style ='width:100%;font-family:Arial;'>" +
                //       "<tr>" +
                //       "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                //        "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                //        "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                //        "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                //        "<td style ='width:40%;text-align:left;'>" + VerticalName + "</td></tr></table>");


                //        var RiskRatinglist = (from MCRR in entities.mst_Risk_ControlRating
                //                              where MCRR.IsRiskControl == "R"
                //                              select MCRR).OrderByDescending(entry => entry.Value).ToList();

                //        if (RiskRatinglist.Count > 0)
                //        {
                //            foreach (var item in RiskRatinglist)
                //            {
                //                var ItemDetails = itemlist.Where(a => a.RiskRating == item.Value).ToList();
                //                int rcnt = 1;
                //                if (ItemDetails.Count > 0)
                //                {
                //                    //stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");
                //                    stringbuilderTOPSecond.Append(@"<table style='width:100%; font-family:Arial; border-collapse: collapse; border: 1px solid black;'>" +
                //                    "<tr>" +
                //                    "<td colspan ='5'><b>" + item.Name.Trim() + " Risk </b></td></tr>" +
                //                    "<tr>" +
                //                    "<th style='width:5%;border: 1px solid black;'><b>Sr No.</b></th>" +
                //                    "<th style='width:30%;border: 1px solid black;'><b>Process/Sub Process</b></th>" +
                //                    "<th style='width:35%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                //                    "<th style='width:15%;border: 1px solid black;'><b>Location</b></th>" +
                //                    "<th style='width:15%;border: 1px solid black;'><b>Action Plan</b></th>" +
                //                    "</tr>");
                //                    foreach (var cat in ItemDetails)
                //                    {
                //                        string ControlNo = string.Empty;
                //                        string Observation = string.Empty;
                //                        string ManagementResponse = string.Empty;
                //                        DateTime TimeLine = new DateTime();
                //                        string ProcessName = string.Empty;
                //                        string SubProcessName = string.Empty;
                //                        string ControlObjective = string.Empty;
                //                        string ControlDescription = string.Empty;
                //                        string RootCause = string.Empty;
                //                        string Branchname = string.Empty;
                //                        string RiskRating = string.Empty;
                //                        string PSPCOCD = string.Empty;
                //                        string ORC = string.Empty;
                //                        string APT = string.Empty;

                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                //                        {
                //                            ControlNo = cat.ControlNo;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                //                        {
                //                            Observation = cat.Observation;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                //                        {
                //                            ManagementResponse = cat.ManagementResponse;

                //                        }
                //                        if (cat.TimeLine != null)
                //                        {
                //                            TimeLine = Convert.ToDateTime(cat.TimeLine);
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                //                        {
                //                            ProcessName = cat.ProcessName;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                //                        {
                //                            SubProcessName = cat.SubProcessName;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                //                        {
                //                            ControlObjective = cat.ControlObjective;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                //                        {
                //                            ControlDescription = cat.ControlDescription;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                //                        {
                //                            RootCause = cat.RootCost;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                //                        {
                //                            Branchname = cat.Branch;
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                //                        {
                //                            FinancialYear = Convert.ToString(cat.FinancialYear);
                //                        }
                //                        if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                //                        {
                //                            RiskRating = Convert.ToString(cat.RiskRatingName);
                //                        }

                //                        PSPCOCD = ProcessName + " /" + SubProcessName;

                //                        ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                //                        APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                //                        string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                //                             "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                //                             "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                //                        string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                //                        stringbuilderTOP.Append(@"<tr>" +
                //                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                //                        "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                //                        "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                //                        "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                //                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                //                        "</tr>");

                //                        PSPCOCD = string.Empty;
                //                        ORC = string.Empty;
                //                        APT = string.Empty;

                //                        PSPCOCD = ProcessName + " /" + SubProcessName;
                //                        ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";
                //                        APT = ManagementResponse;
                //                        stringbuilderTOPSecond.Append(@"<tr>" +
                //                        "<td style='width:5%;border: 1px solid black;vertical-align: top;'>" + rcnt + "</td>" +
                //                        "<td style='width:30%;border: 1px solid black;vertical-align: top;'>" + PSPCOCD.Trim() + "</td>" +
                //                        "<td style='width:35%;border: 1px solid black;vertical-align: top;'>" + ORC.Trim() + "</td>" +
                //                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + Branchname.Trim() + "</td>" +
                //                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ManagementResponse.Trim() + "</td>" +
                //                        "</tr>");
                //                        rcnt++;
                //                    }//itemdetails foreach end
                //                    stringbuilderTOPSecond.Append(@"</table>");
                //                }
                //            }//Riskrating details foreach end
                //        }
                //        ProcessRequest_AsPerClientSECOND(stringbuilderTOPSecond.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);
                //        #endregion
                //    }
                //} 
                #endregion
                #endregion

                #region Excel Report
                #region code comment
                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                //                    select row).ToList();
                //    if (itemlist.Count > 0)
                //    {

                //        using (ExcelPackage exportPackge = new ExcelPackage())
                //        {
                //            try
                //            {
                //                String FileName = String.Empty;
                //                ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Observation Report");
                //                DataTable ExcelData = null;
                //                DataView view = new System.Data.DataView((itemlist as List<SPGETObservationDetails_AsPerClient_Result>).ToDataTable());

                //                ExcelData = view.ToTable("Selected", false, "ProcessName", "SubProcessName", "Risk", "ControlObjective", "ControlDescription", "Observation",
                //                "RootCost", "ManagementResponse", "PersonResponsibleName", "TimeLine", "Branch");

                //                foreach (DataRow item in ExcelData.Rows)
                //                {
                //                    if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value)
                //                        item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                //                }

                //                FileName = "Observation Report";
                //                exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                //                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                //                exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["A2"].Value = customerName;
                //                exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["A3"].Value = FileName;
                //                exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["A4"].Value = "Location";
                //                exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A4"].Style.Font.Size = 12;


                //                exWorkSheet.Cells["B4"].Value = CustomerBranchName;
                //                exWorkSheet.Cells["B4"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["A5"].Value = "Vertical";
                //                exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A5"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["B5"].Value = VerticalName.Trim();
                //                exWorkSheet.Cells["B5"].Style.Font.Size = 12;

                //                exWorkSheet.Cells["A7"].LoadFromDataTable(ExcelData, true);

                //                exWorkSheet.Cells["A7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["A7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["A7"].Value = "Process";
                //                exWorkSheet.Cells["A7"].AutoFitColumns(35);

                //                exWorkSheet.Cells["B7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["B7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["B7"].Value = "Sub Process";
                //                exWorkSheet.Cells["B7"].AutoFitColumns(25);

                //                exWorkSheet.Cells["C7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["C7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["C7"].Value = "Risk";
                //                exWorkSheet.Cells["C7"].AutoFitColumns(25);

                //                exWorkSheet.Cells["D7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["D7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["D7"].Value = "Control Objective";
                //                exWorkSheet.Cells["D7"].AutoFitColumns(50);

                //                exWorkSheet.Cells["E7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["E7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["E7"].Value = "Entity Control";
                //                exWorkSheet.Cells["E7"].AutoFitColumns(50);

                //                exWorkSheet.Cells["F7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["F7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["F7"].Value = "Observation";
                //                exWorkSheet.Cells["F7"].AutoFitColumns(30);

                //                exWorkSheet.Cells["G7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["G7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["G7"].Value = "Root Cause";
                //                exWorkSheet.Cells["G7"].AutoFitColumns(30);

                //                exWorkSheet.Cells["H7"].Value = "Management Action Plan";
                //                exWorkSheet.Cells["H7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["H7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["H7"].AutoFitColumns(50);

                //                exWorkSheet.Cells["I7"].Value = "Process Owner";
                //                exWorkSheet.Cells["I7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["I7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["I7"].AutoFitColumns(30);

                //                exWorkSheet.Cells["J7"].Value = "Due Date";
                //                exWorkSheet.Cells["J7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["J7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["J7"].AutoFitColumns(20);

                //                exWorkSheet.Cells["K7"].Value = "Location";
                //                exWorkSheet.Cells["K7"].Style.Font.Bold = true;
                //                exWorkSheet.Cells["K7"].Style.Font.Size = 12;
                //                exWorkSheet.Cells["K7"].AutoFitColumns(30);

                //                using (ExcelRange col = exWorkSheet.Cells[7, 1, 7 + ExcelData.Rows.Count, 11])
                //                {
                //                    col.Style.Numberformat.Format = "dd-MMM-yyyy";
                //                    col.Style.WrapText = true;
                //                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                //                    // Assign borders
                //                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                //                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                //                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                //                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //                }

                //                Byte[] fileBytes = exportPackge.GetAsByteArray();

                //                #region Save Code
                //                if (string.IsNullOrEmpty(PeriodName))
                //                {
                //                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                //                    if (peridodetails != null)
                //                    {
                //                        if (peridodetails.Count > 0)
                //                        {
                //                            foreach (var ForPeriodName in peridodetails)
                //                            {
                //                                string fileName = "Draft Observation Report" + FinancialYear + ForPeriodName + ".xlsx";
                //                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                //                                {
                //                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                                    FinancialYear = Convert.ToString(FinancialYear),
                //                                    Period = Convert.ToString(ForPeriodName),
                //                                    CustomerId = CustomerId,
                //                                    VerticalID = Convert.ToInt32(VerticalID),
                //                                    FileName = fileName,
                //                                    AuditID = AuditID,
                //                                };
                //                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //                                string directoryPath1 = "";
                //                                bool Success1 = false;
                //                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                //                                {
                //                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + CustomerId + "/"
                //                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                //                                   + "/1.0");
                //                                    if (!Directory.Exists(directoryPath1))
                //                                    {
                //                                        DocumentManagement.CreateDirectory(directoryPath1);
                //                                    }
                //                                    Guid fileKey1 = Guid.NewGuid();
                //                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                //                                    FinalDeleverableUpload.Version = "1.0";
                //                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                //                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                //                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                //                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    string fileName = "Draft Observation Report" + FinancialYear + PeriodName + ".xlsx";
                //                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                //                    {
                //                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                        FinancialYear = Convert.ToString(FinancialYear),
                //                        Period = Convert.ToString(PeriodName),
                //                        CustomerId = CustomerId,
                //                        VerticalID = Convert.ToInt32(VerticalID),
                //                        FileName = fileName,
                //                        AuditID = AuditID,
                //                    };
                //                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //                    string directoryPath1 = "";

                //                    bool Success1 = false;
                //                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                //                    {
                //                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + CustomerId + "/"
                //                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                //                       + "/1.0");
                //                        if (!Directory.Exists(directoryPath1))
                //                        {
                //                            DocumentManagement.CreateDirectory(directoryPath1);
                //                        }

                //                        Guid fileKey1 = Guid.NewGuid();
                //                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                        FinalDeleverableUpload.FileName = fileName;
                //                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                //                        FinalDeleverableUpload.Version = "1.0";
                //                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                //                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                //                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                //                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                //                    }
                //                }
                //                #endregion

                //                #region Excel Download Code
                //                //Response.ClearContent();
                //                //Response.Buffer = true;
                //                //string fileName = string.Empty;
                //                //fileName = "asd.xlsx";
                //                //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                //                //Response.Charset = "";
                //                //Response.ContentType = "application/vnd.ms-excel";
                //                //StringWriter sw = new StringWriter();
                //                //Response.BinaryWrite(fileBytes);
                //                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                //                #endregion

                //            }
                //            catch (Exception ex)
                //            {
                //                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //            }
                //        }
                //    }
                //} 
                #endregion
                #endregion

                #region PowerPoint Report               
                #region code comment
                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    var details = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName)
                //                   select row).ToList();


                //    string BranchName = string.Empty;
                //    if (details.Count > 0)
                //    {
                //        var ProcessList = GetProcessListOrdered(details);

                //        float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                //        float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;


                //        String PPTName = "Draft Internal Audit Report - " + PeriodName;

                //        string CustomerName = GetByID(customerID).Name;
                //        string colorname = String.Empty;


                //        colorname = "Chocolate";

                //        CreateDymanicHeadingFirstSlide(SlideCounter, xpointsection1, 10, 700, 520, "", 32, "L", colorname);
                //        CreateDymanicHeading(SlideCounter, xpointsection1, 80, 700, 100, CustomerName, 32, "L", colorname);
                //        CreateDymanicHeading(SlideCounter, xpointsection1, 150, 700, 100, BranchName, 30, "L", colorname);
                //        CreateDymanicHeading(SlideCounter, xpointsection1, 200, 700, 60, PPTName, 33, "L", colorname);
                //        CreateDymanicHeading(SlideCounter, xpointsection1, 250, 700, 60, "Year- " + FinancialYear + "", 24, "L", colorname);
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        PPTName = PPTName + " " + "(Year " + FinancialYear + ")";

                //        CreateDymanicPara(SlideCounter, xpointsection1, 200, 700, 205, "Ignore – to be replaced by cover letter", 22);
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 100, "Table of Content", 32, "L", colorname);
                //        DynamicTableOfContent(SlideCounter, xpointsection1, 120, 700, 300, "", 24, colorname, ProcessList);
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", "White");
                //        SetSlideColor(SlideCounter, colorname);
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", colorname);

                //        //Add New Blank Slide 
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        int ProcessID = -1;

                //        int Count = 0;
                //        int ProcessNumber = 0;
                //        int ObservationNumber = 0;

                //        String ObservationNo = string.Empty;

                //        details.ForEach(entry =>
                //        {
                //            String SectionHeading = "Section " + intToRoman(Count + 2) + "";

                //            ObservationNumber++;

                //            if (ProcessID == -1 || ProcessID != entry.ProcessId)
                //            {
                //                if (Count < ProcessList.Count)
                //                {
                //                    ProcessID = Convert.ToInt32(entry.ProcessId);
                //                    ProcessNumber++;
                //                    ObservationNumber = 1;

                //                    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 50, SectionHeading, 32, "L", "White");
                //                    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 50, ProcessList.ElementAt(Count), 32, "L", "White");
                //                    SetSlideColor(SlideCounter, colorname);
                //                    presentationdynamic.Slides.Append();
                //                    SlideCounter++;
                //                    Count++;
                //                }
                //                else
                //                {

                //                }
                //            }

                //            int checkvalueobservation = 0;
                //            int checkvalueRisk = 0;
                //            int checkvalueRecomentdation = 0;
                //            int checkvalueManagementresponse = 0;
                //            int checkvalueTimeLine = 0;

                //            string[] arrayobservation = entry.Observation.Split(' ');
                //            int arrobservationLength = arrayobservation.Length;
                //            checkvalueobservation = (2 * arrobservationLength);

                //            string[] arrayrisk = entry.Risk.Split(' ');
                //            int arrLengthrisk = arrayrisk.Length;
                //            checkvalueRisk = (2 * arrLengthrisk);

                //            string[] arrayRecommendations = entry.Recomendation.Split(' ');
                //            int arrLengthRecommendations = arrayRecommendations.Length;
                //            checkvalueRecomentdation = (2 * arrLengthRecommendations);

                //            string[] arrayManagementResponse = entry.ManagementResponse.Split(' ');
                //            int arrLengthManagementResponsek = arrayManagementResponse.Length;
                //            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

                //            string[] arrayPersonresponsible = entry.ManagementResponse.Split(' ');
                //            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
                //            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

                //            //add footer
                //            presentationdynamic.SetFooterText(PPTName);

                //            //set the footer visible
                //            presentationdynamic.SetFooterVisible(true);

                //            //set the page number visible
                //            presentationdynamic.SetSlideNoVisible(true);

                //            //set the date visible
                //            presentationdynamic.SetDateTimeVisible(true);

                //            if (checkvalueobservation <= 200 && checkvalueRisk <= 200 && checkvalueRecomentdation <= 150
                //                && checkvalueManagementresponse <= 150 && checkvalueTimeLine <= 100)
                //            {
                //                ObservationNo = ProcessNumber + "." + ObservationNumber + " ";
                //                //Observation Title by default null
                //                //PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                //                PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                //                presentationdynamic.Slides.Append();
                //                SlideCounter++;
                //            }
                //            else
                //            {
                //                //Observation Title by default null
                //                PrintDynamicObservationSlide(SlideCounter, Regex.Replace(entry.ObservationNumber, @"\t|\n|\r", "") , Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                //                presentationdynamic.Slides.Append();
                //                SlideCounter++;
                //            }
                //        });

                //        CreateDymanicHeading(SlideCounter, xpointsection1 + 100, 200, 300, 100, "Thank You", 32, "L", "White");
                //        SetSlideColor(SlideCounter, colorname);
                //        presentationdynamic.Slides.Append();
                //        SlideCounter++;

                //        string dt = DateTime.Now.ToString("dd-MMM-yy");
                //        if ((BranchName.Split(' ').First() + "-" + PPTName + "-" + dt).Length < 218)
                //            PPTName = BranchName.Split(' ').FirstOrDefault() + "-" + PPTName + "-" + dt;

                //        string path = Server.MapPath("~//PPTDownload//" + PPTName + ".pptx");

                //        presentationdynamic.DocumentProperty["_MarkAsFinal"] = true;
                //        presentationdynamic.SaveToFile(path, FileFormat.Pptx2010);


                //        Byte[] fileBytes = presentationdynamic.GetBytes();
                //        if (string.IsNullOrEmpty(PeriodName))
                //        {
                //            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear);
                //            if (peridodetails != null)
                //            {
                //                if (peridodetails.Count > 0)
                //                {
                //                    foreach (var ForPeriodName in peridodetails)
                //                    {
                //                        string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".pptx";
                //                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                //                        {
                //                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                            FinancialYear = Convert.ToString(FinancialYear),
                //                            Period = Convert.ToString(ForPeriodName),
                //                            CustomerId = customerID,
                //                            VerticalID = Convert.ToInt32(VerticalID),
                //                            FileName = fileName,
                //                        };
                //                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //                        string directoryPath1 = "";
                //                        bool Success1 = false;
                //                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                //                        {
                //                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                //                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                //                           + "/1.0");
                //                            if (!Directory.Exists(directoryPath1))
                //                            {
                //                                DocumentManagement.CreateDirectory(directoryPath1);
                //                            }
                //                            Guid fileKey1 = Guid.NewGuid();
                //                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                //                            FinalDeleverableUpload.Version = "1.0";
                //                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                //                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                //                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                            DocumentManagement.SaveDocFiles(Filelist1);
                //                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                //                        }

                //                    }
                //                }
                //            }
                //        }
                //        else
                //        {
                //            string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".pptx";
                //            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                //            {
                //                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                //                FinancialYear = Convert.ToString(FinancialYear),
                //                Period = Convert.ToString(PeriodName),
                //                CustomerId = customerID,
                //                VerticalID = Convert.ToInt32(VerticalID),
                //                FileName = fileName,
                //            };
                //            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                //            string directoryPath1 = "";

                //            bool Success1 = false;
                //            if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                //            {
                //                directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                //               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                //               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                //               + "/1.0");
                //                if (!Directory.Exists(directoryPath1))
                //                {
                //                    DocumentManagement.CreateDirectory(directoryPath1);
                //                }

                //                Guid fileKey1 = Guid.NewGuid();
                //                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                //                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                //                FinalDeleverableUpload.FileName = fileName;
                //                FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                //                FinalDeleverableUpload.FileKey = fileKey1.ToString();
                //                FinalDeleverableUpload.Version = "1.0";
                //                FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                //                FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                //                FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //                DocumentManagement.SaveDocFiles(Filelist1);
                //                Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                //            }

                //        }
                //    }
                //    #region PPT Download Code
                //    //Response.ContentType = "application/octet-stream";
                //    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + PPTName + ".pptx");
                //    //Response.TransmitFile(path);
                //    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                //    #endregion
                //} 
                #endregion
                #endregion

                List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                if (FinalDeliverableDocument != null)
                {
                    using (ZipFile AuditZip = new ZipFile())
                    {
                        if (!string.IsNullOrEmpty(PeriodName))
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                        }
                        else
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).ToList();//.Where(entry => entry.Version.Equals("1.0")).ToList();
                        }
                        AuditZip.AddDirectoryByName(FinancialYear);
                        if (FinalDeliverableDocument.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in FinalDeliverableDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        AuditZip.AddEntry(FinancialYear + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        AuditZip.AddEntry(FinancialYear + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                            var zipMs = new MemoryStream();
                            AuditZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ReportGenerated"+ ZipFolderName + ".zip");
                            Response.BinaryWrite(Filedata);
                            Response.Flush();
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                        }
                    }
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var result = (from row in entities.Tran_FinalDeliverableUpload
                                      where row.AuditID == AuditID
                                      select row).ToList().Count();

                        HiddenObjValue.Value = Convert.ToString(result);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnGenrateReport_PERVIOUS_Click(object sender, EventArgs e)
        {
            try
            {
                string CustomerBranchName = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string PeriodName = string.Empty;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                int VerticalID = -1;
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                    }
                }


                var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);


                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                #region Word Report                
                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                    select row).ToList();
                    if (itemlist.Count > 0)
                    {
                        foreach (var cat in itemlist)
                        {
                            string ObservationNumber = string.Empty;
                            string ObservationTitle = string.Empty;
                            string Observation = string.Empty;
                            string Risk = string.Empty;
                            string Recomendation = string.Empty;
                            string ManagementResponse = string.Empty;
                            DateTime TimeLine = new DateTime();
                            string PersonResponsible = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ObservationNumber)))
                            {
                                ObservationNumber = cat.ObservationNumber;
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ObservationTitle)))
                            {
                                ObservationTitle = cat.ObservationTitle;
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                            {
                                Observation = cat.Observation;
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.Risk)))
                            {
                                Risk = cat.Risk;
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.Recomendation)))
                            {
                                Recomendation = cat.Recomendation;
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                            {
                                ManagementResponse = cat.ManagementResponse;
                            }
                            if (cat.TimeLine != null)
                            {
                                TimeLine = Convert.ToDateTime(cat.TimeLine);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.PersonResponsible)))
                            {
                                PersonResponsible = GetUserName(Convert.ToInt32(cat.PersonResponsible));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                            {
                                FinancialYear = Convert.ToString(cat.FinancialYear);
                            }


                            stringbuilderTOP.Append(@" <style>
                                             .break { page-break-before: always; }
                                            </style>");

                            stringbuilderTOP.Append(@"<table style='width:100%; font-family:Arial;'>" +
                            "<tr><td colspan='1' style='width:20%'><b>Observation No.</b></td><td colspan='3' style='text-align:left;width:80%'>" + ObservationNumber + "</td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><b>" + ObservationTitle + " </b></td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>Observation: </b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Observation + "</td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>Risk: </b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Risk + "</td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>Recommendation:</b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Recomendation + "</td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>Management Response:</b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + ManagementResponse + "</td></tr> " +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>TimeLine:</b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + TimeLine.ToString("dd-MMM-yyyy") + "</td></tr>" +
                            "<tr><td colspan='4' style='margin-top:50%'><br><b>Person Responsible:</b></td></tr>" +
                            "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + PersonResponsible + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td colspan='4'><h1 class='break'>&nbsp;</h1></td></tr>");
                            stringbuilderTOP.Append(@"</table>");
                        }
                        ProcessRequest(stringbuilderTOP.ToString(), customer.ID, customer.Name, PeriodName, FinancialYear, VerticalID, CustomerBranchId);
                    }
                }
                #endregion

                #region Excel Report

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Open Observation Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["GridObservationDetails"]);
                        ExcelData = view.ToTable("Selected", false, "Branch", "FinancialYear", "ForPeriod", "ProcessName", "Observation", "ObservationCategoryName",
                        "ObservationSubCategoryName", "Risk", "Recomendation", "ManagementResponse", "PersonResponsibleName", "TimeLine",
                        "ProcessId", "ObservationCategory", "ObservationSubCategory", "PersonResponsible");
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Branch"] = ShowLocation(Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["ObservationCategory"].ToString()),
                            Convert.ToInt32(item["ObservationSubCategory"].ToString()), item["FinancialYear"].ToString(),
                            item["Observation"].ToString(), item["ManagementResponse"].ToString(), Convert.ToInt32(item["PersonResponsible"].ToString()));
                            if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value)
                                item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                        }
                        ExcelData.Columns.Remove("ProcessId");
                        ExcelData.Columns.Remove("ObservationCategory");
                        ExcelData.Columns.Remove("ObservationSubCategory");
                        ExcelData.Columns.Remove("PersonResponsible");

                        FileName = "Open Observation Report";
                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A2"].Value = customer.Name;
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;


                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = FileName;
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A6"].Value = "Location";
                        exWorkSheet.Cells["A6"].AutoFitColumns(50);

                        exWorkSheet.Cells["B6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].Value = "FinancialYear";
                        exWorkSheet.Cells["B6"].AutoFitColumns(25);

                        exWorkSheet.Cells["C6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C6"].Value = "ForMonth";
                        exWorkSheet.Cells["C6"].AutoFitColumns(25);

                        exWorkSheet.Cells["D6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D6"].Value = "Process";
                        exWorkSheet.Cells["D6"].AutoFitColumns(50);

                        exWorkSheet.Cells["E6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E6"].Value = "Observation";
                        exWorkSheet.Cells["E6"].AutoFitColumns(50);


                        exWorkSheet.Cells["F6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F6"].Value = "Observation Category Name";
                        exWorkSheet.Cells["F6"].AutoFitColumns(30);

                        exWorkSheet.Cells["G6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G6"].Value = "Observation Sub Category Name";
                        exWorkSheet.Cells["G6"].AutoFitColumns(30);

                        exWorkSheet.Cells["H6"].Value = "Risk";
                        exWorkSheet.Cells["H6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H6"].AutoFitColumns(50);

                        exWorkSheet.Cells["I6"].Value = "Recomendation";
                        exWorkSheet.Cells["I6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I6"].AutoFitColumns(50);

                        exWorkSheet.Cells["J6"].Value = "Management Response";
                        exWorkSheet.Cells["J6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J6"].AutoFitColumns(50);

                        exWorkSheet.Cells["K6"].Value = "PersonResponsible";
                        exWorkSheet.Cells["K6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K6"].AutoFitColumns(30);

                        exWorkSheet.Cells["L6"].Value = "Time Line";
                        exWorkSheet.Cells["L6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L6"].AutoFitColumns(15);

                        using (ExcelRange col = exWorkSheet.Cells[6, 1, 6 + ExcelData.Rows.Count, 12])
                        {
                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();

                        if (string.IsNullOrEmpty(PeriodName))
                        {
                            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                            if (peridodetails != null)
                            {
                                if (peridodetails.Count > 0)
                                {
                                    foreach (var ForPeriodName in peridodetails)
                                    {
                                        string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".xlsx";
                                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                        {
                                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                            FinancialYear = Convert.ToString(FinancialYear),
                                            Period = Convert.ToString(ForPeriodName),
                                            CustomerId = customerID,
                                            VerticalID = Convert.ToInt32(VerticalID),
                                            FileName = fileName,
                                        };
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                        string directoryPath1 = "";
                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                        {
                                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                           + "/1.0");
                                            if (!Directory.Exists(directoryPath1))
                                            {
                                                DocumentManagement.CreateDirectory(directoryPath1);
                                            }
                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                            FinalDeleverableUpload.Version = "1.0";
                                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".xlsx";
                            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                            {
                                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                FinancialYear = Convert.ToString(FinancialYear),
                                Period = Convert.ToString(PeriodName),
                                CustomerId = customerID,
                                VerticalID = Convert.ToInt32(VerticalID),
                                FileName = fileName,
                            };
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                            string directoryPath1 = "";

                            bool Success1 = false;
                            if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                            {
                                directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                               + "/1.0");
                                if (!Directory.Exists(directoryPath1))
                                {
                                    DocumentManagement.CreateDirectory(directoryPath1);
                                }

                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                FinalDeleverableUpload.FileName = fileName;
                                FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                FinalDeleverableUpload.Version = "1.0";
                                FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                            }
                        }
                        #region Excel Download Code
                        //Response.ClearContent();
                        //Response.Buffer = true;
                        //fileName = "asd.xlsx";
                        //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        //Response.Charset = "";
                        //Response.ContentType = "application/vnd.ms-excel";
                        //StringWriter sw = new StringWriter();
                        //Response.BinaryWrite(fileBytes);
                        //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                #endregion

                #region PowerPoint Report


                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var details = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                   select row).ToList();


                    string BranchName = string.Empty;
                    if (details.Count > 0)
                    {
                        var ProcessList = GetProcessListOrdered(details);

                        float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                        float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;


                        String PPTName = "Draft Internal Audit Report - " + PeriodName;

                        string CustomerName = GetByID(customerID).Name;
                        string colorname = String.Empty;


                        colorname = "Chocolate";

                        CreateDymanicHeadingFirstSlide(SlideCounter, xpointsection1, 10, 700, 520, "", 32, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 80, 700, 100, CustomerName, 32, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 150, 700, 100, BranchName, 30, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 200, 700, 60, PPTName, 33, "L", colorname);
                        CreateDymanicHeading(SlideCounter, xpointsection1, 250, 700, 60, "Year- " + FinancialYear + "", 24, "L", colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        PPTName = PPTName + " " + "(Year " + FinancialYear + ")";

                        CreateDymanicPara(SlideCounter, xpointsection1, 200, 700, 205, "Ignore – to be replaced by cover letter", 22);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 100, "Table of Content", 32, "L", colorname);
                        DynamicTableOfContent(SlideCounter, xpointsection1, 120, 700, 300, "", 24, colorname, ProcessList);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", "White");
                        SetSlideColor(SlideCounter, colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", colorname);

                        //Add New Blank Slide 
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        int ProcessID = -1;

                        int Count = 0;
                        int ProcessNumber = 0;
                        int ObservationNumber = 0;

                        String ObservationNo = string.Empty;

                        details.ForEach(entry =>
                        {
                            String SectionHeading = "Section " + intToRoman(Count + 2) + "";

                            ObservationNumber++;

                            if (ProcessID == -1 || ProcessID != entry.ProcessId)
                            {
                                if (Count < ProcessList.Count)
                                {
                                    ProcessID = Convert.ToInt32(entry.ProcessId);
                                    ProcessNumber++;
                                    ObservationNumber = 1;

                                    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 50, SectionHeading, 32, "L", "White");
                                    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 50, ProcessList.ElementAt(Count), 32, "L", "White");
                                    SetSlideColor(SlideCounter, colorname);
                                    presentationdynamic.Slides.Append();
                                    SlideCounter++;
                                    Count++;
                                }
                                else
                                {

                                }
                            }

                            int checkvalueobservation = 0;
                            int checkvalueRisk = 0;
                            int checkvalueRecomentdation = 0;
                            int checkvalueManagementresponse = 0;
                            int checkvalueTimeLine = 0;

                            string[] arrayobservation = entry.Observation.Split(' ');
                            int arrobservationLength = arrayobservation.Length;
                            checkvalueobservation = (2 * arrobservationLength);

                            string[] arrayrisk = entry.Risk.Split(' ');
                            int arrLengthrisk = arrayrisk.Length;
                            checkvalueRisk = (2 * arrLengthrisk);

                            string[] arrayRecommendations = entry.Recomendation.Split(' ');
                            int arrLengthRecommendations = arrayRecommendations.Length;
                            checkvalueRecomentdation = (2 * arrLengthRecommendations);

                            string[] arrayManagementResponse = entry.ManagementResponse.Split(' ');
                            int arrLengthManagementResponsek = arrayManagementResponse.Length;
                            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

                            string[] arrayPersonresponsible = entry.ManagementResponse.Split(' ');
                            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
                            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

                            //add footer
                            presentationdynamic.SetFooterText(PPTName);

                            //set the footer visible
                            presentationdynamic.SetFooterVisible(true);

                            //set the page number visible
                            presentationdynamic.SetSlideNoVisible(true);

                            //set the date visible
                            presentationdynamic.SetDateTimeVisible(true);

                            if (checkvalueobservation <= 200 && checkvalueRisk <= 200 && checkvalueRecomentdation <= 150
                                && checkvalueManagementresponse <= 150 && checkvalueTimeLine <= 100)
                            {
                                ObservationNo = ProcessNumber + "." + ObservationNumber + " ";
                                //Observation Title by default null
                                PrintDynamicObservationSlideFix(SlideCounter, ObservationNo, entry.ObservationTitle, entry.Observation, entry.Risk, entry.Recomendation, entry.ManagementResponse, Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                            }
                            else
                            {
                                //Observation Title by default null
                                PrintDynamicObservationSlide(SlideCounter, entry.ObservationNumber, entry.ObservationTitle, entry.Observation, entry.Risk, entry.Recomendation, entry.ManagementResponse, Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                            }
                        });

                        CreateDymanicHeading(SlideCounter, xpointsection1 + 100, 200, 300, 100, "Thank You", 32, "L", "White");
                        SetSlideColor(SlideCounter, colorname);
                        presentationdynamic.Slides.Append();
                        SlideCounter++;

                        string dt = DateTime.Now.ToString("dd-MMM-yy");
                        if ((BranchName.Split(' ').First() + "-" + PPTName + "-" + dt).Length < 218)
                            PPTName = BranchName.Split(' ').FirstOrDefault() + "-" + PPTName + "-" + dt;

                        string path = Server.MapPath("~//PPTDownload//" + PPTName + ".pptx");

                        presentationdynamic.DocumentProperty["_MarkAsFinal"] = true;
                        presentationdynamic.SaveToFile(path, FileFormat.Pptx2010);


                        Byte[] fileBytes = presentationdynamic.GetBytes();
                        if (string.IsNullOrEmpty(PeriodName))
                        {
                            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                            if (peridodetails != null)
                            {
                                if (peridodetails.Count > 0)
                                {
                                    foreach (var ForPeriodName in peridodetails)
                                    {
                                        string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".pptx";
                                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                        {
                                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                            FinancialYear = Convert.ToString(FinancialYear),
                                            Period = Convert.ToString(ForPeriodName),
                                            CustomerId = customerID,
                                            VerticalID = Convert.ToInt32(VerticalID),
                                            FileName = fileName,
                                        };
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                        string directoryPath1 = "";
                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                        {
                                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                           + "/1.0");
                                            if (!Directory.Exists(directoryPath1))
                                            {
                                                DocumentManagement.CreateDirectory(directoryPath1);
                                            }
                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                            FinalDeleverableUpload.Version = "1.0";
                                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                        }
                                        //else
                                        //{
                                        //    int count = RiskCategoryManagement.Tran_FinalDeliverableUploadResultCount(FinalDeleverableUpload);
                                        //    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                        //        + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                        //        + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                        //        + "/" + (count + 1) + ".0");
                                        //    if (!Directory.Exists(directoryPath1))
                                        //    {
                                        //        DocumentManagement.CreateDirectory(directoryPath1);
                                        //    }
                                        //    Guid fileKey1 = Guid.NewGuid();
                                        //    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                        //    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                        //    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        //    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                        //    FinalDeleverableUpload.Version = (count + 1) + ".0";
                                        //    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                        //    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                        //    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        //    DocumentManagement.SaveDocFiles(Filelist1);
                                        //    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                        //}
                                    }
                                }
                            }
                        }
                        else
                        {
                            string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".pptx";
                            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                            {
                                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                FinancialYear = Convert.ToString(FinancialYear),
                                Period = Convert.ToString(PeriodName),
                                CustomerId = customerID,
                                VerticalID = Convert.ToInt32(VerticalID),
                                FileName = fileName,
                            };
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                            string directoryPath1 = "";

                            bool Success1 = false;
                            if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                            {
                                directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                               + "/1.0");
                                if (!Directory.Exists(directoryPath1))
                                {
                                    DocumentManagement.CreateDirectory(directoryPath1);
                                }

                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                FinalDeleverableUpload.FileName = fileName;
                                FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                FinalDeleverableUpload.Version = "1.0";
                                FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                            }
                            //else
                            //{
                            //    int count = RiskCategoryManagement.Tran_FinalDeliverableUploadResultCount(FinalDeleverableUpload);

                            //    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                            //   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                            //   + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                            //    + "/" + (count + 1) + ".0");
                            //    if (!Directory.Exists(directoryPath1))
                            //    {
                            //        DocumentManagement.CreateDirectory(directoryPath1);
                            //    }

                            //    Guid fileKey1 = Guid.NewGuid();
                            //    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                            //    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                            //    FinalDeleverableUpload.FileName = fileName;
                            //    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            //    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                            //    FinalDeleverableUpload.Version = (count + 1) + ".0";
                            //    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                            //    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                            //    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            //    DocumentManagement.SaveDocFiles(Filelist1);
                            //    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                            //}
                        }
                    }
                    #region PPT Download Code
                    //Response.ContentType = "application/octet-stream";
                    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + PPTName + ".pptx");
                    //Response.TransmitFile(path);
                    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    #endregion
                }
                #endregion


                List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                if (FinalDeliverableDocument != null)
                {
                    using (ZipFile AuditZip = new ZipFile())
                    {
                        if (!string.IsNullOrEmpty(PeriodName))
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(-1).Where(entry => entry.Version.Equals("1.0")).ToList();
                        }
                        else
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(-1).ToList();//.Where(entry => entry.Version.Equals("1.0")).ToList();
                        }
                        AuditZip.AddDirectoryByName(FinancialYear);
                        if (FinalDeliverableDocument.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in FinalDeliverableDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                            var zipMs = new MemoryStream();
                            AuditZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=FinalDeliverableDocuments.zip");
                            Response.BinaryWrite(Filedata);
                            Response.Flush();
                            Response.End();
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Word Report Funcation
        public void ProcessRequestWordFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId, string LocationName)
        {
            long AuditID = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            {
                AuditID = Convert.ToInt64(ViewState["AuditID"]);
            }
            //string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>

                 p.MsoHeader, li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }

                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.2in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Calibri;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;                   
                }

                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
            sbTop.Append(context);
            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append("<div style='border:none;mso-border-bottom-alt:solid windowtext.75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            //"<div style='text-align:right;'> <img height='100' width='120' src='" + hpath + "' alt ='d'></img></div>" +
            " <div style='text-align:center;'> Executive Summary Report</div></div>");

            sbTop.Append(@"<p class=MsoHeader style='border:none;mso-border-bottom-alt:solid windowtext 50pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");


            sbTop.Append("</div>");

            sbTop.Append("<div style='mso-element:footer' id=f1>" +
            "<p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>" +
            "<font size='2'> <span> Confidential</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;<span style='mso-field-code: PAGE'><span style='mso-no-proof:yes'>1</span></span> of<span style='mso-field-code: NUMPAGES '></span></font></div>");

            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            //#region Word Download Code
            //Response.AppendHeader("Content-Type", "application/msword");
            //Response.AppendHeader("Content-disposition", "attachment; filename=" + "Executive Summary" + " - " + LocationName + ".doc");
            //Response.Write(strBody);
            //#endregion
            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
            if (peridodetails != null)
            {
                if (peridodetails.Count > 0)
                {
                    foreach (var ForPeriodName in peridodetails)
                    {
                        string fileName = "Executive Summary_" + FinancialYear + "_" + ForPeriodName + ".doc";
                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                        {
                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                            FinancialYear = Convert.ToString(FinancialYear),
                            Period = Convert.ToString(ForPeriodName),
                            CustomerId = customerID,
                            VerticalID = Convert.ToInt32(VerticalID),
                            FileName = fileName,
                            AuditID = AuditID,

                        };
                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        string directoryPath1 = "";
                        bool Success1 = false;
                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                        {
                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                           + "/1.0");
                            if (!Directory.Exists(directoryPath1))
                            {
                                DocumentManagement.CreateDirectory(directoryPath1);
                            }
                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                            FinalDeleverableUpload.Version = "1.0";
                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                        }
                    }
                }
            }
        }
        public void ProcessRequestWordSecond(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId, string LocationName)
        {
            long AuditID = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            {
                AuditID = Convert.ToInt64(ViewState["AuditID"]);
            }
            //string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");

            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                 p.MsoHeader, li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.2in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Calibri;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;                   
                }

                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
            sbTop.Append(context);

            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append("<div style='border:none;mso-border-bottom-alt:solid windowtext.75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            //"<div style='text-align:right;'> <img height='100' width='120' src='" + hpath + "' alt ='d'></img></div>" +
            " <div style='text-align:center;'> Internal Audit Report</div></div>");

            sbTop.Append(@"<p class=MsoHeader style='border:none;mso-border-bottom-alt:solid windowtext 50pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");


            sbTop.Append("</div>");

            sbTop.Append("<div style='mso-element:footer' id=f1>" +
            "<p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>" +
            "<font size='2'> <span> Confidential</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;<span style='mso-field-code: PAGE'><span style='mso-no-proof:yes'>1</span></span> of<span style='mso-field-code: NUMPAGES '></span></font></div>");

            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            //#region Word Download Code
            //Response.AppendHeader("Content-Type", "application/msword");
            //Response.AppendHeader("Content-disposition", "attachment; filename=" + "Internal Audit Report" + ".doc");
            //Response.Write(strBody);
            //#endregion
            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
            if (peridodetails != null)
            {
                if (peridodetails.Count > 0)
                {
                    foreach (var ForPeriodName in peridodetails)
                    {
                        string fileName = "Internal Audit Report_" + FinancialYear + "_" + ForPeriodName + ".doc";
                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                        {
                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                            FinancialYear = Convert.ToString(FinancialYear),
                            Period = Convert.ToString(ForPeriodName),
                            CustomerId = customerID,
                            VerticalID = Convert.ToInt32(VerticalID),
                            FileName = fileName,
                            AuditID = AuditID,
                        };
                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        string directoryPath1 = "";
                        bool Success1 = false;
                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                        {
                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                           + "/1.0");
                            if (!Directory.Exists(directoryPath1))
                            {
                                DocumentManagement.CreateDirectory(directoryPath1);
                            }
                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                            FinalDeleverableUpload.Version = "1.0";
                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                        }
                    }
                }
            }
        }
        public void ProcessRequest_AsPerClientFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Arial;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Consolidated Location Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,

                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Consolidated Location Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest_AsPerClientSECOND(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Arial;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Audit Committee Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Audit Committee Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest(string context, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string details = "Audit Report for the (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string NameVertical = VerticalName.VerticalName;
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + companyname + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }
        public string GetUserName(int userid)
        {
            string processnonprocess = "";
            if (userid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(userid);
                    mst_User mstuser = (from row in entities.mst_User
                                        where row.ID == userid && row.IsDeleted == false && row.IsActive == true
                                        select row).FirstOrDefault();

                    processnonprocess = mstuser.FirstName + " " + mstuser.LastName;
                }
            }
            return processnonprocess;
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure> TempAuditClosureResult(int branchid, string Financialyear, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.CustomerbranchId == branchid && row.VerticalID == VerticalID && row.FinancialYear == Financialyear
                             && row.ACStatus == 1
                             select row).ToList();
                return query.ToList();
            }
        }
        #endregion

        #region Power Point Report Function
        public int SlideCounter = 0;

        Presentation presentationdynamic = new Presentation();
        public static mst_Customer GetByID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_Customer
                                where row.ID == customerID && row.IsDeleted == false
                                select row).SingleOrDefault();

                return customer;
            }
        }
        public static List<String> GetProcessListOrdered(List<Business.DataRisk.AuditClosure> ACC)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in ACC
                                                join MP in entities.Mst_Process
                                                on row.ProcessId equals MP.Id
                                                orderby row.ProcessOrder
                                                select MP.Name).Distinct().ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public static List<String> GetProcessListOrdered(List<Business.DataRisk.SPGETObservationDetails_Result> ACC)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in ACC
                                                join row1 in entities.AuditClosures
                                                on row.ProcessId equals row1.ProcessId
                                                join MP in entities.Mst_Process
                                                on row.ProcessId equals MP.Id
                                                orderby row1.ProcessOrder
                                                select MP.Name).Distinct().ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public static List<Business.DataRisk.AuditClosure> GetAuditWiseObservationDetailList(List<long> BranchList, int verticalID, String FinYear, String Period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditWiseObservationList = (from row in entities.AuditClosures
                                                where BranchList.Contains(row.CustomerbranchId)
                                                && row.VerticalID == verticalID
                                                && row.FinancialYear == FinYear
                                                //&& row.ForPeriod == Period
                                                && row.ACStatus == 1
                                                select row).ToList();

                AuditWiseObservationList = AuditWiseObservationList
                .OrderBy(entry => entry.ProcessOrder)
                .ThenBy(entry => entry.ObservationNumber).ToList();

                return AuditWiseObservationList.ToList();
            }
        }
        public void SetSlideColor(int slidenumber, string colorName)
        {
            presentationdynamic.Slides[slidenumber].SlideBackground.Type = BackgroundType.Custom;
            presentationdynamic.Slides[slidenumber].SlideBackground.Fill.FillType = FillFormatType.Solid;

            switch (colorName)
            {
                #region A
                case "AliceBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    presentationdynamic.Slides[slidenumber].SlideBackground.Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
        }
        public void CreateDymanicPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            if (Contain.Contains('\n'))
            {
                string ss = Contain.Replace("\r\n", "");
                para_Secc.Text = ss;
            }
            else
            {
                para_Secc.Text = Contain;
            }
            para_Secc.Alignment = TextAlignmentType.Justify;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Arial (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.MarginTop = 0;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }
        public void CreateDymanicHeadingFirstSlide(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);

            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Gray;
                    break;
                case "Green":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Peru;
                    break;
                case "Pink":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Pink;
                    break;
                case "Plum":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tan;
                    break;
                case "Teal":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    shape_Secc.ShapeStyle.LineColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            //switch (colorname)
            //{
            //    case "Navy":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Navy;
            //        break;
            //    case "OrangeRed":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.OrangeRed;
            //        break;
            //    case "RoyalBlue":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.RoyalBlue;
            //        break;
            //    case "Red":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Red;
            //        break;
            //    case "Green":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Green;
            //        break;
            //    case "Sienna":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Sienna;
            //        break;
            //    case "Purple":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Purple;
            //        break;
            //    case "Maroon":
            //        shape_Secc.ShapeStyle.LineColor.Color = Color.Maroon;
            //        break;
            //    default:
            //        break;
            //}

            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();

            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }


        public void PrintDynamicObservationSlideFix(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, int? ObsRating, string colorname)
        {
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;

            int checkvalueobservation = 0;
            int checkvalueRisk = 0;
            int checkvalueRecomentdation = 0;
            int checkvalueManagementresponse = 0;
            int checkvalueTimeLine = 0;

            string[] arrayobservation = paraobservation.Split(' ');
            int arrobservationLength = arrayobservation.Length;
            checkvalueobservation = (2 * arrobservationLength);

            string[] arrayrisk = pararisk.Split(' ');
            int arrLengthrisk = arrayrisk.Length;
            checkvalueRisk = (2 * arrLengthrisk);

            string[] arrayRecommendations = pararRecommendations.Split(' ');
            int arrLengthRecommendations = arrayRecommendations.Length;
            checkvalueRecomentdation = (2 * arrLengthRecommendations);

            string[] arrayManagementResponse = paraManagementResponse.Split(' ');
            int arrLengthManagementResponsek = arrayManagementResponse.Length;
            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

            string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

            String ObservationRating = String.Empty;
            String ObsRatingColor = String.Empty;


            if (ObsRating != null)
            {
                if (ObsRating == 1)
                {
                    ObservationRating = "High";
                    ObsRatingColor = "Red";
                }
                else if (ObsRating == 2)
                {
                    ObservationRating = "Medium";
                    ObsRatingColor = "Chocolate";
                }
                else if (ObsRating == 3)
                {
                    ObservationRating = "Low";
                    ObsRatingColor = "Green";
                }
            }

            CreateDymanicHeading(slidecounter, xpointsection1, 30, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
            CreateDymanicFilledHeading(slidecounter, 615, 5, 100, 20, ObservationRating, 15, "C", ObsRatingColor);
            CreateDymanicTitle(slidecounter, xpointsection1, 70, 350, 10, "Observations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 100, 350, 205, paraobservation, 13);

            CreateDymanicTitle(slidecounter, xpointsection1, 250, 350, 10, "Risk:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection1, 280, 350, 205, pararisk, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 100, 350, 150, pararRecommendations, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 200, 350, 10, "Management Response:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 230, 350, 150, paraManagementResponse, 13);

            CreateDymanicTitle(slidecounter, xpointsection2, 350, 350, 10, "Responsibility and Timeline:", 13, colorname);
            CreateDymanicPara(slidecounter, xpointsection2, 380, 350, 100, paraPersonresponsible, 13);
        }

        public void PrintDynamicObservationSlide(int slidecounter, string observationnumber, string observationtitle, string paraobservation, string pararisk, string pararRecommendations, string paraManagementResponse, string paraPersonresponsible, string colorname)
        {
            try
            {
                string printstartfrom = "";
                int dynamicwordlength = 0;
                int dynamicheight = 0;
                int totalheight = 400;
                String Firstpara = String.Empty;
                String Nextpara = String.Empty;
                string section = "F";
                int yposition = 0;
                int newvalue = 0;
                string sectionvalue = "A";
                float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
                float ypoint = 40;
                int thirdval = 0;

                //int checkvalueobservation = 0;
                //int checkvalueRisk = 0;
                //int checkvalueRecomentdation = 0;
                //int checkvalueManagementresponse = 0;
                //int checkvalueTimeLine = 0;

                string[] arrayobservation = paraobservation.Split(' ');
                int arrobservationLength = arrayobservation.Length;

                string[] arrayrisk = pararisk.Split(' ');
                int arrLengthrisk = arrayrisk.Length;

                string[] arrayRecommendations = pararRecommendations.Split(' ');
                int arrLengthRecommendations = arrayRecommendations.Length;

                string[] arrayManagementResponse = paraManagementResponse.Split(' ');
                int arrLengthManagementResponsek = arrayManagementResponse.Length;

                string[] arrayPersonresponsible = paraPersonresponsible.Split(' ');
                int arrLengthPersonresponsible = arrayPersonresponsible.Length;

                CreateDymanicHeading(slidecounter, xpointsection1, ypoint, 700, 20, observationnumber + " " + observationtitle, 21, "L", colorname);
                CreateDymanicTitle(slidecounter, xpointsection1, ypoint + 30, 350, 10, "Observations:", 13, colorname);
                CreateDymanicLoopPara(slidecounter, xpointsection1, ypoint + 70, 350, totalheight, paraobservation, 13, arrobservationLength, arrayobservation, section, colorname, out yposition, out section);

                newvalue = yposition;
                sectionvalue = section;
                if (sectionvalue == "F")//observation First Section Value
                {
                    dynamicheight = (2 * arrLengthrisk);
                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                    CreateDymanicTitle(slidecounter, xpointsection1, (ypoint + 70 + newvalue), 350, 10, "Risk:", 13, colorname);
                    if (dynamicheight > totalheight - newvalue)
                    {
                        printstartfrom = "MID";
                        CreateDymanicLoopParaDynamicArrayLength(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20,
                        pararisk, 13, arrLengthrisk, arrayrisk, dynamicwordlength, colorname, out yposition, out section);
                        thirdval += yposition;
                    }
                    else
                    {
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicLoopPara(slidecounter, xpointsection1, (ypoint + 70 + newvalue + 20), 350, totalheight - newvalue + 20, pararisk, 13,
                            arrLengthrisk, arrayrisk, section, colorname, out yposition, out section);
                        printstartfrom = "START";
                    }//risk Print Complete                       
                    newvalue = yposition;
                    sectionvalue = section;
                    if (sectionvalue == "F")//Risk  First Section Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval = newvalue;
                        }
                        else
                        {
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += newvalue;
                        }
                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                            newvalue = yposition;
                            sectionvalue = section;
                            if (sectionvalue == "F")
                            {
                                thirdval += newvalue;
                                dynamicheight = (2 * arrLengthPersonresponsible);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(slidecounter, xpointsection2, (ypoint + 110 + thirdval), 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                CreateDymanicLoopPara(slidecounter, xpointsection2, (ypoint + 120 + thirdval + 20), 350, totalheight - thirdval + 20, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                            }
                            else if (sectionvalue == "S")//Recomendation Second Section Value
                            {
                                if (newvalue > 300)
                                {
                                    presentationdynamic.Slides.Append();
                                    SlideCounter++;
                                    dynamicheight = (2 * arrLengthRecommendations);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);

                                    newvalue = yposition;
                                    sectionvalue = section;
                                    if (sectionvalue == "F")
                                    {
                                        dynamicheight = (2 * arrLengthPersonresponsible);
                                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Responsibility And TimeLine:", 13, colorname);
                                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraPersonresponsible, 13, arrLengthPersonresponsible, arrayPersonresponsible, sectionvalue, colorname, out yposition, out section);
                                    }
                                }
                            }
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            if (newvalue > 300)
                            {
                                presentationdynamic.Slides.Append();
                                SlideCounter++;
                                dynamicheight = (2 * arrLengthRecommendations);
                                dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Recommendations:", 13, colorname);
                                CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);

                                newvalue = yposition;
                                sectionvalue = section;
                                if (sectionvalue == "F")
                                {
                                    dynamicheight = (2 * arrLengthManagementResponsek);
                                    dynamicwordlength = (totalheight - newvalue - 20) / 2;
                                    CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Management Response:", 13, colorname);
                                    CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, dynamicheight, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                                }
                            }
                        }
                    }
                    else if (sectionvalue == "S")//Risk Section Second Value
                    {
                        dynamicheight = (2 * arrLengthRecommendations);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        if (printstartfrom == "START")
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                        else
                        {
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 70 + newvalue), 350, 10, "Recommendations:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 70 + newvalue + 20), 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                            thirdval += yposition;
                        }
                        newvalue = yposition;
                        sectionvalue = section;

                        if (sectionvalue == "F")//Recomendation  First Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, (ypoint + 90 + newvalue), 350, 10, "Management Response:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, (ypoint + 90 + newvalue + 20), 350, totalheight - newvalue + 20, paraManagementResponse, 13, arrLengthManagementResponsek, arrayManagementResponse, sectionvalue, colorname, out yposition, out section);
                        }
                        else if (sectionvalue == "S")//Recomendation Second Section Value
                        {
                            dynamicheight = (2 * arrLengthManagementResponsek);
                            dynamicwordlength = (totalheight - thirdval - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, thirdval + 70, 350, 10, "Management Response:", 13, colorname);
                        }
                    }
                }
                else if (sectionvalue == "S") //observation Second Section Value
                {
                    if (newvalue > 300)
                    {
                        presentationdynamic.Slides.Append();
                        SlideCounter++;
                        dynamicheight = (2 * arrLengthrisk);
                        dynamicwordlength = (totalheight - newvalue - 20) / 2;
                        CreateDymanicTitle(SlideCounter, xpointsection1, 70, 350, 10, "Risk:", 13, colorname);
                        CreateDymanicLoopPara(SlideCounter, xpointsection1, ypoint + 70, 350, totalheight, pararisk, 13, arrLengthrisk, arrayrisk, sectionvalue, colorname, out yposition, out section);

                        newvalue = yposition;
                        sectionvalue = section;
                        if (sectionvalue == "F")
                        {
                            dynamicheight = (2 * arrLengthRecommendations);
                            dynamicwordlength = (totalheight - newvalue - 20) / 2;
                            CreateDymanicTitle(SlideCounter, xpointsection2, 70, 350, 10, "Recomendtions:", 13, colorname);
                            CreateDymanicLoopPara(SlideCounter, xpointsection2, ypoint + 70, 350, dynamicheight, pararRecommendations, 13, arrLengthRecommendations, arrayRecommendations, sectionvalue, colorname, out yposition, out section);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void CreateDymanicLoopParaDynamicArrayLength(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, int dymnamicchar, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > dymnamicchar)
            {
                if (arrLength > dymnamicchar)
                {
                    int cnt = (arrLength / dymnamicchar) + 1;

                    for (int j = dymnamicchar; j > 0; j--)
                    {
                        if (array[j].Contains("."))
                        {
                            indextoSplit = j;
                            j = 0;
                            break;
                        }
                    }
                    for (int j = 0; j <= indextoSplit; j++)
                    {
                        if (!String.IsNullOrEmpty(Firstpara))
                            Firstpara = Firstpara + " " + array[j];
                        else
                            Firstpara = Firstpara + array[j];
                        Lntcnt += 1;
                    }
                    CreateDymanicPara(0, x, y, width, height, Firstpara, fontSize);
                    Firstpara = "";
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    arrLength = 0;
                    array = Nextpara.Split(' ');
                    arrLength = array.Length;
                    Lntcnt = 0;
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    section = "S";
                    yposition = (2 * arrLength);
                    CreateDymanicLoopPara(0, xpointsection2, 70, 350, dynamicheight, Contain, fontSize, arrLength, array, section, colorname, out yposition, out section);

                }
                else
                {

                    yposition = (2 * arrLength);
                    CreateDymanicPara(0, xpointsection2, 70, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";

                }
            }
            else
            {
                int dynamicheight = 0;
                dynamicheight = (2 * arrLength);
                CreateDymanicPara(0, x, y, 350, dynamicheight, Contain, fontSize);
                yposition = dynamicheight;
                section = "S";

            }
        }
        public void CreateDymanicLoopPara(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, int arrLength, string[] array, string printsection, string colorname, out int yposition, out string section)
        {
            section = "F";
            yposition = 0;
            String Firstpara = String.Empty;
            String Nextpara = String.Empty;
            int indextoSplit = 0;
            int Lntcnt = 0;
            float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
            float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;
            if (arrLength > 200)
            {
                do
                {
                    if (arrLength > 200)
                    {
                        int cnt = (arrLength / 200) + 1;

                        for (int j = 200; j > 0; j--)
                        {
                            if (array[j].Contains("."))
                            {
                                indextoSplit = j;
                                j = 0;
                                break;
                            }
                        }
                        for (int j = 0; j <= indextoSplit; j++)
                        {
                            if (!String.IsNullOrEmpty(Firstpara))
                                Firstpara = Firstpara + " " + array[j];
                            else
                                Firstpara = Firstpara + array[j];
                            Lntcnt += 1;
                        }
                        CreateDymanicPara(slidenumber, x, y, width, height, Firstpara, fontSize);
                        Firstpara = "";
                        Nextpara = "";
                        for (int k = Lntcnt; k < arrLength; k++)
                        {
                            if (!String.IsNullOrEmpty(Nextpara))
                                Nextpara = Nextpara + " " + array[k];
                            else
                                Nextpara = Nextpara + array[k];
                        }
                        arrLength = 0;
                        array = Nextpara.Split(' ');
                        arrLength = array.Length;
                        Lntcnt = 0;
                    }
                    else
                    {
                        if (printsection == "S")
                        {
                            presentationdynamic.Slides.Append();
                            SlideCounter++;
                            yposition = (2 * arrLength);
                            CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 20, "1.1  Explore possibility of payment of Ocean Freight bills in USD", 21, "L", colorname);
                            CreateDymanicPara(SlideCounter, xpointsection1, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "F";
                        }
                        else
                        {
                            yposition = (2 * arrLength);
                            CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                            Nextpara = "";
                            arrLength = 0;
                            Lntcnt = 0;
                            section = "S";
                        }

                    }
                } while (arrLength > 0);
            }
            else
            {
                if (printsection == "S")
                {
                    Nextpara = "";
                    for (int k = Lntcnt; k < arrLength; k++)
                    {
                        if (!String.IsNullOrEmpty(Nextpara))
                            Nextpara = Nextpara + " " + array[k];
                        else
                            Nextpara = Nextpara + array[k];
                    }
                    yposition = (2 * arrLength);
                    CreateDymanicPara(slidenumber, xpointsection2, y, width, yposition, Nextpara, fontSize);
                    Nextpara = "";
                    arrLength = 0;
                    Lntcnt = 0;
                    section = "S";
                }
                else
                {
                    int dynamicheight = 0;
                    dynamicheight = (2 * arrLength);
                    CreateDymanicPara(slidenumber, x, y, 350, dynamicheight, Contain, fontSize);
                    yposition = dynamicheight;
                    section = "F";
                }
            }
        }
        public void CreateDymanicTitle(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;
            para_Secc.Alignment = TextAlignmentType.Left;
            para_Secc.TextRanges[0].LatinFont = new TextFont("Arial (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;

                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            //para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }
        public void DynamicTableOfContent(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string colorname, List<string> ProcessList)
        {
            RectangleF rec_bullet = new RectangleF(x, y, 700, height);
            IAutoShape shape_bullet = presentationdynamic.Slides[SlideCounter].Shapes.AppendShape(ShapeType.Rectangle, rec_bullet);
            shape_bullet.ShapeStyle.LineColor.Color = Color.White;
            shape_bullet.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            shape_bullet.TextFrame.AnchoringType = TextAnchorType.Top;

            string[] str = new string[] { "Executive Summary", "Detailed Report" };

            foreach (string txt in str)
            {
                TextParagraph textParagraph = new TextParagraph();
                textParagraph.Text = txt;
                textParagraph.Alignment = TextAlignmentType.Left;
                textParagraph.Indent = 35;

                //set the Bullets
                textParagraph.BulletType = TextBulletType.Symbol;
                textParagraph.BulletStyle = NumberedBulletStyle.BulletRomanLCPeriod;
                textParagraph.LineSpacing = 150;
                shape_bullet.TextFrame.Paragraphs.Append(textParagraph);

                if (txt == "Detailed Report")
                {
                    string[] str1 = ProcessList.ToArray();

                    foreach (string txt1 in str1)
                    {
                        TextParagraph textinnerParagraph = new TextParagraph();
                        textinnerParagraph.Text = txt1;
                        textinnerParagraph.Alignment = TextAlignmentType.Left;
                        textinnerParagraph.Indent = 24;
                        textinnerParagraph.LeftMargin = 50;
                        textinnerParagraph.LineSpacing = 150;
                        //set the Bullets
                        textinnerParagraph.BulletType = TextBulletType.Numbered;
                        textinnerParagraph.BulletStyle = NumberedBulletStyle.BulletArabicPeriod;
                        shape_bullet.TextFrame.Paragraphs.Append(textinnerParagraph);
                    }
                }
            }

            ////set the font and fill style
            foreach (TextParagraph paragraph in shape_bullet.TextFrame.Paragraphs)
            {
                paragraph.TextRanges[0].LatinFont = new TextFont("Arial (Body)");
                paragraph.TextRanges[0].FontHeight = 24;
                paragraph.TextRanges[0].Fill.FillType = FillFormatType.Solid;
                switch (colorname)
                {
                    #region A
                    case "AliceBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                        break;
                    case "AntiqueWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                        break;
                    case "Aqua":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                        break;
                    case "Aquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                        break;
                    case "Azure":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                        break;
                    #endregion

                    #region B
                    case "Black":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                        break;
                    case "BlanchedAlmond":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                        break;
                    case "Blue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                        break;
                    case "BlueViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                        break;
                    case "Brown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                        break;
                    case "BurlyWood":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                        break;
                    #endregion

                    #region C
                    case "CadetBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                        break;
                    case "Chartreuse":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                        break;
                    case "Chocolate":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                        break;
                    case "Coral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                        break;
                    case "CornflowerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                        break;
                    case "Crimson":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                        break;
                    case "Cornsilk":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                        break;
                    case "Cyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                        break;
                    #endregion

                    #region D
                    case "DarkBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                        break;
                    case "DarkCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                        break;
                    case "DarkGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                        break;
                    case "DarkGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                        break;
                    case "DarkGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                        break;
                    case "DarkKhaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                        break;
                    case "DarkMagenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                        break;
                    case "DarkOliveGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                        break;
                    case "DarkOrange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                        break;
                    case "DarkOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                        break;
                    case "DarkRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                        break;
                    case "DarkSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                        break;
                    case "DarkSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                        break;
                    case "DarkSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                        break;
                    case "DarkSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                        break;
                    case "DarkTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                        break;
                    case "DarkViolet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                        break;
                    case "DeepPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                        break;
                    case "DeepSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                        break;
                    case "DimGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                        break;
                    case "DodgerBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                        break;
                    #endregion

                    #region F
                    case "Firebrick":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                        break;
                    case "FloralWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                        break;
                    case "Fuchsia":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                        break;
                    case "ForestGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                        break;
                    #endregion

                    #region G
                    case "Gainsboro":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                        break;
                    case "GhostWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                        break;
                    case "Gold":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                        break;
                    case "Goldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                        break;
                    case "Gray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                        break;
                    case "Green":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                        break;
                    case "GreenYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                        break;
                    #endregion

                    #region H
                    case "Honeydew":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                        break;
                    case "HotPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                        break;
                    #endregion

                    #region I
                    case "IndianRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                        break;
                    case "Indigo":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                        break;
                    case "Ivory":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                        break;
                    #endregion

                    #region K
                    case "Khaki":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                        break;
                    #endregion

                    #region L
                    case "Lavender":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                        break;
                    case "LavenderBlush":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                        break;
                    case "LawnGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                        break;
                    case "LemonChiffon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                        break;
                    case "LightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                        break;
                    case "LightCoral":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                        break;
                    case "LightCyan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                        break;
                    case "LightGoldenrodYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                        break;
                    case "LightGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                        break;
                    case "LightGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                        break;
                    case "LightPink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                        break;
                    case "LightSalmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                        break;
                    case "LightSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                        break;
                    case "LightSkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                        break;
                    case "LightSlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                        break;
                    case "LightSteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                        break;
                    case "LightYellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                        break;
                    case "Lime":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                        break;
                    case "LimeGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                        break;
                    case "Linen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                        break;
                    #endregion

                    #region M
                    case "Magenta":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                        break;
                    case "MediumAquamarine":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                        break;
                    case "Maroon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                        break;
                    case "MediumBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                        break;
                    case "MediumOrchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                        break;
                    case "MediumPurple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                        break;
                    case "MediumSeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                        break;
                    case "MediumSlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                        break;
                    case "MediumSpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                        break;
                    case "MediumTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                        break;
                    case "MediumVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                        break;
                    case "MidnightBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                        break;
                    case "MintCream":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                        break;
                    case "MistyRose":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                        break;
                    case "Moccasin":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                        break;
                    #endregion

                    #region N
                    case "Navy":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                        break;
                    case "NavajoWhite":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                        break;
                    #endregion

                    #region O
                    case "OldLace":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                        break;
                    case "Olive":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                        break;
                    case "OliveDrab":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                        break;
                    case "Orange":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                        break;
                    case "OrangeRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                        break;
                    case "Orchid":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                        break;

                    #endregion

                    #region P
                    case "PaleGoldenrod":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                        break;
                    case "PaleGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                        break;
                    case "PaleTurquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                        break;
                    case "PaleVioletRed":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                        break;
                    case "PapayaWhip":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                        break;
                    case "PeachPuff":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                        break;
                    case "Peru":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                        break;
                    case "Pink":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                        break;
                    case "Plum":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                        break;
                    case "PowderBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                        break;
                    case "Purple":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                        break;
                    #endregion

                    #region R
                    case "RoyalBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                        break;
                    case "Red":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                        break;
                    case "RosyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                        break;
                    #endregion

                    #region S
                    case "SaddleBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                        break;
                    case "Salmon":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                        break;
                    case "SandyBrown":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                        break;
                    case "SeaGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                        break;
                    case "SeaShell":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                        break;
                    case "Sienna":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                        break;
                    case "Silver":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                        break;
                    case "SkyBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                        break;
                    case "SlateBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                        break;
                    case "SlateGray":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                        break;
                    case "Snow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                        break;
                    case "SpringGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                        break;
                    case "SteelBlue":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                        break;
                    #endregion

                    #region T
                    case "Tan":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                        break;
                    case "Teal":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                        break;
                    case "Thistle":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                        break;
                    case "Tomato":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                        break;
                    case "Turquoise":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                        break;
                    #endregion

                    #region V
                    case "Violet":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                        break;

                    #endregion

                    #region W
                    case "Wheat":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                        break;
                    case "WhiteSmoke":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                        break;

                    #endregion

                    #region Y
                    case "YellowGreen":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                        break;
                    case "Yellow":
                        paragraph.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                        break;
                    #endregion
                    default:
                        break;
                }
            }
        }

        public void CreateDymanicFilledHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;

            switch (colorname)
            {
                case "Red":
                    shape_Secc.Fill.SolidColor.Color = Color.Red;
                    break;
                case "Chocolate":
                    shape_Secc.Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Green":
                    shape_Secc.Fill.SolidColor.Color = Color.Green;
                    break;
                default:
                    break;
            }

            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Arial (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;

            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }

        public void CreateDymanicHeading(int slidenumber, float x, float y, int width, int height, string Contain, int fontSize, string flag, string colorname)
        {
            RectangleF rec_Secc = new RectangleF(x, y, width, height);
            IAutoShape shape_Secc = presentationdynamic.Slides[slidenumber].Shapes.AppendShape(ShapeType.Rectangle, rec_Secc);
            shape_Secc.ShapeStyle.LineColor.Color = Color.Transparent;
            shape_Secc.Fill.FillType = Spire.Presentation.Drawing.FillFormatType.None;
            TextParagraph para_Secc = new TextParagraph();
            para_Secc.Text = Contain;

            switch (flag)
            {
                case "C":
                    para_Secc.Alignment = TextAlignmentType.Center;
                    break;
                case "L":
                    para_Secc.Alignment = TextAlignmentType.Left;
                    break;
                case "R":
                    para_Secc.Alignment = TextAlignmentType.Right;
                    break;
                case "J":
                    para_Secc.Alignment = TextAlignmentType.Justify;
                    break;
                case "N":
                    para_Secc.Alignment = TextAlignmentType.None;
                    break;
                default:
                    break;
            }
            para_Secc.TextRanges[0].LatinFont = new TextFont("Arial (Body)");
            para_Secc.TextRanges[0].FontHeight = fontSize;
            para_Secc.TextRanges[0].Fill.FillType = Spire.Presentation.Drawing.FillFormatType.Solid;
            switch (colorname)
            {
                #region A
                case "AliceBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AliceBlue;
                    break;
                case "AntiqueWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.AntiqueWhite;
                    break;
                case "Aqua":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aqua;
                    break;
                case "Aquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Aquamarine;
                    break;
                case "Azure":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Azure;
                    break;
                #endregion

                #region B
                case "Black":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Black;
                    break;
                case "BlanchedAlmond":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlanchedAlmond;
                    break;
                case "Blue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Blue;
                    break;
                case "BlueViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BlueViolet;
                    break;
                case "Brown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Brown;
                    break;
                case "BurlyWood":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.BurlyWood;
                    break;

                #endregion

                #region C
                case "CadetBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CadetBlue;
                    break;
                case "Chartreuse":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chartreuse;
                    break;
                case "Chocolate":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Chocolate;
                    break;
                case "Coral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Coral;
                    break;
                case "CornflowerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.CornflowerBlue;
                    break;
                case "Crimson":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Crimson;
                    break;
                case "Cornsilk":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cornsilk;
                    break;
                case "Cyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Cyan;
                    break;
                #endregion

                #region D
                case "DarkBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkBlue;
                    break;
                case "DarkCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkCyan;
                    break;
                case "DarkGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGoldenrod;
                    break;
                case "DarkGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGray;
                    break;
                case "DarkGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkGreen;
                    break;
                case "DarkKhaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkKhaki;
                    break;
                case "DarkMagenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkMagenta;
                    break;
                case "DarkOliveGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOliveGreen;
                    break;

                case "DarkOrange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrange;
                    break;
                case "DarkOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkOrchid;
                    break;
                case "DarkRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkRed;
                    break;
                case "DarkSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSalmon;
                    break;
                case "DarkSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSeaGreen;
                    break;
                case "DarkSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateBlue;
                    break;

                case "DarkSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkSlateGray;
                    break;
                case "DarkTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkTurquoise;
                    break;
                case "DarkViolet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DarkViolet;
                    break;
                case "DeepPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepPink;
                    break;
                case "DeepSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DeepSkyBlue;
                    break;
                case "DimGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DimGray;
                    break;
                case "DodgerBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.DodgerBlue;
                    break;
                #endregion

                #region F
                case "Firebrick":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Firebrick;
                    break;
                case "FloralWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.FloralWhite;
                    break;
                case "Fuchsia":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Fuchsia;
                    break;
                case "ForestGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.ForestGreen;
                    break;
                #endregion

                #region G
                case "Gainsboro":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gainsboro;
                    break;
                case "GhostWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GhostWhite;
                    break;
                case "Gold":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gold;
                    break;
                case "Goldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Goldenrod;
                    break;
                case "Gray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Gray;
                    break;
                case "Green":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Green;
                    break;
                case "GreenYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.GreenYellow;
                    break;
                #endregion

                #region H
                case "Honeydew":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Honeydew;
                    break;
                case "HotPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.HotPink;
                    break;
                #endregion

                #region I
                case "IndianRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.IndianRed;
                    break;
                case "Indigo":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Indigo;
                    break;
                case "Ivory":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Ivory;
                    break;
                #endregion

                #region K
                case "Khaki":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Khaki;
                    break;
                #endregion

                #region L
                case "Lavender":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lavender;
                    break;
                case "LavenderBlush":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LavenderBlush;
                    break;
                case "LawnGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LawnGreen;
                    break;
                case "LemonChiffon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LemonChiffon;
                    break;
                case "LightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightBlue;
                    break;
                case "LightCoral":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCoral;
                    break;
                case "LightCyan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightCyan;
                    break;
                case "LightGoldenrodYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGoldenrodYellow;
                    break;
                case "LightGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGray;
                    break;
                case "LightGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightGreen;
                    break;
                case "LightPink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightPink;
                    break;
                case "LightSalmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSalmon;
                    break;
                case "LightSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSeaGreen;
                    break;

                case "LightSkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSkyBlue;
                    break;
                case "LightSlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSlateGray;
                    break;
                case "LightSteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightSteelBlue;
                    break;
                case "LightYellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LightYellow;
                    break;

                case "Lime":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Lime;
                    break;
                case "LimeGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.LimeGreen;
                    break;
                case "Linen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Linen;
                    break;
                #endregion

                #region M
                case "Magenta":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Magenta;
                    break;
                case "MediumAquamarine":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumAquamarine;
                    break;
                case "Maroon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Maroon;
                    break;

                case "MediumBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumBlue;
                    break;
                case "MediumOrchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumOrchid;
                    break;
                case "MediumPurple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumPurple;
                    break;
                case "MediumSeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSeaGreen;
                    break;

                case "MediumSlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSlateBlue;
                    break;
                case "MediumSpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumSpringGreen;
                    break;
                case "MediumTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumTurquoise;
                    break;
                case "MediumVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MediumVioletRed;
                    break;

                case "MidnightBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MidnightBlue;
                    break;
                case "MintCream":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MintCream;
                    break;
                case "MistyRose":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.MistyRose;
                    break;
                case "Moccasin":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Moccasin;
                    break;

                #endregion

                #region N
                case "Navy":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Navy;
                    break;
                case "NavajoWhite":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.NavajoWhite;
                    break;
                #endregion

                #region O
                case "OldLace":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OldLace;
                    break;
                case "Olive":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Olive;
                    break;
                case "OliveDrab":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OliveDrab;
                    break;
                case "Orange":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orange;
                    break;
                case "OrangeRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.OrangeRed;
                    break;
                case "Orchid":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Orchid;
                    break;

                #endregion

                #region P
                case "PaleGoldenrod":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGoldenrod;
                    break;
                case "PaleGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleGreen;
                    break;
                case "PaleTurquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleTurquoise;
                    break;
                case "PaleVioletRed":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PaleVioletRed;
                    break;
                case "PapayaWhip":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PapayaWhip;
                    break;
                case "PeachPuff":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PeachPuff;
                    break;
                case "Peru":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Peru;
                    break;
                case "Pink":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Pink;
                    break;
                case "Plum":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Plum;
                    break;
                case "PowderBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.PowderBlue;
                    break;
                case "Purple":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Purple;
                    break;
                #endregion

                #region R
                case "RoyalBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RoyalBlue;
                    break;
                case "Red":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Red;
                    break;
                case "RosyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.RosyBrown;
                    break;
                #endregion

                #region S
                case "SaddleBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SaddleBrown;
                    break;
                case "Salmon":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Salmon;
                    break;
                case "SandyBrown":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SandyBrown;
                    break;
                case "SeaGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaGreen;
                    break;
                case "SeaShell":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SeaShell;
                    break;
                case "Sienna":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Sienna;
                    break;
                case "Silver":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Silver;
                    break;
                case "SkyBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SkyBlue;
                    break;
                case "SlateBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateBlue;
                    break;
                case "SlateGray":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SlateGray;
                    break;
                case "Snow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Snow;
                    break;
                case "SpringGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SpringGreen;
                    break;
                case "SteelBlue":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.SteelBlue;
                    break;
                #endregion

                #region T
                case "Tan":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tan;
                    break;
                case "Teal":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Teal;
                    break;
                case "Thistle":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Thistle;
                    break;
                case "Tomato":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Tomato;
                    break;
                case "Turquoise":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Turquoise;
                    break;
                #endregion

                #region V
                case "Violet":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Violet;
                    break;

                #endregion

                #region W
                case "Wheat":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Wheat;
                    break;
                case "WhiteSmoke":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.WhiteSmoke;
                    break;
                case "White":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.White;
                    break;
                #endregion

                #region Y
                case "YellowGreen":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.YellowGreen;
                    break;
                case "Yellow":
                    para_Secc.TextRanges[0].Fill.SolidColor.Color = Color.Yellow;
                    break;
                #endregion
                default:
                    break;
            }
            shape_Secc.TextFrame.AnchoringType = TextAnchorType.Top;
            shape_Secc.TextFrame.Paragraphs.Append(para_Secc);
        }


        public static string intToRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }
        #endregion

        protected void lbtObservationList_Click(object sender, EventArgs e)
        {
            string status = string.Empty;
            int role = -1;
            int AuditID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (AuditID == -1 || AuditID == 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
            {
                role = Convert.ToInt32(Request.QueryString["Role"]);
            }
            Response.Redirect("../InternalAuditTool/ObservationDraftList.aspx?AOD=" + "AuditClose" + "&AuditID=" + AuditID + "&Role=" + 4 + "&Type=AuditClose");
        }

        protected void btnDraft_Click(object sender, EventArgs e)
        {
            #region code generate single report 
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (AuditID == -1)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditIDForBack"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditIDForBack"]);
                    }
                }

                string CustomerBranchName = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string PeriodName = string.Empty;
                int VerticalID = -1;
                string VerticalName = string.Empty;
                string ZipFolderName = string.Empty;

                string AditHeadName = string.Empty;
                string BranchName = string.Empty;
                string Area = string.Empty;
                string location = string.Empty;
                var ReportNameAndNumber = string.Empty;
                var FileName = string.Empty;
                //List<string> reportFilePathList = new List<string>();
                List<KeyValuePair<string, string>> reportFilePathList = new List<KeyValuePair<string, string>>();
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        CustomerBranchName = ddlSubEntity4.SelectedItem.Text.Trim();
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    var videtails = UserManagementRisk.getverticaldetails(Portal.Common.AuthenticationHelper.CustomerID);
                    if (videtails != null)
                    {
                        VerticalName = videtails.VerticalName;
                        VerticalID = videtails.ID;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalName = ddlVerticalID.SelectedItem.Text.Trim();
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["brachName"])))
                {
                    CustomerBranchName = Convert.ToString(ViewState["brachName"]);
                }
                List<string> AssignedUserList = new List<string>();
                var customerName = UserManagementRisk.GetCustomerName(CustomerId);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditDetails = (from row in entities.AuditClosureDetails
                                        where row.ID == AuditID
                                        select row).FirstOrDefault();
                    CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                    FinancialYear = AuditDetails.FinancialYear;
                    PeriodName = AuditDetails.ForMonth;
                    VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                    if (string.IsNullOrEmpty(VerticalName))
                    {
                        VerticalName = (from row in entities.mst_Vertical
                                        where row.ID == VerticalID
                                        select row.VerticalName).FirstOrDefault();
                    }
                    ReportNameAndNumber = "Draft/" + FinancialYear + "/" + VerticalName;
                    FileName =  FinancialYear + "_" + VerticalName;
                    ZipFolderName=  FinancialYear + "_" + VerticalName;
                    AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                        join row1 in entities.mst_User
                                        on row.UserID equals row1.ID
                                        where row.IsActive == true
                                        && row.AuditID == AuditID
                                        select row1.FirstName + " " + row1.LastName).Distinct().ToList();
                    
                }

                DateTime DraftReportDateTime = (DateTime)UserManagementRisk.UpdateDraftReportDate(CustomerId, CustomerBranchId, VerticalID, PeriodName, FinancialYear);
                //string draftDate = string.Empty;
                //string draftTime = string.Empty;
                string draftDateTime = string.Empty;

                if (DraftReportDateTime != null)
                {
                    var draftDate = DraftReportDateTime.Date.ToString("MMM dd,yyyy");
                    var draftTime = DraftReportDateTime.ToString("HH:mm");// added by sagar more on 23-01-2020
                    draftDateTime = draftDate + " " + draftTime;
                }

                #region Word Report 1  
                {
                    System.Text.StringBuilder stringbuilderTOPFirst = new System.Text.StringBuilder();

                    // added by sagar more on 20-01-2020
                    //string SWReportImage = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg";

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                        select row).ToList();

                        var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList(); // added by sagar more on 21-01-2020

                        if (itemlist.Count > 0)
                        {
                            #region first Report
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                + " Area:" + Area + " </b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                           + " Location: " + CustomerBranchName + "</b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                           + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                            stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b><u>"
                           + "Part A: Executive Summary</u></b></p>");

                            stringbuilderTOPFirst.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                            "<table style='width:100%; font-family:Calibri;'>" +
                            "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                            "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                            "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                            "</table>" +
                            "</td>" +
                            "</tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                            "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr></table>");

                            stringbuilderTOPFirst.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 1.Executive Summary </b><br/><br/>");
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> Key findings are as under: </b>");

                            int AsciiValue = 97;
                            int SrNoCategoryTable = 97;
                            var TotalMajorCatg = 0;
                            var TotalModerateCatg = 0;
                            var TotalMinorCatg = 0;
                            var TotalCount = 0;
                            var ProcessRatingName = string.Empty;
                            foreach (var item in itemlist)
                            {
                                stringbuilderTOPFirst.Append(@"<p style='font-family:Calibri; font-size:12pt;'>"
                                + Convert.ToChar(AsciiValue) + ") " + item.BriefObservation +
                                "<br/><b><u>Management response: </u></b>" + item.ManagementResponse + "</p>");
                                AsciiValue++;
                                if (!string.IsNullOrEmpty(item.ProcessRatingName))
                                {
                                    ProcessRatingName = item.ProcessRatingName;
                                }
                            }

                            stringbuilderTOPFirst.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                            stringbuilderTOPFirst.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 
                                A summary of findings based on risk rankings and nature of control gaps is given in the table below: </p><br/>");

                            stringbuilderTOPFirst.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>
                                                        <thead><tr> 
                                                        <th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Sr.No." + "</th>" +
                                                    "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Nature of issues reported" + "</th>" +
                                                    "<th colspan='4' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Risk Rating" + "</th>" +
                                                    "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Page reference (in the detailed report)" +
                                                    "</th></tr><tr><th style='border: 1px solid black; background-color:#FDB924; font-weight:bold'>Major</th>" +
                                                    "<th style='border: 1px solid black; background-color:#FFFF00; font-weight:bold'>Moderate</th>" +
                                                    "<th style='border: 1px solid black; background-color:#92D050; font-weight:bold'>Minor</th>" +
                                                    "<th style='border: 1px solid black; background-color:#BEEAF7; font-weight:bold'>Total</th></tr></thead><tbody>"
                                );

                            var CategoryList = itemlist.Select(entry => entry.ObservationCategory).Distinct().ToList();
                            foreach (var catgID in CategoryList)
                            {
                                var catgName = itemlist.Where(entry => entry.ObservationCategory == catgID).FirstOrDefault();
                                var MajorCatg = itemlist.Where(entry => entry.ObservationRating == 1 && entry.ObservationCategory == catgID).ToList().Count();
                                var ModerateCatg = itemlist.Where(entry => entry.ObservationRating == 2 && entry.ObservationCategory == catgID).ToList().Count();
                                var MinorCatg = itemlist.Where(entry => entry.ObservationRating == 3 && entry.ObservationCategory == catgID).ToList().Count();
                                var Total = MajorCatg + ModerateCatg + MinorCatg;

                                TotalMajorCatg = TotalMajorCatg + MajorCatg;
                                TotalModerateCatg = TotalModerateCatg + ModerateCatg;
                                TotalMinorCatg = TotalMinorCatg + MinorCatg;
                                TotalCount = TotalCount + Total;

                                stringbuilderTOPFirst.Append(@"<tr><td style='border: 1px solid black; text-align: center;'>" + Convert.ToChar(SrNoCategoryTable) + "." +
                                "</td><td style='border: 1px solid black;'>" +
                                catgName.ObservationCategoryName + "</td><td style='border: 1px solid black; text-align: center;'>" + MajorCatg
                                + "</td><td style='border: 1px solid black; text-align: center;'>" + ModerateCatg + "</td><td style='border: 1px solid black; text-align: center;'>" + MinorCatg + "</td><td style='border: 1px solid black;  text-align: center;'>" + Total + "</td><td style='border: 1px solid black;'></td></tr>"
                                );
                                SrNoCategoryTable++;
                            }
                            stringbuilderTOPFirst.Append(@"</tbody><tfoot><tr><td Colspan='2' style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'>Total</td>
                                  <td style='border: 1px solid black; background-color:#FDB924; font-weight:bold; text-align: center;'>" + TotalMajorCatg
                            + "</td><td style='border: 1px solid black; background-color:#FFFF00; font-weight:bold; text-align: center;'>" + TotalModerateCatg
                            + "</td><td style='border: 1px solid black; background-color:#92D050; font-weight:bold; text-align: center;'>" + TotalMinorCatg
                            + "</td><td style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold; text-align: center;'>" + TotalCount
                            + "</td><td style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'></td></tr>"
                            + "<tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Process Rating and Maturity Level</td>");
                            if (!string.IsNullOrEmpty(ProcessRatingName))
                            {
                                stringbuilderTOPFirst.Append(@"<td colspan='5' style='border: 1px solid black;'>" + ProcessRatingName + "</td></tr>");
                            }
                            else
                            {
                                stringbuilderTOPFirst.Append(@"<td colspan='5' style='border: 1px solid black;'></td></tr>");
                            }
                            stringbuilderTOPFirst.Append(@"< tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Overall conclusion</td><td colspan='5' style='border: 1px solid black; font-weight:bold'></td></tr>"
                             + "</tfoot></table>"
                             );

                            stringbuilderTOPFirst.Append(@"</table>");
                            string firstFilePath = DraftProcessRequestWordFirst(stringbuilderTOPFirst.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId, CustomerBranchName);

                            if (!string.IsNullOrEmpty(firstFilePath))
                            {
                                string fileName = "Executive Summary" + FileName + ".doc";
                                reportFilePathList.Add(new KeyValuePair<string, string>(fileName, firstFilePath));
                            }
                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                        }
                    }
                }
                #endregion

                #region Word Report Second                 
                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                int AnnexueId = 1;
               // string SWReportImage1 = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg"; // added by sagar more on 21-01-2020
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditDetails = (from row in entities.AuditClosureDetails
                                        where row.ID == AuditID
                                        select row).FirstOrDefault();
                    CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                    FinancialYear = AuditDetails.FinancialYear;
                    PeriodName = AuditDetails.ForMonth;
                    VerticalID = Convert.ToInt32(AuditDetails.VerticalId);

                    CustomerBranchName = (from row in entities.mst_CustomerBranch
                                          where row.ID == CustomerBranchId
                                          select row.Name).FirstOrDefault().ToString();

                    var s = (from row in entities.Tran_FinalDeliverableUpload
                             where row.AuditID == AuditID
                             select row).ToList();
                    AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                        join row1 in entities.mst_User
                                        on row.UserID equals row1.ID
                                        where row.IsActive == true
                                        && row.AuditID == AuditID
                                        select row1.FirstName + " " + row1.LastName).Distinct().ToList();
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                    select row).ToList();

                    var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList(); // added by sagar more on 21-01-2020
                    if (itemlist.Count > 0)
                    {
                        #region first Report
                        stringbuilderTOP.Append(@"<p style='margin-top:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                + " Area: " + Area + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                       + " Location: " + CustomerBranchName + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                       + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b><u>"
                       + "Part B: Internal Audit report </u></b></p>");

                        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                        "<table style='width:100%; font-family:Calibri;'>" +
                        "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                        "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                        "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                        "</table>" +
                        "</td>" +
                        "</tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr>" +
                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + location + " : " + AditHeadName + "</td></tr></table>");

                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b> Table of Contents </b>");

                        stringbuilderTOP.Append("<ol>");
                        stringbuilderTOP.Append("<li><b>Background:......................................................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("<li><b>Audit Scope and Objective:...............................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("<li><b>Detailed Findings:..............................................................................................................................................................................</b></li>");
                        stringbuilderTOP.Append("</ol></p>");


                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 1. Background: </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 2. Audit Scope and Objective: </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-left:15px;margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> Objective(s) of the review:  </b></p>");
                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 3. Detailed Findings: </b></p><br/><br/>");

                        int SrNo = 01;

                        foreach (var item in itemlist)
                        {
                            string ColorCode = string.Empty;
                            if (SrNo != 01)
                            {
                                stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                            }
                            stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border-top: 1px solid black;'>");
                            stringbuilderTOP.Append(@"<tr> 
                                    <td colspan='2' style='width:70%;border-top: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Issue # " + SrNo + " : " + item.ObservationTitle + "</td>");
                            if (item.ObservationRating == 1)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#FDB924; font-weight:bold'>Major/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            else if (item.ObservationRating == 2)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#FFFF00; font-weight:bold'>Moderate/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            else if (item.ObservationRating == 3)
                            {
                                stringbuilderTOP.Append(@"<td style='width:30%; border-top: 1px solid black; background-color:#92D050; font-weight:bold'>Minor/" + item.ObservationCategoryName + "</td></tr>");
                            }
                            stringbuilderTOP.Append(@"<tr><td colspan='3'>" + item.ObjBackground + "</td ></tr>" +
                            "<tr><td style='width:30%; border-top: 1px solid black; font-weight:bold'>Finding(s)</td>" +
                            "<td style='width:40%; border-top: 1px solid black;font-weight:bold'>Probable Cause(s)</td>" +
                            "<td style='width:30%; border-top: 1px solid black;font-weight:bold'>Recommendation(s)</td></tr>" +
                            "<tr><td style='width:30%; border-top: 1px solid black;'>" + item.Observation + "</td>" +
                            "<td valign='top' style='width:40%; border-top: 1px solid black;'>" + item.RootCost + "</td>" +
                            "<td style='width:30%; border-top: 1px solid black;'>" + item.Recomendation + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;font-weight:bold'>Impact/Potential Risk(s)</td>" +
                                "<td style='width:30%;font-weight:bold'>Management Action Plan(s)</td></tr>");

                            string Timeline = item.TimeLine != null ? item.TimeLine.Value.ToString("MMM dd, yyyy") : null;

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td  valign='top' style='width:40%;'>" + item.FinancialImpact + "</td>" +
                                "<td style='width:30%;'>" + item.ManagementResponse + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;'></td>" +
                                "<td style='width:30%;'>" + "<b>Responsibility</b> : " + item.PersonResponsibleName + "</td></tr>");

                            stringbuilderTOP.Append(@"<tr><td style='width:30%;'></td>" +
                                "<td style='width:40%;'></td>" +
                                "<td style='width:30%;'>" + "<b>Timeline</b> : " + Timeline + "</td></tr>");

                            stringbuilderTOP.Append(@"</table>");
                            stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                            List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDID);
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetObservationAudioVideoList(AuditID, item.ATBDID);
                            if (!string.IsNullOrEmpty(item.BodyContent) || AllinOneDocumentList.Count > 0 || observationAudioVideoList.Count > 0)
                            {
                                stringbuilderTOP.Append(@"<label><b>Annexure:</b>" + AnnexueId + "</label><br>");
                                stringbuilderTOP.Append(@"<label><b>" + item.AnnexueTitle.Trim() + "</b></label><br/>");// changed by sagar more on 22-01-2020
                                AnnexueId++;
                            }

                            if (!string.IsNullOrEmpty(item.BodyContent))
                            {
                                stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                                stringbuilderTOP.Append(@"<P style='margin-top:1%;margin-bottom:1%; font-family:Calibri;'>" + item.BodyContent.Replace("@nbsp;", "").Replace("\r", "").Replace("\n", "") + "</p>");
                                stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                            }

                           
                            if (AllinOneDocumentList.Count > 0)
                            {
                                foreach (var ObjImageitem in AllinOneDocumentList)
                                {

                                    string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                    if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                                    {
                                        stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                                        stringbuilderTOP.Append(@"<P style='margin-top:1%;margin-bottom:1%; font-family:Calibri;''>");
                                        DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                        // DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        stringbuilderTOP.Append("<img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                                        + CustomerBranchId + "/" + VerticalID + "/" + item.ProcessId + "/"
                                        + item.FinancialYear + "/" + item.ForPeriod + "/"
                                        + AuditID + "/"
                                        + item.ATBDID + "/1.0/" + ObjImageitem.ImageName + "' alt ='d'></img >" + "<br>");
                                        stringbuilderTOP.Append(@"</p>");
                                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    }
                                }
                            }
                            int audioVideoChar = 97;
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (var audioVideoLink in observationAudioVideoList)
                                {
                                    char alpha = Convert.ToChar(audioVideoChar);
                                    stringbuilderTOP.Append(@"<label>" + alpha + ")</label>&nbsp;&nbsp;<a href=" + audioVideoLink.AudioVideoLink.Trim() + ">" + audioVideoLink.AudioVideoLink.Trim() + "</a><br/>");
                                    audioVideoChar++;
                                }
                            }
                            stringbuilderTOP.Append(@"<br clear=all style='mso-special-character:line-break;page-break-before:always'>");
                        }
                        string secondFilePath = DraftProcessRequestWordSecond(stringbuilderTOP.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId, CustomerBranchName);

                        // added by sagar more on 20-01-2020
                        if (!string.IsNullOrEmpty(secondFilePath))
                        {
                            string fileName = "Internal Audit Report" + FileName + ".doc";
                            reportFilePathList.Add(new KeyValuePair<string, string>(fileName, secondFilePath));
                        }
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                    }
                }
                #endregion
                // changed by sagar more on 20-01-2020
                if (reportFilePathList.Count > 0)
                {
                    using (ZipFile AuditZip = new ZipFile())
                    {
                        AuditZip.AddDirectoryByName(FinancialYear);
                        foreach (var file in reportFilePathList)
                        {
                            string filePath = file.Value;
                            if (file.Value != null && File.Exists(filePath))
                            {
                                string[] filename = file.Value.Split('.');
                                string[] docFilePath = filePath.Split(new string[] { FinancialYear }, StringSplitOptions.None);
                                AuditZip.AddEntry(FinancialYear + "/" + file.Key, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                        }
                        var zipMs = new MemoryStream();
                        AuditZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=DraftReportGenerated"+ ZipFolderName + ".zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            #endregion
        }

        
        public string DraftProcessRequestWordFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId, string LocationName)
        {
            long AuditID = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            {
                AuditID = Convert.ToInt64(ViewState["AuditID"]);
            }
            //string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>

                 p.MsoHeader, li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }

                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.2in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Calibri;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;                   
                }

                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
            sbTop.Append(context);
            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append("<div style='border:none;mso-border-bottom-alt:solid windowtext.75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            //"<div style='text-align:right;'> <img height='100' width='120' src='" + hpath + "' alt ='d'></img></div>" +
            " <div style='text-align:center;'> Executive Summary Report</div></div>");

            sbTop.Append(@"<p class=MsoHeader style='border:none;mso-border-bottom-alt:solid windowtext 50pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");
            sbTop.Append("</div>");

            sbTop.Append("<div style='mso-element:footer' id=f1>" +
            "<p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>" +
            "<font size='2'> <span> Confidential</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;<span style='mso-field-code: PAGE'><span style='mso-no-proof:yes'>1</span></span> of<span style='mso-field-code: NUMPAGES '></span></font></div>");

            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);

            string directoryPath = string.Empty;
            directoryPath = Server.MapPath("~/TempAuditFinalDeleverableDocument/" + customerID + "/"
                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                           + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                           + "/1.0");
            if (!Directory.Exists(directoryPath))
            {
                DocumentManagement.CreateDirectory(directoryPath);
            }
            string fileName = "Executive Summary_" + FinancialYear + "_" + PeriodName + ".doc";
            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            Guid fileKey1 = Guid.NewGuid();
            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(fileName));
            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            DocumentManagement.Audit_SaveDocFiles(Filelist1);
            string finalFilePath = string.Empty;
            foreach (KeyValuePair<string, Byte[]> item in Filelist1)
            {
                finalFilePath = item.Key;
            }
            return finalFilePath;
        }
        
        public string DraftProcessRequestWordSecond(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId, string LocationName)
        {
            long AuditID = -1;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            {
                AuditID = Convert.ToInt64(ViewState["AuditID"]);
            }
            //string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");

            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                 p.MsoHeader, li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.2in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Calibri;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;                   
                }

                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
            sbTop.Append(context);

            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append("<div style='border:none;mso-border-bottom-alt:solid windowtext.75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            //"<div style='text-align:right;'> <img height='100' width='120' src='" + hpath + "' alt ='d'></img></div>" +
            " <div style='text-align:center;'> Internal Audit Report</div></div>");

            sbTop.Append(@"<p class=MsoHeader style='border:none;mso-border-bottom-alt:solid windowtext 50pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");


            sbTop.Append("</div>");

            sbTop.Append("<div style='mso-element:footer' id=f1>" +
            "<p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>" +
            "<font size='2'> <span> Confidential</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;<span style='mso-field-code: PAGE'><span style='mso-no-proof:yes'>1</span></span> of<span style='mso-field-code: NUMPAGES '></span></font></div>");

            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            string directoryPath = string.Empty;
            directoryPath = Server.MapPath("~/TempAuditFinalDeleverableDocument/" + customerID + "/"
                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                           + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                           + "/1.0");
            if (!Directory.Exists(directoryPath))
            {
                DocumentManagement.CreateDirectory(directoryPath);
            }
            string fileName = "Internal Audit Report_" + FinancialYear + "_" + PeriodName + ".doc";
            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            Guid fileKey1 = Guid.NewGuid();
            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(fileName));
            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            DocumentManagement.Audit_SaveDocFiles(Filelist1);
            string finalFilePath = string.Empty;
            foreach (KeyValuePair<string, Byte[]> item in Filelist1)
            {
                finalFilePath = item.Key;
            }
            return finalFilePath;
        }
        
        public void BindMainGridObservationFromBackButton(long AuditID, string ddlLegalEntity, string ddlSubEntity1, string ddlSubEntity2, string ddlSubEntity3, string ddlSubEntity4, string FinancialYearr, string ddlPeriod)
        {
            string FinancialYear = string.Empty;
            string PeriodName = string.Empty;
            int CustomerBranchId = -1;
            int VerticalID = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity))
            {
                if (ddlLegalEntity != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1))
            {
                if (ddlSubEntity1 != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2))
            {
                if (ddlSubEntity2 != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3))
            {
                if (ddlSubEntity3 != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4))
            {
                if (ddlSubEntity4 != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4);
                }
            }
            if (!string.IsNullOrEmpty(FinancialYearr))
            {
                FinancialYear = FinancialYearr;
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }
            if (!string.IsNullOrEmpty(ddlPeriod))
            {
                if (ddlPeriod != "-1")
                {
                    PeriodName = ddlPeriod;
                }
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditDetails = (from row in entities.AuditClosureDetails
                                    where row.ID == (int)AuditID
                                    select row).FirstOrDefault();
                CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                FinancialYear = AuditDetails.FinancialYear;
                PeriodName = AuditDetails.ForMonth;
                VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                
                    var branchname = (from row in entities.mst_CustomerBranch
                                      where row.ID == CustomerBranchId
                                      select row.Name).FirstOrDefault();
                   var CustomerBranchName = branchname;
                var VerticalDetails = (from row in entities.AuditClosureDetails
                                    join row1 in entities.mst_Vertical
                                    on row.VerticalId equals row1.ID
                                    where row.ID == (int)AuditID
                                    select row1.VerticalName).FirstOrDefault();

                lblObservationSelectionDetails.Text = string.Empty;
                lblObservationSelectionDetails.Text = CustomerBranchName + "/" + VerticalDetails + "/" + FinancialYear + "/" + PeriodName;
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SPGETObservationDetails_Result> query = new List<SPGETObservationDetails_Result>();
                if (CustomerBranchId != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinancialYear) && !string.IsNullOrEmpty(PeriodName) && AuditID != -1)
                {
                    query = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                             select row).ToList();

                    if (query != null)
                    {
                        lbtObservationList.Visible = btnObservation.Visible = btnDraft.Visible = btnGenrateReport.Visible = true;

                        GridObservationDetails.DataSource = query;
                        Session["TotalRowsObservation"] = query.Count;
                        GridObservationDetails.DataBind();
                        //LBTObservationDetails.Attributes.Add("class", "active");
                        //dvObservationDetails.Attributes.Add("class", "active");
                        //LBTAuditDetails.Attributes.Remove("class");
                        //dvAuditDetails.Attributes.Remove("class");
                        //LBTAuditDetails.Attributes.Add("class", "tab-pane");
                        LBTObservationDetails_Click(null, null);
                        bindObservationPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCountObservation());
                        if (count > 0)
                        {
                            int gridindex = GridObservationDetails.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListObservation.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }
                else if (CustomerBranchId != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinancialYear) && AuditID != -1)
                {
                    query = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                             select row).ToList();

                    GridObservationDetails.DataSource = null;
                    GridObservationDetails.DataBind();

                    if (query != null)
                    {
                        GridObservationDetails.DataSource = query;
                        Session["TotalRowsObservation"] = query.Count;
                        GridObservationDetails.DataBind();

                        bindObservationPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCountObservation());
                        if (count > 0)
                        {
                            int gridindex = GridObservationDetails.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListObservation.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }

            }
        }
    }
}