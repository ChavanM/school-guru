﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class PastAuditUpload : System.Web.UI.Page
    {
        protected static int RoleId;
        protected static string AuditHeadOrManagerReport;
        protected bool DepartmentHead = false;
        protected static bool ManagementFlag = false;
        public bool VerticalApplicable = false;
        public List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            //RoleId = CustomerManagementRisk.GetRoleid(Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (CustomerManagementRisk.CheckIsManagement(Portal.Common.AuthenticationHelper.UserID) == 8)
            {
                ManagementFlag = true;
            }
            if (!IsPostBack)
            {
                BindLegalEntityData(ddlLegalEntity);
                BindFnancialYear(ddlFilterFinancial);
                BindAuditBackground();
                BindPastAuditData();
                bindPageNumber();
                // GetPageDisplaySummary();
                if (Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    VerticalApplicable = false;
                }
                else
                {
                    VerticalApplicable = true;
                }
            }
        }

        public void BindAuditBackground()
        {
            int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Result = (from row in entities.AuditBackgrounds
                              where row.CustomerID == CustomerID
                              && row.IsDeleted == false
                              select row).ToList();

                var query = (from row in Result
                             select new { ID = row.ID, AuditBackground = row.AuditBackground1 }).OrderBy(entry => entry.AuditBackground).ToList<object>();

                ddlAuditBackground.DataTextField = "AuditBackground";
                ddlAuditBackground.DataValueField = "ID";
                ddlAuditBackground.DataSource = query;
                ddlAuditBackground.DataBind();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            BindPastAuditData();
        }
        public void BindVerticalID(DropDownList DRP, int? BranchId)
        {
            if (BranchId != null)
            {
                DRP.DataTextField = "VerticalName";
                DRP.DataValueField = "VerticalsId";
                DRP.Items.Clear();
                DRP.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                DRP.DataBind();
                DRP.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }

        public void BindFnancialYear(DropDownList PopFDRP)
        {
            var details = UserManagementRisk.FillFnancialYear();
            if (details != null)
            {
                PopFDRP.DataTextField = "Name";
                PopFDRP.DataValueField = "ID";
                PopFDRP.Items.Clear();
                PopFDRP.DataSource = details;
                PopFDRP.DataBind();
                PopFDRP.Items.Insert(0, new ListItem("Financial Year", "-1"));
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {

        }

        public void BindLegalEntityData(DropDownList PopFDRP)
        {
            int CustomerId = (int)Portal.Common.AuthenticationHelper.CustomerID;

            int userID = Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));
            
            PopFDRP.DataTextField = "Name";
            PopFDRP.DataValueField = "ID";
            PopFDRP.Items.Clear();
            if (DepartmentHead)
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            else
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            }
            PopFDRP.DataBind();
            PopFDRP.Items.Insert(0, new ListItem("Unit", "-1"));           
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerId = (int)Portal.Common.AuthenticationHelper.CustomerID;
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindVerticalID(ddlVerticalID, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindVerticalID(ddlVerticalID, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindVerticalID(ddlVerticalID, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindVerticalID(ddlVerticalID, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindVerticalID(ddlVerticalID, Convert.ToInt32(ddlFilterLocation.SelectedValue));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                //this.countrybindIndustory(ddlVertical, 0);
                //BindSubEntityData(ddlSubEntity1PopPup, 0);
                //BindSubEntityData(ddlSubEntity2PopPup, 0);
                //BindSubEntityData(ddlSubEntity3PopPup, 0);
                //BindSubEntityData(ddlFilterLocationPopPup, 0);                
                ViewState["Mode"] = 0;
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindPastAuditData()
        {
            try
            {
                int CustomerID = (int)Portal.Common.AuthenticationHelper.CustomerID;
                string FinancialYear = string.Empty;
                long VerticalId = -1;
                int CustomerBranchId = -1;
                int AuditBackgroundID = -1;
                string Filter = string.Empty;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        Session["BranchList"] = 0;
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                else
                {
                    List<long> branchs = new List<long>();
                    for (int i = 0; i < ddlLegalEntity.Items.Count; i++)
                    {
                        if (ddlLegalEntity.Items[i].Value != "-1")
                        {
                            if (ddlLegalEntity.Items[i].Value != "")
                            {
                                branchs.Add(Convert.ToInt64(ddlLegalEntity.Items[i].Value));
                                Session["BranchList"] = branchs;
                            }
                        }

                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    mst_Vertical objVerti = new mst_Vertical()
                    {
                        VerticalName = "NA",
                        CustomerID = CustomerID
                    };
                    if (!UserManagementRisk.VerticalNameExist(objVerti))
                    {
                        UserManagementRisk.CreateVerticalName(objVerti);
                        VerticalId = objVerti.ID;
                    }
                    else
                    {
                        VerticalId = UserManagementRisk.VerticalgetBycustomerid(CustomerID);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalId = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedValue != "-1")
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                if (!String.IsNullOrEmpty(ddlAuditBackground.SelectedValue))
                {
                    if (ddlAuditBackground.SelectedValue != "-1")
                    {
                        AuditBackgroundID = Convert.ToInt32(ddlAuditBackground.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(tbxFilter.Text.Trim()))
                {
                    Filter = tbxFilter.Text.Trim();
                }
                Branchlist.Clear();
                List<long> branch = new List<long>();
                branch = Session["BranchList"] as List<long>;
                if (branch != null)
                {
                    for (int i = 0; i < branch.Count; i++)
                    {
                        CustomerBranchId = Convert.ToInt32(branch[i]);
                        GetAllHierarchy(CustomerID, CustomerBranchId);
                    }
                }
                else
                {
                    var bracnhes = GetAllHierarchy(CustomerID, CustomerBranchId);
                }

                var Branchlistloop = Branchlist.ToList();


                var AllComplianceRoleMatrix = GetAuditGridDisplay(CustomerID, Branchlist.ToList(), VerticalId, FinancialYear, AuditBackgroundID, Filter);
                grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                Session["TotalRows"] = AllComplianceRoleMatrix.Count;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<PastAuditDocumentclass> GetAuditGridDisplay(int Customerid, List<long> BranchID, long VerticalID, string FinancialYear, int AuditBackgroundID, string Filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<PastAuditDocumentclass> riskassignmentexport = new List<PastAuditDocumentclass>();
                riskassignmentexport = (from C in entities.SP_BindPastAuditDocument(Customerid)
                                        group C by new
                                        {
                                            C.CustomerBranch,
                                            C.CustomerBrachName,
                                            C.FinancialYear,
                                            C.VerticalID,
                                            C.VerticalName,
                                            C.AuditBackgroundID,
                                            C.AuditBackground,
                                            C.AuditDocumentID
                                        } into GCS
                                        select new PastAuditDocumentclass()
                                        {
                                            CustomerBranch = GCS.Key.CustomerBranch,
                                            CustomerBrachName = GCS.Key.CustomerBrachName,
                                            FinancialYear = GCS.Key.FinancialYear,
                                            VerticalID = GCS.Key.VerticalID,
                                            VerticalName = GCS.Key.VerticalName,
                                            AuditBackgroundID = GCS.Key.AuditBackgroundID,
                                            AuditBackground = GCS.Key.AuditBackground,
                                            AuditDocumentID = GCS.Key.AuditDocumentID
                                        }).ToList();

                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long)entry.CustomerBranch)).ToList();
                }
                if (VerticalID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.VerticalID == VerticalID).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                if (AuditBackgroundID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.AuditBackgroundID == AuditBackgroundID).ToList();
                }
                if (!string.IsNullOrEmpty(Filter))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.CustomerBrachName.ToLower().Contains(Filter.ToLower()) || entry.FinancialYear.Contains(Filter) || entry.AuditBackground.ToLower().Contains(Filter.ToLower())).ToList();
                }
                return riskassignmentexport;
            }
        }

        public class PastAuditDocumentclass
        {
            public long CustomerBranch { get; set; }
            public string CustomerBrachName { get; set; }
            public int VerticalID { get; set; }
            public string VerticalName { get; set; }
            public string FinancialYear { get; set; }
            public int AuditBackgroundID { get; set; }
            public string AuditBackground { get; set; }
            public int AuditDocumentID { get; set; }
        }
        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            /*
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            */
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindPastAuditData();
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<PastAuditDocumentMapping> GetFileData1(int PastAuditDocID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.PastAuditDocumentMappings
                                where row.PastAuditDocID == PastAuditDocID
                                select row).ToList();

                return fileData;
            }
        }


        protected void grdComplianceRoleMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int PastAuditDocID = Convert.ToInt32(commandArgs[0]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "PopulateVerticalMappingdata(" + PastAuditDocID + "," + 2 + ");", true);//View
                    //DownLoadClick(PastAuditDocID);
                }
                if (e.CommandName.Equals("EditAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int PastAuditDocID = Convert.ToInt32(commandArgs[0]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "PopulateVerticalMappingdata(" + PastAuditDocID + "," + 0 + ");", true);//Edit
                    //DownLoadClick(PastAuditDocID);
                }
                if (e.CommandName.Equals("DeleteAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int PastAuditDocID = Convert.ToInt32(commandArgs[0]);
                    DeletePastAuditDoc(PastAuditDocID);
                }
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DeletePastAuditDoc(int PastAuditDocID)
        {
            try
            {
                int CustomerID = (int)Portal.Common.AuthenticationHelper.CustomerID;
                AuditControlEntities entities = new AuditControlEntities();

                var file = (from C in entities.SP_BindPastAuditDocumentFileMapping(CustomerID, PastAuditDocID)
                            select C).ToList();
                foreach (var item in file)
                {
                    int file_id = item.FileID;
                    var subfile = (from row in entities.PastAuditDocumentMappings
                                   where row.ID == file_id
                                   select row).FirstOrDefault();
                    subfile.IsDeleted = true;
                    entities.SaveChanges();
                }


                var temp_file = (from C in entities.Past_Audit_Documents
                                 where C.ID == PastAuditDocID
                                 select C).FirstOrDefault();
                temp_file.Isdeleted = true;

                entities.SaveChanges();
                BindPastAuditData();

                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Audit Deleted Successfully.";

            }
            catch (Exception)
            {
                throw;
            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindPastAuditData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlAuditBackground_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPastAuditData();
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindPastAuditData();
        }
    }
}