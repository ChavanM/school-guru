﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ObservationDraftList : System.Web.UI.Page
    {
        //int Statusid;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool auditHeadFlag;
        public static List<long> Branchlist = new List<long>();
        protected static int CustomerId = 0;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                ViewState["Status"] = 1;

                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Convert.ToString(Request.QueryString["Type"]) == "AuditClose")
                    {
                        roles.Clear();
                        roles.Add(4);
                    }
                }
                if (roles.Contains(3))
                {
                    PerformerFlag = true;
                    ShowPerformer(sender, e);
                }
                else if (roles.Contains(4))
                {
                    ReviewerFlag = true;
                    ShowReviewer(sender, e);
                }
                else
                {
                    PerformerFlag = true;
                    ShowPerformer(sender, e);
                }

                BindProcess();
                bindPageNumber();
            }
        }
        private void BindProcess()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (AuditHeadOrManagerReport == null)
                {
                    AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
                }
                int auditId = 0;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditId = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";
                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID, auditId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                }
                else
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownPerformerAndReviewer(CustomerId, Portal.Common.AuthenticationHelper.UserID, auditId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                int processID = -1;
                long branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                int Verticalid = -1;
                long auditId = 0;
                int statusid = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditId = Convert.ToInt64(Request.QueryString["AuditID"]);
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        statusid = Convert.ToInt32(ddlStatus.SelectedValue);
                    }
                }

                var AuditDetail = UserManagementRisk.GetAuditDatail(auditId);
                if (AuditDetail != null)
                {
                    List<sp_Observation_DraftList_Result> ccccc = new List<sp_Observation_DraftList_Result>();
                    branchid = AuditDetail.BranchId;
                    FinYear = AuditDetail.FinancialYear;
                    Period = AuditDetail.ForMonth;
                    Verticalid = (int)AuditDetail.VerticalId;
                    var ObservationDetail = UserManagementRisk.GetObservationDetails(branchid, FinYear, Period, Verticalid, processID, statusid, Convert.ToInt32(auditId));
                    if (AuditHeadOrManagerReport == null)
                    {
                        if (PerformerFlag || ReviewerFlag)
                        {
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                var objcust = (from row in entities.InternalControlAuditAssignments
                                               where row.AuditID == auditId && row.UserID == Portal.Common.AuthenticationHelper.UserID
                                               select row.SubProcessId).ToList();
                                
                                ObservationDetail = ObservationDetail.Where(entry => objcust.Contains(entry.SubProcessId)).ToList();

                                ObservationDetail = ObservationDetail.OrderByDescending(e => e.Observation).ToList();
                            }
                        }
                        grdObservationList.DataSource = ObservationDetail;
                        Session["TotalRows"] = ObservationDetail.Count;
                        grdObservationList.DataBind();
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var objcust = (from row in ObservationDetail
                                           join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                           on row.ProcessId equals row1.ProcessId
                                           where row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                           && row1.BranchID == branchid
                                           select row).ToList();

                            objcust = objcust.OrderByDescending(e => e.Observation).ToList();

                            if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                            {
                                if (Convert.ToString(Request.QueryString["Type"]) == "AuditClose")
                                {
                                    objcust = objcust.Where(entry => entry.Observation != null && entry.Observation != "").Distinct().ToList();
                                }
                            }

                            objcust = objcust.OrderByDescending(e => e.Observation).ToList();
                            grdObservationList.DataSource = objcust;
                            Session["TotalRows"] = objcust.Count;
                            grdObservationList.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdObservationList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox tbxObservation = (e.Row.FindControl("tbxObservation") as TextBox);
                TextBox tbxManagementResponse = (e.Row.FindControl("tbxManagementResponse") as TextBox);
                TextBox txtStartDate = (e.Row.FindControl("txtStartDate") as TextBox);
                Button btnlnkAuditDetails = (e.Row.FindControl("lnkAuditDetails") as Button);
                LinkButton lnkViewAudit = (e.Row.FindControl("lnkViewAudit") as LinkButton);
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Convert.ToString(Request.QueryString["Type"]) == "AuditClose")
                    {
                        roles.Clear();
                        roles.Add(4);
                    }
                }
                if (roles.Contains(3))
                {
                    grdObservationList.Columns[8].Visible = false;
                }
                else
                {
                    grdObservationList.Columns[8].Visible = true;
                    if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                    {
                        if (Convert.ToString(Request.QueryString["Type"]) == "AuditClose")
                        {
                            lnkViewAudit.Visible = true;
                        }
                        else
                        {
                            lnkViewAudit.Visible = false;
                        }
                    }
                    else
                    {
                        lnkViewAudit.Visible = false;
                    }
                }
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                DropDownList ddlPersonResponsible = (e.Row.FindControl("ddlPersonResponsible") as DropDownList);
                TextBox tbxPersonResponsible = (e.Row.FindControl("tbxPersonResponsible") as TextBox);

                if (ddlPersonResponsible != null)
                {
                    ddlPersonResponsible.DataTextField = "Name";
                    ddlPersonResponsible.DataValueField = "ID";
                    ddlPersonResponsible.DataSource = RiskCategoryManagement.FillUsers(customerID);
                    ddlPersonResponsible.DataBind();
                    ddlPersonResponsible.Items.Insert(0, new ListItem("Select PersonResponsible", "-1"));
                }

                if (!string.IsNullOrEmpty(tbxPersonResponsible.Text))
                {
                    ddlPersonResponsible.SelectedValue = tbxPersonResponsible.Text;
                }
            }
        }

        protected void grdObservationList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void grdObservationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                int Verticalid = -1;
                if (e.CommandName.Equals("DeleteObservation"))
                {
                    int auditID = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    var AuditDetail = UserManagementRisk.GetAuditDatail(auditID);
                    if (AuditDetail != null)
                    {
                        branchid = AuditDetail.BranchId;
                        FinYear = AuditDetail.FinancialYear;
                        Period = AuditDetail.ForMonth;
                        Verticalid = (int)AuditDetail.VerticalId;

                        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;

                        long ATBDIdValue = 0;
                        string ObservationValue = string.Empty;
                        string ManagementResponseValue = string.Empty;
                        DateTime? TimeLineValue = null;
                        long PersonResponsibleValue = 0;
                        Label lblATBDId = (Label)grdObservationList.Rows[RowIndex].FindControl("lblATBDId") as Label;
                        TextBox tbxObservation = (TextBox)grdObservationList.Rows[RowIndex].FindControl("tbxObservation") as TextBox;
                        TextBox tbxManagementResponse = (TextBox)grdObservationList.Rows[RowIndex].FindControl("tbxManagementResponse") as TextBox;
                        TextBox tbxTimeLine = (TextBox)grdObservationList.Rows[RowIndex].FindControl("txtStartDate") as TextBox;
                        DropDownList ddlPersonResponsible = (DropDownList)grdObservationList.Rows[RowIndex].FindControl("ddlPersonResponsible") as DropDownList;

                        ATBDIdValue = Convert.ToInt64(lblATBDId.Text);
                        ObservationValue = tbxObservation.Text;
                        ManagementResponseValue = tbxManagementResponse.Text;
                        if (!string.IsNullOrEmpty(tbxTimeLine.Text))
                        {
                            TimeLineValue = Convert.ToDateTime(tbxTimeLine.Text);
                        }
                        else
                        {
                            TimeLineValue = null;
                        }
                        PersonResponsibleValue = Convert.ToInt64(ddlPersonResponsible.SelectedValue);

                        Observation_DraftList objauditDraft = new Observation_DraftList();
                        objauditDraft.ATBDId = ATBDIdValue;
                        objauditDraft.Observation = ObservationValue;
                        objauditDraft.ManagementResponse = ManagementResponseValue;
                        objauditDraft.TimeLine = TimeLineValue;
                        objauditDraft.PersonResponsible = PersonResponsibleValue;
                        objauditDraft.AuditID = auditID;
                        objauditDraft.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                        UserManagementRisk.AddObservationDraft(objauditDraft);

                        InternalControlAuditResult objaudit = new InternalControlAuditResult();
                        objaudit.CustomerBranchId = branchid;
                        objaudit.VerticalID = Verticalid;
                        objaudit.FinancialYear = FinYear;
                        objaudit.ForPerid = Period;
                        objaudit.ATBDId = ATBDIdValue;
                        objaudit.Observation = ObservationValue;
                        objaudit.ManagementResponse = ManagementResponseValue;
                        objaudit.TimeLine = TimeLineValue;
                        objaudit.PersonResponsible = PersonResponsibleValue;
                        objaudit.AuditID = auditID;
                        string OptDelete = "Delete";
                        RiskCategoryManagement.UpdateObservationDataDelete(objaudit, OptDelete);
                        RiskCategoryManagement.UpdateInternalAuditTransactionPersonresponsible(objaudit);
                        //added by rahul on 7 OCT 2017 
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var MstRiskResult = (from AC in entities.AuditClosures
                                                 where AC.ACStatus == 1
                                                 && AC.ATBDId == ATBDIdValue
                                                 && AC.CustomerbranchId == branchid
                                                 && AC.FinancialYear == FinYear
                                                 && AC.VerticalID == Verticalid
                                                 && AC.ForPeriod == Period
                                                 select AC).ToList();
                            if (MstRiskResult.Count > 0)
                            {
                                MstRiskResult.ForEach(entry => entry.ACStatus = 0);
                                entities.SaveChanges();
                            }
                        }
                        BindData();
                    }
                }
                else if (e.CommandName.Equals("ViewObservation"))
                {
                    int auditID = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    var AuditDetail = UserManagementRisk.GetAuditDatail(auditID);
                    if (AuditDetail != null)
                    {
                        branchid = AuditDetail.BranchId;
                        FinYear = AuditDetail.FinancialYear;
                        Period = AuditDetail.ForMonth;
                        Verticalid = (int)AuditDetail.VerticalId;
                        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        long ATBDIdValue = 0;
                        Label lblATBDId = (Label)grdObservationList.Rows[RowIndex].FindControl("lblATBDId") as Label;
                        Label lblProcessId = (Label)grdObservationList.Rows[RowIndex].FindControl("lblProcessId") as Label;
                        TextBox tbxObservation = (TextBox)grdObservationList.Rows[RowIndex].FindControl("tbxObservation") as TextBox;
                        TextBox tbxManagementResponse = (TextBox)grdObservationList.Rows[RowIndex].FindControl("tbxManagementResponse") as TextBox;
                        TextBox tbxTimeLine = (TextBox)grdObservationList.Rows[RowIndex].FindControl("txtStartDate") as TextBox;
                        DropDownList ddlPersonResponsible = (DropDownList)grdObservationList.Rows[RowIndex].FindControl("ddlPersonResponsible") as DropDownList;
                        int ProcessId = 0;
                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                        {
                            ProcessId = Convert.ToInt32(lblProcessId.Text);
                        }

                        ATBDIdValue = Convert.ToInt64(lblATBDId.Text);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + ATBDIdValue + "," + branchid + ",'" + FinYear + "','" + Period + "'," + 3 + "," + ProcessId + "," + Verticalid + "," + auditID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdObservationList.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            BindData();
        }

        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            BindData();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            BindData();
        }

        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }

        protected bool CheckActionButtonEnable(String Role, String SDate, String EDate)
        {
            if ((SDate == "" || EDate == "") && Role == "Performer")
                return false;
            else if (SDate != "" && EDate != "")
                return true;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {

        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdObservationList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void lblExport_Click(object sender, EventArgs e)
        {
            long branchid = -1;
            string FinYear = String.Empty;
            string Period = String.Empty;
            int Verticalid = -1;
            long auditId = 0;
            int statusid = -1;
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                auditId = Convert.ToInt64(Request.QueryString["AuditID"]);
            }
            if (!String.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                if (ddlStatus.SelectedValue != "-1")
                {
                    statusid = Convert.ToInt32(ddlStatus.SelectedValue);
                }
            }
            var AuditDetail = UserManagementRisk.GetAuditDatail(auditId);
            if (AuditDetail != null)
            {
                branchid = AuditDetail.BranchId;
                FinYear = AuditDetail.FinancialYear;
                Period = AuditDetail.ForMonth;
                Verticalid = (int)AuditDetail.VerticalId;
                
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var customer = (from row in entities.mst_Customer
                                    where row.ID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID
                                    select row.Name).FirstOrDefault();
                    
                    var itemlist = (from row in entities.sp_ObservationDraftList_Reports(Convert.ToInt32(branchid), FinYear, Verticalid, Period, Convert.ToInt32(auditId))
                                    select row).ToList();

                    if (AuditHeadOrManagerReport == null)
                    {
                        if (PerformerFlag || ReviewerFlag)
                        {
                                var objcust = (from row in entities.InternalControlAuditAssignments
                                               where row.AuditID == auditId && row.UserID == Portal.Common.AuthenticationHelper.UserID
                                               select row.SubProcessId).ToList();

                            itemlist = itemlist.Where(entry => objcust.Contains(entry.SubProcessId)).ToList();
                        }
                    }
                    else
                    {
                            var objcust = (from row in itemlist
                                           join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                           on row.ProcessID equals row1.ProcessId
                                           where row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                           && row1.BranchID == branchid
                                           select row).ToList();
                            
                    }
                    if (statusid != -1)
                    {
                        itemlist = itemlist.Where(entry => entry.AStatusId == statusid).ToList();
                    }

                    itemlist = itemlist.Where(entry => entry.Observation != null && entry.Observation != "").ToList();

                    if (itemlist.Count > 0)
                    {
                        Session["GridObservationDetailsNEW"] = null;
                        Session["GridObservationDetailsNEW"] = (itemlist as List<sp_ObservationDraftList_Reports_Result>).ToDataTable();

                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            try
                            {
                                string CustomerBranch = ProcessManagement.GetClientnameName(branchid);
                                String FileName = String.Empty;
                                ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Open Observation Report");
                                DataTable ExcelData = null;
                                DataView view = new System.Data.DataView((DataTable)Session["GridObservationDetailsNEW"]);

                                ExcelData = view.ToTable("Selected", false, "ProcessName", "SubProcessName",
                                   "AuditSteps", "ObservationTitle", "Observation", "Risk", "RootCost",
                                 "FinancialImpact", "Recomendation", "ManagementResponse", "PersonResponsibleName",
                                 "Timeline");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value)
                                        item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                                }
                                FileName = "Draft Observation Report";
                                exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                                exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                                exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                                exWorkSheet.Cells["A2"].Value = customer;
                                exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A2"].Style.Font.Size = 12;
                                
                                exWorkSheet.Cells["A8"].LoadFromDataTable(ExcelData, true);

                                exWorkSheet.Cells["A3"].Value = FileName;
                                exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A3"].AutoFitColumns(50);

                                exWorkSheet.Cells["A4"].Value = "Branch";
                                exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A4"].AutoFitColumns(50);

                                exWorkSheet.Cells["B4"].Value = CustomerBranch;
                                exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B4"].AutoFitColumns(50);

                                exWorkSheet.Cells["A5"].Value = "Financial Year";
                                exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A5"].AutoFitColumns(50);

                                exWorkSheet.Cells["B5"].Value = FinYear;
                                exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B5"].AutoFitColumns(50);

                                exWorkSheet.Cells["A6"].Value = "For Period";
                                exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A6"].AutoFitColumns(50);
                                exWorkSheet.Cells["A6"].Style.Font.Bold = true;

                                exWorkSheet.Cells["B6"].Value = Period;
                                exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B6"].AutoFitColumns(50);

                                exWorkSheet.Cells["A8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A8"].Value = "Process";
                                exWorkSheet.Cells["A8"].AutoFitColumns(40);

                                exWorkSheet.Cells["B8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["B8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B8"].Value = "Sub Process";
                                exWorkSheet.Cells["B8"].AutoFitColumns(25);
                                
                                exWorkSheet.Cells["C8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["C8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["C8"].Value = "Audit Step";
                                exWorkSheet.Cells["C8"].AutoFitColumns(30);
                                
                                exWorkSheet.Cells["D8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["D8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["D8"].Value = "ObservationTitle";
                                exWorkSheet.Cells["D8"].AutoFitColumns(30);

                                exWorkSheet.Cells["E8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["E8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["E8"].Value = "Observation";
                                exWorkSheet.Cells["E8"].AutoFitColumns(30);

                                exWorkSheet.Cells["F8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["F8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["F8"].Value = "Business Implication";
                                exWorkSheet.Cells["F8"].AutoFitColumns(30);

                                exWorkSheet.Cells["G8"].Value = "Root Cause";
                                exWorkSheet.Cells["G8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["G8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["G8"].AutoFitColumns(50);

                                exWorkSheet.Cells["H8"].Value = "Financial Impact";
                                exWorkSheet.Cells["H8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["H8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["H8"].AutoFitColumns(50);

                                exWorkSheet.Cells["I8"].Value = "Recomendation";
                                exWorkSheet.Cells["I8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["I8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["I8"].AutoFitColumns(50);
                                
                                exWorkSheet.Cells["J8"].Value = "Management Response";
                                exWorkSheet.Cells["J8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["J8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["J8"].AutoFitColumns(30);

                                exWorkSheet.Cells["K8"].Value = "Process Owner";
                                exWorkSheet.Cells["K8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["K8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["K8"].AutoFitColumns(30);

                                exWorkSheet.Cells["L8"].Value = "Timeline";
                                exWorkSheet.Cells["L8"].Style.Font.Bold = true;
                                exWorkSheet.Cells["L8"].Style.Font.Size = 12;
                                exWorkSheet.Cells["L8"].AutoFitColumns(30);
                                
                                using (ExcelRange col = exWorkSheet.Cells[8, 1, 8 + ExcelData.Rows.Count, 12])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    using (ExcelRange col1 = exWorkSheet.Cells[8,11, 8 + ExcelData.Rows.Count, 12])
                                    {
                                        col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                    }
                                }
                                
                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                
                                #region Excel Download Code
                                Response.ClearContent();
                                Response.Buffer = true;
                                string fileName = string.Empty;
                                fileName = "ObservationDraft_Reports.xlsx";
                                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                #endregion

                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                    }
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["AOD"]))
            {
                url = Request.QueryString["AOD"];
            }
            if (url == "AOD")
            {
                url = "Type=Process&" + Request.QueryString["Status"];
                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditObservationDraft.aspx");
            }
            if (url == "ASU")
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                int Role = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                {
                    Role = Convert.ToInt32(Request.QueryString["Role"]);
                }
                string Status = string.Empty;
                if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                {
                    Status = Request.QueryString["ReturnUrl1"];
                }
                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + Role + "&AuditID=" + AuditID + "&ReturnUrl1=" + Status);
            }
            if (url == "AMSU")
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                string Status = string.Empty;
                if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                {
                    Status = Request.QueryString["ReturnUrl1"];
                }
                // Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Process&Status=Open");
                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?AuditID=" + AuditID + "&Type=Process&ReturnUrl1=" + Status);
            }
            if (url == "AuditClose")
            {
                // code changed by sagar more on 16-01-2020
                if (!string.IsNullOrEmpty(Session["AuditID"].ToString()) && !string.IsNullOrEmpty(Session["FinancialYear"].ToString()))
                {
                    int AuditID = Convert.ToInt32(Session["AuditID"].ToString());
                    string financialYear = Session["FinancialYear"].ToString();

                    // code comment by sagar more on 21-01-2020
                    //Session.Remove("FinancialYear");
                    //Session.Remove("AuditID");
                    Response.Redirect("~/RiskManagement/InternalAuditTool/AuditClosure_New.aspx?AuditIDForBack=" + AuditID + "&FinancialYearForBack=" + financialYear);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int chkValidationflag = 0;
            foreach (GridViewRow rw in grdObservationList.Rows)
            {
                CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                if (chkBx != null && chkBx.Checked)
                {
                    chkValidationflag = chkValidationflag + 1;
                }
            }
            if (chkValidationflag > 0)
            {
                long branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                int Verticalid = -1;
                long auditId = 0;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditId = Convert.ToInt64(Request.QueryString["AuditID"]);
                }

                string workDone = string.Empty;
                string remark = string.Empty;
                string remarkreview = string.Empty;
                var AuditDetail = UserManagementRisk.GetAuditDatail(auditId);
                if (AuditDetail != null)
                {
                    branchid = AuditDetail.BranchId;
                    FinYear = AuditDetail.FinancialYear;
                    Period = AuditDetail.ForMonth;
                    Verticalid = (int)AuditDetail.VerticalId;

                    foreach (GridViewRow rw in grdObservationList.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        if (chkBx != null && chkBx.Checked)
                        {
                            long ATBDIdValue = 0;
                            string ObservationValue = string.Empty;
                            string ManagementResponseValue = string.Empty;
                            DateTime? TimeLineValue = null;
                            long PersonResponsibleValue;
                            Label lblATBDId = (Label)rw.FindControl("lblATBDId") as Label;
                            TextBox tbxObservation = (TextBox)rw.FindControl("tbxObservation") as TextBox;
                            TextBox tbxManagementResponse = (TextBox)rw.FindControl("tbxManagementResponse") as TextBox;
                            TextBox tbxTimeLine = (TextBox)rw.FindControl("txtStartDate") as TextBox;
                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible") as DropDownList;

                            ATBDIdValue = Convert.ToInt64(lblATBDId.Text);
                            ObservationValue = tbxObservation.Text;
                            ManagementResponseValue = tbxManagementResponse.Text;
                            if (!string.IsNullOrEmpty(tbxTimeLine.Text))
                            {
                                TimeLineValue = Convert.ToDateTime(tbxTimeLine.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                            }

                            PersonResponsibleValue = Convert.ToInt64(ddlPersonResponsible.SelectedValue);

                            InternalControlAuditResult objaudit = new InternalControlAuditResult();
                            objaudit.ATBDId = ATBDIdValue;
                            objaudit.Observation = ObservationValue;
                            objaudit.ManagementResponse = ManagementResponseValue;
                            objaudit.TimeLine = TimeLineValue;
                            objaudit.PersonResponsible = PersonResponsibleValue;
                            objaudit.CustomerBranchId = branchid;
                            objaudit.VerticalID = Verticalid;
                            objaudit.FinancialYear = FinYear;
                            objaudit.ForPerid = Period;
                            objaudit.AuditID = auditId;
                            objaudit.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            objaudit.UpdatedOn = DateTime.Now;
                            RiskCategoryManagement.UpdateObservationData(objaudit);

                            InternalAuditTransaction objPersonResp = new InternalAuditTransaction();
                            objPersonResp.ATBDId = ATBDIdValue;
                            objPersonResp.CustomerBranchId = branchid;
                            objPersonResp.VerticalID = Verticalid;
                            objPersonResp.FinancialYear = FinYear;
                            objPersonResp.ForPeriod = Period;
                            objPersonResp.PersonResponsible = PersonResponsibleValue;
                            objPersonResp.AuditID = auditId;
                            objPersonResp.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            objPersonResp.UpdatedOn = DateTime.Now;
                            UserManagementRisk.UpdatePersonResposible(objPersonResp);
                            BindData();
                        }
                    }
                }
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        
    }
}
