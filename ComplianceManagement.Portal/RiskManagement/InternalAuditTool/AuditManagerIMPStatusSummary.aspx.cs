﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditManagerIMPStatusSummary : System.Web.UI.Page
    {
        protected int AuditID = 0;
        protected static int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P");
                    BindProcess("P");
                }
                else
                {
                    BindData("N");
                    BindProcess("N");
                }                             
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    ViewState["Status"] = Request.QueryString["Status"];
                }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditStatus.PageIndex = chkSelectedPage - 1;            
            grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);            
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");             
            }
            else
            {
                BindData("N");             
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string FinancialYear = string.Empty;
            if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
            {
                FinancialYear = Convert.ToString(Request.QueryString["FY"]);
            }
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            {
                url = Request.QueryString["ReturnUrl1"];
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&FY=" + FinancialYear + "&" + url.Replace("@", "="));
                }
                else
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&" + url.Replace("@", "="));
                }
                    
            }
            else
            {
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&Status=Open&FY=" + FinancialYear + "");
                }
                else
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&Status=Open");
                }
            }            
        }        
        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdAuditStatus.DataSource = null;
                grdAuditStatus.DataBind();               
                BindData("P");                
            }
            else
            {
                grdAuditStatus.DataSource = null;
                grdAuditStatus.DataBind();                
                BindData("N");                
            }
        }       
        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void BindProcess(string Flag)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (Flag == "P")
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.Items.Clear();
                    var AuditLists = ProcessManagement.GetProcessFillDropDown(CustomerId, AuditID);
                    ddlProcess.DataSource = AuditLists;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Preocess Name", "-1"));
                }
                else
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.Items.Clear();
                    var AuditLists = ProcessManagement.GetProcessFillDropDown(CustomerId, AuditID);
                    ddlProcess.DataSource = AuditLists;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Preocess Name", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindData(string Flag)
        {
            try
            {
                int Processid = -1;
                string ProcessIdName = "";
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        Processid = Convert.ToInt32(ddlProcess.SelectedValue);
                        ProcessIdName = ddlProcess.SelectedItem.ToString();
                    }
                }
                if (Flag == "P")
                {
                    var AuditLists = InternalControlManagementDashboardRisk.GetAuditManagerStatusProcessSubProcessUserWiseIMP(CustomerId, Processid, ProcessIdName, AuditID);
                    grdAuditStatus.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Rows.Count;
                    grdAuditStatus.DataBind();
                }
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditStatus.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData("P");            
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {
        }

        protected void grdAuditStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = e.CommandArgument.ToString();

                    string[] arg = Args.ToString().Split(';');
                    
                    string URLStr = string.Empty;
                    string url = "";
                    long AuditID = -1;
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt64(Request.QueryString["AuditID"]);
                    }
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                    }
                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditManagerMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FY=" + arg[4] + "&Period=" + arg[5] + "&VID=" + arg[6] + "&AuditID="+ AuditID + url;                        
                    }
                    else
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditManagerMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FY=" + arg[4] + "&Period=" + arg[5] + "&VID=" + arg[6] + "&AuditID=" + AuditID;
                    }
                    if (Args != "")
                        Response.Redirect(URLStr, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
               
                //Reload the Grid
                BindData("P");                
                               
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                String AuditDetail = String.Empty;
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P");
                }
                else
                {
                    BindData("N");
                }

                var fetchdetailsProcess = InternalControlManagementDashboardRisk.GetAuditinstanceData(AuditID);
                if (fetchdetailsProcess != null)
                {
                    AuditDetail = fetchdetailsProcess.location + "/" + fetchdetailsProcess.FinancialYear + "/" + fetchdetailsProcess.period + "/" + fetchdetailsProcess.verticalName;
                }
               

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Internal Financial Control");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView(grdAuditStatus.DataSource as DataTable);

                        ExcelData = view.ToTable("Selected", false, "ProcessName", "FinancialYear", "ForMonth", "Total", "NotDone", "Submited", "TeamReview", "AuditeeReview", "FinalReview", "Closed");

                        var customername = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));
                        FileName = "Implementation Audit Status Summary";

                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A2"].Value = customername;
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A4"].Value = "Audit:";
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B4"].Value = AuditDetail;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = FileName;
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);
                        

                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A6"].Value = "ProcessName";
                        exWorkSheet.Cells["A6"].AutoFitColumns(15);

                        exWorkSheet.Cells["B6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].Value = "FinancialYear";
                        exWorkSheet.Cells["B6"].AutoFitColumns(25);

                        exWorkSheet.Cells["C6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C6"].Value = "ForMonth";
                        exWorkSheet.Cells["C6"].AutoFitColumns(25);

                        exWorkSheet.Cells["D6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D6"].Value = "Total";
                        exWorkSheet.Cells["D6"].AutoFitColumns(50);

                        exWorkSheet.Cells["E6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E6"].Value = "NotDone";
                        exWorkSheet.Cells["E6"].AutoFitColumns(50);


                        exWorkSheet.Cells["F6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F6"].Value = "Submited";
                        exWorkSheet.Cells["F6"].AutoFitColumns(50);

                        exWorkSheet.Cells["G6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G6"].Value = "TeamReview";
                        exWorkSheet.Cells["G6"].AutoFitColumns(20);

                        exWorkSheet.Cells["H6"].Value = "AuditeeReview";
                        exWorkSheet.Cells["H6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H6"].AutoFitColumns(20);

                        exWorkSheet.Cells["I6"].Value = "FinalReview";
                        exWorkSheet.Cells["I6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I6"].AutoFitColumns(20);

                        exWorkSheet.Cells["J6"].Value = "Closed";
                        exWorkSheet.Cells["J6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J6"].AutoFitColumns(20);


                        using (ExcelRange col = exWorkSheet.Cells[6, 1, 6 + ExcelData.Rows.Count, 10])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }


                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=ImplementationAuditStatusSummary.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void grdAuditStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label chklabl = (Label) e.Row.FindControl("lblProcessName");
                string chk = chklabl.Text.ToString();
                if (chk.Equals("Total"))
                {
                    e.Row.Font.Bold = true;
                    e.Row.Cells[0].Text = "";
                }
            }
        }

       
    }
}