﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmObservationReportWord : System.Web.UI.Page
    {
        protected List<Int32> roles;        
        protected static string AuditHeadOrManagerReport;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindFinancialYear();
                BindLegalEntityData();

                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure> TempAuditClosureResult(int branchid, string Financialyear, int VerticalID, List<int> AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.CustomerbranchId == branchid
                             && row.VerticalID == VerticalID
                             && row.FinancialYear == Financialyear
                             && AuditID.Contains((int)row.AuditID)
                             && row.ACStatus == 1
                             select row).ToList();
                return query.ToList();
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure> TempAuditClosureResult(int branchid, string Financialyear, int VerticalID,int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditClosures
                             where row.CustomerbranchId == branchid
                             && row.VerticalID == VerticalID 
                             && row.FinancialYear == Financialyear
                             && row.AuditID== AuditID
                             && row.ACStatus == 1
                             select row).ToList();
                return query.ToList();
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }

        public string GetUserName(int userid)
        {
            string processnonprocess = "";
            if (userid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(userid);
                    mst_User mstuser = (from row in entities.mst_User
                                        where row.ID == userid 
                                        && row.IsDeleted == false && row.IsActive == true
                                        select row).FirstOrDefault();

                    processnonprocess = mstuser.FirstName + " " + mstuser.LastName;
                }
            }
            return processnonprocess;
        }

        protected void btnExportDoc_Click(object sender, EventArgs e)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure> itemlist = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure>();
                List<int> lstauditid = new List<int>();
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                int VerticalID = -1;
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                if (CustomerBranchId != -1)
                {

                    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (VerticalID != -1)
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                if (AuditHeadOrManagerReport != null)
                                {
                                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                                    {
                                        lstauditid = UserManagementRisk.GetAuditManagerAuditID(CustomerBranchId, Portal.Common.AuthenticationHelper.UserID);
                                        itemlist = TempAuditClosureResult(Convert.ToInt32(CustomerBranchId), ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(VerticalID), lstauditid);
                                    }
                                }
                                else
                                {
                                    lstauditid = UserManagementRisk.GetPerformerReviewerAuditID(CustomerBranchId, Convert.ToInt32(VerticalID), ddlFinancialYear.SelectedItem.Text, Portal.Common.AuthenticationHelper.UserID);
                                    itemlist = TempAuditClosureResult(Convert.ToInt32(CustomerBranchId), ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(VerticalID), lstauditid);
                                }
                                string FinancialYear = string.Empty;
                                string ForPeriod = string.Empty;
                                string CustomerBranchName = string.Empty;
                               
                                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();


                                for (int index = 0; index < itemlist.Count; index++)
                                {
                                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditClosure cat = itemlist[index];
                                    if (cat != null)
                                    {

                                        string ObservationNumber = string.Empty;
                                        string ObservationTitle = string.Empty;
                                        string Observation = string.Empty;
                                        string Risk = string.Empty;
                                        string Recomendation = string.Empty;
                                        string ManagementResponse = string.Empty;
                                        DateTime TimeLine = new DateTime();
                                        string PersonResponsible = string.Empty;
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ObservationNumber)))
                                        {
                                            ObservationNumber = cat.ObservationNumber;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ObservationTitle)))
                                        {
                                            ObservationTitle = cat.ObservationTitle;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                                        {
                                            Observation = cat.Observation;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Risk)))
                                        {
                                            Risk = cat.Risk;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Recomendation)))
                                        {
                                            Recomendation = cat.Recomendation;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                                        {
                                            ManagementResponse = cat.ManagementResponse;
                                        }
                                        if (cat.TimeLine != null)
                                        {
                                            TimeLine = Convert.ToDateTime(cat.TimeLine);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.PersonResponsible)))
                                        {
                                            PersonResponsible = GetUserName(Convert.ToInt32(cat.PersonResponsible));
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                                        {
                                            FinancialYear = Convert.ToString(cat.FinancialYear);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ForPeriod)))
                                        {
                                            ForPeriod = Convert.ToString(cat.ForPeriod);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.CustomerbranchId)))
                                        {
                                            CustomerBranchName = GetCustomerBranchName(Convert.ToInt32(cat.CustomerbranchId));
                                        }
                                        

                                        stringbuilderTOP.Append(@" <style>
                                             .break { page-break-before: always; }
                                            </style>");

                                        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri;'>" +
                                        "<tr><td colspan='1' style='width:20%'><b>Observation No.</b></td><td colspan='3' style='text-align:left;width:80%'>" + ObservationNumber + "</td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><b>" + ObservationTitle + " </b></td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>Observation: </b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Observation + "</td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>Risk: </b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Risk + "</td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>Recommendation:</b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + Recomendation + "</td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>Management Response:</b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + ManagementResponse + "</td></tr> " +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>TimeLine:</b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + TimeLine.ToString("dd-MMM-yyyy") + "</td></tr>" +
                                        "<tr><td colspan='4' style='margin-top:50%'><br><b>Person Responsible:</b></td></tr>" +
                                        "<tr><td colspan='4' style='text-align:justify; margin-top:50%'>" + PersonResponsible + "</td></tr>");

                                        stringbuilderTOP.Append(@"<tr><td colspan='4'><h1 class='break'>&nbsp;</h1></td></tr>");
                                        stringbuilderTOP.Append(@"</table>");
                                    }
                                }
                                ProcessRequest(stringbuilderTOP.ToString(), CustomerBranchName, ForPeriod, FinancialYear, VerticalID);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select financial year.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select vertical.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select financial year.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Sub Unit.";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void ProcessRequest(string context, string CompanyName, string PeriodName, string FinancialYear,int VerticalID)
        {
            if (VerticalID != -1)
            {
                string companyname = CompanyName.Trim();
                string details = "Audit Report for the " + PeriodName + "(" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(companyname + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                var VerticalName = UserManagementRisk.GetVerticalListByID(VerticalID);

                string NameVertical = VerticalName.VerticalName;
                string fnames = companyname + "_" + NameVertical + ".doc";
                fnames = fnames.Replace(',', ' ');
                Response.AppendHeader("Content-Type", "application/msword");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + fnames);
                Response.Write(strBody);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindVertical();
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            string CheckStatus = CustomerManagementRisk.GetAuditHeadOrManagerid(UserID);
            if (CheckStatus == "AM" || CheckStatus == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
                  
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}