﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class PersonResponsibleMainUI : System.Web.UI.Page
    {
        public List<InternalAuditTransactionView> MasterInternalAuditTransactionView = new List<InternalAuditTransactionView>();
        public List<InternalControlAuditAssignment> MasterInternalControlAuditAssignment = new List<InternalControlAuditAssignment>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["PID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["SPID"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["peroid"]))
                                                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                                {
                                                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                                    {
                                                        string statusidasd = Request.QueryString["Status"].ToString();
                                                        ViewState["Arguments"] = Request.QueryString["Status"]
                                                            + ";" + Request.QueryString["ID"]
                                                            + ";" + Request.QueryString["SID"]
                                                            + ";" + Request.QueryString["PID"]
                                                            + ";" + Request.QueryString["SPID"]
                                                            + ";" + Request.QueryString["CustBranchID"]
                                                            + ";" + Request.QueryString["VID"]
                                                            + ";" + Request.QueryString["peroid"]
                                                            + ";" + Request.QueryString["FY"]
                                                            + ";" + Request.QueryString["AuditID"];

                                                        int customerID = -1;
                                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                        BindProcess(customerID, Convert.ToInt32(Request.QueryString["AuditID"]));
                                                        if (Convert.ToInt32(Request.QueryString["PID"]) != -1)
                                                        {
                                                            ddlProcess.ClearSelection();
                                                            ddlProcess.SelectedValue = Request.QueryString["PID"].Trim();
                                                            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                            {
                                                                if (ddlProcess.SelectedValue != "-1")
                                                                {
                                                                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                                                                }
                                                            }
                                                            if (Convert.ToInt32(Request.QueryString["SPID"]) != -1)
                                                            {
                                                                ddlSubProcess.ClearSelection();
                                                                ddlSubProcess.SelectedValue = Request.QueryString["SPID"].Trim();
                                                            }
                                                        }


                                                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                        bindPageNumber();
                                                        if (statusidasd.Equals("AuditeeReview"))
                                                        {
                                                            btnAllSavechk.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            btnAllSavechk.Enabled = false;
                                                        }

                                                        //PageTitle
                                                        string Pname = string.Empty;
                                                        string SPname = string.Empty;
                                                        string VName = string.Empty;
                                                        string Fy = string.Empty;
                                                        string Period1 = string.Empty;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FY"])))
                                                        {
                                                            Fy = "/" + Convert.ToString(Request.QueryString["FY"]);
                                                        }
                                                        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["peroid"])))
                                                        {
                                                            Period1 = "/" + Convert.ToString(Request.QueryString["peroid"]);
                                                        }
                                                        if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                        {
                                                            if (ddlProcess.SelectedValue != "-1")
                                                            {
                                                                Pname = "/" + ddlProcess.SelectedItem.Text;
                                                            }
                                                        }
                                                        if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                                                        {
                                                            if (ddlSubProcess.SelectedValue != "-1")
                                                            {
                                                                SPname = "/" + ddlSubProcess.SelectedItem.Text;
                                                            }
                                                        }
                                                        var BrachName = CustomerBranchManagement.GetByID(Convert.ToInt32(Request.QueryString["CustBranchID"])).Name;
                                                        var VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                                                        if (string.IsNullOrEmpty(VerticalName))
                                                        {
                                                            VName = "/" + VerticalName;
                                                        }
                                                        LblPageDetails.InnerText = BrachName + "/" + Fy + Period1 + Pname + SPname + VName;
                                                        //End
                                                    }
                                                }
            }
        }

        private void BindProcess(int CustomerID, int AuditID)
        {
            try
            {
                ddlSubProcess.Items.Clear();
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownPersonResponsible(CustomerID, Portal.Common.AuthenticationHelper.UserID, AuditID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }


        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());


                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;

            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindDetailView(ViewState["Arguments"].ToString(), "P");

        }
        public string ShowObservation(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string Observation = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                Observation = MstRiskResult.Observation;
            }
            return Observation;
        }

        public string ShowWorkDone(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string workdone = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                workdone = MstRiskResult.ActivityToBeDone;
            }
            return workdone;
        }

        public string ShowRemark(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string remarks = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                remarks = MstRiskResult.FixRemark;
            }
            return remarks;
        }
        protected void grdSummaryDetailsAuditCoverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string statusidasd = Request.QueryString["Status"].ToString();

            if (statusidasd.Equals("AuditeeReview"))
            {
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    chkBx.Enabled = true;
                }
            }
            else
            {
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    chkBx.Enabled = false;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (statusidasd == "TeamReview")
                {
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SID"]);
                    int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    TextBox txtstepId = (TextBox)e.Row.FindControl("ATBDID");
                    Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                    var User = (from row in MasterInternalAuditTransactionView
                                where row.AuditScheduleOnID == ScheduledOnID
                                && row.AuditID == AuditID
                                && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                    var ListPerformer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 3 select row.UserID).FirstOrDefault();
                    var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                    if (User != null)
                    {
                        if (ListPerformer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "black");
                        }
                        else if (ListReviewer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "red");
                        }
                    }
                }
                else if (statusidasd == "AuditeeReview")
                {
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SID"]);
                    int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    TextBox txtstepId = (TextBox)e.Row.FindControl("ATBDID");
                    Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                    var User = (from row in MasterInternalAuditTransactionView
                                where row.AuditScheduleOnID == ScheduledOnID
                                && row.AuditID == AuditID
                                && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                select row).OrderByDescending(el => el.Dated).FirstOrDefault();
                    var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                    if (User != null)
                    {
                        if (ListReviewer == User.CreatedBy)
                        {
                            lblcolor.Style.Add("color", "blue");
                        }
                        else
                        {
                            lblcolor.Style.Add("color", "black");
                        }
                    }
                }
            }
        }


        protected void btnAllsave_click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {

                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        string workDone = string.Empty;
                        string remarkreview = string.Empty;
                        string observatiofinal = string.Empty;

                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        if (chkBx != null && chkBx.Checked)
                        {
                            workDone = ((Label)rw.FindControl("LblWorkDone")).Text;
                            remarkreview = ((Label)rw.FindControl("LblRemark")).Text;
                            observatiofinal = ((Label)rw.FindControl("LblObservation")).Text;

                            LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                            String Args = chklnkbutton.CommandArgument.ToString();
                            string[] arg = Args.ToString().Split(',');
                            int ATBDid = Convert.ToInt32(arg[0]);
                            int customerbranchid = Convert.ToInt32(arg[1]);
                            string FinancialYear = arg[2].ToString();
                            string period = arg[3].ToString();
                            int verticalId = Convert.ToInt32(arg[4]);
                            string AuditStatusID = arg[5].ToString();
                            int AuditID = Convert.ToInt32(arg[6]);

                            long roleid = 4;
                            DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, Convert.ToString(FinancialYear), Convert.ToString(period), Convert.ToInt32(customerbranchid), Convert.ToInt32(ATBDid), Convert.ToInt32(verticalId), AuditID);
                            if (getscheduleondetails != null)
                            {
                                if (RiskCategoryManagement.InternalAuditTransactionsExists(getscheduleondetails.ProcessId, Convert.ToString(FinancialYear), Convert.ToString(period), Convert.ToInt32(customerbranchid), com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 4, Convert.ToInt32(ATBDid), AuditID) == false)
                                {
                                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                    {
                                        AuditScheduleOnID = getscheduleondetails.ID,
                                        ProcessId = getscheduleondetails.ProcessId,
                                        FinancialYear = FinancialYear,
                                        ForPerid = period,
                                        CustomerBranchId = Convert.ToInt32(customerbranchid),
                                        IsDeleted = false,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        ATBDId = Convert.ToInt32(ATBDid),
                                        RoleID = roleid,
                                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                        VerticalID = verticalId,
                                        AuditID = AuditID,
                                    };
                                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                                    {
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                        AuditScheduleOnID = getscheduleondetails.ID,
                                        FinancialYear = FinancialYear,
                                        CustomerBranchId = Convert.ToInt32(customerbranchid),
                                        InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                        ProcessId = getscheduleondetails.ProcessId,
                                        SubProcessId = -1,
                                        ForPeriod = period,
                                        ATBDId = Convert.ToInt32(ATBDid),
                                        RoleID = roleid,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        VerticalID = Convert.ToInt32(verticalId),
                                        AuditID = AuditID,
                                    };
                                    string remark = string.Empty;
                                    transaction.StatusId = 6;
                                    MstRiskResult.AStatusId = 6;
                                    remark = "Audit Steps Under Auditee Review.";

                                    var MstRiskResultchk = RiskCategoryManagement.GetInternalControlAuditResultbyID(getscheduleondetails.ID, Convert.ToInt32(ATBDid), verticalId, AuditID);
                                    if (MstRiskResultchk != null)
                                    {
                                        if (string.IsNullOrEmpty(MstRiskResultchk.AuditObjective))
                                            MstRiskResult.AuditObjective = null;
                                        else
                                            MstRiskResult.AuditObjective = MstRiskResultchk.AuditObjective;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.AuditSteps))
                                            MstRiskResult.AuditSteps = "";
                                        else
                                            MstRiskResult.AuditSteps = MstRiskResultchk.AuditSteps;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.AnalysisToBePerofrmed))
                                            MstRiskResult.AnalysisToBePerofrmed = "";
                                        else
                                            MstRiskResult.AnalysisToBePerofrmed = MstRiskResultchk.AnalysisToBePerofrmed;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                            MstRiskResult.ProcessWalkthrough = "";
                                        else
                                            MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ActivityToBeDone))
                                            MstRiskResult.ActivityToBeDone = "";
                                        else
                                            MstRiskResult.ActivityToBeDone = MstRiskResultchk.ActivityToBeDone;
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                            MstRiskResult.Population = "";
                                        else
                                            MstRiskResult.Population = MstRiskResultchk.Population;
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                            MstRiskResult.Sample = "";
                                        else
                                            MstRiskResult.Sample = MstRiskResultchk.Sample;

                                        if (string.IsNullOrEmpty(MstRiskResultchk.ObservationNumber))
                                        {
                                            MstRiskResult.ObservationNumber = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.ObservationTitle))
                                        {
                                            MstRiskResult.ObservationTitle = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Observation))
                                        {
                                            MstRiskResult.Observation = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Observation = MstRiskResultchk.Observation;
                                        }
                                        if (MstRiskResultchk.ISACPORMIS != null)
                                        {
                                            MstRiskResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                        }
                                        else
                                        {
                                            MstRiskResult.ISACPORMIS = null;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Risk))
                                        {
                                            MstRiskResult.Risk = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Risk = MstRiskResultchk.Risk;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.RootCost))
                                        {
                                            MstRiskResult.RootCost = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.RootCost = MstRiskResultchk.RootCost;
                                        }

                                        if (MstRiskResultchk.AuditScores == null)
                                            MstRiskResult.AuditScores = null;
                                        else
                                            MstRiskResult.AuditScores = Convert.ToDecimal(MstRiskResultchk.AuditScores);

                                        if (MstRiskResultchk.FinancialImpact == null)
                                        {
                                            MstRiskResult.FinancialImpact = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.Recomendation))
                                        {
                                            MstRiskResult.Recomendation = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.Recomendation = MstRiskResultchk.Recomendation;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.ManagementResponse))
                                        {
                                            MstRiskResult.ManagementResponse = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                        }
                                        if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                            MstRiskResult.FixRemark = "";
                                        else
                                            MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;

                                        DateTime dt = new DateTime();
                                        if (MstRiskResultchk.TimeLine != null)
                                        {
                                            string s = MstRiskResultchk.TimeLine != null ? MstRiskResultchk.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                                            dt = DateTime.ParseExact(s, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            MstRiskResult.TimeLine = dt.Date;
                                        }
                                        else
                                        {
                                            MstRiskResult.TimeLine = null;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.PersonResponsible)))
                                        {
                                            if (MstRiskResultchk.PersonResponsible == -1)
                                            {
                                                MstRiskResult.PersonResponsible = null;
                                                transaction.PersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                transaction.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.Owner)))
                                        {
                                            if (MstRiskResultchk.Owner == -1)
                                            {
                                                MstRiskResult.Owner = null;
                                                transaction.Owner = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                transaction.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationRating)))
                                        {
                                            if (MstRiskResultchk.ObservationRating == -1)
                                            {
                                                transaction.ObservatioRating = null;
                                                MstRiskResult.ObservationRating = null;
                                            }
                                            else
                                            {
                                                transaction.ObservatioRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                MstRiskResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationCategory)))
                                        {
                                            if (MstRiskResultchk.ObservationCategory == -1)
                                            {
                                                transaction.ObservationCategory = null;
                                                MstRiskResult.ObservationCategory = null;
                                            }
                                            else
                                            {
                                                transaction.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                MstRiskResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationSubCategory)))
                                        {
                                            if (MstRiskResultchk.ObservationSubCategory == -1)
                                            {
                                                transaction.ObservationSubCategory = null;
                                                MstRiskResult.ObservationSubCategory = null;
                                            }
                                            else
                                            {
                                                transaction.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                MstRiskResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                            }
                                        }
                                    }
                                    if (workDone.Trim() == "")
                                    {
                                        MstRiskResult.ActivityToBeDone = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.ActivityToBeDone = workDone.ToString();
                                    }
                                    if (remarkreview.Trim() == "")
                                    {
                                        MstRiskResult.FixRemark = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.FixRemark = remarkreview.ToString();
                                    }
                                    if (observatiofinal.Trim() == "")
                                    {
                                        MstRiskResult.Observation = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.Observation = observatiofinal.ToString();
                                    }

                                    //Code added by Sushant
                                    if (MstRiskResultchk.BodyContent == "")
                                    {
                                        MstRiskResult.BodyContent = "";
                                        transaction.BodyContent = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.BodyContent = MstRiskResultchk.BodyContent;
                                        transaction.BodyContent = MstRiskResultchk.BodyContent;
                                    }
                                    if (MstRiskResultchk.UHComment == "")
                                    {
                                        MstRiskResult.UHComment = "";
                                        transaction.UHComment = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.UHComment = MstRiskResultchk.UHComment;
                                        transaction.UHComment = MstRiskResultchk.UHComment;
                                    }

                                    if (MstRiskResultchk.PRESIDENTComment == "")
                                    {
                                        MstRiskResult.PRESIDENTComment = "";
                                        transaction.PRESIDENTComment = "";
                                    }
                                    else
                                    {
                                        MstRiskResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                        transaction.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                    }

                                    if (MstRiskResultchk.UHPersonResponsible != null)
                                    {
                                        MstRiskResult.UHPersonResponsible = null;
                                        transaction.UHPersonResponsible = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                        transaction.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                    }
                                    if (!string.IsNullOrEmpty(MstRiskResultchk.BriefObservation))
                                    {
                                        MstRiskResult.BriefObservation = MstRiskResultchk.BriefObservation;
                                        transaction.BriefObservation = MstRiskResultchk.BriefObservation;
                                    }
                                    else
                                    {
                                        MstRiskResult.BriefObservation = null;
                                        transaction.BriefObservation = null;
                                    }
                                    if (!string.IsNullOrEmpty(MstRiskResultchk.ObjBackground))
                                    {
                                        MstRiskResult.ObjBackground = MstRiskResultchk.ObjBackground;
                                        transaction.ObjBackground = MstRiskResultchk.ObjBackground;
                                    }
                                    else
                                    {
                                        MstRiskResult.ObjBackground = null;
                                        transaction.ObjBackground = null;
                                    }
                                    if (MstRiskResultchk.UHPersonResponsible != null)
                                    {
                                        MstRiskResult.PRESIDENTPersonResponsible = null;
                                        transaction.PRESIDENTPersonResponsible = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                        transaction.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                    }
                                    //End



                                    transaction.Remarks = remark.Trim();
                                    bool Success1 = false;
                                    bool Success2 = false;
                                    if (!string.IsNullOrEmpty(observatiofinal) && MstRiskResult.TimeLine == null)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Provide Management Response and Timeline Before Save";
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                        {
                                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            transaction.CreatedOn = DateTime.Now;
                                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                        }
                                        else
                                        {
                                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            transaction.CreatedOn = DateTime.Now;
                                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                        }
                                        //if (Success1 == true && Success2 == true)
                                        //{
                                        //    ProcessReminderOnAuditeeReviewReviewer(Convert.ToString(getscheduleondetails.ForMonth), Convert.ToString(getscheduleondetails.FinancialYear), Convert.ToInt32(getscheduleondetails.ID), Convert.ToInt32(getscheduleondetails.ProcessId), AuditID);
                                        //}
                                        //else
                                        //{
                                        //    cvDuplicateEntry.IsValid = false;
                                        //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                        //}
                                    }
                                }
                            }
                        }
                    }
                    BindDetailView(ViewState["Arguments"].ToString(), "P");
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one step.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void ProcessReminderOnAuditeeReviewReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid,int AuditID)
        //{
        //    try
        //    {
        //        long userid = GetReviewer(ForPeriod, FinancialYear, ScheduledOnID, processid, AuditID);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;                   
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(customerID).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditeeReviewAuditReviewer
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Auditee Review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //public static long GetReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid,int AuditID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var statusList = (from row in entities.InternalControlAuditAssignments
        //                          join row1 in entities.InternalAuditScheduleOns
        //                          on row.ProcessId equals row1.ProcessId
        //                          where row.AuditID == row1.AuditID
        //                          && row.InternalAuditInstance == row1.InternalAuditInstance
        //                          && row.AuditID == AuditID
        //                          && row.ProcessId == processid &&
        //                          row1.ForMonth == ForPeriod
        //                          && row1.FinancialYear == FinancialYear
        //                          && row1.ID == ScheduledOnID
        //                          && row.RoleID == 4
        //                          select row.UserID).FirstOrDefault();

        //        return (long)statusList;
        //    }
        //}
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url4 = "";
            int PID = -1;
            int SPID = -1;
            int PageSize = -1;
            int gridpagesize = -1;
            int AuditID = -1;
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["chkPID"]))
            {
                PID = Convert.ToInt32(Request.QueryString["chkPID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["chkSPID"]))
            {
                SPID = Convert.ToInt32(Request.QueryString["chkSPID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]))
            {
                PageSize = Convert.ToInt32(Request.QueryString["PageSize"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["gridpagesize"]))
            {
                gridpagesize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
            {
                url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"] + "&PID=" + PID + "&SPID=" + SPID + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&AuditID=" + AuditID;
            }
            Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsibleStatusSummary.aspx?" + url4);
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int atbdid = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                int AuditStatusID = -1;
                int AuditID = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    atbdid = Convert.ToInt32(commandArgs[0]);
                    ViewState["ATBDID"] = null;
                    ViewState["ATBDID"] = atbdid;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[4]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                if (!string.IsNullOrEmpty(commandArgs[5]))
                {
                    AuditStatusID = Convert.ToInt32(commandArgs[5]);
                    ViewState["AuditStatusID"] = null;
                    ViewState["AuditStatusID"] = AuditStatusID;
                }
                if (!string.IsNullOrEmpty(commandArgs[6]))
                {
                    AuditID = Convert.ToInt32(commandArgs[6]);
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + atbdid + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "','" + VerticalId + "'," + AuditStatusID + "," + AuditID + ");", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }

        public bool ATBDVisibleorNot(int? StatusID)
        {
            if (StatusID != 6)
                return false;
            else
                return true;
        }

        private void BindDetailView(string Arguments, String IFlag)
        {
            int ProcessId = -1;
            int Subprocessid = -1;
            int BranchID = -1;
            string financialyear = String.Empty;
            string filter = String.Empty;
            string Peroid = string.Empty;
            int VerticalID = -1;
            int InstanceID = -1;
            long auditID = -1;
            int customerID = -1;

            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int userid = -1;
            userid = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

            string[] arg = Arguments.ToString().Split(';');

            if (arg.Length > 0)
            {
                InstanceID = Convert.ToInt32(arg[1]);
                ProcessId = Convert.ToInt32(arg[3]);
                Subprocessid = Convert.ToInt32(arg[4]);
                BranchID = Convert.ToInt32(arg[5]);
                VerticalID = Convert.ToInt32(arg[6]);
                Peroid = arg[7].ToString();
                financialyear = arg[8].ToString();

                List<int> statusIds = new List<int>();
                List<int?> statusNullableIds = new List<int?>();

                filter = arg[0].Trim();

                if (arg[0].Trim().Equals("NotDone"))
                {
                    statusIds.Add(-1);
                }
                else if (arg[0].Trim().Equals("Submited"))
                {
                    statusIds.Add(2);
                }
                else if (arg[0].Trim().Equals("TeamReview"))
                {
                    statusIds.Add(4);
                }
                else if (arg[0].Trim().Equals("Closed"))
                {
                    statusIds.Add(3);
                }
                else if (arg[0].Trim().Equals("AuditeeReview"))
                {
                    statusIds.Add(6);
                }
                else if (arg[0].Trim().Equals("FinalReview"))
                {
                    statusIds.Add(5);
                }
                else
                {
                    statusIds.Add(1);
                    statusIds.Add(4);
                    statusIds.Add(2);
                    statusIds.Add(3);
                    statusIds.Add(5);
                    statusIds.Add(6);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt64(Request.QueryString["AuditID"]);
                }
                if (auditID != -1 && auditID != 0)
                {
                    if (IFlag == "P")
                    {
                        //var detailView = InternalControlManagementDashboardRisk.GetPersonResponsibleAuditSteps(userid, customerID, BranchID, VerticalID, financialyear, ProcessId, Subprocessid, InstanceID, statusIds, statusNullableIds, filter, Peroid, auditID);
                        //grdSummaryDetailsAuditCoverage.DataSource = detailView;
                        //Session["TotalRows"] = detailView.Count;
                        //grdSummaryDetailsAuditCoverage.DataBind();
                    }
                }
                //GetPageDisplaySummary();
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}

                //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        //BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        //BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}