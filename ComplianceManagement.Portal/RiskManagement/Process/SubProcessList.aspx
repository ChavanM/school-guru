﻿<%@ Page Title="Sub Process List" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="SubProcessList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process.SubProcessList" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">
        function fopenpopup() {
            $('#SubProcessesList').modal('show');
            return true;
        }
        //This is used for Close Popup after save or update data.
        function CloseWin() {
            $('#SubProcessesList').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller() {
            setInterval(CloseWin, 3000000);
        };

        $(document).ready(function () {
            //setactivemenu('Business Process');
            $('#ContentPlaceHolder1_dlBreadcrumb').hide();
            if ($('#ContentPlaceHolder1_dlBreadcrumb').text().trim() != '') {
                fhead('Sub Process List / ' + $('#ContentPlaceHolder1_dlBreadcrumb').text().trim());
            } else {
                fhead('Sub Process List');
            }
        });

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        };

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <asp:UpdatePanel ID="upSubProcessList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-2 colpadding0" style="width:23%">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5" Selected="True" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; display:none;">                     
                                <div class="col-md-2 colpadding0" >
                                <p style="color: #999; margin-top: 5px;"> Filter</p>
                                </div>                      
                                <asp:TextBox runat="server" ID="tbxFilter" MaxLength="50" AutoPostBack="true" CssClass="form-control"    Style="width: 150px;"
                                    OnTextChanged="tbxFilter_TextChanged" />
                            </div>

                             <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;text-align: right"></div>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;text-align: right; float: right;">
                                  <asp:LinkButton Text="Back" runat="server" ID="lnkBackToProcess" CssClass="btn btn-primary" OnClick="lnkBackToProcess_Click"/>
                                  <asp:LinkButton Text="Add New" runat="server" OnClientClick="fopenpopup()" ID="btnAddSubEvent" CssClass="btn btn-primary" OnClick="btnAddSubEvent_Click" Visible="false" />
                             </div>
                   
                            <div style="text-align:center;display:none">
                                <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                    RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                            runat="server" Style="text-decoration: none; color: Black" />
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                                    <SeparatorStyle Font-Size="12" />
                                    <SeparatorTemplate>
                                        &gt;
                                    </SeparatorTemplate>
                                </asp:DataList>
                            </div>
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdSubProcessList" AutoGenerateColumns="false"
                                    OnSorting="grdSubProcessList_Sorting" AllowSorting="true"
                                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                    DataKeyNames="ID" OnRowCommand="grdSubProcessList_RowCommand" OnRowDataBound="grdSubProcessList_RowDataBound"
                                    OnPageIndexChanging="grdSubProcessList_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Process">
                                            <ItemTemplate>                                                
                                                <asp:Label  runat="server"  Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sub Process">
                                            <ItemTemplate>                                                
                                                <asp:Label  runat="server"  Text='<%# Eval("SubProcessName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Scores" Visible="false">
                                            <ItemTemplate>                                                
                                                <asp:Label  runat="server"  Text='<%# Eval("Scores") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField  HeaderText="Sub Activity">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>'>sub-activity</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                                                                     
                                        <asp:TemplateField ItemStyle-Width="7%" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_SUBPROCESS" OnClientClick="fopenpopup()" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Sub-unit" data-toggle="tooltip" data-placement="top" title="Edit Sub-unit" /></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SUBPROCESS" Visible="false" CommandArgument='<%# Eval("ID") %>'
                                                    OnClientClick="return confirm('This will also delete all the sub-process associated with current sub process. Are you certain you want to delete this sub process?');"><img src="../../Images/delete_icon_new.png"  alt="Disable Sub-unit" data-toggle="tooltip" data-placement="top" title="Disable Sub-unit" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>                                     
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Sub Process Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div style="float: right;">
                                      <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                          class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                      </asp:DropDownListChosen>  
                                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p>
                                                <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                
                                        <div class="table-paging-text" style="float:right">
                                            <p>Page</p>
                                        </div>                                
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                           
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="SubProcessesList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 55%; height: 75%; margin-top:100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divSubProcessDialog">
                        <asp:UpdatePanel ID="upSubEvents" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="SubEventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="SubEventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div style="margin-bottom: 5px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">
                                            Status</label>
                                        <span style="color: black; font-size: 13px; margin-left: 15.5%">&nbsp;&nbsp;<asp:Literal ID="litstatus" runat="server" /></span>
                                    </div>

                                    <div style="margin-bottom: 5px">
                                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                         <label style="display: block; float: left; font-size: 13px; color: #333;">
                                            Process</label>
                                        <span style="color: black; font-size: 13px; margin-left: 14%">&nbsp;&nbsp;<asp:Literal ID="litCustomer" runat="server" /></span>
                                    </div>                                 
                                    <div style="margin-bottom: 5px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" class="form-control" Style="width: 65%; margin-left: 23%"
                                             MaxLength="100" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="SubEventValidationGroup" Display="None" />
                                    </div>

                                    <div style="margin-bottom: 5px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" class="form-control" MaxLength="500" 
                                            Style="width: 65%; margin-left: 23%" ToolTip="" TextMode="MultiLine" />
                                    </div>

                                     <div style="margin-bottom: 5px; display:none;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">
                                            Scores</label>
                                        <asp:TextBox runat="server" ID="txtScores" class="form-control" onkeypress="return allowOnlyNumber(event);" onpaste="return false;" Style="width: 16%; margin-left: 23%"/>
                                        
                                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="SubEventValidationGroup"
                                        ErrorMessage="Please enter a valid scores." ControlToValidate="txtScores"
                                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    </div>

                                    <div style="margin-bottom: 5px; margin-left: 35%; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="SubEventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>

                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>

                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
