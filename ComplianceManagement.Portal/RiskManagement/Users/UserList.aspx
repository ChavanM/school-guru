﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Users.UserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('#divUsersDialog').dialog({
                height: 580,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "User Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox(flag) {

            if (flag == 1) {
                $("#<%= ddlRole.ClientID %>").combobox();
                $("#<%= ddlCustomer.ClientID %>").combobox();
                $("#<%= ddlDepartment.ClientID %>").combobox();
            }
        }

        function disableCombobox() {

            $(".custom-combobox").attr('disabled', 'disabled');
        }

        function initializeJQueryUI() {

            $("#<%= tbxBranch.ClientID %>").unbind('click');

        $("#<%= tbxBranch.ClientID %>").click(function () {
            $("#divBranches").toggle("blind", null, 500, function () { });
        });
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom">
                        <div id="divCustomerfilter" runat="server" style="margin-left: 300px">
                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: White; margin-bottom: -5px;">
                                Select Customer :</label>
                            <div style="width: 150px; float: left; margin-top: -15px; margin-left: 109px;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td align="right" style="width: 65%; padding-right: 60px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdUser_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdUser_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging" OnRowDataBound="grdUser_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="FirstName" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                        <asp:BoundField DataField="ContactNumber" HeaderText="Contact No." ItemStyle-HorizontalAlign="Center" SortExpression="ContactNumber" />
                        <asp:BoundField DataField="Role" HeaderText="Role" />
                        <asp:TemplateField ItemStyle-Width="60px" HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                    ToolTip="Click to toggle the status..." OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField ItemStyle-Width="150px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnModifyAssignment" runat="server" CommandName="MODIFY_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                ToolTip="Modify assignments for this user...">Modify Assignments</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="130px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnEventAssignment" runat="server" CommandName="MODIFY_EVENT_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                ToolTip="Modify event assignments for this user...">Modify Events</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit User" title="Edit User" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                    OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../Images/delete_icon.png" alt="Delete User" title="Delete User" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to reset password for this user?');"><img src="../Images/reset_password.png" alt="Reset Password" title="Reset Password" /></asp:LinkButton>
                                <%--<asp:LinkButton runat="server" CommandName="UNLOCK_USER" CommandArgument='<%# Eval("ID") %>'
                                Visible='<%# IsLocked((string)Eval("Email")) %>'><img src="../Images/permissions_icon.png" alt="Unblock User" title="Unblock User" /></asp:LinkButton>--%>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUsersDialog">
        <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="UserValidationGroup" />

                        <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                            ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            First Name</label>
                        <asp:TextBox runat="server" ID="tbxFirstName" Style="height: 16px; width: 390px;"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid first name."
                            ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Last Name</label>
                        <asp:TextBox runat="server" ID="tbxLastName" Style="height: 16px; width: 390px;"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Last Name can not be empty."
                            ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid last name."
                            ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Designation</label>
                        <asp:TextBox runat="server" ID="tbxDesignation" Style="height: 16px; width: 390px;"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Designation can not be empty."
                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid designation."
                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>

                    <div runat="server" id="divDepartment" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Department</label>
                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Email</label>
                        <asp:TextBox runat="server" ID="tbxEmail" Style="height: 16px; width: 390px;" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid email."
                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Contact No</label>
                        <asp:TextBox runat="server" ID="tbxContactNo" Style="height: 16px; width: 390px;"
                            MaxLength="32" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid contact number."
                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Role</label>
                        <asp:DropDownList runat="server" ID="ddlRole" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divCustomer" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select customer."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divCustomerBranch" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                            <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please select Location."
                            ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserValidationGroup" InitialValue="< Select Location >"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Address</label>
                        <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                            TextMode="MultiLine" />
                    </div>
                    <asp:Repeater runat="server" ID="repParameters">
                        <ItemTemplate>
                            <div style="margin-bottom: 7px">
                                <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                    Text='<%# Eval("Name")  + ":"%>' />
                                <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                    MaxLength='<%# Eval ("Length") %>' />
                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                                <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="UserValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUsersDialog').dialog('close');" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
