﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditManagerDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.AuditManagerDashboard" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <!-- Offline-->
    <script type="text/javascript" src="../../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="../../avantischarts/highcharts/js/modules/exporting.js"></script>--%>
    <script type="text/javascript" src="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width:90% !important;            
        }
        div.dd_chk_drop {
                top: 32px !important; 
        }
    </style>
    
    <style type="text/css">

        #ContentPlaceHolder1_grdAuditTrackerSummary.table tr td {
            border: 1px solid white;
        }

         .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            /*background: #fafafa;*/
            border: 2px solid #ddd;
        }
    </style>

    <style type="text/css">

                ol.progtrckr {
            margin: 0;
            padding: 0;
            list-style-type: none;
            }

            ol.progtrckr li {
            display: inline-block;
            /*text-align: center;*/
            line-height: 3em;
            }

            ol.progtrckr[data-progtrckr-steps="2"] li { width: 25%; }
            ol.progtrckr[data-progtrckr-steps="3"] li { width: 25%; }
            ol.progtrckr[data-progtrckr-steps="4"] li { width: 25%; }
            /*ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
            ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
            ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
            ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
            ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }*/

            ol.progtrckr li.progtrckr-done {
            color: black;
            border-bottom: 4px solid yellowgreen;
            }

            ol.progtrckr li.progtrckr-closed {
            color: black;
            border-bottom: 0px solid yellowgreen;
            width:0%;   
            }

            ol.progtrckr li.progtrckr-todo {
            color: silver; 
            border-bottom: 4px solid silver;
            }

            ol.progtrckr li.progtrckr-todo-closed {
            color: silver; 
            border-bottom: 0px solid silver;
            width:0%; 
            }

            ol.progtrckr li.progtrckr-current {
            color: black; 
            border-bottom: 4px solid silver;
            }

            ol.progtrckr li:after {
            content: "\00a0\00a0";
            }

            ol.progtrckr li:before {
            position: relative;
            bottom: -2.5em;
            float: left;
            /*left: 50%;*/
            line-height: 1em;
            }

            ol.progtrckr li.progtrckr-closed:before {
            content: "\2714";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-done:before {
            content: "\2714";
            color: white;
            background-color: yellowgreen;
            height: 1.2em;
            width: 1.2em;
            line-height: 1.2em;
            border: none;
            border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-todo:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-todo-closed:before {
            content: "\039F";
            color: silver;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-current:before {
            content: "\039F";
            color: #A16BBE;
            background-color: white;
            font-size: 1.5em;
            bottom: -1.6em;
            }
            .border-box {
            border: 1px solid #1fd9e1;
        }
        .info-box .count {
            font-size: 40px !important;
        }

        .col-lg-3 {
            width: 22% !important;
        }
    </style>

    <script type="text/javascript">

        function hideAdvanceSearch() {
            
            var div = document.getElementById('DivAdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        //function ShowObservationDetails(Type, customerid, branchid, finYear, ObsID, PID, UserType) {
            
        //    $('#DivReports').modal('show');
        //    $('#showdetails').attr('width', '1250px');
        //    $('#showdetails').attr('height', '600px');
        //    $('.modal-dialog').css('width', '1300px');
        //    $('#showdetails').attr('src', "../../Management/ObservationDetails.aspx?Type=" + Type + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + finYear + "&UType=" + UserType + "&ObsID=" + ObsID + "&PID=" + PID);
        //}

        function ShowObservationDetails(Type, customerid, branchid, finYear, ObjCatName, PName, UserType, subEntityOneIdCommaSeparatedList, SubEntityTwoIdCommaSeparatedList, SubEntityThreeIdCommaSeparatedList, FilterLocationIdCommaSeparatedList, VerticalID, statusListwithComma) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1250px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1300px');
            $('#showdetails').attr('src', "../../Management/ObservationDetails.aspx?Type=" + Type + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + finYear + "&UType=" + UserType + "&ObjCatName=" + ObjCatName + "&PName=" + PName + "&subEntityOneIdCommaSeparatedList=" + subEntityOneIdCommaSeparatedList + "&SubEntityTwoIdCommaSeparatedList=" + SubEntityTwoIdCommaSeparatedList + "&SubEntityThreeIdCommaSeparatedList=" + SubEntityThreeIdCommaSeparatedList + "&FilterLocationIdCommaSeparatedList=" + FilterLocationIdCommaSeparatedList + "&VerticalID=" + VerticalID + "&statusListwithComma=" + statusListwithComma);
        }

        //function PlannedActualAuditDetails(Type, branchid, Period, FROMDate, ToDate, verticalID, UserType) {
         
        //    $('#DivReports').modal('show');
        //    $('#showdetails').attr('width', '1250px');
        //    $('#showdetails').attr('height', '580px');
        //    $('.modal-dialog').css('width', '1300px');
        //    $('#showdetails').attr('src', "../../RiskManagement/Common/PlannedActualAuditDetails.aspx?Type=" + Type + "&branchid=" + branchid + "&Period=" + Period + "&FDate=" + FROMDate + "&TDate=" + ToDate + "&VID=" + verticalID + "&UType=" + UserType);
        //}

        function PlannedActualAuditDetails(Type, branchid, Period, FROMDate, ToDate, verticalID, CustomerIdsList, FinancialYearList, UserType, subEntityOneIdCommaSeparatedList, SubEntityTwoIdCommaSeparatedList, SubEntityThreeIdCommaSeparatedList, FilterLocationIdCommaSeparatedList) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1250px');
            $('#showdetails').attr('height', '580px');
            $('.modal-dialog').css('width', '1300px');
            $('#showdetails').attr('src', "../../RiskManagement/Common/PlannedActualAuditDetails.aspx?Type=" + Type + "&branchid=" + branchid + "&Period=" + Period + "&FDate=" + FROMDate + "&TDate=" + ToDate + "&VerticalID=" + verticalID + "&UType=" + UserType + "&CustomerIdsList=" + CustomerIdsList + "&FinancialYearList=" + FinancialYearList + "&subEntityOneIdCommaSeparatedList=" + subEntityOneIdCommaSeparatedList + "&SubEntityTwoIdCommaSeparatedList=" + SubEntityTwoIdCommaSeparatedList + "&SubEntityThreeIdCommaSeparatedList=" + SubEntityThreeIdCommaSeparatedList + "&FilterLocationIdCommaSeparatedList=" + FilterLocationIdCommaSeparatedList);
        }
        
        //function ShowOpenObservationDetails(Type, customerid, branchid, finYear, Period, PID, verticalID, UserType) {
        //    $('#DivReports').modal('show');
        //    $('#showdetails').attr('width', '1250px');
        //    $('#showdetails').attr('height', '600px');
        //    $('.modal-dialog').css('width', '1300px');
        //    $('#showdetails').attr('src', "../../RiskManagement/Common/OpenObservationDetails.aspx?Type=" + Type + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + finYear + "&Period=" + Period + "&PID=" + PID + "&VID=" + verticalID + "&UType=" + UserType);
        //}

        function ShowOpenObservationDetails(Type, customerid, branchid, finYear, Period, PName, verticalID, UserType, subEntityOneIdCommaSeparatedList, SubEntityTwoIdCommaSeparatedList, SubEntityThreeIdCommaSeparatedList, FilterLocationIdCommaSeparatedList, statusListwithComma) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1250px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1300px');
            $('#showdetails').attr('src', "../../RiskManagement/Common/OpenObservationDetails.aspx?Type=" + Type + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + finYear + "&Period=" + Period + "&PName=" + PName + "&VID=" + verticalID + "&UType=" + UserType + "&subEntityOneIdCommaSeparatedList=" + subEntityOneIdCommaSeparatedList + "&SubEntityTwoIdCommaSeparatedList=" + SubEntityTwoIdCommaSeparatedList + "&SubEntityThreeIdCommaSeparatedList=" + SubEntityThreeIdCommaSeparatedList + "&FilterLocationIdCommaSeparatedList=" + FilterLocationIdCommaSeparatedList + "&statusListwithComma=" + statusListwithComma);
        }

        function statusclick(customerId, custBranchId, forMonth, financialYear, verticalId, auditId) {

            var navigateFromDashboard = "navigateFromDashboard";
            var ReturnUrl1 = "Status@Open";
            var url = "../../RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?customerId=" + customerId + "&AuditID=" + auditId + "&FY=" + financialYear + "&navigateFromDashboard=" + navigateFromDashboard + "&ReturnUrl1=" + ReturnUrl1;
            location.href = url;
            //
            //console.log(CustomerBranchID);
            //location.href = "https://google.com";
            return false;
        }

        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }       

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

        var ObservationStatusColorScheme = {
            Major: "<%=highcolor.Value %>",
            Moderate: "<%=mediumcolor.Value %>",
            Minor: "<%=lowcolor.Value %>"
        };

         $(document).ready(function () {      
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Observation Category Wise Observation Status
                var ObservationStatusColumnChart = Highcharts.chart('DivGraphObsStatus', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 'Click the columns to view details.'
                    },
                    xAxis: {
                        type: 'category',
                        title: {
                            text: 'Observation Category'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Nos. of Observations'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            showInLegend: true,
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                                //format: '{point.y:.1f}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                    },


                    <%=ObservationStatusChart%>

                });                
            });
         });


         $(document).ready(function () {        
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

               //Process Wise Observation Status                             
                
                    <%=ProcessWiseObservationStatusChart%>   
            });
         });


         $(document).ready(function () {        
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });
                
                <%=ObservationProcessWiseAgingChart%>   
               
            });
         });

        $(document).ready(function () {
            $("[name='ContentPlaceHolder1_ddlProcessMultiSelect_sll']").click(function () {
                
                var count = '<%= Session["ProcessCount"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlProcessMultiSelect_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlProcessMultiSelect_" + i).prop("checked", false);
                    }
                }
            });

            $("[name='ContentPlaceHolder1_ddlLegalEntityMultiSelect_sll']").click(function () {
                
                var count = '<%= Session["LegalEntityCount"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlLegalEntityMultiSelect_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlLegalEntityMultiSelect_" + i).prop("checked", false);
                    }
                }
            });

            $("[name='ContentPlaceHolder1_ddlSubEntity1_sll']").click(function () {
                
                var count = '<%= Session["SubEntity1Count"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity1_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity1_" + i).prop("checked", false);
                    }
                }
            });

            $("[name='ContentPlaceHolder1_ddlSubEntity2_sll']").click(function () {
                
                var count = '<%= Session["SubEntity2Count"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity2_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity2_" + i).prop("checked", false);
                    }
                }
            });

            $("[name='ContentPlaceHolder1_ddlSubEntity3_sll']").click(function () {
                
                var count = '<%= Session["SubEntity3Count"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity3_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlSubEntity3_" + i).prop("checked", false);
                    }
                }
            });
            
            $("[name='ContentPlaceHolder1_ddlFilterLocation_sll']").click(function () {
                
                var count = '<%= Session["FilterLocationCount"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlFilterLocation_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlFilterLocation_" + i).prop("checked", false);
                    }
                }
            });
            
            $("[name='ContentPlaceHolder1_ddlFinancialYearMultiSelect_sll']").click(function () {
                
                var count = '<%= Session["FinancialYearCount"] %>';
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlFinancialYearMultiSelect_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlFinancialYearMultiSelect_" + i).prop("checked", false);
                    }
                }
            });
            
            $("[name='ContentPlaceHolder1_ddlStatusList_sll']").click(function () {
                
                var count = 3;
                if ($(this).prop("checked")) {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlStatusList_" + i).prop("checked", true);
                    }
                    
                } else {
                    for (var i = 0; i < count; i++) {
                        $("#ContentPlaceHolder1_ddlStatusList_" + i).prop("checked", false);
                    }
                }
            });
        });
      
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="wrapper">
                <div class="row1" style="background:none;">
                    <div class="dashboard1">                                             
                   <div id="DivFilters" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Filters </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivFilters"><i class="fa fa-chevron-down"></i></a>
                                        <a href="javascript:closeDiv('DivFilters')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                                <div id="collapseDivFilters" class="panel-collapse collapse" runat="server">         
                                 <div class="panel-body">
                           <div class="col-md-12 colpadding0">
                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;display:none;">                                    
                                    
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                </div>

                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                   <asp:DropDownCheckBoxes ID="ddlLegalEntityMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlLegalEntityMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                                <Texts SelectBoxCaption="Select Unit" />
                                            </asp:DropDownCheckBoxes>
                                   </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">
                                    
                                          <asp:DropDownCheckBoxes ID="ddlSubEntity1" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="280" DropDownBoxBoxWidth="250" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>
                                                                 
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                   
                                    
                                      <asp:DropDownCheckBoxes ID="ddlSubEntity2" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin-top: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="280" DropDownBoxBoxWidth="250" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>                                  
                                </div> 
                                                                                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                    
                                   
                                    <asp:DropDownCheckBoxes ID="ddlSubEntity3" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="280" DropDownBoxBoxWidth="250" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>                               
                                </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%">                                                           
                                         <asp:DropDownCheckBoxes ID="ddlFilterLocation" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;margin-top: 8px;">
                                            <Style SelectBoxWidth="280" DropDownBoxBoxWidth="230" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>  
                                    </div> 

                          </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;display:none;">                     
                                   
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="false"
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                        OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>                                                           
                                    </div>       
                            
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                           <asp:DropDownCheckBoxes ID="ddlFinancialYearMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlFinancialYearMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                                <Texts SelectBoxCaption="Select Financial Year" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                                                                                                                                     
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%;display:none;">                     
                                   
                                        <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Select Process" AutoPostBack="true"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>

                                    </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                       <asp:DropDownCheckBoxes ID="ddlProcessMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                        CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcessMultiSelect_SelectedIndexChanged"
                                        AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                        Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                        <Style SelectBoxWidth="320" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                        <Texts SelectBoxCaption="Select Process" />
                                        </asp:DropDownCheckBoxes>
                                   </div>
                                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%> 
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%;display:none;">                                                        
                                            <asp:DropDownListChosen runat="server" ID="ddlVertical" DataPlaceHolder="Verticals" AutoPostBack="true"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                            class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div>   

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%;">                                                        
                                            <asp:DropDownCheckBoxes runat="server" ID="ddlVerticalList" DataPlaceHolder="Verticals" AutoPostBack="true"
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlVerticalList_SelectedIndexChanged"
                                        AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                        Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                        <Style SelectBoxWidth="320" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                        <Texts SelectBoxCaption="Select Vertical" />
                                            </asp:DropDownCheckBoxes>
                                        </div>
                                 <%}%>              
                            
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%;">                                                        
                                            <asp:DropDownCheckBoxes runat="server" ID="ddlStatusList" DataPlaceHolder="Verticals" AutoPostBack="true"
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlStatusList_SelectedIndexChanged"
                                        AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                        Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                        <Style SelectBoxWidth="320" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                        <Items>
                                                <asp:ListItem Text="Open-Due" Value="0"/>
                                                <asp:ListItem Text="Open-Not Due" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Closed" Value="2"></asp:ListItem>
                                            </Items>
                                                <Texts SelectBoxCaption="Select Status" />
                                            </asp:DropDownCheckBoxes>
                                        </div>  
                                                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%;"> 
                                        <asp:Button ID="btnFilter" class="btn btn-search" runat="server" OnClick="btnTopSearch_Click" Text="Apply Filters" />   
                                        <asp:Button ID="btnExport" class="btn btn-search" runat="server" Text="Export Excel" style="margin-left: 10px;" OnClick="btnExport_Click"/>                                                                        
                                    </div>                                                  
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; display:none; ">                     
                                        <asp:DropDownList runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList runat="server" ID="ddlPeriod" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>                 
                                </div> 

                             <div class="clearfix"></div>

                             <div class="col-md-12 colpadding0">                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                                           
                                    </div>
                               </div>  
                   <div class="clearfix"></div>                                    
                  
                    <div class="col-md-12 colpadding0" style="float: left;">

                        <div class="col-md-6 colpadding0 entrycount" style="display:none;">                         
                        <h5 style="font-weight:bold;">Select Your Risk Color Preference</h5>
                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="High" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Major</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                              <input id="medium" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Moderate</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                             <input id="low" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Minor</h5>
                            </div>
                         </div>

                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  

                         </div>
                    </div>
                </div>
                                   </div>
                        </div>
                    </div>
                </div>     
                           </div>               

                        <div class="clearfix"></div>
                         <asp:HiddenField ID="highcolor" runat="server" Value="#FF0000" /> <%--#FF7473--%>
                         <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFFF00"/>  <%--#FFC952--%>
                         <asp:HiddenField ID="lowcolor" runat="server" Value="#90ed7d"/>  <%--#1FD9E1--%>

                         <!-- ObservationStatus Start -->
                <asp:UpdatePanel ID="upObservationStatus" runat="server" UpdateMode="Conditional">       
                          <ContentTemplate>  
                <div id="ObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Observations - Category Wise </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="DivGraphObsStatus" class="col-md-12">                                           
                                        </div>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>  
                         </ContentTemplate>   
                 </asp:UpdatePanel>              
                 <!-- ObservationStatus End -->

                 <div class="clearfix"></div>

                 <!-- Process Wise Observation Status Start -->
                <%-- <asp:UpdatePanel ID="upProcessWiseObservationStatus" runat="server" UpdateMode="Conditional">       
                          <ContentTemplate> --%> 
                <div id="ProcessWiseObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Observations - Process Wise </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseProcessObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseProcessObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="DivGraphProcessObsStatus" class="col-md-12" runat="server" style="height: 200px; overflow-y: auto;">                                           
                                        </div>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div> 
                              <%-- </ContentTemplate>   
                 </asp:UpdatePanel> --%>                
                  <!-- Process Wise Observation Status End -->

                 <!-- Observation Aging Process Wise Start -->
                <div id="ObservationAgingProcessWise" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                   <h2>Open Observations- Aging - Process Wise </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseObsAgingProcess"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationAgingProcessWise')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseObsAgingProcess" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="DivObsAgingProcess" class="col-md-12" runat="server" style="height: 200px; overflow-y: auto;">                                           
                                        </div>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                  <!-- Observation Aging Process Wise End -->

                 <!-- Audit Tracker start -->
                <div id="divAuditTrackerSummary" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Audit Tracker</h2>                                                                       
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditTrackerSummary"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('divAuditTrackerSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div> 

                             <div id="collapseAuditTrackerSummary" class="panel-collapse collapse in"> 

                                <div class="panel-body">                               
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table style="margin-top: 20px;" width="100%">
                                                 <tr>
                                                    <td>
                                                        <div style="width: 99%; margin-top: 15px; ">

                                                        <asp:GridView runat="server" ID="grdAuditTrackerSummary" AutoGenerateColumns="true"  AllowSorting="false"  AllowPaging="false" CellSpacing="5"
                                                           GridLines="None" CssClass="table" Width="100%" OnRowDataBound ="grdAuditTrackerSummary_RowDataBound" >                                           
                                                         <RowStyle CssClass="clsROWgrid"   />
                                                         <HeaderStyle CssClass="clsheadergrid"    />
                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                           No Records Found.
                                                        </EmptyDataTemplate>
                                                        </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                              <tr>                                               
                                                </tr>
                                             </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                             </div>
                            </div>
                        </div>
                    </div>
                </div>   
                 <!-- Audit Tracker End --> 
               
                 <!-- Audit Status start -->
                 <%-- <asp:UpdatePanel ID="upAuditStatusSummary" runat="server">       
                          <ContentTemplate>--%>
                <div id="divAuditStatusSummary" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Audit Status</h2>                                                                       
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditStatusSummary"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('divAuditStatusSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div> 

                             <div id="collapseAuditStatusSummary" class="panel-collapse collapse in"> 

                                <div class="panel-body">                               
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td style="float:right;">
                                                        <div class="col-md-12 colpadding0">
                                                            <%-- <asp:Button Text="Advanced Filters" class="btn btn-advanceSearch" runat="server" OnClientClick="return showdiv();" ValidationGroup="DocumentsValidation" />--%>
                                                           <a class="btn btn-advanceSearch" data-toggle="modal" href="#DivAdvanceSearch" title="Search" style="display:none;">Advanced Filters</a>
                                                        </div>
                                                    </td>
                                                    
                                                    <th class="ui-widget-header" style="display:none;">
                                                    <%--<asp:Label ID="Label1" runat="server" Text="Past 12 Months's Summary"></asp:Label>--%>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="DivGraphAuditStatus" style="height: 200px; overflow-y: auto; width: 100%; margin-top: 5px;" runat="server">                                                       
                                                                                                                
                                                        </div>
                                                    </td>
                                                </tr>
                                              <tr>                                               
                                                </tr>
                                             </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                                    
                                </div>                                
                             </div>
                            </div>
                        </div>
                    </div>
                </div>   
                              <%--</ContentTemplate>   
                 </asp:UpdatePanel>  --%>
                 <!-- Audit Tracker End --> 
                <div class="row Dashboard-white-widget">
                <div class="dashboard">  
                            <div id="AuditSummary" class="col-lg-12 col-md-12 colpadding0" style="padding-left: 23px;">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Audit Head Summary/Audit Manager Summary</h2>
                                </div>
                            </div>                             
                        </div>                        
                            <div class="row" style="padding-left: 23px; margin-top: 10px;">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-6  borderright">             
                                                <a href="../AuditTool/AuditManagerStatusUI.aspx?Type=Process&Status=Open&FY=<%=IsHiddenFY%>&IsHiddenSubEntity=<%=IsHiddenSubEntity%>&IsHiddenSubEntityOne=<%=IsHiddenSubEntityOne%>&IsHiddenSubEntityTwo=<%=IsHiddenSubEntityTwo%>&IsHiddenSubEntityThree=<%=IsHiddenSubEntityThree%>&IsHiddenSubEntityFour=<%=IsHiddenSubEntityFour%>">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divOpenAMCount">0</div>
                                                </a>
                                                <div class="desc">Audit</div>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="../AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&Status=Open&FY=<%=IsHiddenFY%>&IsHiddenSubEntity=<%=IsHiddenSubEntity%>&IsHiddenSubEntityOne=<%=IsHiddenSubEntityOne%>&IsHiddenSubEntityTwo=<%=IsHiddenSubEntityTwo%>&IsHiddenSubEntityThree=<%=IsHiddenSubEntityThree%>&IsHiddenSubEntityFour=<%=IsHiddenSubEntityFour%>">  
                                                     <div class="count" runat="server" id="divOpenAMIMPCount">0</div>
                                                </a>
                                                <div class="desc">Implementation</div>
                                            </div>                                                                  
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-6  borderright">             
                                               <a href="../AuditTool/AuditManagerStatusUI.aspx?Type=Process&Status=Closed&FY=<%=IsHiddenFY%>&IsHiddenSubEntity=<%=IsHiddenSubEntity%>&IsHiddenSubEntityOne=<%=IsHiddenSubEntityOne%>&IsHiddenSubEntityTwo=<%=IsHiddenSubEntityTwo%>&IsHiddenSubEntityThree=<%=IsHiddenSubEntityThree%>&IsHiddenSubEntityFour=<%=IsHiddenSubEntityFour%>">                                                                                                                                                                                                                                                                                          
                                                    <div class="count" runat="server" id="divCloseAMCount">0</div>
                                                </a>
                                                <div class="desc">Audit</div>
                                            </div>
                                            <div class="col-md-6">
                                               <a href="../AuditTool/AuditManagerStatusUI.aspx?Type=Implementation&Status=Closed&FY=<%=IsHiddenFY%>&IsHiddenSubEntity=<%=IsHiddenSubEntity%>&IsHiddenSubEntityOne=<%=IsHiddenSubEntityOne%>&IsHiddenSubEntityTwo=<%=IsHiddenSubEntityTwo%>&IsHiddenSubEntityThree=<%=IsHiddenSubEntityThree%>&IsHiddenSubEntityFour=<%=IsHiddenSubEntityFour%>">   
                                                     <div class="count" runat="server" id="divCloseAMIMPCount">0</div>
                                                </a>
                                                <div class="desc">Implementation</div>
                                            </div>                                                                  
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div>
                           </div>
                      

                            <div class="clearfix"></div>
                                <% if (roles.Contains(3) || roles.Contains(4))%>
                                 <% { %>     
                                        <div id="PerformerReviewerSummary" class="col-lg-12 col-md-12 colpadding0" style="padding-left: 23px;">
                                            <div class="panel panel-default" style="background:none;">
                                                <div class="panel-heading" style="background:none;">
                                                   <h2>Performer/Reviewer Summary</h2>
                                                </div>
                                            </div>   
                                        </div>
                                        <div class="row" style="padding-left: 23px; margin-top: 10px;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                                            <div class="col-md-6  borderright">             
                                                                <a href="../AuditTool/AuditStatusUI.aspx?Status=Open&FY=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                         
                                                                    <div class="count" runat="server" id="divOpenAuditCount">0</div>
                                                                </a>
                                                                <div class="desc">Audit</div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Open&FY=<%=IsHiddenFY%>">   
                                                                     <div class="count" runat="server" id="divOpenAuditIMPCount">0</div>
                                                                </a>
                                                                <div class="desc">Implementation</div>
                                                            </div>                                                                  
                                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                                    </div>   
                                                </div> 

                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                                            <div class="col-md-6  borderright">             
                                                                <a href="../AuditTool/AuditStatusUI.aspx?Status=Closed&FY=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                         
                                                                    <div class="count" runat="server" id="divCloseAuditCount">0</div>
                                                                </a>
                                                                <div class="desc">Audit</div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Closed&FY=<%=IsHiddenFY%>">   
                                                                     <div class="count" runat="server" id="divCloseAuditIMPCount">0</div>
                                                                </a>
                                                                <div class="desc">Implementation</div>
                                                            </div>                                                                  
                                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                                    </div>   
                                                </div>
                                           </div>
                                 <% }%>
                                 <% if (prerqusite)%>
                                 <%{%>  
                                        <div id="Prerequisitesummary" class="col-lg-12 col-md-12 colpadding0" style="padding-left: 23px;">
                                            <div class="panel panel-default" style="background: none;">
                                                <div class="panel-heading" style="background: none;">
                                                    <h2>Pre-Requisite Summary</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 23px; margin-top: 10px;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Open</div>
                                                        <div class="col-md-12" style="padding-top: 22px;">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Open&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteOpenAuditCount">0</div>
                                                            </a>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Submitted</div>
                                                        <div class="col-md-12" style="padding-top: 22px;">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Submitted&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteSubmittedAuditCount">0</div>
                                                            </a>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Rejected</div>
                                                        <div class="col-md-12" style="padding-top: 22px;">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Rejected&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteRejectedCount">0</div>
                                                            </a>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                 <%}%>
                    </div>
                    </div>
                            <div class="clearfix"></div>

                            <div class="modal fade" id="DivReports" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog" style="width:90%;">          
                            <div class="modal-content" style="width:100%;">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>

                                <div class="table-responsive" style="width:100%;">              
                                    <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>                                      
                                </div>
                            </div>
                        </div>
                   </div>

                            <div class="modal fade" id="DivAdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog" style="width:90%;">          
                            <div class="modal-content" style="width:100%;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>                  
    </section>
    </asp:Content>