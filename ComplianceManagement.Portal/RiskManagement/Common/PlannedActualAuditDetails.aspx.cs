﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class PlannedActualAuditDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //BindLegalEntityData();
                //BindFinancialYear();               
                //BindVertical();
                //BindData();
                //bindPageNumber();                
                ////ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ////ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ////ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ////ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));                
                //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                //ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindFinancialYear();
                string financialYearCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FinancialYearList"]))
                {
                    financialYearCommaSeparatedList = Request.QueryString["FinancialYearList"].ToString();
                }

                if (!string.IsNullOrEmpty(financialYearCommaSeparatedList))
                {
                    List<string> finYearsList = financialYearCommaSeparatedList.Split(',').ToList();
                    if (finYearsList.Count > 0)
                    {
                        foreach (string fy in finYearsList)
                        {
                            for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                            {
                                if (ddlFinancialYearMultiSelect.Items[i].Text == fy)
                                {
                                    ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                }

                            }
                        }
                    }
                }

                string processIdsCommanSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    processIdsCommanSeparatedList = Request.QueryString["PID"].ToString();
                }
                BindLegalEntityData();
                List<long> CHKBranchlist = new List<long>();
                string branchIdsCommanSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    branchIdsCommanSeparatedList = Request.QueryString["branchid"].ToString();
                }
                if (!string.IsNullOrEmpty(branchIdsCommanSeparatedList) && branchIdsCommanSeparatedList != "-1")
                {
                    List<string> branchIdsList = branchIdsCommanSeparatedList.Split(',').ToList();
                    if (branchIdsList.Count > 0)
                    {
                        for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                        {
                            foreach (string bId in branchIdsList)
                            {
                                if (ddlLegalEntityMultiSelect.Items[i].Value == bId)
                                {
                                    ddlLegalEntityMultiSelect.Items[i].Selected = true;
                                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                                }
                            }
                        }
                    }
                }

                List<long> SelectedSubEnityOneListId = new List<long>();
                if (CHKBranchlist.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["subEntityOneIdCommaSeparatedList"]))
                    {
                        subEntityOneIdCommaSeparatedList = Request.QueryString["subEntityOneIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityOneIdCommaSeparatedList) && subEntityOneIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityOneIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity1MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity1MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityOneListId.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                List<long> SelectedSubEnityTwoListId = new List<long>();
                if (SelectedSubEnityOneListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2MultiSelect, SelectedSubEnityOneListId);
                    string subEntityTwoIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["SubEntityTwoIdCommaSeparatedList"]))
                    {
                        subEntityTwoIdCommaSeparatedList = Request.QueryString["SubEntityTwoIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityTwoIdCommaSeparatedList) && subEntityTwoIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityTwoIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity2MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity2MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityTwoListId.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        Session["BranchListMG"] = CHKBranchlist;
                    }
                }
                BindVertical();
                BindSchedulingType();
                for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
                {
                    ddlSchedulingTypeMultiSelect.Items[i].Selected = true;
                }
                BindPeriod();
                BindData();
                bindPageNumber();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;                
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);                
                BindData();
            }
        }        
        public void BindVertical()
        {
            try
            {
                List<int> branchidList = new List<int>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearLegalBranchList = false;
                        }

                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                ddlverticalList.DataTextField = "VerticalName";
                ddlverticalList.DataValueField = "VerticalsId";
                ddlverticalList.Items.Clear();
                ddlverticalList.DataSource = UserManagementRisk.FillVerticalListFromMultiselect(branchidList);
                ddlverticalList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.Items.Clear();
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
            //ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntityMultiSelect.DataTextField = "Name";
            ddlLegalEntityMultiSelect.DataValueField = "ID";
            ddlLegalEntityMultiSelect.Items.Clear();
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(CustomerId, UserID);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(CustomerId, UserID);
                }
            }
            //ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntityMultiSelect.DataBind();
            //ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            bool IsAuditManager = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();

            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }            
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSubEntityData(DropDownCheckBoxes DRP, List<long> ParentIdList)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            bool IsAuditManager = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();

            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentIdList);
            }
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentIdList);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
        }
        public void BindSchedulingType()
        {
            List<int> branchidList = new List<int>();
            bool clearLegalBranchList = false;
            bool clearSubEntity1List = false;
            bool clearSubEntity2List = false;
            bool clearSubEntity3List = false;
            bool clearFilterLocation = false;

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    clearLegalBranchList = true;
                    branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    if (clearLegalBranchList && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearLegalBranchList = false;
                    }

                    clearSubEntity1List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity1List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity1List = false;
                    }
                    clearSubEntity2List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity2List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity2List = false;
                    }
                    clearSubEntity3List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
            {
                if (ddlFilterLocationMultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity3List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity3List = false;
                    }
                    clearFilterLocation = true;
                    branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                }
            }
            ddlSchedulingTypeMultiSelect.DataTextField = "Name";
            ddlSchedulingTypeMultiSelect.DataValueField = "ID";
            ddlSchedulingTypeMultiSelect.DataSource = UserManagementRisk.FillSchedulingTypeMultiple(branchidList);
            ddlSchedulingTypeMultiSelect.DataBind();
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindData()
        {
            try
            {
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}
                //ApplyFilter = false;                
                //int CustBranchID = -1;
                //int VerticalID = -1;                
                //int UserID = -1;
                //bool IsAuditManager = false;
                //bool IsManagement = false;
                //String FinancialYear = String.Empty;
                //String Period = String.Empty;
                //String Type = String.Empty;

                //DateTime FromDate = new DateTime();
                //DateTime ToDate = new DateTime();               
                //if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                //{
                //    Type = Request.QueryString["Type"];
                //}                
                //if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                //{
                //    CustBranchID = Convert.ToInt32(Request.QueryString["branchid"]);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["FDate"]))
                //{
                //    FromDate = Convert.ToDateTime(Request.QueryString["FDate"]);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["TDate"]))
                //{
                //    ToDate = Convert.ToDateTime(Request.QueryString["TDate"]);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                //{
                //    Period = Convert.ToString(Request.QueryString["Period"]);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                //{
                //    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                //{
                //    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                //    {
                //        IsAuditManager = true;                        
                //    }
                //    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                //    {
                //        IsManagement = true;                        
                //    }
                //}
                //UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //List<int?> BranchList = new List<int?>();
                //if (Session["BranchList"] != null)
                //{
                //    BranchList = (List<int?>)(Session["BranchList"]);
                //}
                //var detailView = InternalControlManagementDashboardRisk.GetAuditPlannedActualDisplayView(Type, CustomerId, BranchList, FinancialYear, Period, FromDate, ToDate, VerticalID, UserID, IsAuditManager, IsManagement);
                //grdSummaryDetailsAuditCoverage.DataSource = detailView;
                //Session["TotalRows"] = detailView.Count;
                //grdSummaryDetailsAuditCoverage.DataBind();
                //Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<Sp_AuditPlannedActualDetailDisplayView_Result>).ToDataTable();

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ApplyFilter = false;
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsManagement = false;
                String Type = String.Empty;

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                List<string> FinancialYearList = new List<string>();
                List<int> Vertilcallist = new List<int>();
                List<string> PeriodList = new List<string>();
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FDate"]))
                {
                    FromDate = Convert.ToDateTime(Request.QueryString["FDate"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["TDate"]))
                {
                    ToDate = Convert.ToDateTime(Request.QueryString["TDate"]);
                }
                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VerticalID"]))
                {
                    string VerticalID = Convert.ToString(Request.QueryString["VerticalID"]);
                    List<string> verticalList = new List<string>();
                    verticalList = VerticalID.Split(',').ToList();
                    for (int i = 0; i < ddlverticalList.Items.Count; i++)
                    {
                        if (verticalList.Contains(ddlverticalList.Items[i].Value))
                        {
                            ddlverticalList.Items[i].Selected = true;
                            Vertilcallist.Add(Convert.ToInt32(ddlverticalList.Items[i].Value));
                        }
                    }
                }
                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                List<long> BranchList = new List<long>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<long>)(Session["BranchList"]);
                }
                if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                    BranchList = (List<long>)(Session["BranchListMG"]);
                }
                var detailView = InternalControlManagementDashboardRisk.GetAuditPlannedActualDisplayView(Type, CustomerId, BranchList, FinancialYearList, PeriodList, FromDate, ToDate, Vertilcallist, UserID, IsAuditManager, IsManagement);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<Sp_AuditPlannedActualDetailDisplayView_Result>).ToDataTable();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDataFilter()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsManagement = false;
                List<string> FinancialYearList = new List<string>();
                List<int> Vertilcallist = new List<int>();
                List<string> PeriodList = new List<string>();
                String Type = String.Empty;

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();

                List<long> branchidList = new List<long>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearLegalBranchList = false;
                        }

                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FDate"]))
                {
                    FromDate = Convert.ToDateTime(Request.QueryString["FDate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["TDate"]))
                {
                    ToDate = Convert.ToDateTime(Request.QueryString["TDate"]);
                }
                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }
                for (int i = 0; i < ddlverticalList.Items.Count; i++)
                {
                    if (ddlverticalList.Items[i].Selected)
                    {
                        Vertilcallist.Add(Convert.ToInt32(ddlverticalList.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                if (branchidList.Count == 0)
                {
                    if (Session["BranchList"] != null)
                    {
                        branchidList = (List<long>)(Session["BranchList"]);
                    }
                }
                var detailView = InternalControlManagementDashboardRisk.GetAuditPlannedActualDisplayView(Type, CustomerId, branchidList, FinancialYearList, PeriodList, FromDate, ToDate, Vertilcallist, UserID, IsAuditManager, IsManagement);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<Sp_AuditPlannedActualDetailDisplayView_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = true;
                BindDataFilter();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;

                    FileName = "Audit Status Report";

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);

                    ExcelData = view.ToTable("Selected", false, "Branch", "VerticalName", "FinancialYear", "ForMonth", "StartDate", "EndDate", "ExpectedStartDate", "ExpectedEndDate");
                   
                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["StartDate"] != null && item["StartDate"] != DBNull.Value)  
                                item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");

                        if (item["EndDate"] != null && item["EndDate"] != DBNull.Value)
                            item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");

                        if (item["ExpectedStartDate"] != null && item["ExpectedStartDate"] != DBNull.Value)
                            item["ExpectedStartDate"] = Convert.ToDateTime(item["ExpectedStartDate"]).ToString("dd-MMM-yyyy");

                        if (item["ExpectedEndDate"] != null && item["ExpectedEndDate"] != DBNull.Value)
                            item["ExpectedEndDate"] = Convert.ToDateTime(item["ExpectedEndDate"]).ToString("dd-MMM-yyyy");  
                    }

                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12; 
                    
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Audit Status Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Vertical";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Financial Year";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Period";
                    exWorkSheet.Cells["D5"].AutoFitColumns(15);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Audit Planned (From)";
                    exWorkSheet.Cells["E5"].AutoFitColumns(15);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Audit Planned (To)";
                    exWorkSheet.Cells["F5"].AutoFitColumns(15);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Audit Actual (From)";
                    exWorkSheet.Cells["G5"].AutoFitColumns(15);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Audit Actual (To)";
                    exWorkSheet.Cells["H5"].AutoFitColumns(15);                   

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                    {
                        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();                   
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                   
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);               
                //Reload the Grid
                if (ApplyFilter)
                    BindDataFilter();
                else
                    BindData();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();
            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity1MultiSelect.Items.Clear();
                }
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity1MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();
            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity2MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }

                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }

                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity3MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }

                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();
            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlFilterLocationMultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlFilterLocationMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSchedulingTypeMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }

        private void BindPeriod()
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);

                }
            }
        }
        public void BindAuditSchedule(List<string> flagList, List<int?> countList)
        {
            List<string> FlagList = new List<string>();
            try
            {
                foreach (var flag in flagList)
                {
                    if (flag == "A")
                    {
                        FlagList.Add("Annually");
                    }
                    else if (flag == "H")
                    {
                        FlagList.Add("Apr-Sep");
                        FlagList.Add("Oct-Mar");
                    }
                    else if (flag == "Q")
                    {
                        FlagList.Add("Apr-Jun");
                        FlagList.Add("Jul-Sep");
                        FlagList.Add("Oct-Dec");
                        FlagList.Add("Jan-Mar");
                    }
                    else if (flag == "M")
                    {
                        FlagList.Add("Apr");
                        FlagList.Add("May");
                        FlagList.Add("Jun");
                        FlagList.Add("Jul");
                        FlagList.Add("Aug");
                        FlagList.Add("Sep");
                        FlagList.Add("Oct");
                        FlagList.Add("Nov");
                        FlagList.Add("Dec");
                        FlagList.Add("Jan");
                        FlagList.Add("Feb");
                        FlagList.Add("Mar");
                    }
                    else if (flag == "S")
                    {
                        FlagList.Add("Special Audit");
                    }
                    else
                    {
                        int count = countList.Count;
                        if (count == 1)
                        {
                            FlagList.Add("Phase1");
                        }
                        else if (count == 2)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                        }
                        else if (count == 3)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                        }
                        else if (count == 4)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                        }
                        else
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                            FlagList.Add("Phase5");
                        }
                    }
                }
                if (FlagList.Count > 0)
                {
                    int setIndex = 0;
                    ddlPeriodMultiSelect.Items.Clear();
                    foreach (string item in FlagList)
                    {
                        ddlPeriodMultiSelect.Items.Insert(setIndex, item);
                        setIndex = setIndex + 1;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}