﻿//using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
//using AuditManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public class LoggerMessage
    {
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk>();
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk msg = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = 0;
                ReminderUserList.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk() { LogLevel = 0, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });                
                com.VirtuosoITech.ComplianceManagement.Business.UserManagementRisk.InsertLogToDatabase(ReminderUserList);
            }
            catch (Exception)
            {
                throw;

            }
        }
    }
}