﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class InternalControlDashboard : System.Web.UI.Page
    {
        protected bool prerqusite = false;
        protected List<Int32> roles;
        protected static string IsHiddenFY;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.ARSGetAssignedRolesBOTH(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindFinancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                    IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                }               
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                bindcount();
            }
        }

        public void bindcount()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string FinYear = String.Empty;
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                var dataAuditSummary = (from c in entities.AuditCountView_Dashboard
                                        select c).ToList();

                var dataImplimentInternal = (from c in entities.SP_ImplimentInternalAuditClosedCountView(customerID)
                                             select c).ToList();

                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                select row).ToList();

                if (!string.IsNullOrEmpty(FinYear))
                {
                    dataAuditSummary = dataAuditSummary.Where(entry => entry.FinancialYear == FinYear).ToList();
                    dataImplimentInternal = dataImplimentInternal.Where(entry => entry.FinancialYear == FinYear).ToList();
                    Masterrecord = Masterrecord.Where(entry => entry.FinancialYear == FinYear).ToList();
                }
                
                divOpenAuditCount.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 1, customerID).ToString();
                divCloseAuditCount.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 3, customerID).ToString();

                divOpenAuditIMPCount.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 1).ToString();
                divCloseAuditIMPCount.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 3).ToString();
                             
                if (Masterrecord.Count > 0)
                {
                    prerqusite = true;
                    var opencount = Masterrecord.Where(entry => entry.DocStatus == "Open").ToList().Count;
                    var submittedcount = Masterrecord.Where(entry => entry.DocStatus == "Submitted" || entry.DocStatus == "Approved" || entry.DocStatus == "Re-Submitted").ToList().Count;
                    var Rejectedcount = Masterrecord.Where(entry => entry.DocStatus == "Rejected").ToList().Count;
                    divPreRequisiteOpenAuditCount.InnerText = Convert.ToString(opencount);
                    divPreRequisiteSubmittedAuditCount.InnerText = Convert.ToString(submittedcount);
                    divPreRequisiteRejectedCount.InnerText = Convert.ToString(Rejectedcount);
                }
                else
                {
                    prerqusite = false;
                    divPreRequisiteOpenAuditCount.InnerText = "0";
                    divPreRequisiteSubmittedAuditCount.InnerText = "0";
                    divPreRequisiteRejectedCount.InnerText = "0";
                }
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                }
            }
            bindcount();
        }

        public static int GetAuditCountIMP(List<SP_ImplimentInternalAuditClosedCountView_Result> ImplimentInternalAuditClosedRecords,int userid, int statusid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;

                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;
                    int flag = 0;
                    var OpenImplimentCount = (from row in ImplimentInternalAuditClosedRecords
                                              where row.UserID == userid
                                              && row.AllClosed != flag
                                              select row.AuditID).Distinct().ToList();
                    
                    count = OpenImplimentCount.Count();
                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;
                    int flag = 0;
                    var closedImplimentCount = (from row in ImplimentInternalAuditClosedRecords
                                                where row.UserID == userid
                                                && row.AllClosed == flag
                                                select row.AuditID).Distinct().ToList();
                    count = closedImplimentCount.Count;
                }
                return count;
            }
        }
        public static int GetAuditCount(List<AuditCountView_Dashboard> AuditSummaryCountRecords,int userid, int statusid, long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;                   
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID=GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();                  
                    count = record.Count();

                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;                
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              //&& C.AuditStatusID == 3
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID=GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();

                
                    count = record.Count();
                }
                return count;
            }
        }


        public decimal GetImplementationPercentageAuditProcess()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetImplementationPercentageAuditProcessclose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }


        public decimal GetProcessPercentageAuditProcess()
        {
              decimal returnvalue = 0;

              int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

              int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

              int total = AuditOpen + AuditClose;

              if (AuditOpen > 0)
              {
                  if (AuditClose > 0)
                  {
                      returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                  }
                  else {
                      returnvalue = 100;
                  }
              }         
            return returnvalue;
        }

        public decimal GetProcessPercentageAuditProcessclose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

    }
}