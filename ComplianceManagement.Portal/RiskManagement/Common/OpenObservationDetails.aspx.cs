﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class OpenObservationDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected string AuditHeadOrManagerReport = null;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindVertical();
                List<long> CHKBranchlist = new List<long>();
                string branchIdsCommanSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    branchIdsCommanSeparatedList = Request.QueryString["branchid"].ToString();
                }
                if (!string.IsNullOrEmpty(branchIdsCommanSeparatedList) && branchIdsCommanSeparatedList != "-1")
                {
                    List<string> branchIdsList = branchIdsCommanSeparatedList.Split(',').ToList();
                    if (branchIdsList.Count > 0)
                    {
                        for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                        {
                            foreach (string bId in branchIdsList)
                            {
                                if (ddlLegalEntityMultiSelect.Items[i].Value == bId)
                                {
                                    ddlLegalEntityMultiSelect.Items[i].Selected = true;
                                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                                }
                            }
                        }
                    }
                }
                BindFinancialYear();
                string financialYearCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    financialYearCommaSeparatedList = Request.QueryString["FinYear"].ToString();
                }
                if (!string.IsNullOrEmpty(financialYearCommaSeparatedList))
                {
                    List<string> finYearsList = financialYearCommaSeparatedList.Split(',').ToList();
                    if (finYearsList.Count > 0)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            foreach (string fy in finYearsList)
                            {
                                if (ddlFinancialYearMultiSelect.Items[i].Text == fy)
                                {
                                    ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                    }
                }
                BindProcess("P");
                string ProcessNameQuery = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["PName"]))
                {
                    ProcessNameQuery = Request.QueryString["PName"].ToString();
                }
                if (!string.IsNullOrEmpty(ProcessNameQuery))
                {
                    for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                    {
                        if (ddlProcessMultiSelect.Items[i].Text == ProcessNameQuery)
                        {
                            ddlProcessMultiSelect.Items[i].Selected = true;
                        }
                    }
                    BindSubProcess();
                }

                List<long> SelectedSubEnityOneListId = new List<long>();
                if (CHKBranchlist.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["subEntityOneIdCommaSeparatedList"]))
                    {
                        subEntityOneIdCommaSeparatedList = Request.QueryString["subEntityOneIdCommaSeparatedList"].ToString();


                        if (!string.IsNullOrEmpty(subEntityOneIdCommaSeparatedList) && subEntityOneIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityOneIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity1MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity1MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityOneListId.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                List<long> SelectedSubEnityTwoListId = new List<long>();
                if (SelectedSubEnityOneListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2MultiSelect, SelectedSubEnityOneListId);
                    string subEntityTwoIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["SubEntityTwoIdCommaSeparatedList"]))
                    {
                        subEntityTwoIdCommaSeparatedList = Request.QueryString["SubEntityTwoIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityTwoIdCommaSeparatedList) && subEntityTwoIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityTwoIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity2MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity2MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityTwoListId.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                List<long> SelectedSubEnityThreeListId = new List<long>();
                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3MultiSelect, SelectedSubEnityTwoListId);

                    string subEntityThreeIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["SubEntityThreeIdCommaSeparatedList"]))
                    {
                        subEntityThreeIdCommaSeparatedList = Request.QueryString["SubEntityThreeIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(subEntityThreeIdCommaSeparatedList) && subEntityThreeIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityThreeIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity3MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity3MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityThreeListId.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationMultiSelect, SelectedSubEnityThreeListId);

                    string filterLocationIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["FilterLocationIdCommaSeparatedList"]))
                    {
                        filterLocationIdCommaSeparatedList = Request.QueryString["FilterLocationIdCommaSeparatedList"].ToString();
                        if (!string.IsNullOrEmpty(filterLocationIdCommaSeparatedList) && filterLocationIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = filterLocationIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlFilterLocationMultiSelect.Items[i].Value == bId)
                                        {
                                            ddlFilterLocationMultiSelect.Items[i].Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        Session["BranchListMG"] = CHKBranchlist;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        Session["BranchListDH"] = CHKBranchlist;
                    }
                }

                BindSchedulingType();
                for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
                {
                    ddlSchedulingTypeMultiSelect.Items[i].Selected = true;
                }
                BindPeriod();
                BindData();
                bindPageNumber();
            }
        }

        private void BindPeriod()
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        private void BindProcess(string flag)
        {
            try
            {
                //bool IsAuditManager = false;
                //bool IsDepartmentHead = false;
                //bool IsManagement = false;
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}
                //if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                //{
                //    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                //    {
                //        IsAuditManager = true;
                //    }
                //    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                //    {
                //        IsDepartmentHead = true;
                //    }
                //    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                //    {
                //        IsManagement = true;
                //    }
                //}
                //ddlProcess.Items.Clear();
                //ddlProcess.DataTextField = "Name";
                //ddlProcess.DataValueField = "Id";

                //if (IsAuditManager)
                //{
                //    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                //    ddlProcess.DataBind();
                //    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                //}
                //else if (IsDepartmentHead)
                //{
                //    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownDepartmentHead(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                //    ddlProcess.DataBind();
                //    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                //}
                //else if (IsManagement)
                //{
                //    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                //    ddlProcess.DataBind();
                //    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                //}

                ////if (flag == "P")
                ////{
                ////    ddlProcess.DataSource = ProcessManagement.FillProcess("P", CustomerId);
                ////    ddlProcess.DataBind();
                ////    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                ////}
                ////else
                ////{                    
                ////    ddlProcess.DataSource = ProcessManagement.FillProcess("N", CustomerId);
                ////    ddlProcess.DataBind();
                ////    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                ////}

                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                List<int> branchidList = new List<int>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                ddlProcessMultiSelect.Items.Clear();
                ddlProcessMultiSelect.DataTextField = "Name";
                ddlProcessMultiSelect.DataValueField = "Id";

                if (IsAuditManager)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownAuditManagerForAuditHeadDashBoard(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
                else if (IsDepartmentHead)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownDepartmentHeadForAuditHeadDashBoard(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
                else if (IsManagement)
                {
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownManagementForAuditHeadDashBoard(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindVertical()
        {
            try
            {
                CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                List<int> branchidList = new List<int>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                ddlVertilcaList.DataTextField = "VerticalName";
                ddlVertilcaList.DataValueField = "VerticalsId";
                ddlVertilcaList.Items.Clear();
                ddlVertilcaList.DataSource = UserManagementRisk.FillVerticalListFromMultiselect(branchidList);
                ddlVertilcaList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.Items.Clear();
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntityMultiSelect.DataTextField = "Name";
            ddlLegalEntityMultiSelect.DataValueField = "ID";
            ddlLegalEntityMultiSelect.Items.Clear();
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(CustomerId, UserID);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(CustomerId, UserID);
                }
            }
            ddlLegalEntityMultiSelect.DataBind();
            //ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            bool IsAuditManager = false;
            bool IsDepartmentHead = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM" || Convert.ToString(Request.QueryString["UType"]) == "AH")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsDepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        public void BindSchedulingType()
        {
            CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            List<int> branchidList = new List<int>();
            bool clearLegalBranchList = false;
            bool clearSubEntity1List = false;
            bool clearSubEntity2List = false;
            bool clearSubEntity3List = false;
            bool clearFilterLocation = false;

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    clearLegalBranchList = true;
                    branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    if (clearLegalBranchList && branchidList.Count > 0)
                    {
                        //branchidList.Clear();
                        clearLegalBranchList = false;
                    }
                    clearSubEntity1List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity1List && branchidList.Count > 0)
                    {
                        //branchidList.Clear();
                        clearSubEntity1List = false;
                    }
                    clearSubEntity2List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity2List && branchidList.Count > 0)
                    {
                        //branchidList.Clear();
                        clearSubEntity2List = false;
                    }
                    clearSubEntity3List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
            {
                if (ddlFilterLocationMultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity3List && branchidList.Count > 0)
                    {
                        //branchidList.Clear();
                        clearSubEntity3List = false;
                    }
                    clearFilterLocation = true;
                    branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                }
            }
            ddlSchedulingTypeMultiSelect.DataTextField = "Name";
            ddlSchedulingTypeMultiSelect.DataValueField = "ID";
            ddlSchedulingTypeMultiSelect.DataSource = UserManagementRisk.FillSchedulingTypeMultiple(branchidList);
            ddlSchedulingTypeMultiSelect.DataBind();
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindData()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                ApplyFilter = false;
                List<int> VerticalIDlist = new List<int>();
                List<string> StatusListAudit = new List<string>();
                int UserID = -1;
                bool IsDepartmentHead = false;
                bool IsAuditManager = false;
                bool IsManagement = false;
                List<String> FinancialYear = new List<String>();
                List<String> PeriodList = new List<String>();
                String Type = String.Empty;
                List<long> ProcessIDList = new List<long>();
                List<long?> SubProcessProcessIDList = new List<long?>();
                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected)
                    {
                        ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    List<string> finYear = Request.QueryString["FinYear"].Split(',').ToList();
                    if (finYear.Count > 0)
                    {
                        foreach (string fy in finYear)
                        {
                            FinancialYear.Add(fy);
                        }
                    }
                }
                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["statusListwithComma"]))
                {
                    string statusListwithComma = Convert.ToString(Request.QueryString["statusListwithComma"]);
                    StatusListAudit = statusListwithComma.Split(',').ToList();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VerticalID"]))
                {
                    string VerticalID = Convert.ToString(Request.QueryString["VerticalID"]);
                    List<string> verticalList = new List<string>();
                    verticalList = VerticalID.Split(',').ToList();
                    for (int i = 0; i < ddlVertilcaList.Items.Count; i++)
                    {
                        if (verticalList.Contains(ddlVertilcaList.Items[i].Value))
                        {
                            ddlVertilcaList.Items[i].Selected = true;
                            VerticalIDlist.Add(Convert.ToInt32(ddlVertilcaList.Items[i].Value));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                List<long> BranchList = new List<long>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<long>)(Session["BranchList"]);
                }
                if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                    BranchList = (List<long>)(Session["BranchListDH"]);
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                    BranchList = (List<long>)(Session["BranchListMG"]);
                }
                var detailView = InternalControlManagementDashboardRisk.GetOpenObservationsDetailView(Type, CustomerId, BranchList, FinancialYear, PeriodList, ProcessIDList, VerticalIDlist, UserID, IsAuditManager, IsManagement, IsDepartmentHead, SubProcessProcessIDList);
                if (StatusListAudit.Count > 0)
                {
                    List<int> ClosestutusList = new List<int>();
                    ClosestutusList.Add(2); ClosestutusList.Add(3);
                    if (StatusListAudit.Count == 1)
                    {
                        List<int> OpenstutusList = new List<int>();
                        OpenstutusList.Add(4); OpenstutusList.Add(5);
                        if (StatusListAudit.Contains("0"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("1"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("2"))
                        {
                            detailView = detailView.Where(entry => entry.AuditImplementStatus == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                        }
                    }
                    else if (StatusListAudit.Count == 2)
                    {
                        List<int> OpenstutusList = new List<int>();
                        OpenstutusList.Add(4); OpenstutusList.Add(5);
                        if (StatusListAudit.Contains("0") && StatusListAudit.Contains("1"))
                        {
                            detailView = detailView.Where(entry => (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                        }
                        else if (StatusListAudit.Contains("1") && StatusListAudit.Contains("2"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now).ToList();
                        }
                        else if (StatusListAudit.Contains("2") && StatusListAudit.Contains("0"))
                        {
                            detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now).ToList();
                        }
                    }
                }
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<SP_ObservationAging_DisplayView_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDataFilter()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int CatID = -1;
                List<int> VerticalIDlist = new List<int>();
                List<long> ProcessIDList = new List<long>();
                List<long?> SubProcessIDList = new List<long?>();
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                List<String> FinancialYear = new List<String>();
                List<String> PeriodList = new List<String>();
                String Type = String.Empty;

                List<long> branchidList = new List<long>();
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {
                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && branchidList.Count > 0)
                        {
                            //branchidList.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYear.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }

                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected)
                    {
                        ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlSubProcessMultiSelect.Items[i].Selected)
                    {
                        SubProcessIDList.Add(Convert.ToInt32(ddlSubProcessMultiSelect.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlVertilcaList.Items.Count; i++)
                {
                    if (ddlVertilcaList.Items[i].Selected)
                    {
                        VerticalIDlist.Add(Convert.ToInt32(ddlVertilcaList.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ObsID"]))
                {
                    CatID = Convert.ToInt32(Request.QueryString["ObsID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                List<int> ObsRating = new List<int>();
                if (Type.Trim().Equals("Mejor"))
                    ObsRating.Add(1);
                else if (Type.Trim().Equals("Moderate"))
                {
                    ObsRating.Add(2);
                }
                else if (Type.Trim().Equals("Medium"))
                {
                    ObsRating.Add(3);
                }
                else
                {
                    ObsRating.Add(1);
                    ObsRating.Add(2);
                    ObsRating.Add(3);
                }
                var detailView = InternalControlManagementDashboardRisk.GetOpenObservationsDetailView(Type, CustomerId, branchidList, FinancialYear, PeriodList, ProcessIDList, VerticalIDlist, UserID, IsAuditManager, IsManagement, IsDepartmentHead, SubProcessIDList);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<SP_ObservationAging_DisplayView_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;
                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                if (ApplyFilter)
                {
                    BindDataFilter();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else
                {
                    BindData();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = true;
                BindDataFilter();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;
                    FileName = "Observation Report";
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "Branch", "FinancialYear", "VerticalName", "ForMonth", "ProcessName", "SubProcessName", "ActivityDescription", "Observation", "ManagementResponse", "Recomendation", "StatusTimeline", "ObservationCategoryName", "ObservatioRating");
                    ExcelData.Columns.Add("Rating");
                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));
                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["ObservatioRating"] != null)
                        {
                            if (item["ObservatioRating"].ToString() == "1")
                                item["Rating"] = "Major";
                            else if (item["ObservatioRating"].ToString() == "2")
                                item["Rating"] = "Moderate";
                            else if (item["ObservatioRating"].ToString() == "3")
                                item["Rating"] = "Minor";
                        }
                    }

                    ExcelData.Columns.Remove("ObservatioRating");
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Observation Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Financial Year";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Vertical Name";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Period";
                    exWorkSheet.Cells["D5"].AutoFitColumns(15);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Process";
                    exWorkSheet.Cells["E5"].AutoFitColumns(25);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "SubProcessName";
                    exWorkSheet.Cells["F5"].AutoFitColumns(50);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Activity Description";
                    exWorkSheet.Cells["G5"].AutoFitColumns(50);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Observation";
                    exWorkSheet.Cells["H5"].AutoFitColumns(50);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Management Response";
                    exWorkSheet.Cells["I5"].AutoFitColumns(50);

                    exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J5"].Value = "Recommandation";
                    exWorkSheet.Cells["J5"].AutoFitColumns(50);

                    exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K5"].Value = "Time Line";
                    exWorkSheet.Cells["K5"].AutoFitColumns(15);

                    exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L5"].Value = "Observation Category";
                    exWorkSheet.Cells["L5"].AutoFitColumns(25);

                    exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M5"].Value = "Observation Rating";
                    exWorkSheet.Cells["M5"].AutoFitColumns(20);

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();
            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity1MultiSelect.Items.Clear();
                }
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity1MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity2MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity3MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlFilterLocationMultiSelect, parentIdsList);
            }
            else
            {
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }

        

        protected void ddlSchedulingTypeMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3MultiSelect.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }

        protected void ddlProcessMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubProcess();
        }

        private void BindSubProcess()
        {
            List<long?> ProcessIdsList = new List<long?>();
            for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
            {
                if (ddlProcessMultiSelect.Items[i].Selected == true)
                {
                    ProcessIdsList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                }
            }
            if (ProcessIdsList.Count > 0)
            {
                ddlSubProcessMultiSelect.DataTextField = "Name";
                ddlSubProcessMultiSelect.DataValueField = "ID";
                ddlSubProcessMultiSelect.DataSource = ProcessManagement.FillSubProcessDropdownManagementForAuditHeadDashBoard(ProcessIdsList, Portal.Common.AuthenticationHelper.UserID);
                ddlSubProcessMultiSelect.DataBind();
            }
            else
            {
                ddlSubProcessMultiSelect.Items.Clear();
            }
        }

        public void BindSubEntityData(DropDownCheckBoxes DRP, List<long> ParentIdList)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            bool IsAuditManager = false;
            bool IsDepartmentHead = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM" || Convert.ToString(Request.QueryString["UType"]) == "AH")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentIdList);
            }
            else if (IsDepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentIdList);
            }
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityDataMultiple(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentIdList);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
        }

        public void BindAuditSchedule(List<string> flagList, List<int?> countList)
        {
            List<string> FlagList = new List<string>();
            try
            {
                foreach (var flag in flagList)
                {
                    if (flag == "A")
                    {
                        FlagList.Add("Annually");
                    }
                    else if (flag == "H")
                    {
                        FlagList.Add("Apr-Sep");
                        FlagList.Add("Oct-Mar");
                    }
                    else if (flag == "Q")
                    {
                        FlagList.Add("Apr-Jun");
                        FlagList.Add("Jul-Sep");
                        FlagList.Add("Oct-Dec");
                        FlagList.Add("Jan-Mar");
                    }
                    else if (flag == "M")
                    {
                        FlagList.Add("Apr");
                        FlagList.Add("May");
                        FlagList.Add("Jun");
                        FlagList.Add("Jul");
                        FlagList.Add("Aug");
                        FlagList.Add("Sep");
                        FlagList.Add("Oct");
                        FlagList.Add("Nov");
                        FlagList.Add("Dec");
                        FlagList.Add("Jan");
                        FlagList.Add("Feb");
                        FlagList.Add("Mar");
                    }
                    else if (flag == "S")
                    {
                        FlagList.Add("Special Audit");
                    }
                    else
                    {
                        int count = countList.Count;
                        if (count == 1)
                        {
                            FlagList.Add("Phase1");
                        }
                        else if (count == 2)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                        }
                        else if (count == 3)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                        }
                        else if (count == 4)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                        }
                        else
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                            FlagList.Add("Phase5");
                        }
                    }
                }
                if (FlagList.Count > 0)
                {
                    int setIndex = 0;
                    ddlPeriodMultiSelect.Items.Clear();
                    foreach (string item in FlagList)
                    {
                        ddlPeriodMultiSelect.Items.Insert(setIndex, item);
                        setIndex = setIndex + 1;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}