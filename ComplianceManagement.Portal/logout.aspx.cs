﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                //FormsAuthentication.SignOut();
                //Session.Abandon();
                //FormsAuthentication.RedirectToLoginPage();
                //Response.Redirect("LogoutSuccessfully.aspx", false);      

                int userID = -1;

                if (AuthenticationHelper.UserID != null)
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);

                string redirectURL = string.Empty;
                if (userID != -1)
                {
                    User user = UserManagement.GetByID(userID);

                    if (user != null)
                        redirectURL = user.LogOutURL;
                    //redirectURL = @"https://login.microsoftonline.com/common/wsfederation?wa=wsignout1.0";
                }

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                FormsAuthentication.SignOut();

                if (!String.IsNullOrEmpty(redirectURL))
                    Response.Redirect(redirectURL, false);
                else
                {
                    FormsAuthentication.RedirectToLoginPage();
                    Response.Redirect("LogoutSuccessfully.aspx", false);
                }
            }
            catch (Exception ex)
            {
                // LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
        //        //FormsAuthentication.SignOut();
        //        //Session.Abandon();
        //        //FormsAuthentication.RedirectToLoginPage();
        //        //Response.Redirect("LogoutSuccessfully.aspx", false);              

        //        Session.Clear();
        //        Session.Abandon();
        //        Session.RemoveAll();
        //        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
        //        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
        //        FormsAuthentication.SignOut();
        //        FormsAuthentication.RedirectToLoginPage();
        //        Response.Redirect("LogoutSuccessfully.aspx", false);
        //    }
        //    catch (Exception ex)
        //    {
        //       // LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
    }
}