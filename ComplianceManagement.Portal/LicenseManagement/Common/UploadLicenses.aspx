﻿<%@ Page Title="Import Utility" Language="C#" MasterPageFile="~/LicenseManagement.Master"
     AutoEventWireup="true" CodeBehind="UploadLicenses.aspx.cs"
     Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Common.UploadLicenses" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .excelgriddisplay {
            display: none;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .dd_chk_select {
            height: 30px !important;
            text-align: center;
            border-radius: 5px;
        }

        div.dd_chk_select div#caption {
            margin-top: 5px;
        }

        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        input[type=checkbox], input[type=radio] {
            margin: 5px 5px;
            margin-top: 0px;
        }

        .alert {
            padding: 15px 15px 15px 30px;
            margin-bottom: 10px;
            margin-top: 10px;
            border: 1px solid transparent;
        }
    </style>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        //$(document).ready(function () {
        //    setactivemenu('Import / Export Utility');
        //    fhead('Import / Export Utility');
        //});
    </script>

    <script type="text/javascript">

        function showProgress() {
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>
   
    <script language="javascript" type="text/javascript">
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>

    <script type="text/javascript">
        $(function () {
            $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });
        })

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceDetails">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <section class="panel">
                    <div class="col-lg-12 col-md-12 ">

                        <header class="panel-heading tab-bg-primary ">
                                 <ul id="rblRole" class="nav nav-tabs">                                                                                                                                           
                                    <li class="active" id="liCC" runat="server">
                                        <asp:LinkButton ID="lnkCC" OnClick="lnkCC_Click"  PostBackUrl="../Common/UploadCourtCases.aspx"   runat="server">Case</asp:LinkButton>                                           
                                    </li>  
                                      <li class="" id="liLN" runat="server">
                                        <asp:LinkButton ID="lnkLN" OnClick="lnkLN_Click"  PostBackUrl="../Common/UploadCourtCases.aspx" runat="server">Notice</asp:LinkButton>                                           
                                    </li>                                                                                                                                                                                                                                                                                                                            
                                 </ul>                                                                         
                                </header>

                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 250px;overflow-y: auto;">
                            <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                    class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="oplValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                <%--<div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>                                    
                                </div>--%>
                            </asp:Panel>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                        <div class="col-lg-12 col-md-12 ">
                    <div runat="server" id="divCase">
                        <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <li class="active" id="liNotDone" runat="server">
                                    <asp:LinkButton ID="Tab1" class="active" runat="server">Import Utility</asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix" style="clear: both; height: 15px;"></div>
                        <div class="clearfix" style="clear: both;"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0">
                                <asp:RadioButton ID="rdoCCUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Court Cases" OnCheckedChanged="rdoCCUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>

                            <div class="col-md-3 colpadding0">
                                <asp:RadioButton ID="rdoCHUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Case Hearing" OnCheckedChanged="rdoCHUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>

                            <div class="col-md-3 colpadding0">
                                <asp:RadioButton ID="rdoCOUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Case Orders" OnCheckedChanged="rdoCOUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>
                            <div class="col-md-3 colpadding0">
                                <asp:RadioButton ID="rdoCPUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Case Payments" OnCheckedChanged="rdoCPUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>
                        </div>

                        <div class="clearfix" style="clear: both; height: 15px;"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-4 colpadding0 entrycount">
                                <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                            </div>
                            <div class="col-md-2 colpadding0 text-right">
                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                    class="btn btn-primary" OnClick="btnUploadFile_Click" OnClientClick="showProgress()"  data-toggle="tooltip" ToolTip="Upload"/>
                            </div>
                            <div class="col-md-6 colpadding0 text-right">
                                <asp:LinkButton ID="lnksampleForm" class="newlink" Font-Underline="True" OnClick="sampleForm_Click"
                                    data-toggle="tooltip" data-placement="bottom" 
                                    ToolTip="Download Sample Excel Document Format for Court Case/ Case Hearing/ Case Orders/ Case Payments Upload" runat="server">Sample Format</asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div runat="server" id="divNotice" class="tab-pane">
                        <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                            <ul id="rblImportUtilityLN" class="nav nav-tabs">
                                <li class="active" id="liImportUtilityLN" runat="server">
                                    <asp:LinkButton ID="lnkImportUtilityLN" class="active" runat="server">Import Utility</asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix" style="clear: both; height: 15px;"></div>
                        <div class="clearfix" style="clear: both;"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-4 colpadding0">
                                <asp:RadioButton ID="rdoLNUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Legal Notice" OnCheckedChanged="rdoLNUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>

                            <div class="col-md-4 colpadding0">
                                <asp:RadioButton ID="rdoNRUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Notice Responses" OnCheckedChanged="rdoNRUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>

                            <div class="col-md-4 colpadding0">
                                <asp:RadioButton ID="rdoPIUpload" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Payment Info" OnCheckedChanged="rdoPIUpload_CheckedChanged" GroupName="uploadContentGroup" />
                            </div>
                        </div>

                        <div class="clearfix" style="clear: both; height: 15px;"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-4 colpadding0 entrycount">
                                <asp:FileUpload ID="MasterFileUploadLN" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                    runat="server" Display="None" ValidationGroup="oplValidationGroupLN" />
                            </div>
                            <div class="col-md-2 colpadding0 text-right">
                                <asp:Button ID="btnUploadFileLN" runat="server" Text="Upload" ValidationGroup="oplValidationGroupLN"
                                    class="btn btn-primary" OnClick="btnUploadFileLN_Click" OnClientClick="showProgress()" />
                            </div>
                            <div class="col-md-6 colpadding0 text-right">
                                <asp:LinkButton ID="lnksampleFormLN" class="newlink" Font-Underline="True" OnClick="sampleFormLN_Click" 
                                     data-toggle="tooltip" data-placement="bottom" 
                                    ToolTip="Click Here to Download Sample Excel Document Format for Legal Notice/ Notice Responses/ Notice Payments Upload"
                                    runat="server">Sample Format</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                            </div>
                   </section>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lnksampleForm" />
            <asp:PostBackTrigger ControlID="btnUploadFileLN" />
            <asp:PostBackTrigger ControlID="lnksampleFormLN" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
