﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Common
{
    public partial class DownloadDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOnID"])
                    && !string.IsNullOrEmpty(Request.QueryString["ISStatutoryflag"]))
                {
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ScheduledOnID"]);
                    string IsStatutory = Request.QueryString["ISStatutoryflag"];

                    if (IsStatutory == "S")
                    {
                        #region  Statutory
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).ToList();
                                ComplianceFileData = ComplianceDocument;

                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + ScheduledOnID);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + ScheduledOnID + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).ToList();
                                ComplianceFileData = ComplianceDocument;

                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + ScheduledOnID);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + ScheduledOnID + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (IsStatutory == "I")
                    {
                        #region Internal
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {

                                List<GetLicenseInternalComplianceDocumentsView> ComplianceFileData = new List<GetLicenseInternalComplianceDocumentsView>();
                                List<GetLicenseInternalComplianceDocumentsView> ComplianceDocument = new List<GetLicenseInternalComplianceDocumentsView>();

                                ComplianceDocument = GetDocumnetsInternal(ScheduledOnID).ToList();
                                ComplianceFileData = ComplianceDocument;

                                var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + ScheduledOnID);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + ScheduledOnID + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion

                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                List<GetLicenseInternalComplianceDocumentsView> ComplianceFileData = new List<GetLicenseInternalComplianceDocumentsView>();
                                List<GetLicenseInternalComplianceDocumentsView> ComplianceDocument = new List<GetLicenseInternalComplianceDocumentsView>();

                                ComplianceDocument = GetDocumnetsInternal(ScheduledOnID).ToList();
                                ComplianceFileData = ComplianceDocument;

                                var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + ScheduledOnID);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + ScheduledOnID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocument_" + ScheduledOnID + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
            }


            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

    public static List<GetLicenseInternalComplianceDocumentsView> GetDocumnetsInternal(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
    }
}