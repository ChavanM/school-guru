﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLicensesPopup.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.EditLicensesPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>License Details</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->

    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocationPopup.ClientID %>') > -1) {
                    $("#divFilterLocationPopup").show();
                } else {
                    $("#divFilterLocationPopup").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tbxFilterLocationPopup.ClientID %>') > -1) {
                $("#divFilterLocationPopup").show();
            } else if (event.target.id != '<%= tbxFilterLocationPopup.ClientID %>') {
                $("#divFilterLocationPopup").hide();
            } else if (event.target.id == '<%= tbxFilterLocationPopup.ClientID %>') {
                $('<%= tbxFilterLocationPopup.ClientID %>').unbind('click');

                $('<%= tbxFilterLocationPopup.ClientID %>').click(function () {
                    $("#divFilterLocationPopup").toggle("blind", null, 500, function () { });
                });
            }
        });


$(document).ready(function () {
    if ($("#<%=tbxFilterLocationPopup.ClientID %>") != null) {
        $("#<%=tbxFilterLocationPopup.ClientID %>").unbind('click');

        $("#<%=tbxFilterLocationPopup.ClientID %>").click(function () {
            $("#divFilterLocationPopup").toggle("blind", null, 500, function () { });
        });
    }
});
function InitializeRequest(sender, args) { }




function hideDivBranch() {
    $('#divFilterLocationPopup').hide("blind", null, 0, function () { });
}

function initializeJQueryUI(textBoxID, divID) {
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}
      

function checkAll(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
            cbox.checked = cb.checked;
        }
    }
}


function UncheckHeader() {
    var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}
        $(document).ready(function () {

            BindControls();

        });
        function BindControls() {
            var startDate = new Date();
            $(function () {

                $('input[id*=tbxStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {

                $('input[id*=tbxEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

        }

function initializeDatePicker(date) {

    var startDate = new Date();
    $("#<%= tbxStartDate.ClientID %>").datepicker({
        dateFormat: 'dd-mm-yy',
        defaultDate: startDate,
        numberOfMonths: 1,
        //minDate: startDate,
        onClose: function (startDate) {
            $("#<%= tbxStartDate.ClientID %>").datepicker("option", "minDate", startDate);
        }
    });
        if (date != null) {
            $("#<%= tbxStartDate.ClientID %>").datepicker("option", "defaultDate", date);

        }

        var EndDate = new Date();
        $("#<%= tbxEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: EndDate,
            numberOfMonths: 1,
            //minDate: EndDate,
            onClose: function (EndDate) {
                $("#<%= tbxEndDate.ClientID %>").datepicker("option", "minDate", EndDate);
            }
        });
            if (date != null) {
                $("#<%= tbxEndDate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>

</head>
<body style="background: none !important; overflow-y: hidden;">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <input type="hidden" id="hiddenColor" style="display: none;" />

        <div class="mainDiv" style="background-color: #f7f7f7;">

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView" class="boxscroll do-nicescroll4" style="height: 500px; overflow-y: auto;">
                <div style="width: 100%; float: left; margin-bottom: 15px">
                    <div class="container">

                        <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                            <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                                <asp:ValidationSummary ID="VSLicPopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="LicPopUpValidationGroup" />
                                <asp:CustomValidator ID="cvLicPopUp" runat="server" EnableClientScript="False"
                                    ValidationGroup="LicPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </asp:Panel>
                        </div>

                        <div id="divLicenseDialog" class="row Dashboard-white-widget">
                                <asp:UpdatePanel ID="upLicense" runat="server" UpdateMode="Conditional" OnLoad="upLicense_Load">
                                <ContentTemplate>

                            <div class="row col-lg-12 col-md-12">

                                <div class="form-group col-md-6">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="ddlLicenseTypePopup" class="control-label">License Type</label>
                                    <asp:DropDownList runat="server" ID="ddlLicenseTypePopup" Enabled="false" CssClass="form-control" Width="100%" AutoPostBack="true">
                                        <asp:ListItem Text="Statutory" Value="S"></asp:ListItem>
                                        <asp:ListItem Text="Internal" Value="I"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="tbxFilterLocationPopup" class="control-label">Location</label>
                                    <div id="divBranchesPopup" runat="server">
                                        <asp:TextBox runat="server" ID="tbxFilterLocationPopup" CssClass="form-control" Width="100%" Enabled="false" />
                                        <div style="position: absolute; z-index: 10; display: none;" id="divFilterLocationPopup">
                                            <asp:TreeView runat="server" ID="tvFilterLocationPopup" BackColor="White" BorderColor="Black" OnSelectedNodeChanged="tvFilterLocationPopup_SelectedNodeChanged"
                                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px" NodeStyle-ForeColor="Black"
                                                Style="overflow: auto; display: none; margin-left: 37.5%" ShowLines="true">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-3" style="display: none;">
                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label for="ddlDepartment" class="control-label">Department</label>
                                <asp:DropDownList runat="server" ID="ddlDepartment" CssClass="form-control" Width="100%"
                                    AutoPostBack="true" Enabled="false" />
                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">

                                <div class="form-group col-md-6 ">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="tbxLicenseNo" class="control-label">License Number</label>
                                    <asp:TextBox runat="server" ID="tbxLicenseNo" CssClass="form-control" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="License number can not be empty."
                                        ControlToValidate="tbxLicenseNo" runat="server" ValidationGroup="LicPopUpValidationGroup"
                                        Display="None" />
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="tbxLicenseTitle" class="control-label">License Title</label>
                                    <asp:TextBox runat="server" ID="tbxLicenseTitle" CssClass="form-control" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="License title can not be empty."
                                        ControlToValidate="tbxLicenseTitle" runat="server" ValidationGroup="LicPopUpValidationGroup"
                                        Display="None" />
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">

                                <div class="form-group col-md-6 ">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="tbxStartDate" class="control-label">License Start Date</label>
                                    <asp:TextBox runat="server" CssClass="form-control" Width="100%" Enabled="false" ID="tbxStartDate"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label for="tbxEndDate" class="control-label">License End Date</label>
                                    <asp:TextBox runat="server" Enabled="false" ID="tbxEndDate" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">

                                <div class="form-group col-md-6 ">
                                    <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label for="tbxApplicationdays" class="control-label">Application (Days)</label>
                                    <asp:TextBox runat="server" ID="tbxApplicationdays" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>

                                <div class="form-group col-md-6 ">
                                    <label for="fuSampleFile" class="control-label">Upload  Document(s)</label>
                                    <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" Style="color: black" />
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="row col-lg-12 col-md-12">
                                <div class="form-group col-md-6 ">
                                    <table style="width: 100%">
                                        <tr>
                                            <label style="width: 23%; display: block; float: left; font-size: 13px; color: #333;">Versions</label>
                                            <td style="width: 77%;">
                                                <table width="100%" style="text-align: left">
                                                    <thead>
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                                    OnItemCommand="rptLicenseVersion_ItemCommand"
                                                                    OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblldoc1">
                                                                            <thead>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                            ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                    <td>
                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                            ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                        </asp:LinkButton>
                                                                                        <asp:Label ID="lblSlashReview1" Text="/" Style="color: blue; display: none;" runat="server" />
                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDoc1" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue; display: none;"
                                                                                            runat="server" Font-Underline="false" />
                                                                                        <asp:Label ID="lblpathReviewDoc1" runat="server" Style="display: none"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                            <td valign="top">
                                                                <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                                    OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblLicenseDocumnets">
                                                                            <thead>
                                                                                <th>Compliance Related Documents</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                    ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                </asp:LinkButton></td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                                <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                                    OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblLicenseWorkingFiles">
                                                                            <thead>
                                                                                <th>Compliance Working Files</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                    ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                </asp:LinkButton></td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                            <div class="form-group col-md-12 text-center">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                    ValidationGroup="LicPopUpValidationGroup" />
                             <%--   <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary"  data-dismiss="modal"  AutoPostBack="true"
                                     OnClientClick="$('#divLicenseDialog').dialog('close');" />--%>
                            </div>

                            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                            </div>

                             </ContentTemplate>
                            <triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </triggers>
                            </asp:UpdatePanel>
                        </div>
                    <%--divLicenseDialog END--%>
                </div>
            </div>
        </div>

        </div>
    </form>
</body>
</html>
