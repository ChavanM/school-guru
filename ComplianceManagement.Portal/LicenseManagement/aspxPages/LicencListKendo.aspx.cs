﻿using System;
using System.Collections.Generic;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class LicencListKendo : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Custid;
        protected static int UserID;
        protected static string Role;
        protected static string type;
        protected string Internalsatutory;
        protected int LicenseTypeID;
        protected int StatusFlagID;
        protected static string Authorization;
        protected static List<Int32> roles;
        protected string ChangeFlag;
        protected string StatusFlag;
        public static bool HideStatusOnActiveEnable;
        public static bool HideAddNew;
        public static bool IPRStatus;
        protected void Page_Load(object sender, EventArgs e)
        {
            HideStatusOnActiveEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "HideStatusOnActive");
            HideAddNew = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "LicenseType");
            IPRStatus = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "IPRStatus_Symphony");
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Custid = Convert.ToString(AuthenticationHelper.CustomerID);
            UserID = AuthenticationHelper.UserID;
            Role = AuthenticationHelper.Role;
            roles = GetAssignedroleid(AuthenticationHelper.UserID);
            LicenseTypeID = -1;
            StatusFlag = Convert.ToString(Request.QueryString["Status"]);
          //  ChangeFlag = "P";
             if(StatusFlag == "PR")
            {
                StatusFlag = "PendingForReview";
            }
            bool LicenseCustID = CheckForClient(Convert.ToInt32(Custid), "LicenseType");
        
         
            if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
            {
                string val = Request.QueryString["ISI"].ToString().Trim();
                if(val == "Statutory")
                {
                    Internalsatutory = "S";
                }
                else
                    if(val == "Internal")
                {
                    Internalsatutory = "I";
                }
               // Internalsatutory= val;
            }
            else
            {
                Internalsatutory = "S";
            }

            //  }

        }
        public static bool CheckForClient(int CustomerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == CustomerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.Sp_GetAllAssignedRoles(Userid)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList(); 
                return roles;
            }
        }   
    }
}