﻿<%@ Page Title="License(s) List :: My Workspace " Language="C#" MasterPageFile="~/LicenseManagement.Master"
    AutoEventWireup="true" CodeBehind="InternalLicenseActivation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.InternalLicenseActivation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <style type="text/css">
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .panel-heading {
            background: #ffffff;
        }

        .hiddencol {
            display: none;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #fff;
        }
    </style>
    <script type="text/javascript">

        function fredv(type) {
            if (type == "P") {
                $('#ContentPlaceHolder1_liAssigned1').addClass('active');
                $('#ContentPlaceHolder1_liNotAssigned1').removeClass('active');
                $('#ContentPlaceHolder1_assignedTab').removeClass('active');
                $('#ContentPlaceHolder1_assignedTab').removeClass('active');
                $('#ContentPlaceHolder1_assignedTab').addClass('active', 'tab-pane active');
                $('#ContentPlaceHolder1_notAssignedTab').removeClass('active');
                $('#ContentPlaceHolder1_notAssignedTab').removeClass('class');
                $('#ContentPlaceHolder1_notAssignedTab').addClass('class', 'tab-pane');
            } else if (type == "R") {
                $('#ContentPlaceHolder1_liAssigned1').removeClass('active');
                $('#ContentPlaceHolder1_liNotAssigned1').addClass('active');
                $('#ContentPlaceHolder1_notAssignedTab').addClass('active');

                $('#ContentPlaceHolder1_notAssignedTab').removeClass('active');
                $('#ContentPlaceHolder1_notAssignedTab').addClass('active', 'tab-pane active');
                $('#ContentPlaceHolder1_assignedTab').removeClass('active');
                $('#ContentPlaceHolder1_assignedTab').removeClass('class');
                $('#ContentPlaceHolder1_assignedTab').addClass('class', 'tab-pane');
            }
        }


        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('License(s)');
            if (ReadQuerySt('r') == 'yes') {
                $('#main-content').css('margin-left', '10px');
                $('.header').css('display', 'none');
                $('#sidebar').css('display', 'none');
                $('#pagetype').parent('div').parent('.wrapper').parent('div.row').hide();
            }
        });
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function ReadQuerySt(e) {
            try {
                for (hu = window.location.search.substring(1), gy = hu.split('&'), i = 0; i < gy.length; i++)
                    if (ft = gy[i].split('='), ft[0].toLowerCase() == e.toLowerCase()) return ft[1];
                return ''
            } catch (t) {
                return ''
            }
        }
        $(document).ready(function () {
            $(".notification-row > ul > .dropdown").click(function () { $('.notification-row > ul > .dropdown').addClass('open') });
        });

        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });

        $(window).bind("load", function () {
            $('#updateProgress').hide();
        });

        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        function ShowDialog(LicenseInstanceID, status) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPage.aspx?AccessID=" + LicenseInstanceID + "&Status=" + status);
        };

        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }

        function ShowLicDialog(LicNo) {
             
            $('#divShowLicDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showlicdetails').attr('width', '100%');
            $('#showlicdetails').attr('height', '550px');
            $('#showlicdetails').attr('src', "/LicenseManagement/aspxPages/AddNewInternalLicenseDetailsPage.aspx?AccessID=" + LicNo);

        };

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

function ClosePopLicense() {
    $('#AddLicenseTypeDetailsPop').modal('hide');
    document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
}
    </script>

    <!--lic activation code start-->
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            BindControls();

            if ($("#<%=tbxFilterLocation.ClientID %>") != null) {
                $("#<%=tbxFilterLocation.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }

             if ($("#<%=tbxFilterLocation1.ClientID %>") != null) {
                $("#<%=tbxFilterLocation1.ClientID %>").unbind('click');

                $("#<%=tbxFilterLocation1.ClientID %>").click(function () {
                    $("#divBranches1").toggle("blind", null, 500, function () { });
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            applyCSSDate();

            $("html").getNiceScroll().resize();

            $('.gridtextbox').blur(function () {
                var chk = $(this).parent('td').parent('tr').find('.chkgridbox');
                var chk1 = $(chk).find('input');
                if ($(this).val() == '' && $(chk1).is(':checked') == true) {
                    $(this).css('border', '1px solid red');
                    valid = false;
                } else {
                    $(this).css('border', '1px solid #c7c7cc');
                }
            });

            //date
            $('.gridtextbox').change(function () {
                var chk = $(this).parent('td').parent('tr').find('.chkgridbox');
                var chk1 = $(chk).find('input');
                if ($(this).val() == '' && $(chk1).is(':checked') == true) {
                    $(this).css('border', '1px solid red');
                    valid = false;
                } else {
                    $(this).css('border', '1px solid #c7c7cc');
                }
            });
            $('.date').blur(function () {
                var chk = $(this).parent('div').parent('div').parent('td').parent('tr').find('.chkgridbox');
                var chk1 = $(chk).find('input');
                if ($(this).val() == '' && $(chk1).is(':checked') == true) {
                    $(this).css('border', '1px solid red');
                    valid = false;
                } else {
                    $(this).css('border', '1px solid #c7c7cc');
                }
            });

            $('.chkgridboxhead > input').click(function () {
                if ($(this).is(':checked')) {
                    $('.chkgridbox > input').prop('checked', true)

                } else {
                    $('.chkgridbox > input').prop('checked', false)
                }
            });

        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {

                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }

        function hideDivBranch1() {
            $('#divBranches1').hide("blind", null, 0, function () { });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function applyCSSDate() {
            $('input[id*=txtStartDate]').removeClass();
            $('input[id*=txtStartDate]').addClass('form-control');

            $('input[id*=txtEndDate]').removeClass();
            $('input[id*=txtEndDate]').addClass('form-control');

            $('input[id*=txtApplicationDays]').removeClass();
            $('input[id*=txtApplicationDays]').addClass('form-control');
        }
        function fvalidateinput() {

            var valid = true;
            var c = 0;
            var ram = 0;
            for (var s = 0; s < $('.gridtextbox').length; s++) {
                var chk = $($('.gridtextbox')[s]).parent('td').parent('tr').find('.chkgridbox');
                var chk1 = $(chk).find('input');
                if (chk1.length == 0) {
                    chk = $($('.gridtextbox')[s]).parent('div').parent('div').parent('td').parent('tr').find('.chkgridbox');
                    chk1 = $(chk).find('input');
                    if ($(chk1).is(':checked') == true) {
                        ram++
                    }
                }
            }
            if (ram == 0) {
                alert("please check checkbox first");
                return false;
            }
            for (var i = 0; i < $('.gridtextbox').length; i++) {
                var chk = $($('.gridtextbox')[i]).parent('td').parent('tr').find('.chkgridbox');
                var chk1 = $(chk).find('input');
                if (chk1.length == 0) {
                    chk = $($('.gridtextbox')[i]).parent('div').parent('div').parent('td').parent('tr').find('.chkgridbox');
                    chk1 = $(chk).find('input');
                    if ($(chk1).is(':checked') == true) {
                        c++;
                    }
                }
                //required fields
                if ($($('.gridtextbox')[i]).val() == '' && $(chk1).is(':checked') == true) {
                    $($('.gridtextbox')[i]).css('border', '1px solid red');
                    valid = false;
                } else {
                    $($('.gridtextbox')[i]).css('border', '1px solid #c7c7cc');
                }
            }
            if (valid) {
                if (c != 0) {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    confirm_value.value = "";
                    confirm_value
                    if (confirm("Are you certain you want to activate this License? , to continue saving click OK!!")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            } else {
                return false;
            }

        }

        function validateCheckBoxes() {
             var isValid = false;
             var gridView = document.getElementById('<%= grdLicenseList1.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }

        function checkName(thisvalues) {
            var count = 0;
            var currentval = $('#' + thisvalues.id).val();

            if (currentval != "" && thisvalues.id.search("txtLicenseNo") != -1) {
                $('.gridtextbox').each(function () {
                    console.log(this.value + ',' + this.id);
                    if (currentval.trim().toLowerCase() == this.value.trim().toLowerCase() && thisvalues.id != this.id) {
                        count++;
                    }
                });

                if (count > 0) {
                    alert('License No =  ' + currentval + ' already exists');
                    $('#' + thisvalues.id).val("");
                }
            }
            return false;
        }

        function checkTitleName(thisvalues) {
            var count = 0;
            var currentval = $('#' + thisvalues.id).val();

            if (currentval != "" && thisvalues.id.search("txtTitle") != -1) {
                $('.gridtextbox').each(function () {

                    console.log(this.value + ',' + this.id);
                    if (currentval.trim().toLowerCase() == this.value.trim().toLowerCase() && thisvalues.id != this.id && this.id.search("txtTitle") != -1) {
                        count++;
                    }
                });

                if (count > 0) {
                    alert('License Title =  ' + currentval + ' already exists');
                    $('#' + thisvalues.id).val("");
                }
            }
            return false;
        }


        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }
    </script>
    <!--lic activation code end-->


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-md-12 colpadding0 text-right">
                <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgColor-gray" style="height: 30px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom col-md-6 plr0">
                                    Add/Edit License Detail(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                            </div>

                            <div class="modal-body" style="background-color: #f7f7f7;">
                                <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="col-lg-12 col-md-12" style="padding-left: 1px; padding-right: 1px;">
                <section class="panel">

                    <div class="clearfix"></div>

                    <div class="panel-body">
                        <ul class="nav nav-tabs calender-li" role="tablist">
                            <li id="liAssigned1" style="cursor: pointer;" runat="server" onclick="fredv('P');"><a id="liAssigned" runat="server" aria-controls="assignedTab" role="tab" data-toggle="tab">License Creation</a></li>
                            <li id="liNotAssigned1" runat="server" style="cursor: pointer;" onclick="fredv('R');"><a id="liNotAssigned" runat="server" aria-controls="notAssignedTab" role="tab" data-toggle="tab">New Assignment</a></li>
                        </ul>

                        <div class="tab-content" style="padding-top: 2px">
                            <div role="tabpanel" class="tab-pane" runat="server" id="assignedTab">
                                <div class="tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="row Dashboard-white-widget" style="padding: 5px 0px 5px;">
                                            <div class="dashboard" id="div1" runat="server">
                                                <div class="col-md-12 colpadding0">
                                                    <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" Style="padding-left: 5%" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="LicenseListPageValidationGroup" />
                                                    <asp:CustomValidator ID="cvErrorLicenseListPage" Style="padding-left: 5%" runat="server" EnableClientScript="true"
                                                        ValidationGroup="LicenseListPageValidationGroup" Display="none"
                                                        class="alert alert-block alert-danger fade in" />
                                                </div>
                                                <div class="row">
                                                    <div class="form-group required col-md-2" id="CustomerList" runat="server">
                                                        <label for="ddlFilterCustomer" class="control-label">Customer</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlFilterCustomer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            DataPlaceHolder="Select Customer"
                                                            class="form-control" Width="100%" Enabled="true" OnSelectedIndexChanged="ddlFilterCustomer_SelectedIndexChanged" AutoPostBack="true" />
                                                        <asp:RequiredFieldValidator ID="rfvCustomer" ErrorMessage="Please Select Customer"
                                                            ControlToValidate="ddlFilterCustomer" runat="server" ValidationGroup="LicenseListPageValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="form-group required col-md-2" style="display: none;">
                                                        <label for="ddlIsStatutoryNonStatutory" class="control-label">Type</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlIsStatutoryNonStatutory" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            DataPlaceHolder="Select"
                                                            class="form-control" Width="100%" Enabled="true">
                                                            <asp:ListItem Text="Internal" Value="I" Selected="True" />
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                    <div class="form-group required col-md-2">
                                                        <label for="ddlLicenseType" class="control-label">License Type</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            class="form-control" Width="100%" DataPlaceHolder="Select License Type" OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                        <asp:RequiredFieldValidator ID="rfvLicenseType" ErrorMessage="Please Select License Type"
                                                            ControlToValidate="ddlLicenseType" runat="server" ValidationGroup="LicenseListPageValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-right: -2%; margin-left: 5px;">
                                                        <label for="tvFilterLocation" class="control-label" style="display: block; float: left;">
                                                            Entity/Sub-Entity/Location</label>
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                                        <asp:TextBox runat="server" ID="tbxFilterLocation" CssClass="form-control bgColor-white" ReadOnly="true" autocomplete="off" AutoPostBack="true" />

                                                        <div style="position: absolute; z-index: 10; width: 92%" id="divBranches">
                                                            <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" ShowCheckBoxes="All" onclick="OnTreeClick(event)" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged"
                                                                Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                ShowLines="true">
                                                            </asp:TreeView>
                                                            <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="select" />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Sub-Entity/Location."
                                                            ControlToValidate="tbxFilterLocation" runat="server" ValidationGroup="LicenseListPageValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="form-group col-md-2 d-none">
                                                        <label for="ddlDepartment" class="control-label">Department</label>
                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                            <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 colpadding0" style="float: right; padding-top: 16px;">
                                                        <asp:LinkButton ID="lnkBtn_RebindGrid" Visible="false" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>

                                                    </div>

                                                     <div class="form-group col-md-2"  style="float: right;">
                                                        <asp:LinkButton ID="btnAddnew" OnClick="btnAddnew_Click" class="btn btn-primary" runat="server">Add New</asp:LinkButton>
                                                    </div>

                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>
                                                <div class="row">
                                                    <div class="form-group required col-md-2">
                                                        <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" class="btn btn-primary" runat="server">Download</asp:LinkButton>
                                                    </div>
                                                    <div class="form-group required col-md-2">
                                                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" Style="color: black" />
                                                    </div>
                                                    <div class="form-group required col-md-2">
                                                        <asp:LinkButton ID="btnUpload" OnClick="btnUpload_Click" class="btn btn-primary" runat="server">Upload</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>

                                                <div class="row" id="gridandpaging">
                                                    <div class="row">

                                                        <div class="col-md-12 colpadding0">
                                                            <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID"
                                                                OnRowCommand="grdLicenseList_RowCommand" OnSorting="grdLicenseList_Sorting" OnRowCreated="grdLicenseList_RowCreated"
                                                                OnRowEditing="grdLicenseList_RowEditing" OnRowUpdating="grdLicenseList_RowUpdating"
                                                                OnRowCancelingEdit="grdLicenseList_RowCancelingEdit">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" ID="chkboxhead" CssClass="chkgridboxhead" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" ID="chkbox" CssClass="chkgridbox" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Compliance ID" ItemStyle-Width="10%" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtComplianceID" TextMode="SingleLine" Text='<%# Eval("ID") %>' autocomplete="off" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Compliance" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                                                <asp:Label runat="server" ID="branchId" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchID") %>' Style="display: none"></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="License No." ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtLicenseNo" TextMode="SingleLine"
                                                                                CssClass="form-control gridtextbox" autocomplete="off" onfocusout="checkName(this)" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="License Title" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtTitle" TextMode="SingleLine"
                                                                                CssClass="form-control gridtextbox" autocomplete="off" />
                                                                            <%--onfocusout="checkTitleName(this)" --%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Start date" ItemStyle-Width="12%">
                                                                        <ItemTemplate>
                                                                            <%--<div class="form-group col-md-3" style="display: block; width: 100%;padding-left: 3px;padding-right: 3px;">--%>
                                                                            <div class="input-group date">
                                                                                <%-- <span class="input-group-addon">
                                                                    <span class="fa fa-calendar color-black"></span>
                                                                </span> --%>
                                                                                <asp:TextBox ID="txtStartDate" runat="server" placeholder="DD-MM-YYYY"
                                                                                    class="form-control gridtextbox date" autocomplete="off" />
                                                                            </div>
                                                                            <%-- </div>--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Expiry date" ItemStyle-Width="12%">
                                                                        <ItemTemplate>
                                                                            <%--<div class="form-group col-md-3" style="display: block; width: 100%;padding-left: 3px;padding-right: 3px;">--%>
                                                                            <div class="input-group date">
                                                                                <%--<span class="input-group-addon">
                                                                    <span class="fa fa-calendar color-black"></span>
                                                                </span>--%>
                                                                                <asp:TextBox ID="txtEndDate" runat="server" placeholder="DD-MM-YYYY" class="form-control gridtextbox date" autocomplete="off" />
                                                                            </div>
                                                                            <%--</div>--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Application days" ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                <div>
                                                                                    <asp:TextBox ID="txtApplicationDays" TextMode="Number" data-attr="<%#Container.DataItemIndex+1 %>" runat="server" placeholder="" class="form-control col-md-8 gridtextbox date" autocomplete="off" onkeydown="return ((event.keyCode>=65 && event.keyCode>=96 && event.keyCode<=105)||!(event.keyCode>=65) && event.keyCode!=32);" />
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Upload Files" ItemStyle-Width="8%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; width: 178px; text-overflow: ellipsis; white-space: nowrap;">
                                                                                <asp:FileUpload ID="LicenseFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="File Number" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtfileNo" TextMode="SingleLine"
                                                                                CssClass="form-control gridtextbox" autocomplete="off" onfocusout="checkName(this)" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Physical Location" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtphysicallocation" TextMode="SingleLine"
                                                                                CssClass="form-control gridtextbox" autocomplete="off" onfocusout="checkName(this)" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                      <asp:TemplateField HeaderText="Cost" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtCost" TextMode="SingleLine" style="width: 85px;"
                                                                                CssClass="form-control gridtextbox" autocomplete="off" onfocusout="checkName(this)" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Add New" ItemStyle-Width="8%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("CustomerBranchID")+","+Eval("LicensetypeID")%>'
                                                                                AutoPostBack="true" CommandName="ViewLicensePopup" ID="lnkViewLicenseInstance" runat="server"
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View">
                                                            <img src='<%# ResolveUrl("~/Images/add.png")%>' alt="View"/>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <PagerSettings Visible="false" />
                                                                <PagerTemplate>
                                                                </PagerTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Record Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 colpadding0">
                                                            <div class="col-md-10 colpadding0">
                                                                <div runat="server" id="DivRecordsScrum" style="color: #999">
                                                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                 <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 text-right colpadding0">
                                                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                                    <asp:ListItem Text="5" />
                                                                    <asp:ListItem Text="10" Selected="True" />
                                                                    <asp:ListItem Text="20" />
                                                                    <asp:ListItem Text="50" />
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-1 text-right colpadding0">
                                                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                            <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                                                            </asp:LinkButton>
                                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row" id="noteId" runat="server">
                                            <p style="color: red;"><strong>Note:</strong> Relevent compliance has been not mapped to any user</p>
                                        </div>
                                        <div class="form-group col-md-12 text-center" id="DivLicenseSave" runat="server">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLicense" CssClass="btn btn-primary"
                                                OnClick="btnSaveLicense_Click" OnClientClick="return fvalidateinput();"
                                                ValidationGroup=""></asp:Button>
                                            <%--scrollUpPage()--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" runat="server" id="notAssignedTab">
                                <div class="tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="row Dashboard-white-widget" style="padding: 5px 0px 5px;">
                                            <div class="dashboard" id="divMainView" runat="server">
                                                <div class="col-md-12 colpadding0">
                                                    <asp:ValidationSummary ID="vsLicenseListPage1" runat="server" Display="none" Style="padding-left: 5%" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="LicenseListPageValidationGroup1" />
                                                    <asp:CustomValidator ID="cvErrorLicenseListPage1" Style="padding-left: 5%" runat="server" EnableClientScript="true"
                                                        ValidationGroup="LicenseListPageValidationGroup1" Display="none"
                                                        class="alert alert-block alert-danger fade in" />
                                                </div>
                                                <div class="row">
                                                    <div class="form-group required col-md-2" id="CustomerList1" runat="server">
                                                        <label for="ddlFilterCustomer1" class="control-label">Customer</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlFilterCustomer1" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            DataPlaceHolder="Select Customer"
                                                            class="form-control" Width="100%" Enabled="true" OnSelectedIndexChanged="ddlFilterCustomer1_SelectedIndexChanged" AutoPostBack="true" />
                                                        <asp:RequiredFieldValidator ID="rfvCustomer1" ErrorMessage="Please Select Customer"
                                                            ControlToValidate="ddlFilterCustomer1" runat="server" ValidationGroup="LicenseListPageValidationGroup1" Display="None" />
                                                    </div>
                                                    <div class="form-group required col-md-2" style="display: none;">
                                                        <label for="ddlIsStatutoryNonStatutory1" class="control-label">Type</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlIsStatutoryNonStatutory1" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            DataPlaceHolder="Select"
                                                            class="form-control" Width="100%" Enabled="true">
                                                            <asp:ListItem Text="Internal" Value="I" Selected="True" />
                                                        </asp:DropDownListChosen>
                                                    </div>
                                                    <div class="form-group required col-md-2">
                                                        <label for="ddlLicenseType1" class="control-label">License Type</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType1" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                            class="form-control" Width="100%" DataPlaceHolder="Select License Type" OnSelectedIndexChanged="ddlLicenseType1_SelectedIndexChanged">
                                                        </asp:DropDownListChosen>
                                                        <asp:RequiredFieldValidator ID="rfvLicenseType1" ErrorMessage="Please Select License Type"
                                                            ControlToValidate="ddlLicenseType1" runat="server" ValidationGroup="LicenseListPageValidationGroup1" Display="None" />
                                                    </div>
                                                    <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-right: -2%; margin-left: 5px;">
                                                        <label for="tvFilterLocation1" class="control-label" style="display: block; float: left;">
                                                            Entity/Sub-Entity/Location</label>
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;" class="control-label">*</label>
                                                        <asp:TextBox runat="server" ID="tbxFilterLocation1" CssClass="form-control bgColor-white" ReadOnly="true" autocomplete="off" AutoPostBack="true" />
                                                        <div style="position: absolute; z-index: 10; width: 92%" id="divBranches1">
                                                            <asp:TreeView runat="server" ID="tvFilterLocation1" BackColor="White" BorderColor="Black"
                                                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" ShowCheckBoxes="All" onclick="OnTreeClick(event)"  OnSelectedNodeChanged="tvFilterLocation1_SelectedNodeChanged"
                                                                Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                                ShowLines="true">
                                                            </asp:TreeView>
                                                            <asp:Button ID="btnlocation1" runat="server" OnClick="btnlocation1_Click" Text="select" />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="rfvBranch1" ErrorMessage="Please Select Entity/Sub-Entity/Location."
                                                            ControlToValidate="tbxFilterLocation1" runat="server" ValidationGroup="LicenseListPageValidationGroup1" Display="None" />
                                                    </div>
                                                    <div class="form-group col-md-2 d-none">
                                                        <label for="ddlDepartment" class="control-label">Department</label>
                                                        <div class="col-md-11 col-sm-11 col-xs-11 plr0">
                                                            <asp:DropDownListChosen runat="server" ID="ddlDepartment1" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 colpadding0" style="float: right; padding-top: 16px;">
                                                        <asp:LinkButton ID="lnkBtn_RebindGrid1" Visible="false" OnClick="lnkBtn_RebindGrid1_Click" Style="display: none;" runat="server"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>
                                                <div class="clearfix"></div>

                                                <div class="row" id="gridandpaging1">
                                                    <div class="row">

                                                        <div class="col-md-12 colpadding0">
                                                            <asp:GridView runat="server" ID="grdLicenseList1" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID"
                                                                OnRowCommand="grdLicenseList1_RowCommand" OnRowDataBound="grdLicenseList1_RowDataBound" OnSorting="grdLicenseList1_Sorting" OnRowCreated="grdLicenseList1_RowCreated"
                                                                OnRowEditing="grdLicenseList1_RowEditing" OnRowUpdating="grdLicenseList1_RowUpdating"
                                                                OnRowCancelingEdit="grdLicenseList1_RowCancelingEdit">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox runat="server" ID="chkboxhead1" CssClass="chkgridboxhead1" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox runat="server" ID="chkbox1" CssClass="chkgridbox1" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Compliance ID" ItemStyle-Width="10%" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txtComplianceID1" TextMode="SingleLine" Text='<%# Eval("ID") %>' autocomplete="off" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Compliance" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                                                <asp:Label runat="server" ID="lblbranchId1" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchID") %>' Style="display: none"></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Performer" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("PerformerID") %>' Style="display: none" />
                                                                            <asp:DropDownList ID="ddlPerformer" class="form-control gridtextbox" Width="100%" DataPlaceHolder="Select Performer" runat="server">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("ReviewerID") %>' Style="display: none" />
                                                                            <asp:DropDownList ID="ddlReviewer" class="form-control" Width="100%" DataPlaceHolder="Select Reviewer" runat="server">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Approver" ItemStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblApprover" runat="server" Text='<%# Eval("ApproverID") %>' Style="display: none" />
                                                                            <asp:DropDownList ID="ddlApprover" class="form-control" Width="100%" DataPlaceHolder="Select Approver" runat="server">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <PagerSettings Visible="false" />
                                                                <PagerTemplate>
                                                                </PagerTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Record Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12 colpadding0">
                                                            <div class="col-md-10 colpadding0">
                                                                <div runat="server" id="DivRecordsScrum1" style="color: #999">
                                                                    <asp:Label ID="lblStartRecord1" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                                <asp:Label ID="lblEndRecord1" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                 <asp:Label ID="lblTotalRecord1" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 text-right colpadding0">
                                                                <asp:DropDownList runat="server" ID="ddlPageSize1" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize1_SelectedIndexChanged">
                                                                    <asp:ListItem Text="5" />
                                                                    <asp:ListItem Text="10" Selected="True" />
                                                                    <asp:ListItem Text="20" />
                                                                    <asp:ListItem Text="50" />
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-1 text-right colpadding0">
                                                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo1" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                                    OnSelectedIndexChanged="DropDownListPageNo1_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                            <asp:LinkButton ID="lnkBtnBindGrid1" OnClick="lnkBtnBindGrid1_Click" Style="float: right; display: none;" Width="100%" runat="server">
                                                            </asp:LinkButton>
                                                            <asp:HiddenField ID="TotalRows1" runat="server" Value="0" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row" id="noteId1" runat="server">
                                            <p style="color: red;"><strong>Note:</strong> Relevent compliance has been not mapped to any user</p>
                                        </div>
                                        <div class="form-group col-md-12 text-center" id="DivLicenseSave1" runat="server">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLicense1" CssClass="btn btn-primary"
                                                OnClick="btnSaveLicense1_Click" OnClientClick="return validateCheckBoxes();"
                                                ValidationGroup=""></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>

             <div class="modal fade" id="divShowLicDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add Internal License Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeCaseModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showlicdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

        </div>
    </div>
</asp:Content>
