﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web;
using System.IO;
using Ionic.Zip;
using System.Configuration;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class EditLicenses : System.Web.UI.Page
    {
        protected bool flag;
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";
        protected static string user_Roles;
        public static string LicenseDocPath = "";
        protected long customerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    string status = Request.QueryString["Status"].ToString().Trim();
                    ddlLicenseStatus.SelectedValue = status;
                }
                else
                {
                    ddlLicenseStatus.SelectedValue = "Status";
                }
                string isstatutoryinternal = "S";
                if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                {
                    isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();
                }
                if (isstatutoryinternal == "S")
                {
                    ddlfilterStatutoryNonStatutory.SelectedValue = "S";
                }
                else if (isstatutoryinternal == "I")
                {
                    ddlfilterStatutoryNonStatutory.SelectedValue = "I";
                }
                user_Roles = AuthenticationHelper.Role;
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
               
                BindLocationFilter();
                BindLicenseType();
                BindGrid();
                BindDepartment();
                //bindPageNumber();
                //ShowGridDetail();
                try
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        public void BindLicenseType()
        {
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            string isstatutoryinternal = "S";
            if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";
            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();
            ddlLicenseType.Items.Insert(0, new ListItem("License Type Name", "-1"));
        }
        private void BindLocationFilter()
        {
            try
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                var LocationList = new List<int>();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvErrorLicenseListPage.IsValid = false;
                //cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvErrorLicenseListPage.IsValid = false;
                //cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlfilterStatutoryNonStatutory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLicenseType();
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        public void BindGrid()
        {
            try
            {
                int CustomerID = 0;
                //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //{
                //    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //}
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int branchID = -1;
                int deptID = -1;
                string licenseStatus = string.Empty;
                long licenseTypeID = -1;
                int departmentTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (ddlLicenseStatus.SelectedValue != "Status")
                {
                    licenseStatus = ddlLicenseStatus.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDepartment.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(CustomerID), branchID);
                //List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();
                List<Lic_SP_MyWorkspaceDetailEdit_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetailEdit_Result>();
                string isstatutoryinternal = "S";
                if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                 
                if (AuthenticationHelper.Role == "MGMT")
                {
                    MasterTransction = LicenseMgmt.GetAllLicenseDetialsEdit(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                        branchList, deptID, licenseStatus, licenseTypeID, "MGMT", isstatutoryinternal, tbxtypeTofilter.Text);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    MasterTransction = LicenseMgmt.GetAllLicenseDetialsEdit(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "CADMN", isstatutoryinternal, tbxtypeTofilter.Text);

                }
                else if (roles.Contains(3) && roles.Contains(4))
                {

                    MasterTransction = LicenseMgmt.GetAllLicenseDetialsEdit(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                         branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal, tbxtypeTofilter.Text);

                }
                else
                {

                    MasterTransction = LicenseMgmt.GetAllLicenseDetialsEdit(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal, tbxtypeTofilter.Text);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                Session["TotalRows"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }

                Session["grdDetailData"] = (MasterTransction).ToDataTable();
                if (MasterTransction.Count > 0)
                    btnExportExcel.Enabled = true;
                else
                    btnExportExcel.Enabled = false;

                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            //bindPageNumber();
            //ShowGridDetail();

            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLicenseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter2", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = 1;

            if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                if (Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString()) != 0)
                    chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            grdLicenseList.PageIndex = chkSelectedPage - 1;
            grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            ShowGridDetail();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLicenseList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void DeleteInternal(int licenseId, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ltlmToUpdate = (from row in entities.Lic_tbl_InternalLicenseInstance
                                    where row.ID == licenseId
                                    select row).FirstOrDefault();



                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void Delete(int licenseId, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ltlmToUpdate = (from row in entities.Lic_tbl_LicenseInstance
                                    where row.ID == licenseId
                                    select row).FirstOrDefault();



                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static bool CheckLicenseransactionExists(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckLicenseISAssigned(LicenseID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckInternalLicenseransactionExists(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckInternalComplianceISAssigned(LicenseID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void grdLicenseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                //{
                //    string isstatutoryinternal = "S";
                //    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                //    {
                //        isstatutoryinternal = "S";
                //    }
                //    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                //    {
                //        isstatutoryinternal = "I";
                //    }

                //    clearLicenseControls();
                //    BindDepartments();
                //    BindLocationFilterPopup();
                //    long licenseID = 0;
                //    long complianceinstanceid = 0;
                //    long compliancescheduleonid = 0;
                //    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                //    if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[0])))
                //    {
                //        if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[1])))
                //        {
                //            if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[2])))
                //            {
                //                if (isstatutoryinternal == "S")
                //                {
                //                    #region Staturory
                //                    licenseID = Convert.ToInt32(commandArgs[0]);
                //                    complianceinstanceid = Convert.ToInt32(commandArgs[1]);
                //                    compliancescheduleonid = Convert.ToInt32(commandArgs[2]);

                //                    ViewState["LicenseID"] = licenseID;
                //                    ViewState["complianceinstanceid"] = complianceinstanceid;
                //                    ViewState["compliancescheduleonid"] = compliancescheduleonid;
                //                    if (licenseID != 0)
                //                    {
                //                        var LicensedocumentVersionData = LicenseDocumentManagement.GetFileData(licenseID, -1).Select(x => new
                //                        {
                //                            ID = x.ID,
                //                            ScheduledOnID = x.ScheduledOnID,
                //                            LicenseID = x.LicenseID,
                //                            FileID = x.FileID,
                //                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                //                            VersionDate = x.VersionDate,
                //                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                //                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                //                        rptLicenseVersion.DataSource = LicensedocumentVersionData;
                //                        rptLicenseVersion.DataBind();

                //                        rptLicenseDocumnets.DataSource = null;
                //                        rptLicenseDocumnets.DataBind();


                //                        var licenseRecord = LicenseMgmt.GetLicenseByID(licenseID);
                //                        if (licenseRecord != null)
                //                        {
                //                            if (licenseRecord.IsStatutory == true)
                //                                ddlLicenseTypePopup.SelectedItem.Value = "1";
                //                            else
                //                                ddlLicenseTypePopup.SelectedItem.Value = "0";

                //                            if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                //                                ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                //                            tbxLicenseNo.Text = licenseRecord.LicenseNo;
                //                            tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                //                            if (licenseRecord.CustomerBranchID != 0)
                //                            {
                //                                foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                //                                {
                //                                    if (node.Value == licenseRecord.CustomerBranchID.ToString())
                //                                    {
                //                                        node.Selected = true;
                //                                    }
                //                                    foreach (TreeNode item1 in node.ChildNodes)
                //                                    {
                //                                        if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                //                                            item1.Selected = true;
                //                                    }
                //                                }
                //                            }

                //                            tvFilterLocationPopup_SelectedNodeChanged(null, null);

                //                            ddlDepartment.ClearSelection();

                //                            if (licenseRecord.DepartmentID != null)
                //                                ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                //                            if (licenseRecord.StartDate != null)
                //                                tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                //                            else
                //                                tbxStartDate.Text = string.Empty;

                //                            if (licenseRecord.EndDate != null)
                //                                tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                //                            else
                //                                tbxEndDate.Text = string.Empty;


                //                            if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                //                                tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                //                            else
                //                                tbxApplicationdays.Text = string.Empty;

                //                        }
                //                        upLicense.Update();
                //                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                //                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                //                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                //                    }
                //                    #endregion
                //                }
                //                else if (isstatutoryinternal == "I")
                //                {
                //                    #region Internal
                //                    licenseID = Convert.ToInt32(commandArgs[0]);
                //                    complianceinstanceid = Convert.ToInt32(commandArgs[1]);
                //                    compliancescheduleonid = Convert.ToInt32(commandArgs[2]);

                //                    ViewState["LicenseID"] = licenseID;
                //                    ViewState["complianceinstanceid"] = complianceinstanceid;
                //                    ViewState["compliancescheduleonid"] = compliancescheduleonid;
                //                    if (licenseID != 0)
                //                    {
                //                        var LicensedocumentVersionData = InternalLicenseMgmt.GetInternalFileData(licenseID, -1).Select(x => new
                //                        {
                //                            ID = x.ID,
                //                            ScheduledOnID = x.ScheduledOnID,
                //                            LicenseID = x.LicenseID,
                //                            FileID = x.FileID,
                //                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                //                            VersionDate = x.VersionDate,
                //                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                //                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                //                        rptLicenseVersion.DataSource = LicensedocumentVersionData;
                //                        rptLicenseVersion.DataBind();

                //                        rptLicenseDocumnets.DataSource = null;
                //                        rptLicenseDocumnets.DataBind();


                //                        var licenseRecord = InternalLicenseMgmt.GetInternalLicenseByID(licenseID);
                //                        if (licenseRecord != null)
                //                        {
                //                            if (licenseRecord.IsStatutory == true)
                //                                ddlLicenseTypePopup.SelectedItem.Value = "1";
                //                            else
                //                                ddlLicenseTypePopup.SelectedItem.Value = "0";

                //                            if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                //                                ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                //                            tbxLicenseNo.Text = licenseRecord.LicenseNo;
                //                            tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                //                            if (licenseRecord.CustomerBranchID != 0)
                //                            {
                //                                foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                //                                {
                //                                    if (node.Value == licenseRecord.CustomerBranchID.ToString())
                //                                    {
                //                                        node.Selected = true;
                //                                    }
                //                                    foreach (TreeNode item1 in node.ChildNodes)
                //                                    {
                //                                        if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                //                                            item1.Selected = true;
                //                                    }
                //                                }
                //                            }

                //                            tvFilterLocationPopup_SelectedNodeChanged(null, null);

                //                            ddlDepartment.ClearSelection();

                //                            if (licenseRecord.DepartmentID != null)
                //                                ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                //                            if (licenseRecord.StartDate != null)
                //                                tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                //                            else
                //                                tbxStartDate.Text = string.Empty;

                //                            if (licenseRecord.EndDate != null)
                //                                tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                //                            else
                //                                tbxEndDate.Text = string.Empty;


                //                            if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                //                                tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                //                            else
                //                                tbxApplicationdays.Text = string.Empty;

                //                        }
                //                        upLicense.Update();
                //                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                //                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                //                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                //                    }
                //                    #endregion
                //                }
                //            }
                //        }
                //    }
                //}
               // else 
                if (e.CommandName.Equals("DELETE_License"))
                {
                    List<string> lstErrorMsg = new List<string>();
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    string isstatutoryinternal = "S";
                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }
                    if (isstatutoryinternal == "S")
                    {
                        if (!CheckLicenseransactionExists(licenseId))
                        {
                            Delete(licenseId, AuthenticationHelper.UserID);
                            BindGrid();
                        }
                        else
                        {
                            cvErrorLicenseListPage.IsValid = false;
                            cvErrorLicenseListPage.ErrorMessage = "License can not be deleted ,because it is tagged to compliances which are already performed.";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('License can not be deleted ,because it is tagged to compliances which are already performed.');", true);

                        }
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        if (!CheckInternalLicenseransactionExists(licenseId))
                        {
                            DeleteInternal(licenseId, AuthenticationHelper.UserID);
                            BindGrid();
                        }
                        else
                        {
                            cvErrorLicenseListPage.IsValid = false;
                            cvErrorLicenseListPage.ErrorMessage = "License can not be deleted ,because it is tagged to compliances which are already performed.";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('License can not be deleted ,because it is tagged to compliances which are already performed.');", true);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upLicenseList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter5", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdLicenseList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLicenseList.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                    int LicenseID = Convert.ToInt32(commandArgs[0]);
                    int ComplianceInstanceID = Convert.ToInt32(commandArgs[1]);
                    int ComplianceScheduleOnID = Convert.ToInt32(commandArgs[2]);
                    string status = Convert.ToString(commandArgs[3]);
                    string isstatutoryinternal = "S";
                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }

                    if (LicenseID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + LicenseID + "," + ComplianceInstanceID + ", " + ComplianceScheduleOnID +" , '"+ isstatutoryinternal + "'); ", true); 
                    }
                }
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvErrorLicenseListPage.IsValid = false;
            cvErrorLicenseListPage.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }

        protected void grdLicenseList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CustomerID = 0;
                //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //{
                //    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //}
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int branchID = -1;
                int deptID = -1;
                string licenseStatus = string.Empty;
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (ddlLicenseStatus.SelectedValue != "Status")
                {
                    licenseStatus = ddlLicenseStatus.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(CustomerID), branchID);
                List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();
                string isstatutoryinternal = "S";
                if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                  branchList, deptID, licenseStatus, licenseTypeID, "IMPT", isstatutoryinternal);
                MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();


                //string SortExpr = string.Empty;
                //string CheckDirection = string.Empty;
                //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                //{
                //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                //    {
                //        CheckDirection = Convert.ToString(ViewState["Direction"]);
                //        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                //        if (CheckDirection == "Ascending")
                //        {
                //            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                //        }
                //        else
                //        {
                //            CheckDirection = "Descending";
                //            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                //        }
                //    }
                //}
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdLicenseList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicenseList.Columns.IndexOf(field);
                    }
                }

                Session["TotalRows"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdLicenseList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void tbxtypeTofilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);
            //Page DropDown
            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, new ListItem("All", "-1"));
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdDetailData"] != null)
                    {
                        String FileName = String.Empty;

                        FileName = "LicenseReport";

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;

                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        // ExcelData = view.ToTable("Selected", false, "LicenseNo", "Status", "LicenseTitle", "LicenseDetailDesc", "LicensetypeName", "ApplicationDate", "CustomerBrach", "DepartmentName", "StartDate", "EndDate");

                        ExcelData = view.ToTable("Selected", false, "CustomerBrach",  "LicenseNo", "LicenseTitle", "LicensetypeName", "MGRStatus", "ApplicationDate", "StartDate", "EndDate", "PerformerName", "ReviewerName");
                        //ExcelData = view.ToTable("Selected", false, "CustomerBrach", "LicenseNo", "LicenseTitle", "LicensetypeName", "Status", "StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);

                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;

                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }

                                if (item["ApplicationDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["ApplicationDate"])))
                                    {
                                        item["ApplicationDate"] = Convert.ToDateTime(item["ApplicationDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }

                            var customer = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = customer;
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);
                            exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["A5"].Style.WrapText = true;

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);
                            exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["B5"].Style.WrapText = true;

                              
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);
                            exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["C5"].Style.WrapText = true;

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);
                            exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["D5"].Style.WrapText = true;

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);
                            exWorkSheet.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["E5"].Style.WrapText = true;

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);
                            exWorkSheet.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F5"].Style.WrapText = true;

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Application Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);
                            exWorkSheet.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["G5"].Style.WrapText = true;

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "Start Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            exWorkSheet.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["H5"].Style.WrapText = true;

                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["I5"].Value = "End Date";
                            exWorkSheet.Cells["I5"].AutoFitColumns(20);
                            exWorkSheet.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["I5"].Style.WrapText = true;


                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["J5"].Value = "Performer Name";
                            exWorkSheet.Cells["J5"].AutoFitColumns(20);
                            exWorkSheet.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["J5"].Style.WrapText = true;

                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["K5"].Value = "Reviewer Name";
                            exWorkSheet.Cells["K5"].AutoFitColumns(20);
                            exWorkSheet.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K5"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["K5"].Style.WrapText = true;

                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 11])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }

                            using (ExcelRange col = exWorkSheet.Cells[5, 2, 6 + ExcelData.Rows.Count, 11])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }

                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        else
                        {
                            cvErrorLicenseListPage.IsValid = false;
                            cvErrorLicenseListPage.ErrorMessage = "No data available to export for current selection(s)";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool CanChangeStatus(long LicenseID, long ComplianceInstanceID, long ComplianceScheduleOnID, string Status , bool IsDeleted)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = true;

                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        if (Status == "PendingForReview"  )
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        if (Status == "PendingForReview"  )
                        {
                            result = false;
                        }

                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return false;
        }

        protected bool ActionShowHide(bool IsDeleted)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    String userRole = Convert.ToString(AuthenticationHelper.Role);

                    if (userRole == "CADMN" || userRole == "MGMT")
                    {
                        if (IsDeleted != true)
                        {
                            result = true;
                        }

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return false;

        }

        

    }
}