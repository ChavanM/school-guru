﻿<%@ Page Language="C#" AutoEventWireup="true" Culture="en-GB" CodeBehind="LicenseDetailsPageView.aspx.cs"
    EnableEventValidation="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.LicenseDetailsPageView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f7f7f7;">
<head runat="server">
    <title>License Detail</title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>
    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />
    <style type="text/css">
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }

        .btn-group {
            width: 100%;
        }
    </style>

    <style>
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }
    </style>

    <script type="text/javascript">


        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            BindControls();

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });


            

            $("html").getNiceScroll().resize();

            $('#updateProgress').hide();

            $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');

            $('#txtFileTags').tagsinput({});
            $('#lblFileTags').tagsinput({});

            $('input[id*=lstBoxFileTags]').hide();

            $('textarea').on('input', function () {

                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });
        });

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollDown() {
            $('html, body').animate({ scrollTop: $elem.height() }, 800);
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }
        function fopendocfile() {
            $('#DocumentPopUp').modal('show');
            $('#docViewerAll').attr('src', "../../docviewer.aspx?docurl=" + $("#lblpathsample").text());
        }
        function fopendocfileLicenseAllShowPopUp() {
            $('#DocumentShowPopUp').modal('show');
        }
        function fopendocfilelicense(file) {
            $('#DocumentShowPopUp').modal('show'); 
            $('#docViewerLicenseAll').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                //$('#DocumentPopUpSampleForm').modal('hide');
                $('#DocumentShowPopUp').modal('hide');
                //$('#modalDocumentReviewerViewer').modal('hide');
            });

        });
        function hide(object) {
            if (object != null)
                object.style.display = "none";
        }

        function show(object) {
            if (object != null)
                object.style.display = "block";
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('input[id*=txtEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('input[id*=txtFromDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtToDate_AuditLog]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('input[id*=txtLicenseNextDueDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });

            $(function () {

                $('[id*=lstBoxOwner]').multiselect({
                    includeSelectAllOption: true,
                    numberDisplayed: 2,
                    buttonWidth: '100%',
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Type to Search for User..',
                    nSelectedText: ' - Owner(s) selected',
                });


            });
        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 0, function () { });
        }

     
        function CloseMe() {
            window.parent.ClosePopLicense();
        }
    </script>

    <script type="text/javascript">
        function fcheckLicense(obj) {
            var span = $(obj).parent('span.label.label - info');
            $(span).addClass('label-info-selected')
        }
    </script>

    <style>
        .panel-heading .nav > li > a {
            font-size: 15px !important;
            border-bottom: 0px;
            background: none;
        }
    </style>

    <style type="text/css">
        ol.progtrckr {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

            ol.progtrckr li {
                display: inline-block;
                line-height: 3em;
            }

            ol.progtrckr[data-progtrckr-steps="2"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="3"] li {
                width: 25%;
            }

            ol.progtrckr[data-progtrckr-steps="4"] li {
                width: 25%;
            }

            ol.progtrckr li.progtrckr-done {
                color: black;
                border-bottom: 4px solid yellowgreen;
            }

            ol.progtrckr li.progtrckr-closed {
                color: black;
                border-bottom: 0px solid yellowgreen;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-todo {
                color: silver;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li.progtrckr-todo-closed {
                color: silver;
                border-bottom: 0px solid silver;
                width: 0%;
            }

            ol.progtrckr li.progtrckr-current {
                color: black;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li:after {
                content: "\00a0\00a0";
            }

            ol.progtrckr li:before {
                position: relative;
                bottom: -2.5em;
                float: left;
                line-height: 1em;
            }

            ol.progtrckr li.progtrckr-closed:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-done:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-todo:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-todo-closed:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-current:before {
                content: "\039F";
                color: #A16BBE;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }
    </style>



    <script>
        var view = $("#tslshow");
        var move = "100px";
        var sliderLimit = -750;
        $("#rightArrow").click(function () {
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition >= sliderLimit) view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 })
        });

        $("#leftArrow").click(function () {
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition < 0) view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 })
        });
    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smLicenseDetailPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <div class="mainDiv">
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView">
                <div class="container">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" Style="padding-left: 5%" runat="server" Display="none"
                            class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                            EnableClientScript="true" ValidationGroup="ComplianceValidationGroup1" Style="display: none;" />
                    </div>

                    <div id="ActDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                <h2>Compliance Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseActDetails" class="panel-collapse collapse in"> <%--class="collapse"--%>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                <table style="width: 100%;">
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Name</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold; vertical-align: top;">Section /Rule</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold; vertical-align: top;">Short Description</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold; vertical-align: top;">Penalty</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                        maximunsize="300px" autosize="true" runat="server" />
                                                                    <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                        runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                    <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                    <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                        runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                    <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                                runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                                maximunsize="300px" autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ComplianceDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                <h2>License Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseComplianceDetails" class="panel-collapse collapse in">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                <table style="width: 100%">
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">License Type</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblLicenseType" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">License Number</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblLicenseNumber" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">License Title</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblLicenseTitle" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">Application Due Date</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblApplicationDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">Start Date</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblStartdate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="spaceUnder">
                                                        <td style="width: 25%; font-weight: bold; vertical-align: top;">End Date</td>
                                                        <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                        <td style="width: 73%;">
                                                            <asp:Label ID="lblEndDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                autosize="true" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; font-weight: bold;">Versions</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 73%;">
                                                            <table width="100%" style="text-align: left">
                                                                <thead>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                                                OnItemCommand="rptLicenseVersion_ItemCommand"
                                                                                OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblLicenseDocumnets">
                                                                                        <thead>
                                                                                            <%-- <th>Versions</th>--%>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                        ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                <td>
                                                                                                    <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                        ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                                    <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                        Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                        runat="server" Font-Underline="false" />
                                                                                                    <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                    <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:LinkButton ID="lblpathDownload" CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                                    OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </table>
                                                                                </FooterTemplate>
                                                                            </asp:Repeater>

                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                                                OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblLicenseDocumnets">
                                                                                        <thead>
                                                                                            <th>Compliance Related Documents</th>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("FileID")%>'                                                                                                
                                                                                                ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                            </asp:LinkButton>

                                                                                              <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />  

                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </table>
                                                                                </FooterTemplate>
                                                                            </asp:Repeater>
                                                                            <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                                                OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                                                <HeaderTemplate>
                                                                                    <table id="tblLicenseWorkingFiles">
                                                                                        <thead>
                                                                                            <th>Compliance Working Files</th>
                                                                                        </thead>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("FileID")%>'
                                                                                                ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                            </asp:LinkButton>
                                                                                              <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </table>
                                                                                </FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </td>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

               
                    <!--AuditLog panel start-->
                    <div id="AuditLog" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                                <h2>Audit Log</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseAuditLog" class="panel-collapse collapse in">
                                        <div runat="server" id="log" style="text-align: left;">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                    <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                        AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                        BorderWidth="0px">
                                                        <Columns>
                                                               <asp:BoundField DataField="LicenseTypeName" HeaderText="License Type" />
                                                            <asp:BoundField DataField="LicenseNumber" HeaderText="License Number" />
                                                            <asp:BoundField DataField="LicenseTitle" HeaderText="License Title" />                                                             
                                                             <asp:TemplateField HeaderText="Start Date">
                                                                <ItemTemplate>
                                                                    <%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") : ""%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="End Date">
                                                                <ItemTemplate>
                                                                    <%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : ""%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                           
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </fieldset>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--AuditLog panel end-->
                </div>
            </div>

            <div>
                <div class="modal fade" id="DocumentShowPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptLicenseVersionView" runat="server" OnItemCommand="rptLicenseVersionView_ItemCommand"
                                                                    OnItemDataBound="rptLicenseVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblLicenseDocumnets">
                                                                            <thead>
                                                                                <th>Versions</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("LicenseID")+","+Eval("FileID") %>' 
                                                                                            ID="lblLicenseVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblLicenseVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptLicenseVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerLicenseAll" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
