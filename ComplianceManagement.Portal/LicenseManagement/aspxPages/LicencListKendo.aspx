﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="LicencListKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages.LicencListKendo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 300px !important;
        }

        #grid1 .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
            margin-left: -5px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -2px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>
   
    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>
    <script type="text/javascript">
        var PerformerReviewer = "";
        $(document).ready(function () {
            $($('.btndiv').parent('td')).css('padding', "8px 0px 8px 4px");
            setactivemenu('leftworkspacemenu');
            fhead('License');
            BindLocation();
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" },
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                  //  Bindgrid();
                    BindLicenseType();
                    FilterAll();
                }
            });

            BindLicenseType();

            Bindgrid();

            $("#txtSearchfilter").on('input', function (e) {
                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;
                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field == "LicensetypeName" || x.field == "Status" || x.field == "Licensetitle" || x.field == "CustomerBrach" || x.field == "LicenseNo" ) {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
            });

            $("#dropdownlistStatus").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                autoWidth: true,
                optionLabel: "Select Status",
                change: function (e) {
                    FilterAll();
                },
                dataSource: [
                    { text: "Active", value: "Active" },
                    { text: "Expired", value: "Expired" },
                    { text: "Expiring", value: "Expiring" },
                    { text: "Applied", value: "Applied" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                  <% if(IPRStatus == true)%>
                  <% {%>
                    { text: "Registered", value: "Registered" },
                    { text: "Registered & Renewal Filed", value: "Validity Expired" },
                    { text: "Validity Expired", value: "Validity Expired" }
                   <% }%>

                ]
            });
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                checkAllTemplate: "Select All",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =Custid%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });
            $("#dropdownlistStatus").data("kendoDropDownList").value("<%=StatusFlag%>");
            $("#dropdownlistStatus").data("kendoDropDownList").trigger("change");
            $("#dropdownlistComplianceType").data("kendoDropDownList").value("<% =Internalsatutory%>");
        });
        PerformerReviewer = "P";
        function reviewer_click(e) {
            $('li').removeClass('active');
            $('#ContentPlaceHolder1_lidivreviewer').attr('class', 'active');
            PerformerReviewer = "R";
            $("#dropdownlistComplianceType").data("kendoDropDownList").value("<% =Internalsatutory%>");
            Bindgrid();
            BindLicenseType();
            $("#dropdownlistStatus").data("kendoDropDownList").value("<%=StatusFlag%>");
            $("#dropdownlistStatus").data("kendoDropDownList").trigger("change");
            $("#dropdownlistLicenseType").data("kendoDropDownList").select(0);
            e.preventDefault();
        }
        function performer_click(e)
        {
            $('li').removeClass('active');
            $('#ContentPlaceHolder1_liPerfomerdiv').attr('class', 'active');
            $("#dropdownlistComplianceType").data("kendoDropDownList").value("<% =Internalsatutory%>");
            PerformerReviewer = "P";
            Bindgrid();
            $("#dropdownlistStatus").data("kendoDropDownList").value("<%=StatusFlag%>");
            $("#dropdownlistStatus").data("kendoDropDownList").trigger("change");
           BindLicenseType();
            e.preventDefault();
        }
        function DataBindDaynamicKendoGriddMain() {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data('kendoGrid').dataSource.data([]);
            if ($("#dropdownlistComplianceType").val() == "S") {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>License/GetWorkspaceLicenseData?customerID=<% =Custid%>&userID=<% =UserID %>&Flag=<% =Role%>&isstatutoryinternal=S&licenseStatus=&ClickChangeflag=' + PerformerReviewer,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ApplicationDate: { type: "date" },
                                EndDate: { type: "date" },
                            }
                        },
                    },

                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else
            {
                      var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>License/GetWorkspaceLicenseData?customerID=<% =Custid%>&userID=<% =UserID %>&Flag=<% =Role%>&isstatutoryinternal=I&licenseStatus=&ClickChangeflag=' + PerformerReviewer,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                          schema: {
                              model: {
                                  id: "ID",
                                  fields: {
                                      ComplianceID: { type: "string", },
                                      ApplicationDate: { type: "date" },
                                      EndDate: { type: "date" },
                                  }
                              },
                          },

                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }    
        }
        
        function BindLicenseType() {
            $("#dropdownlistLicenseType").kendoDropDownList({
                filter: "startswith",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select License Type",
                autoClose: true,
                change: function (e) {
                    FilterAll();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>License/LicenseTypeList?userID=<% =UserID%>&role=<% =Role %>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                },
                dataBound: function (e) {
                    e.sender.list.width("200");
                }
            });
        }
    
        function Bindgrid() {

            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>License/GetWorkspaceLicenseData?customerID=<% =Custid%>&userID=<% =UserID %>&Flag=<% =Role%>&isstatutoryinternal=<%=Internalsatutory%>&licenseStatus=&ClickChangeflag=' + PerformerReviewer,
<%--                            url: '<% =Path%>License/GetWorkspaceLicenseData?customerID=<% =Custid%>&userID=<% =UserID %>&Flag=<% =Role%>&isstatutoryinternal=' + $("#dropdownlistComplianceType").val() + '&licenseStatus=&ClickChangeflag=' + PerformerReviewer,--%>
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ComplianceID: { type: "string", },
                            ApplicationDate: { type: "date" },
                            EndDate: { type: "date" },
                        }
                    },
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBound,
                dataBinding: function () {
                },
                columns: [
                {
                    field: "CustomerBrach", title: 'Location',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains",
                            }
                        }
                    }, width: "10%",
                },
                  {
                      field: "LicenseNo", title: 'License No.',
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          multi: true,
                          extra: false,
                          search: true,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }, width: "10%",
                  },
                {
                    field: "LicensetypeName", title: 'License Type',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },

                {
                    field: "Licensetitle", title: 'Title',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                 {
                     hidden: true,
                     field: "ComplianceID", title: 'Compliance ID',
                     attributes: {
                         style: 'white-space: nowrap;'

                     }, filterable: {
                         multi: true,
                         extra: false,
                         search: true,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }, width: "13%",
                 },
                    {
                        field: "PerformerName", title: 'Performer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%;",
                    },
                   {
                       field: "ReviewerName", title: 'Reviewer',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           multi: true,
                           extra: false,
                           search: true,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }, width: "10%;",
                   },

                {
                    field: "ApplicationDate", title: 'App Due Date',
                    type: "date",
                    format: "{0:dd-MMM-yyyy}",
                    //template: "#= kendo.toString(kendo.parseDate(RemindOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                type: "date",
                                format: "{0:dd-MMM-yyyy}",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        },
                    }, width: "10%",
                },
                {
                    field: "EndDate", title: 'End Date',
                    type: "date",
                    format: "{0:dd-MMM-yyyy}",
                    //template: "#if(ReminderStatus==0){#<div>pending</div>#}#",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                type: "date",
                                format: "{0:dd-MMM-yyyy}",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },
                {
                    <%if (IPRStatus == true)%>
                    <%{%>
                    field: "Status",
                    <%}%>
                    <%else%>
                    <%{%>
                    field: "MGRStatus",
                    <%}%>
                   title: 'Status',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },

                       {
                           field: "DeptName", title: 'Department',
                           attributes: {
                               style: 'white-space: nowrap;'

                           }, filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }, width: "10%",
                       },
                 {
                     command: [
                         {
                             name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-overview"
                         },
                         //{ name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                     ], title: "Action", lock: true, width: 100,// width: 150,
                     headerAttributes: {
                         style: "text-align: center;"
                     }
                 }
                ]
            });
                function OnGridDataBound(e) {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                    var grid = $("#grid").data("kendoGrid");
                    var gridData = grid.dataSource.view();
                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overview");

                        if (gridData[i].StatusResult == true) {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            createUserButton.hide();
                        }

                       <%if (HideStatusOnActiveEnable == true)%><%{%>
                        if (gridData[i].MGRStatus == "Active") {
                            createUserButton.hide();
                        }
                       <%}%>
                    }
                }
                $("#grid").kendoTooltip({
                    filter: "th",
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown 
                        return $(target).text();
                    }
                });
                $("#grid").kendoTooltip({
                    filter: "td", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    }
                }).data("kendoTooltip");

                $("#grid").kendoTooltip({
                    filter: ".k-grid-edit",
                    content: function (e) {
                        return "Edit";
                    }
                });

                $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                    debugger;
                    var RoleFlag = document.getElementById('PRA').value;
                    $('#divShowReminderDialog').modal('show');
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    //  OpenOverViewpup(item.ComplianceInstanceID, item.ComplianceScheduleOnID, item.Status);
                    if (PerformerReviewer == "P") {
                        if ($("#dropdownlistComplianceType").val() == "S") {
                            if ($("#dropdownlistStatus").val() == "Applied")
                            {
                                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPage.aspx?AccessID=" +item.LicenseID);

                            }
                            else
                            {
                                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/compliances/perCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.MGRStatus);
                            }
                        }
                        if ($("#dropdownlistComplianceType").val() == "I") {

                            if ($("#dropdownlistStatus").val() == "Applied")
                            {
                                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + item.LicenseID);

                            }
                            else {
                                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/compliances/perInternalCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.MGRStatus);
                            }
                        }
                    }
                    if (PerformerReviewer == "R") {
                        if ($("#dropdownlistComplianceType").val() == "S") {
                            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/compliances/revCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.MGRStatus);
                        }
                        if ($("#dropdownlistComplianceType").val() == "I") {
                            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "/compliances/revInternalCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.MGRStatus);
                        }
                    }
                    return true;
                });
            }

            function CloseMyReminderPopup() {
                $('#divShowReminderDialog').modal('hide');
                //  Bindgrid();
            }
            function OpenOverViewpup(instanceid, scheduledonid, Status) {
                $('#divShowReminderDialog').modal('show');
                $('#showReminderDetail').attr('width', '98%');
                $('#showReminderDetail').attr('height', '600px');
                $('.modal-dialog').css('width', '92%');
                if ($("#dropdownlistComplianceType").val() == "S") {
                    // $('#showReminderDetail').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

                    $('#showReminderDetail').attr('src', "/compliances/perCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.Status);
                }
                if ($("#dropdownlistComplianceType").val() == "I") {
                    $('#showReminderDetail').attr('src', "/compliances/perInternalCompliancestatusTransaction.aspx?CID=" + item.ComplianceInstanceID + "&SOID=" + item.ComplianceScheduleOnID + "&status=" + item.Status);
                }
            }
            function FilterAll() {
                debugger;
                var status = "MGRStatus";
                <% if(IPRStatus == true)%>
                <% {%>
                status = "Status";
            <%}%>
                var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
                var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;
                if (locationlist.length > 0
                    || Departmentlist.length > 0
                    || ($("#dropdownDept").val() != "" && $("#dropdownDept").val() != null && $("#dropdownDept").val() != undefined)
                    || ($("#dropdownlistLicenseType").val() != "" && $("#dropdownlistLicenseType").val() != null && $("#dropdownlistLicenseType").val() != undefined)
                    || ($("#dropdownlistStatus").val() != "" && $("#dropdownlistStatus").val() != null && $("#dropdownlistStatus").val() != undefined)
                    || ($("#dropdownlistComplianceType").val() == "S" || $("#dropdownlistComplianceType").val() == "I")) {

                    var finalSelectedfilter = { logic: "and", filters: [] };

                    if ($("#dropdownlistComplianceType").val() == "S" && $("#dropdownlistComplianceType").val() == "I") {
                        var FYFilter = { logic: "or", filters: [] };

                        FYFilter.filters.push({
                            field: "isstatutoryinternal", operator: "eq", value: parseInt($("#dropdownlistComplianceType").val())
                        });
                        finalSelectedfilter.filters.push(FYFilter);
                    }
                    if (locationlist.length > 0) {
                        var locFilter = { logic: "or", filters: [] };

                        $.each(locationlist, function (i, v) {
                            locFilter.filters.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(locFilter);
                    }
                    if (Departmentlist.length > 0) {
                        var DeptFilter = { logic: "or", filters: [] };

                        $.each(Departmentlist, function (i, v) {
                            DeptFilter.filters.push({
                                //   field: "departmentID1", operator: "eq", value: parseInt(v)
                                field: "DepartmentID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(DeptFilter);
                    }

                    if ($("#dropdownlistLicenseType").val() != "" && $("#dropdownlistLicenseType").val() != null && $("#dropdownlistLicenseType").val() != undefined) {
                        var LicenseFilter = { logic: "or", filters: [] };
                        LicenseFilter.filters.push({
                            field: "LicenseTypeID", operator: "eq", value: $("#dropdownlistLicenseType").val()
                            //field: "LicenseTypeID", operator: "eq", value: parseInt($("#dropdownlistLicenseType").val())
                            //   field: "LicenseTypeID", operator: "eq", value: parseInt(v)
                        });
                        finalSelectedfilter.filters.push(LicenseFilter);
                    }

                    if ($("#dropdownlistStatus").val() != "" && $("#dropdownlistStatus").val() != null && $("#dropdownlistStatus").val() != undefined) {
                        var StatusFilter = { logic: "or", filters: [] };
                        StatusFilter.filters.push({
                            field: status, operator: "eq", value: $("#dropdownlistStatus").val()
                        });
                        finalSelectedfilter.filters.push(StatusFilter);
                    }

                    if (finalSelectedfilter.filters.length > 0) {
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(finalSelectedfilter);
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }

            }


            function ClearAllFilterMain(e) {
                $("#dropdowntree").data("kendoDropDownTree").value([]);
               //$("#dropdownlistStatus").data("kendoDropDownList").select(0);
                $("#dropdownlistLicenseType").data("kendoDropDownList").select(0);
                $("#dropdownDept").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#Searchfilter').val('');
                FilterAll();
                e.preventDefault();
            }
            function fcloseStory(obj) {

                var DataId = $(obj).attr('data-Id');
                var dataKId = $(obj).attr('data-K-Id');
                var seq = $(obj).attr('data-seq');
                var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
                $(deepspan).trigger('click');
                var upperli = $('#' + dataKId);
                $(upperli).remove();
                //for rebind if any pending filter is present (Main Grid)
                fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');

                //fCreateStoryBoard('dropdownlistStatus', 'filtersstoryStatus', 'status');
                CheckFilterClearorNotMain();
            };

            function CheckFilterClearorNotMain() {

                if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                   ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                    $('#ClearfilterMain').css('display', 'none');
                }
            }

            function fCreateStoryBoard(Id, div, filtername) {

                var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
                $('#' + div).html('');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
                $('#' + div).css('display', 'block');
                if (div == 'filtersstoryboard') {
                    $('#' + div).append('Location&nbsp;&nbsp;:');
                    $('#ClearfilterMain').css('display', 'block');
                }
                if (div == 'filterdept') {
                    $('#' + div).append('Department&nbsp;&nbsp;:');
                    $('#ClearfilterMain').css('display', 'block');
                }
                for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                    var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                    $(button).css('display', 'none');
                    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                    var buttontest = $($(button).find('span')[0]).text();
                    if (buttontest.length > 10) {
                        buttontest = buttontest.substring(0, 10).concat("...");
                    }
                    $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                    //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
                }

                if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                    $('#' + div).css('display', 'none');
                    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

                }
                CheckFilterClearorNotMain();
            }
            function ClosePopNoticeDetialPage() {
                $('#divShowDialog').modal('hide');
                //  Bindgrid();
            }

            function BindLocation() {
                $("#dropdowntree").kendoDropDownTree({
                    placeholder: "Entity/Sub Entity/Location",
                    checkboxes: {
                        checkChildren: true
                    },
                    autoClose:false,                
                    autoWidth: true,
                    //checkAllTemplate: "Select All",
                    dataTextField: "Name",
                    dataValueField: "ID",
                    change: function (e) {
                        FilterAll();
                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Litigation/GetLocationList?customerId=<% =Custid%>',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    },
                });
            }

            function AddNewLicense_click(e) {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '550px');
                $('.modal-dialog').css('width', '100%');

                if ($("#dropdownlistComplianceType").val() == "S") {
                    $('#showdetails').attr('src', "/LicenseManagement/aspxPages/AddNewLicenseDetailsPage.aspx");
                }
                else if ($("#dropdownlistComplianceType").val() == "I") {
                    $('#showdetails').attr('src', "/LicenseManagement/aspxPages/AddNewInternalLicenseDetailsPage.aspx");
                }
                e.preventDefault();
                return false;
            }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divwindow"></div>
    <div id="btncloseall" onclick="close_windows()" style="display: none;"></div>
    <input id="PRA" type="hidden" value="<% =ChangeFlag%>" />
    <input id="Role" type="hidden" value="<% =Role%>" />
     <input id="btnCustID" type="hidden" value="<% =HideAddNew%>" />

    <div class="row Dashboard-white-widget">
        <div class="col-lg-12 col-md-12 ">
            <section class="panel">
                <header class="panel-heading tab-bg-primary ">
                    <ul id="rblRole1" class="nav nav-tabs">
                        <%if (roles != null )
                            {
                        %>
                        <% if (roles.Contains(3) && Role != "MGMT" && Role != "CADMN")%>
                        <%{%>
                               <li class="active" id="liPerfomerdiv" runat="server">
                            <asp:LinkButton ID="liPerformer" OnClientClick="performer_click(event);"  runat="server" >Performer</asp:LinkButton>
                        </li>
                        <%}%>
                        <% if (roles.Contains(4) && Role != "MGMT" && Role != "CADMN")%>
                        <%{%>
                        <li class="" id="lidivreviewer" runat="server">
                            <asp:LinkButton ID="liReviewer"  OnClientClick="reviewer_click(event);" runat="server">Reviewer</asp:LinkButton>
                        </li>
                        <%}%>
                        <%}%>
                    </ul>
                </header>
            </section>
        </div>
    </div>

    <div class="row" style="margin-left: 11px;">
        <div class="toolbar">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 270px; margin-right: 10px;" />
            <input id="dropdownlistComplianceType" style="width: 172px; margin-right: 10px;" />
            <input id="dropdownlistStatus" style="width: 172px; margin-right: 10px;" />
            <input id="dropdownlistLicenseType" style="width: 172px; margin-right: 10px;" />
            <input id="dropdownDept" data-placeholder="Department" style="width: 172px; margin-right: 10px;" />
             <%if (HideAddNew == true && Role == "EXCT")
              {
              %>
            <button id="AddNewLicense" runat="server" style="width: 7.1%" onclick="AddNewLicense_click(event)">
                <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</button>
            <%}%>
            <input id='txtSearchfilter' type="text" class='k-textbox' placeholder="Type to Filter" style="width: 220px; margin-left: 0px; margin-top: 10px;" onkeydown="return (event.keyCode!=13);" />

            <button id="ClearfilterMain" style="float: right; height: 28px; padding-top: 2px; margin-top: 10px; margin-left: 142px; display: none; padding-right: 12px" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
    </div>

    <div class="row" style="padding-top: 12px;">
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold; margin-left: 13px;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold; margin-left: 13px;" id="filtersstoryStatus">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold; margin-left: 13px;" id="filterdept">&nbsp;</div>
        <div id="grid" style="margin-left: 10px; margin-right: 10px;"></div>
    </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        License Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                </div>
            </div>
        </div>
    </div>
   
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add License Details</label>
                    <button runat="server" id="btnAddEditLicense" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPage();">&times;</button>
                   
                     </div>
                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

