﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class LicenseDetailsPageView : System.Web.UI.Page
    {
        static string sampleFormPath = "";
        public static string CompDocReviewPath = "";
        public static List<long> Branchlist = new List<long>();       
        protected int LoggedInUserCustomerID = 0;
        protected long UserID = AuthenticationHelper.UserID;
        protected string UserName = AuthenticationHelper.User;
        protected string productID = AuthenticationHelper.ProductApplicableLogin;        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                                
                if (!IsPostBack)
                {
                    if (Convert.ToInt32(AuthenticationHelper.CustomerID) != 0)
                    {
                        LoggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var licenseInstanceID = Request.QueryString["AccessID"];
                        if (licenseInstanceID != "")
                        {
                            ViewState["LicenseInstanceID"] = licenseInstanceID;
                            Session["LicenseInstanceID"] = licenseInstanceID;
                            btnEditLicense_Click(sender, e); //Edit Detail                                
                        }
                    }                    
                }                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private void BindTransactions(long LicenseID)
        {
            try
            {
                grdTransactionHistory.DataSource = LicenseMgmt.GetAllTransactionLog(LicenseID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["LicenseInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download

                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                    List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                    ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                FileID = x.FileID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName,
                                LicenseID = x.LicenseID
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {

                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion                                                      
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                            Session["LicenseID"] = commandArg[0];
                            if (CMPDocuments != null)
                            {
                                List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                    entityData.Version = "1.0";
                                    entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                CompDocReviewPath = filePath1;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                                lblMessageReviewer1.Text = "";

                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                            Session["LicenseID"] = commandArg[0];
                            if (CMPDocuments != null)
                            {
                                List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                    entityData.Version = "1.0";
                                    entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                CompDocReviewPath = FileName;

                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }


            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }    
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[2]), -1).ToList();
                        Session["LicenseID"] = commandArg[2];

                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[2]);
                                entitiesData.Add(entityData);
                            }


                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    CMPDocuments = CMPDocuments.Where(entry => entry.FileID == Convert.ToInt32(commandArg[3])).ToList();
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[2]), -1).ToList();
                        Session["LicenseID"] = commandArg[2];

                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[2]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                CMPDocuments = CMPDocuments.Where(entry => entry.FileID == Convert.ToInt32(commandArg[3])).ToList();
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + CompDocReviewPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
                   
        public void DownloadFile(int fileId)
        {
            try
            {

                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                if (AWSData != null)
                {
                    #region AWS Storage
                    var file = Business.ComplianceManagement.GetFile(fileId);
                    if (file.FilePath != null)
                    {
                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                        string directoryPath = "~/TempDocuments/AWS/" + User;
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                        }

                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                        {
                            GetObjectRequest request = new GetObjectRequest();
                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                            request.Key = file.Name;
                            GetObjectResponse response = client.GetObject(request);
                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.Name);
                        }
                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.Name);
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            Response.BinaryWrite(DocumentManagement.ReadDocFiles(filePath)); // create the file                               
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Normal Storage
                    var file = Business.ComplianceManagement.GetFile(fileId);

                    if (file.FilePath != null)
                    {
                        string filePath = string.Empty;
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                        }
                        else
                        {
                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        }
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnEditLicense_Click(object sender, EventArgs e)
        {
            try
            {                                                                                                          
                long licenseInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseInstanceID"])))
                {
                    licenseInstanceID = Convert.ToInt32(ViewState["LicenseInstanceID"]);
                    if (licenseInstanceID != 0)
                    {                                             
                        var licenseRecord = LicenseMgmt.GetLicenseByID(licenseInstanceID);
                        if (licenseRecord != null)
                        {
                            var MasterTransction = LicenseMgmt.GetPreviousLicenseDetail(licenseInstanceID);                            
                            BindTransactions(licenseInstanceID);                                                        
                            List<InternalComplianceAssignment> nonStatAssignmentlst = new List<InternalComplianceAssignment>();
                            if (licenseRecord.IsStatutory == true)
                            {
                                var scheduledetails = LicenseMgmt.GetLicenseComplianceInstanceScheduleOnByID(licenseInstanceID);
                                if (scheduledetails != null)
                                {
                                    if (MasterTransction !=null)
                                    {
                                        lblLicenseType.Text = MasterTransction.LicensetypeName;
                                        lblLicenseNumber.Text = MasterTransction.LicenseNo;
                                        lblLicenseTitle.Text = MasterTransction.Licensetitle;                                        
                                        lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                                        lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                                        lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                                        var documentVersionData = LicenseDocumentManagement.GetFileData(licenseInstanceID,-1).Select(x => new
                                        {
                                            ID = x.ID,
                                            ScheduledOnID = x.ScheduledOnID,
                                            LicenseID = x.LicenseID,
                                            FileID = x.FileID,
                                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                            VersionDate = x.VersionDate,
                                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                            ISLink = x.ISLink,
                                            FilePath = x.FilePath,
                                            FileName = x.FileName
                                        }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                                        rptLicenseVersion.DataSource = documentVersionData;
                                        rptLicenseVersion.DataBind();

                                        rptLicenseDocumnets.DataSource = null;
                                        rptLicenseDocumnets.DataBind();
                                    }
                                    var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID((int)scheduledetails.ComplianceScheduleOnID);
                                    var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                                    if (complianceInfo != null)
                                    {
                                        lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                                        lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                                        lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                                        lblDetailedDiscription.Text = complianceInfo.Description;
                                        lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                                        lblPenalty.Text = complianceInfo.PenaltyDescription;
                                        lblRule.Text = complianceInfo.Sections;
                                        lblFormNumber.Text = complianceInfo.RequiredForms;
                                        lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);                                        
                                        lblPenalty.Text = complianceInfo.PenaltyDescription;
                                        string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, scheduledetails.ComplianceInstanceID);                                        
                                        lblRisk.Text = risk;
                                        var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                                        if (ActInfo != null)
                                        {
                                            lblActName.Text = ActInfo.Name;
                                        }
                                        if (complianceInfo.UploadDocument == true && complianceForm != null)
                                        {                                            
                                            lbDownloadSample.Text = "Download";
                                            lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                                            sampleFormPath = complianceForm.FilePath;
                                            sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);                                            
                                            lnkViewSampleForm.Visible = true;
                                            lblSlash.Visible = true;
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                lblpathsample.Text = sampleFormPath;
                                            }
                                            else
                                            {
                                                lblpathsample.Text = sampleFormPath;
                                            }
                                        }
                                        else
                                        {                                            
                                            lblSlash.Visible = false;
                                            lnkViewSampleForm.Visible = false;
                                            sampleFormPath = "";
                                        }
                                    }
                                }                              
                            }                                                                                                                                                                                                                      
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }                    
    }
}