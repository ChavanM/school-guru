﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class LicenseActivation : System.Web.UI.Page
    {
        protected bool flag;
        protected long loggedInUserId = AuthenticationHelper.UserID;
        protected string loggedInUserRoleCode = AuthenticationHelper.Role;
        protected long customerID = 0;
        protected static string user_Roles;
        protected long UserID = AuthenticationHelper.UserID;
        public static List<int> locationList = new List<int>();
        public static List<int> locationList1 = new List<int>();
        protected string UserName = AuthenticationHelper.User;                        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;
                    BindLicenseType();
                    // BindCustomerFilter();
                    //BindCustomerFilter1();
                    BindCustomers();
                    if (user_Roles.Contains("IMPT"))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                            customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                    }
                    else if (user_Roles.Contains("CADMN"))
                    {
                        CustomerList1.Visible = false;
                        CustomerList.Visible = false;
                        customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                        if (ddlFilterCustomer.Items.Count > 1)
                        {
                            ddlFilterCustomer.SelectedValue = Convert.ToString(customerID);
                            ddlFilterCustomer1.SelectedValue = Convert.ToString(customerID);
                        }
                    }
                    else if (user_Roles.Contains("MGMT"))
                    {
                        CustomerList1.Visible = false;
                        CustomerList.Visible = false;
                        customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                        if (ddlFilterCustomer.Items.Count > 1)
                        {
                            ddlFilterCustomer.SelectedValue = Convert.ToString(customerID);
                            ddlFilterCustomer1.SelectedValue = Convert.ToString(customerID);
                        }
                         
                    }
                    else if (user_Roles.Contains("EXCT"))
                    {
                        CustomerList1.Visible = false;
                        CustomerList.Visible = false;
                        customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                        if (ddlFilterCustomer.Items.Count > 1)
                        {
                            ddlFilterCustomer.SelectedValue = Convert.ToString(customerID);
                            ddlFilterCustomer1.SelectedValue = Convert.ToString(customerID);
                        }
                    }
                    if (customerID != 0)
                    {
                        var branchList = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(customerID));
                        BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);
                        BindCustomerBranches(tvFilterLocation1, tbxFilterLocation1, branchList);
                    }

                    if (grdLicenseList.Rows.Count == 0)
                    {
                        noteId.Visible = false;
                    }
                    else
                    {
                        noteId.Visible = true;
                    }

                    liAssigned1.Attributes.Add("class", "active");
                    liNotAssigned1.Attributes.Add("class", "");
                    notAssignedTab.Attributes.Remove("class");
                    notAssignedTab.Attributes.Add("class", "tab-pane");
                    assignedTab.Attributes.Remove("class");
                    assignedTab.Attributes.Add("class", "tab-pane active");


                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divBranches1');", tbxFilterLocation1.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree1", "hideDivBranch1();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindGridWithPaging()
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindGridWithPaging1()
        {
            BindGrid1();
            bindPageNumber1();
            ShowGridDetail1();
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divFilterLocation1');", tbxFilterLocation1.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree1", "hideDivBranch1();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindCustomerFilter()
        //{
        //    List<Customer> lstCustomers = CustomerManagement.GetAll(-1, null);

        //    if (lstCustomers.Count > 0)
        //        lstCustomers = lstCustomers.OrderBy(entry => entry.Name).ToList();

        //    ddlFilterCustomer.DataTextField = "Name";
        //    ddlFilterCustomer.DataValueField = "ID";

        //    ddlFilterCustomer.DataSource = lstCustomers;
        //    ddlFilterCustomer.DataBind();

        //    ddlFilterCustomer1.DataTextField = "Name";
        //    ddlFilterCustomer1.DataValueField = "ID";

        //    ddlFilterCustomer1.DataSource = lstCustomers;
        //    ddlFilterCustomer1.DataBind();
        //}

        private void BindCustomers()
        {
            try
            {
                int UserID = AuthenticationHelper.UserID;
                var customerdata= GetAllCustomer(UserID, "");
            
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";

                ddlFilterCustomer.DataSource = customerdata;
                ddlFilterCustomer.DataBind();

                ddlFilterCustomer1.DataTextField = "Name";
                ddlFilterCustomer1.DataValueField = "ID";

                ddlFilterCustomer1.DataSource = customerdata;
                ddlFilterCustomer1.DataBind();

                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }
        public static object GetAllCustomer(int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var users = (from row in entities.CustomerAssignmentDetails
                             join row1 in entities.Customers
                             on row.CustomerID equals row1.ID

                             where row.IsDeleted == false
                             && row.UserID == userID
                             && row1.IsDeleted == false
                             && row1.ComplianceProductType != 1
                             && row1.Status == 1
                             select row1);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();
                NameValueHierarchy branch = null;
                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }
                treeTxtBox.Text = "Select Entity/Branch/Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }
                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }        
        protected void RetrieveNodes(TreeNode node)
        {
            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i]);
                    }
                }
            }
        }
        protected void RetrieveNodes1(TreeNode node)
        {
            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList1.Contains(Convert.ToInt32(node.Value)))
                    locationList1.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList1.Contains(Convert.ToInt32(tn.Value)))
                        locationList1.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes1(tn.ChildNodes[i]);
                    }
                }
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                customerID = Convert.ToInt32(ddlFilterCustomer.SelectedItem.Value);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                var LocationList = new List<int>();
                foreach (var item in bracnhes)
                {
                    TreeNode node  = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Entity/Sub-Entity/Location";

                liAssigned1.Attributes.Add("class", "active");
                liNotAssigned1.Attributes.Add("class", "");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane active");

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter1()
        {
            try
            {
                customerID = Convert.ToInt32(ddlFilterCustomer1.SelectedItem.Value);
                tvFilterLocation1.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                var LocationList = new List<int>();
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocation1.Nodes.Add(node);
                }
                tvFilterLocation1.CollapseAll();
                tbxFilterLocation1.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation1.SelectedNode.Text : "Entity/Sub-Entity/Location";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divBranches1');", tbxFilterLocation1.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree1", "hideDivBranch1();", true);
                liAssigned1.Attributes.Add("class", "");
                liNotAssigned1.Attributes.Add("class", "active");

                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane");

                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane active");



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
        }
        protected void ddlFilterCustomer1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter1();


        }
        protected void ddlLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindGridWithPaging();

            liNotAssigned1.Attributes.Add("class", "");
            liAssigned1.Attributes.Add("class", "active");

            notAssignedTab.Attributes.Remove("class");
            notAssignedTab.Attributes.Add("class", "tab-pane");

            assignedTab.Attributes.Remove("class");
            assignedTab.Attributes.Add("class", "tab-pane active");
        }

        protected void ddlLicenseType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindGridWithPaging();

            liAssigned1.Attributes.Add("class", "");
            liNotAssigned1.Attributes.Add("class", "active");

            assignedTab.Attributes.Remove("class");
            assignedTab.Attributes.Add("class", "tab-pane");

            notAssignedTab.Attributes.Remove("class");
            notAssignedTab.Attributes.Add("class", "tab-pane active");

        }
        private bool LicenseSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("StatutoryLicense"))
                    {
                        if (sheet.Name.Trim().Equals("StatutoryLicense"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                if (fuSampleFile.HasFile)
                {

                    long cid = -1;
                    if (AuthenticationHelper.Role.Contains("IMPT"))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                            cid = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                    }
                    else if (AuthenticationHelper.Role.Contains("CADMN"))
                    {
                        cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                    }
                    else
                    {
                        cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                    }

                    if (cid != -1)
                    {
                        string filename = Path.GetFileName(fuSampleFile.FileName);
                        Random R = new Random();
                        fuSampleFile.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                        FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flag = LicenseSheetsExitsts(xlWorkbook, "StatutoryLicense");
                                if (flag == true)
                                {
                                    int ComplianeId = 0;
                                    int LocationId = 0;
                                    int LicenseType = 0;
                                    string LicenseNo = string.Empty;
                                    string LicenseTitle = string.Empty;
                                    Decimal Licensecost = 0;
                                    string FileNo = string.Empty;
                                    string PhysicalLocation = string.Empty;
                                    DateTime startDate = new DateTime();
                                    DateTime ExpiryDate = new DateTime();
                                    int ApplicationDays = 0;
                                    List<string> errorMessage = new List<string>();
                                    List<string> existsMessage = new List<string>();
                                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["StatutoryLicense"];
                                    if (xlWorksheet != null)
                                    {
                                        int xlrow2 = xlWorksheet.Dimension.End.Row;

                                        #region Validation
                                        for (int i = 2; i <= xlrow2; i++)
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                            {
                                                try
                                                {
                                                    ComplianeId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text);
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorMessage.Add(" Compliance Id At  - " + i + " should be intenger");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add(" Compliance Id At  - " + i + " should not blank");
                                            }

                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                                            {
                                                try
                                                {
                                                    LocationId = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorMessage.Add(" Location Id At  - " + i + " should be intenger");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add(" Location Id At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                            {

                                                try
                                                {
                                                    LicenseType = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorMessage.Add(" Location Id At  - " + i + " should be intenger");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add(" License Type Id At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                            {
                                                LicenseNo = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                            }
                                            else
                                            {
                                                errorMessage.Add(" License No Id At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                            {
                                                LicenseTitle = Convert.ToString(xlWorksheet.Cells[i, 8].Text);
                                            }
                                            else
                                            {
                                                errorMessage.Add(" License Title At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                            {
                                                if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text))
                                                {
                                                    errorMessage.Add("Please Check Start Date or Date should be in DD-MMM-YYYY Format At" + i + " or Start Date can not be empty.");
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        bool check = CheckDate(Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim());
                                                        if (!check)
                                                        {
                                                            errorMessage.Add("Please Check Start Date or Date should be in DD-MMM-YYYY Format At" + i);
                                                        }
                                                        else
                                                        {
                                                            string c = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                                                            DateTime? aaaa = CleanDateField(c);
                                                            DateTime dt1 = Convert.ToDateTime(aaaa);
                                                            startDate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add(" Start Date At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                            {
                                                if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                                                {
                                                    errorMessage.Add("Please Check Expiry Date or Date should be in DD-MMM-YYYY Format At" + i + " or Expiry Date can not be empty.");
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        bool check = CheckDate(Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim());
                                                        if (!check)
                                                        {
                                                            errorMessage.Add("Please Check Expiry Date or Date should be in DD-MMM-YYYY Format At" + i);
                                                        }
                                                        else
                                                        {
                                                            string c = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                                                            DateTime? aaaa = CleanDateField(c);
                                                            DateTime dt1 = Convert.ToDateTime(aaaa);
                                                            ExpiryDate = GetDate(dt1.ToString("dd/MM/yyyy"));

                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text) && !string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text))
                                                {
                                                    if (DateTime.Compare(DateTimeExtensions.GetDate(ExpiryDate.ToString("dd-MM-yyyy")), DateTimeExtensions.GetDate(startDate.ToString("dd-MM-yyyy"))) <= 0)
                                                    {
                                                        errorMessage.Add("Expiry Date should be greater than Start Date.");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add("Expiry Date At  - " + i + " should not blank");
                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                            {
                                                try
                                                {
                                                    ApplicationDays = Convert.ToInt16(xlWorksheet.Cells[i, 11].Text);
                                                    if (ApplicationDays < 0)
                                                    {
                                                        errorMessage.Add(" Application Days Id At  - " + i + " should be Positive");
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorMessage.Add(" Application Days Id At  - " + i + " should be intergar");
                                                }
                                            }
                                            else
                                            {
                                                errorMessage.Add(" Application Days Id At  - " + i + " should not blank");

                                            }
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                            {
                                                try
                                                {
                                                    Licensecost = Convert.ToDecimal(xlWorksheet.Cells[i, 14].Text);
                                                }
                                                catch (Exception ex)
                                                {
                                                    errorMessage.Add(" Cost At  - " + i + " should be Numeric");
                                                }
                                            }
                                            else
                                            {
                                                //errorMessage.Add(" Cost  At  - " + i + " should not blank");
                                                Licensecost = 0;
                                            }
                                        }
                                        #endregion

                                        if (errorMessage.Count > 0)
                                        {
                                            showErrorMessages(errorMessage, cvErrorLicenseListPage);
                                        }
                                        else
                                        {
                                            #region  Save Code
                                            bool saveSuccess = false;
                                            for (int i = 2; i <= xlrow2; i++)
                                            {
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                                {
                                                    ComplianeId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                                                {
                                                    LocationId = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                                {
                                                    LicenseType = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                                {
                                                    LicenseNo = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                                {
                                                    LicenseTitle = Convert.ToString(xlWorksheet.Cells[i, 8].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                                {
                                                    //startDate = Convert.ToDateTime(xlWorksheet.Cells[i, 9].Text);                                 
                                                    string c = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                                                    DateTime? aaaa = CleanDateField(c);
                                                    DateTime dt1 = Convert.ToDateTime(aaaa);
                                                    startDate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                                {
                                                    //ExpiryDate = Convert.ToDateTime(xlWorksheet.Cells[i, 10].Text);
                                                    string c = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                                                    DateTime? aaaa = CleanDateField(c);
                                                    DateTime dt1 = Convert.ToDateTime(aaaa);
                                                    ExpiryDate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                                {
                                                    ApplicationDays = Convert.ToInt16(xlWorksheet.Cells[i, 11].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                                {
                                                    FileNo = Convert.ToString(xlWorksheet.Cells[i, 12].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                                {
                                                    PhysicalLocation = Convert.ToString(xlWorksheet.Cells[i, 13].Text);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                                {
                                                    Licensecost = Convert.ToDecimal(xlWorksheet.Cells[i, 14].Text);
                                                }
                                                else
                                                {
                                                    Licensecost = 0;
                                                }
                                                #region Save License Instance and Status
                                                long newLicenseID = 0;
                                                Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                                {
                                                    CustomerID = Convert.ToInt32(cid),
                                                    IsDeleted = false,
                                                    CreatedBy = loggedInUserId
                                                };
                                                Lic_tbl_LicenseInstance_Log newLic_tbl_InternalLicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                                                {
                                                    CustomerID = licenseRecord.CustomerID,
                                                    CreatedBy = loggedInUserId,
                                                };
                                                licenseRecord.LicenseNo = LicenseNo;
                                                newLic_tbl_InternalLicenseInstance_Log.LicenseNo = LicenseNo;

                                                licenseRecord.LicenseTitle = LicenseTitle;
                                                newLic_tbl_InternalLicenseInstance_Log.LicenseTitle = LicenseTitle;

                                                licenseRecord.CustomerBranchID = LocationId;

                                                licenseRecord.LicenseTypeID = LicenseType;
                                                newLic_tbl_InternalLicenseInstance_Log.LicenseTypeID = LicenseType;

                                                licenseRecord.StartDate = startDate;
                                                newLic_tbl_InternalLicenseInstance_Log.StartDate = startDate;

                                                licenseRecord.EndDate = ExpiryDate;
                                                newLic_tbl_InternalLicenseInstance_Log.EndDate = ExpiryDate;

                                                licenseRecord.RemindBeforeNoOfDays = ApplicationDays;
                                                newLic_tbl_InternalLicenseInstance_Log.RemindBeforeNoOfDays = ApplicationDays;

                                                licenseRecord.RecurringNoOfDays = 0;

                                                //IsStatutory
                                                bool IsStatutory = false;
                                                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory.SelectedValue))
                                                {
                                                    if (ddlIsStatutoryNonStatutory.SelectedValue == "S")
                                                        IsStatutory = true;
                                                    else
                                                        IsStatutory = false;
                                                }
                                                licenseRecord.IsStatutory = IsStatutory;
                                                newLic_tbl_InternalLicenseInstance_Log.IsStatutory = IsStatutory;

                                                licenseRecord.FileNO = FileNo;
                                                licenseRecord.PhysicalLocation=PhysicalLocation;

                                                licenseRecord.Cost = Licensecost;
                                                newLic_tbl_InternalLicenseInstance_Log.Cost = Licensecost;
                                                int statusId = 0;
                                                bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(cid), licenseRecord.LicenseNo);
                                                if (!existLicNo)
                                                {
                                                    newLicenseID = LicenseMgmt.CreateLicense(licenseRecord);
                                                    newLic_tbl_InternalLicenseInstance_Log.LicenseID = newLicenseID;
                                                    LicenseMgmt.CreateLicenseLog(newLic_tbl_InternalLicenseInstance_Log);
                                                 
                                                    //start date and end date if not empty
                                                    if (!string.IsNullOrEmpty(Convert.ToString((startDate))) || !string.IsNullOrEmpty(Convert.ToString(ExpiryDate)))
                                                    {
                                                        if (DateTimeExtensions.GetDate(Convert.ToString(ExpiryDate.ToString("dd/MM/yyyy"))).Date <= DateTime.Today.Date)
                                                        {
                                                            statusId = 3;        //Expired
                                                        }
                                                        else if (DateTimeExtensions.GetDate(Convert.ToString(ExpiryDate.ToString("dd/MM/yyyy"))).Date <= DateTime.Today.Date.AddDays(30))
                                                        {
                                                            statusId = 4;            //Expiring
                                                        }
                                                        else if (DateTimeExtensions.GetDate(Convert.ToString(ExpiryDate.ToString("dd/MM/yyyy"))).Date > Convert.ToDateTime(DateTime.Now).Date)
                                                        {
                                                            statusId = 2;            //Active
                                                        }
                                                        else
                                                        {
                                                            statusId = 3;        //Expired
                                                        }
                                                    }
                                                    else
                                                        statusId = 1;       //Draft                            

                                                    if (newLicenseID > 0)
                                                    {
                                                        licenseRecord.ID = newLicenseID;
                                                        string status = string.Empty;
                                                        if (statusId == 2)
                                                        {
                                                            status = "Active";
                                                        }
                                                        else if (statusId == 3)
                                                        {
                                                            status = "Expired";
                                                        }
                                                        else if (statusId == 4)
                                                        {
                                                            status = "Expiring";
                                                        }
                                                        //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                        //{
                                                        //    CustomerID = licenseRecord.CustomerID,
                                                        //    LicenseID = newLicenseID,
                                                        //    StatusID = statusId,
                                                        //    StatusChangeOn = DateTime.Now,
                                                        //    IsActive = true,
                                                        //    CreatedBy = AuthenticationHelper.UserID,
                                                        //    CreatedOn = DateTime.Now,
                                                        //    UpdatedBy = AuthenticationHelper.UserID,
                                                        //    UpdatedOn = DateTime.Now,
                                                        //    Remark = "New license created"
                                                        //};
                                                        //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                                        Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                                                        {
                                                            CustomerID = licenseRecord.CustomerID,
                                                            LicenseID = newLicenseID,
                                                            StatusID = statusId,
                                                            IsActive = true,
                                                            CreatedBy = AuthenticationHelper.UserID,
                                                            Remark = "Change license status to " + status + ""
                                                        };
                                                        //StartDate
                                                        if (!string.IsNullOrEmpty(Convert.ToString((startDate))))
                                                            newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(Convert.ToString((startDate.ToString("dd/MM/yyyy"))));

                                                        //EndDate
                                                        if (!string.IsNullOrEmpty(Convert.ToString((ExpiryDate))))
                                                            newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(Convert.ToString((ExpiryDate.ToString("dd/MM/yyyy"))));

                                                        saveSuccess = LicenseMgmt.CreateLicenseAuditLog(newLicenseInstance_Log);
                                                    }
                                                }
                                                else
                                                {
                                                    existsMessage.Add("License No At Row - " + i + " are already exists, License No:-" + LicenseNo + "");
                                                }
                                               
                                                #endregion

                                                #region Statutory Non Statutory Compliances Entry                       
                                                try
                                                {
                                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                    {
                                                        long performerId = -1;
                                                        long reviewerId = -1;
                                                        long approverId = -1;
                                                        if (licenseRecord.ID > 0)
                                                        {
                                                            //Statutory Type
                                                            if (licenseRecord.IsStatutory == true)
                                                            {
                                                                #region Statutory
                                                                List<long> lstExistingStatCompLst = LicenseMgmt.GetLicenseToComplianceInstanceMappingList(licenseRecord.ID);
                                                                if (lstExistingStatCompLst.Count == 0)
                                                                {
                                                                    try
                                                                    {
                                                                        long complianceInstanceId = 0;
                                                                        long complianceScheduleOnId = 0;
                                                                        long complianceTransactionId = 0;

                                                                        var tempdetails = (from TAT in entities.TempAssignmentTables
                                                                                           where TAT.ComplianceId == ComplianeId
                                                                                           && TAT.CustomerBranchID == licenseRecord.CustomerBranchID
                                                                                           select TAT).ToList();
                                                                        if (tempdetails.Count > 0)
                                                                        {
                                                                            performerId = (from pr in tempdetails
                                                                                           where pr.RoleID == 3
                                                                                           select pr.UserID).FirstOrDefault();

                                                                            reviewerId = (from pr in tempdetails
                                                                                          where pr.RoleID == 4
                                                                                          select pr.UserID).FirstOrDefault();

                                                                            approverId = (from pr in tempdetails
                                                                                          where pr.RoleID == 6
                                                                                          select pr.UserID).FirstOrDefault();

                                                                            if (licenseRecord.EndDate != null)
                                                                            {
                                                                                ComplianceInstance compInstance = new ComplianceInstance()
                                                                                {
                                                                                    ComplianceId = ComplianeId,
                                                                                    ScheduledOn = licenseRecord.EndDate.Value.Date,
                                                                                    CustomerBranchID = licenseRecord.CustomerBranchID,
                                                                                    GenerateSchedule = true,
                                                                                    IsDeleted = false,
                                                                                    CreatedOn = DateTime.Now,
                                                                                };
                                                                                complianceInstanceId = LicenseMgmt.CreateComplianceInstance(compInstance);
                                                                            }

                                                                            if (complianceInstanceId > 0)
                                                                            {
                                                                                Lic_tbl_LicenseComplianceInstanceMapping licToCompInstanceMapping = new Lic_tbl_LicenseComplianceInstanceMapping()
                                                                                {
                                                                                    LicenseID = licenseRecord.ID,
                                                                                    ComplianceInstanceID = complianceInstanceId,
                                                                                    IsStatutoryORInternal = "S"
                                                                                };
                                                                                LicenseMgmt.CreateLicenseComplianceInstanceMapping(licToCompInstanceMapping);

                                                                                if (performerId > 0)
                                                                                {
                                                                                    ComplianceAssignment complianceAssignmentForPerformer = new ComplianceAssignment()
                                                                                    {
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        RoleID = 3,
                                                                                        UserID = performerId,
                                                                                    };
                                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForPerformer);
                                                                                }
                                                                                if (reviewerId > 0)
                                                                                {
                                                                                    ComplianceAssignment complianceAssignmentForReviewer = new ComplianceAssignment()
                                                                                    {
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        RoleID = 4,
                                                                                        UserID = reviewerId,
                                                                                    };
                                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForReviewer);
                                                                                }
                                                                                if (approverId != -1 && approverId > 0)
                                                                                {
                                                                                    ComplianceAssignment complianceAssignmentForApprover = new ComplianceAssignment()
                                                                                    {
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        RoleID = 6,
                                                                                        UserID = approverId,
                                                                                    };
                                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForApprover);
                                                                                }
                                                                                var AssignedRole = GetAssignedUsers((int)complianceInstanceId);
                                                                                if (licenseRecord.RemindBeforeNoOfDays == 0)
                                                                                {
                                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                                    {
                                                                                        ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        IsActive = true,
                                                                                        IsUpcomingNotDeleted = true,
                                                                                    };
                                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);

                                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                                    {
                                                                                        CustomerID = licenseRecord.CustomerID,
                                                                                        LicenseID = newLicenseID,
                                                                                        StatusID = statusId,
                                                                                        StatusChangeOn = DateTime.Now,
                                                                                        IsActive = true,
                                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                                        CreatedOn = DateTime.Now,
                                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                                        UpdatedOn = DateTime.Now,
                                                                                        Remark = "New license created",
                                                                                        ComplianceScheduleOnID= complianceScheduleOnId,
                                                                                    };
                                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                                                                    Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                                    {
                                                                                        LicenseID = licenseRecord.ID,
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                                        IsActivation = "Active",
                                                                                        IsStatutoryORInternal = "S"
                                                                                    };
                                                                                    LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);


                                                                                    ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                                                    {
                                                                                        ComplianceInstanceId = complianceInstanceId,
                                                                                        StatusId = 1,
                                                                                        Remarks = "New compliance assigned.",
                                                                                        Dated = DateTime.Now,
                                                                                        CreatedBy = UserID,
                                                                                        CreatedByText = UserName,
                                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                                    };
                                                                                    complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                                                    if (AssignedRole.Count > 0)
                                                                                    {
                                                                                        foreach (var roles in AssignedRole)
                                                                                        {
                                                                                            if (roles.RoleID != 6)
                                                                                            {
                                                                                                var reminders = (from RT in entities.ReminderTemplates
                                                                                                                 where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                                 select RT).ToList();

                                                                                                reminders.ForEach(day =>
                                                                                                {
                                                                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                                                                    {
                                                                                                        ComplianceAssignmentID = roles.ID,
                                                                                                        ReminderTemplateID = day.ID,
                                                                                                        ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                                                        RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                                                                    };
                                                                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                                    entities.ComplianceReminders.Add(reminder);

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                        entities.SaveChanges();
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    #region Application Days
                                                                                    DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                                                                    Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                                    {
                                                                                        ScheduleOn = Applicationdate.Date,
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        IsActive = true,
                                                                                        IsUpcomingNotDeleted = true,
                                                                                    };
                                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);
                                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                                    {
                                                                                        CustomerID = licenseRecord.CustomerID,
                                                                                        LicenseID = newLicenseID,
                                                                                        StatusID = statusId,
                                                                                        StatusChangeOn = DateTime.Now,
                                                                                        IsActive = true,
                                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                                        CreatedOn = DateTime.Now,
                                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                                        UpdatedOn = DateTime.Now,
                                                                                        Remark = "New license created",
                                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                                    };
                                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                                                                                    Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                                    {
                                                                                        LicenseID = licenseRecord.ID,
                                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                                        IsActivation = "Application",
                                                                                        IsStatutoryORInternal = "S"
                                                                                    };
                                                                                    LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                                                                    ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                                                    {
                                                                                        ComplianceInstanceId = complianceInstanceId,
                                                                                        StatusId = 1,
                                                                                        Remarks = "New compliance assigned.",
                                                                                        Dated = DateTime.Now,
                                                                                        CreatedBy = UserID,
                                                                                        CreatedByText = UserName,
                                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                                    };
                                                                                    complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                                                    if (AssignedRole.Count > 0)
                                                                                    {
                                                                                        foreach (var roles in AssignedRole)
                                                                                        {
                                                                                            if (roles.RoleID != 6)
                                                                                            {
                                                                                                var reminders = (from RT in entities.ReminderTemplates
                                                                                                                 where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                                 select RT).ToList();

                                                                                                reminders.ForEach(day =>
                                                                                                {
                                                                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                                                                    {
                                                                                                        ComplianceAssignmentID = roles.ID,
                                                                                                        ReminderTemplateID = day.ID,
                                                                                                        ComplianceDueDate = Applicationdate.Date,
                                                                                                        RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                                                                    };
                                                                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                                    entities.ComplianceReminders.Add(reminder);

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                        entities.SaveChanges();
                                                                                    }
                                                                                    #endregion
                                                                                }
                                                                                //TemplateAssignment IsActive set to 0
                                                                                LicenseMgmt.UpdateTemplateAssignment(ComplianeId, licenseRecord.CustomerBranchID);
                                                                            }
                                                                        }
                                                                        saveSuccess = true;
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                                        cvErrorLicenseListPage.IsValid = false;
                                                                        cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                                                        vsLicenseListPage.CssClass = "alert alert-danger";
                                                                        saveSuccess = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    cvErrorLicenseListPage.IsValid = false;
                                                                    cvErrorLicenseListPage.ErrorMessage = "Compliance already Assigned to License";
                                                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                                                }
                                                                #endregion
                                                            }                                                          
                                                        }
                                                    }//Using End
                                                }
                                                catch (Exception ex)
                                                {
                                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    cvErrorLicenseListPage.IsValid = false;
                                                    cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                                }
                                                #endregion                                                                                            
                                            }

                                            if (existsMessage.Count > 0)
                                            {
                                                showErrorMessages(existsMessage, cvErrorLicenseListPage);
                                                vsLicenseListPage.CssClass = "alert alert-danger";
                                            }
                                            else
                                            {
                                                if (saveSuccess)
                                                {
                                                    cvErrorLicenseListPage.IsValid = false;
                                                    cvErrorLicenseListPage.ErrorMessage = "Compliance Created and Assigned Successfully";
                                                    vsLicenseListPage.CssClass = "alert alert-success";
                                                }
                                                else
                                                {
                                                    cvErrorLicenseListPage.IsValid = false;
                                                    cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    cvErrorLicenseListPage.IsValid = false;
                                    cvErrorLicenseListPage.ErrorMessage = "Please correct the sheet name.";
                                }
                            }
                        }
                    }
                    else
                    {
                        cvErrorLicenseListPage.IsValid = false;
                        cvErrorLicenseListPage.ErrorMessage = "Please select customer.";
                    }
                }

                liAssigned1.Attributes.Add("class", "active");
                liNotAssigned1.Attributes.Add("class", "");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane active");
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                }
                if (customerID != 0)
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        // if (Session["grdLicenseDetailData1"] != null)
                        {
                            int branchID = -1;
                            int deptID = -1;
                            string IsStatutory = "S";
                            long licenseTypeID = -1;
                            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                            {
                                branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                            }
                            locationList.Clear();

                            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                            {
                                RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                            }

                            if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory.SelectedValue))
                            {
                                IsStatutory = ddlIsStatutoryNonStatutory.SelectedValue;
                            }
                            if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                            {
                                licenseTypeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                            }
                            //  var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                            if (customerID != 0)
                            {
                                List<Lic_SP_GetAssignedCompliances_All_Result> MasterTransction = new List<Lic_SP_GetAssignedCompliances_All_Result>();
                                if (IsStatutory == "S")
                                {
                                    MasterTransction = LicenseMgmt.GetAssignedCompliancesList(customerID, AuthenticationHelper.UserID,
                                           3, locationList, deptID, IsStatutory, licenseTypeID);

                                }
                                else if (IsStatutory == "N")
                                {
                                    Lic_SP_GetAssignedCompliances_All_Result obj = new Lic_SP_GetAssignedCompliances_All_Result();
                                    MasterTransction.Add(obj); //Add empty object to list
                                }
                                String FileName = String.Empty;
                                FileName = "LicenseDatasheet";
                                ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("StatutoryLicense");
                                DataTable ExcelData = null;
                                DataView view = new System.Data.DataView((DataTable)(MasterTransction).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "Id", "CustomerBranchID", "LicensetypeId", "Description", "CustomerBranchName", "LicenseTypeName");
                                ExcelData.Columns.Add("LicenseNo");
                                ExcelData.Columns.Add("LicenseTitle");
                                ExcelData.Columns.Add("StartDate");
                                ExcelData.Columns.Add("EndDate");
                                ExcelData.Columns.Add("ApplicationDays");
                                ExcelData.Columns.Add("FileNo");
                                ExcelData.Columns.Add("PhysicalLocation");
                                ExcelData.Columns.Add("Cost");
                                if (ExcelData.Rows.Count > 0)
                                {

                                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet.Cells["A1"].Merge = true;
                                    exWorkSheet.Cells["A1"].Value = "ComplianceId";
                                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["B1"].Merge = true;
                                    exWorkSheet.Cells["B1"].Value = "CustomerBranchID";
                                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["C1"].Merge = true;
                                    exWorkSheet.Cells["C1"].Value = "LicenseTypeId";
                                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["C1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["D1"].Merge = true;
                                    exWorkSheet.Cells["D1"].Value = "Compliance";
                                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["D1"].AutoFitColumns(50);

                                    exWorkSheet.Cells["E1"].Merge = true;
                                    exWorkSheet.Cells["E1"].Value = "CustomerBranchName";
                                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["E1"].AutoFitColumns(50);

                                    exWorkSheet.Cells["F1"].Merge = true;
                                    exWorkSheet.Cells["F1"].Value = "LicenseTypeName";
                                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["F1"].AutoFitColumns(50);

                                    exWorkSheet.Cells["G1"].Merge = true;
                                    exWorkSheet.Cells["G1"].Value = "LicenseNo";
                                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["G1"].AutoFitColumns(50);

                                    exWorkSheet.Cells["H1"].Merge = true;
                                    exWorkSheet.Cells["H1"].Value = "LicenseTitle";
                                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["H1"].AutoFitColumns(50);


                                    exWorkSheet.Cells["I1"].Merge = true;
                                    exWorkSheet.Cells["I1"].Value = "StartDate (DD-MMM-YYYY)";
                                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["I1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["J1"].Merge = true;
                                    exWorkSheet.Cells["J1"].Value = "EndDate (DD-MMM-YYYY)";
                                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["J1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["K1"].Merge = true;
                                    exWorkSheet.Cells["K1"].Value = "ApplicationDays";
                                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["K1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["L1"].Merge = true;
                                    exWorkSheet.Cells["L1"].Value = "FileNo";
                                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["L1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["M1"].Merge = true;
                                    exWorkSheet.Cells["M1"].Value = "PhysicalLocation";
                                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["M1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["N1"].Merge = true;
                                    exWorkSheet.Cells["N1"].Value = "Cost";
                                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["N1"].AutoFitColumns(30);



                                    //Assign borders
                                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 14])
                                    {
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    cvErrorLicenseListPage.IsValid = false;
                                    cvErrorLicenseListPage.ErrorMessage = "No data available to export for current selection(s)";
                                }
                            }
                        }
                    }
                }
                else
                {
                    cvErrorLicenseListPage.IsValid = false;
                    cvErrorLicenseListPage.ErrorMessage = "Please select Customer";
                }

                liAssigned1.Attributes.Add("class", "active");
                liNotAssigned1.Attributes.Add("class", "");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane active");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {              
                BindGrid();
                bindPageNumber();
                ShowGridDetail();

                liAssigned1.Attributes.Add("class", "active");
                liNotAssigned1.Attributes.Add("class", "");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane active");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        protected void btnlocation1_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid1();
                bindPageNumber1();
                ShowGridDetail1();

                liAssigned1.Attributes.Add("class", "");
                liNotAssigned1.Attributes.Add("class", "active");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane active");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        public void BindGrid()
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                string IsStatutory = "S";
                long licenseTypeID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory.SelectedValue))
                {
                    IsStatutory = ddlIsStatutoryNonStatutory.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                }
              //  var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<Lic_SP_GetAssignedCompliances_All_Result> MasterTransction = new List<Lic_SP_GetAssignedCompliances_All_Result>();
                if (IsStatutory == "S")
                {
                    MasterTransction = LicenseMgmt.GetAssignedCompliancesList(customerID, AuthenticationHelper.UserID,
                           3, locationList, deptID, IsStatutory, licenseTypeID);
                    grdLicenseList.Columns[1].Visible = true;
                    grdLicenseList.Columns[2].Visible = true;
                    grdLicenseList.Columns[3].Visible = true;
                }
                else if (IsStatutory == "N")
                {
                    Lic_SP_GetAssignedCompliances_All_Result obj = new Lic_SP_GetAssignedCompliances_All_Result();
                    MasterTransction.Add(obj); //Add empty object to list
                    grdLicenseList.Columns[1].Visible = false;
                    grdLicenseList.Columns[2].Visible = false;
                    grdLicenseList.Columns[3].Visible = false;
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                Session["grdLicenseDetailData1"] = (MasterTransction).ToDataTable();
                Session["TotalRows"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }

                if (grdLicenseList.Rows.Count > 0)
                {
                    noteId.Visible = false;
                }
                else
                {
                    noteId.Visible = true;                    
                }
                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGrid1()
        {
            try
            {
                int branchID = -1;
                int deptID = -1;
                string IsStatutory = "S";
                long licenseTypeID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation1.SelectedValue) && tvFilterLocation1.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation1.SelectedValue);
                }
                locationList1.Clear();

                for (int i = 0; i < this.tvFilterLocation1.Nodes.Count; i++)
                {
                    RetrieveNodes1(this.tvFilterLocation1.Nodes[i]);
                }
                if (!string.IsNullOrEmpty(ddlFilterCustomer1.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer1.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory1.SelectedValue))
                {
                    IsStatutory = ddlIsStatutoryNonStatutory1.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType1.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt64(ddlLicenseType1.SelectedValue);
                }
                List<Lic_SP_GetTempActivatedCompliances_All_Result> MasterTransction = new List<Lic_SP_GetTempActivatedCompliances_All_Result>();
                if (IsStatutory == "S")
                {
                    MasterTransction = LicenseMgmt.GetTempActivatedCompliancesList(customerID, AuthenticationHelper.UserID,
                           3, locationList1, deptID, IsStatutory, licenseTypeID);
                    grdLicenseList1.Columns[1].Visible = true;
                    grdLicenseList1.Columns[2].Visible = true;
                    grdLicenseList1.Columns[3].Visible = true;
                }
                else if (IsStatutory == "N")
                {
                    Lic_SP_GetTempActivatedCompliances_All_Result obj = new Lic_SP_GetTempActivatedCompliances_All_Result();
                    MasterTransction.Add(obj); //Add empty object to list
                    grdLicenseList1.Columns[1].Visible = false;
                    grdLicenseList1.Columns[2].Visible = false;
                    grdLicenseList1.Columns[3].Visible = false;
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                Session["grdLicenseDetailData1"] = (MasterTransction).ToDataTable();
                Session["TotalRows1"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList1.DataSource = MasterTransction;
                    grdLicenseList1.DataBind();
                    Session["TotalRows1"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList1.DataSource = MasterTransction;
                    grdLicenseList1.DataBind();
                    Session["TotalRows1"] = null;
                }

                if (grdLicenseList1.Rows.Count > 0)
                {
                    noteId.Visible = false;
                }
                else
                {
                    noteId.Visible = true;
                }
                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindLicenseType()
        {
            List<Lic_tbl_LicenseType_Master> data = new List<Lic_tbl_LicenseType_Master>();
            if (string.IsNullOrEmpty(user_Roles))
            {
                user_Roles = AuthenticationHelper.Role;
            }
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseType();
            }
            else
            {
                data = LicenseTypeMasterManagement.GetUserWiseLicenseType(AuthenticationHelper.UserID);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";
            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();
            ddlLicenseType.Items.Insert(0, new ListItem("License Type Name", "-1"));


            //Page DropDown
            ddlLicenseType1.DataTextField = "Name";
            ddlLicenseType1.DataValueField = "ID";
            ddlLicenseType1.DataSource = data;
            ddlLicenseType1.DataBind();
            ddlLicenseType1.Items.Insert(0, new ListItem("License Type Name", "-1"));

        }       
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid1_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid1();
                bindPageNumber1();
                ShowGridDetail1();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void bindPageNumber1()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount1());
                if (DropDownListPageNo1.Items.Count > 0)
                {
                    DropDownListPageNo1.Items.Clear();
                }
                DropDownListPageNo1.DataTextField = "ID";
                DropDownListPageNo1.DataValueField = "ID";
                DropDownListPageNo1.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo1.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo1.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo1.Items.Add("0");
                    DropDownListPageNo1.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private void ShowGridDetail1()
        {
            if (Session["TotalRows1"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize1.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize1.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo1.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo1.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows1"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord1.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord1.Text = "0";

                lblEndRecord1.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord1.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord1.Text = "0";
                lblEndRecord1.Text = "0 ";
                lblTotalRecord1.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int GetTotalPagesCount1()
        {
            try
            {
                TotalRows1.Value = Session["TotalRows1"].ToString();
                int totalPages = Convert.ToInt32(TotalRows1.Value) / Convert.ToInt32(ddlPageSize1.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows1.Value) % Convert.ToInt32(ddlPageSize1.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLicenseList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void ddlPageSize1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList1.PageSize = Convert.ToInt32(ddlPageSize1.SelectedValue);
                BindGrid1();
                bindPageNumber1();
                int count = Convert.ToInt32(GetTotalPagesCount1());
                if (count > 0)
                {
                    int gridindex = grdLicenseList1.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo1.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = AuthenticationHelper.UserID;
                if (e.CommandName.Equals("ViewLicensePopup"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int complianceid = Convert.ToInt32(commandArgs[0]);
                    int branchid = Convert.ToInt32(commandArgs[1]);
                    int LicensetypeID = Convert.ToInt32(commandArgs[2]);

                    long PerformerID = -1;
                    long ReviewerID = -1;
                    long ApproverID = -1;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var tempdetails = (from TAT in entities.TempAssignmentTables
                                           join LTLCM in entities.Lic_tbl_LicType_Compliance_Mapping
                                           on TAT.ComplianceId equals LTLCM.ComplianceID
                                           where TAT.ComplianceId == complianceid
                                           && TAT.CustomerBranchID == branchid
                                           && LTLCM.LicenseTypeID == LicensetypeID                                           
                                           select TAT).Distinct().ToList();
                        if (tempdetails.Count > 0)
                        {
                            PerformerID = (from pr in tempdetails
                                           where pr.RoleID == 3
                                           select pr.UserID).FirstOrDefault();

                            ReviewerID = (from pr in tempdetails
                                          where pr.RoleID == 4
                                          select pr.UserID).FirstOrDefault();

                            ApproverID = (from pr in tempdetails
                                          where pr.RoleID == 6
                                          select pr.UserID).FirstOrDefault();

                            List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                            TempAssignmentTable TempAssP = new TempAssignmentTable();
                            TempAssP.ComplianceId = complianceid;
                            TempAssP.CustomerBranchID = branchid;
                            TempAssP.RoleID = 3;
                            TempAssP.UserID = PerformerID;
                            TempAssP.IsActive = true;
                            TempAssP.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssP);

                            TempAssignmentTable TempAssR = new TempAssignmentTable();
                            TempAssR.ComplianceId = complianceid;
                            TempAssR.CustomerBranchID = branchid;
                            TempAssR.RoleID = 4;
                            TempAssR.UserID = ReviewerID;
                            TempAssR.IsActive = true;
                            TempAssR.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssR);
                            if (ApproverID != -1)
                            {
                                if (ApproverID != 0)
                                {
                                    TempAssignmentTable TempAssA = new TempAssignmentTable();
                                    TempAssA.ComplianceId = complianceid;
                                    TempAssA.CustomerBranchID = branchid;
                                    TempAssA.RoleID = 6;
                                    TempAssA.UserID = ApproverID;
                                    TempAssA.IsActive = true;
                                    TempAssA.CreatedOn = DateTime.Now;
                                    TempassignmentTableList.Add(TempAssA);
                                }
                            }
                            if (TempassignmentTableList.Count > 0)
                            {
                                CreateExcelTempAssignmentTable(TempassignmentTableList);
                                BindGrid();
                                bindPageNumber();
                                ShowGridDetail();
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = AuthenticationHelper.UserID;
                if (e.CommandName.Equals("ViewLicensePopup"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int complianceid = Convert.ToInt32(commandArgs[0]);
                    int branchid = Convert.ToInt32(commandArgs[1]);
                    int LicensetypeID = Convert.ToInt32(commandArgs[2]);

                    long PerformerID = -1;
                    long ReviewerID = -1;
                    long ApproverID = -1;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var tempdetails = (from TAT in entities.TempAssignmentTables
                                           join LTLCM in entities.Lic_tbl_LicType_Compliance_Mapping
                                           on TAT.ComplianceId equals LTLCM.ComplianceID
                                           where TAT.ComplianceId == complianceid
                                           && TAT.CustomerBranchID == branchid
                                           && LTLCM.LicenseTypeID == LicensetypeID
                                           select TAT).Distinct().ToList();
                        if (tempdetails.Count > 0)
                        {
                            PerformerID = (from pr in tempdetails
                                           where pr.RoleID == 3
                                           select pr.UserID).FirstOrDefault();

                            ReviewerID = (from pr in tempdetails
                                          where pr.RoleID == 4
                                          select pr.UserID).FirstOrDefault();

                            ApproverID = (from pr in tempdetails
                                          where pr.RoleID == 6
                                          select pr.UserID).FirstOrDefault();

                            List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                            TempAssignmentTable TempAssP = new TempAssignmentTable();
                            TempAssP.ComplianceId = complianceid;
                            TempAssP.CustomerBranchID = branchid;
                            TempAssP.RoleID = 3;
                            TempAssP.UserID = PerformerID;
                            TempAssP.IsActive = true;
                            TempAssP.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssP);

                            TempAssignmentTable TempAssR = new TempAssignmentTable();
                            TempAssR.ComplianceId = complianceid;
                            TempAssR.CustomerBranchID = branchid;
                            TempAssR.RoleID = 4;
                            TempAssR.UserID = ReviewerID;
                            TempAssR.IsActive = true;
                            TempAssR.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssR);
                            if (ApproverID != -1)
                            {
                                if (ApproverID != 0)
                                {
                                    TempAssignmentTable TempAssA = new TempAssignmentTable();
                                    TempAssA.ComplianceId = complianceid;
                                    TempAssA.CustomerBranchID = branchid;
                                    TempAssA.RoleID = 6;
                                    TempAssA.UserID = ApproverID;
                                    TempAssA.IsActive = true;
                                    TempAssA.CreatedOn = DateTime.Now;
                                    TempassignmentTableList.Add(TempAssA);
                                }
                            }
                            if (TempassignmentTableList.Count > 0)
                            {
                                CreateExcelTempAssignmentTable(TempassignmentTableList);
                                BindGrid1();
                                bindPageNumber();
                                ShowGridDetail();
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public bool CreateExcelTempAssignmentTable(List<TempAssignmentTable> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        entities.TempAssignmentTables.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLicenseList.PageIndex = chkSelectedPage - 1;
            grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            ShowGridDetail();
        }

        protected void DropDownListPageNo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo1.SelectedItem.ToString());
            grdLicenseList1.PageIndex = chkSelectedPage - 1;
            grdLicenseList1.PageSize = Convert.ToInt32(ddlPageSize1.SelectedValue);
            BindGrid();
            ShowGridDetail();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "Entity/Sub-Entity/Location";
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation1.Text = tvFilterLocation1.SelectedNode != null ? tvFilterLocation1.SelectedNode.Text : "Entity/Sub-Entity/Location";
                BindGrid1();
                bindPageNumber1();
                ShowGridDetail1();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divBranches1');", tbxFilterLocation1.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree1", "hideDivBranch1();", true);

                liAssigned1.Attributes.Add("class", "");
                liNotAssigned1.Attributes.Add("class", "active");

                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane active");

                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdLicenseList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                int deptID = -1;

                string IsStatutory = "S";
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory.SelectedValue))
                {
                    IsStatutory = ddlIsStatutoryNonStatutory.SelectedValue;
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<Lic_SP_GetAssignedCompliances_All_Result> MasterTransction = new List<Lic_SP_GetAssignedCompliances_All_Result>();
                MasterTransction = LicenseMgmt.GetAssignedCompliancesList(customerID, AuthenticationHelper.UserID,
                       3, branchList, deptID, IsStatutory, licenseTypeID);

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdLicenseList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicenseList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                if (MasterTransction.Count > 0)
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList1_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                int deptID = -1;

                string IsStatutory = "S";
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation1.SelectedValue) && tvFilterLocation1.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation1.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlFilterCustomer1.SelectedValue))
                {
                    customerID = Convert.ToInt64(ddlFilterCustomer1.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory1.SelectedValue))
                {
                    IsStatutory = ddlIsStatutoryNonStatutory1.SelectedValue;
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<Lic_SP_GetTempActivatedCompliances_All_Result> MasterTransction = new List<Lic_SP_GetTempActivatedCompliances_All_Result>();
                MasterTransction = LicenseMgmt.GetTempActivatedCompliancesList(customerID, AuthenticationHelper.UserID,
                       3, branchList, deptID, IsStatutory, licenseTypeID);

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdLicenseList1.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicenseList1.Columns.IndexOf(field);
                    }
                }
                flag = true;
                if (MasterTransction.Count > 0)
                {
                    grdLicenseList1.DataSource = MasterTransction;
                    grdLicenseList1.DataBind();
                    Session["TotalRows1"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList1.DataSource = MasterTransction;
                    grdLicenseList1.DataBind();
                    Session["TotalRows1"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdLicenseList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdLicenseList1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdLicenseList_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //NewEditIndex property used to determine the index of the row being edited.  
            grdLicenseList.EditIndex = e.NewEditIndex;
            BindGridWithPaging();
        }

        protected void grdLicenseList1_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //NewEditIndex property used to determine the index of the row being edited.  
            grdLicenseList1.EditIndex = e.NewEditIndex;
            BindGridWithPaging1();
        }
        protected void grdLicenseList_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {

            grdLicenseList.EditIndex = -1;
            //Call ShowData method for displaying updated data  
            BindGridWithPaging();
        }
        protected void grdLicenseList1_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {

            grdLicenseList1.EditIndex = -1;
            //Call ShowData method for displaying updated data  
            BindGridWithPaging1();
        }
        protected void grdLicenseList_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdLicenseList.EditIndex = -1;
            BindGridWithPaging();
        }

        protected void grdLicenseList1_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdLicenseList1.EditIndex = -1;
            BindGridWithPaging1();
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void lnkBtn_RebindGrid1_Click(object sender, EventArgs e)
        {
            BindGrid1();
            bindPageNumber1();
            ShowGridDetail1();
        }
        #region License Activation       
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }
        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }        
        protected void btnSaveLicense_Click(object sender, EventArgs e)
        {
            try
            {
                long cid = -1;
                bool formValidateSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();
                #region Validation for Start Date End Date
                foreach (GridViewRow row in grdLicenseList.Rows)
                {
                    if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartDate")).Text))
                    {
                        try
                        {
                            bool check = LicenseCommonMethods.CheckValidDate(((TextBox)row.FindControl("txtStartDate")).Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndDate")).Text))
                    {
                        try
                        {
                            bool check = LicenseCommonMethods.CheckValidDate(((TextBox)row.FindControl("txtEndDate")).Text);
                            if (!check)
                                lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check End Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartDate")).Text) && !string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndDate")).Text))
                    {
                        if (DateTime.Compare(DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtEndDate")).Text.ToString()), DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtStartDate")).Text.ToString())) <= 0)
                        {
                            lstErrorMsg.Add("End Date should be greater than Start Date.");
                        }
                    }
                }

                foreach (GridViewRow row in grdLicenseList.Rows)
                {
                    CheckBox chk = ((CheckBox)row.FindControl("chkbox"));
                    if (chk.Checked)
                    {
                        FileUpload LicenseFileUpload = ((FileUpload)row.FindControl("LicenseFileUpload")) as FileUpload;                        
                        if (LicenseFileUpload.PostedFiles.Count > 0)
                        {
                            string[] validFileTypes = { "exe", "bat", "dll", "css", "js", };
                            string ext = System.IO.Path.GetExtension(LicenseFileUpload.PostedFile.FileName);
                            if (ext == "")
                            {
                                //lstErrorMsg.Add("Please do not upload virus file or blank files or file has no extention.");
                            }
                            else
                            {
                                for (int i = 0; i < validFileTypes.Length; i++)
                                {
                                    if (ext == "." + validFileTypes[i])
                                    {
                                        lstErrorMsg.Add("Please do not upload virus file or blank files.");
                                        break;
                                    }
                                }

                                for (int i = 0; i < LicenseFileUpload.PostedFiles.Count; i++)
                                {
                                    HttpPostedFile uploadfile = LicenseFileUpload.PostedFiles[i];
                                    if (uploadfile.ContentLength > 0)
                                    {
                                    }
                                    else
                                    {
                                        lstErrorMsg.Add("Please do not upload virus file or blank file =>" + uploadfile.FileName);
                                    }
                                }
                            }
                                                   
                        }
                    }
                }
                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvErrorLicenseListPage);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
                }
                else
                    formValidateSuccess = true;

                #endregion

                Page.Validate("LicenseListPageValidationGroup");

                if (Page.IsValid == true && grdLicenseList.Rows.Count > 0 && formValidateSuccess == true)
                {
                  
                    if (AuthenticationHelper.Role.Contains("IMPT"))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                            cid = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
                    }
                    else if (AuthenticationHelper.Role.Contains("CADMN"))
                    {
                        cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                    }
                    else
                    {
                        cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                    }
                    if (cid != -1)
                    {
                        bool statutorySuccess = false;
                        foreach (GridViewRow row in grdLicenseList.Rows)
                        {
                            CheckBox chk = ((CheckBox)row.FindControl("chkbox"));
                            if (chk.Checked)
                            {
                                #region Save License Instance and Status
                                long newLicenseID = 0;
                                Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                {
                                    CustomerID = Convert.ToInt32(cid),
                                    IsDeleted = false,
                                    CreatedBy = loggedInUserId
                                };
                                Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                                {
                                    CustomerID = licenseRecord.CustomerID,
                                    CreatedBy = loggedInUserId,
                                };
                                //LicenseNo
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLicenseNo")).Text))
                                {
                                    licenseRecord.LicenseNo = Convert.ToString(((TextBox)row.FindControl("txtLicenseNo")).Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(((TextBox)row.FindControl("txtLicenseNo")).Text);
                                }
                                //LicenseTitle
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtTitle")).Text))
                                {
                                    licenseRecord.LicenseTitle = Convert.ToString(((TextBox)row.FindControl("txtTitle")).Text);
                                    newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(((TextBox)row.FindControl("txtTitle")).Text);
                                }
                                if (!string.IsNullOrEmpty((((Label)row.FindControl("branchId")).Text)))
                                {
                                    licenseRecord.CustomerBranchID = Convert.ToInt32(((Label)row.FindControl("branchId")).Text);
                                }
                                //LicenseTypeID
                                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                                {
                                    licenseRecord.LicenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                                    newLic_tbl_LicenseInstance_Log.LicenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                                }
                                //StartDate
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartDate")).Text))
                                {
                                    licenseRecord.StartDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtStartDate")).Text);
                                    newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtStartDate")).Text);
                                }
                                //EndDate
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndDate")).Text))
                                {
                                    licenseRecord.EndDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtEndDate")).Text);
                                    newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtEndDate")).Text);
                                }
                                //RemindBeforeNoOfDays
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtApplicationDays")).Text))
                                {
                                    licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(((TextBox)row.FindControl("txtApplicationDays")).Text);
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = Convert.ToInt32(((TextBox)row.FindControl("txtApplicationDays")).Text);
                                }
                                else
                                {
                                    licenseRecord.RemindBeforeNoOfDays = 0;
                                    newLic_tbl_LicenseInstance_Log.RemindBeforeNoOfDays = 0;
                                }
                                licenseRecord.FileNO = Convert.ToString(((TextBox)row.FindControl("txtfileno")).Text);
                                licenseRecord.PhysicalLocation = Convert.ToString(((TextBox)row.FindControl("txtphysicallocation")).Text);
                                //Added by renuka
                                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtCost")).Text))
                                {
                                    licenseRecord.Cost = Convert.ToInt32(((TextBox)row.FindControl("txtCost")).Text);
                                    newLic_tbl_LicenseInstance_Log.Cost = Convert.ToInt32(((TextBox)row.FindControl("txtCost")).Text);
                                }


                                //RecurringNoOfDays                       
                                licenseRecord.RecurringNoOfDays = 0;

                                //IsStatutory
                                bool IsStatutory = false;
                                if (!string.IsNullOrEmpty(ddlIsStatutoryNonStatutory.SelectedValue))
                                {
                                    if (ddlIsStatutoryNonStatutory.SelectedValue == "S")
                                        IsStatutory = true;
                                    else
                                        IsStatutory = false;
                                }
                                licenseRecord.IsStatutory = IsStatutory;
                                newLic_tbl_LicenseInstance_Log.IsStatutory = IsStatutory;
                                bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(cid), licenseRecord.LicenseNo);
                                int statusId = 0;
                                if (!existLicNo)
                                {
                                    //if (!LicenseMgmt.ExistsLicenseTitle(Convert.ToInt32(customerID), licenseRecord.LicenseTitle, 0))
                                    //{
                                    newLicenseID = LicenseMgmt.CreateLicense(licenseRecord);
                                    newLic_tbl_LicenseInstance_Log.LicenseID = newLicenseID;
                                    LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);
                                 
                                    //start date and end date if not empty
                                    if (!string.IsNullOrEmpty(Convert.ToString(((TextBox)row.FindControl("txtStartDate")).Text)) || !string.IsNullOrEmpty(Convert.ToString(((TextBox)row.FindControl("txtEndDate")).Text)))
                                    {
                                        if (DateTimeExtensions.GetDate(Convert.ToString(((TextBox)row.FindControl("txtEndDate")).Text)).Date <= DateTime.Today.Date)
                                        {
                                            statusId = 3;        //Expired
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(((TextBox)row.FindControl("txtEndDate")).Text)).Date <= DateTime.Today.Date.AddDays(30))
                                        {
                                            statusId = 4;            //Expiring
                                        }
                                        else if (DateTimeExtensions.GetDate(Convert.ToString(((TextBox)row.FindControl("txtEndDate")).Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                        {
                                            statusId = 2;            //Active
                                        }
                                        else
                                        {
                                            statusId = 3;        //Expired
                                        }
                                    }
                                    else
                                        statusId = 1;       //Draft                            

                                    if (newLicenseID > 0)
                                    {
                                        licenseRecord.ID = newLicenseID;
                                        string status = string.Empty;
                                        if (statusId == 2)
                                        {
                                            status = "Active";
                                        }
                                        else if (statusId == 3)
                                        {
                                            status = "Expired";
                                        }
                                        else if (statusId == 4)
                                        {
                                            status = "Expiring";
                                        }
                                        //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                        //{
                                        //    CustomerID = licenseRecord.CustomerID,
                                        //    LicenseID = newLicenseID,
                                        //    StatusID = statusId,
                                        //    StatusChangeOn = DateTime.Now,
                                        //    IsActive = true,
                                        //    CreatedBy = AuthenticationHelper.UserID,
                                        //    CreatedOn = DateTime.Now,
                                        //    UpdatedBy = AuthenticationHelper.UserID,
                                        //    UpdatedOn = DateTime.Now,
                                        //    Remark = "New license created"
                                        //};
                                        //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                        Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                                        {
                                            CustomerID = licenseRecord.CustomerID,
                                            LicenseID = newLicenseID,
                                            StatusID = statusId,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            Remark = "Change license status to " + status + ""
                                        };
                                        //StartDate
                                        if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartDate")).Text))
                                            newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtStartDate")).Text);

                                        //EndDate
                                        if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndDate")).Text))
                                            newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(((TextBox)row.FindControl("txtEndDate")).Text);

                                        saveSuccess = LicenseMgmt.CreateLicenseAuditLog(newLicenseInstance_Log);
                                    }
                                    //}
                                    //else
                                    //{
                                    //    cvErrorLicenseListPage.IsValid = false;
                                    //    cvErrorLicenseListPage.ErrorMessage = "License Details with Same Title already Exists.";
                                    //    vsLicenseListPage.CssClass = "alert alert-danger";
                                    //    formValidateSuccess = false;
                                    //}
                                }
                                else
                                {
                                    cvErrorLicenseListPage.IsValid = false;
                                    cvErrorLicenseListPage.ErrorMessage = "License Details with Same License Number already exists";
                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                    formValidateSuccess = false;
                                }

                                if (saveSuccess)
                                {
                                    cvErrorLicenseListPage.IsValid = false;
                                    cvErrorLicenseListPage.ErrorMessage = "License Details Created Successfully";
                                    vsLicenseListPage.CssClass = "alert alert-success";
                                    //btnSaveLicense.Enabled = false;
                                }
                                #endregion

                                #region Statutory Non Statutory Compliances Entry                       
                                try
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        long performerId = -1;
                                        long reviewerId = -1;
                                        long approverId = -1;
                                        if (formValidateSuccess && licenseRecord.ID > 0)
                                        {
                                            //Statutory Type
                                            if (licenseRecord.IsStatutory == true)
                                            {
                                                #region Statutory
                                                List<long> lstExistingStatCompLst = LicenseMgmt.GetLicenseToComplianceInstanceMappingList(licenseRecord.ID);
                                                if (lstExistingStatCompLst.Count == 0)
                                                {
                                                    try
                                                    {
                                                        long complianceId = 0;
                                                        long complianceInstanceId = 0;
                                                        long complianceScheduleOnId = 0;
                                                        long complianceTransactionId = 0;

                                                        if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtComplianceID")).Text))
                                                            complianceId = Convert.ToInt64(((TextBox)row.FindControl("txtComplianceID")).Text);

                                                        statutorySuccess = false;

                                                        var tempdetails = (from TAT in entities.TempAssignmentTables
                                                                           where TAT.ComplianceId == complianceId
                                                                           && TAT.CustomerBranchID == licenseRecord.CustomerBranchID
                                                                           select TAT).ToList();
                                                        if (tempdetails.Count > 0)
                                                        {
                                                            performerId = (from pr in tempdetails
                                                                           where pr.RoleID == 3
                                                                           select pr.UserID).FirstOrDefault();

                                                            reviewerId = (from pr in tempdetails
                                                                          where pr.RoleID == 4
                                                                          select pr.UserID).FirstOrDefault();

                                                            approverId = (from pr in tempdetails
                                                                          where pr.RoleID == 6
                                                                          select pr.UserID).FirstOrDefault();

                                                            if (licenseRecord.EndDate != null)
                                                            {
                                                                ComplianceInstance compInstance = new ComplianceInstance()
                                                                {
                                                                    ComplianceId = complianceId,
                                                                    ScheduledOn = licenseRecord.EndDate.Value.Date,
                                                                    CustomerBranchID = licenseRecord.CustomerBranchID,
                                                                    GenerateSchedule = true,
                                                                    IsDeleted = false,
                                                                    CreatedOn = DateTime.Now,
                                                                };
                                                                complianceInstanceId = LicenseMgmt.CreateComplianceInstance(compInstance);
                                                            }

                                                            if (complianceInstanceId > 0)
                                                            {
                                                                Lic_tbl_LicenseComplianceInstanceMapping licToCompInstanceMapping = new Lic_tbl_LicenseComplianceInstanceMapping()
                                                                {
                                                                    LicenseID = licenseRecord.ID,
                                                                    ComplianceInstanceID = complianceInstanceId,
                                                                    IsStatutoryORInternal = "S"
                                                                };
                                                                LicenseMgmt.CreateLicenseComplianceInstanceMapping(licToCompInstanceMapping);

                                                                if (performerId > 0)
                                                                {
                                                                    ComplianceAssignment complianceAssignmentForPerformer = new ComplianceAssignment()
                                                                    {
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        RoleID = 3,
                                                                        UserID = performerId,
                                                                    };
                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForPerformer);
                                                                }
                                                                if (reviewerId > 0)
                                                                {
                                                                    ComplianceAssignment complianceAssignmentForReviewer = new ComplianceAssignment()
                                                                    {
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        RoleID = 4,
                                                                        UserID = reviewerId,
                                                                    };
                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForReviewer);
                                                                }
                                                                if (approverId != -1 && approverId > 0)
                                                                {
                                                                    ComplianceAssignment complianceAssignmentForApprover = new ComplianceAssignment()
                                                                    {
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        RoleID = 6,
                                                                        UserID = approverId,
                                                                    };
                                                                    LicenseMgmt.CreateComplianceAssignment(complianceAssignmentForApprover);
                                                                }
                                                                var AssignedRole = GetAssignedUsers((int)complianceInstanceId);
                                                                if (licenseRecord.RemindBeforeNoOfDays == 0)
                                                                {
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);
                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = licenseRecord.CustomerID,
                                                                        LicenseID = newLicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "New license created",
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                                                                    Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                    {
                                                                        LicenseID = licenseRecord.ID,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        IsActivation = "Active",
                                                                        IsStatutoryORInternal = "S"
                                                                    };
                                                                    LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);


                                                                    ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                                    {
                                                                        ComplianceInstanceId = complianceInstanceId,
                                                                        StatusId = 1,
                                                                        Remarks = "New compliance assigned.",
                                                                        Dated = DateTime.Now,
                                                                        CreatedBy = UserID,
                                                                        CreatedByText = UserName,
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                                    if (AssignedRole.Count > 0)
                                                                    {
                                                                        foreach (var roles in AssignedRole)
                                                                        {
                                                                            if (roles.RoleID != 6)
                                                                            {
                                                                                var reminders = (from RT in entities.ReminderTemplates
                                                                                                 where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                 select RT).ToList();

                                                                                reminders.ForEach(day =>
                                                                                {
                                                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                                                    {
                                                                                        ComplianceAssignmentID = roles.ID,
                                                                                        ReminderTemplateID = day.ID,
                                                                                        ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                                        RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                                                    };
                                                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                    entities.ComplianceReminders.Add(reminder);

                                                                                });
                                                                            }
                                                                        }
                                                                        entities.SaveChanges();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    #region Application Days
                                                                    DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                                                    Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(licenseRecord.RemindBeforeNoOfDays));
                                                                    ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                                                    {
                                                                        ScheduleOn = Applicationdate.Date,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        IsActive = true,
                                                                        IsUpcomingNotDeleted = true,
                                                                    };
                                                                    complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);
                                                                    Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                                                    {
                                                                        CustomerID = licenseRecord.CustomerID,
                                                                        LicenseID = newLicenseID,
                                                                        StatusID = statusId,
                                                                        StatusChangeOn = DateTime.Now,
                                                                        IsActive = true,
                                                                        CreatedBy = AuthenticationHelper.UserID,
                                                                        CreatedOn = DateTime.Now,
                                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                                        UpdatedOn = DateTime.Now,
                                                                        Remark = "New license created",
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                                                                    Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                                                    {
                                                                        LicenseID = licenseRecord.ID,
                                                                        ComplianceInstanceID = complianceInstanceId,
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                        IsActivation = "Application",
                                                                        IsStatutoryORInternal = "S"
                                                                    };
                                                                    LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                                                    ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                                                    {
                                                                        ComplianceInstanceId = complianceInstanceId,
                                                                        StatusId = 1,
                                                                        Remarks = "New compliance assigned.",
                                                                        Dated = DateTime.Now,
                                                                        CreatedBy = UserID,
                                                                        CreatedByText = UserName,
                                                                        ComplianceScheduleOnID = complianceScheduleOnId,
                                                                    };
                                                                    complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                                                    if (AssignedRole.Count > 0)
                                                                    {
                                                                        foreach (var roles in AssignedRole)
                                                                        {
                                                                            if (roles.RoleID != 6)
                                                                            {
                                                                                var reminders = (from RT in entities.ReminderTemplates
                                                                                                 where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                                                 select RT).ToList();

                                                                                reminders.ForEach(day =>
                                                                                {
                                                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                                                    {
                                                                                        ComplianceAssignmentID = roles.ID,
                                                                                        ReminderTemplateID = day.ID,
                                                                                        ComplianceDueDate = Applicationdate.Date,
                                                                                        RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                                                    };
                                                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                                                    entities.ComplianceReminders.Add(reminder);

                                                                                });
                                                                            }
                                                                        }
                                                                        entities.SaveChanges();
                                                                    }
                                                                    #endregion
                                                                }

                                                                #region Upload File
                                                                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                                                List<FileData> files = new List<FileData>();
                                                                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                                                FileUpload LicenseFileUpload = ((FileUpload)row.FindControl("LicenseFileUpload")) as FileUpload;
                                                                bool blankfileCount = true;
                                                                string directoryPath = null;
                                                                if (LicenseFileUpload.PostedFiles.Count > 0)
                                                                {                                                                   
                                                                    var AWSData = AmazonS3.GetAWSStorageDetail(cid);
                                                                    if (AWSData != null)
                                                                    {
                                                                        #region AWS Storage
                                                                        int customerID = Convert.ToInt32(cid);
                                                                        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                                      
                                                                        string version = null;
                                                                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));

                                                                        directoryPath = "avacomdocuments\\" + customerID + "\\" + licenseRecord.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;

                                                                        IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                                                        S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                                                        if (!di.Exists)
                                                                        {
                                                                            di.Create();
                                                                        }

                                                                        for (int i = 0; i < LicenseFileUpload.PostedFiles.Count; i++)
                                                                        {
                                                                            HttpPostedFile uploadfile = LicenseFileUpload.PostedFiles[i];
                                                                            String fileName = "";
                                                                            fileName = "LicenseDoc_" + uploadfile.FileName;
                                                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                            Guid fileKey = Guid.NewGuid();
                                                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                                                            Stream fs = uploadfile.InputStream;
                                                                            BinaryReader br = new BinaryReader(fs);
                                                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                                            if (uploadfile.ContentLength > 0)
                                                                            {
                                                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                                                string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                                                                string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                                                                string TFilePath = Server.MapPath(TdirectoryPath);
                                                                                if (File.Exists(TFilePath))
                                                                                    File.Delete(TFilePath);
                                                                                if (!Directory.Exists(directoryPath1))
                                                                                {
                                                                                    Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                                                                }

                                                                                FileStream objFileStrm = File.Create(TFilePath);
                                                                                objFileStrm.Close();
                                                                                File.WriteAllBytes(TFilePath, bytes);

                                                                                string AWSpath = "";
                                                                                AWSpath = directoryPath + "\\LicenseDoc_" + uploadfile.FileName;

                                                                                FileInfo localFile = new FileInfo(TFilePath);
                                                                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                                                                if (!s3File.Exists)
                                                                                {
                                                                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                                                                    {
                                                                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                                                                    }
                                                                                }

                                                                                string filepathvalue = string.Empty;
                                                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                                filepathvalue = vale.Replace(@"\", "/");

                                                                                FileData file = new FileData()
                                                                                {
                                                                                    Name = fileName,
                                                                                    FilePath = filepathvalue,
                                                                                    FileKey = fileKey.ToString(),
                                                                                    Version = version,
                                                                                    VersionDate = DateTime.UtcNow,
                                                                                    FileSize = uploadfile.ContentLength,
                                                                                };
                                                                                files.Add(file);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                                    blankfileCount = false;
                                                                            }
                                                                        }
                                                                        #endregion
                                                                    }
                                                                    else
                                                                    {
                                                                        #region Normal Storage
                                                                        int customerID = Convert.ToInt32(cid);
                                                                        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));                                                                        
                                                                        string version = null;
                                                                        version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                        {
                                                                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + licenseRecord.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                                        }
                                                                        else
                                                                        {
                                                                            directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + licenseRecord.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                                        }
                                                                        DocumentManagement.CreateDirectory(directoryPath);
                                                                        for (int i = 0; i < LicenseFileUpload.PostedFiles.Count; i++)
                                                                        {
                                                                            HttpPostedFile uploadfile = LicenseFileUpload.PostedFiles[i];
                                                                            String fileName = "";
                                                                            fileName = "LicenseDoc_" + uploadfile.FileName;
                                                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                                            Guid fileKey = Guid.NewGuid();
                                                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                                                            Stream fs = uploadfile.InputStream;
                                                                            BinaryReader br = new BinaryReader(fs);
                                                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                                            if (uploadfile.ContentLength > 0)
                                                                            {
                                                                                string filepathvalue = string.Empty;
                                                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                                                {
                                                                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                                                    filepathvalue = vale.Replace(@"\", "/");
                                                                                }
                                                                                else
                                                                                {
                                                                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                                                }
                                                                                FileData file = new FileData()
                                                                                {
                                                                                    Name = fileName,
                                                                                    FilePath = filepathvalue,
                                                                                    FileKey = fileKey.ToString(),
                                                                                    Version = version,
                                                                                    VersionDate = DateTime.UtcNow,
                                                                                    FileSize = uploadfile.ContentLength,
                                                                                };
                                                                                files.Add(file);
                                                                            }
                                                                            else
                                                                            {
                                                                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                                                    blankfileCount = false;
                                                                            }
                                                                        }
                                                                        #endregion
                                                                    }
                                                                }
                                                                bool flag = false;
                                                                if (blankfileCount)
                                                                {
                                                                    flag = LicenseMgmt.UploadLicenseCreateDocumentsAWS(complianceTransactionId, complianceScheduleOnId, files, list, Filelist, directoryPath,(int)cid);
                                                                }
                                                                else
                                                                {
                                                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                                    cvErrorLicenseListPage.IsValid = false;
                                                                    cvErrorLicenseListPage.ErrorMessage = "Please do not upload virus file or blank files.";
                                                                }
                                                                #endregion
                                                                //TemplateAssignment IsActive set to 0
                                                                LicenseMgmt.UpdateTemplateAssignment(complianceId, licenseRecord.CustomerBranchID);
                                                            }
                                                            if (complianceInstanceId > 0)
                                                                statutorySuccess = true;
                                                            saveSuccess = statutorySuccess;
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        cvErrorLicenseListPage.IsValid = false;
                                                        cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                                        vsLicenseListPage.CssClass = "alert alert-danger";
                                                        saveSuccess = false;
                                                    }
                                                }
                                                else
                                                {
                                                    cvErrorLicenseListPage.IsValid = false;
                                                    cvErrorLicenseListPage.ErrorMessage = "Compliance already Assigned to License";
                                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                                }
                                                #endregion
                                            }
                                            if (saveSuccess)
                                            {
                                                cvErrorLicenseListPage.IsValid = false;
                                                cvErrorLicenseListPage.ErrorMessage = "Compliance Created and Assigned Successfully";
                                                vsLicenseListPage.CssClass = "alert alert-success";

                                            }
                                            else
                                            {
                                                cvErrorLicenseListPage.IsValid = false;
                                                cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                                vsLicenseListPage.CssClass = "alert alert-danger";
                                            }
                                        }
                                    }//Using End
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    cvErrorLicenseListPage.IsValid = false;
                                    cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again";
                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                }
                                #endregion
                            }
                        }
                        BindGrid();
                        bindPageNumber();
                        ShowGridDetail();
                    }
                }
                else
                {
                    cvErrorLicenseListPage.IsValid = false;
                    cvErrorLicenseListPage.ErrorMessage = "Please Select Filters Properly. ";
                    vsLicenseListPage.CssClass = "alert alert-danger";
                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvErrorLicenseListPage);
                    }
                }

                liAssigned1.Attributes.Add("class", "active");
                liNotAssigned1.Attributes.Add("class", "");
                assignedTab.Attributes.Remove("class");
                assignedTab.Attributes.Add("class", "tab-pane active");
                notAssignedTab.Attributes.Remove("class");
                notAssignedTab.Attributes.Add("class", "tab-pane");

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        protected void grdLicenseList1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList dp = (DropDownList)e.Row.FindControl("ddlPerformer");
                    BindUsers(dp);
                    Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
                    if (lblPerformer != null)
                    {
                        dp.SelectedValue = lblPerformer.Text;
                    }

                    DropDownList dp1 = (DropDownList)e.Row.FindControl("ddlReviewer");
                    BindUsers(dp1);
                    Label lblReviewer = (Label)e.Row.FindControl("lblReviewer");
                    if (lblReviewer != null)
                    {
                        dp1.SelectedValue = lblReviewer.Text;
                    }

                    DropDownList dp2 = (DropDownList)e.Row.FindControl("ddlApprover");
                    BindUsers(dp2);
                    Label lblApprover = (Label)e.Row.FindControl("lblApprover");
                    if (lblApprover != null)
                    {
                        dp2.SelectedValue = lblApprover.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(ddlFilterCustomer1.SelectedValue);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLicense1_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstErrorMsg = new List<string>();
                foreach (GridViewRow row in grdLicenseList1.Rows)
                {
                    CheckBox chk = ((CheckBox)row.FindControl("chkbox"));
                    if (chk.Checked)
                    {
                        string Performer = ((DropDownList)row.FindControl("ddlPerformer")).Text;
                        if (Performer == "-1")
                        {
                            lstErrorMsg.Add("Please select performer");
                            break;
                        }
                        string Reviewer = ((DropDownList)row.FindControl("ddlReviewer")).Text;
                        if (Reviewer == "-1")
                        {
                            lstErrorMsg.Add("Please select Reviewer");
                            break;
                        }
                    }
                }

                if (lstErrorMsg.Count > 0)
                {
                    showErrorMessages(lstErrorMsg, cvErrorLicenseListPage1);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript1", "scrollUpPage1();", true);
                }
                else
                {
                    List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                    foreach (GridViewRow row in grdLicenseList1.Rows)
                    {
                        CheckBox chk = ((CheckBox)row.FindControl("chkbox"));
                        if (chk.Checked)
                        {
                            long complianceId = 0;
                            int CustomerBranchId = 0;
                            long PerformerId = 0;
                            long ReviewerId = 0;
                            long ApproverId = 0;

                            if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtComplianceID1")).Text))
                                complianceId = Convert.ToInt64(((TextBox)row.FindControl("txtComplianceID1")).Text);

                            if (!string.IsNullOrEmpty(((Label)row.FindControl("lblbranchId1")).Text))
                                CustomerBranchId = Convert.ToInt32(((Label)row.FindControl("lblbranchId1")).Text);

                            if (!string.IsNullOrEmpty(((DropDownList)row.FindControl("ddlPerformer")).SelectedValue))
                                PerformerId = Convert.ToInt64(((DropDownList)row.FindControl("ddlPerformer")).SelectedValue);

                            if (!string.IsNullOrEmpty(((DropDownList)row.FindControl("ddlReviewer")).SelectedValue))
                                ReviewerId = Convert.ToInt64(((DropDownList)row.FindControl("ddlReviewer")).SelectedValue);

                            if (!string.IsNullOrEmpty(((DropDownList)row.FindControl("ddlApprover")).SelectedValue))
                                ApproverId = Convert.ToInt64(((DropDownList)row.FindControl("ddlApprover")).SelectedValue);

                        
                            TempAssignmentTable TempAssP = new TempAssignmentTable();
                            TempAssP.ComplianceId = complianceId;
                            TempAssP.CustomerBranchID = CustomerBranchId;
                            TempAssP.RoleID = 3;
                            TempAssP.UserID = PerformerId;
                            TempAssP.IsActive = true;
                            TempAssP.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssP);

                            TempAssignmentTable TempAssR = new TempAssignmentTable();
                            TempAssR.ComplianceId = complianceId;
                            TempAssR.CustomerBranchID = CustomerBranchId;
                            TempAssR.RoleID = 4;
                            TempAssR.UserID = ReviewerId;
                            TempAssR.IsActive = true;
                            TempAssR.CreatedOn = DateTime.Now;
                            TempassignmentTableList.Add(TempAssR);
                            if (ApproverId != -1)
                            {
                                if (ApproverId != 0)
                                {
                                    TempAssignmentTable TempAssA = new TempAssignmentTable();
                                    TempAssA.ComplianceId = complianceId;
                                    TempAssA.CustomerBranchID = CustomerBranchId;
                                    TempAssA.RoleID = 6;
                                    TempAssA.UserID = ApproverId;
                                    TempAssA.IsActive = true;
                                    TempAssA.CreatedOn = DateTime.Now;
                                    TempassignmentTableList.Add(TempAssA);
                                }
                            }
                        }
                    }

                    if (TempassignmentTableList.Count > 0)
                    {
                        CreateExcelTempAssignmentTable(TempassignmentTableList);
                        BindGrid();
                        bindPageNumber();
                        ShowGridDetail();

                        BindGrid1();
                        bindPageNumber1();
                        ShowGridDetail1();

                        cvErrorLicenseListPage1.IsValid = false;
                        cvErrorLicenseListPage1.ErrorMessage = "Assignment saved successfully";
                        vsLicenseListPage1.CssClass = "alert alert-success";
                    }

                    liAssigned1.Attributes.Add("class", "");
                    liNotAssigned1.Attributes.Add("class", "active");

                    notAssignedTab.Attributes.Remove("class");
                    notAssignedTab.Attributes.Add("class", "tab-pane active");

                    assignedTab.Attributes.Remove("class");
                    assignedTab.Attributes.Add("class", "tab-pane");

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divBranches1');", tbxFilterLocation1.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree1", "hideDivBranch1();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddnew_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowLicDialog(" + 0 + ");", true);
        }
    }
}
