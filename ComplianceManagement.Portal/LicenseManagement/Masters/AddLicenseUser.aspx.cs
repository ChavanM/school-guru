﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class AddLicenseUser : System.Web.UI.Page
    {
        public static string LicID = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
         
            if (!IsPostBack)
            {
                ViewState["Mode"] = 0;               
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long ContactNo = 0;
                bool MailValid = false;
                string MailID = string.Empty;

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(tbxContactNo.Text))
                {
                    ContactNo = Convert.ToInt64(tbxContactNo.Text);
                }
                if (!string.IsNullOrEmpty(tbxEmail.Text))
                {
                    MailID = tbxEmail.Text;
                }

                if (ZohoCRMAPI.IsValidEmail(MailID))
                {
                    MailValid = true;
                }                            

                #region License User
                User user = new User()
                {
                    IsActive = false,
                    IsExternal = true,
                    CustomerID = customerID,

                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,

                    RoleID = 7,
                    LicenseRoleID = 7,                    
                };
                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    IsActive = false,
                    IsExternal = true,
                    CustomerID = customerID,

                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,

                    RoleID = -1,
                    LicenseRoleID = 7,                   
                };

                #endregion

                bool emailExists;

                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists";
                    cvDuplicateEntry.CssClass = "alert alert-danger";
                    return;
                }

                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "User with Same Email already Exists";
                    cvDuplicateEntry.CssClass = "alert alert-danger";
                    return;
                }

                bool result = false;
                int newUserID = 0;

                if ((int)ViewState["Mode"] == 0)
                {
                    string passwordText = Util.CreateRandomPassword(10);

                    user.Password = Util.CalculateAESHash(passwordText);
                    mstUser.Password = Util.CalculateAESHash(passwordText);                    

                    if (Convert.ToString(ContactNo).Length == 10 && (Convert.ToString(ContactNo).StartsWith("9") || Convert.ToString(ContactNo).StartsWith("8") || Convert.ToString(ContactNo).StartsWith("7")))
                    {
                        if (MailValid)
                        {
                            newUserID = LicenseUserManagement.CreateNewUserCompliance(user);
                            if (newUserID > 0)
                            {
                                result = LicenseUserManagement.CreateNewUserAudit(mstUser);

                                if (!result)
                                {
                                    
                                    UserManagement.deleteUser(newUserID);
                                }
                                else
                                {
                                    LicID = newUserID.ToString();

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "User Created Successfully";
                                    vsAddUserLicense.CssClass = "alert alert-success";
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Invalid Email, Please enter Email in valid format";
                            cvDuplicateEntry.CssClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid Contact Number or Contact Number should starts with 9, 8 or 7";
                        cvDuplicateEntry.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        [WebMethod]
        public static string getLicUserID()
        {
            
            try
            {
                string CUserID = string.Empty;
                if(LicID!=null)
                {
                    CUserID = LicID;
                }
                return CUserID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "null";
            }
        }

       
    }
}