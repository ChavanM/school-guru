﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using System.Collections.Generic;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class AddInternalLicenseType : System.Web.UI.Page
    {
        protected static long customerID = 0;
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {
                        BindLicense();
                        if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")))
                        {
                            btnAddLicense.Visible = true;
                        }
                        user_Roles = AuthenticationHelper.Role;

                        BindCustomerFilter();
                        if (user_Roles.Contains("IMPT"))
                        {
                            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                                customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                        }
                        else if (user_Roles.Contains("CADMN"))
                        {
                            CustomerList1.Visible = false;
                            customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                            if (ddlCustomer.Items.Count > 1)
                                ddlCustomer.SelectedValue = Convert.ToString(customerID);
                        }
                        else if (user_Roles.Contains("MGMT"))
                        {
                            CustomerList1.Visible = false;
                            customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                            if (ddlCustomer.Items.Count > 1)
                                ddlCustomer.SelectedValue = Convert.ToString(customerID);
                        }
                        else if (user_Roles.Contains("EXCT"))
                        {
                            CustomerList1.Visible = false;
                            customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                            if (ddlCustomer.Items.Count > 1)
                                ddlCustomer.SelectedValue = Convert.ToString(customerID);
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindCustomerFilter()
        {
            List<Customer> lstCustomers = CustomerManagement.GetAll(-1, null);

            if (lstCustomers.Count > 0)
                lstCustomers = lstCustomers.OrderBy(entry => entry.Name).ToList();

            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";

            ddlCustomer.DataSource = lstCustomers;
            ddlCustomer.DataBind();
        }
        private void BindLicenseType()
        {
            user_Roles = AuthenticationHelper.Role;            
            if (user_Roles.Contains("IMPT"))
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
            }
            else if (user_Roles.Contains("CADMN"))
            {                
                customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);                
            }
            else if (user_Roles.Contains("MGMT"))
            {                
                customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);                
            }
            else if (user_Roles.Contains("EXCT"))
            {                
                customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);                
            }

            var lstInternalcompliancetype = InternalLicenseMgmt.GetInternalComplianceType(Convert.ToInt32(customerID),null);

            if (lstInternalcompliancetype.Count > 0)
                lstInternalcompliancetype = lstInternalcompliancetype.OrderBy(entry => entry.Name).ToList();

            ddlLicneseType.DataTextField = "Name";
            ddlLicneseType.DataValueField = "ID";
            ddlLicneseType.DataSource = lstInternalcompliancetype;
            ddlLicneseType.DataBind();
        }
        
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLicenseType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rdbLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }      
        private void BindLicense()
        {
            try
            {                
                user_Roles = AuthenticationHelper.Role;
                if (user_Roles.Contains("IMPT"))
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                }
                else if (user_Roles.Contains("CADMN"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("MGMT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("EXCT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                grdLicense.DataSource = null;
                grdLicense.DataBind();
                grdLicense.DataSource = LicenseTypeMasterManagement.GetInternalcompliancetypeAll(Convert.ToInt32(customerID), tbxFilter.Text);
                grdLicense.DataBind();
                upLicenseList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLicense_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               if (e.CommandName.Equals("VIEW_Compliances"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/LicenseManagement/Masters/LicenseTypeComplianceMappingList.aspx?LTMLID=" + licenseId, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdLicense_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLicense.PageIndex = e.NewPageIndex;
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddLicense_Click(object sender, EventArgs e)
        {
            try
            {                
                txtComplianceIDList.Text = string.Empty;
                upLicense.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divLicenseDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicense.PageIndex = 0;
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<string> CheckIntenalCompliance(List<long> masterlst, List<long> textboxids)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> emsg = new List<string>();                
                textboxids.ForEach(entry =>
                {
                    if (!masterlst.Contains(entry))
                    {
                        emsg.Add("This compliance not belongs to this customer " + entry + " so it could not be assigned");
                    }                                      
                });
                entities.SaveChanges();
                return emsg.ToList();
            }
        }
        public static List<string> UpdateComplianceMapping(long LicenseTypeID, long Updatedby, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> emsg = new List<string>();
                var ids = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                           where  row.LicenseTypeID == LicenseTypeID      
                           && row.CustomerID== customerID
                           select row).ToList();

                ids.ForEach(entry =>
                {
                    if (!CheckTransactionExists(entry.ComplianceID))
                    {
                        var prevmappedids = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                                             where row.ID == entry.ID                                             
                                             select row).FirstOrDefault();

                        prevmappedids.IsDeleted = true;
                        prevmappedids.UpdatedBy = Updatedby;
                        prevmappedids.UpdatedOn = DateTime.Now;
                    }
                    else
                    {
                        emsg.Add("This compliance " + entry.ComplianceID + " has transaction so it could not be deleted");
                    }
                });
                entities.SaveChanges();
                return emsg.ToList();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                user_Roles = AuthenticationHelper.Role;
                if (user_Roles.Contains("IMPT"))
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                }
                else if (user_Roles.Contains("CADMN"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("MGMT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("EXCT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }

                if (customerID != -1 && customerID != 0)
                {
                    if (!string.IsNullOrEmpty(rdbLicenseType.SelectedValue))
                    {
                        List<string> emsg = new List<string>();
                        #region 
                        List<string> firstemsg = new List<string>();
                        List<string> finalemsg = new List<string>();
                        List<Lic_tbl_LicType_InternalCompliance_Mapping> LTLCMlist = new List<Lic_tbl_LicType_InternalCompliance_Mapping>();
                        List<string> ComplianceTextboxList = new List<string>();
                        List<long> ComplianceList = new List<long>();
                        ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
                        ComplianceList = ComplianceTextboxList.Select(long.Parse).Distinct().ToList();
                        var customerInternalcompliancelist= LicenseTypeMasterManagement.GetInternalcomplianceList(customerID);
                        var notbelongstocustomerids=  CheckIntenalCompliance(customerInternalcompliancelist, ComplianceList);
                        if (notbelongstocustomerids.Count>0)
                        {
                            finalemsg.Add(ErrorMessages1(notbelongstocustomerids.ConvertAll<string>(row => row.ToString()).ToList()));
                        }
                        firstemsg = UpdateComplianceMapping(Convert.ToInt64(ddlLicneseType.SelectedValue), AuthenticationHelper.UserID, customerID);
                        if (firstemsg.Count > 0)
                        {
                            finalemsg.Add(ErrorMessages1(firstemsg.ConvertAll<string>(row => row.ToString()).ToList()));
                        }
                        var checkcompliance = Exists(ComplianceList);
                        if (checkcompliance.Count == 0)
                        {
                            foreach (long clist in ComplianceList)
                            {
                                if (!LTLCMlist.Any(x => x.CustomerID == customerID && x.ComplianceID == Convert.ToInt32(clist) && x.LicenseTypeID == Convert.ToInt32(ddlLicneseType.SelectedValue)))
                                {
                                    Lic_tbl_LicType_InternalCompliance_Mapping LTLCM = new Lic_tbl_LicType_InternalCompliance_Mapping();
                                    LTLCM.LicenseTypeID = Convert.ToInt32(ddlLicneseType.SelectedValue);
                                    LTLCM.ComplianceID = Convert.ToInt32(clist);
                                    LTLCM.CreatedBy = AuthenticationHelper.UserID;
                                    LTLCM.IsDeleted = false;
                                    LTLCM.CustomerID = customerID;
                                    LTLCMlist.Add(LTLCM);
                                }
                            }//foreach end
                        }
                        else
                        {
                            var complianceList = ComplianceList.Except(checkcompliance);
                            foreach (long clist in complianceList)
                            {
                                if (!LTLCMlist.Any(x => x.CustomerID== customerID && x.ComplianceID == Convert.ToInt32(clist) && x.LicenseTypeID == Convert.ToInt32(ddlLicneseType.SelectedValue)))
                                {
                                    Lic_tbl_LicType_InternalCompliance_Mapping LTLCM = new Lic_tbl_LicType_InternalCompliance_Mapping();
                                    LTLCM.LicenseTypeID = Convert.ToInt32(ddlLicneseType.SelectedValue);
                                    LTLCM.ComplianceID = Convert.ToInt32(clist);
                                    LTLCM.CreatedBy = AuthenticationHelper.UserID;
                                    LTLCM.IsDeleted = false;
                                    LTLCM.CustomerID = customerID;
                                    LTLCMlist.Add(LTLCM);
                                }
                            }//foreach end

                            if (checkcompliance.Count > 0)
                            {
                                finalemsg.Add(ErrorMessages(checkcompliance.ConvertAll<string>(row => row.ToString()).ToList()));
                            }
                        }
                        bool checkFlag = false;
                        checkFlag = LicenseTypeMasterManagement.BlukLTLCMInternalComplianceMapping(LTLCMlist);
                        if (checkFlag)
                        {
                            if (finalemsg.Count > 0)
                            {
                                FinalErrorMessages(finalemsg);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        #endregion
                        BindLicense();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<long> Exists(List<long> complianceIDlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> existscompliancelist = new List<long>();
                var query = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                             where row.IsDeleted == false
                             && complianceIDlist.Contains(row.ComplianceID)                             
                             select row.ComplianceID).Distinct().ToList();
                if (query.Count > 0)
                {
                    existscompliancelist = query.ToList();
                }
                return existscompliancelist;
            }
        }
        public static bool UpdateExists(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var query = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                             where row.IsDeleted == false
                             && row.ComplianceID== complianceID                             
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }        
        public static bool CheckTransactionExists(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckInternalComplianceISAssigned(complianceID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public string ErrorMessages1(List<string> emsg)
        {

            string finalErrMsg = string.Empty;

            //finalErrMsg += "<ol type='1'>";
            finalErrMsg += "<ul>";
            //finalErrMsg += "<li>Following complianceid already mapped with license type</li>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            return finalErrMsg;
            //cvDuplicateEntry.IsValid = false;
            //cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        public string ErrorMessages(List<string> emsg)
        {

            string finalErrMsg = string.Empty;

            //finalErrMsg += "<ol type='1'>";
            finalErrMsg += "<ul>";
            finalErrMsg += "<li>Following complianceid already mapped with license type</li>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            return finalErrMsg;
            //cvDuplicateEntry.IsValid = false;
            //cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        public void FinalErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;            
            finalErrMsg += "<ul>";            
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ul>";
            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        protected void upLicense_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void ddlfilterStatutoryNonStatutory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLicense();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                  
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdLicense_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {                
                var licenseList = LicenseTypeMasterManagement.GetAll("I", tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    licenseList = licenseList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    licenseList = licenseList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdLicense.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicense.Columns.IndexOf(field);
                    }
                }
                grdLicense.DataSource = licenseList;
                grdLicense.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdLicense_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdLicense_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //LinkButton lbtEdit = (LinkButton) e.Row.FindControl("lbtEdit");
                    //LinkButton lbtDelete = (LinkButton) e.Row.FindControl("lbtDelete");
                    //lbtEdit.Visible = false;
                    //lbtDelete.Visible = false;
                    //if (AuthenticationHelper.Role.Equals("SADMN"))
                    //{
                    //    lbtEdit.Visible = true;
                    //    lbtDelete.Visible = true;
                    //}
                    //if (AuthenticationHelper.Role.Equals("IMPT"))
                    //{
                    //    lbtEdit.Visible = true;
                    //    lbtDelete.Visible = true;
                    //}
                }            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}