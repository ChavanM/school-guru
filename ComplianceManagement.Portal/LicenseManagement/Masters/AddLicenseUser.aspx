﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLicenseUser.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters.AddLicenseUser" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">
        function CloseMe() {
            window.parent.ClosePopUser();
        }
        $('#ContentPlaceHolder1_tbxFirstName').click(function () {

            alert('empty');
            //process further

        });
    </script>

</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LicenseAddUser" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="upUserDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row">
                             <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCUsr">
                            <asp:Label runat="server" ID="successmsgaCUsr" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsAddUserLicense" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="NewAddLaywerValidate" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-4">
                                <label for="tbxFirstName" class="control-label">First Name</label>
                                <asp:TextBox runat="server" ID="tbxFirstName" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter First Name."
                                    ControlToValidate="tbxFirstName" runat="server"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid First Name."
                                    ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-4">
                                <label for="tbxLastName" class="control-label">Last Name</label>
                                <asp:TextBox runat="server" ID="tbxLastName" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Please Enter Last Name."
                                    ControlToValidate="tbxLastName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid Last Name."
                                    ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-4">
                                <label for="tbxEmail" class="control-label">Email</label>
                                <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Enter Email-ID."
                                    ControlToValidate="tbxEmail" runat="server" ValidationGroup="NewAddLaywerValidate" Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group required col-md-4">
                                <label for="tbxContactNo" class="control-label">Contact No</label>
                                <asp:TextBox runat="server" ID="tbxContactNo" CssClass="form-control" MaxLength="10" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Enter Contact Number."
                                    ControlToValidate="tbxContactNo" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid Contact Number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit."
                                    ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 text-center">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                    ValidationGroup="NewAddLaywerValidate" OnClientClick="addUserClick" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
