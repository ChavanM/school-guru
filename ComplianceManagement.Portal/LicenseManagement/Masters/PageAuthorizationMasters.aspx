﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="PageAuthorizationMasters.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters.PageAuthorizationMasters" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / PageAuthorization');
        });

        function ReloadPage() {
            window.location.reload;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-md-12 colpadding0 text-right">
                <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgColor-gray" style="height: 30px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom col-md-6 plr0">
                                    License Type Authorization</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                            </div>

                            <div class="modal-body" style="background-color: #f7f7f7;">
                                <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="col-lg-12 col-md-12">
                <section class="panel">

                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="vsSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PageAutorizationValidationGroup" />
                        <asp:CustomValidator ID="cvPage" runat="server" EnableClientScript="False"
                            ValidationGroup="PageAutorizationValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div style="margin-bottom: 4px">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="col-md-2" style="width: 11%;">
                                        <asp:DropDownList runat="server" ID="ddlComplianceType" class="form-control" Style="width: 105px;"
                                            OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="Statutory" Value="-1" />
                                            <asp:ListItem Text="Internal" Value="0" />
                                        </asp:DropDownList>
                                    </div> 
                                    <div class="col-md-4">
                                        <asp:DropDownListChosen runat="server" ID="ddlUser" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            CssClass="form-control" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>                                  
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upPageAuthorizatonMaster" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <asp:GridView runat="server" ID="grdDisplayUserdata" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdDisplayUserdata_RowDataBound"
                                    PageSize="12" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                                    DataKeyNames="ID" OnRowCommand="grdDisplayUserdata_RowCommand" OnSorting="grdDisplayUserdata_Sorting" OnRowCreated="grdDisplayUserdata_RowCreated">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LicenseTypeName" HeaderText="License Type Name" ItemStyle-Width="15%" HeaderStyle-Width="68%" SortExpression="LicenseTypeName" />

                                        <asp:TemplateField HeaderText="LicenseTypeID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="LicenseTypeID" runat="server" Text='<%# Eval("LicenseTypeID")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ADD">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkADD" runat="server" AutoPostBack="true" Checked='<%# Eval("IsActive") != null ? bool.Parse(Convert.ToString(Eval("IsActive"))) : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-8 colpadding0">
                                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                            <p style="padding-right: 0px !Important;">
                                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" Selected="True" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 colpadding0" style="float: right;">
                                        <div style="float: left; width: 50%">
                                            <p class="clsPageNo" style="color: #666">Page</p>
                                        </div>
                                        <div style="float: left; width: 50%">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="row">
                        <div class="form-group col-md-12" style="text-align: center; width: 30%; margin-left: 358px;">
                            <div style="text-align: center">
                                <asp:Button Text="Save" runat="server" ID="btnSavePageAutorization" CssClass="btn btn-primary" OnClick="btnSavePageAutorization_Click"
                                    ValidationGroup="ValidLayerRating" CausesValidation="true" Visible="false"></asp:Button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</asp:Content>
