﻿using System;
using System.Collections.Generic;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Reports
{
    public partial class LicenseReportNew : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Custid;
        protected static int UserID;
        protected static string Role;
        protected static string type;
        protected string Internalsatutory;
        protected int LicenseTypeID;
        protected int StatusFlagID;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Custid = Convert.ToString(AuthenticationHelper.CustomerID);
            UserID = AuthenticationHelper.UserID;
            Role = AuthenticationHelper.Role;
            LicenseTypeID = -1;

            if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
            {
                Internalsatutory = Request.QueryString["Internalsatutory"];
                if (Internalsatutory == "Statutory")
                {
                    LicenseTypeID = -1;
                }
                else
                {
                    LicenseTypeID = 0;
                }

            }

            //if (StatusFlagID == -1)
            //{
            //    LicenseTypeID = 0;//statutory        
            //}
            //if (StatusFlagID == 0)
            //{
            //    LicenseTypeID = 2;//Internal
            //}
        }
    }
}