﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="MyReportLicense.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Reports.MyReportLicense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Reports');
        });

        $(window).bind("load", function () {
            $('#updateProgress').hide();
        });

        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                 $("#divFilterLocation").hide();
             } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                 $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

        function ShowDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + LicenseInstanceID);
        };

        function ShowDialogInternal(LicenseInstanceID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + LicenseInstanceID);
        };
    </script>

    <style type="text/css">
        .form-control {
            height: 33px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="LicenseListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>
            <div class="row">

                <div class="col-md-12 colpadding0">
                    <div class="col-md-1 colpadding0" style="padding-right: 3px;">
                        <label for="lblLicenseType" class="filter-label">Show</label>
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; height: 32px !important; margin-right: 6%"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>

                    </div>
                    <div class="col-md-1 colpadding0"  style="width: 10.5%;">
                        <label for="tbxFilterLocation" class="filter-label">Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" class="form-control" Style="width: 110px;"
                            OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" Value="-1" />
                            <asp:ListItem Text="Internal" Value="0" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3 colpadding0" style="width: 25%;">
                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                            <ContentTemplate>
                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 100%" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                        Style="margin-top: -15px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                        ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-md-3 colpadding0" style="width: 25%; padding-left:10px;">
                        <label for="lblLicenseType" class="filter-label">License Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AllowSingleDeselect="false" DisableSearchThreshold="3"
                            class="form-control" Width="100%" />
                    </div>
                    <div class="col-md-2 colpadding0" style="width: 12%;  padding-left:10px;">
                        <label for="ddlStatus" class="filter-label">Status</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                            class="form-control" Width="100%">
                             <asp:ListItem Text="Status" Value="Status" />
                                                <asp:ListItem Text="Active" Value="Active" />
                                                <asp:ListItem Text="Expired" Value="Expired" />
                                                <asp:ListItem Text="Expiring" Value="Expiring" />
                                                <asp:ListItem Text="Applied" Value="Applied" />
                                                <asp:ListItem Text="PendingForReview" Value="PR" />
                                                <asp:ListItem Text="Rejected" Value="Rejected" />
                        </asp:DropDownListChosen>
                         
                    </div>
                    <div class="col-md-2 colpadding0 text-right" style="width: 10%; padding-left:10px;">
                        <div class="col-md-6 colpadding0 float-left">
                            <label for="ddlStatus" class="hidden-label">Filter</label>
                            <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter"
                                OnClick="lnkBtnApplyFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                        </div>
                        <div class="col-md-6 colpadding0 float-right">
                            <label for="ddlStatus" class="hidden-label">Filter</label>
                            <asp:LinkButton Text="Apply" runat="server" ID="btnExportExcel" OnClick="btnExportExcel_Click"
                                Width="80%"
                                data-toggle="tooltip" data-placement="bottom" ToolTip="Export to Excel">
                            <img src="/Images/Excel _icon.png" alt="Export" /> 
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 colpadding0">
                    <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="LicenseID"
                        OnRowCommand="grdLicenseList_RowCommand" OnSorting="grdLicenseList_Sorting" OnRowCreated="grdLicenseList_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="License Type" ItemStyle-Width="20%" SortExpression="LicensetypeName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="License No." ItemStyle-Width="10%" SortExpression="LicenseNo">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Title" ItemStyle-Width="12%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="App Due Date" ItemStyle-Width="15%" SortExpression="ApplicationDate">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'
                                            ToolTip='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="End Date" ItemStyle-Width="10%" SortExpression="EndDate">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%" SortExpression="Status">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 110px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEditLicense" runat="server" OnClick="lnkEditLicense_Click" CommandArgument='<%# Eval("LicenseID") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip="View">                                   
                                    <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                    </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="color: #999">
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-1 text-right colpadding0">
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>

                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                        License Detail(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
