﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseDashboardGrpahDetailsKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard.LicenseDashboardGrpahDetailsKendo" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
          .k-multiselect-wrap .k-input {
            display: inherit !important;
        }
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .dashboard h1 {
            color: #555;
            padding: 0px 0 0px;
            margin: 0 0 0px;
        }

        .Dashboard-white-widget {
            padding: 0px 10px 10px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .modal-header, .panel-heading {
            background: #fff;
            color: #688a7e;
            margin-inline-end: -1676px;
            height: 27px;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus 
        {
           margin-left: -18px;
           cursor: pointer;
        }
    </style>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #EBEBEB;
            border-color: #a6a6ad;
            color: #535b6a;
        }
    </style>

    <script>
        $(document).ready(function () {
            DepartmentType();
            BindLocation();
            BindPastData();
            BindComplianceType();
        });
             function DepartmentType()
          {
                 $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                checkAllTemplate: "Select All",

                change: function (e) {
                    //DataBindDaynamicKendoGriddMain();
                    FilterAll();
                    fCreateStoryBoardGD('dropdownDept', 'filterdept', 'dept');
                    $('input[id=chkAllMain]').prop('checked', false);
                 },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =Custid%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                }
                
            }
        }
            });
             }

        function BindComplianceType() {
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                     { text: "Internal", value: "I" }
                ],
                cascade: function (e) {
                    Bindgrid();
                }
            });
        }

        function BindPastData() {
            $("#dropdownlistLicenseType").kendoDropDownList({
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select License Type",
                autoClose: true,
                change: function (e) {
                    FilterAll();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read :{
                           url : '<% =Path%>License/LicenseTypeList?userID=<% =UserID%>&role=<% =Role %>&isstatutoryinternal=<%=Internalsatutory%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    },
                    },
                }
            });
     }

     function BindLocation() {
         $("#dropdowntree").kendoDropDownTree({
             placeholder: "Entity/Sub-Entity/Location",
             checkboxes: {
                 checkChildren: true
             },
             filter: "contains",
             checkAll: true,
             autoClose: false,
             autoWidth: true,
             checkAllTemplate: "Select All",
             dataTextField: "Name",
             dataValueField: "ID",
             change: function (e) {
                 FilterAll();
                 fCreateStoryBoardGD('dropdowntree', 'filtersstoryboard', 'loc')
             },
             dataSource: {
                 severFiltering: true,
                 transport: {
                     read: {
                         url: '<% =Path%>Data/GetLocationList?customerId=<% =Custid%>&userId=<% =UserID%>&Flag=<% =Role%>&IsStatutoryInternal=<%=Internalsatutory%>',
                             dataType: "json",
                      beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            }
        function Bindgrid() {
            debugger;
                var grid = $('#grid').data("kendoGrid");
                if (grid != undefined || grid != null)
                    $('#grid').empty();
                var grid = $("#grid").kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: '<% =Path%>License/GetGraphDetailData?Userid=<% =UserID %>&Customerid=<% =Custid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&IsFlag=<% =Role%>&status=<% =StatusFlag%>&internalStatutory=<% =Internalsatutory%>&licenseTypeID=<%=LicenseTypeID%>&branchId=<%=branchID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            if (<% =statVal%> == 1) {
                                return response[0].Statutory;
                            }
                            else if (<% =statVal%> == 2) {
                                   return response[0].Internal;
                               }
                        },
                        total: function (response) {
                            debugger;
                            if (<% =statVal%> == 1) {
                                return response[0].Statutory.length;
                            }
                            else if (<% =statVal%> == 2) {
                                  return response[0].Internal.length;
                              }
                        }
                    },
                    pageSize: 10
                    },
                    excel: {
                        allPages: true
                    },
                    excelExport: function(e) {
                        var columns = e.workbook.sheets[0].columns;
                        e.workbook.fileName = kendo.toString(new Date, "d") + " LicenseGraphReport.xlsx";
                        columns.forEach(function(column){
                            delete column.width;
                            column.autoWidth = true;
                        });
                    },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                {
                    field: "CustomerBrach", title: 'Location',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains",
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "LicensetypeName", title: 'License Type',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                      {
                          hidden:true,
                          field: "DepartmentName", title: 'Department',
                          attributes: {
                              style: 'white-space: nowrap;'

                          }, filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }, width: "10%",
                      },
                {
                    field: "LicenseNo", title: 'License No.',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "Licensetitle", title: 'Title',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "EndDate", title: 'End Date',
                    type: "date",
                    format: "{0:dd-MMM-yyyy}",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                type: "date",
                                format: "{0:dd-MMM-yyyy}",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },
                {
                    field: "Status", title: 'Status',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },
                 {
                     command: [
                         { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                     ], title: "Action", lock: true, width: 100,// width: 150,
                     headerAttributes: {
                         style: "text-align: center;"
                     }
                 }
                ]
            });
              $("#grid").kendoTooltip({
                  filter: "td", //this filter selects the second column's cells
                  position: "down",
                  content: function (e) {
                      var content = e.target.context.textContent;
                      if (content != "") {
                          return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                      }
                       else
                        e.preventDefault();
                  }
              }).data("kendoTooltip");
              $("#grid").kendoTooltip({
                  filter: ".k-grid-edit",
                  content: function (e) {
                      return "View";
                  }
              });
              $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                  debugger;
                  var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                  $('#divOverView').modal('show');
                  $('#OverViews').attr('width', '1150px');
                  $('#OverViews').attr('height', '600px');
                  $('.modal-dialog').css('width', '1200px');
                  if (<% =statVal%> == 1) {
                      $('#OverViews').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + item.LicenseID);
                  }
                  if (<% =statVal%> == 2) 
                      {
                      $('#OverViews').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + item.LicenseID);
                      }
                  return true;

              });
          }
   
          function fCreateStoryBoardGD(Id, div, filtername) {
              var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
              $('#' + div).html('');
              $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
              $('#' + div).css('display', 'block');

              if (div == 'filtersstoryboard') {
                  $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp:&nbsp;');
                  $('#ClearfilterMain').css('display', 'block');
              }
              else if (div == 'filterdept') {
                  $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:');
                  $('#ClearfilterMain').css('display', 'block');
              }
              for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                  var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                  $(button).css('display', 'none');
                  $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                  var buttontest = $($(button).find('span')[0]).text();
                  if (buttontest.length > 10) {
                      buttontest = buttontest.substring(0, 10).concat("...");
                  }
                  $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
              }

              if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                  $('#' + div).css('display', 'none');
                  $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

              }

              CheckFilterClearorNot();
          }

          function CheckFilterClearorNot() {
              if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                   ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                  $('#ClearfilterMain').css('display', 'block');
              }
              else {
                  $('#ClearfilterMain').css('display', 'block');
              }
            
          }

          function fcloseStory11(obj) {
              var DataId = $(obj).attr('data-Id');
              var dataKId = $(obj).attr('data-K-Id');
              var seq = $(obj).attr('data-seq');
              var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
              $(deepspan).trigger('click');
              var upperli = $('#' + dataKId);
              $(upperli).remove();

              fCreateStoryBoardGD('dropdowntree', 'filtersstoryboard', 'loc');

              CheckFilterClearorNot();

          };

          function FilterAll() {
              var location = [];
              if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                  location = $("#dropdowntree").data("kendoDropDownTree")._values;
              }
              var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;
              if (location.length > 0
                  || Departmentlist.length > 0
                  || ($("#dropdownlistComplianceType").data("kendoDropDownList") != undefined && $("#dropdownlistComplianceType").data("kendoDropDownList") != "")
                  || ($("#dropdownDept").val() != "" && $("#dropdownDept").val() != null && $("#dropdownDept").val() != undefined)
                  || ($("#dropdownlistLicenseType").data("kendoDropDownList") != undefined && $("#dropdownlistLicenseType").data("kendoDropDownList") != "")) {

                  var finalSelectedfilter = { logic: "and", filters: [] };
                  if ($("#dropdownlistComplianceType").val() == "S" && $("#dropdownlistComplianceType").val() == "I") {
                      var FYFilter = { logic: "or", filters: [] };

                      FYFilter.filters.push({
                          field: "ISCA", operator: "eq", value: parseInt($("#dropdownlistComplianceType").val())
                      });
                      finalSelectedfilter.filters.push(FYFilter);
                  }

                  if (location.length > 0) {

                      var locationfilter = { logic: "or", filters: [] };

                      $.each(location, function (i, v) {
                          locationfilter.filters.push({
                              field: "CustomerBranchID", operator: "eq", value: v
                          });
                      });

                      finalSelectedfilter.filters.push(locationfilter);
                  }
                  if (Departmentlist.length > 0) {
                      var DeptFilter = { logic: "or", filters: [] };

                      $.each(Departmentlist, function (i, v) {
                          DeptFilter.filters.push({
                            //  field: "departmentID1", operator: "eq", value: parseInt(v)
                              field: "DepartmentID", operator: "eq", value: parseInt(v)
                          });
                      });

                      finalSelectedfilter.filters.push(DeptFilter);
                  }
                  if ($("#dropdownlistLicenseType").val() != "" && $("#dropdownlistLicenseType").val() != null && $("#dropdownlistLicenseType").val() != undefined) {
                      var LicenseFilter = { logic: "or", filters: [] };
                      LicenseFilter.filters.push({
                          field: "LicenseTypeID", operator: "eq", value: parseInt($("#dropdownlistLicenseType").val())
                      });
                      finalSelectedfilter.filters.push(LicenseFilter);
                  }

                  if (finalSelectedfilter.filters.length > 0) {
                      var dataSource = $("#grid").data("kendoGrid").dataSource;
                      dataSource.filter(finalSelectedfilter);
                  }
                  else {
                      $("#grid").data("kendoGrid").dataSource.filter({});
                  }
              }
              else {
                  $("#grid").data("kendoGrid").dataSource.filter({});
              }
          }

          function ClearAllFilterMain(e) {
              $("#dropdowntree").data("kendoDropDownTree").value([]);
              $("#dropdownDept").data("kendoDropDownTree").value([]);

              BranchID = "-1";
              $("#dropdownlistComplianceType").data("kendoDropDownList").select(0);
              $("#dropdownlistLicenseType").data("kendoDropDownList").select(0);
              Bindgrid();
              $("#grid").data("kendoGrid").dataSource.filter({});
              e.preventDefault();
          }

          function exportReportGenerate(e) {

              var grid = $("#grid").data("kendoGrid");
              grid.saveAsExcel();
              e.preventDefault();
              return false;
          }

          function CloseClearPopupView() {
              $('#divOverView').modal('hide');
              Bindgrid();
          }

    </script>
</head>
<body>
    <form id="form1" runat="server" style="margin-left: -14px;">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div id="Details" style="margin-left: 1%;" runat="server">
                <div style="margin: 0% 0% 1%; width: 99%;">
                    <input id="dropdowntree" style="width: 20%; margin-right: 10px;" />
                    <input id="dropdownlistComplianceType" style="width: 18%; margin-right: 10px;visibility:hidden" />
                    <input runat="server" id="dropdownlistLicenseType" style="width: 14.5%; margin-right: 8px;margin-left: -235px;" />
                    <input id="dropdownDept" data-placeholder="Department" style="width: 145px;margin-right: 10px; margin-left: 5px;"/>
                    <button id="exportReport" onclick="exportReportGenerate(event)" style="height: 30px;margin-left:425px" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                    <button id="ClearfilterMain" style="height: 30px; float: right;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
                <div class="row" style="padding-bottom: 4px; margin-left: 11px; font-size: 12px; display: none;font-weight:bold; color: #535b6a;" id="filtersstoryboard">&nbsp;</div>
<%--                <div class="row" style="padding-bottom: 4px; font-size: 12px;color: #535b6a;font-weight:bold;margin-left: 13px;" id="filterdept">&nbsp;</div>--%>
                   <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterdept">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; margin-left: 11px; font-size: 12px; display: none; color: #535b6a;" id="filterrisk">&nbsp;</div>
             <div id="grid"></div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                         <div class="modal-header" style="background-color: #f7f7f7; height: 30px;width:1170px">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
    </form>
</body>
</html>
