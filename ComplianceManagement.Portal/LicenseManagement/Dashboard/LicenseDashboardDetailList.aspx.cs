﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard
{
    public partial class LicenseDashboardDetailList : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                BindLocationFilter();
                BindLicenseType();
                string isstatutoryinternal = "Statutory";
                if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                {
                    isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();
                }
                if (isstatutoryinternal == "Statutory")
                {
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
                else
                {
                    BindGridInternal();
                    bindPageNumber();
                    ShowGridDetail();
                }               
                upDivLocation_Load(sender, e);
            }
        }
        protected void lnkEditLicense_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long licenseInstanceID = Convert.ToInt64(btn.CommandArgument);
                    if (licenseInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowLicenseDialog(" + licenseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "S";
                    if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                    {
                        var val = Request.QueryString["ISI"].ToString().Trim();
                        if (val == "Statutory")
                        {
                            isstatutoryinternal = "S";
                        }
                        else if (val == "Internal")
                        {
                            isstatutoryinternal = "I";
                        }
                    }
                    var LocationList = LicenseMgmt.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);
                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
           
        private void BindLicenseStatusList()
        {
            try
            {
                ddlLicenseStatus.DataSource = null;
                ddlLicenseStatus.DataBind();
                ddlLicenseStatus.ClearSelection();

                ddlLicenseStatus.DataTextField = "StatusName";
                ddlLicenseStatus.DataValueField = "ID";

                var statusList = LicenseMgmt.GetStatusList_All();

                ddlLicenseStatus.DataSource = statusList;
                ddlLicenseStatus.DataBind();

                ddlLicenseStatus.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindLicenseType()
        {
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            string isstatutoryinternal = "S";          
            if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
            {
                var val= Request.QueryString["ISI"].ToString().Trim();
                if (val == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (val == "Internal")
                {
                    isstatutoryinternal = "I";
                }                                 
            }            
            if (AuthenticationHelper.Role.Contains("CADMN") || AuthenticationHelper.Role.Contains("IMPT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);                
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";

            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();

            ddlLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                string isstatutoryinternal = "Statutory";
                if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                {
                    isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();
                }
                if (isstatutoryinternal == "Statutory")
                {
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
                else
                {
                    BindGridInternal();
                    bindPageNumber();
                    ShowGridDetail();
                }
                upDivLocation_Load(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tbxFilterLocation.Text = "All";                                             
                ddlLicenseType.ClearSelection();
                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdDetailData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);                        
                        ExcelData = view.ToTable("Selected", false, "CustomerBrach","LicenseNo","LicenseTitle","LicensetypeName","Status","StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }                            
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Start Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "End Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);
                            
                            
                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                            {
                                //col.Style.Numberformat.Format = "dd/MM/yyyy";
                               
                                
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            using (ExcelRange col = exWorkSheet.Cells[5, 7, 7 + ExcelData.Rows.Count, 8])
                            {                                
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        else
                        {
                            cvGraphDetail.IsValid = false;
                            cvGraphDetail.ErrorMessage = "No data available to export for current selection(s)";
                            cvGraphDetail.CssClass = "alert alert-danger;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGridInternal()
        {
            try
            {
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                statusId.Add(7);
                statusId.Add(9);
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int branchID = -1;
                int deptID = -1;
                long licenseStatusID = -1;
                long licenseTypeID = -1;
                string Status = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    Status = Convert.ToString(Request.QueryString["Status"]);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                if (!string.IsNullOrEmpty(ddlLicenseStatus.SelectedValue))
                {
                    licenseStatusID = Convert.ToInt64(ddlLicenseStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var lstAssignedLicenses = (entities.SP_InternalLicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, AuthenticationHelper.Role)).ToList();
                    if (branchList.Count > 0)
                        lstAssignedLicenses = lstAssignedLicenses.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (licenseTypeID != 0 && licenseTypeID != -1)
                    {
                        lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                    }

                    if (lstAssignedLicenses.Count > 0)
                    {
                        lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.EndDate).ToList();
                    }
                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.RoleID == 3).ToList();

                    if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN")
                    {
                        //List<int> cstatusId = new List<int>();
                        //cstatusId.Add(1);
                        //cstatusId.Add(2);
                        //cstatusId.Add(3);
                        //cstatusId.Add(6);
                        //cstatusId.Add(8);
                        //cstatusId.Add(10);

                        if (Request.QueryString["status"] == "ApplicationOverduebutnotapplied" || Request.QueryString["status"] == "Expiring")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                            (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();
                        }
                        if (Request.QueryString["status"] == "Expiredappliedbutnotrenewed")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Applied"
                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                        }
                        if (Request.QueryString["status"] == "Expired")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired").ToList();
                        }
                        if (Request.QueryString["status"] == "Active")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active").ToList();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["status"]))
                        {
                            if (Request.QueryString["status"] == "Expiring")
                            {
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expiring" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Applied")
                            {
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Expired")
                            {
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expired" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Active")
                            {
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                        }
                    }
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = lstAssignedLicenses.Count;
                    Session["grdDetailData"] = (grdLicenseList.DataSource as List<SP_InternalLicenseInstanceTransactionCount_Result>).ToDataTable();
                    if (lstAssignedLicenses.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;
                    lstAssignedLicenses.Clear();
                    lstAssignedLicenses = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
                cvGraphDetail.CssClass = "alert alert-danger";
            }
        }
        public void BindGrid()
        {
            try
            {
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                statusId.Add(7);
                statusId.Add(9);

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int branchID = -1;
                int deptID = -1;                           
                long licenseStatusID = -1;
                long licenseTypeID = -1;
                string Status = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    Status = Convert.ToString(Request.QueryString["Status"]);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);                             
                if (!string.IsNullOrEmpty(ddlLicenseStatus.SelectedValue))
                {
                    licenseStatusID = Convert.ToInt64(ddlLicenseStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                }               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var lstAssignedLicenses = (entities.SP_LicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, AuthenticationHelper.Role)).ToList();
                    if (branchList.Count > 0)
                        lstAssignedLicenses = lstAssignedLicenses.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (licenseTypeID != 0 && licenseTypeID != -1)
                    {
                        lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                    }

                    if (lstAssignedLicenses.Count > 0)
                    {
                        lstAssignedLicenses = lstAssignedLicenses.OrderBy(entry => entry.EndDate).ToList();
                    }
                    lstAssignedLicenses = lstAssignedLicenses.Where(entry => entry.RoleID == 3).ToList();

                    if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN")
                    {
                        //List<int> cstatusId = new List<int>();
                        //cstatusId.Add(1);
                        //cstatusId.Add(2);
                        //cstatusId.Add(3);
                        //cstatusId.Add(6);
                        //cstatusId.Add(8);
                        //cstatusId.Add(10);

                        if (Request.QueryString["status"] == "ApplicationOverduebutnotapplied" || Request.QueryString["status"]== "Expiring")
                        {
                            //lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                            //(row.Status == "Expiring" || row.Status == "Active")
                            //&& row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                            lstAssignedLicenses = lstAssignedLicenses.Where(row =>
                                (row.MGRStatus == "Expiring" || row.MGRStatus == "PendingForReview")).ToList();
                        }
                        if (Request.QueryString["status"] == "Expiredappliedbutnotrenewed")
                        {
                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Applied"
                            && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                        }
                        if (Request.QueryString["status"] == "Expired")
                        {
                            //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired"
                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Expired").ToList();
                        }
                        if (Request.QueryString["status"] == "Active")
                        {
                            //lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active"
                            //&& cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                            lstAssignedLicenses = lstAssignedLicenses.Where(row => row.MGRStatus == "Active").ToList();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["status"]))
                        {
                            if (Request.QueryString["status"] == "Expiring")
                            {                                
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expiring" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Applied")
                            {                                
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Expired")
                            {                                
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Expired" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            if (Request.QueryString["status"] == "Active")
                            {                                
                                lstAssignedLicenses = lstAssignedLicenses.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                        }
                    }             
                    grdLicenseList.DataSource = lstAssignedLicenses;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = lstAssignedLicenses.Count;
                    Session["grdDetailData"] = (grdLicenseList.DataSource as List<SP_LicenseInstanceTransactionCount_Result>).ToDataTable();
                    if (lstAssignedLicenses.Count > 0)
                        btnExportExcel.Enabled = true;
                    else
                        btnExportExcel.Enabled = false;
                    lstAssignedLicenses.Clear();
                    lstAssignedLicenses = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
                cvGraphDetail.CssClass = "alert alert-danger";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                var PageSize = Convert.ToInt32(grdLicenseList.PageSize);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;
                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                string isstatutoryinternal = "Statutory";
                if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                {
                    isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();
                }
                if (isstatutoryinternal == "Statutory")
                {
                    BindGrid();
                    bindPageNumber();                    
                }
                else
                {
                    BindGridInternal();
                    bindPageNumber();                    
                }                              
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLicenseList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLicenseList.PageIndex = chkSelectedPage - 1;
            grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);          
            string isstatutoryinternal = "Statutory";
            if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
            {
                isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();
            }
            if (isstatutoryinternal == "Statutory")
            {
                BindGrid();                
                ShowGridDetail();
            }
            else
            {
                BindGridInternal();                
                ShowGridDetail();
            }

        }
    }
}