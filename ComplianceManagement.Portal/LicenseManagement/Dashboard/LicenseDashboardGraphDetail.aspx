﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseDashboardGraphDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard.LicenseDashboardGraphDetail" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/license_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>


    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });
        function ShowLicenseDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '100%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + LicenseInstanceID);
        };
        function ShowInternalLicenseDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '100%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + LicenseInstanceID);
        };
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>
</head>

<body class="bgColor-white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smDashboardGraphDetail" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="col-md-12">
                <asp:ValidationSummary ID="vsGraphDetailPage" runat="server" class="alert alert-block alert-danger fade in"
                    ValidationGroup="GraphDetailPageValidationGroup" />
                <asp:CustomValidator ID="cvGraphDetail" runat="server" EnableClientScript="False"
                    ValidationGroup="GraphDetailPageValidationGroup" Display="None" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-2 colpadding0 entrycount" style="float: left; display:none;">
                        <label for="ddlStatus" class="filter-label">Status</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                            DataPlaceHolder="Select Status" class="form-control" Width="95%">
                        </asp:DropDownListChosen>
                    </div>
                    <div class="col-md-2 colpadding0 entrycount" style="float: left; margin-left: 5px;">
                        <label for="ddlLicenseType" class="filter-label">License Type</label>
                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                            DataPlaceHolder="Select Type" class="form-control" Width="100%">
                        </asp:DropDownListChosen>
                    </div>
                    <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: 5px;">
                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                            <ContentTemplate>
                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 320px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                    CssClass="txtbox" AutoComplete="off" />
                                <div style="margin-left: 1px; margin-top: -19px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="320px" NodeStyle-ForeColor="#8e8e93"
                                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>


                    <div class="col-md-1 colpadding0" style="float: left;  width: 5.333333%; ">
                        <label for="lblFilter" class="filter-label" style="margin-top: 39px; "></label>
                        <asp:Button ID="btnFilter" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" />
                    </div>
                    <div class="col-md-1 colpadding0" style="float: left; width: 5.333333%; display:none;">
                        <label for="lblClearFilter" class="filter-label" style="margin-top: 39px;"></label>
                        <asp:Button ID="btnClearFilter" class="btn btn-primary" runat="server" Text="Clear"
                            OnClick="btnClearFilter_Click" />
                    </div>
                    <div class="col-md-1 colpadding0" style="float: left; width: 5.333333%;">
                        <label for="lblExportExcel" class="filter-label" style="margin-top: 39px;"></label>
                        <asp:LinkButton ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click"
                            data-toggle="tooltip" data-placement="left" ToolTip="Export to Excel">
                         <img src="/Images/Excel _icon.png" alt="Export" /> 
                        </asp:LinkButton>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: left;">
                    </div>
                </div>
            </div>



            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 colpadding0">
                    <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="LicenseID">

                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location" ItemStyle-Width="15%" SortExpression="CustomerBrach">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="License Type" ItemStyle-Width="20%" SortExpression="LicensetypeName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="License No." ItemStyle-Width="20%" SortExpression="LicenseNo">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Title" ItemStyle-Width="15%" SortExpression="LicenseTitle">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                            ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBtnViewLicense" runat="server" OnClick="lnkEditLicense_Click" CommandArgument='<%# Eval("LicenseID") %>'
                                        data-toggle="tooltip" data-placement="left" ToolTip="View License Detail(s)">
                                <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="height: 32px !important; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
        </div>
        <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                        <%--onclick="javascript:reloadTaskList();"--%>
                        <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7;">
                        <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
