﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard
{
    public partial class LicenseDashboard : System.Web.UI.Page
    {
        protected string user_Roles;
        protected static string queryStringFlag = "";
        protected static string IsSatutoryInternal;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternal = "Internal";
                }
                user_Roles = AuthenticationHelper.Role;

                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";

                //if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN")
                //{
                    
                    BindLocationFilter();
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        BindDashboard();
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        BindDashboardInternal();
                    }
                    BindLicenseType();
                }
            //}
        }
        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "S";
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }
                    var LocationList = LicenseMgmt.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);
                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    BindDashboard();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    BindDashboardInternal();
                }
                //BindLicenseType();
                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExportExcelExpired_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdLicenseExpiredData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseExpiredReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdLicenseExpiredData"]);
                        ExcelData = view.ToTable("Selected", false, "CustomerBrach", "LicenseNo", "LicenseTitle", "LicensetypeName", "MGRStatus", "StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Start Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "End Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);


                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            using (ExcelRange col = exWorkSheet.Cells[5, 7, 7 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnExportExcelExpiring_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdLicenseExpiringData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseExpiringReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdLicenseExpiringData"]);
                        ExcelData = view.ToTable("Selected", false, "CustomerBrach", "LicenseNo", "LicenseTitle", "LicensetypeName", "MGRStatus", "StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Start Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "End Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);


                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            using (ExcelRange col = exWorkSheet.Cells[5, 7, 7 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseType()
        {
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            string isstatutoryinternal = "S";
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            user_Roles = AuthenticationHelper.Role;
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT") || user_Roles.Contains("MGMT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";

            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();

            ddlLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlLicenseType.ClearSelection();
                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkEditLicense_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long licenseInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (licenseInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowLicenseDialog(" + licenseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicenseDashboard.IsValid = false;
                cvLicenseDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlLicenseExpiry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                BindDashboard();
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                BindDashboardInternal();
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {                    
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {                    
                    IsSatutoryInternal = "Internal";
                }
                BindLicenseType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDashboard()
        {
            try
            {
                BindLicenseStatusCounts();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDashboardInternal()
        {
            try
            {
                BindInternalLicenseStatusCounts();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindInternalLicenseStatusCounts()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                    
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_InternalLicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID,AuthenticationHelper.Role)).ToList();
                    List<int> statusId = new List<int>();
                    statusId.Add(4);
                    statusId.Add(5);
                    statusId.Add(7);
                    statusId.Add(9);
                    MastertransactionsQuery = MastertransactionsQuery.GroupBy(a => a.LicenseID).Select(a => a.FirstOrDefault()).ToList();
                    if (MastertransactionsQuery != null && MastertransactionsQuery.Count > 0)
                    {
                        divExpiringcount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Expiring" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divActiveCount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divExpiredCount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Expired" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divAppliedcount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        //divPendingForReview.InnerText = MastertransactionsQuery.Where(row => row.StatusID == 6 || row.StatusID == 7).Count().ToString();
                        divPendingForReview.InnerText = MastertransactionsQuery.Where(row => (row.StatusID == 6 || row.StatusID == 7) && (!statusId.Contains((int)row.ComplianceStatusID))).Count().ToString();
                        divRejected.InnerText = MastertransactionsQuery.Where(row => row.Status == "Rejected").Count().ToString();

                        var transactionquery = MastertransactionsQuery.Where(row => row.Status == "Expiring").ToList();

                        if (transactionquery.Count>0)                        
                            btnExportExcelExpiring.Visible = true;                        
                         else
                            btnExportExcelExpiring.Visible = false;

                        if (transactionquery.Count > 2)                        
                            lnkShowDetailLicense.Visible = true;                                                  
                        else                        
                            lnkShowDetailLicense.Visible = false;                                                    

                        grdLicenseExpiring.DataSource = transactionquery;
                        grdLicenseExpiring.DataBind();

                        Session["grdLicenseExpiringData"] = null;
                        Session["grdLicenseExpiringData"] = (grdLicenseExpiring.DataSource as List<SP_InternalLicenseInstanceTransactionCount_Result>).ToDataTable();

                        var transactionqueryExpired = MastertransactionsQuery.Where(row => row.Status == "Expired").ToList();

                        if (transactionqueryExpired.Count>0)
                            btnExportExcelExpired.Visible = true;
                        else
                            btnExportExcelExpired.Visible = false;

                        if (transactionqueryExpired.Count >2)                        
                            lnkLicExpiredshowMore.Visible = true;                          
                        else                        
                            lnkLicExpiredshowMore.Visible = false;
                                                    

                        grdLicenseExpired.DataSource = transactionqueryExpired;
                        grdLicenseExpired.DataBind();

                        Session["grdLicenseExpiredData"] = null;
                        Session["grdLicenseExpiredData"] = (grdLicenseExpired.DataSource as List<SP_InternalLicenseInstanceTransactionCount_Result>).ToDataTable();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseStatusCounts()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_LicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, AuthenticationHelper.Role)).ToList();
                    List<int> statusId = new List<int>();
                    statusId.Add(4);
                    statusId.Add(5);
                    statusId.Add(7);
                    statusId.Add(9);
                    MastertransactionsQuery = MastertransactionsQuery.GroupBy(a => a.LicenseID).Select(a => a.FirstOrDefault()).ToList();
                    if (MastertransactionsQuery != null && MastertransactionsQuery.Count > 0)
                    {
                        divExpiringcount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Expiring" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divActiveCount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Active" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divExpiredCount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Expired" && !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        divAppliedcount.InnerText = MastertransactionsQuery.Where(row => row.Status == "Applied" && statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                        //divPendingForReview.InnerText = MastertransactionsQuery.Where(row => row.StatusID == 6 || row.StatusID == 7).Count().ToString();
                        divPendingForReview.InnerText = MastertransactionsQuery.Where(row => (row.StatusID == 6 || row.StatusID == 7) && (!statusId.Contains((int)row.ComplianceStatusID))).Count().ToString();
                        divRejected.InnerText = MastertransactionsQuery.Where(row => row.Status == "Rejected").Count().ToString();

                        var transactionquery = MastertransactionsQuery.Where(row => row.Status == "Expiring").ToList();

                        if (transactionquery.Count > 0)
                            btnExportExcelExpiring.Visible = true;
                        else
                            btnExportExcelExpiring.Visible = false;

                        if (transactionquery.Count > 2)
                            lnkShowDetailLicense.Visible = true;
                        else
                            lnkShowDetailLicense.Visible = false;

                        grdLicenseExpiring.DataSource = transactionquery;
                        grdLicenseExpiring.DataBind();

                        Session["grdLicenseExpiringData"] = null;
                        Session["grdLicenseExpiringData"] = (grdLicenseExpiring.DataSource as List<SP_LicenseInstanceTransactionCount_Result>).ToDataTable();

                        var transactionqueryExpired = MastertransactionsQuery.Where(row => row.Status == "Expired").ToList();

                        if (transactionqueryExpired.Count > 0)
                            btnExportExcelExpired.Visible = true;
                        else
                            btnExportExcelExpired.Visible = false;

                        if (transactionqueryExpired.Count > 2)
                            lnkLicExpiredshowMore.Visible = true;
                        else
                            lnkLicExpiredshowMore.Visible = false;


                        grdLicenseExpired.DataSource = transactionqueryExpired;
                        grdLicenseExpired.DataBind();

                        Session["grdLicenseExpiredData"] = null;
                        Session["grdLicenseExpiredData"] = (grdLicenseExpired.DataSource as List<SP_LicenseInstanceTransactionCount_Result>).ToDataTable();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
    }
}