﻿<%@ Page Title="My Dashboard" Language="C#" MasterPageFile="~/LicenseManagement.Master" AutoEventWireup="true" CodeBehind="LicenseDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard.LicenseDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
         $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
        function ShowLicenseDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 150;

            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + LicenseInstanceID);
        };
        function ShowDashboardDetailList(Status, ISI) {
            $('#divLicenseDetails').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showLicenseDetails').attr('src', 'about: blank');
           // $('#showLicenseDetails').attr('width', '95%');
            // $('#showLicenseDetails').attr('height', '80%');
            $('#showLicenseDetails').attr('width', '1250px');
            $('#showLicenseDetails').attr('height', '620px');
            $('#showLicenseDetails').attr('src', "/LicenseManagement/Dashboard/LicenseExpiringOnKendo.aspx?Status=" + Status + "&ISI=" + ISI);
        }
    </script>
    <style type="text/css">
        #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapseDivFilters > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        @media screen and (max-width: 750px) {
            iframe {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
        }

        .mang-dashboard-white-widget {
            background: #fff;
            padding: 5px 0px 5px 0px;
            margin-bottom: 10px;
            border-radius: 10px;
        }

        .panel {
            margin-bottom: 10px;
        }

            .panel .panel-heading {
                border: none;
                background: #fff;
                padding: 0;
            }

                .panel .panel-heading h2 {
                    font-size: 20px;
                }


            .panel .panel-heading-dashboard {
                border: none;
                background: #fff;
                padding: 0;
            }

        .info-box {
            min-height: 102px;
            margin-bottom: 10px;
        }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .info-box .countMD {
                font-size: 20px;
                color: #fff;
                text-align: center;
            }

            .info-box .titleMD {
                font-size: 15px;
                color: #fff;
                text-align: center;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="dashboard">
        <div class="col-md-12 colpadding0">
            <asp:ValidationSummary ID="vsLicenseDashboard" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                ValidationGroup="LicenseDashboardValidationGroup" />
            <asp:CustomValidator ID="cvLicenseDashboard" runat="server" EnableClientScript="False"
                ValidationGroup="LicenseDashboardValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
        </div>

        <!-- Top Count start -->

        <div id="divTabs" class="row five-cols">
            <div id="activeCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box amber-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=Active&ISI=<%=IsSatutoryInternal%>">--%>
                            <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=Active&ISI=<%=IsSatutoryInternal%>">   
                         <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Active.png" height="55" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Active</div>
                                <div id="divActiveCount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="expringcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box rednew-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=Expiring&ISI=<%=IsSatutoryInternal%>">--%>
                              <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=Expiring&ISI=<%=IsSatutoryInternal%>">
                            <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Expired.png" height="55" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Expiring</div>
                                <div id="divExpiringcount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="expiredCount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box bluenew-bg w100per">
                    <div class="div-location" style="cursor: pointer">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=Expired&ISI=<%=IsSatutoryInternal%>">--%>
                            <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=Expired&ISI=<%=IsSatutoryInternal%>">
                            <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Expired.png" height="60" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Expired</div>
                                <div id="divExpiredCount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="Appliedcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box greennew-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=Applied&ISI=<%=IsSatutoryInternal%>">--%>
                              <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=Applied&ISI=<%=IsSatutoryInternal%>">
                            <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Reviewed.png" height="55" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Applied</div>
                                <div id="divAppliedcount" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="PendingForReviewcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box seablue-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=PR&ISI=<%=IsSatutoryInternal%>">--%>
                             <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=PR&ISI=<%=IsSatutoryInternal%>">
                            <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Pending-Review.png" height="55" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Pending For Review</div>
                                <div id="divPendingForReview" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="Rejectedcount" runat="server" class="col-lg-1 col-md-1 col-sm-1 TMB plr5">
                <div class="info-box seablue-bg w100per">
                    <div class="div-location" style="cursor: pointer;">
<%--                        <a href="/LicenseManagement/aspxPages/LicenseList.aspx?Status=Rejected&ISI=<%=IsSatutoryInternal%>">--%>
                             <a href="/LicenseManagement/aspxPages/LicencListKendo.aspx?Status=Rejected&ISI=<%=IsSatutoryInternal%>">
                            <div class="col-md-4 colpadding0 mt10">
                                <img src="/img/Draft.png" height="55" />
                            </div>
                            <div class="col-md-8 colpadding0">
                                <div class="titleMD">Rejected</div>
                                <div id="divRejected" runat="server" class="countMD">0</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Count End -->

        <!-- Filters-->
        <div class="row">
            <div id="DivFilters" class="row mang-dashboard-white-widget">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="margin-left: 5px;">
                            <h2>Filters</h2>
                            <div class="panel-actions">
                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#<%=collapseDivFilters.ClientID%>">
                                    <i class="fa fa-chevron-up"></i></a>
                            </div>
                        </div>
                        <div id="collapseDivFilters" class="panel-collapse collapse in" runat="server">
                            <div class="panel-body">
                                <div class="col-md-12 colpadding0 form-group">
                                    <div class="col-md-2 colpadding0">
                                        <label for="tbxFilterLocation" class="filter-label">Type</label>
                                        <asp:DropDownList runat="server" ID="ddlComplianceType" class="form-control" Style="width: 170px;"
                                            OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="Statutory" Value="S" />
                                            <asp:ListItem Text="Internal" Value="I" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-4 colpadding0" style="width: 25%;">
                                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                            <ContentTemplate>
                                                <label for="tbxFilterLocation" class="filter-label">Entity/Branch/Location</label>
                                                <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="95%" />
                                                <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 263px; margin-top: -15px;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                    </asp:TreeView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-4 colpadding0" style="width: 25%;">
                                        <label for="ddlLicenseType" class="filter-label">License Type</label>
                                        <asp:DropDownListChosen runat="server" ID="ddlLicenseType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            DataPlaceHolder="Select Type" class="form-control" Width="100%">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-4 colpadding0 text-right" style="width: 20%;">
                                        <div class="col-md-6 colpadding0 float-left">
                                            <label for="btnFilter" class="hidden-label">Apply</label>
                                            <asp:Button ID="btnFilter" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" Width="95%" />
                                        </div>
                                        <div class="col-md-6 colpadding0 float-right">
                                            <label for="btnClearFilter" class="hidden-label">Clear</label>
                                            <asp:Button ID="btnClearFilter" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClearFilter_Click" Width="95%" />
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2 colpadding0" style="width: 19%"></div>
                                    <div class="col-md-2 colpadding0" style="width: 19%"></div>
                                    <div class="col-md-2 colpadding0 w20per"></div>
                                    <div class="col-md-2 colpadding0 w20per"></div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Filters End-->


        <!-- License Expired-->
        <div class="row">
            <div id="divOuterLicenseExpired" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="float-left">
                                            <h2>License Expired On</h2>
                                        </div>
                                        <div class="float-left ml10 mt5">
                                            <asp:DropDownListChosen runat="server" ID="ddlLicenseExpired" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                DataPlaceHolder="Select" class="form-control">
                                                <asp:ListItem Text="Next 30 Days" Value="30" Selected="True"></asp:ListItem>
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseLicenseExpired">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="collapseLicenseExpired" class="collapse">
                                        <%-- <div class="panel-body" style="max-height: 300px; overflow: auto;">--%>
                                        <div id="divLicenseExpired" class="col-md-12 plr0">
                                            <asp:GridView runat="server" ID="grdLicenseExpired" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="License Type" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="License No." HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                    ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkBtnViewLicense" runat="server" OnClick="lnkEditLicense_Click" CommandArgument='<%# Eval("LicenseID") %>'
                                                                data-toggle="tooltip" data-placement="left" title="View License Detail(s)">
                                                                <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                                <PagerSettings Visible="false" />
                                            </asp:GridView>
                                        </div>
                                        <%--</div>--%>
                                        <div class="col-md-12 text-right">
                                            <asp:LinkButton ID="btnExportExcelExpired" runat="server" OnClick="btnExportExcelExpired_Click"
                                                data-toggle="tooltip" data-placement="left" ToolTip="Export to Excel">
                                            <img src="/Images/Excel _icon.png" alt="Export" /> 
                                            </asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkLicExpiredshowMore" Text="..Show More" Visible="false" OnClientClick="ShowDashboardDetailList('Expired')"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnExportExcelExpired" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-- License Expired END-->


        <!-- License Expiring-->
        <div class="row">
            <div id="divOuterLicenseExpiring" class="row mang-dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="float-left">
                                            <h2>License Expiring On</h2>
                                        </div>
                                        <div class="float-left ml10 mt5">
                                            <asp:DropDownListChosen runat="server" ID="ddlLicenseExpiry" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                                DataPlaceHolder="Select" class="form-control">
                                                <asp:ListItem Text="< 30 Days" Value="30" Selected="True"></asp:ListItem>
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseLicenseExpiring">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="collapseLicenseExpiring" class="collapse">
                                        <div id="divLicenseExpiring" class="col-md-12 plr0">
                                            <asp:GridView runat="server" ID="grdLicenseExpiring" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%" AllowPaging="true" ShowFooter="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="License Type" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="License No." HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Title" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Expiry Date" HeaderStyle-Width="20%" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                                <asp:Label runat="server" data-toggle="tooltip" Width="100%" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                    ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkBtnViewLicense" runat="server" OnClick="lnkEditLicense_Click" CommandArgument='<%# Eval("LicenseID") %>'
                                                                data-toggle="tooltip" data-placement="left" ToolTip="View License Detail(s)">
                                                                <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                                <PagerSettings Visible="false" />
                                            </asp:GridView>
                                        </div>

                                        <div class="col-md-12 text-right">
                                            <asp:LinkButton ID="btnExportExcelExpiring" runat="server" OnClick="btnExportExcelExpiring_Click"
                                                data-toggle="tooltip" data-placement="left" ToolTip="Export to Excel">
                                            <img src="/Images/Excel _icon.png" alt="Export" /> 
                                            </asp:LinkButton>

                                            <asp:LinkButton runat="server" ID="lnkShowDetailLicense" Text="..Show More" Visible="false" OnClientClick="ShowDashboardDetailList('Expiring')"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExportExcelExpiring" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <!-- License Expiring END-->
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divLicenseDetails" role="dialog" aria-labelledby="myModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%; height: 635px">
                    <iframe id="showLicenseDetails" src="about:blank" style="min-height: 550px !important" scrolling="auto" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
