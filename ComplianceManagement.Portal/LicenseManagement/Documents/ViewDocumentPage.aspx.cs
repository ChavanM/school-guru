﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Documents
{
    public partial class ViewDocumentPage : System.Web.UI.Page
    {
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)

        {
            if (!IsPostBack)
            {

                try
                {
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    int transactionid = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                    string IsStatutory = (Request.QueryString["ISStatutoryflag"]);
                    if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceTransactionID"]))
                    {
                        int ComplianceScheduledOnID = ScheduledOnID;
                        int ComplainceTransactionID = transactionid;
                        string IsSStatutory = IsStatutory;
                        BindTransactionDetails(ScheduledOnID, ComplainceTransactionID, IsStatutory);


                    }
                }

                catch(Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        
        protected void rptComplianceVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // LinkButton btnComplinceVersionDocView = (LinkButton)e.Item.FindControl("btnComplinceVersionDocView");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(btnComplinceVersionDocView);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        public static List<GetLicenseInternalComplianceDocumentsView> GetFileDataInternalView(int ScheduledOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                string IsStatutory = (Request.QueryString["ISStatutoryflag"]);

                if (IsStatutory == "S")
                {
                    #region Statutory
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }
                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                else if (IsStatutory == "I")
                {
                    #region Internal
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                        if (CMPDocuments != null)
                        {
                            List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                        if (CMPDocuments != null)
                        {
                            List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                            string FileName = DateFolder + "/" + User + "" + extension;
                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion

                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorLicenseListPage.IsValid = false;
                cvErrorLicenseListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTransactionDetails(long ScheduledOnID, long TransactionID, string IsStatutory)
        {
            try
            {
                if (IsStatutory == "S")
                {
                    #region Statutory
                    //string[] commandArg = e.CommandArgument.ToString().Split(',');

                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(ScheduledOnID, 0);

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = ScheduledOnID;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptComplianceVersionView.DataBind();

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region  Normal Storage
                        List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(ScheduledOnID, 0);

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = ScheduledOnID;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptComplianceVersionView.DataBind();

                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerIDs = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerIDs + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                }
                else if (IsStatutory == "I")
                {
                    #region Internal

                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetDocumnetsInternal(ScheduledOnID);

                        if (CMPDocuments != null)
                        {
                            List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = ScheduledOnID;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptComplianceVersionView.DataBind();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicense();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        List<GetLicenseInternalComplianceDocumentsView> CMPDocuments = GetDocumnetsInternal(ScheduledOnID);

                        if (CMPDocuments != null)
                        {
                            List<GetLicenseInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetLicenseInternalComplianceDocumentsView entityData = new GetLicenseInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = ScheduledOnID;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptComplianceVersionView.DataBind();

                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerIDs = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerIDs + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLicense('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<GetLicenseInternalComplianceDocumentsView> GetDocumnetsInternal(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetLicenseInternalComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }

    }
}