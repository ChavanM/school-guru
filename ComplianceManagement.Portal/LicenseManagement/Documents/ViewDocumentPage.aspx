﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDocumentPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Documents.ViewDocumentPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>



    <script type="text/javascript">
        function fopendocfileLicense(file) {
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').attr('src', "../../docviewer.aspx?docurl=" + file);
        }


        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };

        function fopendocfileReview(file) {
            debugger;
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function ViewInternalDocument() {
            $('#divViewInternalDocument').modal('show');
            return true;
        };

        function fopendocfileReviewInternal(file) {
            $('#divViewInternalDocument').modal('show');
            $('#docViewerInternal').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in" Style="padding-left: 5%"
                    ValidationGroup="LicenseListPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div id="divViewDocument" class="modal fade" style="width: 100%; background: #fff;">
                <div style="float: left; width: 10%">
                    <table width="100%" style="text-align: left; margin-left: 25%;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th style="font-weight: bold;">File Name</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div style="float: left; width: 90%">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                        <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                    </fieldset>
                </div>
            </div>


        </div>
    </form>
</body>
</html>
