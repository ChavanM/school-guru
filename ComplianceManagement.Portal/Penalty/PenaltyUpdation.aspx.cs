﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Penalty
{
    public partial class PenaltyUpdation : System.Web.UI.Page
    {
        protected bool IsRemarkCompulsory = false;
        public string ispenaltytext;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                    {
                        string controlID = Page.Request.Params["__EVENTTARGET"];
                        Control postbackControl = Page.FindControl(controlID);
                    }
                    BindTypes();
                    BindCategories();
                    BindActList();
                    //BindCompliances();

                    BindStatus();
                    BindLocationFilter();

                    FillComplianceDocuments();
                    ispenaltytext = Convert.ToString(ConfigurationManager.AppSettings["IsPenaltyTextchanged"]);
                    if (ispenaltytext == Convert.ToString(AuthenticationHelper.CustomerID))
                    {
                        lblPenalty.InnerText = "Penalty/Additional Fee";
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";
                isstatutoryinternal = "S";
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        protected void linkbutton_onclick(object sender,EventArgs e)
        {
            try
            { 
                 Response.Redirect("~/Controls/frmUpcomingCompliancessNew.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindStatus()
        {
            try
            {
                foreach (PenaltyPerformer r in Enum.GetValues(typeof(PenaltyPerformer)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(PenaltyPerformer), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = ComplianceTypeManagement.GetAll();
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind all acts.
        /// </summary>

        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);

                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = 0;

                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
          
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = 0;

                FillComplianceDocuments();
                //saveopo.Value = "False";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
          
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
              
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;


                //Reload the Grid
                FillComplianceDocuments();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void FillComplianceDocuments()
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);

                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);

                //PenaltyPerformer Status = (PenaltyPerformer) Convert.ToInt16(ddlStatus.SelectedIndex);
                String Status = ddlStatus.SelectedItem.Text;

                //String location = tvFilterLocation.SelectedNode.Text;
                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);

                int type = Convert.ToInt32(ddlType.SelectedValue);

                int category = Convert.ToInt32(ddlCategory.SelectedValue);

                int ActID = Convert.ToInt32(ddlAct.SelectedValue);

                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                if (txtAdvStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtAdvStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtAdvEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtAdvEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                string StringType = "";
                StringType = txtSearchType.Text;
                var ReviseComplianceDocs = ComplianceManagement.Business.ComplianceManagement.BindPenaltyForPerformer(customerid, AuthenticationHelper.UserID,risk, Status, location, type, category, ActID, dtfrom, dtTo, StringType);

                grdRviseCompliances.DataSource = ReviseComplianceDocs;
                Session["TotalRows"] = ReviseComplianceDocs.Count;
                grdRviseCompliances.DataBind();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdRviseCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Penalty")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ViewState["ComplianceTransactionID"] = commandArg[2];
                    
                    var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(Convert.ToInt32(commandArg[0]));
                    var complianceTransaction = Business.ComplianceManagement.GetComplianceTransaction(Convert.ToInt32(commandArg[2]));

                    lblShorDescription.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription.Text = complianceInfo.Description;

                    txtPenalty.Text =Convert.ToString(complianceTransaction.Penalty);
                    txtInterest.Text = Convert.ToString(complianceTransaction.Interest);
                    txtremark.Text = "";
                    if(txtPenalty.Text == "0.00")
                    {
                        txtPenalty.Text = "";
                        //IsRemarkCompulsory = true;
                    }
                    else
                    {
                       // IsRemarkCompulsory = false;
                    }

                    if (txtInterest.Text == "0.00")
                    {
                        txtInterest.Text = "";
                    }
                    // BindTransactions(Convert.ToInt32(commandArg[0]));

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate();", true);

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divReviseCompliance", "$(\"#divReviseCompliance\").dialog('open');fchgr();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetStatus(long ComplianceScheduledID)
        {
            try
            {
                string result = "";
                result = GetLatestStatus(ComplianceScheduledID);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        public  string GetLatestStatus(long ComplianceScheduledID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.ScheduledOnID == ComplianceScheduledID
                                         select row.Status).FirstOrDefault();
                return transactionsQuery;
            }
        }
        protected void grdRviseCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    LinkButton btnChangeStatus = (LinkButton) e.Row.FindControl("lnkReviseCompliances");
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                    e.Row.ToolTip = "Click on row to Penalty Updation";

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatus");
                    
                    lblStatus.ToolTip = "";

                    //if (lblScheduledOn != null && lblStatus != null)
                    //{
                    //    String GridStatus = lblStatus.Text;
                    //    if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Open-Upcoming-Awaiting Submission";
                    //    else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Open-Overdue-Awaiting Submission";
                        
                    //    else if (GridStatus == "Complied but pending review")
                    //        lblStatus.Text = "Open-Upcoming-Awaiting Review";
                       
                    //    else if (GridStatus == "Complied Delayed but pending review")
                    //        lblStatus.Text = "Open-Overdue-Awaiting Review";

                    //    else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Open-Upcoming-Awaiting Submission";
                    //    else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Open-Overdue-Awaiting Submission";

                    //    else if (GridStatus == "Not Complied" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Open-Upcoming-Awaiting Re-Submission";
                    //    else if (GridStatus == "Not Complied" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Open-Overdue-Awaiting Re-Submission";

                    //    else if (GridStatus == "Rejected" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Open-Upcoming-Awaiting Re-Submission";
                    //    else if (GridStatus == "Rejected" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Open-Overdue-Awaiting Re-Submission";

                    //    else if (GridStatus == "Submitted For Interim Review")
                    //        lblStatus.Text = "Open-Awaiting Interim Review";

                    //    else if (GridStatus == "Interim Review Approved")
                    //        lblStatus.Text = "Completed-Awaiting Interim Review";

                    //    else if (GridStatus == "Interim Rejected")
                    //        lblStatus.Text = "Open-Awaiting Interim Resubmission";

                    //    lblStatus.ToolTip = lblStatus.Text;
                    //}

                    //if (lblScheduledOn != null && lblStatus != null)
                    //{
                    //    String GridStatus = lblStatus.Text;
                    //    if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Upcoming";
                    //    else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Overdue";
                    //    else if (GridStatus == "Complied but pending review")
                    //        lblStatus.Text = "Pending For Review";
                    //    else if (GridStatus == "Complied Delayed but pending review")
                    //        lblStatus.Text = "Pending For Review";
                    //    else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                    //        lblStatus.Text = "Upcoming";
                    //    else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                    //        lblStatus.Text = "Overdue";
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        protected void btnSetFilter_Click(object sender, EventArgs e)
        {
            try
            {
                //ddlComplinceCatagory.SelectedIndex = -1;
                //ddlFilterComplianceType.SelectedIndex = -1;
                ViewState["checkedCompliancesInstances"] = null;
                //BindCompliances();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReviseFilter\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        private void CmpPopulateCheckedValues()
        {
            try
            {
                ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

                if (complianceDetails != null && complianceDetails.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdRviseCompliances.Rows)
                    {
                        LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                        int index = Convert.ToInt32(lblDownLoadfile.CommandArgument); ;
                        if (complianceDetails.Contains(index))
                        {
                            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                            myCheckBox.Checked = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }

        private void CmpSaveCheckedValues()
        {
            try
            {
                ArrayList userdetails = new ArrayList();
                int index = -1;
                foreach (GridViewRow gvrow in grdRviseCompliances.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    index = Convert.ToInt32(lblDownLoadfile.CommandArgument);
                    bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                    // Check in the Session
                    if (ViewState["checkedCompliances"] != null)
                        userdetails = (ArrayList)ViewState["checkedCompliances"];
                    if (result)
                    {
                        if (!userdetails.Contains(index))
                            userdetails.Add(index);
                    }
                    else
                        userdetails.Remove(index);
                }
                if (userdetails != null && userdetails.Count > 0)
                    ViewState["checkedCompliances"] = userdetails;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }

        //private void CmpInstancesPopulateCheckedValues()
        //{
        //    try
        //    {
        //        ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliancesInstances"];

        //        if (complianceDetails != null && complianceDetails.Count > 0)
        //        {
        //            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
        //            {

        //                int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
        //                if (complianceDetails.Contains(index))
        //                {
        //                    CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkFilterCompliances");
        //                    myCheckBox.Checked = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
           
        //}

        //private void CmpInstancesSaveCheckedValues()
        //{
        //    try
        //    {
        //        ArrayList userdetails = new ArrayList();
        //        int index = -1;
        //        foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
        //        {

        //            index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
        //            bool result = ((CheckBox)gvrow.FindControl("chkFilterCompliances")).Checked;

        //            // Check in the Session
        //            if (ViewState["checkedCompliancesInstances"] != null)
        //                userdetails = (ArrayList)ViewState["checkedCompliancesInstances"];
        //            if (result)
        //            {
        //                if (!userdetails.Contains(index))
        //                    userdetails.Add(index);
        //            }
        //            else
        //                userdetails.Remove(index);
        //        }
        //        if (userdetails != null && userdetails.Count > 0)
        //            ViewState["checkedCompliancesInstances"] = userdetails;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
           
        //}

        //private void BindCompliances(int pageIndex = 0)
        //{
        //    try
        //    {
        //        int complianceCatagoryID = -1;
        //        int complianceTypeID = -1;
        //        if (ddlComplinceCatagory.SelectedValue != "-1" && ddlComplinceCatagory.SelectedValue != "")
        //        {
        //            if (ddlFilterComplianceType.SelectedValue != "-1" && ddlFilterComplianceType.SelectedValue != "")
        //            {
        //                complianceCatagoryID = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
        //                complianceTypeID = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);

        //                List<int> actIds = new List<int>();
        //                foreach (RepeaterItem aItem in rptActList.Items)
        //                {
        //                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
        //                    if (chkAct.Checked)
        //                    {
        //                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
        //                    }
        //                }

        //                if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
        //                {
        //                    grdComplianceInstances.PageIndex = pageIndex;
        //                    grdComplianceInstances.DataSource = Business.ComplianceManagement.GetComplianceForRevise(complianceTypeID, complianceCatagoryID, false, -1, actIds);
        //                    grdComplianceInstances.DataBind();
        //                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    try
        //    {
        //        CmpInstancesSaveCheckedValues();
        //        BindCompliances(pageIndex: e.NewPageIndex);
        //        CmpInstancesPopulateCheckedValues();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
           
        //}

        protected void grdComplianceInstances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

       

        protected string GetPeriodName(long complianceScheduledid,long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetPeriod(complianceScheduledid, complianceinstanceid);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                //BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                //BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                // BindCompliances();
                //saveopo.Value = "false";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
         
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            //saveopo.Value = "true";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ID = Convert.ToInt64(Convert.ToInt32(ViewState["ComplianceTransactionID"])),
                        Penalty = Convert.ToDecimal(txtPenalty.Text),
                        Interest = Convert.ToDecimal(txtInterest.Text),
                        IsPenaltySave = false,
                        PenaltySubmit = "P",
                        Remarks = txtremark.Text,
                    };

                    Business.ComplianceManagement.UpdatePenaltyTransaction(transaction);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Penalty Details save Successfully.";
                    FillComplianceDocuments();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillComplianceDocuments();

                //CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdReviseVersionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        //protected void grdReviseVersionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    try
        //    {
        //        grdReviseVersionHistory.PageIndex = e.NewPageIndex;
        //        BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void grdReviseVersionHistory_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {

        //        var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(ViewState["ScheduledOnID"])).Select(x => new
        //        {
        //            ID = x.ID,
        //            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
        //            VersionDate = x.VersionDate,
        //            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
        //        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

        //        if (direction == SortDirection.Ascending)
        //        {
        //            documentVersionData = documentVersionData.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //            direction = SortDirection.Descending;
        //        }
        //        else
        //        {
        //            documentVersionData = documentVersionData.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //            direction = SortDirection.Ascending;
        //        }

        //        foreach (DataControlField field in grdReviseVersionHistory.Columns)
        //        {
        //            if (field.SortExpression == e.SortExpression)
        //            {
        //                ViewState["SortIndexHistory"] = grdReviseVersionHistory.Columns.IndexOf(field);
        //            }
        //        }

        //        grdReviseVersionHistory.DataSource = documentVersionData;
        //        grdReviseVersionHistory.DataBind();


        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        //private void BindTransactions(int ScheduledOnID)
        //{
        //    try
        //    {
        //        // throw new NotImplementedException();
        //        var documentVersionData = DocumentManagement.GetFileData1(ScheduledOnID).Select(x => new
        //        {
        //            ID = x.ID,
        //            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
        //            VersionDate = x.VersionDate,
        //            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
        //        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

        //        grdReviseVersionHistory.DataSource = documentVersionData;
        //        grdReviseVersionHistory.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtAdvStartDate.Text = string.Empty;
                txtAdvEndDate.Text = string.Empty;
                txtSearchType.Text = string.Empty;
                divAdvSearch.Visible = false;

                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Submit(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                ViewState["checkedCompliances"] = null;

                int type = 0;
                int Category = 0;
                int Act = 0;

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text == "" || txtAdvStartDate.Text == "" && txtAdvEndDate.Text != "")
                {
                    if (txtAdvStartDate.Text == "")
                        txtAdvStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtAdvEndDate.Text == "")
                        txtAdvEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtAdvEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtAdvEndDate.Text;
                    }
                }
                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type to Filter: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    FillComplianceDocuments();
                }
                else
                {
                    divAdvSearch.Visible = false;
                    FillComplianceDocuments();
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDocumentDownload_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }
    }
}

