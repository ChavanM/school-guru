﻿<%@ Page Title="Penalty Updation" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" Async="true" EnableEventValidation="false" CodeBehind="PenaltyUpdationReviewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Penalty.PenaltyUpdationReviewer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            fhead('Penalty Updation Reviewer');
        });


        function fnCheck() {
            if (document.getElementById("ContentPlaceHolder1_txtRevIntrest").value === "") {
                alert("Please enter Interest ");
                return false;
            }
            if (document.getElementById("ContentPlaceHolder1_txtRevPenalty").value != "") {
                var Penalty = document.getElementById("ContentPlaceHolder1_txtRevPenalty").value;
                var penalty = parseInt(Penalty);
                if (penalty == 0 && document.getElementById("ContentPlaceHolder1_txtremarkReviewer").value === "") {
                    alert("Please enter remark");
                    return false;
                }
            } else if (document.getElementById("ContentPlaceHolder1_txtRevPenalty").value === "") {
                alert("Please enter Penalty");
                return false;
            }
        }
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            fchgr();
        });
        function fchgr() {
            $('#ContentPlaceHolder1_txtRevPenalty').change(function () {

                if ($('#ContentPlaceHolder1_txtRevPenalty').val() == 0) {
                    var Penalty = document.getElementById("ContentPlaceHolder1_txtRevPenalty").value;
                    var penalty = parseInt(Penalty);
                    if (penalty == 0) {
                        $('#aaaaa').html('*');
                    }
                }
                else {

                    $('#aaaaa').html('');
                }
            });
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        $(function () {
            $('#divReviseFilter').dialog({
                height: 600,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Manage Document Filter",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divReviseCompliance').dialog({
                height: 600,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Revise Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });


        <%--function SelectheaderCheckboxes(headerchk, gridname) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");

            if (gridname == "grdComplianceDocument") {
                gvcheck = document.getElementById("<%=grdRviseCompliances.ClientID %>");
            } else {
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
            }

            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }--%>

       <%-- function Selectchildcheckboxes(header, gridname) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck;
            if (gridname == "grdComplianceDocument") {
                gvcheck = document.getElementById("<%=grdRviseCompliances.ClientID %>");
            } else {
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
            }
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }--%>

        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= txtAdvStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                onClose: function (startDate) {
                    $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "minDate", startDate);
             }
            });

             $("#<%= txtAdvEndDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,

            });

            if (date != null) {
                $("#<%= txtAdvStartDate.ClientID %>").datepicker("option", "defaultDate", date);
     $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "defaultDate", date);
 }
}

function initializeJQueryUI(textBoxID, divID) {
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}

function checkAll(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
            cbox.checked = cb.checked;
        }
    }
}

function UncheckHeader() {
    var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upDocumentDownload_Load">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                <div class="clearfix"></div>
  <div style="float:right;margin-top:-20px;">
            <asp:LinkButton runat="server" CssClass="btn btn-search" onclick="linkbutton_onclick" >Back</asp:LinkButton>
  
        </div>
<div class="panel-body">
    <div class="col-md-12 colpadding0">

    <div class="col-md-2 colpadding0 entrycount" >
        <div class="col-md-3 colpadding0" >
            <p style="color: #999; margin-top: 5px;">Show </p>
        </div>

        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"> 
            <asp:ListItem Text="5" Selected="True"/>
            <asp:ListItem Text="10" />
            <asp:ListItem Text="20" />
            <asp:ListItem Text="50" />
        </asp:DropDownList> 
       <div class="col-md-3 colpadding0" >
        <p style="color: #999; margin-top: 5px;margin-left:5px;"> Entries</p>
       </div>
    </div>

     <div class="col-md-9 colpadding0" style="text-align: right; float: right">       
            <div style="float:left;margin-right: 2%;">
            <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select"  style="width:110px;" >               
                <asp:ListItem Text="Risk" Value="-1" />
                <asp:ListItem Text="Critical" Value="3" /> 
                <asp:ListItem Text="High" Value="0" />
                <asp:ListItem Text="Medium" Value="1" />
                <asp:ListItem Text="Low" Value="2" />                
            </asp:DropDownList>
                </div>
         <div style="float:left;margin-right: 2%;">
            <asp:DropDownList runat="server" ID="ddlStatus" Visible="false" class="form-control m-bot15 select_location"  style="width: 165px;"></asp:DropDownList>
        </div>

         <div style="float:left;margin-right: 2%;">
            <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 220px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                CssClass="txtbox" /> 
                   
            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                </asp:TreeView>
            </div>  
         </div>

          <div style="float:left;margin-right: 2%;">                
                <asp:Button ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-search" runat="server" Text="Apply" />
            </div>
            <div style="float:right;">
                <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
            </div>
    </div>  

         <!--advance search starts-->
    <%--<div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>

                <div class="modal-body">
                     <h2 style="text-align: center;margin-top:10px;">Advanced Search</h2>
                    <br /> 

                    <div class="col-md-12 colpadding0">
                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div class="table-advanceSearch-Dates">
                            <div class="col-md-6">
                                <div class="form-group">
                                      <div class='input-group date' id='datetimepicker1'>                                            
                                        <asp:TextBox runat="server" Height ="34px" style="border-radius: 4px;" placeholder="Start Date" Width="100px" class="form-control" ID="txtAdvStartDate"/>                                        
                                     </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                     <div class='input-group date' id='datetimepicker2'>
                                        <asp:TextBox runat="server" style="border-radius: 4px;" Height ="34px" placeholder="End Date" Width="100px" class="form-control" ID="txtAdvEndDate"/>                                      
                                      </div>
                                </div>
                            </div>
                        </div>

                <div class="table-advanceSearch-buttons">
                        <asp:Button Text="Search" class="btn btn-search"  runat="server" OnClientClick="return hidediv();" ValidationGroup="DocumentsValidation" OnClick="Submit" />  
                            <button type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                                    <div class="clearfix"></div>
                                                                       
                                                            </div>

                          <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="alert alert-block alert-danger fade in"
                        ValidationGroup="DocumentsValidation" />
                    <asp:CustomValidator ID="cvDocuments" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="DocumentsValidation" Display="None" />                       
                           <br />
                    </div>   
                </div>
            </div>
        </div>
    </div>--%>
        <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body" style="margin-left:50px">
                     <h2 style="text-align: center;margin-top:10px;">Advanced Search</h2>
                    
                    <div class="col-md-12 colpadding0">
                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <asp:Panel ID="Panel1" runat="server">
                             <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                              <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtAdvStartDate" CssClass="StartDate"/>
                           </div>  </asp:Panel><div class="clearfix"></div>
                         <asp:Panel ID="Panel2" runat="server">
                             <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                             <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtAdvEndDate" CssClass="StartDate"/>
                            </div> </asp:Panel>
                        
                <div class="table-advanceSearch-buttons">
                        <asp:Button Text="Search" class="btn btn-search"  runat="server" OnClientClick="return hidediv();" ValidationGroup="DocumentsValidation" OnClick="Submit" />  <%-- --%>
                            <button type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                           </div>

                          <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="alert alert-block alert-danger fade in"
                        ValidationGroup="DocumentsValidation" />
                    <asp:CustomValidator ID="cvDocuments" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="DocumentsValidation" Display="None" />                       
                          
                    </div>   
                </div>
            </div>
        </div>
    </div>
    <!--advance search ends-->
        
<!-- Advance Search scrum-->
<div class="clearfix"></div>

<div class="col-md-12 AdvanceSearchScrum">
<div id="divAdvSearch" runat="server" visible="false">
     <p><asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label></p>
    <p> <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton> </p>
    </div>
<div runat="server" id="DivRecordsScrum" style="float: right;">
   <p style="padding-right: 0px !Important;">
        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
    </p>
</div>

    </div>

</div>
</div>
                   

<div class="clearfix"></div>

            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdRviseCompliances" AutoGenerateColumns="false" GridLines="none" AllowSorting="true" BorderWidth="0px"
                    OnRowCommand="grdRviseCompliances_RowCommand" AllowPaging="True" PageSize="5" Width="100%"
                    CssClass="table" OnRowDataBound="grdRviseCompliances_RowDataBound">
                    <Columns>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:250px; ">
                                    <asp:Label ID="lblShortDiscription" runat="server" Text='<%# Eval("ShortDescription") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    <asp:Label ID="lblComplianceTransactionID" Visible="false" runat="server" Text='<%# Eval("ComplianceTransactionID") %>'></asp:Label>
                                    <asp:Label ID="lblComplianceStatusID" Visible="false" runat="server" Text='<%# Eval("ComplianceStatusID") %>'></asp:Label>
                                    <asp:Label ID="lblComplianceInstanceID" Visible="false" runat="server" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                    <asp:Label ID="lblComplianceScheduledID" Visible="false" runat="server" Text='<%# Eval("ComplianceScheduledID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                     
                    <asp:TemplateField HeaderText="Performer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPerformer" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                  
                        <asp:TemplateField HeaderText="Due Date">
                            <ItemTemplate>
                                  <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                        
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                <asp:Label ID="lblForMonth" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# GetPeriodName((long)Eval("ComplianceScheduledID"), (long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                                            </ItemTemplate>
                         </asp:TemplateField>     
                          <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                  <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                       <asp:Label ID="lblStatus" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                  </div>
                                            </ItemTemplate>
                         </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkReviseCompliances" runat="server" CommandName="Penalty" CommandArgument='<%# Eval("ComplianceScheduledID") +" , "+ Eval("ComplianceInstanceID") +" , "+ Eval("ComplianceTransactionID")%>' 
                                    Visible='<%# visiblePenaltyButton(Convert.ToInt32(Eval("ComplianceScheduledID")),Convert.ToInt32(Eval("ComplianceInstanceID"))) %>'
                                    ToolTip="Add Penalty"><a data-toggle="modal" href="#Newaddremider"/><img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>'  data-toggle="tooltip" data-placement="bottom" alt="Add Penalty" title="Add Penalty" /></asp:LinkButton> <%--<a data-toggle="modal" href="#Newaddremider">--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid"   />
                <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                   
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>  

            <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click" /> 
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click"/>                
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>

                        </div>
                     </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div>
        <div class="modal fade" id="Newaddremider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 750px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ReviseComplianceDetailsGroup" />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                        ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>

                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Compliance Details</legend>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 20%; font-weight: bold; vertical-align: top;">Short Description</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 78%;">
                                                <asp:Label ID="lblShorDescription" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                            <td style="width: 78%;">
                                                <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>

                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Penalty Information</legend>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Performer Interest</label>
                                            <asp:TextBox runat="server" ReadOnly="true" ID="txtInterestPerformer" Width="100px" class="form-control" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Performer Penalty</label>
                                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPenaltyPerformer" Width="100px" class="form-control" />
                                        </div>
                                          <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Performer Remark</label>
                                            <asp:TextBox runat="server" ReadOnly="true" ID="txtPerformerRemark" Width="100px" class="form-control" />
                                        </div>
                                    </fieldset>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Reviewer Interest</label>
                                            <asp:TextBox runat="server" ID="txtRevIntrest" Width="100px" class="form-control" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                TargetControlID="txtRevIntrest" />
                                            <asp:RequiredFieldValidator ErrorMessage="Please enter intrest" ControlToValidate="txtRevIntrest"
                                                runat="server" ID="RequiredFieldValidator2" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />

                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Reviewer Penalty</label>
                                            <asp:TextBox runat="server" ID="txtRevPenalty" Width="100px" class="form-control" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                TargetControlID="txtRevPenalty" />
                                            <asp:RequiredFieldValidator ErrorMessage="Please enter penalty" ControlToValidate="txtRevPenalty"
                                                runat="server" ID="RequiredFieldValidator4" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label id="aaaaa" style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                                Remark</label>
                                            <asp:TextBox runat="server" ID="txtremarkReviewer" Width="100px" class="form-control" />
                                        </div>
                                    </fieldset>
                                    <div class="table-advanceSearch-buttons">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" class="btn btn-search"
                                            ValidationGroup="ReviseComplianceDetailsGroup" OnClientClick="javascript:return fnCheck()" OnClick="btnSave_Click" />
                                        <%--<button type="button" class="btn btn-search" onclick="" data-dismiss="modal">Close</button>--%>
                                        <asp:Button Text="Close" runat="server" ID="btnClose" class="btn btn-search" data-dismiss="modal" OnClick="btnClose_Click" />
                                    </div>
                                </fieldset>

                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 4px">
        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
    </div>
</asp:Content>
