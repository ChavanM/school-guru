﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class PenaltyDetailsAPI : System.Web.UI.Page
    {
        public string CompDocReviewPath = "";
        protected string CId;
        protected string UserId;
        protected int CustId;
        protected int UId;
        protected List<Int32> roles;
        protected int RoleID;
        protected int UserRoleID;
        protected int RoleFlag;
        protected string Flag;
        protected bool DisableFalg;
        protected string Path;
        protected int isapprover;
        protected string IS;
        protected string Internalsatutory;
        protected int IsDeptHead;
        protected int ComplianceTypeID;
        protected static string CustomerName;
        protected string pointname;
        protected string attribute;
        protected string branchid;
        protected string FromDate;
        protected string Enddate;
        protected static string DisplayName;
        protected static string Authorization;
        public string ispenaltytext;
        public string customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            ispenaltytext = Convert.ToString(ConfigurationManager.AppSettings["IsPenaltyTextchanged"]);
            customerid = Convert.ToString(AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IsDeptHead = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    IsDeptHead = Convert.ToInt32(Request.QueryString["IsDeptHead"]);
                }
                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    isapprover = 0;
                    Flag = "MGMT";
                }
                else if (IsDeptHead==1)
                {
                    Flag = "DEPT";
                }
                else
                {
                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                    if (GetApprover.Count > 0)
                    {
                        isapprover = 1;
                        Flag = "APPR";
                    }
                }

                if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || IsDeptHead == 1 || AuthenticationHelper.ComplianceProductType == 3 || Flag== "APPR")
                {

                    Path = ConfigurationManager.AppSettings["KendoPathApp"];
                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    UserId = Convert.ToString(AuthenticationHelper.UserID);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    CustomerName = CustomerManagement.CustomerGetByIDName(CustId);

                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        Internalsatutory = Request.QueryString["Internalsatutory"];
                        if (Internalsatutory == "Statutory")
                        {
                            IS = "S";
                        }
                        else
                        {
                            IS = "I";
                        }
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["DisplayName"]))
                    {
                        DisplayName = Convert.ToString(Request.QueryString["DisplayName"]);
                    }
                    if(ispenaltytext==customerid)
                    {
                        if (DisplayName == "topPnsummary")
                        {
                            display.InnerText = "Penalty/Additional Fee";
                        }
                        else if (DisplayName == "PNH_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                        else if (DisplayName == "PNM_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                        else if (DisplayName == "PNL_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                    }
                    else
                    {
                        if (DisplayName == "topPnsummary")
                        {
                            display.InnerText = "Penalty";
                        }
                        else if (DisplayName == "PNH_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                        else if (DisplayName == "PNM_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                        else if (DisplayName == "PNL_psummary")
                        {
                            display.InnerText = "Compliance List";
                        }
                    }
                    

                    if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                    {
                        pointname = Request.QueryString["pointname"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["attrubute"]))
                    {
                        attribute = Request.QueryString["attrubute"];
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        branchid = Convert.ToString(Request.QueryString["branchid"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["startdate"]))
                    {
                        FromDate = Convert.ToString(Request.QueryString["startdate"]);
                    }
                    else
                    {
                        FromDate = "01-01-1900";
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["enddate"]))
                    {
                        Enddate = Convert.ToString(Request.QueryString["enddate"]);
                    }
                    else
                    {
                        Enddate = "01-01-1900";
                    }

                    if (ComplianceTypeID != 3)
                    {
                        if (isapprover == 0)
                        {
                            if (IsDeptHead == 0)
                                ComplianceTypeID = 0;//statutory
                            else
                                ComplianceTypeID = 1;//statutoryDept
                        }
                        else
                        {
                            ComplianceTypeID = 2;//Approver
                        }
                    }
                }
            }
        }
    }
}
