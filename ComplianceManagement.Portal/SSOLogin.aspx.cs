﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using AjaxControlToolkit;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class SSOLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                txtemail.Focus();
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string userIpAddress = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string Macaddress = string.Empty;
                    Macaddress = Util.GetMACAddress();
                    //userIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    //if (userIpAddress == "" || userIpAddress == null)
                    //    userIpAddress = Request.ServerVariables["REMOTE_ADDR"];



                    if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-Request.ServerVariables[HTTP_X_FORWARDED_FOR]-" + Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);

                    if (!string.IsNullOrEmpty(Request.ServerVariables["REMOTE_ADDR"]))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-Request.ServerVariables[REMOTE_ADDR]-" + Request.ServerVariables["REMOTE_ADDR"]);

                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-HttpContext.Current.Request.UserHostAddress-" + HttpContext.Current.Request.UserHostAddress);

                    userIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    if (userIpAddress == "" || userIpAddress == null)
                        userIpAddress = Request.ServerVariables["REMOTE_ADDR"];

                    if (userIpAddress == "" || userIpAddress == null)
                        userIpAddress = HttpContext.Current.Request.UserHostAddress;



                    try
                    {
                        UserLoginTrack obj = new UserLoginTrack()
                        {
                            Email = txtemail.Text,
                            LoginDate = DateTime.Now,
                            IPAddress = userIpAddress,
                            MACAddress = Macaddress,
                            LoginFlag = false
                        };
                        UserManagement.CreateUserLoginTrack(obj);
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + userIpAddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    NoBotState state;
                    //if (true)
                    if (PageNoBot.IsValid(out state))
                    {
                        Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;

                        if (txtemail.Text.Trim().Contains('@'))
                        {
                            Business.Data.User user = null;

                            if (UserManagement.IsValidUser(txtemail.Text.Trim(), out user))
                            {
                                if (user != null)
                                {
                                    try
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, userIpAddress);
                                            if (!blockip.Item1)
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin";
                                                return;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }

                                    try
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            if (Convert.ToBoolean(user.SSOAccess))
                                            {
                                                LogLibrary.WriteErrorLog("\n\nIPAddress- " + userIpAddress);

                                                var alliplist = (from entry in entities.AllowedIPs
                                                                 where entry.Customerid == user.CustomerID
                                                                 && entry.IsActive == true                                                                
                                                                 select entry).ToList();

                                                if (alliplist != null && alliplist.Count > 0)
                                                {
                                                    var checkip = alliplist.Where(en => en.IPAddress == userIpAddress).ToList();
                                                    if (checkip.Count > 0)
                                                    {
                                                        var custRecord = SAMLManagement.CheckCustomerExists(Convert.ToInt32(user.CustomerID));
                                                        if (custRecord != null)
                                                        {
                                                            if (!string.IsNullOrEmpty(custRecord.SAMLLoginPageURL) && custRecord.IdentityProvider == "Microsoft")
                                                            {
                                                                Response.Redirect(custRecord.SAMLLoginPageURL + "?ID=" + user.CustomerID, true);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvLogin.IsValid = false;
                                                        cvLogin.ErrorMessage = "Your IP Address is blocked. Please Contact to Admin.";
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    var custRecord = SAMLManagement.CheckCustomerExists(Convert.ToInt32(user.CustomerID));

                                                    if (custRecord != null)
                                                    {
                                                        if (!string.IsNullOrEmpty(custRecord.SAMLLoginPageURL) && custRecord.IdentityProvider == "Microsoft")
                                                        {
                                                            Response.Redirect(custRecord.SAMLLoginPageURL + "?ID=" + user.CustomerID, true);
                                                        }
                                                    }
                                                }                                                
                                            }
                                            else
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Request you to login through Google or your Company AD";
                                                return;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    UserManagement.WrongUpdate(txtemail.Text.Trim());
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Please enter valid username";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                                }
                            }
                            else
                            {
                                UserManagement.WrongUpdate(txtemail.Text.Trim());
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Please enter valid username";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Please enter valid email";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + userIpAddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool data(List<AllowedIP> iplist)
        {
            try
            {
                bool ipAllowed = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                 
                    HttpContext obj = HttpContext.Current;
                    //Logger.WriteErrorLog("Application_BeginRequest");
                    string userIpAddress = string.Empty;

                    //userIpAddress = HttpContext.Current.Request.UserHostAddress;

                    //if (!string.IsNullOrEmpty(userIpAddress))
                    //    userIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    //if (!string.IsNullOrEmpty(userIpAddress))
                    //    userIpAddress = Request.ServerVariables["REMOTE_ADDR"];

                    if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-Request.ServerVariables[HTTP_X_FORWARDED_FOR]-" + Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);

                    if (!string.IsNullOrEmpty(Request.ServerVariables["REMOTE_ADDR"]))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-Request.ServerVariables[REMOTE_ADDR]-" + Request.ServerVariables["REMOTE_ADDR"]);

                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
                        LogLibrary.WriteErrorLog("\n\nIPAddress-HttpContext.Current.Request.UserHostAddress-" + HttpContext.Current.Request.UserHostAddress);

                    userIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (userIpAddress == "" || userIpAddress == null)
                        userIpAddress = Request.ServerVariables["REMOTE_ADDR"];

                    if (userIpAddress == "" || userIpAddress == null)
                        userIpAddress = HttpContext.Current.Request.UserHostAddress;

                    if (!string.IsNullOrEmpty(userIpAddress))
                    {
                        LogLibrary.WriteErrorLog("\n\nRequest Received at-" + DateTime.Now + "-From IPAddress-" + userIpAddress);

                        var checkip = iplist.Where(en => en.IPAddress == userIpAddress).ToList();
                      
                        if (checkip.Count>0)
                        {
                            ipAllowed = true;
                        }
                        else
                        {
                            ipAllowed = false;
                        }                     
                    }
                    else
                        LogLibrary.WriteErrorLog("IPAddress-BLANK");
                }
                return ipAllowed;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertErrorMsg_DBLog(ex.ToString(), MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}