﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CMPLogin.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.CMPLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login Page</title>
</head>
<body style="background-color: #232323">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" />
    <div style="background-image: url('./Images/bg.jpg'); height: 612px; width: 1251px;">
        <div runat="server" id="divLogin" style="margin-left: 550px; padding-top: 200px;
            clear: both;">
            <div style="margin-top: 0; float: right; margin-right: 30px">
                <table border="0" cellpadding="0" cellspacing="5" style="color: #5E6871; font-family: Verdana;
                    font-size: 14px; clear: both" width="300px">
                    <tr>
                        <td colspan="2">
                            Account Information
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Username:
                        </td>
                        <td>
                            <asp:TextBox ID="tbxUsername" runat="server" Width="100%" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ErrorMessage="Username can not be empty."
                                ControlToValidate="tbxUsername" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="tbxPassword" runat="server" TextMode="Password" Width="100%" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Password can not be empty."
                                ControlToValidate="tbxPassword" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbRememberMe" Text="Keep me logged in" runat="server" Font-Size="12px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <%--        <asp:ImageButton ID="ImageButton1" ImageUrl="~/Images/login-tab.png" runat="server" OnClick="btnLogin_Click"
                                UseSubmitBehavior="true" />
                            --%>
                            <asp:Button ID="Button1" Text="Login" runat="server" OnClick="btnLogin_Click" UseSubmitBehavior="true"
                                Style="padding: 5px; width: 80px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CustomValidator ID="cvLogin" EnableClientScript="False" runat="server" Display="None" />
                            <asp:ValidationSummary ID="vsLogin" runat="server" Style="border: solid 1px red;
                                background-color: #ffe8eb;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: left">
                <label style="color: #ECEE73; font-family: Verdana; font-size: 32px; margin-top: 80;">
                    LOGIN PANEL</label>
                <br />
                <label style="color: #5E6871; font-family: Verdana; font-size: 14px; clear: both">
                    Please enter your username and password.</label>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
