﻿<%@ Page Title="Upload Master Data" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadMasterData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadMasterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Add Compliance" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="Update Compliance" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                    OnClick="Tab2_Click" />
                <asp:Button Text="Add Checklist" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />
                 <asp:Button Text="Add Event Compliance" BorderStyle="None" ID="Tab4" CssClass="Initial" runat="server"
                    OnClick="Tab4_Click" />

                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div>
                                            <asp:RadioButton ID="rdoAct" runat="server" AutoPostBack="false" Text="Act" GroupName="uploadContentGroup" /><br />
                                            <asp:RadioButton ID="rdoCompliance" runat="server" AutoPostBack="false" Text="Compliance" GroupName="uploadContentGroup" /><br />                                          
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup1" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage1" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage1" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />

                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile1" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload1" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload1"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile1" runat="server" Text="Upload" ValidationGroup="oplValidationGroup1"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile1_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                    <asp:View ID="View3" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup2" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage2" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage2" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry2" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup2" Display="None" Enabled="true" ShowSummary="true" />

                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">                                
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div>
                                            <asp:RadioButton ID="rdoAct1" runat="server" AutoPostBack="false" Text="Act" GroupName="uploadContentGroup1" /><br />
                                            <asp:RadioButton ID="rdoChecklist" runat="server" AutoPostBack="false" Text="Checklist" GroupName="uploadContentGroup1" /><br />                                          
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile2" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload2" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload2"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile2" runat="server" Text="Upload" ValidationGroup="oplValidationGroup2"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile2_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>

                      <asp:View ID="View4" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1Event" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroupEvent" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessageEvent" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessageEvent" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntryEvent" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroupEvent" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div>
                                            <asp:RadioButton ID="rdoActEvent" runat="server" AutoPostBack="false" Text="Act" GroupName="uploadContentGroupEvent" /><br />
                                            <asp:RadioButton ID="rdoComplianceEvent" runat="server" AutoPostBack="false" Text="Compliance" GroupName="uploadContentGroupEvent" /><br />                                          
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFileEvent" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUploadEvent" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select File." ControlToValidate="MasterFileUploadEvent"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroupEvent" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFileEvent" runat="server" Text="Upload" ValidationGroup="oplValidationGroupEvent"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFileEvent_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                   

                </asp:MultiView>
            </td>
        </tr>
    </table>
</asp:Content>
