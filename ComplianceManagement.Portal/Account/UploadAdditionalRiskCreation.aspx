﻿<%@ Page Title="Upload/Update Audit Steps" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="UploadAdditionalRiskCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadAdditionalRiskCreation" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        $(document).ready(function () {
          //  setactivemenu('Import / Export Utility');
            fhead('Import / Export Utility');
        });
    </script>

    <script type="text/javascript">

        function showProgress() {
            
            var updateProgress = $get("<%# updateProgress.ClientID %>");
               updateProgress.style.display = "block";
           }
    </script>

    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   

     <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

 <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <section class="panel">                     
                     
                    <%-- <div class="col-md-12 colpadding0">   --%>
                        <header class="panel-heading tab-bg-primary ">

                            <ul id="rblRole" class="nav nav-tabs">                                                                                                                                           
                                    <li class="active" id="liRCM" runat="server">
                                        <asp:LinkButton ID="lnkRCM"  PostBackUrl="../Account/UploadRiskCreation.aspx"   runat="server">Step 1 - Risk Control Matrix</asp:LinkButton>                                           
                                    </li>                                                                                         
                                    <li class="" id="liAuditchecklist" runat="server">
                                        <asp:LinkButton ID="lnkAuditchecklist" PostBackUrl="../Account/UploadAdditionalRiskCreation.aspx" runat="server">Step 2 - Audit Steps</asp:LinkButton>                                           
                                    </li>                                                                                                                                                                                                    
                                 </ul>                           
                        </header>
                    <%--</div>--%>  

                    <div class="clearfix"></div> 
                    
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"  
                            ValidationGroup="oplValidationGroup" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                    </div> 

                     <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">  
                        <ul id="rblRole1" class="nav nav-tabs">
                            <li class="active" id="liImport" runat="server">                                                    
                                <asp:LinkButton ID="Tab1" OnClick="Tab1_Click" class="active" runat="server">Import Utility</asp:LinkButton>
                            </li>
                            <li class="" id="liExportCreate" runat="server">                                                    
                                <asp:LinkButton ID="Tab2" OnClick="Tab2_Click" class=""  runat="server">Export Utility</asp:LinkButton>
                            </li>                          
                            <li class="" id="liExportUpdate" runat="server">                                                    
                                <asp:LinkButton ID="Tab3" OnClick="Tab3_Click" class=""  runat="server">Export (Update)</asp:LinkButton>
                            </li>                         
                        </ul>
                     </div>

                    <div class="clearfix"></div> 
                   
                    <div class="tab-content ">

                        <div runat="server" id="performerdocuments"  class="tab-pane active"> 
                            <div style="width: 100%; float: left; margin-bottom: 15px">         
                                 <div class="col-md-12 colpadding0" style="margin-top: 1em;">                      
                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top:5px;">                                                  
                                        <asp:RadioButton ID="rdoCompliance" runat="server" AutoPostBack="false" Text="Upload Audit Steps" GroupName="uploadContentGroup" ForeColor="#333" />
                                    </div>

                                     <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; color:#333;">
                                        <asp:RadioButton ID="rdoAuditStepUpdate" runat="server" Text="Update Audit Steps" GroupName="uploadContentGroup" />
                                    </div>

                                    <div class="col-md-5 colpadding0 entrycount" style="margin-top:5px; color:#333;">                               
                                        <asp:FileUpload ID="MasterFileUpload" runat="server"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                        runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </div>

                                    <div class="col-md-2 colpadding0 entrycount" style="margin-top:5px;">
                                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup" 
                                            class="btn btn-primary"  Style="margin-left: 75px;" OnClick="btnUploadFile_Click" OnClientClick="showProgress()" />
                                    </div>
                                </div>  
                            </div>                                      
                        </div>

                        <div class="clearfix"> </div>

                        <div runat="server" id="reviewerdocuments" class="tab-pane">
                            <div style="width: 100%; float: left; margin-bottom: 15px">

                                <div class="col-md-12 colpadding0">   
                                   <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup1" />
                                    <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                        <asp:Label ID="lblMessage1" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="LblErormessage1" runat="server" Text="" ForeColor="red"></asp:Label>
                                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                                    </div>
                                </div>                               

                                 <div class="col-md-12 colpadding0">                                                                                         
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                        
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                  
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15"  Width="90%" Height="32px"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                        </asp:DropDownListChosen>
                                    </div>
                                 </div>

                                <div class="clearfix"> </div>

                                <div class="col-md-12 colpadding0">                                       
                                   <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location" 
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" CssClass="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                   </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlProcess" AutoPostBack="true" Width="90%" Height="32px"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" CssClass="form-control m-bot15" DataPlaceHolder="Process">
                                        </asp:DropDownListChosen>
                                    </div> 
                                    
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" 
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  Width="90%" Height="32px" DataPlaceHolder="Sub Process">
                                    </asp:DropDownListChosen>                                  
                                 </div>  
                                         <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%> 
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">     
                                          <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" 
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"  CssClass="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical">
                                        </asp:DropDownListChosen>                                     
                                    </div>    
                                     <%}%>                                    
                                </div>

                                 <div class="clearfix"></div>
                               
                                     <div class="col-md-12 colpadding0" style="text-align: right;margin-top: 5px;">
                                       <asp:Button ID="btnUploadFile1" runat="server" Text="Download" OnClick="btnUploadFile1_Click" 
                                            class="btn btn-primary" ValidationGroup="oplValidationGroup" Style="margin-right:3%;" />
                                    </div>
                           
                                  <div class="clearfix"> </div>  
                           
                                <div class="clearfix"> </div>             
                                   
                               
                                        <asp:Panel ID="Panel2" runat="server">
                                            <%--OnDataBound="GridExportExcel_DataBound"--%>
                                            <asp:GridView ID="GridExportExcel" runat="server"  Width="100%" ShowHeaderWhenEmpty="true" 
                                            CssClass="table" GridLines="none" BorderWidth="0px" AutoGenerateColumns="false" PageSize="50" AllowPaging="true" >
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField> 

                                                    <asp:BoundField DataField="RiskCreationId" HeaderText="RiskID" Visible="false"/>
                                                    <asp:BoundField DataField="RiskActivityID" HeaderText="RiskActivityID"  Visible="false"  />
                                                   
                                                    <asp:BoundField DataField="ControlNo" HeaderText="Control No" />

                                                     <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="BranchName" runat="server" Text='<%# Eval("BranchName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("BranchName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                     <asp:TemplateField HeaderText="Vertical">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="VerticalName" runat="server" Text='<%# Eval("VerticalName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("VerticalName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                   <asp:TemplateField HeaderText="Process">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="ProcessName" runat="server" Text='<%# Eval("ProcessName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ProcessName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Sub Process">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                                            <asp:Label ID="SubProcessName" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("SubProcessName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Risk&nbsp;Description">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Control Description">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="lblControlDescription" runat="server" Text='<%# Eval("ControlDescription") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ControlDescription") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:BoundField DataField="RiskRating" HeaderText="RiskRating"/>
                                                    
                                                    <asp:BoundField DataField="ActivityTobeDone" HeaderText="Audit Step"/>
                                                   
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />                                  
                                                <PagerStyle HorizontalAlign="Right" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>
                                     
                                                      
                            </div>
                        </div>

                         <div class="clearfix"> </div>

                         <div runat="server" id="DivExportUpdate" class="tab-pane">
                            <div style="width: 100%; float: left; margin-bottom: 15px">

                                <div class="col-md-12 colpadding0">   
                                   <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup1" />
                                    <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">                                        
                                        <asp:CustomValidator ID="cvAuditStepUpdate" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplUpdateValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                                    </div>
                                </div>                               

                                 <div class="col-md-12 colpadding0">                                                                                         
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                        
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntityUpdate"  class="form-control m-bot15"  Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntityUpdate_SelectedIndexChanged" DataPlaceHolder="Unit">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1Update" class="form-control m-bot15"  Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1Update_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                  
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2Update"  class="form-control m-bot15"  Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2Update_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Update" class="form-control m-bot15" Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3Update_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                        </asp:DropDownListChosen>
                                    </div>
                                 </div>

                                <div class="clearfix"> </div>

                                <div class="col-md-12 colpadding0">                                       
                                   <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLocationUpdate" AutoPostBack="true"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlLocationUpdate_SelectedIndexChanged" CssClass="form-control m-bot15" 
                                                DataPlaceHolder="Location" Width="90%" Height="32px"                                      >
                                        </asp:DropDownListChosen>
                                   </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlProcessUpdate" AutoPostBack="true" 
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlProcessUpdate_SelectedIndexChanged" CssClass="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Process">
                                        </asp:DropDownListChosen>
                                    </div> 
                                    
                                   <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlSubProcessUpdate" runat="server" AutoPostBack="true" class="form-control m-bot15" 
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   Width="90%" Height="32px" DataPlaceHolder="Sub Process">
                                    </asp:DropDownListChosen>                                  
                                 </div> 
                                             <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%> 
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">     
                                          <asp:DropDownListChosen runat="server" ID="ddlVerticalUpdate" AutoPostBack="true" 
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"  CssClass="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical">
                                        </asp:DropDownListChosen>                                     
                                    </div>
                                     <%}%> 
                                    </div>

                                <div class="clearfix"></div>
                               
                                     <div class="col-md-12 colpadding0" style="text-align: right;margin-top: 5px;">
                                        <asp:Button ID="btnExportUpdate" runat="server" Text="Download" OnClick="btnExportUpdate_Click" 
                                            class="btn btn-primary" ValidationGroup="oplValidationGroup"  Style="margin-right: 3%;" />
                                    </div>
                           
                                  <div class="clearfix"> </div>         
                                   
                                <%--<asp:UpdatePanel ID="upExportExcel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>--%>
                                        <asp:Panel ID="Panel1" runat="server">                                           
                                            <asp:GridView ID="GridExportUpdate" runat="server"  Width="100%" ShowHeaderWhenEmpty="true" 
                                            CssClass="table" GridLines="none" BorderWidth="0px" AutoGenerateColumns="false" PageSize="50" AllowPaging="true" >
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField> 

                                                    <asp:BoundField DataField="RiskCreationId" HeaderText="RiskID" Visible="false"/>
                                                    <asp:BoundField DataField="RiskActivityID" HeaderText="RiskActivityID"  Visible="false"  />
                                                   
                                                    <asp:BoundField DataField="ControlNo" HeaderText="Control No" />

                                                     <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="BranchName" runat="server" Text='<%# Eval("BranchName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("BranchName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                     <asp:TemplateField HeaderText="Vertical">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="VerticalName" runat="server" Text='<%# Eval("VerticalName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("VerticalName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                   <asp:TemplateField HeaderText="Process">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="ProcessName" runat="server" Text='<%# Eval("ProcessName") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ProcessName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Sub Process">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                                            <asp:Label ID="SubProcessName" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("SubProcessName") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Risk&nbsp;Description">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Control Description">
                                                        <ItemTemplate><div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:80px;">
                                                            <asp:Label ID="lblControlDescription" runat="server" Text='<%# Eval("ControlDescription") %>'  data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ControlDescription") %>'></asp:Label></div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:BoundField DataField="RiskRating" HeaderText="RiskRating"/>
                                                    
                                                    <asp:BoundField DataField="ActivityTobeDone" HeaderText="Audit Step"/>
                                                    <asp:BoundField DataField="AuditObjective" HeaderText="Audit Objective" Visible="false"/>
                                                    <asp:BoundField DataField="AuditStepRating" HeaderText="Audit Step Rating" Visible="false"/>
                                                   
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />                                  
                                                <PagerStyle HorizontalAlign="Right" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>
                                     
                                   <%-- </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUploadFile1" />
                                        <asp:PostBackTrigger ControlID="btnUploadFile" />                                       
                                    </Triggers>
                                </asp:UpdatePanel>--%>                    
                            </div>
                        </div>

                        <div class="clearfix"> </div>

                    </div>
                </section>
            </div>
        </div>
    </div>  

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile1" />
            <asp:PostBackTrigger ControlID="btnUploadFile" />   
            <asp:PostBackTrigger ControlID="btnExportUpdate" />                                    
        </Triggers>
    </asp:UpdatePanel>  
</asp:Content>
