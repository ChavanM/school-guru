﻿
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ComplianceAssignmentUploadAll : System.Web.UI.Page
    {
        bool suucess = false;
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomers(userID);
                BindECustomers(userID);
                Tab1_Click(sender, e);
            }
        }
        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            string FileName = "ComplianceAssignmentFormat.xlsx";

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "image/jpeg";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(Server.MapPath("~/ExcelFormat/ComplianceAssignmentFormat.xlsx"));
            response.Flush();
            response.End();
        }
        private void BindECustomers(int userID)
        {
            try
            {
                ddlECustomerList.DataTextField = "Name";
                ddlECustomerList.DataValueField = "ID";
                ddlECustomerList.DataSource = CustomerManagement.GetAllCustomer(userID, "");
                ddlECustomerList.DataBind();

                ddlECustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers(int userID)
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                //  ddlCustomerList.DataSource = CustomerManagement.GetAll("");
                ddlCustomerList.DataSource = CustomerManagement.GetAllCustomer(userID,"");
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void BtnDownloadAssignment_Click(object sender, EventArgs e)
        {

            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AssignmentReportStatutory");
                        DataTable ExcelData = null;
                        DateTime CurrentDate = DateTime.Today.Date;

                        if (ddlECustomerList.SelectedValue != "-1")
                        {
                            int customerid = Convert.ToInt32(ddlECustomerList.SelectedValue);

                            var assignmentList = ComplianceCertificate.GetStatutoryEventBaesedComplianceAssigned(customerid, -1).ToList();

                            if (assignmentList.Count > 0)
                            {
                                DataView view = new System.Data.DataView((assignmentList).ToDataTable());

                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "BranchID", "Branch",
                                     "ActName", "ShortDescription", "Section", "Performer", "Reviewer", "Cer_OwnerUserID", "Cer_OfficerUserID");

                                exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A1"].Value = "ComplianceID";
                                exWorkSheet.Cells["A1"].AutoFitColumns(30);

                                exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B1"].Value = "BranchID";
                                exWorkSheet.Cells["B1"].AutoFitColumns(30);

                                exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["C1"].Value = "Location";
                                exWorkSheet.Cells["C1"].AutoFitColumns(30);

                                exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["D1"].Value = "Act Name";
                                exWorkSheet.Cells["D1"].AutoFitColumns(50);


                                exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["E1"].Value = "ShortDescription";
                                exWorkSheet.Cells["E1"].AutoFitColumns(50);

                                exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["F1"].Value = "Section";
                                exWorkSheet.Cells["F1"].AutoFitColumns(30);

                                exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["G1"].Value = "Performer";
                                exWorkSheet.Cells["G1"].AutoFitColumns(30);

                                exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["H1"].Value = "Reviewer";
                                exWorkSheet.Cells["H1"].AutoFitColumns(30);


                                exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["I1"].Value = "Certificate OwnerID";
                                exWorkSheet.Cells["I1"].AutoFitColumns(30);

                                exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["J1"].Value = "Certificate OfficerID";
                                exWorkSheet.Cells["J1"].AutoFitColumns(30);

                                using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 10])
                                {
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }

                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=AssignmentReportStatutory.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data Not Available!!!";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select Customer";
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        private bool ComplianceAssignmentUploadSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AssignmentReportStatutory"))
                    {
                        if (sheet.Name.Trim().Equals("AssignmentReportStatutory"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void btnEUploadFile_Click(object sender, EventArgs e)
        {
            if (EMasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlECustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(EMasterFileUpload.FileName);
                            EMasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentUploadSheetsExitsts(xlWorkbook, "AssignmentReportStatutory");
                                        if (flag == true)
                                        {
                                            int customerID = Convert.ToInt32(ddlECustomerList.SelectedValue);
                                            int complianceProductType = 0;
                                            complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);

                                            if (HttpContext.Current.Cache.Get("EComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EComplianceListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OwnerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OfficerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                            }

                                            GetStatutoryCompliance();
                                            GetCertificateOfficerUser(customerID);
                                            GetCertificateOwnerUser(customerID);
                                            GetCustomerBranch(customerID);

                                            ComplianceOfficerOwnerAssignmentData(xlWorkbook);

                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry2.IsValid = false;
                                                cvDuplicateEntry2.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("EComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OwnerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OfficerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please check the sheet name, Sheet name must be 'UploadComplianceAssignment'";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        private void ComplianceOfficerOwnerAssignmentData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AssignmentReportStatutory"];
                if (xlWorksheet != null)
                {
                    int count = 1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<ComplianceInstance> ComplianceInstanceList = new List<ComplianceInstance>();
                    List<string> errorMessage = new List<string>();


                    #region Validations
                    int valComplianceID = -1;
                    int valOfficerID = -1;
                    int valOwnerID = -1;
                    int valCustomerBranchID = -1;
                    string valSequence = string.Empty;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valComplianceID = -1;
                        valOfficerID = -1;
                        valOwnerID = -1;
                        valCustomerBranchID = -1;
                        valSequence = string.Empty;
                        #region 1 ComplianceID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valComplianceID == -1 || valComplianceID == 0)
                        {
                            errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                        }
                        else
                        {
                            if (ComplainceStatutoryExists(valComplianceID) == false)
                            {
                                errorMessage.Add("ComplianceID not defined in the System or Informative Compliance at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 2 OwnerID OfficerID                       

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString()))
                        {
                            valOwnerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 9].Text);
                            if (valOwnerID != -1 && valOwnerID != 0)
                            {
                                if (UserOwnerExists(valOwnerID) == false)
                                {
                                    errorMessage.Add("OwnerID not Defined in the System at row number-" + rowNum);
                                }

                            }
                        }
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString()))
                        {
                            valOfficerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 10].Text);
                            if (valOfficerID != -1 && valOfficerID != 0)
                            {
                                if (UserOfficerExists(valOfficerID) == false)
                                {
                                    errorMessage.Add("OfficerID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }
                        #endregion

                        #region 3 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessagesUplad(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int ComplianceID = -1;
                            int OfficerID = -1;
                            int OwnerID = -1; ;
                            int CustomerBranchID = -1;
                            List<string> abclist = new List<string>();
                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region OwnerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString()))
                            {
                                OwnerID = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text);
                            }
                            #endregion

                            #region OfficerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString()))
                            {
                                OfficerID = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text);
                            }
                            #endregion 

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            if ((Business.ComplianceCertificate.ExistComplianceInstance(ComplianceID, CustomerBranchID)))
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    ComplianceInstance TempAssP = new ComplianceInstance();
                                    TempAssP.ComplianceId = ComplianceID;
                                    TempAssP.CustomerBranchID = CustomerBranchID;
                                    TempAssP.Cer_OwnerUserID = OwnerID;
                                    TempAssP.Cer_OfficerUserID = OfficerID;

                                    ComplianceInstanceList.Add(TempAssP);
                                }
                            }
                        }
                        #endregion

                        if (ComplianceInstanceList.Count > 0)
                        {
                            suucess = UpdateExcelComplianceInstanceTable(ComplianceInstanceList);
                        }
                        else
                        {
                            suucess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public bool ComplainceStatutoryExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["EComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public void ErrorMessagesUplad(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry2.IsValid = false;
            cvDuplicateEntry2.ErrorMessage = finalErrMsg;
        }
        public bool UpdateExcelComplianceInstanceTable(List<ComplianceInstance> ComplianceInstanceTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ComplianceInstanceTable.ForEach(entry =>
                    {
                        ComplianceInstance complianceInstanceToUpdate = (from row in entities.ComplianceInstances
                                                                         where row.CustomerBranchID == entry.CustomerBranchID
                                                                         && row.ComplianceId == entry.ComplianceId
                                                                         select row).FirstOrDefault();

                        complianceInstanceToUpdate.Cer_OwnerUserID = entry.Cer_OwnerUserID;
                        complianceInstanceToUpdate.Cer_OfficerUserID = entry.Cer_OfficerUserID;

                        entities.SaveChanges();

                    });

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UserOwnerExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>)Cache["OwnerUserData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UserOfficerExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>)Cache["OfficerUserData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public void GetCertificateOfficerUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var OfficerUserList = HttpContext.Current.Cache["OfficerUserData"];

            if (OfficerUserList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID == CustomerID && row.Cer_OfficerRoleID == 1
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("OfficerUserData", Records); // add it to cache
                }
            }
        }
        public void GetCertificateOwnerUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var OwnerUserList = HttpContext.Current.Cache["OwnerUserData"];

            if (OwnerUserList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID == CustomerID && row.Cer_OwnerRoleID == 1
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("OwnerUserData", Records); // add it to cache
                }
            }
        }
        public static void GetStatutoryCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();

            var ComplianceList = HttpContext.Current.Cache["EComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Compliances
                               where row.IsDeleted == false
                               && row.Status == null
                               && row.ComplinceVisible == true
                               && row.IsMapped != false
                               && row.ComplianceType != 1
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EComplianceListData", Records); // add it to cache
                }
            }
        }
        #region Add Process      
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(MasterFileUpload.FileName);
                            MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentSheetsExitsts(xlWorkbook, "UploadComplianceAssignment");
                                        if (flag == true)
                                        {
                                            int customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                                            int complianceProductType = 0;
                                            complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);

                                            if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ComplianceListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("UserListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("UserListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("DepartmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("DepartmentListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OwnerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OfficerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("ComplianceInstanceAssignmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ComplianceInstanceAssignmentListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("HRComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("HRComplianceListData");
                                            }

                                            GetCompliance();
                                            GetUser(customerID);
                                            GetCustomerBranch(customerID);
                                            GetDepartment(customerID);
                                            GetCertificateOfficerUser(customerID);
                                            GetCertificateOwnerUser(customerID);
                                            GetComplianceInstanceAssignment(customerID);

                                            if (complianceProductType >= 2) //0-AVACOM, 1-TL, 2-HR Only, 3-AVACOM+TL, 4-AVACOM+HR 
                                            {
                                                GetHRCompliances();
                                            }

                                            ComplianceAssignmentData(complianceProductType, xlWorkbook);

                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("UserListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("UserListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("DepartmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("DepartmentListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OwnerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OfficerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("ComplianceInstanceAssignmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ComplianceInstanceAssignmentListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("HRComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("HRComplianceListData");
                                                }
                                            }                                            
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;                                            
                                            cvDuplicateEntry.ErrorMessage = "Please check the sheet name, Sheet name must be 'UploadComplianceAssignment'";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool ComplianceAssignmentSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignment"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private bool ComplianceAssignmentCheckListSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignmentCheckList"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        public partial class TempTable
        {          
            public long ComplianceId { get; set; }
            public int CustomerBranchID { get; set; }
            public long Performerid { get; set; }
            public string SequenceID { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {          
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void ComplianceAssignmentData(int complianceProductType, ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadComplianceAssignment"];
                if (xlWorksheet != null)
                {
                    int count = 1;                   
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();                    
                    List<TempAssignmentTableCheckList> TempassignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<string> errorMessage = new List<string>();
                    List<TempTable> lstTemptable = new List<TempTable>();

                    #region Validations
                    int valComplianceID = -1;
                    int valPerformerID = -1;
                    int valReviewerID = -1;
                    int valApproverID = -1;
                    int valCustomerBranchID = -1;
                    int valCar_OfficerID = -1;
                    int valCae_OwnerID = -1;
                    string valIsCheckList = string.Empty;
                    int valDepartmentID = -1;
                    string valSequence = string.Empty;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valComplianceID = -1;
                        valPerformerID = -1;
                        valReviewerID = -1;
                        valApproverID = -1;
                        valCar_OfficerID = -1;
                        valCae_OwnerID = -1;
                        valCustomerBranchID = -1;
                        valIsCheckList = string.Empty;
                        valDepartmentID = -1;
                        valSequence = string.Empty;
                        #region 1 ComplianceID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valComplianceID == -1 || valComplianceID == 0)
                        {
                            errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                        }
                        else
                        {
                            if (ComplainceExists(valComplianceID) == false)
                            {
                                errorMessage.Add("ComplianceID not defined in the System or It is Event Based or Informative Compliance at row number-" + rowNum);
                            }
                            else
                            {
                                //HRCompliance Mapping Check
                                if (complianceProductType >= 2)
                                {
                                    if (HRComplianceExists(valComplianceID))
                                    {
                                        errorMessage.Add("ComplianceID tagged to HR Product, This Compliance can be Assign/Activate using Menu--> Setup HR+ --> Compliance Assignment, check at row number-" + rowNum);
                                    }
                                }

                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                                {
                                    valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                                    if (!BranchIDExists(valCustomerBranchID) == false)
                                    {
                                        //Added by rahul on 10 FEB 2020 to add multipal complianceid at same location
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                                        {
                                            valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                                        }
                                        if (valPerformerID != -1 || valPerformerID != 0)
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString()))
                                            {
                                                valSequence = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text);
                                            }

                                            if (!lstTemptable.Any(x => x.ComplianceId == valComplianceID
                                            && x.CustomerBranchID == valCustomerBranchID 
                                            && x.SequenceID == valSequence))
                                            {
                                                TempTable tt = new TempTable();
                                                tt.ComplianceId = valComplianceID;
                                                tt.CustomerBranchID = valCustomerBranchID;
                                                tt.Performerid = valPerformerID;
                                                tt.SequenceID = valSequence;
                                                lstTemptable.Add(tt);
                                            }
                                            else
                                            {
                                                errorMessage.Add("Compliance with this ComplianceID (" + valComplianceID + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                            }

                                        }
                                    }                              
                                }
                                valIsCheckList = CheckISCheckListOrComplianceExists(valComplianceID);
                                if (string.IsNullOrEmpty(valIsCheckList))
                                {
                                    errorMessage.Add("Please check the compliance type in master at row number -" + rowNum);
                                }
                            }
                        }
                        #endregion                        

                        #region Check Compliance Schedule               
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString()))
                        {
                            if (valComplianceID != 0 || valComplianceID != -1)
                            {
                                valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                                Boolean checkflag = GetCompliancePresent(valComplianceID);
                                if (checkflag == false)
                                {
                                    errorMessage.Add("Please Correct the Display Schedule ComplianceID -" + valComplianceID + " at row number-" + rowNum);                                    
                                }                                
                            }
                        }
                        #endregion

                        #region 2 PerformerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);                                                       
                        }
                        if (valPerformerID == -1 || valPerformerID == 0)
                        {
                            errorMessage.Add("Required PerformerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valPerformerID) == false)
                            {
                                errorMessage.Add("PerformerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 3 ReviewerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                        {
                            valReviewerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                        }
                        if (valReviewerID == -1 || valReviewerID == 0)
                        {
                            errorMessage.Add("Required ReviewerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valReviewerID) == false)
                            {
                                errorMessage.Add("ReviewerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 4 ApproverID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString()))
                        {
                            valApproverID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 4].Text);

                            if (valApproverID != -1 || valApproverID != 0)
                            {
                                if (UserExists(valApproverID) == false)
                                {
                                    errorMessage.Add("ApproverID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }
                                             
                        #endregion

                        #region 5 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);                           
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);                              
                            }
                        }
                        #endregion
                        
                        #region 6 Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                        {
                            if (CheckInt(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                            {
                                valDepartmentID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text);                                
                            }                                              
                        }
                        if (valDepartmentID != -1 && valDepartmentID != 0)
                        {
                            if (DepartmentExists(valDepartmentID) == false)
                            {
                                errorMessage.Add("DepartmentID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 7 OwnerID OfficerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString()))
                        {
                            valCae_OwnerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 7].Text);
                            if (valCae_OwnerID != -1 && valCae_OwnerID != 0)
                            {
                                if (UserOwnerExists(valCae_OwnerID) == false)
                                {
                                    errorMessage.Add("OwnerID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString()))
                        {
                            valCar_OfficerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 8].Text);
                            if (valCar_OfficerID != -1 && valCar_OfficerID != 0)
                            {
                                if (UserOfficerExists(valCar_OfficerID) == false)
                                {
                                    errorMessage.Add("OfficerID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }
                        #endregion

                        #region 8 Label
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString()))
                        {
                            valSequence = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text);
                        }
                        if (!string.IsNullOrEmpty(valSequence))
                        {
                            if (valSequence.Length > 24)
                            {
                                //errorMessage.Add("Label Length ('" + valSequence + "') exceed  at row number-" + rowNum);
                                errorMessage.Add("Label Length ('" + valSequence + "') exceed, it should be less than 25 characters  at row number-" + rowNum);
                            }
                            if (AssignmentExists(valCustomerBranchID,valComplianceID,valSequence))
                            {
                                errorMessage.Add("Label Assignment ('" + valSequence + "') already in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int ComplianceID = -1;
                            int PerformerID = -1;
                            int ReviewerID = -1;
                            int ApproverID = -1;
                            int OfficerID = -1;
                            int OwnerID = -1;
                            int CustomerBranchID = -1;
                            string IsCheckList = string.Empty;
                            int DepartmentID = -1;
                            string SequenceID= string.Empty;
                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region PerformerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                PerformerID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region ReviewerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                            {
                                ReviewerID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                            }
                            #endregion

                            #region ApproverID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                            {
                                ApproverID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text);
                            }
                            #endregion

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 5].Text);
                            }
                            #endregion

                            #region IsCheckList

                            IsCheckList=CheckISCheckListOrComplianceExists(ComplianceID);
                            #endregion

                            #region Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString()))
                            {
                                DepartmentID = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text);
                            }
                            #endregion

                            #region OwnerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString()))
                            {
                                OwnerID = Convert.ToInt32(xlWorksheet.Cells[i, 7].Text);
                            }
                            #endregion

                            #region OfficerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString()))
                            {
                                OfficerID = Convert.ToInt32(xlWorksheet.Cells[i, 8].Text);
                            }
                            #endregion

                            #region Sequence
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString()))
                            {
                                SequenceID = Convert.ToString(xlWorksheet.Cells[i, 9].Text);
                            }
                            #endregion

                            if (IsCheckList == "Y")
                            {
                                #region Checklist
                                if ((Business.ComplianceManagement.ExistsCheckList(ComplianceID, CustomerBranchID, SequenceID)))
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        //var cunt = (from row in entities.TempAssignmentTableCheckLists
                                        //            where row.ComplianceId == ComplianceID
                                        //            && row.CustomerBranchID == CustomerBranchID
                                        //             && row.RoleID == 3
                                        //            select row).OrderByDescending(x => x.SequenceID).Take(1).Select(A => A.SequenceID).FirstOrDefault();
                                        //if (cunt == 0 || cunt == null)
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //    && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();
                                        //    if (tempcnt == 0)
                                        //    {
                                        //        cunt = 1;
                                        //    }
                                        //    else
                                        //    {
                                        //        cunt = tempcnt + 1;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //                 && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();

                                        //    cunt += tempcnt + 1;

                                        //}
                                        TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                        TempAssP.ComplianceId = ComplianceID;
                                        TempAssP.CustomerBranchID = CustomerBranchID;
                                        TempAssP.RoleID = 3;
                                        TempAssP.UserID = PerformerID;
                                        TempAssP.IsActive = true;
                                        TempAssP.CreatedOn = DateTime.Now;

                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempAssP.SequenceID = SequenceID;
                                        TempassignmentTableCheckList.Add(TempAssP);

                                        TempAssignmentTableCheckList TempAssR = new TempAssignmentTableCheckList();
                                        TempAssR.ComplianceId = ComplianceID;
                                        TempAssR.CustomerBranchID = CustomerBranchID;
                                        TempAssR.RoleID = 4;
                                        TempAssR.UserID = ReviewerID;
                                        TempAssR.IsActive = true;
                                        TempAssR.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssR.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssR.DepartmentID = DepartmentID;
                                        }
                                        TempAssR.SequenceID = SequenceID;
                                        TempassignmentTableCheckList.Add(TempAssR);

                                        if (ApproverID != -1)
                                        {
                                            TempAssignmentTableCheckList TempAssA = new TempAssignmentTableCheckList();
                                            TempAssA.ComplianceId = ComplianceID;
                                            TempAssA.CustomerBranchID = CustomerBranchID;
                                            TempAssA.RoleID = 6;
                                            TempAssA.UserID = ApproverID;
                                            TempAssA.IsActive = true;
                                            TempAssA.CreatedOn = DateTime.Now;
                                            if (DepartmentID == -1 || DepartmentID == 0)
                                            {
                                                TempAssA.DepartmentID = null;
                                            }
                                            else
                                            {
                                                TempAssA.DepartmentID = DepartmentID;
                                            }
                                            TempAssA.SequenceID = SequenceID;
                                            TempassignmentTableCheckList.Add(TempAssA);
                                        }
                                    }
                                }//exists end
                                #endregion
                            }
                            else
                            {
                                if ((Business.ComplianceManagement.Exists(ComplianceID, CustomerBranchID, SequenceID)))
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {                                       
                                      
                                        //var cunt = (from row in entities.TempAssignmentTables
                                        //            where row.ComplianceId == ComplianceID
                                        //            && row.CustomerBranchID == CustomerBranchID
                                        //             && row.RoleID == 3
                                        //            select row).OrderByDescending(x => x.SequenceID).Take(1).Select(A => A.SequenceID).FirstOrDefault();
                                        //if (cunt == 0 || cunt==null)
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //    && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();
                                        //    if (tempcnt == 0)
                                        //    {
                                        //        cunt = 1;
                                        //    }
                                        //    else
                                        //    {
                                        //        cunt = tempcnt+1;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //                 && x.CustomerBranchID == CustomerBranchID && x.RoleID==3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();

                                        //    cunt += tempcnt+1;

                                        //}                                                                                                                      
                                        TempAssignmentTable TempAssP = new TempAssignmentTable();
                                        TempAssP.ComplianceId = ComplianceID;
                                        TempAssP.CustomerBranchID = CustomerBranchID;
                                        TempAssP.RoleID = 3;
                                        TempAssP.UserID = PerformerID;
                                        TempAssP.Cer_OwnerUserID = OwnerID;
                                        TempAssP.Cer_OfficerUserID = OfficerID;
                                        TempAssP.IsActive = true;
                                        TempAssP.CreatedOn = DateTime.Now;
                                        TempAssP.DepartmentID = DepartmentID;
                                        TempAssP.SequenceID = SequenceID;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempassignmentTableList.Add(TempAssP);

                                        TempAssignmentTable TempAssR = new TempAssignmentTable();
                                        TempAssR.ComplianceId = ComplianceID;
                                        TempAssR.CustomerBranchID = CustomerBranchID;
                                        TempAssR.RoleID = 4;
                                        TempAssR.UserID = ReviewerID;
                                        TempAssR.Cer_OwnerUserID = OwnerID;
                                        TempAssR.Cer_OfficerUserID = OfficerID;
                                        TempAssR.IsActive = true;
                                        TempAssR.CreatedOn = DateTime.Now;
                                        TempAssR.SequenceID = SequenceID;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssR.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssR.DepartmentID = DepartmentID;
                                        }
                                        TempassignmentTableList.Add(TempAssR);
                                        if (ApproverID != -1)
                                        {
                                            TempAssignmentTable TempAssA = new TempAssignmentTable();
                                            TempAssA.ComplianceId = ComplianceID;
                                            TempAssA.CustomerBranchID = CustomerBranchID;
                                            TempAssA.RoleID = 6;
                                            TempAssA.UserID = ApproverID;
                                            TempAssA.Cer_OwnerUserID = OwnerID;
                                            TempAssA.Cer_OfficerUserID = OfficerID;
                                            TempAssA.IsActive = true;
                                            TempAssA.CreatedOn = DateTime.Now;
                                            TempAssA.SequenceID = SequenceID;
                                            if (DepartmentID == -1 || DepartmentID == 0)
                                            {
                                                TempAssA.DepartmentID = null;
                                            }
                                            else
                                            {
                                                TempAssA.DepartmentID = DepartmentID;
                                            }
                                            TempassignmentTableList.Add(TempAssA);
                                        }
                                    }
                                }//exists end
                            }
                        }
                        #endregion

                        if (TempassignmentTableList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTable(TempassignmentTableList);
                        }
                        if (suucess)
                        {
                            if (TempassignmentTableCheckList.Count > 0)
                            {
                                suucess = CreateExcelTempAssignmentTableCheckList(TempassignmentTableCheckList);
                            }
                        }
                        else if (suucess ==false && TempassignmentTableCheckList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTableCheckList(TempassignmentTableCheckList);
                        }
                        else
                        {
                            suucess = false;
                        }
                    }        
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void GetDepartment(int customerID)
        {
            List<ComplianceManagement.Business.Data.Department> Records = new List<ComplianceManagement.Business.Data.Department>();

            var DepartmentList = HttpContext.Current.Cache["DepartmentListData"];

            if (DepartmentList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Departments
                               where row.IsDeleted == false 
                               && row.CustomerID == customerID                                                        
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("DepartmentListData", Records); // add it to cache
                }
            }
        }
        public bool DepartmentExists(int deptid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Department>)Cache["DepartmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == deptid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public static void GetCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();

            var ComplianceList = HttpContext.Current.Cache["ComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   Records = (from row in entities.Compliances
                              where row.IsDeleted==false
                              //&& row.EventFlag==null 
                              && row.Status ==null 
                              && row.ComplinceVisible ==true // Add 11 March 2019  
                               && row.IsMapped != false  //Added condition 19 NOV 2020                                              
                              select row).ToList();
                    
                    HttpContext.Current.Cache.Insert("ComplianceListData", Records); // add it to cache
                }               
            }
        }
        public static Boolean GetCompliancePresent(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = true;
                var Frequency = (from row in entities.Compliances
                                   where row.ID == ComplianceID
                                   && row.EventFlag != true
                                   select row.Frequency).FirstOrDefault();

                var RecordCount = (from row in entities.ComplianceSchedules
                           where row.ComplianceID == ComplianceID 
                           select row).ToList().Count();

                if (Frequency != null)
                {
                    //int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(Frequency));

                    int frequencyId =Convert.ToInt32(Frequency);

                    if (frequencyId == 0)  //Monthly
                    {
                        if (RecordCount != 12)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 1)  //Quarterly
                    {
                        if (RecordCount != 4)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 2)  //HalfYearly
                    {
                        if (RecordCount != 2)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 3)  //Annual
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 4)  //FourMonthly
                    {
                        if (RecordCount != 3)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 5)  //TwoYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 6)  //SevenYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 7)  //Daily
                    {
                        flag = true;
                    }
                    if (frequencyId == 8)  //Weekly
                    {
                        flag = true;
                    }
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
        }
        public void GetUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var ComplianceList = HttpContext.Current.Cache["UserListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID==CustomerID && row.RoleID !=19
                               && row.IsDeleted==false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("UserListData", Records); // add it to cache
                }
            }
        }
        public void GetCustomerBranch(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.CustomerBranch> Records = new List<ComplianceManagement.Business.Data.CustomerBranch>();

            var ComplianceList = HttpContext.Current.Cache["CustomerBranchListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.CustomerBranches
                               where row.CustomerID == CustomerID
                               && row.IsDeleted==false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("CustomerBranchListData", Records); // add it to cache
                }
            }
        }

        public void GetComplianceInstanceAssignment(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.SP_CheckSequence_Result> Records = new List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>();

            var ComplianceList = HttpContext.Current.Cache["ComplianceInstanceAssignmentListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.SP_CheckSequence(CustomerID,"S")                              
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("ComplianceInstanceAssignmentListData", Records); // add it to cache
                }
            }
        }
        private void ComplianceAssignmentCheckListData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadComplianceAssignmentCheckList"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int ComplianceID = -1;
                    int UserID = -1;
                    int RoleID = -1;
                    int CustomerBranchID = -1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTableCheckList> TempAssignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<TempAssignmentTableCheckList> TempAssignmentTableCheckList1 = new List<TempAssignmentTableCheckList>();
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region Compliance ID                     
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                        }
                        if (ComplianceID == 0 || ComplianceID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Task ID at row number - " + count + " or Task ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region UserID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                        {
                            UserID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                        }
                        if (UserID == 0 || UserID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the User ID at row number - " + count + " or User ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region Role
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                        {
                            RoleID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                        }
                        if (RoleID == 0 || RoleID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Role ID at row number - " + count + " or Role ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                        {
                            CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text);
                        }
                        if (CustomerBranchID == 0 || CustomerBranchID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the CustomerBranch ID at row number - " + count + " or CustomerBranch ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion


                        if (!(ComplianceManagement.Business.ComplianceManagement.ExistsCheckList(ComplianceID, CustomerBranchID,null)))
                        {
                            if (RoleID == 3)
                            {
                                TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                TempAssP.ComplianceId = ComplianceID;
                                TempAssP.CustomerBranchID = CustomerBranchID;
                                TempAssP.RoleID = 3;
                                TempAssP.UserID = UserID;
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssignmentTableCheckList.Add(TempAssP);
                            }
                            if (RoleID == 4)
                            {
                                TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                TempAssP.ComplianceId = ComplianceID;
                                TempAssP.CustomerBranchID = CustomerBranchID;
                                TempAssP.RoleID = 4;
                                TempAssP.UserID = UserID;
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssignmentTableCheckList.Add(TempAssP);
                            }
                        }//exists end
                    }
                    TempAssignmentTableCheckList1 = TempAssignmentTableCheckList.Where(entry => entry.RoleID == 0 || entry.UserID == 0 || entry.ComplianceId == 0 || entry.CustomerBranchID == 0).ToList();
                    if (TempAssignmentTableCheckList1.Count == 0)
                    {
                        suucess = CreateExcelTempAssignmentTableCheckList(TempAssignmentTableCheckList);
                        suucess = true;
                    }
                    else
                    {
                        suucess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public  bool CreateExcelTempAssignmentTableCheckList(List<TempAssignmentTableCheckList> TempsssignmentTableCheckList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempsssignmentTableCheckList.ForEach(entry =>
                    {
                        var cunt = (from row in entities.TempAssignmentTableCheckLists
                                    where row.ComplianceId == entry.ComplianceId
                                    && row.CustomerBranchID == entry.CustomerBranchID
                                    select row).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();
                        if (cunt == 0)
                        {
                            cunt = 1;
                        }
                        //entry.SequenceID = cunt;
                        entities.TempAssignmentTableCheckLists.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public  bool CreateExcelTempAssignmentTable(List<TempAssignmentTable> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        
                        entities.TempAssignmentTables.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ComplainceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>) Cache["ComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UserExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>) Cache["UserListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool BranchIDExists(int Branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.CustomerBranch>)Cache["CustomerBranchListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Branchid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool AssignmentExists(long Branchid,long ComplianceiD,string Sequence)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>) Cache["ComplianceInstanceAssignmentListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.CustomerBranchID == Branchid  
                                 && row.ComplianceId == ComplianceiD
                                 &&  row.SequenceID == Sequence
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public  string  CheckISCheckListOrComplianceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ComplianceType).FirstOrDefault();
                    if (query == 1)
                    {
                        return "Y";
                    }
                    else
                    {
                        return "N";
                    }
                }
                else
                {
                    return "";
                }                             
            }
        }

        public static void GetHRCompliances()
        {
            List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

            var inCacheHRCompliancesList = HttpContext.Current.Cache["HRComplianceListData"];

            if (inCacheHRCompliancesList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                     where row.IsDeleted == false                                     
                                     && row.EventFlag == null && row.Status == null
                                     select row).ToList();

                    HttpContext.Current.Cache.Insert("HRComplianceListData", hrCompliances); // add it to cache
                }
            }
        }

        public bool HRComplianceExists(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_RLCS_RegisterReturnChallanCompliance_Result>)Cache["HRComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == complianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion       
    }
}