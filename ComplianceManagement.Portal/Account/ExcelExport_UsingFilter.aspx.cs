﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ExcelExport_UsingFilter : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["GridfillValue"] = null;
                ViewState["GridfillValue"] = "A";
                BindActs(ddlfilterAct);               
                BindIndustrys(ddlFilterIndustry);
                BindLegalEntityTypes(ddlLegalEntityType);   
                BindGridviewCompliance("A","");               
            }
        }
        public static List<ComplianceViewExport> GetAllActSList(int Actid, string IFlag, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IFlag == "All")
                {
                    var complianceList = (from row in entities.ComplianceViewExports                                          
                                          select row);

                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();
                }
                else
                {
                    var complianceList = (from row in entities.ComplianceViewExports
                                          where row.ActID == Actid
                                          select row);

                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();
                }
               
            }
        }
        public static List<ComplianceViewExport> GetAllIndustrySList(int Actid, int industryId,string IFLAG, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IFLAG == "All")
                {
                    var complianceList = (from row in entities.ComplianceViewExports
                                          join row1 in entities.IndustryMappings
                                          on row.ID equals row1.ComplianceId
                                          //where row.ActID == Actid && row1.IndustryID == industryId
                                          select row).Distinct();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();
                }
                else
                {
                    var complianceList = (from row in entities.ComplianceViewExports
                                          join row1 in entities.IndustryMappings
                                          on row.ID equals row1.ComplianceId
                                          where row.ActID == Actid && row1.IndustryID == industryId
                                          select row).Distinct();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();
                }
            }
        }
        public static List<ComplianceViewExport> GetAllLegalEntityTypeSList(int Actid, int legalentitytypeid, string IFLAG, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IFLAG == "All")
                {
                    var complianceList = (from row in entities.ComplianceViewExports
                                          join row1 in entities.LegalEntityTypeMappings
                                          on row.ID equals row1.ComplianceId                                          
                                          select row).Distinct();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();
                }
                else
                {

                    var complianceList = (from row in entities.ComplianceViewExports
                                          join row1 in entities.LegalEntityTypeMappings
                                          on row.ID equals row1.ComplianceId
                                          where row.ActID == Actid && row1.LegalEntityTypeID == legalentitytypeid
                                          select row).Distinct();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        complianceList = complianceList.Where(entry => entry.ActName.Contains(filter) || entry.Description.Contains(filter) || entry.ShortDescription.Contains(filter) || entry.Frequency.Contains(filter) || entry.RequiredForms.Contains(filter) || entry.Sections.Contains(filter));
                    }
                    return complianceList.ToList();

                }
            }
        }     
        protected void BindGridviewCompliance(string Flag , string filterflag)
        {
            try
            {
                if (Flag == "A")
                {
                    if (ddlfilterAct.SelectedItem.Text == "< ALL >")
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllActSList(Convert.ToInt32(ddlfilterAct.SelectedValue), "All");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllActSList(Convert.ToInt32(ddlfilterAct.SelectedValue), "All", tbxFilter.Text);
                        }
                    }
                    else
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllActSList(Convert.ToInt32(ddlfilterAct.SelectedValue),"");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllActSList(Convert.ToInt32(ddlfilterAct.SelectedValue),"", tbxFilter.Text);
                        }
                        
                    }
                    grdAct.DataBind();
                    upActList.Update();
                }
                else if (Flag == "I")
                {
                    if (ddlfilterAct.SelectedItem.Text == "< ALL >" && ddlFilterIndustry.SelectedItem.Text == "< ALL >")
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllIndustrySList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlFilterIndustry.SelectedValue),"All");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllIndustrySList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlFilterIndustry.SelectedValue),"All", tbxFilter.Text);
                        } 
                    }
                    else
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllIndustrySList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlFilterIndustry.SelectedValue),"");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllIndustrySList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlFilterIndustry.SelectedValue),"", tbxFilter.Text);
                        }                       
                    }
                    grdAct.DataBind();
                    upActList.Update();
                }
                else if (Flag == "L")
                {
                    if (ddlfilterAct.SelectedItem.Text == "< ALL >" && ddlLegalEntityType.SelectedItem.Text == "< ALL >")
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllLegalEntityTypeSList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlLegalEntityType.SelectedValue), "All");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllLegalEntityTypeSList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlLegalEntityType.SelectedValue), "All", tbxFilter.Text);
                        }
                        
                    }
                    else
                    {
                        if (filterflag == "")
                        {
                            grdAct.DataSource = GetAllLegalEntityTypeSList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlLegalEntityType.SelectedValue), "");
                        }
                        else
                        {
                            grdAct.DataSource = GetAllLegalEntityTypeSList(Convert.ToInt32(ddlfilterAct.SelectedValue), Convert.ToInt32(ddlLegalEntityType.SelectedValue), "", tbxFilter.Text);
                        }
                    }
                    grdAct.DataBind();
                    upActList.Update();
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindIndustrys(DropDownList ddlList)
        {
            try
            {
                ddlList.Items.Clear();
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";
                ddlList.DataSource = ComplianceTypeManagement.GetAllIndustrys();
                ddlList.DataBind();
                ddlList.Items.Insert(0, new ListItem("< ALL >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindActs(DropDownList ddlList)
        {
            try
            {
                ddlList.Items.Clear();
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";
                ddlList.DataSource = ComplianceTypeManagement.GetAllACTS();
                ddlList.DataBind();
                ddlList.Items.Insert(0, new ListItem("< ALL >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLegalEntityTypes(DropDownList ddlList)
        {
            try
            {
                ddlList.Items.Clear();
                ddlList.DataTextField = "EntityTypeName";
                ddlList.DataValueField = "ID";
                ddlList.DataSource = ComplianceTypeManagement.GetAllM_LegalEntityTypes();
                ddlList.DataBind();
                ddlList.Items.Insert(0, new ListItem("< ALL >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }             
        protected void grdAct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAct.PageIndex = e.NewPageIndex;
                BindGridviewCompliance(ViewState["GridfillValue"].ToString(),"");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdAct.PageIndex = 0;
                if (tbxFilter.Text != "")
                {
                    BindGridviewCompliance(ViewState["GridfillValue"].ToString(),"Y");
                }
                else
                {
                    BindGridviewCompliance(ViewState["GridfillValue"].ToString(),"");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upAct_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlfilterAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbxFilter.Text != "")
                {
                    BindGridviewCompliance("A", "Y");
                }
                else
                {
                    BindGridviewCompliance("A", "");
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["GridfillValue"] = null;
                ViewState["GridfillValue"] = "I";
                if (tbxFilter.Text != "")
                {
                    BindGridviewCompliance("I", "Y");
                }
                else
                {
                    BindGridviewCompliance("I", "");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlLegalEntityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["GridfillValue"] = null;
                ViewState["GridfillValue"] = "L";
                if (tbxFilter.Text != "")
                {
                    BindGridviewCompliance("I", "L");
                }
                else
                {
                    BindGridviewCompliance("I", "");
                }
                             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.ClearContent();
                Response.Buffer = true;
                //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Compliances.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                grdAct.AllowPaging = false;
                BindGridviewCompliance(ViewState["GridfillValue"].ToString(), "");
                //Change the Header Row back to white color
                grdAct.HeaderRow.Style.Add("background-color", "#FFFFFF");
                //Applying stlye to gridview header cells
                for (int i = 0; i < grdAct.HeaderRow.Cells.Count; i++)
                {
                    grdAct.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                }
                grdAct.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}