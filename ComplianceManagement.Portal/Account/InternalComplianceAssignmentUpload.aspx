﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="InternalComplianceAssignmentUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.InternalComplianceAssignmentUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <table width="100%" align="left">
                <tr>
                    <td>
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                 <div id="divcustomer" runat="server" visible="false">
                                <tr>
                                   
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Select Customer :"></asp:Label>
                                    </td>
                                    <td>
                                          <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                            CssClass="txtbox" />
                                    </td>
                                  
                                </tr>
                                       </div>
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div style="display:none;">
                                            <asp:RadioButton ID="rdoAssignCompliance" runat="server" Checked="true" AutoPostBack="false" Text="Assign Compliance" GroupName="uploadContentGroup" /><br />
                                        </div>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="display:none;">
                                        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
