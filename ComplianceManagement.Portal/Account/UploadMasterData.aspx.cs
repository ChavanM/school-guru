﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadMasterData : System.Web.UI.Page
    {
        bool suucess = false;
        bool saveflag =true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                lblMessage1.Text = string.Empty;
                LblErormessage1.Text = string.Empty;
                Tab1_Click(sender, e);
            }
        }

        #region Add Compliance
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {

            if (MasterFileUpload.HasFile)
            {
                try
                {
                    //bool suucess = false;
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoAct.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Act");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No data found in file.";

                                }
                            }
                            else if (rdoCompliance.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Compliances");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    ProcessComplianceData(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No data found in file.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";

                            }

                            if (suucess == true)//commented by Manisha
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }
        private bool ActSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("Act"))
                    {
                        if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (data.Equals("Compliances"))
                    {
                        if (!sheet.Name.Equals("Checklist"))
                        {
                            if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Compliances"))
                            {
                                flag = true;
                                break;//added by Manisha
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (data.Equals("Checklist"))
                    {
                        if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Checklist"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                lblMessage1.Text = "";
                MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        private void ProcessActData(ExcelPackage xlWorkbook)
        {
            try
            {

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Compliance Types"];
                if (xlWorksheet != null)
                {
                    int xlrow = xlWorksheet.Dimension.End.Row;

                    List<ComplianceType> complianceTypeList = new List<ComplianceType>();

                    for (int i = 2; i <= xlrow - 2; i++)
                    {

                        if (!(ComplianceTypeManagement.Exists(Convert.ToString(xlWorksheet.Cells[i, 1].Value))))
                        {
                            ComplianceType complianceType = new ComplianceType();
                            complianceType.Name = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            complianceType.Description = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            complianceTypeList.Add(complianceType);
                        }
                    }
                    ComplianceTypeManagement.Create(complianceTypeList);
                }

                xlWorksheet = xlWorkbook.Workbook.Worksheets["Compliance Categories"];

                if (xlWorksheet != null)
                {
                    int xlrow1 = xlWorksheet.Dimension.End.Row;
                    List<ComplianceCategory> complianceCategoryList = new List<ComplianceCategory>();

                    for (int i = 2; i <= xlrow1; i++)
                    {
                        if (!(ComplianceCategoryManagement.Exists(Convert.ToString(xlWorksheet.Cells[i, 1].Value))))
                        {
                            ComplianceCategory complianceCategory = new ComplianceCategory();

                            complianceCategory.Name = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            complianceCategory.Description = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                            complianceCategoryList.Add(complianceCategory);
                        }
                    }
                    ComplianceCategoryManagement.Create(complianceCategoryList);
                }

                xlWorksheet = xlWorkbook.Workbook.Worksheets["Act"];
                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<Act> actList = new List<Act>();
                    List<Act> actList1 = new List<Act>();

                    //for (int i = 4; i <= xlrow2 - 2; i++)
                    for (int i = 2; i <= xlrow2; i++)
                    {

                        if (!(ActManagement.Exists(Convert.ToString(xlWorksheet.Cells[i, 1].Value))))
                        {
                            Act act = new Act();
                            act.Name = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            act.Description = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            act.ComplianceTypeId = ComplianceTypeManagement.GetIdByName(Convert.ToString(xlWorksheet.Cells[i, 2].Value));
                            act.ComplianceCategoryId = ComplianceCategoryManagement.GetIdByName(Convert.ToString(xlWorksheet.Cells[i, 3].Value));

                            act.CreatedOn = DateTime.UtcNow;
                            act.IsDeleted = false;
                            if (xlWorksheet.Cells[i, 4].Value != null)
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 2].Value) == "State")
                                {
                                    if (xlWorksheet.Cells[i, 4].Value != null)
                                    {

                                        act.State = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                                        act.StateID = StateCityManagement.GetStateIDByName(xlWorksheet.Cells[i, 4].Value.ToString());
                                    }
                                }

                                //if (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Local (City Specific)" || Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Local (Panchayat Level")
                                //{
                                if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 5].Value) != ""))
                                {
                                    act.State = xlWorksheet.Cells[i, 4].Value.ToString();
                                    act.StateID = StateCityManagement.GetStateIDByName(xlWorksheet.Cells[i, 4].Value.ToString());

                                    act.City = xlWorksheet.Cells[i, 5].Value.ToString();
                                    act.CityID = StateCityManagement.GetCityIdByName(xlWorksheet.Cells[i, 5].Value.ToString());
                                }
                            }
                            actList.Add(act);
                        }

                    }
                    actList1 = actList.Where(entry => entry.ComplianceCategoryId == 0).ToList();
                    ActManagement.Create(actList);
                    suucess = true;
                }

            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;

                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        private void ProcessComplianceData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Compliances"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList1 = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<Tuple<int, string, string>> specialDueDate = new List<Tuple<int, string, string>>();             
                for (int i = 2; i <= filledrow; i++)
                {                   
                    com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        complianceData.ActID = ActManagement.GetIdByName(Convert.ToString(xlWorksheet.Cells[i, 1].Value));
                        complianceData.Sections = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                        {
                            complianceData.ComplianceType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                        {
                            complianceData.ComplianceType = 2;
                        }
                        else
                        {
                            complianceData.ComplianceType = 1;
                        }
                        complianceData.ShortDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                        complianceData.Description = Convert.ToString(xlWorksheet.Cells[i, 6].Value);                      
                        if (Convert.ToString(xlWorksheet.Cells[i, 8].Value).Trim().Equals("Yes"))
                        {
                            complianceData.UploadDocument = true;
                        }
                        else
                        {
                            complianceData.UploadDocument = false;
                        }
                        complianceData.RequiredForms = Convert.ToString(xlWorksheet.Cells[i, 9].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().Equals("High"))
                        {
                            complianceData.RiskType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().Equals("Medium"))
                        {
                            complianceData.RiskType = 1;
                        }
                        else
                        {
                            complianceData.RiskType = 2;
                        }
                        #region Event added by rahul on 27 Jan 2016
                            if (Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim()=="N")
                            {
                                complianceData.EventID = null;
                            }
                            else if (Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim() == "Y")
                            {                            
                                complianceData.EventID = com.VirtuosoITech.ComplianceManagement.Business.EventManagement.GETEVENTID(Convert.ToString(xlWorksheet.Cells[i, 12].Value));
                            }
                            if (Convert.ToString(xlWorksheet.Cells[i, 11].Value).Trim() == "Y")
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 12].Value).Trim() != "" && Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim() != "")
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[i, 12].Value).Trim() != null && Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim() != null)
                                    {
                                        complianceData.SubEventID = com.VirtuosoITech.ComplianceManagement.Business.EventManagement.GETEVENTandSUBEVENTid(Convert.ToString(xlWorksheet.Cells[i, 12].Value), Convert.ToString(xlWorksheet.Cells[i, 14].Value));
                                    }
                                }                           
                            }
                            if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Mandatory"))
                            {
                                complianceData.EventComplianceType = 0;
                            }
                            else if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Optional"))
                            {
                                complianceData.EventComplianceType = 1;
                            }    
                       #endregion
                        //region Compliance Type
                        if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                        {
                            complianceData.Frequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 16].Value)));
                            if (xlWorksheet.Cells[i, 17].Value != null && xlWorksheet.Cells[i, 17].Value.ToString().Trim() != "")
                            {
                                complianceData.DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 17].Value);
                            }
                            int step = 1;
                            switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 16].Value))))
                            {
                                case Frequency.Monthly:
                                    step = 1;
                                    break;
                                case Frequency.Quarterly:
                                    step = 3;
                                    break;
                                case Frequency.FourMonthly:
                                    step = 4;
                                    break;
                                case Frequency.HalfYearly:
                                    step = 6;
                                    break;
                                case Frequency.Annual:
                                    step = 12;
                                    break;
                                default:
                                    step = 12;
                                    break;
                            }
                            int specilMonth = 0;
                            int specialDay = 0;
                            if ((xlWorksheet.Cells[i, 18].Value != null && xlWorksheet.Cells[i, 19].Value != null) && (xlWorksheet.Cells[i, 18].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 19].Value.ToString().Trim() != ""))
                            {
                                specilMonth = Convert.ToInt32(xlWorksheet.Cells[i, 18].Value);
                                specialDay = Convert.ToInt32(xlWorksheet.Cells[i, 19].Value);
                            }
                            List<ComplianceSchedule> complianceSchedule = new List<ComplianceSchedule>();

                            int SpecialMonth;
                            #region Financial Year And Calender
                            string calflag = Convert.ToString(xlWorksheet.Cells[i, 37].Value);
                            if (calflag == "Y")
                            {
                                for (int month = 4; month <= 12; month += step)
                                {

                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.Frequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 10;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 8;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 8)
                                    {
                                        SpecialMonth = 12;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 3 && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 3 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if ((complianceData.Frequency == 5 || complianceData.Frequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                    if (complianceData.DueDate > lastdate)
                                    {                                     
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 17].Value != null && xlWorksheet.Cells[i, 17].Value.ToString().Trim() != "")
                                        {                      
                                            complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 17].Value).ToString("D2") + SpecialMonth.ToString("D2");
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 19].Value).ToString("D2") + Convert.ToInt32(xlWorksheet.Cells[i, 18].Value).ToString("D2");
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                        complianceData.ComplianceSchedules.Add(complianceShedule);
                                    }
                                    else
                                    {
                                        complianceData.ComplianceSchedules.Add(complianceShedule);
                                    }
                                }
                            }//Financial Year close
                            else
                            {
                                for (int month = 1; month <= 12; month += step)
                                {

                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.Frequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 7)
                                    {
                                        SpecialMonth = 1;
                                    }                                    
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 9)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if ((complianceData.Frequency == 3 || complianceData.Frequency == 5 || complianceData.Frequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                    if (complianceData.DueDate > lastdate)
                                    {
                                        //complianceShedule.SpecialDate = lastdate.ToString() + month.ToString("D2");
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {
                                        
                                        if (xlWorksheet.Cells[i, 17].Value != null && xlWorksheet.Cells[i, 17].Value.ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(xlWorksheet.Cells[i, 17].Value) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {                                                
                                                complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 17].Value).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(xlWorksheet.Cells[i, 17].Value) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 19].Value).ToString("D2") + Convert.ToInt32(xlWorksheet.Cells[i, 18].Value).ToString("D2");
                                            }
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        if (Convert.ToInt32(xlWorksheet.Cells[i, 17].Value) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                            complianceData.ComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(xlWorksheet.Cells[i, 17].Value) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceData.ComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                }
                            }//Calender Year Close

                            #endregion
                        }
                        #region Time Based
                        if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                        {


                            if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                            {
                                complianceData.SubComplianceType = 0;//fixed gap
                                complianceData.DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 17].Value);

                            }
                            else
                            {
                                complianceData.SubComplianceType = 1;//periodically
                                int step = 1;
                                complianceData.Frequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 16].Value)));
                                switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 16].Value))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                //int SpecialMonth;
                                List<ComplianceSchedule> complianceSchedule = new List<ComplianceSchedule>();
                                for (int month = 1; month <= 12; month += step)
                                {
                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;
                                    //}

                                    //if (subcomplincetype == 1 || subcomplincetype == 2)
                                    //{
                                    int monthCopy = month;
                                    if (monthCopy == 1)
                                    {
                                        monthCopy = 12;
                                        complianceShedule.ForMonth = monthCopy;
                                    }
                                    else
                                    {
                                        monthCopy = month - 1;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    //if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)//Annual,TwoYearly,SevenYearly
                                    if (Convert.ToString(xlWorksheet.Cells[i, 16].Value).Trim().Equals("Annual") || Convert.ToString(xlWorksheet.Cells[i, 16].Value).Trim().Equals("TwoYearly") || Convert.ToString(xlWorksheet.Cells[i, 16].Value).Trim().Equals("SevenYearly"))
                                    {
                                        monthCopy = 3;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                    complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                    //complianceData.ComplianceSchedules.Add(complianceShedule);
                                    complianceData.ComplianceSchedules.Add(complianceShedule);

                                    //}
                                }

                            }
                        }
                        #endregion Time Based
                        //end region Compliance Type
                        complianceData.NatureOfCompliance = Convert.ToByte(Convert.ToString(xlWorksheet.Cells[i, 7].Value).Trim());
                        if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Monetary"))
                        {
                            complianceData.NonComplianceType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Non-Monetary"))
                        {
                            complianceData.NonComplianceType = 1;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Both"))
                        {
                            complianceData.NonComplianceType = 2;
                        }

                        complianceData.FixedMinimum = Convert.ToDouble(xlWorksheet.Cells[i, 25].Value);
                        complianceData.FixedMaximum = Convert.ToDouble(xlWorksheet.Cells[i, 26].Value);
                        //---------------------------------------------------------------------------------
                        if (Convert.ToString(xlWorksheet.Cells[i, 27].Value).Trim().Equals("Day"))
                        {
                            complianceData.VariableAmountPerDay = Convert.ToDouble(xlWorksheet.Cells[i, 28].Value);
                        }
                        else
                        {
                            complianceData.VariableAmountPerMonth = Convert.ToDouble(xlWorksheet.Cells[i, 28].Value);
                        }
                        //---------------------------------------------------------------------------------
                        complianceData.VariableAmountPerDayMax = Convert.ToDouble(xlWorksheet.Cells[i, 29].Value);
                        //complianceData.VariableAmountPercent = Convert.ToDouble(xlWorksheet.Cells[i, 30].Value);
                        //complianceData.VariableAmountPercentMax = Convert.ToDouble(xlWorksheet.Cells[i, 31].Value);
                        complianceData.VariableAmountPercent = Convert.ToString(xlWorksheet.Cells[i, 30].Value);
                        complianceData.VariableAmountPercentMax = Convert.ToString(xlWorksheet.Cells[i, 31].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 32].Value).Trim().Equals("N"))
                        {
                            complianceData.Imprisonment = false;
                        }
                        else
                        {
                            complianceData.Imprisonment = true;
                        }
                        complianceData.Designation = Convert.ToString(xlWorksheet.Cells[i, 33].Value);
                        complianceData.MinimumYears = Convert.ToInt32(xlWorksheet.Cells[i, 34].Value);
                        complianceData.MaximumYears = Convert.ToInt32(xlWorksheet.Cells[i, 35].Value);
                        //----------------------------------------------------------------------------------
                        complianceData.NonComplianceEffects = Convert.ToString(xlWorksheet.Cells[i, 36].Value);//others in Non-Monetory
                        //----------------------------------------------------------------------------------
                        #region reminder
                        if (Convert.ToString(xlWorksheet.Cells[i, 20].Value).Trim().Equals("Standard"))
                        {
                            complianceData.ReminderType = 0;//standard
                        }
                        else
                        {
                            complianceData.ReminderType = 1;//custom
                            complianceData.ReminderBefore = Convert.ToInt32(xlWorksheet.Cells[i, 21].Value);
                            complianceData.ReminderGap = Convert.ToInt32(xlWorksheet.Cells[i, 22].Value);
                        }

                        #endregion reminder                 
                        complianceData.PenaltyDescription = Convert.ToString(xlWorksheet.Cells[i, 40].Value);
                        complianceData.ReferenceMaterialText = Convert.ToString(xlWorksheet.Cells[i, 41].Text);
                        complianceData.CreatedBy = 1;
                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;                     
                    }
                    complianceList.Add(complianceData);       
                }
                complianceList1 = complianceList.Where(entry => entry.ActID == 0).ToList();//to check that ActID should not be 0 in database
                string str = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int i = 0;
                    complianceList.ForEach(entry =>
                    {
                        i = i + 1;
                        bool chkexists =Business.ComplianceManagement.ExistsR(entry.Description, entry.ShortDescription, entry.ActID);
                        if(chkexists == true)
                        {
                            saveflag = false;  // Same compliance allready save in Sysyem
                            str += "Same compliance Short Description and Detailed Description allready saved in system -(Record - " + i + " )<br />";
                        }
                    });
                }
                
                if (saveflag == true)
                {
                    Business.ComplianceManagement.Create(complianceList);
                    suucess = true;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = str;//"Same compliance Short Description and Detailed Description allready saved in system -(Record -" + i + " )";
                    suucess = false;
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ProcessComplianceDataEvent(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Event based"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList1 = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<Tuple<int, string, string>> specialDueDate = new List<Tuple<int, string, string>>();
                for (int i = 2; i <= filledrow; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        complianceData.ActID = ActManagement.GetIdByName(Convert.ToString(xlWorksheet.Cells[i, 1].Value));
                        complianceData.Sections = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                        //if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                        //{
                            complianceData.ComplianceType = 0;
                        //}
                        //else if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                        //{
                        //    complianceData.ComplianceType = 2;
                        //}
                        //else
                        //{
                        //    complianceData.ComplianceType = 1;
                        //}
                        complianceData.ShortDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                        complianceData.Description = Convert.ToString(xlWorksheet.Cells[i, 6].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 8].Value).Trim().Equals("Yes"))
                        {
                            complianceData.UploadDocument = true;
                        }
                        else
                        {
                            complianceData.UploadDocument = false;
                        }
                        complianceData.RequiredForms = Convert.ToString(xlWorksheet.Cells[i, 9].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().Equals("High"))
                        {
                            complianceData.RiskType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 23].Value).Trim().Equals("Medium"))
                        {
                            complianceData.RiskType = 1;
                        }
                        else
                        {
                            complianceData.RiskType = 2;
                        }
        
                        if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Mandatory"))
                        {
                            complianceData.EventComplianceType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Optional"))
                        {
                            complianceData.EventComplianceType = 1;
                        }
                        complianceData.CreatedBy = 1;
                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.EventFlag = true;

                        complianceData.SampleFormLink = Convert.ToString(xlWorksheet.Cells[i, 12].Value);

                        if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Yes"))
                        {
                            complianceData.UpDocs = true;
                        }
                        else
                        {
                            complianceData.UpDocs = false;
                        }

                        if (Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim().Equals("Yes"))
                        {
                            complianceData.ComplinceVisible = true;
                        }
                        else
                        {
                            complianceData.ComplinceVisible = false;
                        }

                        complianceData.NatureOfCompliance = Convert.ToByte(Convert.ToString(xlWorksheet.Cells[i, 7].Value).Trim());
                        if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Monetary"))
                        {
                            complianceData.NonComplianceType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Non-Monetary"))
                        {
                            complianceData.NonComplianceType = 1;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Both"))
                        {
                            complianceData.NonComplianceType = 2;
                        }

                        complianceData.FixedMinimum = Convert.ToDouble(xlWorksheet.Cells[i, 25].Value);
                        complianceData.FixedMaximum = Convert.ToDouble(xlWorksheet.Cells[i, 26].Value);
                        //---------------------------------------------------------------------------------
                        if (Convert.ToString(xlWorksheet.Cells[i, 27].Value).Trim().Equals("Day"))
                        {
                            complianceData.VariableAmountPerDay = Convert.ToDouble(xlWorksheet.Cells[i, 28].Value);
                        }
                        else
                        {
                            complianceData.VariableAmountPerMonth = Convert.ToDouble(xlWorksheet.Cells[i, 28].Value);
                        }
                        //---------------------------------------------------------------------------------
                        complianceData.VariableAmountPerDayMax = Convert.ToDouble(xlWorksheet.Cells[i, 29].Value);
                        //complianceData.VariableAmountPercent = Convert.ToDouble(xlWorksheet.Cells[i, 30].Value);
                        //complianceData.VariableAmountPercentMax = Convert.ToDouble(xlWorksheet.Cells[i, 31].Value);
                        complianceData.VariableAmountPercent = Convert.ToString(xlWorksheet.Cells[i, 30].Value);
                        complianceData.VariableAmountPercentMax = Convert.ToString(xlWorksheet.Cells[i, 31].Value);
                        if (Convert.ToString(xlWorksheet.Cells[i, 32].Value).Trim().Equals("N"))
                        {
                            complianceData.Imprisonment = false;
                        }
                        else
                        {
                            complianceData.Imprisonment = true;
                        }
                        complianceData.Designation = Convert.ToString(xlWorksheet.Cells[i, 33].Value);
                        complianceData.MinimumYears = Convert.ToInt32(xlWorksheet.Cells[i, 34].Value);
                        complianceData.MaximumYears = Convert.ToInt32(xlWorksheet.Cells[i, 35].Value);
                        //----------------------------------------------------------------------------------
                        complianceData.NonComplianceEffects = Convert.ToString(xlWorksheet.Cells[i, 36].Value);//others in Non-Monetory
                        //----------------------------------------------------------------------------------
                        #region reminder
                        if (Convert.ToString(xlWorksheet.Cells[i, 20].Value).Trim().Equals("Standard"))
                        {
                            complianceData.ReminderType = 0;//standard
                        }
                        else
                        {
                            complianceData.ReminderType = 1;//custom
                            complianceData.ReminderBefore = Convert.ToInt32(xlWorksheet.Cells[i, 21].Value);
                            complianceData.ReminderGap = Convert.ToInt32(xlWorksheet.Cells[i, 22].Value);
                        }

                        #endregion reminder                 
                        complianceData.PenaltyDescription = Convert.ToString(xlWorksheet.Cells[i, 40].Value);
                        complianceData.ReferenceMaterialText = Convert.ToString(xlWorksheet.Cells[i, 41].Text);
                        complianceData.CreatedOn = DateTime.UtcNow;
                        complianceData.IsDeleted = false;
                    }
                    complianceList.Add(complianceData);
                }
                complianceList1 = complianceList.Where(entry => entry.ActID == 0).ToList();//to check that ActID should not be 0 in database
                //Business.ComplianceManagement.Create(complianceList);
                //suucess = true;

                string str = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int i = 0;
                    complianceList.ForEach(entry =>
                    {
                        i = i + 1;
                        bool chkexists = Business.ComplianceManagement.ExistsR(entry.Description, entry.ShortDescription, entry.ActID);
                        if (chkexists == true)
                        {
                            saveflag = false;  // Same compliance allready save in Sysyem
                            str += "Same compliance Short Description and Detailed Description allready saved in system -(Record - " + i + " )<br />";
                        }
                    });
                }

                if (saveflag == true)
                {
                    Business.ComplianceManagement.Create(complianceList);
                    suucess = true;
                }
                else
                {
                    cvDuplicateEntryEvent.IsValid = false;
                    cvDuplicateEntryEvent.ErrorMessage = str;//"Same compliance Short Description and Detailed Description allready saved in system -(Record -" + i + " )";
                    suucess = false;
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryEvent.IsValid = false;
                cvDuplicateEntryEvent.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Update Compliance
        protected void btnUploadFile1_Click(object sender, EventArgs e)
        {

            if (MasterFileUpload1.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload1.FileName);
                    MasterFileUpload1.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            ProcessUpdateComplianceData(xlWorkbook);
                        }
                        if (suucess == true)
                        {
                            cvDuplicateEntry1.IsValid = false;
                            cvDuplicateEntry1.ErrorMessage = "Data uploaded successfully.";

                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Error uploading file. Please try again.";

                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }
        private void ProcessUpdateComplianceData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ComplianceExported"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }

                for (int i = 2; i <= filledrow; i++)
                {
                    string complianceid = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                    string PenaltyDescription = Convert.ToString(xlWorksheet.Cells[i, 16].Value);
                    //Update PenaltyDescription
                    Business.ComplianceManagement.ModifyCompliancePenaltyDescription(Convert.ToInt32(complianceid), PenaltyDescription);

                    //Update IndustryList
                    Business.ComplianceManagement.UpdateIndustryMappedID(Convert.ToInt32(complianceid));

                    string[] IndustryList = null;
                    IndustryList = Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim().Split(',');
                    if (IndustryList.Length > 0)
                    {
                        foreach (string INDList in IndustryList)
                        {
                            if (INDList == "No")
                            {
                            }
                            else
                            {
                                if (INDList == "All")
                                {
                                    var vAllIndustryID = Business.ComplianceManagement.GetAllIndustryID();
                                    for (int r = 0; r <= vAllIndustryID.Count - 1; r++)
                                    {
                                        int Induid = Convert.ToInt32(vAllIndustryID[r].ToString());
                                        IndustryMapping IndustryMapping = new IndustryMapping()
                                        {
                                            ComplianceId = Convert.ToInt32(complianceid),
                                            IndustryID = Induid,
                                            IsActive = true,
                                            EditedDate = DateTime.UtcNow,
                                            EditedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                                    }
                                }
                                else
                                {
                                    int Induid = Business.ComplianceManagement.GetIndustryIDByIndustryName(INDList);
                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                    {
                                        ComplianceId = Convert.ToInt32(complianceid),
                                        IndustryID = Induid,
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                                }
                            }
                        }
                    }

                    //Update LegalEntityList
                    Business.ComplianceManagement.UpdateLegalEntityMappedID(Convert.ToInt32(complianceid));

                    string[] LegalEntityList = null;
                    LegalEntityList = Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim().Split(',');
                    if (LegalEntityList.Length > 0)
                    {
                        foreach (string LegalList in LegalEntityList)
                        {
                            if (LegalList == "No")
                            {
                            }
                            else
                            {
                                if (LegalList == "All")
                                {
                                    var vAllLegalEntityID = Business.ComplianceManagement.GetAllLegalEntityTypeID();
                                    for (int a = 0; a <= vAllLegalEntityID.Count - 1; a++)
                                    {
                                        int LegalEntityid = Convert.ToInt32(vAllLegalEntityID[a].ToString());
                                        LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                        {
                                            ComplianceId = Convert.ToInt32(complianceid),
                                            LegalEntityTypeID = LegalEntityid,
                                            IsActive = true,
                                            EditedDate = DateTime.UtcNow,
                                            EditedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                                    }
                                }
                                else
                                {
                                    int LegalEntityid = Business.ComplianceManagement.GetLegalEntityIDByLegalEntityName(LegalList);
                                    LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                    {
                                        ComplianceId = Convert.ToInt32(complianceid),
                                        LegalEntityTypeID = LegalEntityid,
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                                }
                            }
                        }
                    }
                }
                suucess = true;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        #endregion

        #region Add Checklist
        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Checklist"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance> complianceList1 = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance>();
                List<Tuple<int, string, string>> specialDueDate = new List<Tuple<int, string, string>>();
                for (int i = 2; i <= filledrow; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        //ActID
                        complianceData.ActID = ActManagement.GetIdByName(Convert.ToString(xlWorksheet.Cells[i, 1].Value));
                        //Sections
                        complianceData.Sections = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                        //ComplianceType
                        if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Checklist"))
                        {
                            complianceData.ComplianceType = 1;
                        }
                        //ShortDescription
                        complianceData.ShortDescription = Convert.ToString(xlWorksheet.Cells[i, 7].Value);
                        //Description
                        complianceData.Description = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                        //upload Document
                        if (Convert.ToString(xlWorksheet.Cells[i, 9].Value).Trim().Equals("Yes"))
                        {
                            complianceData.UploadDocument = true;
                        }
                        else
                        {
                            complianceData.UploadDocument = false;
                        }
                        //RequiredForms
                        complianceData.RequiredForms = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                        //RiskType
                        if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("High"))
                        {
                            complianceData.RiskType = 0;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 24].Value).Trim().Equals("Medium"))
                        {
                            complianceData.RiskType = 1;
                        }
                        else
                        {
                            complianceData.RiskType = 2;
                        }
                        //Event added by rahul on 27 Jan 2016
                        #region 

                           //Event ID
                            if (Convert.ToString(xlWorksheet.Cells[i, 12].Value).Trim() == "N")
                            {
                                complianceData.EventID = null;
                            }
                            else if (Convert.ToString(xlWorksheet.Cells[i, 12].Value).Trim() == "Y")
                            {
                                complianceData.EventID = com.VirtuosoITech.ComplianceManagement.Business.EventManagement.GETEVENTID(Convert.ToString(xlWorksheet.Cells[i, 13].Value));
                            }

                            //SubEventID
                            if (Convert.ToString(xlWorksheet.Cells[i, 12].Value).Trim() == "Y")
                            {
                                if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim() != "" && Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim() != "")
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim() != null && Convert.ToString(xlWorksheet.Cells[i, 15].Value).Trim() != null)
                                    {
                                        complianceData.SubEventID = com.VirtuosoITech.ComplianceManagement.Business.EventManagement.GETEVENTandSUBEVENTid(Convert.ToString(xlWorksheet.Cells[i, 13].Value), Convert.ToString(xlWorksheet.Cells[i, 15].Value));
                                    }
                                }
                            }
                            // EventComplianceType
                            if (Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim().Equals("Mandatory"))
                            {
                                complianceData.EventComplianceType = 0;
                            }
                            else if (Convert.ToString(xlWorksheet.Cells[i, 14].Value).Trim().Equals("Optional"))
                            {
                                complianceData.EventComplianceType = 1;
                            }
                        #endregion
                        //checklist type
                        if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Functionbased"))
                        {
                            //Frequency
                            complianceData.Frequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 17].Value)));
                            //DueDate
                            if (xlWorksheet.Cells[i, 18].Value != null && xlWorksheet.Cells[i, 18].Value.ToString().Trim() != "")
                            {
                                complianceData.DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 18].Value);
                            }

                            int step = 1;
                            switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 17].Value))))
                            {
                                case Frequency.Monthly:
                                    step = 1;
                                    break;
                                case Frequency.Quarterly:
                                    step = 3;
                                    break;
                                case Frequency.FourMonthly:
                                    step = 4;
                                    break;
                                case Frequency.HalfYearly:
                                    step = 6;
                                    break;
                                case Frequency.Annual:
                                    step = 12;
                                    break;
                                default:
                                    step = 12;
                                    break;
                            }

                            int specilMonth = 0;
                            int specialDay = 0;
                            if ((xlWorksheet.Cells[i, 19].Value != null && xlWorksheet.Cells[i, 20].Value != null) && (xlWorksheet.Cells[i, 19].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 20].Value.ToString().Trim() != ""))
                            {
                                //Special Due Date - For Month
                                specilMonth = Convert.ToInt32(xlWorksheet.Cells[i, 19].Value);
                                //Special Due Date - Date
                                specialDay = Convert.ToInt32(xlWorksheet.Cells[i, 20].Value);
                            }
                            List<ComplianceSchedule> complianceSchedule = new List<ComplianceSchedule>();

                            int SpecialMonth;
                            #region Financial Year And Calender
                            string calflag = Convert.ToString(xlWorksheet.Cells[i, 25].Value);
                            if (calflag == "Y")
                            {
                                for (int month = 4; month <= 12; month += step)
                                {

                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.Frequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 10;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 8;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 8)
                                    {
                                        SpecialMonth = 12;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 3 && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.Frequency == 3 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if ((complianceData.Frequency == 5 || complianceData.Frequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                    if (complianceData.DueDate > lastdate)
                                    {
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {
                                        //Due Date
                                        if (xlWorksheet.Cells[i, 18].Value != null && xlWorksheet.Cells[i, 18].Value.ToString().Trim() != "")
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 18].Value).ToString("D2") + SpecialMonth.ToString("D2");
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 20].Value).ToString("D2") + Convert.ToInt32(xlWorksheet.Cells[i, 19].Value).ToString("D2");
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                        complianceData.ComplianceSchedules.Add(complianceShedule);
                                    }
                                    else
                                    {
                                        complianceData.ComplianceSchedules.Add(complianceShedule);
                                    }
                                }
                            }//Financial Year close
                            else
                            {
                                for (int month = 1; month <= 12; month += step)
                                {

                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.Frequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 2 && SpecialMonth == 7)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.Frequency == 4 && SpecialMonth == 9)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if ((complianceData.Frequency == 3 || complianceData.Frequency == 5 || complianceData.Frequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                    if (complianceData.DueDate > lastdate)
                                    {
                                        //complianceShedule.SpecialDate = lastdate.ToString() + month.ToString("D2");
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {

                                        if (xlWorksheet.Cells[i, 18].Value != null && xlWorksheet.Cells[i, 18].Value.ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(xlWorksheet.Cells[i, 18].Value) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 18].Value).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(xlWorksheet.Cells[i, 18].Value) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(xlWorksheet.Cells[i, 20].Value).ToString("D2") + Convert.ToInt32(xlWorksheet.Cells[i, 19].Value).ToString("D2");
                                            }
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        if (Convert.ToInt32(xlWorksheet.Cells[i, 18].Value) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                            complianceData.ComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(xlWorksheet.Cells[i, 18].Value) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceData.ComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                }
                            }//Calender Year Close

                            #endregion
                        }
                        #region Time Based
                        if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Timebased"))
                        {
                            if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                            {
                                complianceData.SubComplianceType = 0;//fixed gap
                                complianceData.DueDate = Convert.ToInt32(xlWorksheet.Cells[i, 18].Value);
                            }
                            else
                            {
                                complianceData.SubComplianceType = 1;//periodically
                                int step = 1;
                                //Frequency
                                complianceData.Frequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 17].Value)));
                                switch ((Frequency)Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(xlWorksheet.Cells[i, 17].Value))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                //int SpecialMonth;
                                List<ComplianceSchedule> complianceSchedule = new List<ComplianceSchedule>();
                                for (int month = 1; month <= 12; month += step)
                                {
                                    ComplianceSchedule complianceShedule = new ComplianceSchedule();
                                    complianceShedule.ForMonth = month;
                                    //}

                                    //if (subcomplincetype == 1 || subcomplincetype == 2)
                                    //{
                                    int monthCopy = month;
                                    if (monthCopy == 1)
                                    {
                                        monthCopy = 12;
                                        complianceShedule.ForMonth = monthCopy;
                                    }
                                    else
                                    {
                                        monthCopy = month - 1;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    //if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)//Annual,TwoYearly,SevenYearly
                                    if (Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim().Equals("Annual") || Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim().Equals("TwoYearly") || Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim().Equals("SevenYearly"))
                                    {
                                        monthCopy = 3;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                    complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                    //complianceData.ComplianceSchedules.Add(complianceShedule);
                                    complianceData.ComplianceSchedules.Add(complianceShedule);

                                    //}
                                }

                            }
                        }
                        #endregion Time Based
                        //end region Compliance Type

                        
                        #region reminder
                        //Reminder type
                        if (Convert.ToString(xlWorksheet.Cells[i, 21].Value).Trim().Equals("Standard"))
                        {
                            complianceData.ReminderType = 0;//standard
                        }
                        else
                        {
                            complianceData.ReminderType = 1;//custom
                            //ReminderBefore
                            complianceData.ReminderBefore = Convert.ToInt32(xlWorksheet.Cells[i, 22].Value);
                            //ReminderGap
                            complianceData.ReminderGap = Convert.ToInt32(xlWorksheet.Cells[i, 23].Value);
                        }
                        #endregion reminder
                        //PenaltyDescription
                        complianceData.PenaltyDescription = Convert.ToString(xlWorksheet.Cells[i, 28].Value);

                        //CheckListTypeID
                        if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Functionbased"))
                        {
                            complianceData.CheckListTypeID = 1;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Timebased"))
                        {
                            complianceData.CheckListTypeID = 2;
                        }
                        else if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("OneTime"))
                        {
                            complianceData.CheckListTypeID = 0;
                        }
                        //OneTimeDate
                        if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("OneTime"))
                        {
                            complianceData.OneTimeDate = Convert.ToDateTime(xlWorksheet.Cells[i, 6].Value);
                        }
                        complianceData.PenaltyDescription = Convert.ToString(xlWorksheet.Cells[i, 28].Value);
                        complianceData.ReferenceMaterialText = Convert.ToString(xlWorksheet.Cells[i, 29].Text);

                        complianceData.CreatedBy = 1;
                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;
                    }
                    complianceList.Add(complianceData);
                }
                complianceList1 = complianceList.Where(entry => entry.ActID == 0).ToList();//to check that ActID should not be 0 in database
                //Business.ComplianceManagement.Create(complianceList);
                //suucess = true;
                string str = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int i = 0;
                    complianceList.ForEach(entry =>
                    {
                        i = i + 1;
                        bool chkexists = Business.ComplianceManagement.ExistsR(entry.Description, entry.ShortDescription, entry.ActID);
                        if (chkexists == true)
                        {
                            saveflag = false;  // Same compliance allready save in Sysyem
                            str += "Same compliance Short Description and Detailed Description allready saved in system -(Record - " + i + " )<br />";
                        }
                    });
                }

                if (saveflag == true)
                {
                    Business.ComplianceManagement.Create(complianceList);
                    suucess = true;
                }
                else
                {
                    cvDuplicateEntry2.IsValid = false;
                    cvDuplicateEntry2.ErrorMessage = str;//"Same compliance Short Description and Detailed Description allready saved in system -(Record -" + i + " )";
                    suucess = false;
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnUploadFile2_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload2.HasFile)
            {
                try
                {
                    //bool suucess = false;
                    string filename = Path.GetFileName(MasterFileUpload2.FileName);
                    MasterFileUpload2.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoAct1.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Act");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {
                                    cvDuplicateEntry2.IsValid = false;
                                    cvDuplicateEntry2.ErrorMessage = "No data found in file.";
                                }
                            }
                            else if (rdoChecklist.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Checklist");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    ProcessChecklistData(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {
                                    cvDuplicateEntry2.IsValid = false;
                                    cvDuplicateEntry2.ErrorMessage = "No data found in file.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry2.IsValid = false;
                                cvDuplicateEntry2.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            if (suucess == true)//commented by Manisha
                            {
                                cvDuplicateEntry2.IsValid = false;
                                cvDuplicateEntry2.ErrorMessage = "Data uploaded successfully.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry2.IsValid = false;
                        cvDuplicateEntry2.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry2.IsValid = false;
                    cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        #endregion
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }

        protected void Tab4_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Clicked";
            MainView.ActiveViewIndex = 3;
        }

        protected void btnUploadFileEvent_Click(object sender, EventArgs e)
        {
            if (MasterFileUploadEvent.HasFile)
            {
                try
                {
                    //bool suucess = false;
                    string filename = Path.GetFileName(MasterFileUploadEvent.FileName);
                    MasterFileUploadEvent.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoActEvent.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Act");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {

                                    cvDuplicateEntryEvent.IsValid = false;
                                    cvDuplicateEntryEvent.ErrorMessage = "No data found in file.";

                                }

                            }
                            else if (rdoComplianceEvent.Checked)
                            {
                                bool flag = ActSheetsExitsts(xlWorkbook, "Compliances");
                                if (flag == true)
                                {
                                    ProcessActData(xlWorkbook);
                                    ProcessComplianceDataEvent(xlWorkbook);
                                    //suucess = true;
                                }
                                else
                                {

                                    cvDuplicateEntryEvent.IsValid = false;
                                    cvDuplicateEntryEvent.ErrorMessage = "No data found in file.";


                                }
                            }
                            else
                            {
                                cvDuplicateEntryEvent.IsValid = false;
                                cvDuplicateEntryEvent.ErrorMessage = "Please select type of data wants to be upload.";

                            }

                            if (suucess == true)//commented by Manisha
                            {
                                cvDuplicateEntryEvent.IsValid = false;
                                cvDuplicateEntryEvent.ErrorMessage = "Data uploaded successfully.";

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntryEvent.IsValid = false;
                        cvDuplicateEntryEvent.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntryEvent.IsValid = false;
                    cvDuplicateEntryEvent.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }
    }
}