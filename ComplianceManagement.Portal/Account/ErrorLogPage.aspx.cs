﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ErrorLogPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.UtcNow.AddMonths(-1).ToString("dd-MM-yyyy");
                txtEndDate.Text = DateTime.UtcNow.ToString("dd-MM-yyyy");
                BindLog(txtStartDate.Text, txtEndDate.Text);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dataToExport = GetGrid();

                if (dataToExport == null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "EmptyDataForExport", " $(function () { alert('No data available for export...'); });", true);
                    return;
                }

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Compliance");
                    ws.Column(1).Width = 1;
                    ws.Row(1).Height = 5;
                    using (ExcelRange rng = ws.Cells[2, 2, 2, 5])
                    {
                        rng.Merge = true;
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.White);  //Set color to dark blue
                        //rng.Style.Font.Color.SetColor(Color.White);
                        rng.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.LightGray);
                        rng.Value = "Log Report";
                    }

                    using (ExcelRange rng = ws.Cells[4, 2, 4, 4])
                    {
                        rng.Merge = true;
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.White);  //Set color to dark blue
                        rng.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.LightGray);
                    }

                    ws.Cells[6, 2].LoadFromDataTable(dataToExport, true, OfficeOpenXml.Table.TableStyles.Medium4);


                    using (ExcelRange col = ws.Cells[6, 2, 5 + dataToExport.Rows.Count, 1 + dataToExport.Columns.Count])
                    {
                        col.AutoFitColumns(18);
                    }

                    byte[] fileData = pck.GetAsByteArray();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=ErrorLogReport.xlsx");
                    Response.AddHeader("Content-Length", fileData.Length.ToString());
                    Response.BinaryWrite(fileData); // create the file
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                BindLog(strtdate, enddate);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void BindLog(string startDate, string endDate)
        {
            try
            {
                List<LogMessage> LogMessageList = null;
                if (startDate != "" && endDate != "")
                {
                    DateTime startdate1 = DateTime.ParseExact(startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime endDate1 = DateTime.ParseExact(endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    LogMessageList = EventManagement.GetLogMessages(startdate1, endDate1);
                }
                else
                {
                    LogMessageList = EventManagement.GetLogMessages(DateTime.Now.AddMonths(-1), DateTime.Now);
                }


                grdLogEntry.DataSource = LogMessageList; 
                grdLogEntry.DataBind();
                upComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLogEntry_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                grdLogEntry.PageIndex = e.NewPageIndex;
                BindLog(strtdate, enddate);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdLogEntry_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<LogMessage> LogMessageList = null;
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                if (strtdate != "" && enddate != "")
                {
                    DateTime startdate1 = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime endDate1 = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    LogMessageList = EventManagement.GetLogMessages(startdate1, endDate1);
                }
                else
                {
                    LogMessageList = EventManagement.GetLogMessages(DateTime.Now.AddMonths(-1), DateTime.Now);
                }

                if (direction == SortDirection.Ascending)
                {
                    LogMessageList = LogMessageList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    LogMessageList = LogMessageList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdLogEntry.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLogEntry.Columns.IndexOf(field);
                    }
                }

                grdLogEntry.DataSource = LogMessageList;
                grdLogEntry.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdLogEntry_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       

        public DataTable GetGrid()
        {
            try
            {
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                BindLog(strtdate, enddate);
                return (grdLogEntry.DataSource as List<LogMessage>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;

        }
    }
}