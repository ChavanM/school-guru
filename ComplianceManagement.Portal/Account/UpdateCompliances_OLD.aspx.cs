﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UpdateCompliances_OLD : System.Web.UI.Page
    {

        bool suucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
             
                BindGridviewCompliance();

            }
        }
       
        protected void BindGridviewCompliance()
        {
            try
            {
                var compliancesData = Business.ComplianceManagement.GetAllExcelExport();
            List<object> dataSource = new List<object>();
            foreach (var complianceInfo in compliancesData)
            {

                       

                dataSource.Add(new
                {

                    complianceInfo.ActName,
                    complianceInfo.Sections,                                       
                    complianceInfo.ComplianceType,

                    complianceInfo.SubComplianceType,

                    complianceInfo.ShortDescription,
                    complianceInfo.Description,
                    complianceInfo.NatureOfCompliance,
                    complianceInfo.UploadDocument,
                    complianceInfo.RequiredForms,
                    complianceInfo.sampleform,
                    complianceInfo.iseventbased,
                    //complianceInfo.SubEventID,
                    complianceInfo.EventComplianceType,
                    complianceInfo.Frequency,                      
                    complianceInfo.DueDate,
                    complianceInfo.spdate,
                    complianceInfo.ReminderType,
                    complianceInfo.ReminderBefore,
                    complianceInfo.ReminderGap,
                    complianceInfo.RiskType,
                    complianceInfo.NonComplianceType,
                    complianceInfo.FixedMinimum,
                    complianceInfo.FixedMaximum,
                    complianceInfo.VariableAmountPerMonth,
                    complianceInfo.VariableAmountPerDay,
                    complianceInfo.VariableAmountPerDayMax,
                    complianceInfo.VariableAmountPercent,
                    complianceInfo.VariableAmountPercentMax,              
                    complianceInfo.Imprisonment,
                    complianceInfo.Designation,
                    complianceInfo.MinimumYears,
                    complianceInfo.MaximumYears,
                    complianceInfo.Others,                                        
                    complianceInfo.calflag,
                    //complianceInfo.EventComplianceType,
                    complianceInfo.SubEventName,
                    complianceInfo.EventName,
                    
                    
                                                                         
                });
            }
            grdCompliance.DataSource = dataSource;
            grdCompliance.DataBind();


            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
         
            
        }

       
    }
}