﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AuditTool.Master" CodeBehind="UploadRiskCreationWithExcelsheet.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadRiskCreationWithExcelsheet" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            fhead('Historical Observation');
        });
        function showProgress() {
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>
    <style type="text/css">
        .dd_chk_select {
            height: 81px !important;
            height: 3px !important;
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            height: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        div.dd_chk_drop {
            top: 31px !important;
        }

        div.dd_chk_drop_cust div#checks {
            border-style: Solid !important;
            border-width: 1px !important;
            border-color: Black !important;
        }

        div#ContentPlaceHolder1_ddlVerticalBranch_sl {
            height: 35px !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });
        })

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceDetails">
        <progresstemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>


    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                               
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;display:none">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <li class="active" id="liNotDone" runat="server">
                                    <asp:LinkButton ID="Tab1" class="active" runat="server">Import Utility</asp:LinkButton>
                                </li>
                                <li class="" id="liSubmitted" runat="server">
                                    <asp:LinkButton ID="Tab2" class="" runat="server">Export Utility</asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="tab-content">
                            <div runat="server" id="performerdocuments" class="tab-pane active">
                                <div style="width: 100%; float: left; margin-top: 1em; margin-bottom: 15px">
                                    <div style="margin-top: 5px; color: #333; width: 15%;display:none;">
                                        <asp:RadioButton ID="rdoRCMUpload" runat="server" AutoPostBack="True" Text="Risk Control Matrix" OnCheckedChanged="rdoCompliance_CheckedChanged" GroupName="uploadContentGroup" />
                                    </div>

                                    <div style="margin-top: 5px; color: #333; width: 15%;display:none;">
                                        <asp:RadioButton ID="rdoSubProcess" runat="server" AutoPostBack="True" Text="Sub Process" OnCheckedChanged="rdoSubProcess_CheckedChanged" GroupName="uploadContentGroup" />
                                    </div>

                                    <div style="margin-top: 5px; color: #333; width: 20%;display:none;">
                                        <asp:RadioButton ID="rdoRCMUpdate" runat="server" AutoPostBack="True" Text="Update Risk Control Matrix" OnCheckedChanged="rdoCompliance_CheckedChanged" GroupName="uploadContentGroup" />
                                    </div>

                                    <div class="col-md-9 colpadding0 entrycount" style="margin-top: 5px; color: #333; width: 100%">
                                        <div class="col-md-3 colpadding0 entrycount" style="width: 20%">
                                             <asp:FileUpload ID="fileUploadRCM" runat="server" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="text-align: left; width: 30%">
                                           <asp:Button ID="btnUploadRCM" runat="server" class="btn btn-primary" Text="Upload" OnClick="btnUploadRCM_Click" />
                                        </div>
                                        <div  class="col-md-3 colpadding0 entrycount" style="text-align: right;width: 40%; margin-right: 2%;" runat="server">
                                    <u><a href="../../AuditSampleDocument/Historical Observation Template.xlsx">Sample Format for Historical Observation</a></u>
                                </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div style="margin-bottom: 4px">                                       
                                      <asp:GridView runat="server" ID="grdUploadedHistrory" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true"  CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                     ShowFooter="true" OnRowCommand="grdUploadedHistrory_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UploadId" Visible="false">
                                            <ItemTemplate>
                                               <div class="text_NlinesusingCSS" style="width: 10px;">
                                                <asp:Label ID="lblUploadId" runat="server" Text='<%# Eval("UploadId") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("UploadId") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="File Name">
                                            <ItemTemplate>
                                               <div class="text_NlinesusingCSS">
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("HistoricalObservationFileName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("HistoricalObservationFileName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Uploaded Date">
                                            <ItemTemplate>
                                                  <div class="text_NlinesusingCSS">
                                                <asp:Label ID="lblUploadedDate" runat="server" Text='<%# Eval("HistoricalObservationUploadedDate") != null ? Convert.ToDateTime(Eval("HistoricalObservationUploadedDate")).ToString("dd-MM-yyyy") : ""%>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("HistoricalObservationUploadedDate") != null ? Convert.ToDateTime(Eval("HistoricalObservationUploadedDate")).ToString("dd-MM-yyyy") : ""  %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>  

                                        <asp:TemplateField HeaderText="FilePath" Visible="false">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS">
                                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("HistoricalObservationFilePath") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("HistoricalObservationFilePath") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>   
                                                    <asp:UpdatePanel ID="upAu" runat="server">
                                                    <ContentTemplate>                            
                                                    <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("UploadId") + "," + Eval("HistoricalObservationFileName") + "," + Eval("HistoricalObservationUploadedDate") + "," + Eval("HistoricalObservationFilePath") %>'
                                                    CausesValidation="false">
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" data-toggle="tooltip" data-placement="top" title="Download Document" />
                                                    </asp:LinkButton>
                                                    </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lnkAuditDetails" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />   
                                    <HeaderStyle BackColor="#ECF0F1" />
                                            <PagerSettings Visible="false" />                   
                                    <PagerTemplate>                                      
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                  <div style="float: right;">
                          <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                              class="form-control m-bot15"  Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" >                                   
                          </asp:DropDownListChosen>  
                  </div>
                  </div>
                  <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0" style="float:right">
                            <div class="table-paging" style="margin-bottom: 10px;">
                                <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                <div class="table-paging-text" style="float: right;">
                                    <p>
                                        Page
                                        <%--<asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                    </p>
                                </div>
                                <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                                </div>
                         
               
                               
                              
                                <div style="width: 100%; float: left; margin-right: 2%; display:none;" runat="server" id="Divbranchlist">
                                     <div class="col-md-4 colpadding0" style="margin-top: 5px; color: #333; width: 30%">
                                        <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" PlaceHolder="Select Applicable Location" autocomplete="off"
                                            Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 325px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                            CssClass="txtbox" />


                                        <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                            <asp:TreeView runat="server" ID="tvFilterLocation" ShowCheckBoxes="All" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; 
                                                 border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                ShowLines="true" onclick="OnTreeClick(event)">
                                            </asp:TreeView>
                                        </div>
                                    </div>

                                  <div class="col-md-4 colpadding0" style="margin-top: 5px; color: #333;">
                                            <asp:DropDownCheckBoxes ID="ddlVerticalBranch" runat="server" AutoPostBack="true" CssClass="form-control m-bot15"
                                            AddJQueryReference="false" UseButtons="false" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 80%; height: 50px !important;">
                                            <Style SelectBoxWidth="320" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                            <Texts SelectBoxCaption="Select Vertical" />
                                        </asp:DropDownCheckBoxes>
                                    </div>

                                      <div class="col-md-4 colpadding0" style="margin-top: 5px; color: #333; width: 15%">
                                          </div>
                                </div>
                                
                               
                            </div>
                        </div>

                        <div runat="server" id="reviewerdocuments" class="tab-pane">
                            <div class="clearfix"></div>
                            <div class="clearfix" style="height: 15px"></div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount">
                                    <div class="col-md-3 colpadding0">                                        
                                    </div>                                  
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; color: #999;" runat="server" visible="false">                                    
                                </div>

                                <div class="col-md-6 colpadding0" style="margin-top: 5px; color: #999;">
                                </div>

                                <div class="col-md-3 colpadding0" style="text-align: right; color: #999; float: right; padding-right: 2%;display:none">
                                    <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-primary" CausesValidation="false"
                                         runat="server" />
                                </div>

                                <div style="text-align: right">
                                </div>

                                <div style="float: right; margin-top: 5px;">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0" style="display:none;">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"  DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>--%>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0" style="display:none;">
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" Width="90%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlFilterProcess" runat="server" AutoPostBack="true" DataPlaceHolder="Process" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" DataPlaceHolder="Sub Process"
                                         class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%>                                           
                                            <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true"  DataPlaceHolder="Vertical" 
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  CssClass="form-control m-bot15" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>                                                                               
                                     <%}
                                         else
                                         {%>    
                                        <asp:TextBox runat="server" ID="txtFilter" PlaceHolder="Type to Search" AutoPostBack="true" CssClass="form-control" Style="margin-bottom: 10px; width: 90%;" />
                                    <%}%>  
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="clearfix"></div>      
                            
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </contenttemplate>
        <triggers>
            <asp:PostBackTrigger ControlID="btnUploadRCM" />
            <%-- <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            <asp:PostBackTrigger ControlID="LinkButton_sampleForm" />--%>
        </triggers>
    </asp:UpdatePanel>
</asp:Content>



