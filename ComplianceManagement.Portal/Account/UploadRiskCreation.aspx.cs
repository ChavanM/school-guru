﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using System.Data;
using Ionic.Zip;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadRiskCreation : System.Web.UI.Page
    {
        bool suucess = false;
        public static List<long> locationList = new List<long>();
        public static List<long> Branchlist = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {             
                Divbranchlist.Visible = false;
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                locationList.Clear();
                ViewState["Flagvalue"] = null;
                Tab1_Click1(sender, e);                

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindGridData();

        }
        private void BindLocationFilter()
        {
            try
            {
                long customerID = -1;                
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);  

                TreeNode node = new TreeNode();

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        public void BindLegalEntityData()
        {
            long customerID = -1;            
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            long customerID = -1;           
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        #region  Import Risk Register and Risk Control Matrix 

        protected void RetrieveNodes(TreeNode node)
        {
            if (node.Checked && node.ChildNodes.Count == 0) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked && tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i]);
                    }
                }
            }
        }


        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoRCMUpload.Checked)
                            {
                                bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "RiskControlMatrixCreation");

                                if (flag == true)
                                {
                                    //List<long> locationList = new List<long>();

                                    locationList.Clear();

                                    for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                                    {
                                        RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                                    }

                                    //foreach (ListItem item in ddlBranchList.Items)
                                    //{
                                    //    if(item.Selected)                                        
                                    //        locationList.Add(Convert.ToInt32(item.Value));                                        
                                    //}

                                    if (locationList.Count > 0)
                                    {
                                        if (CheckSubProcessScore(xlWorkbook))
                                            ProcessRiskCategoryDataNew1(xlWorkbook, locationList);
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Select Branch.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixCreation'.";
                                }
                            }
                            else if (rdoSubProcess.Checked)
                            {
                                bool flag = ProcessSheetsExitsts(xlWorkbook, "ProcessSubProcess");
                                if (flag == true)
                                {
                                    ProcessData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ProcessSubProcess'.";
                                }
                            }
                            else if (rdoRCMUpdate.Checked)
                            {
                                bool flag = RCMUpdateSheetExitsts(xlWorkbook, "RiskControlMatrixUpdate");
                                if (flag == true)
                                {
                                    if (CheckSubProcessScoreRCMUpdate(xlWorkbook))
                                        ProcessRiskControlMatrixUpdate(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixUpdate'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            if (suucess == true)//commented by Manisha
                            {
                                locationList.Clear();
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        private bool RCMUpdateSheetExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskControlMatrixUpdate"))
                    {  
                        if (sheet.Name.Trim().Equals("RiskControlMatrixUpdate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }           
        }

        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskControlMatrixCreation"))
                    {
                        if (sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static int GetRiskControlRatingID(string Flag, string Name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where row.Name.ToUpper() == Name.ToUpper() && row.IsActive == false
                                         && row.IsRiskControl == Flag
                                         select row.Value).FirstOrDefault();
                return transactionsQuery;
            }
        }

        private bool CheckSubProcessScore(ExcelPackage xlWorkbook)
        {
            try
            {
                bool CheckScoreSuccess = false;

                List<Tuple<int, int, decimal>> ExcelProcesSubProcessList = new List<Tuple<int, int, decimal>>();

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];

                if (xlWorksheet != null)
                {
                    int count = 0;

                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    int customerID = -1;
                    customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        int processid = -1;
                        int subprocessid = -1;
                        Decimal SubProcessScore = 0;

                       
                        //2 Process 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 2].Text.ToString().Trim(), @"\t|\n|\r", ""), customerID);
                        }

                        if (processid == 0 || processid == -1)
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process at Row - " + (count + 1) + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            CheckScoreSuccess = true;
                        }

                        //3 Sub Process                    
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            subprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 3].Text.ToString().Trim(), @"\t|\n|\r", ""), processid);
                        }

                        if (subprocessid == 0 || subprocessid == -1)
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process at Row - " + (count + 1) + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            CheckScoreSuccess = true;
                        }

                        //35 SubProcess Score
                        try
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                                SubProcessScore = Convert.ToDecimal(xlWorksheet.Cells[i, 35].Text.Trim());
                            else
                                SubProcessScore = 0;
                        }
                        catch (Exception ex)
                        {
                            SubProcessScore = 0;
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Score at Row - " + (count + 1) + " or Sub Process Score not Defined in the System.";
                            break;
                        }

                        if (CheckScoreSuccess)
                        {
                            ExcelProcesSubProcessList.Add(new Tuple<int, int, decimal>(processid, subprocessid, SubProcessScore));
                        }
                    }

                    var NewList = ExcelProcesSubProcessList.GroupBy(s => new { s.Item1, s.Item2 })
                        .Select(g => new ProcessSubProcessScore
                        {
                            ProcessID = g.Key.Item1,
                            SubProcessID = g.Key.Item2,
                            SubProcessScore = g.Sum(x => x.Item3)
                        }).ToList();

                    int customerIDR = -1;
                    customerIDR = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    foreach (ProcessSubProcessScore EachEntry in NewList)
                    {
                        if (EachEntry.SubProcessScore != ProcessManagement.GetSubProcessScore(EachEntry.ProcessID, EachEntry.SubProcessID))
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check Sub Process Score of Process -" + ProcessManagement.GetProcessName(EachEntry.ProcessID, customerIDR) + " and Sub Process -" + ProcessManagement.GetSubProcessName(EachEntry.SubProcessID) + "";
                            break;
                        }
                        else
                            CheckScoreSuccess = true;
                    }                  
                }

                return CheckScoreSuccess;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        private bool CheckSubProcessScoreRCMUpdate(ExcelPackage xlWorkbook)
        {
            try
            {
                bool CheckScoreSuccess = false;

                List<Tuple<int, int, int, int, decimal>> ExcelProcesSubProcessList = new List<Tuple<int, int, int, int, decimal>>(); // List<Tuple<CustBranchID, VerticalID, ProcessID, SubProcessID, decimal Score>>

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixUpdate"];

                if (xlWorksheet != null)
                {
                    int count = 0;

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    int processid = -1;
                    int subprocessid = -1;
                    int CustBranchID = -1;
                    int verticalID = -1;
                    Decimal SubProcessScore = 0;
                    
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                         processid = -1;
                         subprocessid = -1;
                         CustBranchID = -1;
                         verticalID = -1;
                         SubProcessScore = 0;                        

                        //23 BranchID 
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check BID at Row - " + i + ", BID can not be empty;";
                            break;
                        }
                        else
                        {
                            CustBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 23].Text);
                            CheckScoreSuccess = true;
                        }


                        //24 VerticalID 
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check VID at Row - " + i + ", VID can not be empty;";
                            break;
                        }
                        else
                        {
                            verticalID = Convert.ToInt32(xlWorksheet.Cells[i, 24].Text);
                            CheckScoreSuccess = true;
                        }

                        //25 ProcessID 
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check PID at Row - " + i + ", PID can not be empty;";
                            break;
                        }
                        else
                        {
                            processid = Convert.ToInt32(xlWorksheet.Cells[i, 25].Text);
                            CheckScoreSuccess = true;
                        }
                        

                        //26 SubProcessID                    
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check SPID at Row - " + i + ", SPID can not be empty;";
                            break;
                        }
                        else
                        {
                            subprocessid = Convert.ToInt32(xlWorksheet.Cells[i, 26].Text);
                            CheckScoreSuccess = true;
                        }                           

                        //20 SubProcess Score
                        try
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                                SubProcessScore = Convert.ToDecimal(xlWorksheet.Cells[i, 20].Text.Trim());
                            else
                                SubProcessScore = 0;
                        }
                        catch (Exception ex)
                        {
                            SubProcessScore = 0;
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Score at Row - " + (count + 1) + ", Sub Process Score can not be empty;";
                            break;
                        }

                        if (CheckScoreSuccess)
                        {
                            ExcelProcesSubProcessList.Add(new Tuple<int, int,int,int, decimal>(CustBranchID, verticalID, processid, subprocessid, SubProcessScore));
                        }
                    }

                    if (CheckScoreSuccess)
                    {
                        var NewList = ExcelProcesSubProcessList.GroupBy(s => new { s.Item1, s.Item2, s.Item3, s.Item4 })
                        .Select(g => new ProcessSubProcessScore
                        {
                            CustBranchID = g.Key.Item1,
                            VerticalID = g.Key.Item2,
                            ProcessID = g.Key.Item3,
                            SubProcessID = g.Key.Item4,
                            SubProcessScore = g.Sum(x => x.Item5)
                        }).ToList();                        

                        foreach (ProcessSubProcessScore EachEntry in NewList)
                        {
                            if (EachEntry.SubProcessScore != ProcessManagement.GetSubProcessScore(EachEntry.ProcessID, EachEntry.SubProcessID))
                            {
                                CheckScoreSuccess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check Sub Process Score of Process- " + ProcessManagement.GetProcessName(EachEntry.ProcessID, customerID) + " and Sub Process- " + ProcessManagement.GetSubProcessName(EachEntry.SubProcessID) + "";
                                break;
                            }
                            else
                                CheckScoreSuccess = true;
                        }
                    }
                }

                return CheckScoreSuccess;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        private void ProcessRiskControlMatrixUpdate(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixUpdate"];

                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (xlWorksheet != null)
                {
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();

                    List<RiskCategoryCreation> riskcategorycreationlist = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist1 = new List<RiskCategoryCreation>();

                    #region Verify Excel Sheet for Blank/Valid Values

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        string ActivityDescription = "";
                        string ControlObjective = "";
                        string ControlDescription = string.Empty;
                        string MControlDescription = string.Empty;                      
                        string ControlNo = "";

                        int RiskID = -1;
                        int RiskActivityID = -1;

                        int processid = -1;
                        int subprocessid = -1;
                        int CustBranchID = -1;
                        int verticalID = -1;
                        Decimal SubProcessScore = 0;

                        //6 ControlNo                   
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            ControlNo = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();

                        if (String.IsNullOrEmpty(ControlNo))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required ControlNo at row - " + i + ".";
                            break;
                        }
                        else
                            suucess = true;

                        //7 Risk Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                            ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                        if (String.IsNullOrEmpty(ActivityDescription))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Risk/Activity Description at row  - " + i + ".";
                            break;
                        }
                        else
                            suucess = true;

                        //8 Control Objective
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                        }

                        if (String.IsNullOrEmpty(ControlObjective))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Control Objective at row  - " + i + ".";
                            break;
                        }
                        else
                            suucess = true;

                        //9 Control Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }

                        if (String.IsNullOrEmpty(ControlDescription))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Control Description at row  - " + i + ".";
                            break;
                        }
                        else
                            suucess = true;

                        //10 Mitigating Control Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                        {
                            MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                        }

                        if (String.IsNullOrEmpty(MControlDescription))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Mitigation Control Description at row - " + i + ".";
                            break;
                        }
                        else
                            suucess = true;

                        //11 Person Responsible
                        int personresponsibleIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                        {   //12 Email
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            {
                                personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 11].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 12].Text.ToString().Trim());
                            }
                        }

                        if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Person Responsible Name/Email at row - " + i + " or Person Responsible(User) not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //13 Control Owner
                        int ControlOwnerIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {   //14 Email
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            {
                                ControlOwnerIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                            }
                        }

                        if (ControlOwnerIDcheck == 0 || ControlOwnerIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control Owner Name/Email at row - " + i + " or Control Owner(User) not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //15 KC1
                        int KC1check = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 15].Text.ToString().ToUpper().Trim() == "YES")
                            {
                                KC1check = 1;
                            }
                            else if (xlWorksheet.Cells[i, 15].Text.ToString().ToUpper().Trim() == "NO")
                            {
                                KC1check = 2;
                            }
                            // KC1check = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 15].Text.ToString().Trim());
                        }
                        if (KC1check == 0 || KC1check == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the KC-1 Multiple risks addressed at row number - " + i + ".";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //16 KC2

                        int KeyIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "YES")
                            {
                                KeyIDcheck = 1;
                            }
                            else if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "NO")
                            {
                                KeyIDcheck = 2;
                            }
                            //KeyIDcheck = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                        }
                        if (KeyIDcheck == 0 || KeyIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the KC-2 Criticality of control at row number - " + i + ".";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        

                        //17 Test Strategy   

                        //18 Risk Rating
                        int RiskRatingcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                        {
                            RiskRatingcheck = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim());
                        }
                        if (RiskRatingcheck == 0 || RiskRatingcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Risk Rating at row - " + i + " or Risk Rating not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;

                        //19 Control Rating
                        int ControlRatingCheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                        {
                            ControlRatingCheck = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim());
                        }
                        if (ControlRatingCheck == 0 || ControlRatingCheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control Rating at row - " + i + " or Control Rating not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;

                        //20 SubProcess Score                        
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                            SubProcessScore = Convert.ToDecimal(xlWorksheet.Cells[i, 20].Text.Trim());
                        else
                            SubProcessScore = 0;
                       

                        //21 RiskID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                        {
                            RiskID = Convert.ToInt32(xlWorksheet.Cells[i, 21].Text);
                        }
                        if (RiskID == 0 || RiskID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the RiskID at row - " + i + ", RiskID can not be empty; ";
                            break;
                        }
                        else
                            suucess = true;

                        //22 RiskActivityID 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                        {
                            RiskActivityID = Convert.ToInt32(xlWorksheet.Cells[i, 22].Text);
                        }
                        if (RiskActivityID == 0 || RiskActivityID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the RiskActivityID at row - " + i + ", RiskActivityID can not be empty; ";
                            break;
                        }
                        else
                            suucess = true;

                        //23 BranchID 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                        {
                            CustBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 23].Text);
                        }
                        if (CustBranchID == 0 || CustBranchID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the BID at row - " + i + ", BID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //24 VerticalID 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                        {
                            verticalID = Convert.ToInt32(xlWorksheet.Cells[i, 24].Text);
                        }
                        if (verticalID == 0 || verticalID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the VID at row - " + i + ", VID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //25 ProcessID 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                        {
                            processid = Convert.ToInt32(xlWorksheet.Cells[i, 25].Text);
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the PID at row - " + i + ", PID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                        //26 SubProcessID 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                        {
                            subprocessid = Convert.ToInt32(xlWorksheet.Cells[i, 26].Text);
                        }
                        if (subprocessid == 0 || subprocessid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the SPID at row - " + i + ", PID can not be empty.";
                            break;
                        }
                        else
                            suucess = true;

                    }

                    #endregion

                    #region Save Data in RiskCategoryCreation/RiskActivityTransaction

                    if (suucess)
                    {
                        int RiskID = -1;
                        int RiskActivityID = -1;
                        int personresponsibleID = 0;
                        int ControlOwnerID = 0;
                        int KC2KeyID = 0;
                        int KC1KeyID = 0;
                        int Frequency = 0;

                        string ControlNo = "";
                        string ActivityDescription = "";
                        string ControlObjective = "";
                        string ControlDescription = string.Empty;
                        string MControlDescription = string.Empty;                        
                        string TestStrategy = "";

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            RiskID = -1;
                            RiskActivityID = -1;
                            personresponsibleID = 0;
                            KC2KeyID = 0;
                            KC1KeyID = 0;
                            Frequency = 0;
                            ControlNo = "";
                            ActivityDescription = "";
                            ControlObjective = "";
                            ControlDescription = string.Empty;
                            MControlDescription = string.Empty;
                            TestStrategy = "";
                         
                            //21 RiskID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                            {
                                RiskID = Convert.ToInt32(xlWorksheet.Cells[i, 21].Text);
                            }

                            //22 RiskActivityID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                            {
                                RiskActivityID = Convert.ToInt32(xlWorksheet.Cells[i, 22].Text);
                            }

                            if ((RiskID != 0 || RiskID != -1) && (RiskActivityID != 0 || RiskActivityID != -1))
                            {
                                //6 ControlNo                   
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                {
                                    ControlNo = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                }

                                //7 Risk Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                    ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();

                                //8 Control Objective
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                {
                                    ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                }

                                //9 Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                {
                                    ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                                }

                                //10 Mitigating Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                {
                                    MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                                }

                                //11 Person Responsible                              
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                {   //12 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                    {
                                        personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 11].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 12].Text.ToString().Trim());
                                    }
                                }

                                //13 Person Responsible                              
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                {   //14 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                    {
                                        ControlOwnerID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                                    }
                                }

                                //15 KC1KeyID                                 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 15].Text.ToString().ToUpper().Trim() == "YES")
                                    {
                                        KC1KeyID = 1;
                                    }
                                    else if (xlWorksheet.Cells[i, 15].Text.ToString().ToUpper().Trim() == "NO")
                                    {
                                        KC1KeyID = 2;
                                    }

                                    //KC1KeyID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 15].Text.ToString().Trim());
                                }


                                //16 KC2KeyID                                
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "YES")
                                    {
                                        KC2KeyID = 1;
                                    }
                                    else if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "NO")
                                    {
                                        KC2KeyID = 2;
                                    }

                                    //KC2KeyID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                                }

                              
                                //17 Test Strategy  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim().Trim()))
                                {
                                    TestStrategy = Convert.ToString(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                                }

                                //18 Risk Rating
                                int RiskRatingcheck = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                                {
                                    RiskRatingcheck = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim());
                                }

                                //19 Control Rating
                                int ControlRatingCheck = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                {
                                    ControlRatingCheck = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 19].Text).Trim());
                                }

                                //20 Process Scores
                                int processscores = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                                {
                                    processscores = Convert.ToInt32(xlWorksheet.Cells[i, 20].Text);
                                }

                                if (RiskCategoryManagement.ExistsRCCByID(RiskID))
                                {
                                    RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                                    {
                                        Id = RiskID,
                                        ControlNo = ControlNo,
                                        ActivityDescription = ActivityDescription,
                                        ControlObjective = ControlObjective,
                                    };

                                    riskcategorycreationlist.Add(riskcategorycreation);
                                }

                                if (RiskCategoryManagement.ExistsRATByID(RiskActivityID, RiskID))
                                {
                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                    {
                                        Id= RiskActivityID,
                                        RiskCreationId = RiskID,
                                        ControlDescription = ControlDescription,
                                        MControlDescription = MControlDescription,
                                        PersonResponsible = personresponsibleID,
                                        ControlOwner = ControlOwnerID,
                                        Key_Value = KC2KeyID,
                                        KC2= KC1KeyID,
                                        TestStrategy = TestStrategy,
                                        //Frequency = Frequency,
                                        RiskRating = RiskRatingcheck,
                                        ControlRating = ControlRatingCheck,
                                        ProcessScore = processscores,
                                    };

                                    riskactivitytransactionlist.Add(riskactivitytransaction);
                                }
                            }
                        }

                        riskcategorycreationlist1 = riskcategorycreationlist.Where(entry => entry.ProcessId == 0 && entry.SubProcessId == 0).Distinct().ToList();
                        riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.ProcessId == 0 && entry.RiskCreationId == 0).ToList();

                        if (riskcategorycreationlist1.Count == 0 && riskactivitytransactionlist1.Count == 0)
                        {
                            //Update RCC and RAT
                            RiskCategoryManagement.BulkRCCUpdate(riskcategorycreationlist);
                            RiskCategoryManagement.BulkRATUpdate(riskactivitytransactionlist);
                            suucess = true;
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetSafeTagName(string tag)
        {
            tag = tag.Replace("'", "`")
            .Replace('"', '`')
            .Replace('‐', '_');
            
            //.Replace("&", "and")
            //.Replace(",", ":")
            //.Replace(@"\", "/"); //Do not allow escaped characters from user
            tag = Regex.Replace(tag, @"\s+", " "); //multiple spaces with single spaces
            return tag;
        }
       
        private void ProcessRiskCategoryDataNew1(ExcelPackage xlWorkbook, List<long> LocationList)
        {
            try
            {
                List<string> RiskIdProcess = new List<string>();
                List<string> IndustryIdProcess = new List<string>();
                List<int> BranchVerticalID = new List<int>();             
                List<string> AssertionsIdProcess = new List<string>();
                long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];

                int customerID = -1;
                customerID = Convert.ToInt32(Common.AuthenticationHelper.CustomerID);
                int checkVerticalapplicable = 0;
                if (xlWorksheet != null)
                {
                    var cdetails = CustomerManagement.GetByID(Convert.ToInt32(customerID));                   
                    var IsVerticlApplicable = cdetails.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (checkVerticalapplicable ==0)
                    {
                        mst_Vertical objVerti = new mst_Vertical()
                        {
                            VerticalName = "NA",
                            CustomerID = customerID
                        };
                        if (!UserManagementRisk.VerticalNameExist(objVerti))
                        {
                            UserManagementRisk.CreateVerticalName(objVerti);
                        }
                    }                   
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    List<RiskCategoryCreation> riskcategorycreationlistTest = new List<RiskCategoryCreation>();
                    //List<RiskCategoryCreation> riskcategorycreationlistTest1 = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist1 = new List<RiskCategoryCreation>();

                    #region Check Branch Wise Vertical Mapping Exists
                    for (int l = 0; l < LocationList.Count; l++)
                    {
                        if (checkVerticalapplicable == 0)
                        {
                            int verticalid= UserManagementRisk.VerticalgetBycustomerid(customerID);
                            int branchid = -1;
                            branchid=Convert.ToInt32(LocationList.ElementAt(l));
                            if (verticalid != -1 && branchid != -1)
                            {
                                BranchVertical BranchVerticals = new BranchVertical();
                                BranchVerticals.Branch = branchid;
                                BranchVerticals.CustomerID = customerID;
                                BranchVerticals.VerticalID = verticalid;
                                BranchVerticals.IsActive = true;
                                if (!UserManagementRisk.VerticalBrachExist(customerID, branchid, verticalid))
                                {
                                    UserManagementRisk.CreateVerticalBranchlist(BranchVerticals);
                                }
                            }

                            var verticallist = RiskCategoryManagement.getAssignedVerticalBranchWise(LocationList.ElementAt(l));
                            if (verticallist.Count == 0)
                            {
                                suucess = false;
                                String LocationName = CustomerBranchManagement.GetByID(LocationList.ElementAt(l)).Name;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Vertical Assigned to Location-" + LocationName + ".Please Assign Business Vertical before uploading of Risk Control Matrix.";
                                return;
                                break;
                            }
                            else
                                suucess = true;
                        }
                        else
                        {
                            var verticallist = RiskCategoryManagement.getAssignedVerticalBranchWise(LocationList.ElementAt(l));
                            if (verticallist.Count == 0)
                            {
                                suucess = false;
                                String LocationName = CustomerBranchManagement.GetByID(LocationList.ElementAt(l)).Name;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Vertical Assigned to Location-" + LocationName + ".Please Assign Business Vertical before uploading of Risk Control Matrix.";
                                return;
                                break;
                            }
                            else
                                suucess = true;
                        }                      
                    }
                    #endregion

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        string ActivityDescription = "";
                        string ControlObjective = "";
                        string ControlDescription = string.Empty;
                        string ControlNo = "";
                        string stringeffectivedate = string.Empty;
                        int processid = -1;
                        int subprocessid = -1;
                        int activityid = -1;
                        //1 Control No
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            ControlNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(ControlNo))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control No at row number - " + (count + 1) + " or Control No Not Empty.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //2 Process 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 2].Text.ToString().Trim(), @"\t|\n|\r", "") ,customerID);
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + (count + 1) + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //3 Sub Process                    
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            subprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 3].Text.ToString().Trim(), @"\t|\n|\r", "") , processid);
                        }
                        if (subprocessid == 0 || subprocessid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row number - " + (count + 1) + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //4 Activity
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            activityid = ProcessManagement.GetActivityBYName(processid, subprocessid, Regex.Replace(xlWorksheet.Cells[i, 4].Text.ToString().Trim(), @"\t|\n|\r", "") );
                        }
                        if (activityid == 0 || activityid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Activity Name at row number - " + (count + 1) + " or Activity not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //5 Risk Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {                           
                            // ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            ActivityDescription = Convert.ToString(GetSafeTagName(xlWorksheet.Cells[i, 5].Text.ToString().Trim())).Trim();
                        }

                        if (String.IsNullOrEmpty(ActivityDescription))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Risk/Activity Description at row number - " + (count + 1) + ".";
                            break;
                        }

                        //6 Control Objective
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            //ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            ControlObjective = Convert.ToString(GetSafeTagName(xlWorksheet.Cells[i, 6].Text.ToString().Trim())).Trim();                            
                        }

                        if (String.IsNullOrEmpty(ControlObjective))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Control Objective at row number - " + (count + 1) + ".";
                            break;
                        }

                        #region 7 Risk Category Mapping add to List 

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            string risk = xlWorksheet.Cells[i, 7].Text.ToString().Trim();

                            string[] split = risk.Split(',');

                            if (split.Length > 0)
                            {
                                string finalriskcategoryid = "";
                                string riskcategoryid = "";

                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    riskcategoryid = Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName(split[rs].ToString().Trim()));

                                    if (riskcategoryid == "0" || riskcategoryid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Risk Category Name at row number - " + (count + 1) + " or Risk Category Name not Defined in the System.";
                                        break;
                                    }
                                    else
                                    {
                                        suucess = true;
                                    }
                                    if (split.Length > 1)
                                    {
                                        finalriskcategoryid += riskcategoryid + ",";                                        
                                    }
                                    else
                                    {
                                        finalriskcategoryid += riskcategoryid;                                        
                                    }                                    
                                }
                                if (suucess)
                                {
                                    RiskIdProcess.Add(finalriskcategoryid.Trim(','));
                                }
                                else
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Correct the Risk Category Name at row number - " + (count + 1) + " or Risk Category Name not Defined in the System.";
                                    break;
                                }                                
                            }
                        }
                        else
                        {
                            RiskIdProcess.Add("0");
                        }
                        #endregion

                        #region Location - No Need to check for blank Values

                        //8 Customer Branch
                        //
                        //Customerbranchidcheck = Convert.ToInt32(ViewState["chklocation"]);     

                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        //{
                        //    Customerbranchidcheck = Convert.ToInt32(ViewState["chklocation"]);                            
                        //    //Customerbranchidcheck = RiskCategoryManagement.GetBranchIdByName(xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                        //}

                        //if (Customerbranchidcheck == 0 || Customerbranchidcheck == -1)
                        //{
                        //    suucess = false;
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Please Correct the Customer Branch Name at row number - " + (count + 1) + " or Customer Branch not Defined in the System.";
                        //    break;
                        //}
                        //else
                        //{
                        //    suucess = true;
                        //}

                        #endregion

                        #region Verticals Mapping add to List                        

                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        //{
                        //    string verticals = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                        //    string[] split = verticals.Split(',');
                        //    if (split.Length > 0)
                        //    {
                        //        string finalverticalsid = "";
                        //        string verticalsid = "";
                        //        for (int rs = 0; rs < split.Length; rs++)
                        //        {
                        //            verticalsid = Convert.ToString(RiskCategoryManagement.GetBranchVerticalIDByName(customerID, split[rs].ToString().Trim()));
                        //            if (verticalsid == "0" || verticalsid == "-1")
                        //            {
                        //                suucess = false;
                        //                cvDuplicateEntry.IsValid = false;
                        //                cvDuplicateEntry.ErrorMessage = "Please Correct the Verticals Name at row number - " + (count + 1) + " or Verticals Name not Defined in the System.";
                        //                break;
                        //            }
                        //            else
                        //            {
                        //                suucess = true;
                        //            }
                        //            if (split.Length > 1)
                        //            {
                        //                finalverticalsid += verticalsid + ",";
                        //            }
                        //            else
                        //            {
                        //                finalverticalsid += verticalsid;
                        //            }
                        //        }
                        //        BranchVerticalID.Add(finalverticalsid.Trim(','));
                        //    }
                        //}
                        //else
                        //{
                        //    BranchVerticalID.Add("0");
                        //}

                        #endregion

                        #region 8 Assertions Mapping --- add to List

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            string Assertions = xlWorksheet.Cells[i, 8].Text.ToString().Trim(',');
                            string[] splitAssertions = Assertions.Split(',');

                            if (splitAssertions.Length > 0)
                            {
                                string finalAssertionsmappigid = "";
                                string Assertionsmappigid = "";

                                for (int rs = 0; rs < splitAssertions.Length; rs++)
                                {
                                    Assertionsmappigid = Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName(splitAssertions[rs].ToString().Trim()));

                                    if (Assertionsmappigid == "0" || Assertionsmappigid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Assertions at row number - " + (count + 1) + " or Assertions not Defined in the System.";
                                        break;
                                    }
                                    else                                    
                                        suucess = true;                                   

                                    if (splitAssertions.Length > 1)
                                    {
                                        finalAssertionsmappigid += Assertionsmappigid + ",";
                                    }
                                    else
                                    {
                                        finalAssertionsmappigid += Assertionsmappigid;
                                    }
                                }
                                if (suucess)
                                {
                                    AssertionsIdProcess.Add(finalAssertionsmappigid.Trim(','));
                                }
                                else
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Correct the Assertions at row number - " + (count + 1) + " or Assertions not Defined in the System.";
                                    break;
                                }
                            }
                        }
                        else
                        {
                            AssertionsIdProcess.Add("0");
                        }

                        #endregion

                        //9 Control Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(ControlDescription))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Required Control Description at row number - " + (count + 1) + ".";
                            break;
                        }

                        //11 Process Owner
                        int personresponsibleIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                        {   //12 Process Owner Email
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            {
                                personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 11].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 12].Text.ToString().Trim());
                            }
                        }

                        if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Owner Name/Email at row number - " + (count + 1) + " or Process Owner not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //13 Control Owner
                        int ControlownerIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {   //14 Control Ownerr Email
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            {
                                ControlownerIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                            }
                        }
                        if (ControlownerIDcheck == 0 || ControlownerIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control Owner Name/Email at row number - " + (count + 1) + " or Control Owner not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //15 Effective Date                          
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text))
                            stringeffectivedate = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();

                        if (stringeffectivedate == "")
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check the Effective Date at row - " + (count + 1) + " or Effective Date can not be empty.";
                            break;
                        }
                        else
                        {
                            try
                            {
                                bool check = CheckDate(stringeffectivedate);
                                if (check)
                                {
                                    suucess = true;
                                }
                                else
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Check the Effective Date at row - " + (count + 1) + " or Effective Date can not be empty.";
                                    break;
                                }

                            }
                            catch (Exception ex)
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Check the Effective Date at row - " + (count + 1) + " or Effective Date can not be empty.";
                                break;
                            }
                        }
                        

                        //16 KC-1 Multiple risks addressed
                        int KC1check = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim()=="YES" )
                            {
                                KC1check = 1;
                            }
                            else if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "NO")
                            {
                                KC1check = 2;
                            }
                           // KC1check = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                        }
                        if (KC1check == 0 || KC1check == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the KC-1 Multiple risks addressed at row number - " + (count + 1) + ".";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //17 Key(KC-2 Criticality of control)
                        int KeyIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                        {
                            if (xlWorksheet.Cells[i, 17].Text.ToString().ToUpper().Trim() == "YES")
                            {
                                KeyIDcheck = 1;
                            }
                            else if (xlWorksheet.Cells[i, 17].Text.ToString().ToUpper().Trim() == "NO")
                            {
                                KeyIDcheck = 2;
                            }
                            //KeyIDcheck = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                        }
                        if (KeyIDcheck == 0 || KeyIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the KC-2 Criticality of control at row number - " + (count + 1) + ".";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //18 Primary / Secondary
                        int PrimarySecondaryIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                        {
                            PrimarySecondaryIDcheck = RiskCategoryManagement.GetPrimarySecondaryIdByName(Regex.Replace(xlWorksheet.Cells[i, 18].Text.ToString().Trim(), @"\t|\n|\r", ""));
                        }
                        if (PrimarySecondaryIDcheck == 0 || PrimarySecondaryIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Primary / Secondary  Name at row number - " + (count + 1) + " or Primary / Secondary not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;

                        //19 Preventive Control
                        int PrevationControlIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                        {
                            PrevationControlIDcheck = RiskCategoryManagement.GetPrevationControlIdByName(Regex.Replace(xlWorksheet.Cells[i, 19].Text.ToString().Trim(), @"\t|\n|\r", "")  );
                        }
                        if (PrevationControlIDcheck == 0 || PrevationControlIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Preventive/Detective Control Name at row number - " + (count + 1) + " or Preventive/Detective not Defined in the System.";
                            break;
                        }
                        else                       
                            suucess = true;

                        //20 Automated Control
                        int AutomatedControlIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                        {
                            AutomatedControlIDcheck = RiskCategoryManagement.GetAutomatedControlIDByName(Regex.Replace(xlWorksheet.Cells[i, 20].Text.ToString().Trim(), @"\t|\n|\r", "") );
                        }
                        if (AutomatedControlIDcheck == 0 || AutomatedControlIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Automated/Manual Control Name at row number - " + (count + 1) + " or Automated/Manual Control not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //21 Frequency
                        int Frequencycheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                        {
                            Frequencycheck = RiskCategoryManagement.GetFrequencyIDByName(Regex.Replace(xlWorksheet.Cells[i, 21].Text.ToString().Trim(), @"\t|\n|\r", "") );
                        }
                        if (Frequencycheck == 0 || Frequencycheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Frquency Name at row number - " + (count + 1) + " or Frquency not Defined in the System.";
                            break;
                        }
                        else
                            suucess = true;


                        //31 Industry
                        #region 31 Industry Mapping ---Add to List

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                        {
                            string industry = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                            string[] splitindustry = industry.Split(',');

                            if (splitindustry.Length > 0)
                            {
                                string finalindustrymappigid = "";
                                string industrymappigid = "";

                                for (int rs = 0; rs < splitindustry.Length; rs++)
                                {
                                    industrymappigid = Convert.ToString(RiskCategoryManagement.GetIndustryIdByName(splitindustry[rs].ToString().Trim()));

                                    if (industrymappigid == "0" || industrymappigid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Industry at row number - " + (count + 1) + " or Industry not Defined in the System.";
                                        break;
                                    }
                                    else
                                        suucess = true;                                    

                                    if (splitindustry.Length > 1)
                                    {
                                        finalindustrymappigid += industrymappigid + ",";
                                    }
                                    else
                                    {
                                        finalindustrymappigid += industrymappigid;
                                    }
                                }
                                if (suucess)
                                {
                                    IndustryIdProcess.Add(finalindustrymappigid.Trim(','));
                                }
                                else
                                {
                                    suucess = false;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Correct the Industry at row number - " + (count + 1) + " or Industry not Defined in the System.";
                                    break;
                                }
                            }
                        }
                        else
                        {
                            IndustryIdProcess.Add("0");
                        }
                        #endregion

                        //32 Location Type
                        int LocationType = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                        {
                            LocationType = ProcessManagement.GetLocationTypeID(Convert.ToString(xlWorksheet.Cells[i, 32].Text).Trim(), CustomerID);
                        }
                        if (LocationType == 0 || LocationType == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Location Type at row number - " + (count + 1) + " or Location Type not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //33 Risk Rating
                        int RiskRatingcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                        {
                            RiskRatingcheck = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 33].Text).Trim());
                        }
                        if (RiskRatingcheck == 0 || RiskRatingcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Risk Rating at row number - " + (count + 1) + " or Risk Rating not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //34 Control Rating
                        int ControlRatingCheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                        {
                            ControlRatingCheck = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 34].Text).Trim());
                        }
                        if (ControlRatingCheck == 0 || ControlRatingCheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control Rating at row number - " + (count + 1) + " or Control Rating not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        //35 Process Scores
                        int processscores = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                        {
                            processscores = Convert.ToInt32(xlWorksheet.Cells[i, 35].Text);
                        }

                        int Customerbranchidcheck = -1;

                        LocationList.ForEach(EachLocation =>
                        {
                            Customerbranchidcheck = Convert.ToInt32(EachLocation);

                            if (!(RiskCategoryManagement.ExistsNew(Convert.ToString(ActivityDescription), Convert.ToString(ControlObjective), Convert.ToInt32(processid), Convert.ToInt32(LocationType), customerID, Customerbranchidcheck, subprocessid,ControlNo)))
                            {
                                if (!riskcategorycreationlist.Any(x => x.ActivityDescription.ToUpper().Trim() == ActivityDescription.ToUpper().Trim()
                                    && x.ControlObjective.ToUpper().Trim() == ControlObjective.ToUpper().Trim()
                                    && x.ProcessId == processid && x.SubProcessId==subprocessid
                                    && x.LocationType == LocationType && x.CustomerId == customerID 
                                    && x.CustomerBranchId == Customerbranchidcheck
                                    && x.ControlNo.ToUpper().Trim() == ControlNo.ToUpper().Trim()))
                                {
                                    RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation();
                                    riskcategorycreation.ControlNo = ControlNo;
                                    riskcategorycreation.ProcessId = processid;
                                    riskcategorycreation.SubProcessId = subprocessid;
                                    riskcategorycreation.ActivityDescription = ActivityDescription;
                                    riskcategorycreation.ControlObjective = ControlObjective;
                                    riskcategorycreation.IsDeleted = false;
                                    riskcategorycreation.LocationType = LocationType;
                                    riskcategorycreation.IsInternalAudit = "N";
                                    riskcategorycreation.CustomerId = customerID;
                                    riskcategorycreation.CustomerBranchId = Customerbranchidcheck;
                                    riskcategorycreationlist.Add(riskcategorycreation);
                                }
                            }//exists end
                            else
                            {
                                //New Condition Added by rahul on 23 SEP 2016
                                if (!riskcategorycreationlistTest.Any(x => x.ActivityDescription.ToUpper().Trim() == ActivityDescription.ToUpper().Trim()
                                && x.ControlObjective.ToUpper().Trim() == ControlObjective.ToUpper().Trim()
                                && x.ProcessId == processid && x.SubProcessId==subprocessid 
                                && x.LocationType == LocationType && x.CustomerId == customerID 
                                && x.CustomerBranchId == Customerbranchidcheck && x.ControlNo.ToUpper().Trim() == ControlNo.ToUpper().Trim()))
                                {
                                    RiskCategoryCreation riskcategorycreationtest = new RiskCategoryCreation();
                                    riskcategorycreationtest.ControlNo = ControlNo;
                                    riskcategorycreationtest.ProcessId = processid;
                                    riskcategorycreationtest.SubProcessId = subprocessid;
                                    riskcategorycreationtest.ActivityDescription = ActivityDescription;
                                    riskcategorycreationtest.ControlObjective = ControlObjective;
                                    riskcategorycreationtest.IsDeleted = false;
                                    riskcategorycreationtest.LocationType = LocationType;
                                    riskcategorycreationtest.IsInternalAudit = "N";
                                    riskcategorycreationtest.CustomerId = customerID;
                                    riskcategorycreationtest.CustomerBranchId = Customerbranchidcheck;
                                    //riskcategorycreationlistTest.Add(riskcategorycreationtest);
                                    riskcategorycreationlist.Add(riskcategorycreationtest);
                                    
                                }
                            }
                        });
                    }


                    if (suucess)
                    {
                        riskcategorycreationlist1 = riskcategorycreationlist.Where(entry => entry.ProcessId == 0 && entry.SubProcessId == 0).Distinct().ToList();

                        if (riskcategorycreationlist1.Count == 0)
                        {
                            suucess = RiskCategoryManagement.CreateExcel(riskcategorycreationlist);

                            #region 7 Riskcategory Mapping Save

                            if (RiskIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < RiskIdProcess.Count; i++)
                                    {
                                        string aItem = RiskIdProcess[i].ToString();

                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        RiskCategoryId = Convert.ToInt32(split[rs]),
                                                        ProcessId = entry.ProcessId,
                                                        SubProcessId = entry.SubProcessId,
                                                        IsActive = true,                                                        
                                                    };
                                                    RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                                }
                                                RiskIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    RiskCategoryId = Convert.ToInt32(aItem),
                                                    ProcessId = entry.ProcessId,
                                                    SubProcessId = entry.SubProcessId,
                                                    IsActive = true,                                                    
                                                };
                                                RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                            }

                                            RiskIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                RiskIdProcess.Clear();
                            }
                            #endregion

                            #region 30 Industry Mapping Save

                            if (IndustryIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < IndustryIdProcess.Count; i++)
                                    {
                                        string aItem = IndustryIdProcess[i].ToString();
                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        IndustryID = Convert.ToInt32(split[rs]),
                                                        ProcessId = entry.ProcessId,
                                                        SubProcessId = entry.SubProcessId,
                                                        IsActive = true,
                                                        EditedDate = DateTime.Now,
                                                        EditedBy = Convert.ToInt32(Session["userID"]),                                                        
                                                    };
                                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                                }
                                                IndustryIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                IndustryMapping IndustryMapping = new IndustryMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    IndustryID = Convert.ToInt32(aItem),
                                                    ProcessId = entry.ProcessId,
                                                    SubProcessId = entry.SubProcessId,
                                                    IsActive = true,
                                                    EditedDate = DateTime.Now,
                                                    EditedBy = Convert.ToInt32(Session["userID"]),                                                    
                                                };
                                                RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                            }
                                            IndustryIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                IndustryIdProcess.Clear();
                            }
                            #endregion

                            #region 8 Assertions Mapping Save

                            if (AssertionsIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < AssertionsIdProcess.Count; i++)
                                    {
                                        string aItem = AssertionsIdProcess[i].ToString();
                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        AssertionId = Convert.ToInt32(split[rs]),
                                                        ProcessId = entry.ProcessId,
                                                        SubProcessId = entry.SubProcessId,
                                                        IsActive = true,                                                        
                                                    };
                                                    RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                                }
                                                AssertionsIdProcess.Remove(aItem);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    AssertionId = Convert.ToInt32(aItem),
                                                    ProcessId = entry.ProcessId,
                                                    SubProcessId = entry.SubProcessId,
                                                    IsActive = true,                                                    
                                                };
                                                RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                            }
                                            AssertionsIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                AssertionsIdProcess.Clear();
                            }
                            #endregion

                            List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                            List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();
                            List<Internal_AuditAssignment> objInternal_AuditAssignmentList = new List<Internal_AuditAssignment>();
                            int processid = -1;
                            int subprocessid = -1;
                            int activityid = -1;
                            int personresponsibleID = 0;
                            int ControlownerID = 0;
                            int KeyID = 0;
                            int KCONEID = 0;
                            
                            int PrevationControlID = 0;
                            int PrimarySecondaryID = 0;
                            int AutomatedControlID = 0;
                            int Frequency = 0;
                            int Customerbranchid = -1;
                            int LocationType = -1;
                            int riskcreationId = -1;

                            string ControlNo = "";
                            string ActivityDescription = "";
                            string ControlObjective = "";
                            string ControlDescription = string.Empty;
                            string MControlDescription = string.Empty;
                            DateTime effectivedate = new DateTime();
                            string GapDescription = "";
                            string Recommendations = "";
                            string ActionRemediationplan = "";
                            string IPE = "";
                            string ERPsystem = "";
                            string FRC = "";
                            string UniqueReferred = "";
                            string TestStrategy = "";
                            string DocumentsExamined = "";

                            LocationList.ForEach(EachLocation =>
                            {
                                for (int i = 2; i <= xlrow2; i++)
                                {
                                    processid = -1;
                                    subprocessid = -1;
                                    activityid = -1;
                                    personresponsibleID = 0;
                                    KeyID = 0;
                                    PrevationControlID = 0;
                                    AutomatedControlID = 0;
                                    Frequency = 0;
                                    Customerbranchid = -1;
                                    LocationType = -1;
                                    riskcreationId = -1;

                                    ControlNo = "";
                                    ActivityDescription = "";
                                    ControlObjective = "";
                                    ControlDescription = string.Empty;
                                    MControlDescription = string.Empty;
                                    effectivedate = new DateTime();
                                    GapDescription = "";
                                    Recommendations = "";
                                    ActionRemediationplan = "";
                                    IPE = "";
                                    ERPsystem = "";
                                    FRC = "";
                                    UniqueReferred = "";
                                    TestStrategy = "";
                                    DocumentsExamined = "";

                                    //Location
                                    Customerbranchid = Convert.ToInt32(EachLocation);

                                    //Find All Vertical of Above Location in BranchVerticalID List
                                    BranchVerticalID = RiskCategoryManagement.getAssignedVerticalBranchWise(Customerbranchid);

                                    //1 Control No
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                    {
                                        ControlNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                                    }

                                    //2 ProcessName	
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                                    {
                                        processid = ProcessManagement.GetProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 2].Text.ToString().Trim(), @"\t|\n|\r", "") , customerID);
                                    }

                                    //3 SubProcessName
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                    {
                                        subprocessid = ProcessManagement.GetSubProcessIDByName(Regex.Replace(xlWorksheet.Cells[i, 3].Text.ToString().Trim(), @"\t|\n|\r", "") , processid);
                                    }
                                    //4 Activity
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                    {
                                        activityid = ProcessManagement.GetActivityBYName(processid, subprocessid, Regex.Replace(xlWorksheet.Cells[i, 4].Text.ToString().Trim(), @"\t|\n|\r", "") );
                                    }
                                   
                                    //5 Risk Description
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                    {
                                        //ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                        ActivityDescription = Convert.ToString(GetSafeTagName(xlWorksheet.Cells[i, 5].Text.ToString().Trim())).Trim();

                                    }

                                    //6 Control Objective
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                    {
                                        //ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                         ControlObjective = Convert.ToString(GetSafeTagName(xlWorksheet.Cells[i, 6].Text.ToString().Trim())).Trim();
                                    }

                                    //7--RiskCategory, 8---Assertions


                                    //9 Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                                    {
                                        ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                                    }

                                    //10 Mitigating Control Description
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                    {
                                        MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim();
                                    }

                                    //11 Process Owner
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                    {   //12 Process Owner Email
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                        {
                                            personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 11].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 12].Text.ToString().Trim());
                                        }
                                    }

                                    //13 Control Owner
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                    {   //14 Control Ownerr Email
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                        {
                                            ControlownerID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                                        }
                                    }


                                    //15 Effective Date	                          
                                    //DateTime dt = new DateTime();
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                                    {
                                        string c = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();
                                        DateTime? aaaa = CleanDateField(c);
                                        DateTime dt1 = Convert.ToDateTime(aaaa);
                                        effectivedate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                    }

                                    //16 KC-1 Multiple risks addressed
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "YES")
                                        {
                                            KCONEID = 1;
                                        }
                                        else if (xlWorksheet.Cells[i, 16].Text.ToString().ToUpper().Trim() == "NO")
                                        {
                                            KCONEID = 2;
                                        }
                                        // KCONEID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                                    }

                                    //17 Key(KC-2 Criticality of control)
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheet.Cells[i, 17].Text.ToString().ToUpper().Trim() == "YES")
                                        {
                                            KeyID = 1;
                                        }
                                        else if (xlWorksheet.Cells[i, 17].Text.ToString().ToUpper().Trim() == "NO")
                                        {
                                            KeyID = 2;
                                        }
                                        // KeyID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                                    }

                                    //18 Primary / Secondary
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                                    {
                                        PrimarySecondaryID = RiskCategoryManagement.GetPrimarySecondaryIdByName(Regex.Replace(xlWorksheet.Cells[i, 18].Text.ToString().Trim(), @"\t|\n|\r", "") );
                                    }

                                    //19 Preventive Control
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                    {
                                        PrevationControlID = RiskCategoryManagement.GetPrevationControlIdByName(Regex.Replace(xlWorksheet.Cells[i, 19].Text.ToString().Trim(), @"\t|\n|\r", "") );
                                    }

                                    //20 Automated Control
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                                    {
                                        AutomatedControlID = RiskCategoryManagement.GetAutomatedControlIDByName(Regex.Replace(xlWorksheet.Cells[i, 20].Text.ToString().Trim(), @"\t|\n|\r", "") );
                                    }

                                    //21 Frequency
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                                    {
                                        Frequency = RiskCategoryManagement.GetFrequencyIDByName(xlWorksheet.Cells[i, 21].Text.ToString().Trim());
                                    }

                                    //22 Gap Description
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                                    {
                                        GapDescription = Convert.ToString(Regex.Replace(xlWorksheet.Cells[i, 22].Text.ToString().Trim(), @"\t|\n|\r", "") );
                                    }

                                    //23 Recommendations
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                                    {
                                        Recommendations = Convert.ToString(xlWorksheet.Cells[i, 23].Text.ToString().Trim());
                                    }

                                    //24 Action Remediation Plan
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                                    {
                                        ActionRemediationplan = Convert.ToString(xlWorksheet.Cells[i, 24].Text.ToString().Trim());
                                    }

                                    //25 Information Produced by Entity
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                    {
                                        IPE = Convert.ToString(xlWorksheet.Cells[i, 25].Text.ToString().Trim());
                                    }

                                    //26 ERP System	
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                                    {
                                        ERPsystem = Convert.ToString(xlWorksheet.Cells[i, 26].Text.ToString().Trim());
                                    }

                                    //27 Fraud Risk Control	
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                                    {
                                        FRC = Convert.ToString(xlWorksheet.Cells[i, 27].Text.ToString().Trim());
                                    }

                                    //28 Unique Referred
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim().Trim()))
                                    {
                                        UniqueReferred = Convert.ToString(xlWorksheet.Cells[i, 28].Text.ToString().Trim());
                                    }

                                    //29 Test Strategy
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim().Trim()))
                                    {
                                        TestStrategy = Convert.ToString(xlWorksheet.Cells[i, 29].Text.ToString().Trim());
                                    }

                                    //30 Documents Examined
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                                    {
                                        DocumentsExamined = Convert.ToString(xlWorksheet.Cells[i, 30].Text.ToString().Trim());
                                    }
                                    //31 Industry Mapping

                                    //32 Location Type
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                                    {
                                        LocationType = ProcessManagement.GetLocationTypeID(Convert.ToString(xlWorksheet.Cells[i, 32].Text).Trim(), CustomerID);
                                    }

                                    //33 RiskRating
                                    int RiskRating = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                                    {
                                        RiskRating = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 33].Text).Trim());
                                    }

                                    //34 ControlRating
                                    int ControlRating = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                                    {
                                        ControlRating = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 34].Text).Trim());
                                    }
                                    //35 process scores
                                    int processscores = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                                    {
                                        processscores = Convert.ToInt32(xlWorksheet.Cells[i, 35].Text);
                                    }
                                    //string ccc = Regex.Replace(ActivityDescription.Trim(), @"\t|\n|\r", "");
                                    //string ccc1 = Regex.Replace(ControlObjective.Trim(), @"\t|\n|\r", "");
                                    //string ccc2 = Regex.Replace(xlWorksheet.Cells[i, 18].Text.ToString().Trim(), @"\t|\n|\r", "");
                                    riskcreationId = RiskCategoryManagement.GetRiskCreationIdNew(Convert.ToString(ActivityDescription), Convert.ToString(ControlObjective), Convert.ToInt32(processid), Convert.ToInt32(LocationType), customerID, Customerbranchid, subprocessid, ControlNo);
                                    if (riskcreationId==0)
                                    {

                                    }
                                    if (BranchVerticalID.Count > 0)
                                    {
                                        for (int BV = 0; BV < BranchVerticalID.Count; BV++)
                                        {
                                            if (!(RiskCategoryManagement.Exists(processid, subprocessid, riskcreationId, Convert.ToInt32(Frequency), Customerbranchid, BranchVerticalID[BV], activityid, ControlDescription)))
                                            {
                                                RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                riskactivitytransaction.VerticalsId = BranchVerticalID[BV];
                                                riskactivitytransaction.ProcessId = processid;
                                                riskactivitytransaction.SubProcessId = subprocessid;
                                                riskactivitytransaction.RiskCreationId = riskcreationId;
                                                riskactivitytransaction.ControlDescription = ControlDescription;
                                                riskactivitytransaction.MControlDescription = MControlDescription;
                                                riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                riskactivitytransaction.EffectiveDate = effectivedate.Date;
                                                riskactivitytransaction.Key_Value = KeyID;
                                                riskactivitytransaction.PrevationControl = PrevationControlID;
                                                riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                riskactivitytransaction.Frequency = Frequency;
                                                riskactivitytransaction.IsDeleted = false;
                                                riskactivitytransaction.GapDescription = GapDescription;
                                                riskactivitytransaction.Recommendations = Recommendations;
                                                riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                riskactivitytransaction.IPE = IPE;
                                                riskactivitytransaction.ERPsystem = ERPsystem;
                                                riskactivitytransaction.FRC = FRC;
                                                riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                riskactivitytransaction.TestStrategy = TestStrategy;
                                                riskactivitytransaction.RiskRating = RiskRating;
                                                riskactivitytransaction.ControlRating = ControlRating;
                                                riskactivitytransaction.ProcessScore = processscores;
                                                riskactivitytransaction.ActivityID = activityid;
                                                riskactivitytransaction.KC2 = KCONEID;
                                                riskactivitytransaction.Primary_Secondary = PrimarySecondaryID;
                                                riskactivitytransaction.ControlOwner = ControlownerID;
                                                riskactivitytransaction.ISICFR_Close = true;

                                                riskactivitytransactionlist.Add(riskactivitytransaction);

                                                if (!(RiskCategoryManagement.Exists_Internal_AuditAssignment(Customerbranchid, BranchVerticalID[BV], customerID)))
                                                {
                                                    Internal_AuditAssignment InA = new Internal_AuditAssignment();
                                                    InA.AssignedTo = "I";
                                                    InA.VerticalID = BranchVerticalID[BV];
                                                    InA.CustomerBranchid = Customerbranchid;
                                                    InA.ExternalAuditorId = -1;
                                                    InA.CreatedBy = Common.AuthenticationHelper.UserID;
                                                    InA.CreatedOn = DateTime.Now.Date;
                                                    InA.IsActive = false;
                                                    InA.CustomerId = (int) Common.AuthenticationHelper.CustomerID;
                                                    objInternal_AuditAssignmentList.Add(InA);
                                                    var auditassigment = RiskCategoryManagement.CreateAuditAssignment(objInternal_AuditAssignmentList);
                                                }
                                                
                                                #region Previous Old Code
                                                //BranchVerticalID.Remove(aItem);

                                                //string aItem = BranchVerticalID[BV].ToString();
                                                //if (aItem.ToString().Contains(","))
                                                //{
                                                //    string[] split = aItem.ToString().Split(',');
                                                //    if (split.Length > 0)
                                                //    {
                                                //        for (int rs = 0; rs < split.Length; rs++)
                                                //        {
                                                //            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                //            riskactivitytransaction.ProcessId = processid;
                                                //            riskactivitytransaction.SubProcessId = subprocessid;
                                                //            riskactivitytransaction.RiskCreationId = riskcreationId;
                                                //            riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                //            riskactivitytransaction.ControlDescription = ControlDescription;
                                                //            riskactivitytransaction.MControlDescription = MControlDescription;
                                                //            riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                //            riskactivitytransaction.EffectiveDate = dt.Date;
                                                //            riskactivitytransaction.Key_Value = KeyID;
                                                //            riskactivitytransaction.PrevationControl = PrevationControlID;
                                                //            riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                //            riskactivitytransaction.Frequency = Frequency;
                                                //            riskactivitytransaction.IsDeleted = false;
                                                //            riskactivitytransaction.GapDescription = GapDescription;
                                                //            riskactivitytransaction.Recommendations = Recommendations;
                                                //            riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                //            riskactivitytransaction.IPE = IPE;
                                                //            riskactivitytransaction.ERPsystem = ERPsystem;
                                                //            riskactivitytransaction.FRC = FRC;
                                                //            riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                //            riskactivitytransaction.TestStrategy = TestStrategy;
                                                //            riskactivitytransaction.RiskRating = RiskRating;
                                                //            riskactivitytransaction.ControlRating = ControlRating;
                                                //            riskactivitytransaction.ProcessScore = processscores;
                                                //            riskactivitytransaction.VerticalsId = Convert.ToInt32(split[rs]);
                                                //            riskactivitytransaction.ISICFR_Close = true;
                                                //            riskactivitytransactionlist.Add(riskactivitytransaction);
                                                //        }
                                                //        BranchVerticalID.Remove(aItem);
                                                //        break;
                                                //    }
                                                //}
                                                //else
                                                //{
                                                //    if (aItem != "0")
                                                //    {

                                                //        RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                //        riskactivitytransaction.ProcessId = processid;
                                                //        riskactivitytransaction.SubProcessId = subprocessid;
                                                //        riskactivitytransaction.RiskCreationId = riskcreationId;
                                                //        riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                //        riskactivitytransaction.ControlDescription = ControlDescription;
                                                //        riskactivitytransaction.MControlDescription = MControlDescription;
                                                //        riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                //        riskactivitytransaction.EffectiveDate = dt.Date;
                                                //        riskactivitytransaction.Key_Value = KeyID;
                                                //        riskactivitytransaction.PrevationControl = PrevationControlID;
                                                //        riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                //        riskactivitytransaction.Frequency = Frequency;
                                                //        riskactivitytransaction.IsDeleted = false;
                                                //        riskactivitytransaction.GapDescription = GapDescription;
                                                //        riskactivitytransaction.Recommendations = Recommendations;
                                                //        riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                //        riskactivitytransaction.IPE = IPE;
                                                //        riskactivitytransaction.ERPsystem = ERPsystem;
                                                //        riskactivitytransaction.FRC = FRC;
                                                //        riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                //        riskactivitytransaction.TestStrategy = TestStrategy;
                                                //        riskactivitytransaction.RiskRating = RiskRating;
                                                //        riskactivitytransaction.ControlRating = ControlRating;
                                                //        riskactivitytransaction.ProcessScore = processscores;
                                                //        riskactivitytransaction.VerticalsId = Convert.ToInt32(aItem);
                                                //        riskactivitytransaction.ISICFR_Close = true;
                                                //        riskactivitytransactionlist.Add(riskactivitytransaction);
                                                //    }
                                                //    BranchVerticalID.Remove(aItem);
                                                //    break;
                                                //}
                                                #endregion
                                            }//exists end 
                                            else
                                            {

                                            }
                                        }

                                        BranchVerticalID.Clear();
                                    }
                                } //End Of Second For Loop                                

                            }); //Location List Loop                           

                            riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.ProcessId == 0 && entry.RiskCreationId == 0).ToList();

                            if (riskactivitytransactionlist1.Count == 0)
                            {
                                suucess = RiskCategoryManagement.CreateExcelTransaction(riskactivitytransactionlist);
                                suucess = true;
                            }
                            else
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "File Upload Error!!. Please Upload the Excel File in Appropriate Format.";
                            }
                        }
                        else
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "File Upload Error!!. Please Upload the Excel File in Appropriate Format.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }


       
        #endregion

        #region Export  Risk Control Matrix
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }     

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindProcess()
        {
            long customerID = -1;
            //customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            ddlFilterProcess.DataTextField = "Name";
            ddlFilterProcess.DataValueField = "ID";
            ddlFilterProcess.Items.Clear();
            ddlFilterProcess.DataSource = ProcessManagement.GetAllProcess(customerID);
            ddlFilterProcess.DataBind();
            ddlFilterProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                    }
                    else
                    {
                        if (ddlFilterSubProcess.Items.Count > 0)
                            ddlFilterSubProcess.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterSubProcess.Items.Count > 0)
                        ddlFilterSubProcess.Items.Clear();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                    }
                    else
                    {
                        if (ddlFilterSubProcess.Items.Count > 0)
                            ddlFilterSubProcess.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterSubProcess.Items.Count > 0)
                        ddlFilterSubProcess.Items.Clear();
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }

        public void BindGridData()
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
                bindPageNumber();
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                BindData("N");
                bindPageNumber();
            }
        }

        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    this.BindData("P");
                    bindPageNumber();
                }
                else
                {
                    this.BindData("N");
                    bindPageNumber();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindData(string Flag)
        {
            try
            {
                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;

                String FilterText = String.Empty;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }

                if (txtFilter.Text != "")
                    FilterText = txtFilter.Text;
              
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();

                if (Flag == "P")
                {
                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetRiskControlMatrixDataForUpdate(customerID, Branchlist.ToList(), processid, subprocessid,  LocationType, FilterText);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                    Session["grdFilterRCMData"] = (grdRiskActivityMatrix.DataSource as List<FilterRiskControlMatrixForUpdate>).ToDataTable();
                }
                else
                {
                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetRiskControlMatrixDataForUpdate(customerID, Branchlist.ToList(), processid, subprocessid,  LocationType, FilterText);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                    Session["grdFilterRCMData"] = (grdRiskActivityMatrix.DataSource as List<FilterRiskControlMatrixForUpdate>).ToDataTable();
                }

               // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }               
        
        public string ShowLocationType(int locationtypeid)
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            string LocationType = "";
            LocationType = ProcessManagement.GetLocationTypeName(locationtypeid, CustomerID);
            return LocationType;
        }

        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId, customerID);
            return processnonprocess;
        }

        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }

        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }

        public string ShowIndustryName(long categoryid)
        {
            List<IndustryName_Result> a = new List<IndustryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetIndustryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public string ShowCustomerBranchName(long categoryid, long branchid)
        {
            List<CustomerBranchNameLocationWise_Result> a = new List<CustomerBranchNameLocationWise_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedureLocationWise(Convert.ToInt32(categoryid), Convert.ToInt32(branchid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }       

        
        #endregion

        private void ProcessData(ExcelPackage xlWorkbook)
        {
            try
            {                
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ProcessSubProcess"];
                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (xlWorksheet != null)
                {
                   
                    int count = 1;
                    int processid = -1;
                    string processName = string.Empty;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<mst_Subprocess> mstSubprocesslist = new List<mst_Subprocess>();
                    List<mst_Subprocess> mstSubprocesslist1 = new List<mst_Subprocess>();

                    List<ActivityDetails> ActivityDetailList = new List<ActivityDetails>();
                    List<ActivityDetails> ActivityDetailList1 = new List<ActivityDetails>();
                    List<mst_Activity> mstActivitylist = new List<mst_Activity>();

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;                        
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 1].Text.ToString(), customerID);
                            processName=Convert.ToString(xlWorksheet.Cells[i, 1].Text.ToString().Trim());
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + count + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        string isprocessnonprocess = "";
                        if (xlWorksheet.Cells[i, 3].Value != null)
                        {
                            if (xlWorksheet.Cells[i, 3].Value.ToString().Trim() == "P")
                            {
                                isprocessnonprocess = "P";
                            }
                            else
                            {
                                isprocessnonprocess = "N";
                            }
                        }
                        string subprocessname = "";
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            subprocessname = Convert.ToString(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (string.IsNullOrEmpty(subprocessname))
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row number - " + count + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        decimal AuditScores = 0;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            AuditScores = Convert.ToDecimal(xlWorksheet.Cells[i, 4].Text.ToString().Trim());
                        }
                        string activityname = "";
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            activityname = Convert.ToString(xlWorksheet.Cells[i, 5].Text.ToString().Trim());
                        }
                        //if (string.IsNullOrEmpty(activityname))
                        //{
                        //    suucess = false;
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Please Correct the Activity Name at row number - " + count + " or Activity not Defined in the System.";
                        //    break;
                        //}
                        //else
                        //{
                        //    suucess = true;
                        //}                                                                                
                                                                                             
                        if (!(ProcessManagement.ExistsProcess(processid, Convert.ToString(Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "")))))
                        {
                            if (!mstSubprocesslist.Any(x => x.ProcessId == processid && x.Name.ToUpper().Trim() == Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "").ToUpper().Trim()))
                            {
                                mst_Subprocess mstSubprocess = new mst_Subprocess();
                                mstSubprocess.Name = Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "");
                                mstSubprocess.ProcessId = processid;
                                mstSubprocess.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                mstSubprocess.IsDeleted = false;
                                mstSubprocess.CreatedOn = DateTime.Now;
                                mstSubprocess.IsProcessNonProcess = isprocessnonprocess;
                                mstSubprocess.Scores = AuditScores;
                                mstSubprocesslist.Add(mstSubprocess);
                            }
                        }//sub Process exists end

                        if (!string.IsNullOrEmpty(activityname))
                        {
                            ActivityDetails activity_details = new ActivityDetails();
                            activity_details.ProcessName = Regex.Replace(processName.Trim(), @"\t|\n|\r", "");
                            activity_details.ProcessId = processid;
                            activity_details.SubProcessName = Regex.Replace(subprocessname.Trim(), @"\t|\n|\r", "");
                            activity_details.ActivityName = Regex.Replace(activityname.Trim(), @"\t|\n|\r", "");
                            ActivityDetailList.Add(activity_details);
                        }
                        
                    }
                    mstSubprocesslist1 = mstSubprocesslist.Where(entry => entry.ProcessId == 0).ToList();
                    ActivityDetailList1 = ActivityDetailList.Where(entry => entry.ProcessId == 0).ToList();
                    if (mstSubprocesslist1.Count == 0)
                    {
                        if (processid != 0)
                        {
                            if (processid != -1)
                            {
                                suucess = ProcessManagement.CreateExcelProcess(mstSubprocesslist);
                                if (suucess)
                                {
                                    if (ActivityDetailList1.Count == 0)
                                    {
                                        if (ActivityDetailList.Count > 0)
                                        {
                                            long subprocessid = -1;
                                            ActivityDetailList.ForEach(entry =>
                                            {
                                                subprocessid = ProcessManagement.GetSubProcessBYName(entry.ProcessId, entry.SubProcessName);
                                                if (subprocessid != -1)
                                                {
                                                    if (!(ProcessManagement.ExistsActivity(entry.ProcessId, subprocessid, Regex.Replace(entry.ActivityName.Trim(), @"\t|\n|\r", ""))))
                                                    {
                                                        if (!mstActivitylist.Any(x => x.ProcessId == entry.ProcessId && x.SubProcessId == subprocessid && x.Name.ToUpper().Trim() == Regex.Replace(entry.ActivityName.Trim(), @"\t|\n|\r", "").ToUpper().Trim()))
                                                        {
                                                            mst_Activity mstactivity = new mst_Activity();
                                                            mstactivity.Name = entry.ActivityName;
                                                            mstactivity.ProcessId = entry.ProcessId;
                                                            mstactivity.SubProcessId = subprocessid;
                                                            mstactivity.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            mstactivity.IsDeleted = false;
                                                            mstactivity.CreatedOn = DateTime.Now;
                                                            mstActivitylist.Add(mstactivity);
                                                        }
                                                    }
                                                }
                                            });
                                            suucess = ProcessManagement.CreateExcelActivity(mstActivitylist);
                                            suucess = true;
                                        }
                                    }
                                }
                                else
                                {
                                    suucess = false;
                                }                                                               
                            }
                        }
                    }
                    else
                    {
                        suucess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool ProcessSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ProcessSubProcess"))
                    {
                        if (sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }        

        protected void rdoCompliance_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoRCMUpload.Checked == true)
                MasterFileUpload.Visible = true;

            Divbranchlist.Visible = true;
            BindLocationFilter();
        }

        protected void rdoSubProcess_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSubProcess.Checked == true)
                MasterFileUpload.Visible = true;

            Divbranchlist.Visible = false;
        }

        protected void txtFilter_TextChanged(object sender, EventArgs e)
        {
            BindGridData();
        }

        protected void Tab1_Click1(object sender, EventArgs e)
        {           
            try
            {
                performerdocuments.Visible = true;
                reviewerdocuments.Visible = false;


                liNotDone.Attributes.Add("class", "active");
                liSubmitted.Attributes.Add("class", "");

                //Tab1.Attributes.Add("class", "active");
                //Tab2.Attributes.Add("class", "");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        

        protected void sampleForm_Click(object sender, EventArgs e)
        {
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            //string filePath = lblresume.Text;
            string filePath = Server.MapPath("~/AuditSampleDocument/RiskControlMatrix_Sample.xlsx");      
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=RiskControlMatrix_Sample.xlsx");
            byte[] data = req.DownloadData(filePath);
          
           // response.End();
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
           // Response.BinaryWrite(fileBytes);
            response.BinaryWrite(data);
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Flush();
            Response.End();

            
          
            //string filename = "RiskControlMatrix_Sample.xlsx";          
            //string Filpath = Server.MapPath("~/AuditSampleDocument/RiskControlMatrix_Sample.xlsx");       
            //System.IO.FileInfo file = new System.IO.FileInfo(Filpath);
            //    if (file.Exists)
            //    {
            //        Response.Clear();
            //        Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            //        Response.AddHeader("Content-Length", file.Length.ToString());
            //        Response.ContentType = "application/octet-stream";
            //    }


                           

            //using (ZipFile AuditZip = new ZipFile())
            //{
            //    string filePath = Server.MapPath("~/AuditSampleDocument/RiskControlMatrix_Sample.xlsx");
            //    System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            //    //string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
            //    if (File.Exists(filePath))
            //    {
            //        string[] filename = file.Name.Split('.');
            //        //string str = filename[0] + "." + filename[1];
            //       // string str = Sampleform;
            //        string str = "RiskControlMatrix_Sample.xlsx";
            //        AuditZip.AddEntry(str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));

            //    }

            //    var zipMs = new MemoryStream();
            //    AuditZip.Save(zipMs);
            //    zipMs.Position = 0;
            //    byte[] Filedata = zipMs.ToArray();
            //    Response.Buffer = true;
            //    Response.ClearContent();
            //    Response.ClearHeaders();
            //    Response.Clear();
            //    Response.ContentType = "application/zip";
            //    Response.AddHeader("content-disposition", "attachment; filename=SampleForm.zip");
            //    Response.BinaryWrite(Filedata);
            //    Response.Flush();
            //    Response.End();
            //}
                          
        }

        protected void Tab2_Click1(object sender, EventArgs e)
        {           
            try
            {
                if (HttpContext.Current.Cache.Get("MasterRCMData") != null)
                    HttpContext.Current.Cache.Remove("MasterRCMData");

                BindLegalEntityData();
                BindProcess();

                txtFilter.Text = "";

                BindData("P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                performerdocuments.Visible = false;
                reviewerdocuments.Visible = true;
                Divbranchlist.Visible = false;

                liNotDone.Attributes.Add("class", "");
                liSubmitted.Attributes.Add("class", "active");
                //Tab1.Attributes.Add("class", "");
                //Tab2.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("RiskControlMatrixUpdate");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdFilterRCMData"]);
                   
                    ExcelData = view.ToTable("Selected", false, "Branch", "VerticalName", "ProcessName", "SubProcessName", "ActivityName", "ControlNo", "ActivityDescription", "ControlObjective", "ControlDescription", "MControlDescription", "PersonResponsible", "Email", "ControlOwnerName", "ControlOwnerEmail", "KC1Name", "KC2Name", "TestStrategy", "RiskRating", "ControlRating", "ProcessScore", "RiskID", "RiskActivityID", "CustomerBranchId", "VerticalsId", "ProcessId", "SubProcessId", "ControlOwner", "Key_Value", "KC2"); //"Branch", "VerticalName", "ProcessName", "SubProcessName"); // //"Frequency",
                                        
                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["ControlOwner"].ToString() != null)
                        {
                            var userdetails = UserManagementRisk.GetByID(Convert.ToInt32(item["ControlOwner"].ToString()));
                            if (userdetails != null)
                            {
                                item["ControlOwnerName"] = userdetails.FirstName + " " + userdetails.LastName;
                                item["ControlOwnerEmail"] = userdetails.Email;
                            }
                        }

                        if (item["KC2"].ToString() != null)
                        {
                            if (item["KC2"].ToString() == "1")
                            {
                                item["KC1Name"] = "YES";
                            }
                            else if (item["KC2"].ToString() == "2")
                            {
                                item["KC1Name"] = "NO";
                            }
                        }
                        if (item["Key_Value"].ToString() != null)
                        {
                            if (item["Key_Value"].ToString() == "1")
                            {
                                item["KC2Name"] = "YES";
                            }
                            else if (item["Key_Value"].ToString() == "2")
                            {
                                item["KC2Name"] = "NO";
                            }
                        }
                    }
                    ExcelData.Columns.Remove("ControlOwner");
                    ExcelData.Columns.Remove("Key_Value");
                    ExcelData.Columns.Remove("KC2");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Branch";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(30);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Activity";
                    exWorkSheet.Cells["E1"].AutoFitColumns(30);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "ControlNo";
                    exWorkSheet.Cells["F1"].AutoFitColumns(15);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Risk Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(30);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Control Objective";
                    exWorkSheet.Cells["H1"].AutoFitColumns(30);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Control Description";
                    exWorkSheet.Cells["I1"].AutoFitColumns(30);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Mitigating Control Description";
                    exWorkSheet.Cells["J1"].AutoFitColumns(15);

                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "Person Responsible";
                    exWorkSheet.Cells["K1"].AutoFitColumns(20);

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "Email";
                    exWorkSheet.Cells["L1"].AutoFitColumns(30);

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "Control Owner";
                    exWorkSheet.Cells["M1"].AutoFitColumns(20);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "Email";
                    exWorkSheet.Cells["N1"].AutoFitColumns(30);

                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].Value = "KC -1";
                    exWorkSheet.Cells["O1"].AutoFitColumns(10);

                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].Value = "KC -2";
                    exWorkSheet.Cells["P1"].AutoFitColumns(10);

                    exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Q1"].Value = "Test Strategy";
                    exWorkSheet.Cells["Q1"].AutoFitColumns(20);

                    exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["R1"].Value = "Risk Rating";
                    exWorkSheet.Cells["R1"].AutoFitColumns(10);

                    exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["S1"].Value = "Control Rating";
                    exWorkSheet.Cells["S1"].AutoFitColumns(10);

                    exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["T1"].Value = "Process Score";
                    exWorkSheet.Cells["T1"].AutoFitColumns(5);

                    exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["U1"].Value = "RiskID";
                    exWorkSheet.Cells["U1"].AutoFitColumns(5);

                    exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["V1"].Value = "RATID";
                    exWorkSheet.Cells["V1"].AutoFitColumns(5);


                    exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["W1"].Value = "BID";
                    exWorkSheet.Cells["W1"].AutoFitColumns(5);

                    exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["X1"].Value = "VID";
                    exWorkSheet.Cells["X1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Y1"].Value = "PID";
                    exWorkSheet.Cells["Y1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Z1"].Value = "SPID";
                    exWorkSheet.Cells["Z1"].AutoFitColumns(5);


                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 26])
                    {
                        col.Style.WrapText = true;                        
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        //col.AutoFitColumns();

                        //Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    //exWorkSheet.Protection.IsProtected = true;
                    //exWorkSheet.Protection.AllowSort= true;
                    //exWorkSheet.Protection.AllowAutoFilter = true;
                    //exWorkSheet.Protection.AllowDeleteRows = true;

                    //exWorkSheet.Cells["A:L"].Style.Locked = false;
                    //exWorkSheet.Cells["A:L"].AutoFilter = true;

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=RiskControlMatrixExportForUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                
                BindData("P");
                bindPageNumber();
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
               
                BindData("N");
                bindPageNumber();
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                BindGridData();
               // bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

    
       
    }
}