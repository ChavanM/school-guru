﻿
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class InternalComplianceAssignmentUpload : System.Web.UI.Page
    {
        bool suucess = false;
        //static int customerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
              
                if (AuthenticationHelper.Role == "IMPT")
                {
                    divcustomer.Visible = true;
                    BindCustomers();
                }
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                int userid = -1;
                userid = AuthenticationHelper.UserID;
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                ddlCustomerList.DataSource = Assigncustomer.GetAllCustomer(userid); 
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region Add Process     

        public static bool InternalComplianceTempAssignmentExists(int ComplianceID, int Customerbranchid,string SequenceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTableInternals
                             where row.IsActive == true
                                  && row.ComplianceId == ComplianceID
                                  && row.CustomerBranchID == Customerbranchid
                                   && row.SequenceID == SequenceID
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }
        public static void GetDepartment(int customerID)
        {
            List<ComplianceManagement.Business.Data.Department> Records = new List<ComplianceManagement.Business.Data.Department>();

            var DepartmentList = HttpContext.Current.Cache["InternalDepartmentListData"];

            if (DepartmentList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Departments
                               where row.IsDeleted == false
                               && row.CustomerID == customerID
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("InternalDepartmentListData", Records); // add it to cache
                }
            }
        }
        public bool DepartmentExists(int deptid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Department>)Cache["InternalDepartmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == deptid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    int customerID = -1;

                    if (AuthenticationHelper.Role=="IMPT")
                    {
                        customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                    else
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    // int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    //{
                    //    if (ddlCustomerList.SelectedValue != "-1")
                    //    {
                            string filename = Path.GetFileName(MasterFileUpload.FileName);
                            MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentSheetsExitsts(xlWorkbook, "UploadComplianceAssignment");
                                        if (flag == true)
                                        {
                                           
                                            if (HttpContext.Current.Cache.Get("InternalComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("InternalComplianceListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("InternalUserListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("InternalUserListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("InternalCustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("InternalCustomerBranchListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("InternalDepartmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("InternalDepartmentListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("InternalComplianceInstanceAssignmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("InternalComplianceInstanceAssignmentListData");
                                            }
                                            GetCompliance(customerID);
                                            GetUser(customerID);
                                            GetCustomerBranch(customerID);
                                            GetDepartment(customerID);
                                            GetComplianceInstanceAssignment(customerID);
                                            ComplianceAssignmentData(xlWorkbook);
                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("InternalComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("InternalComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("InternalUserListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("InternalUserListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("InternalCustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("InternalCustomerBranchListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("InternalDepartmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("InternalDepartmentListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("InternalComplianceInstanceAssignmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("InternalComplianceInstanceAssignmentListData");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please correct the sheet name.";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool ComplianceAssignmentSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignment"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private bool ComplianceAssignmentCheckListSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignmentCheckList"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public partial class InternalTempTable
        {
            public long ComplianceId { get; set; }
            public int CustomerBranchID { get; set; }
            public long Performerid { get; set; }
            public string SequenceID { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        public static Boolean GetCompliancePresent(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = true;
                var Frequency = (from row in entities.InternalCompliances
                                 where row.ID == ComplianceID
                                 select row.IFrequency).FirstOrDefault();

                var RecordCount = (from row in entities.InternalComplianceSchedules
                                   where row.IComplianceID == ComplianceID
                                   select row).ToList().Count();

                if (Frequency != null)
                {
                   
                    int frequencyId = Convert.ToInt32(Frequency);

                    if (frequencyId == 0)  //Monthly
                    {
                        if (RecordCount != 12)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 1)  //Quarterly
                    {
                        if (RecordCount != 4)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 2)  //HalfYearly
                    {
                        if (RecordCount != 2)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 3)  //Annual
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 4)  //FourMonthly
                    {
                        if (RecordCount != 3)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 5)  //TwoYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 6)  //SevenYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 7)  //Daily
                    {
                        flag = true;
                    }
                    if (frequencyId == 8)  //Weekly
                    {
                        flag = true;
                    }
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void GetComplianceInstanceAssignment(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.SP_CheckSequence_Result> Records = new List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>();

            var ComplianceList = HttpContext.Current.Cache["InternalComplianceInstanceAssignmentListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.SP_CheckSequence(CustomerID, "I")
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("InternalComplianceInstanceAssignmentListData", Records); // add it to cache
                }
            }
        }
        private void ComplianceAssignmentData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadComplianceAssignment"];
                if (xlWorksheet != null)
                {
                    int count = 1;                   
                    int xlrow2 = xlWorksheet.Dimension.End.Row;                  
                    List<TempAssignmentTableInternal> TempassignmentTableList = new List<TempAssignmentTableInternal>();
                    List<string> errorMessage = new List<string>();
                    List<InternalTempTable> lstTemptable = new List<InternalTempTable>();

                    #region Validations
                    int valComplianceID = -1;
                    int valPerformerID = -1;
                    int valReviewerID = -1;
                    int valApproverID = -1;
                    int valCustomerBranchID = -1;
                    string valIsCheckList = string.Empty;
                    int valDepartmentID = -1;
                    string valSequence = string.Empty;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valComplianceID = -1;
                        valPerformerID = -1;
                        valReviewerID = -1;
                        valApproverID = -1;
                        valCustomerBranchID = -1;
                        valIsCheckList = string.Empty;
                        valDepartmentID = -1;
                        valSequence = string.Empty;
                        #region 1 ComplianceID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valComplianceID == -1 || valComplianceID == 0)
                        {
                            errorMessage.Add("Required InternalComplianceID at row number-" + rowNum);
                        }
                        else
                        {
                            if (ComplainceExists(valComplianceID) == false)
                            {
                                errorMessage.Add("InternalComplianceID not Defined in the System  at row number-" + rowNum);
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                                {
                                    valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                                    if (!BranchIDExists(valCustomerBranchID) == false)
                                    {

                                        //Added by rahul on 10 FEB 2020 to add multipal complianceid at same location
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                                        {
                                            valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                                        }
                                        if (valPerformerID != -1 || valPerformerID != 0)
                                        {
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString()))
                                            {
                                                valSequence = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text);
                                            }
                                            if (!lstTemptable.Any(x => x.ComplianceId == valComplianceID
                                            && x.CustomerBranchID == valCustomerBranchID && x.SequenceID == valSequence))
                                            {
                                                InternalTempTable tt = new InternalTempTable();
                                                tt.ComplianceId = valComplianceID;
                                                tt.CustomerBranchID = valCustomerBranchID;
                                                tt.Performerid = valPerformerID;
                                                tt.SequenceID = valSequence;
                                                lstTemptable.Add(tt);
                                            }
                                            else
                                            {
                                                errorMessage.Add("InternalCompliance with this InternalComplianceID (" + valComplianceID + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                            }
                                        }
                                    }
                                }

                                valIsCheckList = CheckISCheckListOrComplianceExists(valComplianceID);
                                if (string.IsNullOrEmpty(valIsCheckList))
                                {
                                    errorMessage.Add("Please check the InternalCompliance type in master at row number -" + rowNum);
                                }
                            }
                        }
                        #endregion                        

                        #region Check Compliance Schedule               
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString()))
                        {
                            if (valComplianceID != 0 || valComplianceID != -1)
                            {
                                valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                                Boolean checkflag = GetCompliancePresent(valComplianceID);
                                if (checkflag == false)
                                {
                                    errorMessage.Add("Please Correct the Display Schedule InternalComplianceID -" + valComplianceID + " at row number-" + rowNum);
                                }
                            }
                        }
                        #endregion

                        #region 2 PerformerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                        }
                        if (valPerformerID == -1 || valPerformerID == 0)
                        {
                            errorMessage.Add("Required PerformerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valPerformerID) == false)
                            {
                                errorMessage.Add("PerformerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 3 ReviewerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                        {
                            valReviewerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                        }
                        if (valReviewerID == -1 || valReviewerID == 0)
                        {
                            errorMessage.Add("Required ReviewerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valReviewerID) == false)
                            {
                                errorMessage.Add("ReviewerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 4 ApproverID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString()))
                        {
                            //valApproverID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 4].Text);
                            var listcommApproverID = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text);
                       
                           if (listcommApproverID != "")
                           {

                            List<string> abclist = listcommApproverID.Split(',').ToList();
                            foreach (string s in abclist)
                            {
                                if (UserExists(Convert.ToInt32(s)) == false)
                                {
                                    errorMessage.Add("ApproverID not Defined in the System at row number-" + rowNum);
                                }
                            }
                          }
                        }
                        #endregion

                        #region 5 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion
                   
                        #region 6 Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                        {
                            if (CheckInt(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                            {
                                valDepartmentID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text);
                            }
                            
                        }
                        if (valDepartmentID != -1 && valDepartmentID != 0)
                        {
                            if (DepartmentExists(valDepartmentID) == false)
                            {
                                errorMessage.Add("DepartmentID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 7 Label
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString()))
                        {
                            valSequence = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text);
                        }
                        if (!string.IsNullOrEmpty(valSequence))
                        {
                            if (valSequence.Length > 24)
                            {
                                //errorMessage.Add("Label Length ('" + valSequence + "') exceed  at row number-" + rowNum);
                                errorMessage.Add("Label Length ('" + valSequence + "') exceed, it should be less than 25 characters  at row number-" + rowNum);
                            }
                            if (AssignmentExists(valCustomerBranchID, valComplianceID, valSequence))
                            {
                                errorMessage.Add("Label Assignment ('" + valSequence + "') already in the System at row number-" + rowNum);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int ComplianceID = -1;
                            int PerformerID = -1;
                            int ReviewerID = -1;
                            int ApproverID = -1;
                            int CustomerBranchID = -1;
                            string IsCheckList = string.Empty;
                            int DepartmentID = -1;
                            string SequenceID = string.Empty;
                            List<string> abclist = new List<string>();
                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());                               
                            }                            
                            #endregion

                            #region PerformerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                PerformerID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);                               
                            }                            
                            #endregion

                            #region ReviewerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                            {
                                ReviewerID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);                                
                            }                            
                            #endregion

                            #region ApproverID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                            {
                                //ApproverID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text); 
                                var listcommApproverID = Convert.ToString(xlWorksheet.Cells[i, 4].Text);
                                abclist = listcommApproverID.Split(',').ToList();
                            }                           
                            #endregion

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 5].Text);                               
                            }
                            #endregion

                            #region IsCheckList
                            IsCheckList = CheckISCheckListOrComplianceExists(ComplianceID);
                                                    
                            #endregion

                            #region Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString()))
                            {
                                DepartmentID = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text);                               
                            }
                            #endregion

                            #region Sequence
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString()))
                            {
                                SequenceID = Convert.ToString(xlWorksheet.Cells[i, 7].Text);
                            }
                            #endregion
                            if (IsCheckList == "Y")
                            {
                                if ((InternalComplianceTempAssignmentExists(ComplianceID, CustomerBranchID, SequenceID)))
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        
                                        //var cunt = (from row in entities.TempAssignmentTableInternals
                                        //            where row.ComplianceId == ComplianceID
                                        //            && row.CustomerBranchID == CustomerBranchID
                                        //            && row.ComplianceFlag=="C"
                                        //             && row.RoleID == 3
                                        //            select row).OrderByDescending(x => x.SequenceID).Take(1).Select(A => A.SequenceID).FirstOrDefault();
                                        //if (cunt == 0 || cunt == null)
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //    && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();
                                        //    if (tempcnt == 0)
                                        //    {
                                        //        cunt = 1;
                                        //    }
                                        //    else
                                        //    {
                                        //        cunt = tempcnt + 1;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //                 && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();

                                        //    cunt += tempcnt + 1;

                                        //}

                                        TempAssignmentTableInternal TempAssP = new TempAssignmentTableInternal();
                                        TempAssP.ComplianceId = ComplianceID;
                                        TempAssP.CustomerBranchID = CustomerBranchID;
                                        TempAssP.RoleID = 3;
                                        TempAssP.UserID = PerformerID;
                                        TempAssP.IsActive = true;
                                        TempAssP.ComplianceFlag = "C";
                                        TempAssP.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempAssP.SequenceID = SequenceID;
                                        TempassignmentTableList.Add(TempAssP);

                                        TempAssignmentTableInternal TempAssR = new TempAssignmentTableInternal();
                                        TempAssR.ComplianceId = ComplianceID;
                                        TempAssR.CustomerBranchID = CustomerBranchID;
                                        TempAssR.RoleID = 4;
                                        TempAssR.UserID = ReviewerID;
                                        TempAssR.IsActive = true;
                                        TempAssR.ComplianceFlag = "C";
                                        TempAssR.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssR.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssR.DepartmentID = DepartmentID;
                                        }
                                        TempAssR.SequenceID = SequenceID;
                                        TempassignmentTableList.Add(TempAssR);

                                        //if (ApproverID != -1)
                                        //{
                                        foreach (var item in abclist)
                                        {
                                            TempAssignmentTableInternal TempAssA = new TempAssignmentTableInternal();
                                            TempAssA.ComplianceId = ComplianceID;
                                            TempAssA.CustomerBranchID = CustomerBranchID;
                                            TempAssA.RoleID = 6;
                                            TempAssA.UserID = Convert.ToInt32(item); 
                                            TempAssA.IsActive = true;
                                            TempAssA.ComplianceFlag = "C";
                                            TempAssA.CreatedOn = DateTime.Now;
                                            if (DepartmentID == -1 || DepartmentID == 0)
                                            {
                                                TempAssA.DepartmentID = null;
                                            }
                                            else
                                            {
                                                TempAssA.DepartmentID = DepartmentID;
                                            }
                                            TempAssA.SequenceID = SequenceID;
                                            TempassignmentTableList.Add(TempAssA);
                                        }
                                            
                                        //}
                                    }                      
                                }//exists end
                            }
                            else
                            {
                                if ((InternalComplianceTempAssignmentExists(ComplianceID, CustomerBranchID, SequenceID)))
                                {
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        //var cunt = (from row in entities.TempAssignmentTableInternals
                                        //            where row.ComplianceId == ComplianceID
                                        //            && row.CustomerBranchID == CustomerBranchID
                                        //            && row.ComplianceFlag == "S"
                                        //             && row.RoleID == 3
                                        //            select row).OrderByDescending(x => x.SequenceID).Take(1).Select(A => A.SequenceID).FirstOrDefault();
                                        //if (cunt == 0 || cunt == null)
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //    && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();
                                        //    if (tempcnt == 0)
                                        //    {
                                        //        cunt = 1;
                                        //    }
                                        //    else
                                        //    {
                                        //        cunt = tempcnt + 1;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    var tempcnt = TempassignmentTableList.Where(x => x.ComplianceId == ComplianceID
                                        //                 && x.CustomerBranchID == CustomerBranchID && x.RoleID == 3).ToList().GroupBy(a => a.RoleID).Select(b => b.FirstOrDefault()).Count();

                                        //    cunt += tempcnt + 1;

                                        //}
                                        //var cunt = string.Empty;
                                        TempAssignmentTableInternal TempAssP = new TempAssignmentTableInternal();
                                        TempAssP.ComplianceId = ComplianceID;
                                        TempAssP.CustomerBranchID = CustomerBranchID;
                                        TempAssP.RoleID = 3;
                                        TempAssP.UserID = PerformerID;
                                        TempAssP.IsActive = true;
                                        TempAssP.ComplianceFlag = "S";
                                        TempAssP.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempAssP.SequenceID = SequenceID;
                                        TempassignmentTableList.Add(TempAssP);

                                        TempAssignmentTableInternal TempAssR = new TempAssignmentTableInternal();
                                        TempAssR.ComplianceId = ComplianceID;
                                        TempAssR.CustomerBranchID = CustomerBranchID;
                                        TempAssR.RoleID = 4;
                                        TempAssR.UserID = ReviewerID;
                                        TempAssR.IsActive = true;
                                        TempAssR.ComplianceFlag = "S";
                                        TempAssR.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssR.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssR.DepartmentID = DepartmentID;
                                        }
                                        TempAssR.SequenceID = SequenceID;
                                        TempassignmentTableList.Add(TempAssR);

                                        //if (ApproverID != -1)
                                        //{
                                        foreach (var item in abclist)
                                        {
                                            TempAssignmentTableInternal TempAssA = new TempAssignmentTableInternal();
                                            TempAssA.ComplianceId = ComplianceID;
                                            TempAssA.CustomerBranchID = CustomerBranchID;
                                            TempAssA.RoleID = 6;
                                            TempAssA.UserID = Convert.ToInt32(item); ;
                                            TempAssA.IsActive = true;
                                            TempAssA.ComplianceFlag = "S";
                                            TempAssA.CreatedOn = DateTime.Now;
                                            if (DepartmentID == -1 || DepartmentID == 0)
                                            {
                                                TempAssA.DepartmentID = null;
                                            }
                                            else
                                            {
                                                TempAssA.DepartmentID = DepartmentID;
                                            }
                                            TempAssA.SequenceID = SequenceID;
                                            TempassignmentTableList.Add(TempAssA);
                                        }
                                           
                                        //}
                                    }
                                }//exists end
                            }
                        }
                        #endregion

                        if (TempassignmentTableList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTable(TempassignmentTableList);
                        }
                        else
                        {
                            suucess = false;
                        }
                    }             
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void GetCompliance(int customerid)
        {
            List<ComplianceManagement.Business.Data.InternalCompliance> Records = new List<ComplianceManagement.Business.Data.InternalCompliance>();

            var ComplianceList = HttpContext.Current.Cache["InternalComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   Records = (from row in entities.InternalCompliances
                              where row.CustomerID == customerid
                              && row.IsDeleted==false
                              select row).ToList();
                    
                    HttpContext.Current.Cache.Insert("InternalComplianceListData", Records); // add it to cache
                }               
            }
        }

        public void GetUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var ComplianceList = HttpContext.Current.Cache["InternalUserListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID==CustomerID && row.RoleID != 19
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("InternalUserListData", Records); // add it to cache
                }
            }
        }
        public void GetCustomerBranch(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.CustomerBranch> Records = new List<ComplianceManagement.Business.Data.CustomerBranch>();

            var ComplianceList = HttpContext.Current.Cache["InternalCustomerBranchListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.CustomerBranches
                               where row.CustomerID == CustomerID
                                && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("InternalCustomerBranchListData", Records); // add it to cache
                }
            }
        }
     
       
        public  bool CreateExcelTempAssignmentTable(List<TempAssignmentTableInternal> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        entities.TempAssignmentTableInternals.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ComplainceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.InternalCompliance>) Cache["InternalComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UserExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>) Cache["InternalUserListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool AssignmentExists(long Branchid, long ComplianceiD, string Sequence)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>)Cache["InternalComplianceInstanceAssignmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.CustomerBranchID == Branchid
                                 && row.ComplianceId == ComplianceiD
                                 && row.SequenceID == Sequence
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool BranchIDExists(int Branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.CustomerBranch>) Cache["InternalCustomerBranchListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Branchid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public  string  CheckISCheckListOrComplianceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.InternalCompliance>)Cache["InternalComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.IComplianceType).FirstOrDefault();
                    if (query == 1)
                    {
                        return "Y";
                    }
                    else
                    {
                        return "N";
                    }
                }
                else
                {
                    return "";
                }                             
            }
        }
        #endregion       
    }
}