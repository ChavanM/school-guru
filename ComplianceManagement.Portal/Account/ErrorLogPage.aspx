﻿<%@ Page Title="Log Report" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ErrorLogPage.aspx.cs" 
Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ErrorLogPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">

function initializeDatePicker(date) {

         var startDate = new Date();
         $("#<%= txtStartDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             numberOfMonths: 1,
             
         });

         $("#<%= txtEndDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             defaultDate: startDate,
             numberOfMonths: 1,
         });


         if (date != null) {
             $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
             $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
         }
     }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     
<div style="float:right;margin-top: 10px;margin-right:7px;margin-top: 5px;">   
        <asp:LinkButton runat="server"  ID="lbtnExportExcel" style="margin-top:15px;margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
     <div style="margin-top: 7px;">
            <label style="width: 100px;margin-left: 50px; display: block; float: left; font-size: 13px; color: #333;">
                Start Date</label>
            <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; float:left; width: 200px;" ReadOnly="true"
            MaxLength="200" />
            <label style="width: 100px; padding-left: 30px; display: block; float:left; font-size: 13px; color: #333;">
                End Date</label>
            <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; float:left; width: 200px;" ReadOnly="true"
            MaxLength="200" />
       </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
           <div>
          <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="button" Text="Search" Height="25px" style="margin-left:10px;"/>
          </div>
    </ContentTemplate>
    </asp:UpdatePanel>
 
 <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
         

            <div style="width:100%">
                <div id="divLogEntryMessage" style="margin:10px">
                 <asp:GridView runat="server" ID="grdLogEntry" AutoGenerateColumns="false" AllowSorting="true"
                        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdLogEntry_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="15" OnSorting="grdLogEntry_Sorting"
                        Width="100%" Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdLogEntry_PageIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="Log Level" HeaderStyle-Width="10%" DataField="LogLevel" SortExpression="LogLevel"/>
                            <asp:BoundField HeaderText="Class Name" HeaderStyle-Width="10%"  DataField="ClassName" SortExpression="ClassName"/>
                            <asp:BoundField HeaderText="Function Name" HeaderStyle-Width="10%"  DataField="FunctionName" SortExpression="FunctionName"/>
                            <asp:TemplateField HeaderText="Message" ItemStyle-Width="20%" SortExpression="Message">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Message") %>' ToolTip='<%# Eval("Message") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="StackTrace" ItemStyle-Width="20%" SortExpression="StackTrace"  ItemStyle-Height="20px" HeaderStyle-Height="20px" >
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("StackTrace") %>' ToolTip='<%# Eval("StackTrace") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Created On" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="CreatedOn" >
                                <ItemTemplate>
                                    <%# Eval("CreatedOn") != null ? ((DateTime)Eval("CreatedOn")).ToString() : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <pagersettings position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
