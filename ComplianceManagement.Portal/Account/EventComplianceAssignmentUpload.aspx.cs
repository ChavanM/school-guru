﻿
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class EventComplianceAssignmentUpload : System.Web.UI.Page
    {
        bool suucess = false;
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                BindCustomers(userID);
                BindECustomers(userID);
                Tab1_Click(sender, e);
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
        }
        private void BindECustomers(int userID)
        {
            try
            {
                ddlECustomerList.DataTextField = "Name";
                ddlECustomerList.DataValueField = "ID";
                ddlECustomerList.DataSource = CustomerManagement.GetAllCustomer(userID, "");
                ddlECustomerList.DataBind();

                ddlECustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                //   ddlCustomerList.DataSource = CustomerManagement.GetAll("");
                ddlCustomerList.DataSource = CustomerManagement.GetAllCustomer(UserID,"");
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region Add Process      
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(MasterFileUpload.FileName);
                            MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentSheetsExitsts(xlWorkbook, "UploadEventComplianceAssignment");
                                        if (flag == true)
                                        {
                                            int customerID =Convert.ToInt32(ddlCustomerList.SelectedValue);
                                            if (HttpContext.Current.Cache.Get("EventComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventComplianceListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("EventUserListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventUserListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("EventCustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventCustomerBranchListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("EventDepartmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EventDepartmentListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OwnerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OfficerUserData");
                                            }
                                            GetCompliance();
                                            GetUser(customerID);
                                            GetCustomerBranch(customerID);
                                            GetDepartment(customerID);
                                            GetCertificateOfficerUser(customerID);
                                            GetCertificateOwnerUser(customerID);
                                            ComplianceAssignmentData(xlWorkbook);
                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("EventComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EventComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("EventUserListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EventUserListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("EventCustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EventCustomerBranchListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OwnerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OfficerUserData");
                                                }
                                            }
                                            //if (suucess == false)
                                            //{
                                            //    cvDuplicateEntry.IsValid = false;
                                            //    cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                                            //}
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            //cvDuplicateEntry.ErrorMessage = "No data found in file.";
                                            cvDuplicateEntry.ErrorMessage = "Please correct the sheet name.";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool ComplianceAssignmentSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadEventComplianceAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("UploadEventComplianceAssignment"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private bool ComplianceAssignmentCheckListSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignmentCheckList"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public partial class TempTable
        {
            public long ComplianceId { get; set; }
            public int CustomerBranchID { get; set; }
        }
        public bool DepartmentExists(int deptid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Department>)Cache["EventDepartmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == deptid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        private void ComplianceAssignmentData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadEventComplianceAssignment"];
                if (xlWorksheet != null)
                {
                    int count = 1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                    List<TempAssignmentTableCheckList> TempassignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<string> errorMessage = new List<string>();
                    List<TempTable> lstTemptable = new List<TempTable>();
                    #region Validations
                    int valComplianceID = -1;
                    int valPerformerID = -1;
                    int valReviewerID = -1;
                    int valApproverID = -1;
                    int valCar_OfficerID = -1;
                    int valCae_OwnerID = -1;
                    int valCustomerBranchID = -1;
                    string valIsCheckList = string.Empty;
                    int valDepartmentID = -1;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valComplianceID = -1;
                        valPerformerID = -1;
                        valReviewerID = -1;
                        valApproverID = -1;
                        valCar_OfficerID = -1;
                        valCae_OwnerID = -1;
                        valCustomerBranchID = -1;
                        valIsCheckList = string.Empty;
                        valDepartmentID = -1;

                        #region 1 ComplianceID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valComplianceID == -1 || valComplianceID == 0)
                        {
                            errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                        }
                        else
                        {
                            if (ComplainceExists(valComplianceID) == false)
                            {
                                errorMessage.Add("ComplianceID not Defined in the System or It is Event Based or Informative Compliance at row number-" + rowNum);
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                                {
                                    valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                                    if (!BranchIDExists(valCustomerBranchID) == false)
                                    {
                                        if (!lstTemptable.Any(x => x.ComplianceId == valComplianceID && x.CustomerBranchID == valCustomerBranchID))
                                        {
                                            TempTable tt = new TempTable();
                                            tt.ComplianceId = valComplianceID;
                                            tt.CustomerBranchID = valCustomerBranchID;
                                            lstTemptable.Add(tt);
                                        }
                                        else
                                        {
                                            //ComplianceID - 
                                            errorMessage.Add("Compliance with this ComplianceID (" + valComplianceID + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region 2 PerformerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                        }
                        if (valPerformerID == -1 || valPerformerID == 0)
                        {
                            errorMessage.Add("Required PerformerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valPerformerID) == false)
                            {
                                errorMessage.Add("PerformerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 3 ReviewerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                        {
                            valReviewerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                        }
                        if (valReviewerID == -1 || valReviewerID == 0)
                        {
                            errorMessage.Add("Required ReviewerID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (UserExists(valReviewerID) == false)
                            {
                                errorMessage.Add("ReviewerID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 4 ApproverID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString()))
                        {
                            valApproverID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 4].Text);
                        }
                        if (valApproverID != -1 && valApproverID != 0)
                        {
                            if (UserExists(valApproverID) == false)
                            {
                                errorMessage.Add("ApproverID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 5 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID  at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 6 Department
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                        {
                            valDepartmentID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text);
                        }
                        if (valDepartmentID != -1 && valDepartmentID != 0)
                        {
                            if (DepartmentExists(valDepartmentID) == false)
                            {
                                errorMessage.Add("DepartmentID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 7 OwnerID OfficerID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString()))
                        {
                            valCae_OwnerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 7].Text);
                            if (valCae_OwnerID != -1 && valCae_OwnerID != 0)
                            {
                                if (UserOwnerExists(valCae_OwnerID) == false)
                                {
                                    errorMessage.Add("OwnerID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString()))
                        {
                            valCar_OfficerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 8].Text);
                            if (valCar_OfficerID != -1 && valCar_OfficerID != 0)
                            {
                                if (UserOfficerExists(valCar_OfficerID) == false)
                                {
                                    errorMessage.Add("OfficerID not Defined in the System at row number-" + rowNum);
                                }
                            }
                        }
                        #endregion


                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        #region 
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int ComplianceID = -1;
                            int PerformerID = -1;
                            int ReviewerID = -1;
                            int ApproverID = -1;
                            int OfficerID = -1;
                            int OwnerID = -1;
                            int CustomerBranchID = -1;
                            string IsCheckList = string.Empty;
                            int DepartmentID = -1;

                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region PerformerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                PerformerID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region ReviewerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                            {
                                ReviewerID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                            }
                            #endregion

                            #region ApproverID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                            {
                                ApproverID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text);
                            }
                            #endregion

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 5].Text);
                            }
                            #endregion

                            #region IsCheckList
                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString()))
                            //{
                            //    IsCheckList = Convert.ToString(xlWorksheet.Cells[i, 6].Text);
                            //}

                            IsCheckList = CheckISCheckListOrComplianceExists(ComplianceID);
                            #endregion

                            #region Department
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString()))
                            {
                                DepartmentID = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text);
                            }
                            #endregion

                            #region OwnerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString()))
                            {
                                OwnerID = Convert.ToInt32(xlWorksheet.Cells[i, 7].Text);
                            }
                            #endregion

                            #region OfficerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString()))
                            {
                                OfficerID = Convert.ToInt32(xlWorksheet.Cells[i, 8].Text);
                            }
                            #endregion

                            if (IsCheckList == "Y")
                            {
                                if ((ComplianceManagement.Business.ComplianceManagement.ExistsCheckList(ComplianceID, CustomerBranchID,null)))
                                {
                                    TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                    TempAssP.ComplianceId = ComplianceID;
                                    TempAssP.CustomerBranchID = CustomerBranchID;
                                    TempAssP.RoleID = 3;
                                    TempAssP.UserID = PerformerID;
                                    TempAssP.IsActive = true;
                                    TempAssP.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssP.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssP.DepartmentID = DepartmentID;
                                    }
                                    TempassignmentTableCheckList.Add(TempAssP);

                                    TempAssignmentTableCheckList TempAssR = new TempAssignmentTableCheckList();
                                    TempAssR.ComplianceId = ComplianceID;
                                    TempAssR.CustomerBranchID = CustomerBranchID;
                                    TempAssR.RoleID = 4;
                                    TempAssR.UserID = ReviewerID;
                                    TempAssR.IsActive = true;
                                    TempAssR.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssP.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssP.DepartmentID = DepartmentID;
                                    }
                                    TempassignmentTableCheckList.Add(TempAssR);

                                    if (ApproverID != -1)
                                    {
                                        TempAssignmentTableCheckList TempAssA = new TempAssignmentTableCheckList();
                                        TempAssA.ComplianceId = ComplianceID;
                                        TempAssA.CustomerBranchID = CustomerBranchID;
                                        TempAssA.RoleID = 6;
                                        TempAssA.UserID = ApproverID;
                                        TempAssA.IsActive = true;
                                        TempAssA.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempassignmentTableCheckList.Add(TempAssA);
                                    }
                                }//exists end
                            }
                            else
                            {
                                if ((ComplianceManagement.Business.ComplianceManagement.Exists(ComplianceID, CustomerBranchID,null)))
                                {

                                    TempAssignmentTable TempAssP = new TempAssignmentTable();
                                    TempAssP.ComplianceId = ComplianceID;
                                    TempAssP.CustomerBranchID = CustomerBranchID;
                                    TempAssP.RoleID = 3;
                                    TempAssP.UserID = PerformerID;
                                    TempAssP.Cer_OwnerUserID = OwnerID;
                                    TempAssP.Cer_OfficerUserID = OfficerID;
                                    TempAssP.IsActive = true;
                                    TempAssP.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssP.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssP.DepartmentID = DepartmentID;
                                    }
                                    TempassignmentTableList.Add(TempAssP);

                                    TempAssignmentTable TempAssR = new TempAssignmentTable();
                                    TempAssR.ComplianceId = ComplianceID;
                                    TempAssR.CustomerBranchID = CustomerBranchID;
                                    TempAssR.RoleID = 4;
                                    TempAssR.UserID = ReviewerID;
                                    TempAssR.IsActive = true;
                                    TempAssR.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssP.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssP.DepartmentID = DepartmentID;
                                    }
                                    TempassignmentTableList.Add(TempAssR);
                                    if (ApproverID != -1)
                                    {
                                        TempAssignmentTable TempAssA = new TempAssignmentTable();
                                        TempAssA.ComplianceId = ComplianceID;
                                        TempAssA.CustomerBranchID = CustomerBranchID;
                                        TempAssA.RoleID = 6;
                                        TempAssA.UserID = ApproverID;
                                        TempAssR.Cer_OwnerUserID = OwnerID;
                                        TempAssR.Cer_OfficerUserID = OfficerID;
                                        TempAssA.IsActive = true;
                                        TempAssA.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        TempassignmentTableList.Add(TempAssA);
                                    }
                                }//exists end
                            }
                        }
                        #endregion

                        if (TempassignmentTableList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTable(TempassignmentTableList);
                        }
                        if (suucess)
                        {
                            if (TempassignmentTableCheckList.Count > 0)
                            {
                                suucess = CreateExcelTempAssignmentTableCheckList(TempassignmentTableCheckList);
                            }
                        }
                        else
                        {
                            suucess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void GetDepartment(int customerID)
        {
            List<ComplianceManagement.Business.Data.Department> Records = new List<ComplianceManagement.Business.Data.Department>();

            var DepartmentList = HttpContext.Current.Cache["EventDepartmentListData"];

            if (DepartmentList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Departments
                               where row.IsDeleted == false
                               && row.CustomerID == customerID
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EventDepartmentListData", Records); // add it to cache
                }
            }
        }
        public static void GetCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();

            var ComplianceList = HttpContext.Current.Cache["EventComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                   Records = (from row in entities.Compliances
                              join row1 in entities.EventMappings
                              on row.ID equals row1.ComplianceID
                              join row2 in entities.EventCompAssignDays
                              on row.ID equals row2.ComplianceID                           
                              where row.IsDeleted==false && row.EventFlag==true 
                              && row.Status ==null 
                              && row.ComplinceVisible !=false
                              && row.IsMapped != false  //Added condition 19 NOV 2020                               
                              select row).ToList();
                    
                    HttpContext.Current.Cache.Insert("EventComplianceListData", Records); // add it to cache
                }               
            }
        }

        public static Boolean GetCompliancePresent(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = true;
                var Frequency = (from row in entities.Compliances
                                   where row.ID == ComplianceID
                                   select row.Frequency).FirstOrDefault();

                var RecordCount = (from row in entities.ComplianceSchedules
                           where row.ComplianceID == ComplianceID 
                           select row).ToList().Count();

                if (Frequency != null)
                {
                    //int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(Frequency));

                    int frequencyId =Convert.ToInt32(Frequency);

                    if (frequencyId == 0)  //Monthly
                    {
                        if (RecordCount != 12)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 1)  //Quarterly
                    {
                        if (RecordCount != 4)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 2)  //HalfYearly
                    {
                        if (RecordCount != 2)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 3)  //Annual
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 4)  //FourMonthly
                    {
                        if (RecordCount != 3)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 5)  //TwoYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 6)  //SevenYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 7)  //Daily
                    {
                        flag = true;
                    }
                    if (frequencyId == 8)  //Weekly
                    {
                        flag = true;
                    }
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
        }

        public void GetUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var ComplianceList = HttpContext.Current.Cache["EventUserListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID==CustomerID && row.RoleID !=19
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EventUserListData", Records); // add it to cache
                }
            }
        }
        public void GetCustomerBranch(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.CustomerBranch> Records = new List<ComplianceManagement.Business.Data.CustomerBranch>();

            var ComplianceList = HttpContext.Current.Cache["EventCustomerBranchListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.CustomerBranches
                               where row.CustomerID == CustomerID
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EventCustomerBranchListData", Records); // add it to cache
                }
            }
        }
        private void ComplianceAssignmentCheckListData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadComplianceAssignmentCheckList"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int ComplianceID = -1;
                    int UserID = -1;
                    int RoleID = -1;
                    int CustomerBranchID = -1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTableCheckList> TempAssignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<TempAssignmentTableCheckList> TempAssignmentTableCheckList1 = new List<TempAssignmentTableCheckList>();
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        #region Compliance ID                     
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                        }
                        if (ComplianceID == 0 || ComplianceID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Task ID at row number - " + count + " or Task ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region UserID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                        {
                            UserID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                        }
                        if (UserID == 0 || UserID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the User ID at row number - " + count + " or User ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region Role
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                        {
                            RoleID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                        }
                        if (RoleID == 0 || RoleID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Role ID at row number - " + count + " or Role ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion

                        #region CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                        {
                            CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text);
                        }
                        if (CustomerBranchID == 0 || CustomerBranchID == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the CustomerBranch ID at row number - " + count + " or CustomerBranch ID not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        #endregion


                        if (!(ComplianceManagement.Business.ComplianceManagement.ExistsCheckList(ComplianceID, CustomerBranchID,null)))
                        {
                            if (RoleID == 3)
                            {
                                TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                TempAssP.ComplianceId = ComplianceID;
                                TempAssP.CustomerBranchID = CustomerBranchID;
                                TempAssP.RoleID = 3;
                                TempAssP.UserID = UserID;
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssignmentTableCheckList.Add(TempAssP);
                            }
                            if (RoleID == 4)
                            {
                                TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                TempAssP.ComplianceId = ComplianceID;
                                TempAssP.CustomerBranchID = CustomerBranchID;
                                TempAssP.RoleID = 4;
                                TempAssP.UserID = UserID;
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssignmentTableCheckList.Add(TempAssP);
                            }
                        }//exists end
                    }
                    TempAssignmentTableCheckList1 = TempAssignmentTableCheckList.Where(entry => entry.RoleID == 0 || entry.UserID == 0 || entry.ComplianceId == 0 || entry.CustomerBranchID == 0).ToList();
                    if (TempAssignmentTableCheckList1.Count == 0)
                    {
                        suucess = CreateExcelTempAssignmentTableCheckList(TempAssignmentTableCheckList);
                        suucess = true;
                    }
                    else
                    {
                        suucess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public  bool CreateExcelTempAssignmentTableCheckList(List<TempAssignmentTableCheckList> TempsssignmentTableCheckList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempsssignmentTableCheckList.ForEach(entry =>
                    {
                        entities.TempAssignmentTableCheckLists.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public  bool CreateExcelTempAssignmentTable(List<TempAssignmentTable> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        entities.TempAssignmentTables.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ComplainceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>) Cache["EventComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool UserExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>) Cache["EventUserListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool BranchIDExists(int Branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.CustomerBranch>) Cache["EventCustomerBranchListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Branchid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public  string  CheckISCheckListOrComplianceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["EventComplianceListData"];                
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ComplianceType).FirstOrDefault();
                    if (query == 1)
                    {
                        return "Y";
                    }
                    else
                    {
                        return "N";
                    }
                }
                else
                {
                    return "";
                }                             
            }
        }
        #endregion

        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            string FileName = "ComplianceAssignmentFormat.xlsx";

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "image/jpeg";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(Server.MapPath("~/ExcelFormat/ComplianceAssignmentFormat.xlsx"));
            response.Flush();
            response.End();
        }

        protected void btnEUploadFile_Click(object sender, EventArgs e)
        {
            if (EMasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlECustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(EMasterFileUpload.FileName);
                            EMasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentUploadSheetsExitsts(xlWorkbook, "AssignmentReportStatutory");
                                        if (flag == true)
                                        {
                                            int customerID = Convert.ToInt32(ddlECustomerList.SelectedValue);
                                            //int complianceProductType = 0;
                                            //complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);

                                            if (HttpContext.Current.Cache.Get("EComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("EComplianceListData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OwnerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("OfficerUserData");
                                            }
                                            if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                            }

                                            GetStatutoryCompliance();
                                            GetCertificateOfficerUser(customerID);
                                            GetCertificateOwnerUser(customerID);
                                            GetCustomerBranch(customerID);

                                            ComplianceOfficerOwnerAssignmentData(xlWorkbook);

                                            if (suucess == true)
                                            {
                                                cvDuplicateEntry2.IsValid = false;
                                                cvDuplicateEntry2.ErrorMessage = "Data uploaded successfully.";

                                                if (HttpContext.Current.Cache.Get("EComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("EComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OwnerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OwnerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("OfficerUserData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("OfficerUserData");
                                                }
                                                if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please check the sheet name, Sheet name must be 'UploadComplianceAssignment'";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool ComplianceAssignmentUploadSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AssignmentReportStatutory"))
                    {
                        if (sheet.Name.Trim().Equals("AssignmentReportStatutory"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private void ComplianceOfficerOwnerAssignmentData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["AssignmentReportStatutory"];
                if (xlWorksheet != null)
                {
                    int count = 1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<ComplianceInstance> ComplianceInstanceList = new List<ComplianceInstance>();
                    List<string> errorMessage = new List<string>();


                    #region Validations
                    int valComplianceID = -1;
                    int valOfficerID = -1;
                    int valOwnerID = -1;
                    int valCustomerBranchID = -1;
                    string valSequence = string.Empty;
                    for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                    {
                        valComplianceID = -1;
                        valOfficerID = -1;
                        valOwnerID = -1;
                        valCustomerBranchID = -1;
                        valSequence = string.Empty;
                        #region 1 ComplianceID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                        {
                            valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                        }
                        if (valComplianceID == -1 || valComplianceID == 0)
                        {
                            errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                        }
                        else
                        {
                            if (ComplainceStatutoryExists(valComplianceID) == false)
                            {
                                errorMessage.Add("ComplianceID not defined in the System or Informative Compliance at row number-" + rowNum);
                            }
                        }
                        #endregion

                        #region 2 OwnerID OfficerID

                        valOwnerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 9].Text);
                        valOfficerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 10].Text);

                        if (valOwnerID != -1 || valOwnerID != 0)
                        {
                            if (UserOwnerExists(valOwnerID) == false)
                            {
                                errorMessage.Add("OwnerID not Defined in the System at row number-" + rowNum);
                            }

                        }

                        if (valOfficerID != -1 || valOfficerID != 0)
                        {
                            //if (valOfficerID == -1 || valOfficerID == 0)
                            //{
                            //    errorMessage.Add("Required OwnerID or OfficerID at row number-" + rowNum);
                            //}
                            //else
                            //{
                            if (UserOfficerExists(valOfficerID) == false)
                            {
                                errorMessage.Add("OfficerID not Defined in the System at row number-" + rowNum);
                            }
                            //}
                        }
                        else

                        #endregion


                        #region 3 CustomerBranchID
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                        {
                            valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                        }
                        if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                        {
                            errorMessage.Add("Required CustomerBranchID at row number-" + rowNum);
                        }
                        else
                        {
                            if (BranchIDExists(valCustomerBranchID) == false)
                            {
                                errorMessage.Add("CustomerBranchID not Defined in the System at row number-" + rowNum);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessagesUplad(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            int ComplianceID = -1;
                            int OfficerID = -1;
                            int OwnerID = -1; ;
                            int CustomerBranchID = -1;
                            List<string> abclist = new List<string>();
                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region OwnerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString()))
                            {
                                OwnerID = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text);
                            }
                            #endregion

                            #region OfficerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString()))
                            {
                                OfficerID = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text);
                            }
                            #endregion 

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            if ((Business.ComplianceCertificate.ExistComplianceInstance(ComplianceID, CustomerBranchID)))
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    ComplianceInstance TempAssP = new ComplianceInstance();
                                    TempAssP.ComplianceId = ComplianceID;
                                    TempAssP.CustomerBranchID = CustomerBranchID;
                                    TempAssP.Cer_OwnerUserID = OwnerID;
                                    TempAssP.Cer_OfficerUserID = OfficerID;

                                    ComplianceInstanceList.Add(TempAssP);
                                }
                            }
                        }
                        #endregion

                        if (ComplianceInstanceList.Count > 0)
                        {
                            suucess = UpdateExcelComplianceInstanceTable(ComplianceInstanceList);
                        }
                        else
                        {
                            suucess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public bool UserOwnerExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>)Cache["OwnerUserData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UserOfficerExists(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.User>)Cache["OfficerUserData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Userid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public void ErrorMessagesUplad(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry2.IsValid = false;
            cvDuplicateEntry2.ErrorMessage = finalErrMsg;
        }
        public bool UpdateExcelComplianceInstanceTable(List<ComplianceInstance> ComplianceInstanceTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ComplianceInstanceTable.ForEach(entry =>
                    {
                        ComplianceInstance complianceInstanceToUpdate = (from row in entities.ComplianceInstances
                                                                         where row.CustomerBranchID == entry.CustomerBranchID
                                                                         && row.ComplianceId == entry.ComplianceId
                                                                         select row).FirstOrDefault();

                        complianceInstanceToUpdate.Cer_OwnerUserID = entry.Cer_OwnerUserID;
                        complianceInstanceToUpdate.Cer_OfficerUserID = entry.Cer_OfficerUserID;

                        entities.SaveChanges();

                    });

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ComplainceStatutoryExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["EComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public void GetCertificateOfficerUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var OfficerUserList = HttpContext.Current.Cache["OfficerUserData"];

            if (OfficerUserList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID == CustomerID && row.Cer_OfficerRoleID == 1
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("OfficerUserData", Records); // add it to cache
                }
            }
        }
        public void GetCertificateOwnerUser(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.User> Records = new List<ComplianceManagement.Business.Data.User>();

            var OwnerUserList = HttpContext.Current.Cache["OwnerUserData"];

            if (OwnerUserList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Users
                               where row.CustomerID == CustomerID && row.Cer_OwnerRoleID == 1
                               && row.IsDeleted == false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("OwnerUserData", Records); // add it to cache
                }
            }
        }
        public static void GetStatutoryCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();

            var ComplianceList = HttpContext.Current.Cache["EComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Compliances
                               where row.IsDeleted == false
                               && row.Status == null
                               && row.ComplinceVisible == true
                               && row.IsMapped != false
                               && row.ComplianceType != 1
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("EComplianceListData", Records); // add it to cache
                }
            }
        }
        
        protected void BtnDownloadAssignment_Click(object sender, EventArgs e)
        {

            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AssignmentReportStatutory");
                        DataTable ExcelData = null;
                        DateTime CurrentDate = DateTime.Today.Date;

                        if (ddlECustomerList.SelectedValue != "-1")
                        {
                            int customerid = Convert.ToInt32(ddlECustomerList.SelectedValue);

                            var assignmentList = ComplianceCertificate.GetStatutoryComplianceAssigned(customerid, -1).ToList();

                            if (assignmentList.Count > 0)
                            {
                                DataView view = new System.Data.DataView((assignmentList).ToDataTable());

                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "BranchID", "Branch",
                                     "ActName", "ShortDescription", "Section", "Performer", "Reviewer", "Cer_OwnerUserID", "Cer_OfficerUserID");

                                exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["A1"].Value = "ComplianceID";
                                exWorkSheet.Cells["A1"].AutoFitColumns(30);

                                exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["B1"].Value = "BranchID";
                                exWorkSheet.Cells["B1"].AutoFitColumns(30);

                                exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["C1"].Value = "Location";
                                exWorkSheet.Cells["C1"].AutoFitColumns(30);

                                exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["D1"].Value = "Act Name";
                                exWorkSheet.Cells["D1"].AutoFitColumns(50);


                                exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["E1"].Value = "ShortDescription";
                                exWorkSheet.Cells["E1"].AutoFitColumns(50);

                                exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["F1"].Value = "Section";
                                exWorkSheet.Cells["F1"].AutoFitColumns(30);

                                exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["G1"].Value = "Performer";
                                exWorkSheet.Cells["G1"].AutoFitColumns(30);

                                exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["H1"].Value = "Reviewer";
                                exWorkSheet.Cells["H1"].AutoFitColumns(30);


                                exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["I1"].Value = "Certificate OwnerID";
                                exWorkSheet.Cells["I1"].AutoFitColumns(30);

                                exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheet.Cells["J1"].Value = "Certificate OfficerID";
                                exWorkSheet.Cells["J1"].AutoFitColumns(30);

                                using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 10])
                                {
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }

                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=AssignmentReportStatutory.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data Not Available!!!";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select Customer";
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}