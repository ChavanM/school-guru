﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class adfsauth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string errMessage = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["email"]))
                {
                    string email = string.Empty;
                    string encryptedEmail = Request.QueryString["email"];

                    if (!string.IsNullOrEmpty(encryptedEmail))
                    {
                        email = CryptographyManagement.Decrypt(encryptedEmail);
                    }
                    
                    Business.Data.User user = null;

                    if (UserManagement.IsValidUser(email.Trim(), out user))
                    {
                        if (user != null)
                        {
                            try
                            {
                                ProceedLogin(user, string.Empty, email);
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        else
                        {
                            errMessage = "No Registered User found with Email - " + email;
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = errMessage;
                        }
                    }
                    else
                    {
                        errMessage = "No Registered User found with Email- " + email;
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = errMessage;
                    }
                }
                else
                {
                    errMessage = "Not able to get Email from Request, Please contact administrator for more details";
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = errMessage;
                }
            }
        }

        public void ProceedLogin(User user, string ipaddress, string userEmail)
        {
            try
            {
                if (user != null)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    bool Success = true;
                    try
                    {
                        if (user.CustomerID != null)
                        {
                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                            if (!blockip.Item1)
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin for more details";
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    bool Auditorexpired = false;
                    if (user.RoleID == 9)
                    {
                        if (user.Enddate == null)
                        {
                            Auditorexpired = true;
                        }
                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                        {
                            Auditorexpired = true;
                        }
                    }
                    if (!Auditorexpired)
                    {
                        if (user.IsActive)
                        {
                            Session["userID"] = user.ID;
                            Session["ContactNo"] = user.ContactNumber;
                            Session["Email"] = user.Email;
                            Session["CustomerID_new"] = user.CustomerID;

                            if (user.RoleID == 9)
                            {
                                Session["Auditstartdate"] = user.AuditStartPeriod;
                                Session["Auditenddate"] = user.AuditEndPeriod;
                            }

                            //Generate Random Number 6 Digit For OTP.
                            Random random = new Random();
                            int value = random.Next(1000000);
                            if (value == 0)
                            {
                                value = random.Next(1000000);
                            }
                            Session["ResendOTP"] = Convert.ToString(value);
                            long Contact;
                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = userEmail,
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = false;
                            try
                            {
                                OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                if (OTPresult)
                                {
                                    OTPData.MobileNo = Contact;
                                }
                                else
                                {
                                    OTPData.MobileNo = 0;
                                }
                            }
                            catch (Exception ex)
                            {
                                OTPData.MobileNo = 0;
                                OTPresult = false;
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table. 
                            
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                {
                                    //Send Email on User Mail Id.
                                    if (user.CustomerID != 5)
                                    {
                                        //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                        SendGridEmailManager.SendGridMail(SenderEmailAddress, "alert@avantis.co.in", new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && 
                                (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                            {
                                try
                                {
                                    if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                    {
                                        //Send SMS on User Mobile No.                                        
                                        var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                        if (data != null)
                                        {
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                        }
                                        else
                                        {
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }

                            MaintainLoginDetail objData = new MaintainLoginDetail()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                Email = userEmail,
                                CreatedOn = DateTime.Now,
                                IPAddress = ipaddress,
                                MACAddress = ipaddress,
                                LoginFrom = "WC",
                                //ProfileID=user.ID
                            };
                            UserManagement.Create(objData);

                            if (Success)
                            {
                                Session["RM"] = false;
                                Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                Response.Redirect("~/Users/OTPVerify.aspx", false);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                            }
                            else
                            {
                                if (UserManagement.HasUserSecurityQuestion(user.ID))
                                {
                                    Session["RM"] = false;
                                    Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                    Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                                else
                                {
                                    Session["QuestionBank"] = true;
                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                }
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Your Account is In-Active, Please contact Company Admin(AVACOM) for more details.";
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your Account is Disabled";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}