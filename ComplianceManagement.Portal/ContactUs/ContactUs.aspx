﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.ContactUs" %>

<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Avantis Main 1">
<meta name="keywords" content="Homepage, Avantis">
<!-- Title -->
<title>Contact Us</title>
<!-- Favicon -->

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800' rel='stylesheet' type='text/css'>
<!-- Stylesheets -->
<link rel='stylesheet' id='twitter-bootstrap-css' href='../Style/NewCss/bootstrap.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='fontello-css' href='../Style/NewCss/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' id='prettyphoto-css-css' href='../Scripts/New/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' id='animation-css' href='../Style/NewCss/animation.css' type='text/css' media='all'/>
<link rel='stylesheet' id='flexSlider-css' href='../Style/NewCss/flexslider.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='../Style/NewCss/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='../Style/NewCss/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='../Style/NewCss/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='../Style/NewCss/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='../Style/NewCss/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
<!-- jQuery -->
<script src="../Scripts/New/jquery-1.11.1.min.js"></script>
<!-- <script src="js/hightlight.js"></script> -->
<!-- Google Maps -->
<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
<!--[if lt IE 9]>
            <script>
                document.createElement("header");
                document.createElement("nav");
                document.createElement("section");
                document.createElement("article");
                document.createElement("aside");
                document.createElement("footer");
                document.createElement("hgroup");
            </script>
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/html5.js"></script>
        <![endif]-->
<!--[if lt IE 7]>
            <script src="js/icomoon.js"></script>
        <![endif]-->
<!--[if lt IE 9]>
            <link href="css/ie.css" rel="stylesheet">
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/jquery.placeholder.js"></script>
            <script src="js/script_ie.js"></script>
        <![endif]-->
        <script type="text/javascript">
          //Code Starts
            function validatePhone(txtPhone) {
                alert('hi');
                var a = document.getElementById("txtPhone").value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }​
        </script>

</head>
<body class="w1170 headerstyle1 preheader-on">
<!-- Content Wrapper -->
<div id="Avantis-content-wrapper">
	<header id="header" class="style1">
<!--div id="upper-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="item left hidden-separator">
					<ul id="menu-shop-header" class="menu">
						
						<li class="menu-item"><a href="#">Wishlist</a></li>
						<li class="menu-item"><a href="#">My Account</a></li>
					</ul>
				</div>
				<div class="item right hidden-separator">
					<div class="cart-menu-item ">
						<a href="#">0 ITEMS(S) - <span class="amount">&pound;0.00</span></a>
						<div class="shopping-cart-dropdown">
							<div class="sc-header">
								<h4>Cart is empty</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Main Header -->
<div id="main-header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="col-lg-4 col-md-4 col-sm-4 logo">
				<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col-sm-8 align-right">
				<!-- Text List -->
				<!--<ul class="text-list">
					<li>Call Us: +91 98234 92350</li>
				</ul>-->
				<!-- Social Media -->
				<!--<ul class="social-media">
					<li><a target="_blank" href="#"><i class="icon-facebook"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-twitter"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-google"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-linkedin"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-instagram"></i></a>
					</li>
				</ul>-->
                <ul  class="menu top_menu" style="border-right:none;margin: 25px 0px 0px;">
					<li class="menu-item"><a href="Index.aspx">JOIN US</a></li>
                    <li class="menu-item"><a href="../Login.aspx">CUSTOMER LOGIN</a></li>
					<li class="menu-item"><a href="ContactUs.aspx" style="color: black;">CONTACT US</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /Main Header -->
<!-- Lower Header -->
<div id="lower-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="lower-logo">
					<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				</div>
				<!-- Main Navigation -->
				<ul id="main-nav" class="menu">
					<li class="menu-item"><a href="Index.aspx">Home</a></li>
                    <li class="menu-item"><a href="Product.aspx">Product</a></li>
                    <li class="menu-item"><a href="AboutUs.aspx">About</a></li>
                    <li class="menu-item"><a href="#">Case Studies</a></li>
					<li class="menu-item"><a href="#">Blog</a></li>
				</ul>
				<!-- /Main Navigation -->
				<!-- Search Box -->
				<div id="search-box" class="align-right">
					<i class="icons icon-search"></i>
					<form role="search" method="get" id="searchform" action="#">
						<input type="text" name="s" placeholder="Search here..">
						<div class="iconic-submit">
							<div class="icon">
								<i class="icons icon-search"></i>
							</div>
							<input type="submit" value="">
						</div>
					</form>
				</div>
				<!-- /Search Box -->
			</div>
		</div>
	</div>
</div>
<!-- /Lower Header -->
</header>
	<!-- /Header -->
	
	<div id="Avantis-content-inner">
	<!-- Main Content -->
    
	<section id="main-content">
	<!-- Container -->
	<div class="container">
		<!-- Google Map -->
		<section class="full-width google-map-ts">
		<div class="sc-map full">
			<div class="sc-map-container">
				<div>
					<div class="wpgmappity_container" id="wpgmappity-map-4">
					</div>
				</div>
			</div>
		</div>
		</section>
		<!-- /Google Map -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 small-padding">
				<div class="clearfix clearfix-height20">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h2 class=" wow animated fadeIn" data-animation="fadeIn">Contact Us</h2>
					<div class="clearfix clearfix-height10">
					</div>
					<form class="get-in-touch light align-left" runat="server">
                       <div class="iconic-input">
						<asp:Label runat="server" ID="LblMessage" Text="" style="color:Green"></asp:Label>
                       </div>
						<div class="iconic-input">
							<asp:TextBox runat="server" ID="txtName" placeholder="Name*"> </asp:TextBox>
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<asp:TextBox runat="server" ID="txtEmail" placeholder="Email*"></asp:TextBox>
							<i class="icons icon-email"></i>
						</div>
                        <div class="iconic-input">
							<asp:TextBox ID="txtPhone" runat="server" placeholder="Phone No*"></asp:TextBox>
							<i class="icons icon-email"></i>
						</div>
						<asp:TextBox ID="txtMsg" runat="server" placeholder="Message" TextMode="MultiLine"></asp:TextBox>
						<asp:Button ID="btnSave" Text="Send" OnClick="btnSave_Click" runat="server" OnClientClick="return validatePhone();"></asp:Button>
						<div class="iconic-button">
							<input type="reset" value="Clear">
							<i class="icons icon-cancel-circle-1"></i>
						</div>
					</form>
					<div id="msg">
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<h2 class=" wow animated fadeIn" data-animation="fadeIn">Contact Info</h2>
					<div class="clearfix clearfix-height10">
					</div>
					<div class="special-text Avantis-special-text" data-animation="">
						 Address:
					</div>
					 Pune, Maharashtra, India
					<div class="special-text Avantis-special-text" data-animation="">
						 Email:
					</div>
					 sandeep@avantis.co.in
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="clearfix clearfix-height60">
					</div>
					<div class="special-text Avantis-special-text" data-animation="">
						 Phone:
					</div>
					 +91 98234 92350
					<div class="special-text Avantis-special-text" data-animation="">
						 Fax:
					</div>
					+91 98234 92350
				</div>
				<div class="clearfix clearfix-height60">
				</div>
				<!--div class="container">
					<div class="row">
						<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
						<div class="full_bg full-width-bg Avantis-container-bg" data-animation="">
							<h2 class="align-center"><span class="Avantis-header6-h3-color">Our Team</span></h2>
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 1">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 1</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 2">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 2</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Sandra Green">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Sandra Green</h4>
										<span class="job-title Avantis-main-h4-span">Manager</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 3">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 3</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content ">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-skype"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-google"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-vimeo"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="clearfix">
							</div>
						</div>
						<div class="clearfix clearfix-height30">
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
						</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Container -->
	</section>
	<!-- /Main Content -->
</div>
	<!-- /Avantis Conten Inner -->
	<!-- Footer -->
<footer id="footer">
<!-- Upper Footer -->

<!-- /Upper Footer -->
<!-- Main Footer -->
<div id="main-footer" class="smallest-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-2" class="widget widget_text">
					<div class="textwidget">
						<img src="../Images/img/logo.png" alt="logo">
						<p>
							Avantis’ Compliance Product provides a comprehensive Solution to Organization’s complete compliance requirements. It is a secured web based product which can be accessed by all business and management users over the internet
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-4" class="widget widget_text">
					<h4>Contact</h4>
					<div class="textwidget">
						<ul class="iconic-list">
							<li>
							<i class="icons icon-location-7"></i>
							Pune, Maharashtra, India
							<li>
							<i class="icons icon-mobile-6"></i>
							Phone: +91 98234 92350 <br>
							 Fax: +91 98234 92350 </li>
							<li>
							<i class="icons icon-mail-7"></i>
							sandeep@avantis.co.in </li>
						</ul>
					</div>
				</div>
			</div>
			<%--<div class="col-lg-4 col-md-4 col-sm-4">
				<h4>Login</h4>
				<form class="get-in-touch contact-form wow animated fadeInUp align-center animated" method="post" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
						<input type="hidden" id="contact_nonce" name="contact_nonce" value="68592e213c"><input type="hidden" name="" value="/">
						<input type="hidden" name="contact-form-value" value="1">
						<div class="iconic-input">
							<input type="text" name="name" placeholder="Name*">
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<input type="password" name="passowrd" placeholder="passowrd*">
							<i class="icons icon-email"></i>
						</div>
						<input type="submit" value="Login" style="float:right;">
						
					</form>
			</div>--%>
		</div>
	</div>
</div>
<!-- /Main Footer -->
<!-- Lower Footer -->
<div id="lower-footer">
	<div class="container">
		<span class="copyright">© 2014 Avantis. All Rights Reserved</span>
	</div>
</div>
<!-- /Lower Footer -->
</footer>
	<!-- /Footer -->
</div>
<!-- /Content Wrapper -->
<!-- JavaScript -->
<script type='text/javascript' src='../Scripts/New/bootstrap.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery-ui.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/SmoothScroll.min.js'></script>
<script type='text/javascript' src='../Scripts/New/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='../Scripts/New/modernizr.js'></script>
<script type='text/javascript' src='../Scripts/New/wow.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.sharre.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.knob.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='../Scripts/New/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.fitvids.js'></script>
<script type='text/javascript' src='../Scripts/New/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.validity.min.js'></script>
<script type='text/javascript' src='../Scripts/New/tweetie.min.js'></script>
<script type='text/javascript' src='../Scripts/New/script.js'></script>
</body>
</html>