﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewHelp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.NewHelp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
 
   <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
    <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
    <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
    .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
   width:1180px;
 
  
}
     .MainContainer {
     
      /*height: 1000px;*/
      
      border:groove;
      border-color:#f1f3f4;
      
    }
       .img1{
        border:ridge;
        border-color:lightgrey;
    }
   
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       

 body {
  font-family: Arial, Helvetica, sans-serif;
 
}

* {
  box-sizing: border-box;
}

    </style>

  
   
    <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>  


</head>
    <body>
    <%--<form id="form1" runat="server">--%>
        &nbsp;&nbsp;&nbsp;&nbsp;  <a id="Mgmt" style="font-size:16px" visible="true" runat="server" href="NewMgmtHelp.aspx"><b>Management</b></a>
        &nbsp;&nbsp;&nbsp;&nbsp;<a id="perf" style="font-size:16px" visible="true" runat="server"   href="NewHelp.aspx"><b>Performer/Reviewer</b></a>
    
 <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                

             <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                           <%-- <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>--%>
                        </li>
                   <%-- <li style="font-size:14px" ><a href="../HelpSupport/Reviewerworkspace.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>Reviewer Workspace</b></a>
                  
                  --%>
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul>    
            <br/>
                
           </div>

          <div class="col-sm-9 MainContainer">
              <br />
              <br />
              <br />
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                  <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <i class="short-full glyphicon glyphicon-plus"></i>
                                  <b>Constant features of Performer and Reviewer user </b>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">



                              <div style="color: #666; background-color: white;"><b>1.Search-</b> On this tab, you can search any Act or Compliance assigned to you.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Search_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>2.Help Center-</b> It Includes Summary of Features, FAQs, and other details in text and video regarding how to use AVACOM.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Help_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>3.Message-</b> It includes the messages received from other users of your organization.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Message_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>4.Notification center-</b> It includes the information of any latest update or amendment in the compliances assigned to you.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Notification_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>5.Profile -</b> You can change your password, security Questions and logout. And following steps will guide you for same, </div>
                              <ul style="list-style-type: lower-alpha;line-height:25px;">
                                  <li><b>Change Password: -</b>
                                      <ul style="list-style-type: square">
                                          <li>Enter Old Password</li>
                                          <li>Enter New Password</li>
                                          <li>Confirm New Password</li>
                                          <li>Click Submit </li>
                                      </ul>
                                  </li>
                                  <li><b>Change Security Questions: -</b>
                                      <ul style="list-style-type: square;">
                                          <li>Select any three secret Questions </li>
                                          <li>Enter their Answers</li>
                                          <li>Click Submit </li>
                                      </ul>
                                  </li>
                                  <li><b>Check features you want to be showcased on Dashboard. </b></li>
                                  <li style="font-size: 16px; font-family: 'Times New Roman'"><b>Logout </b></li>
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 730px; margin-left: 20px; height: 400px" src="../ImagesHelpCenter/Profile_Screenshot.png" /><br />
                                  <br />
                              </ul>



                              <div style="color: #666;line-height:25px; background-color: white;">
                                  <b>6.Toggle navigation-</b><br />
                                  <b>Question -</b>  How do I hide/collapse the side menu?
                                  <br />
                                  <b>Answer-</b> Click on the hamburger icon to hide/collapse the side menu and to view menu click on hamburger icon.<br />
                                  <br />
                                  <img class="img1" style="width: 730px; margin-left: 70px;  height: 400px" src="../ImagesHelpCenter/Toggle%20Navigation_Screenshot.png" /><br />
                                  <br />
                              </div>

                              <div style="color: #666;line-height:25px; background-color: white;">
                                  <b>7.  Chat- </b>
                                  <br />
                                  <b>Question –</b> With whom can I chat?<br />
                                  <b>Answer-</b> You can connect with our Client Support Team directly through this online chat and resolve your queries regarding software and performing compliances. Following steps will help you use Chat feature, 
                        <ul style="list-style-type: square">
                            <li>Click Chat Option</li>
                            <li>Enter your Email-id    </li>
                            <li>Click Start Chat</li>
                        </ul>
                                  <br />
                                  <img class="img1" style="margin-left: 70px" src="../ImagesHelpCenter/Chat_1_Screenshot.png" /><br />
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 700px; margin-left: 70px; height: 400px" src="../ImagesHelpCenter/Chat_2_Screenshot.png" /><br />
                                  <br />
                              </div>

                              <div style="color: #666;line-height:25px; background-color: white;">
                                  <b>8. Internal message system-  </b>
                                  <br />
                                  <b>Question –</b> How can I send message and to whom I can send?<br />
                                  <b>Answer-</b> You can send message to other users of your organization and with message you can attach documents as well.  Following steps will help you use this feature,	   
                        <ul style="list-style-type: square">
                            <li>Click Internal Message Option </li>
                            <li>Enter the details subject, message etc.   </li>
                            <li>To attach any file with message click Choose Files and upload the file.</li>
                            <li>Click Send</li>
                        </ul>
                                  <br />
                                  <img class="img1" style="margin-left: 50px;" src="../ImagesHelpCenter/Message_1_Screenshot.png" /><br />
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 700px; margin-left: 50px;  height: 400px" src="../ImagesHelpCenter/Message_2_Screenshot.png" /><br />
                                  <br />
                                  <br />
                              </div>

                              <div style="color: #666;line-height:25px; background-color: white;">
                                  <b>9. Support ticket-  </b>
                                  <br />
                                  <b>Question –</b>  How can I use this feature?<br />
                                  <b>Answer-</b> You can create a support ticket, bifurcating your issue into (legal, sales, technical or general query)and you will get response  on your registered email address within 48 hours.                                                               
                        <ul style="list-style-type: square">
                            <li>Click Support Ticket Option </li>
                            <li>Enter the Subject   </li>
                            <li>Select Issue from Drop Down list</li>
                            <li>Enter Message</li>
                            <li>Click Create Ticket</li>
                            <br />
                            <br />
                        </ul>
                                  <img class="img1" style="margin-left: 50px;" src="../ImagesHelpCenter/Support%20Ticket_1_Screenshot.png" /><br />
                                  <br />
                                  <img class="img1" style="width: 700px; margin-left: 60px;  height: 400px" src="../ImagesHelpCenter/Support%20Ticket_2_Screenshot.png" /><br />
                                  <br />
                              </div>

                              <div style="color: #666;line-height:25px; background-color: white;">
                                  <b>10. If I am Performer and Reviewer both?</b>
                                  <br />
                                  <b>Answer-</b>  You can access features of both the users. And your Dashboard will be showcased in following manner,<br />
                                  <br />
                                  <img class="img1" style="width: 700px; margin-left: 60px; height: 400px" src="../ImagesHelpCenter/PerformerReviewer_Screenshot.png" />
                              </div>


                          </div>

                      </div>

                  </div>



              </div>
              <!-- panel-group -->


          </div>
          <!-- container -->
</div>

 </div>

    </form>
   
       
<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>



 
</html>
