﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskStatusTransactionPerformer : System.Web.UI.Page
    {
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        public static List<SP_TaskInstanceTransactionStatutoryView_Result> MastersTasklist = new List<SP_TaskInstanceTransactionStatutoryView_Result>();
        public static List<SP_TaskInstanceTransactionInternalView_Result> MastersTasklistinternal = new List<SP_TaskInstanceTransactionInternalView_Result>();
        public static List<TaskDocumentsView> MastersTaskDocumentslistinternal = new List<TaskDocumentsView>();
        protected string UploadDocumentLink;
        public string ISmail;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ISmail = "0";
                try
                {
                    ISmail = Convert.ToString(Request.QueryString["ISM"]);
                    if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                    {
                        lnkgotoportal.Visible = true;
                    }
                    else
                    {
                        lnkgotoportal.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                    ISmail = "0";
                }
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";

                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["TID"]) && !string.IsNullOrEmpty(Request.QueryString["TSOID"]))
                    {
                        var taskInstanceID = Request.QueryString["TID"];
                        var taskScheduleOnID = Request.QueryString["TSOID"];

                        if (taskInstanceID != "" && taskScheduleOnID != "")
                        {
                            BindTransactionDetails(Convert.ToInt32(taskInstanceID), Convert.ToInt32(taskScheduleOnID));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTransactionDetails(int taskInstanceID, int taskScheduledOnID)
        {          
            //added by rhul on 7 June 2019 For check transaction done or not   
            var RTT = TaskManagment.GetCurrentTaskStatus(taskInstanceID, taskScheduledOnID);           
            if (RTT.TaskStatusID == 2 || RTT.TaskStatusID == 3 || RTT.TaskStatusID == 4 || RTT.TaskStatusID == 5)
            {
                btnSave.Visible = false;
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "This task is already submitted for review or already closed.";
            }
            else
            {
                btnSave.Visible = true;
                try
                {
                    var taskDetail = TaskManagment.GetTaskDetail(taskScheduledOnID, taskInstanceID, 4); //In Case Performer wee need to show Reviewer Name 
                    if (taskDetail != null)
                    {
                        lblTaskTitle.Text = taskDetail.TaskTitle;
                        lblTaskDesc.Text = taskDetail.TaskDescription;
                        lblTaskCompliance.Text = TaskManagment.GetTaskComplianceDescription(Convert.ToInt32(taskDetail.MainTaskID), taskDetail.TaskType);
                        lblTaskReviewer.Text = taskDetail.User;
                        lblTaskDueDate.Text = Convert.ToDateTime(taskDetail.ScheduledOn).ToString("dd-MMM-yyyy");
                        lblLocation.Text = taskDetail.Branch;

                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            lblPeriod1.Text = Business.ComplianceManagement.PeriodReplace(taskDetail.ForMonth);
                            lblPeriod.Text = taskDetail.ForMonth;
                        }
                        else
                        {
                            lblPeriod1.Text = taskDetail.ForMonth;
                            lblPeriod.Text = taskDetail.ForMonth;
                        }

                        tbxDate.Text = string.Empty;
                        tbxRemarks.Text = string.Empty;

                        grdDocument.DataSource = null;
                        grdDocument.DataBind();

                        ViewState["taskInstanceID"] = taskInstanceID;
                        hdnTaskInstanceID.Value = Convert.ToString(taskInstanceID);

                        ViewState["taskScheduleOnID"] = taskScheduledOnID;
                        hdnTaskScheduledOnID.Value = Convert.ToString(taskScheduledOnID);

                        ViewState["ComplianceScheduleOnID"] = taskScheduledOnID;
                        hdnComplianceScheduledOnID.Value = Convert.ToString(taskDetail.ComplianceScheduleOnID);

                        BindTaskTransactionLog(Convert.ToInt32(taskDetail.ComplianceScheduleOnID), taskScheduledOnID);
                        BindTempDocumentData(taskScheduledOnID);
                       
                        if (RTT != null)
                        {                            
                            btnSave.Attributes.Remove("disabled");
                            BindStatusList(Convert.ToInt32(RTT.TaskStatusID));
                            if (RTT.TaskStatusID == 1 || RTT.TaskStatusID == 6 || RTT.TaskStatusID == 10 || RTT.TaskStatusID == 13 || RTT.TaskStatusID == 14)
                            {
                                divUploadDocument.Visible = true;
                                btnSave.Visible = true;
                            }
                            else if (RTT.TaskStatusID != 1)
                            {
                                divUploadDocument.Visible = false;
                                btnSave.Attributes.Add("disabled", "disabled");
                            }
                            upTaskPerformer.Update();
                        }
                        var IsAfter = TaskManagment.GetTaskByID(Convert.ToInt32(taskDetail.TaskID)).IsAfter;
                        var showHideButton = false;
                        if (IsAfter == false)
                        {
                            showHideButton = BindSubTasks(taskDetail.TaskType, taskDetail.TaskID, taskDetail.RoleID, taskDetail.ComplianceScheduleOnID);
                            btnSave.Enabled = showHideButton;
                        }
                        else
                        {
                            showHideButton = BindSubTasks(taskDetail.TaskType, taskDetail.TaskID, taskDetail.RoleID, taskDetail.ComplianceScheduleOnID);
                            btnSave.Enabled = true;
                        }

                        var taskForm = Business.TaskManagment.GetTaskFormByTaskID(taskDetail.TaskID);
                        string sampleFormPath = string.Empty;

                        if (taskForm != null)
                        {
                            lbDownloadSample.Text = "Download";
                            lbDownloadSample.CommandArgument = taskForm.TaskID.ToString();
                            sampleFormPath = taskForm.FilePath;
                            sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);

                            lblNote.Visible = true;
                            lnkViewSampleForm.Visible = true;
                            lblSlash.Visible = true;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                lblpathsample.Text = sampleFormPath;
                            }
                            else
                            {
                                lblpathsample.Text = sampleFormPath;
                            }
                        }
                        else
                        {
                            lblNote.Visible = false;
                            lblSlash.Visible = false;
                            lnkViewSampleForm.Visible = false;
                            sampleFormPath = "";
                        }

                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTaskPerformer.IsValid = false;
                    cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                ddlStatus.DataSource = allowedStatusList;
                ddlStatus.DataBind();

                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //added by rhul on 7 June 2019 For check transaction done or not
            var TIID = Convert.ToInt32(hdnTaskInstanceID.Value);
            var TSOID = Convert.ToInt32(hdnTaskScheduledOnID.Value);
            var RTT = TaskManagment.GetCurrentTaskStatus(TIID, TSOID);
            if (RTT.TaskStatusID == 2 || RTT.TaskStatusID == 3)
            {
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "This task is already submitted for review or already closed.";
            }
            else
            {
                #region Save Code
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                try
                {
                    bool isValidFile = true;
                    if (fuTaskDoc.HasFile)
                    {
                        string[] inValidFileTypes = { "exe", "bat", "dll" };

                        string fileExt = System.IO.Path.GetExtension(fuTaskDoc.PostedFile.FileName);

                        for (int i = 0; i < inValidFileTypes.Length; i++)
                        {
                            if (fileExt == "." + inValidFileTypes[i])
                            {
                                isValidFile = false;
                                break;
                            }
                        }
                    }

                    if (isValidFile)
                    {
                        if (grdDocument.Rows.Count > 0)
                        {
                            long? StatusID = TaskManagment.GetCurrentTaskStatus(Convert.ToInt32(hdnTaskInstanceID.Value), Convert.ToInt32(hdnTaskScheduledOnID.Value)).TaskStatusID;

                            TaskTransaction transaction = new TaskTransaction()
                            {
                                ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnID.Value),
                                TaskScheduleOnID = Convert.ToInt64(hdnTaskScheduledOnID.Value),
                                TaskInstanceId = Convert.ToInt64(hdnTaskInstanceID.Value),
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                                StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                Remarks = tbxRemarks.Text,
                            };
                            var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                            if (leavedetails != null)
                            {
                                transaction.OUserID = leavedetails.OldPerformerID;
                            }
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<TaskFileData> files = new List<TaskFileData>();

                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            string directoryPath = null;

                            var TempDocData = Business.ComplianceManagement.GetTempTaskDocumentData(Convert.ToInt64(hdnTaskScheduledOnID.Value));

                            if (TempDocData.Count > 0)
                            {
                                int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                                if (AWSData != null)
                                {
                                    #region AWS Storage
                                    var InstanceData = TaskManagment.GetTaskInstanceByID(Convert.ToInt32(hdnTaskInstanceID.Value));

                                    string version = null;

                                    if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                                    {
                                        version = TaskManagment.GetTaskDocumentVersion(Convert.ToInt32(hdnTaskScheduledOnID.Value));
                                    }
                                    else
                                    {
                                        version = "1.0";
                                    }
                                    directoryPath = "AvacomTaskDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnTaskScheduledOnID.Value + "\\" + version;
                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink == true)
                                        {
                                            String fileName = "";

                                            if (item.DocType == "T")
                                            {
                                                fileName = "TaskDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }

                                            TaskFileData file = new TaskFileData()
                                            {
                                                Name = fileName,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.Now,
                                                ISLink = true,
                                                Version = version
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            String fileName = "";

                                            if (item.DocType == "T")
                                            {
                                                fileName = "TaskDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));

                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                string filepathvalue = string.Empty;
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");

                                                TaskFileData file = new TaskFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue,
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };
                                                files.Add(file);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                        }
                                    }
                                    #endregion
                                  
                                }
                                else
                                {
                                    #region Normal Storage
                                    var InstanceData = TaskManagment.GetTaskInstanceByID(Convert.ToInt32(hdnTaskInstanceID.Value));
                                    string version = null;

                                    if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                                    {
                                        version = TaskManagment.GetTaskDocumentVersion(Convert.ToInt32(hdnTaskScheduledOnID.Value));
                                    }
                                    else
                                    {
                                        version = "1.0";
                                    }

                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomTaskDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnTaskScheduledOnID.Value + "\\" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/AvacomTaskDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnTaskScheduledOnID.Value + "/" + version);
                                    }

                                    DocumentManagement.CreateDirectory(directoryPath);

                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink == true)
                                        {
                                            String fileName = "";

                                            if (item.DocType == "T")
                                            {
                                                fileName = "TaskDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }

                                            TaskFileData file = new TaskFileData()
                                            {
                                                Name = fileName,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.Now,
                                                ISLink = true,
                                                Version = version
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            String fileName = "";

                                            if (item.DocType == "T")
                                            {
                                                fileName = "TaskDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));

                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                string filepathvalue = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                    filepathvalue = vale.Replace(@"\", "/");
                                                }
                                                else
                                                {
                                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                }
                                                TaskFileData file = new TaskFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue,
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };
                                                files.Add(file);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                        }
                                    }
                                    #endregion
                                }

                            }

                            bool flag = false;
                            bool flag1 = false;
                            if (blankfileCount)
                            {
                                flag = TaskManagment.CreateTaskTransactionAWS(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnTaskScheduledOnID.Value), customerID);
                                flag1 = Business.ComplianceManagement.DeleteTempTaskDocumentFileFromScheduleOnID(Convert.ToInt64(hdnTaskScheduledOnID.Value));

                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);

                                cvTaskPerformer.IsValid = false;
                                cvTaskPerformer.ErrorMessage = "Please do not upload virus file or blank files.";
                            }

                            //bool flag = true;
                            if (flag != true)
                            {
                                cvTaskPerformer.IsValid = false;
                                cvTaskPerformer.ErrorMessage = "Something went wrong, Please try again.";
                            }
                            else
                            {
                                cvTaskPerformer.IsValid = false;
                                cvTaskPerformer.ErrorMessage = "Save Sucessfully";
                            }
                        }
                        else
                        {
                            cvTaskPerformer.IsValid = false;
                            cvTaskPerformer.ErrorMessage = "Please select documents for upload.";
                        }
                    }
                    else
                    {
                        cvTaskPerformer.IsValid = false;
                        cvTaskPerformer.ErrorMessage = "Invalid file Uploaded (.exe, .bat, .dll formats not supported)";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTaskPerformer.IsValid = false;
                    cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        private void BindTaskTransactionLog(int complianceScheduleOnID, int taskScheduledOnID)
        {
            try
            {
                grdTransactionLogHistory.DataSource = TaskManagment.GetAllTaskTransactionLog(taskScheduledOnID, complianceScheduleOnID);
                grdTransactionLogHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private bool BindSubTasks(int taskType, long parentTaskID, string period, int roleID, int customerBranchID)
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //        if (taskType == 1)
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
        //                                    where row.ParentID == parentTaskID
        //                                    && row.CustomerBranchID == customerBranchID
        //                                    && row.ForMonth == period
        //                                    && row.RoleID == roleID
        //                                    select row).ToList();

        //                gridSubTask.DataSource = documentData;
        //                gridSubTask.DataBind();

        //                var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

        //                if (documentData.Count == closedSubTaskCount)
        //                    return true;
        //                else
        //                    return false;
        //            }
        //        }

        //        else if (taskType == 2)
        //        {
        //            using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //            {
        //                var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
        //                                    where row.ParentID == parentTaskID
        //                                    && row.ForMonth == period
        //                                    && row.RoleID == roleID
        //                                    select row).ToList();

        //                gridSubTask.DataSource = documentData;
        //                gridSubTask.DataBind();

        //                if (documentData.Count == documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count())
        //                    return true;
        //                else
        //                    return false;
        //            }
        //        }
        //        else
        //            return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvTaskPerformer.IsValid = false;
        //        cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
        //        return false;
        //    }
        //}

        private bool BindSubTasks(int taskType, long parentTaskID, int roleID, long? complianceScheduleOnID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (taskType == 1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                            where row.ParentID == parentTaskID
                                            && row.ComplianceScheduleOnID == complianceScheduleOnID
                                            && row.RoleID == roleID
                                            select row).ToList();
                        MastersTasklist = documentData;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();
                        if (documentData.Count != 0)
                            divTaskSubTask.Visible = true;
                        else
                            divTaskSubTask.Visible = false;
                        var LinkTaskIDList = (from row in entities.SP_TaskLinkingInstanceTransactionStatutoryView(customerID)
                                                where row.TaskID == parentTaskID
                                                && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                && row.RoleID == roleID
                                                select row.TasklinkID).ToList();

                        var LinkdocumentData = (from row in entities.SP_TaskLinkingInstanceTransactionStatutoryView(customerID)
                                                where LinkTaskIDList.Contains(row.TaskID) 
                                                && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                && row.RoleID == roleID
                                                select row).ToList();

                        gridLinkSubTask.DataSource = LinkdocumentData;
                        gridLinkSubTask.DataBind();
                        if (LinkdocumentData.Count != 0)
                            divLinkTaskSubTask.Visible = true;
                        else
                            divLinkTaskSubTask.Visible = false;
                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                }

                else if (taskType == 2)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)                                      
                                            select row).ToList();

                        var TaskdocumentData = (from row in entities.TaskDocumentsViews
                                                where row.CustomerID== customerID && row.TaskType==2
                                                select row).ToList();
                        if (TaskdocumentData.Count>0)
                        {
                            TaskdocumentData = TaskdocumentData.Where(entry => entry.TaskStatusID == 4 || entry.TaskStatusID == 5).ToList();
                            MastersTaskDocumentslistinternal = TaskdocumentData;
                        }
                        MastersTasklistinternal = documentData;
                      
                        documentData = (from row in documentData
                                            where row.ParentID == parentTaskID
                                            && row.ComplianceScheduleOnID == complianceScheduleOnID
                                            && row.RoleID == roleID
                                            select row).ToList();
                
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();
                        if (documentData.Count != 0)
                            divTaskSubTask.Visible = true;
                        else
                            divTaskSubTask.Visible = false;
                        var LinkTaskIDList = (from row in entities.SP_TaskLinkingInstanceTransactionInternalView(customerID)
                                              where row.TaskID == parentTaskID
                                              && row.ComplianceScheduleOnID == complianceScheduleOnID
                                              && row.RoleID == roleID
                                              select row.TasklinkID).ToList();

                        var LinkdocumentData = (from row in entities.SP_TaskLinkingInstanceTransactionInternalView(customerID)
                                                where LinkTaskIDList.Contains(row.TaskID) 
                                                && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                && row.RoleID == roleID
                                                select row).ToList();

                        gridLinkSubTask.DataSource = LinkdocumentData;
                        gridLinkSubTask.DataBind();
                        if (LinkdocumentData.Count != 0)
                            divLinkTaskSubTask.Visible = true;
                        else
                            divLinkTaskSubTask.Visible = false;
                        if (documentData.Count == documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count())
                            return true;
                        else
                            return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        protected void grdTransactionLogHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (hdnComplianceScheduledOnID.Value != null && hdnTaskScheduledOnID.Value != null)
                {
                    grdTransactionLogHistory.PageIndex = e.NewPageIndex;
                    BindTaskTransactionLog(Convert.ToInt32(hdnComplianceScheduledOnID.Value), Convert.ToInt32(hdnTaskScheduledOnID.Value));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upTaskPerformer_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerPerformer", "initializeDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                           
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                           
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            
                                            string extension = System.IO.Path.GetExtension(filePath);

                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            subTaskDocViewPath = filePath1;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");
                    Label lblTaskID = (Label)e.Row.FindControl("lblTaskID");
                    Label lblsubtasks = (Label)e.Row.FindControl("lblsubtasks");
                    Label lblCbranchId = (Label)e.Row.FindControl("lblCbranchId");
                    Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                    Label lblsubtaskDocuments = (Label)e.Row.FindControl("lblsubtaskDocuments");
                    Image chkDocument = (Image)e.Row.FindControl("chkDocument");
                    
                    int taskId = Convert.ToInt32(lblTaskID.Text);
                    int  CbranchId = Convert.ToInt32(lblCbranchId.Text);
                    if (MastersTasklist.Count > 0)
                    {
                        var documentData = (from row in MastersTasklist
                                            where row.ParentID == (long?)taskId
                                            select row).ToList();
                        string strSubtasks = "";
                        string strDocumentSubtasks = "";
                        foreach (var item in documentData)
                        {
                            strSubtasks += "<br/>" + item.TaskTitle;
                            strDocumentSubtasks += "<br/>" + item.TaskTitle;
                        }
                        lblsubtasks.Text = strSubtasks;
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }
                    else if (MastersTasklistinternal.Count > 0)
                    {
                        var documentData = (from row in MastersTasklistinternal
                                            where row.ParentID == (long?)taskId
                                             && row.ForMonth == lblForMonth.Text &&
                                             row.CustomerBranchID == (long?)CbranchId
                                             && row.RoleID == 3
                                            select row).ToList();
                        
                        string strDocumentSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        string strSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        int DocCounter = 0;
                        int counter = 0;
                        foreach (var item in documentData)
                        {
                            counter += 1;
                            string disp = "none";
                            if (item.UserID == AuthenticationHelper.UserID && (item.TaskStatusID == 1 || item.TaskStatusID == 8))
                            {
                                disp = "block";
                            }
                            #region Document
                            if (MastersTaskDocumentslistinternal.Count > 0)
                            {                               
                                var taskdocumentData = (from row in MastersTaskDocumentslistinternal
                                                        where row.ForMonth == item.ForMonth &&
                                                         row.CustomerBranchID == item.CustomerBranchID
                                                         && row.TaskScheduleOnID==item.TaskScheduledOnID
                                                        select row).ToList();
                                if (taskdocumentData.Count>0)
                                {
                                    DocCounter = 1; 
                                     strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                    strDocumentSubtasks += "</div>";
                                }                                
                            }
                            #endregion

                            if (1==1)
                            {
                                string topline = "";
                                if (counter > 1)
                                {
                                    topline = "topline";
                                }
                                strSubtasks += "<div style='float:left;' class='topdivlive "+ topline + "'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='openTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";

                                var documentData1 = (from row in MastersTasklistinternal
                                                     where 
                                                     //row.ParentID == (long?)taskId &&
                                                      row.ForMonth == lblForMonth.Text &&
                                                      row.CustomerBranchID == (long?)CbranchId
                                                      && row.RoleID == 3
                                                       && row.ParentID == item.TaskID   
                                                     select row).ToList();
                                foreach (var item1 in documentData1)
                                {
                               
                                    disp = "none";
                                    if (item1.UserID == AuthenticationHelper.UserID && (item1.TaskStatusID == 1 || item1.TaskStatusID == 8))
                                    {
                                        disp = "block";
                                    }
                                    //style='display:"+ disp + "'
                                    #region Document1
                                    if (MastersTaskDocumentslistinternal.Count > 0)
                                    {
                                        var taskdocumentData1 = (from row in MastersTaskDocumentslistinternal
                                                                 where row.ForMonth == item1.ForMonth &&
                                                                  row.CustomerBranchID == item1.CustomerBranchID
                                                                  && row.TaskScheduleOnID == item1.TaskScheduledOnID
                                                                 select row).ToList();
                                        if (taskdocumentData1.Count > 0)
                                        {
                                            DocCounter = 1;
                                            strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item1.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                            strDocumentSubtasks += "</div>";
                                        }
                                    }
                                    #endregion
                                                                     
                                    strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item1.TaskTitle + "</div><div style='float:left;margin-left: -5px;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "' onclick='openTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                    var documentData2 = (from row in MastersTasklistinternal
                                                         where 
                                                         //row.ParentID == (long?)taskId &&
                                                           row.ForMonth == lblForMonth.Text &&
                                                          row.CustomerBranchID == (long?)CbranchId
                                                          && row.RoleID == 3
                                                           && row.ParentID == item1.TaskID
                                                         select row).ToList();
                                    foreach (var item2 in documentData2)
                                    {
                                        disp = "none";
                                        if (item2.UserID == AuthenticationHelper.UserID && (item2.TaskStatusID == 1 || item2.TaskStatusID == 8))
                                        {
                                            disp = "block";
                                        }
                                        //style='display:"+ disp + "'
                                        #region Document2
                                        if (MastersTaskDocumentslistinternal.Count > 0)
                                        {
                                            var taskdocumentData2 = (from row in MastersTaskDocumentslistinternal
                                                                     where row.ForMonth == item2.ForMonth &&
                                                                      row.CustomerBranchID == item2.CustomerBranchID
                                                                      && row.TaskScheduleOnID == item2.TaskScheduledOnID
                                                                     select row).ToList();
                                            if (taskdocumentData2.Count > 0)
                                            {
                                                strDocumentSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                strDocumentSubtasks += "</div>";
                                            }
                                        }
                                        #endregion

                                        
                                        strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                        var documentData3 = (from row in MastersTasklistinternal
                                                             where 
                                                             //row.ParentID == (long?)taskId &&
                                                               row.ForMonth == lblForMonth.Text &&
                                                              row.CustomerBranchID == (long?)CbranchId
                                                              && row.RoleID == 3
                                                               && row.ParentID == item2.TaskID
                                                             select row).ToList();
                                        foreach (var item3 in documentData3)
                                        {
                                            disp = "none";
                                            if (item3.UserID == AuthenticationHelper.UserID && (item3.TaskStatusID == 1 || item3.TaskStatusID == 8))
                                            {
                                                disp = "block";
                                            }
                                            //style='display:"+ disp + "'
                                            #region Document3
                                            if (MastersTaskDocumentslistinternal.Count > 0)
                                            {
                                                var taskdocumentData3 = (from row in MastersTaskDocumentslistinternal
                                                                         where row.ForMonth == item3.ForMonth &&
                                                                          row.CustomerBranchID == item3.CustomerBranchID
                                                                          && row.TaskScheduleOnID == item3.TaskScheduledOnID
                                                                         select row).ToList();
                                                if (taskdocumentData3.Count > 0)
                                                {
                                                    DocCounter = 1;
                                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item3.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button'  onclick ='downloadTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                    strDocumentSubtasks += "</div>";
                                                }
                                              
                                              
                                            }
                                            #endregion

                                            strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item3.TaskTitle + "</div><div style='float:left;margin-left: -15px;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' style='display:" + disp + "' class='topdivliveimage'><input type='button'  onclick='openTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                            strSubtasks += "</div>";
                                        }
                                        strSubtasks += "</div>";
                                       

                                    }
                                    strSubtasks += "</div>";
                                  
                                }
                                strSubtasks += "</div>";
                              
                            }
                            else
                            {
                                //strSubtasks += "<div style='float:left;'  class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'></div></div><div style='clear:both;height:5px;'>";

                                //var documentData1 = (from row in MastersTasklistinternal
                                //                     where row.ParentID == (long?)taskId
                                //                      && row.ForMonth == lblForMonth.Text &&
                                //                      row.CustomerBranchID == (long?)CbranchId
                                //                      && row.RoleID == 3
                                //                       && row.ParentID == item.TaskID
                                //                     select row).ToList();                                
                                //foreach (var item1 in documentData1)
                                //{
                                //    strSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item1.TaskTitle + "</div><div style='float:left;margin-left: -5px;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' onclick='openTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                //    var documentData2 = (from row in MastersTasklistinternal
                                //                         where row.ParentID == (long?)taskId
                                //                          && row.ForMonth == lblForMonth.Text &&
                                //                          row.CustomerBranchID == (long?)CbranchId
                                //                          && row.RoleID == 3
                                //                           && row.ParentID == item.TaskID
                                //                         select row).ToList();
                                //    foreach (var item2 in documentData2)
                                //    {
                                //        strSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' onclick='openTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                //        var documentData3 = (from row in MastersTasklistinternal
                                //                             where row.ParentID == (long?)taskId
                                //                              && row.ForMonth == lblForMonth.Text &&
                                //                              row.CustomerBranchID == (long?)CbranchId
                                //                              && row.RoleID == 3
                                //                               && row.ParentID == item.TaskID
                                //                             select row).ToList();
                                //        foreach (var item3 in documentData3)
                                //        {
                                //            strSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item3.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' onclick='openTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                //            strSubtasks += "</div>";
                                //        }
                                //        strSubtasks += "</div>";
                                //    }
                                //    strSubtasks += "</div>";
                                //}
                                //strSubtasks += "</div>";
                            }
                        }
                        lblsubtasks.Text = strSubtasks;
                        if(DocCounter==0)
                        {
                            chkDocument.Visible = false;
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }
                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptComplianceVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                rptComplianceVersionView.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";
                                        //UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkTask_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    foreach (GridViewRow gvrow in gridSubTask.Rows)
                    {
                        CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                        if (chk != null & chk.Checked)
                        {
                            int TaskID = Convert.ToInt32(gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString());
                            string str = gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString();
                            Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                            int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                            Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                            int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                            Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                            int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                            Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                            int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                            Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                            string ForMonth = Convert.ToString(lblForMonth.Text);


                            if (TaskID != -1)
                            {
                                long? parentID = TaskID;
                                List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    Tasklist.Clear();
                                    GetAllHierarchyTask(TaskID);
                                    int customerID = -1;
                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                    var documentData = (from row in entities.TaskInstanceTransactionViews
                                                        where Tasklist.Contains(row.TaskID)
                                                        && row.ForMonth == ForMonth
                                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                        && row.RoleID == 4
                                                        && row.CustomerID == customerID
                                                        select row).ToList();

                                    for (int i = 0; i < documentData.Count; i++)
                                    {
                                        var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                        if (IsTaskTransactionPresent == false)
                                        {
                                            int StatusID = 4;
                                            TaskTransaction transaction = new TaskTransaction()
                                            {
                                                ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                                TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                                TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = StatusID,
                                                StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                                Remarks = "Not applicable",
                                                IsTaskClose = true,
                                            };
                                            bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                                        }
                                    }
                                }
                            }
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);
                //List<int> lst = new List<int>();

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                //long tskid = Convert.ToInt32(hierarchy.ToList().Select(entry => entry.ID));
                //Tasklist.Add(tskid);

                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTask.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                                    }
                                }
                            }
                        }
                    }
                }
                var taskInstanceID = Request.QueryString["TID"];
                var taskScheduleOnID = Request.QueryString["TSOID"];

                if (taskInstanceID != "" && taskScheduleOnID != "")
                {
                    BindTransactionDetails(Convert.ToInt32(taskInstanceID), Convert.ToInt32(taskScheduleOnID));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridLinkSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblLinkTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblLinkTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            subTaskDocViewPath = filePath1;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gridLinkSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblLinkStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblLinkSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnLinkSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnLinkSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkLinkTask");
                    //Label lblIsTaskClose = (Label)e.Row.FindControl("lblLinkIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                //if (lblIsTaskClose.Text == "True")
                                //{
                                //    chkTask.Enabled = false;
                                //    chkTask.Checked = true;
                                //    btnSubTaskDocDownload.Visible = false;
                                //    lblSlashReview.Visible = false;
                                //    btnSubTaskDocView.Visible = false;
                                //}
                                //else
                                //{
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.TaskManagment.GetTaskFormByTaskID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                if (file != null)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                    Response.BinaryWrite(file.FileData); // create the file
                    Response.Flush(); // send it to the client to download

                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long taskScheduledOnID = Convert.ToInt64(ViewState["taskScheduleOnID"]);
                long taskInstanceID = Convert.ToInt64(ViewState["taskInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        string directoryPath = null;

                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string[] keys = fileCollection.Keys[i].Split('$');
                            if (keys[keys.Count() - 1].Equals("fuTaskDoc"))
                            {
                                directoryPath = Server.MapPath("~/TempDocuments/Task/");
                            }

                            DocumentManagement.CreateDirectory(directoryPath);
                            string finalPath = Path.Combine(directoryPath, fileName);
                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                            fileCollection[i].SaveAs(Server.MapPath(finalPath));

                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            if (uploadfile.ContentLength > 0)
                            {
                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = taskScheduledOnID,
                                    ComplianceInstanceID = taskInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize = uploadfile.ContentLength,
                                };

                                if (keys[keys.Count() - 1].Equals("fuTaskDoc"))
                                {
                                    tempComplianceDocument.DocType = "T";
                                }
                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(taskScheduledOnID);
                                }
                            }
                        }
                    }
                    #endregion
                }

                BindTempDocumentData(taskScheduledOnID);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempTaskDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPerformer.IsValid = false;
                cvTaskPerformer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["taskScheduleOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["taskScheduleOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvTaskPerformer.IsValid = false;
                                    cvTaskPerformer.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ;
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                    if (lblDocType.Text.Trim() == "T")
                    {
                        lblDocType.Text = "Task Document";
                    }
                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblDocumentName.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblDocumentName.Visible = true;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                //    if (lblDocType.Text.Trim() == "T")
                //    {
                //        lblDocType.Text = "Task Document";
                //    }
                //    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                //    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");

                //    if (lblIsLinkTrue.Text == "True")
                //    {
                //        lnkDownloadDocument.Visible = false;
                //        lnkViewDocument.Visible = false;
                //    }
                //    else
                //    {
                //        lnkDownloadDocument.Visible = true;
                //        lnkViewDocument.Visible = true;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long taskScheduledOnID = Convert.ToInt64(ViewState["taskScheduleOnID"]);
            long taskInstanceID = Convert.ToInt64(ViewState["taskInstanceID"]);

            string userpath = Txtworkingdocumentlnk.Text;
            string url = Txtworkingdocumentlnk.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(Txtworkingdocumentlnk.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };   
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = taskScheduledOnID,
                    ComplianceInstanceID = taskInstanceID,
                    DocPath = userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "T",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    Txtworkingdocumentlnk.Text = "";
                    BindTempDocumentData(taskScheduledOnID);
                }
            }
            else
            {
                Txtworkingdocumentlnk.Text = "";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
            }
            Txtworkingdocumentlnk.Text = "";
            BindTempDocumentData(taskScheduledOnID);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }
    }
}