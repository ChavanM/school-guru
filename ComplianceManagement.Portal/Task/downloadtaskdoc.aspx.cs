﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using System.Data;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class downloadtaskdoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int taskScheduleOnID=Convert.ToInt32(Request.QueryString["taskScheduleOnID"]);
            List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();
            taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
            if (1 == 1)
            {
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    if (taskDocument.Count > 0)
                    {
                        string fileName = string.Empty;
                        fileName = "TaskDocuments";
                        ComplianceZip.AddDirectoryByName(Convert.ToString(taskScheduleOnID));

                        int i = 0;
                        foreach (var eachFile in taskDocument)
                        {
                            //comment by rahul on 20 JAN 2017
                            string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                            //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                            if (eachFile.FilePath != null && File.Exists(filePath))
                            {
                                string[] filename = eachFile.FileName.Split('.');
                                string str = filename[0] + i + "." + filename[1];
                                if (eachFile.EnType == "M")
                                {
                                    ComplianceZip.AddEntry(Convert.ToString(taskScheduleOnID) + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    ComplianceZip.AddEntry(Convert.ToString(taskScheduleOnID) + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                i++;
                            }
                        }
                    }

                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
        }
    }
}