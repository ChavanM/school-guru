﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="SubTaskDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.SubTaskDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">

        $(function () {
            $('#divTaskDetailsDialog').dialog({
                height: 670,
                width: 800,
                resizable: true,
                autoOpen: false,
                draggable: true,
                //width: "auto",
                position: { my: "center", at: "center", of: window },
                title: "Sub Task Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function openTaskDialog() {
            $("#divTaskDetailsDialog").dialog().dialog('open');
        }

        $(function () {
            $('#divAssignmentDetailsDialog').dialog({
                resizable: true,
                autoOpen: false,
                draggable: true,
                width: "auto",
                position: { my: "top", at: "top", of: window },
                title: "Task Assignment Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }

        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }
        function setDate() {
            $(".StartDate").datepicker();
        }

        $(document).ready(function () {
            initializeCombobox();
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {
            $("#<%= ddlPerformerAssign.ClientID %>").combobox();
            $("#<%= ddlReviewerAssign.ClientID %>").combobox();
        }

        $(function () {
            $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });
        })

        function Confirm() {
            var a = document.getElementById("BodyContent_txtDeactivateDate").value;
            if (a != "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                confirm_value.value = "";
                confirm_value
                if (confirm("Do you really want to deactivate child task?")) {
                    return true;
                } else {
                    return false;
                }
            }
        };
    </script>

    <script language="javascript" type="text/javascript">
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upTaskList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="ComplianceValidationGroupSubTask" />
                <asp:CustomValidator ID="CvSubTask" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroupSubTask" Display="None" />
                <asp:Label runat="server" ID="Label3" ForeColor="Red"></asp:Label>
            </div>
            <table width="100%">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="width: 25%; padding-right: 20px;" align="right"></td>
                    <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddSubTask" OnClick="btnAddSubTask_Click" Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                            RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                    runat="server" Style="text-decoration: none; color: Black" />
                            </ItemTemplate>
                            <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                            <SeparatorStyle Font-Size="12" />
                            <SeparatorTemplate>
                                &gt;
                            </SeparatorTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>

            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdTask" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdTask_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdTask_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdTask_Sorting" ShowHeaderWhenEmpty="true"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdTask_RowCommand" OnPageIndexChanging="grdTask_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" SortExpression="ID" />

                        <asp:TemplateField HeaderText="Title" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="200px" SortExpression="Title">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="250px">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# TaskActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandArgument='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Task" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>'>sub-tasks</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" ToolTip="Edit Task" CommandName="EDIT_SUBTASK" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Task"/></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>' ToolTip="Show Assignment">
                                    <img src="../Images/package_icon.png" alt="Display Schedule Information"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_TASK" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Sub task?')" ToolTip="Delete Task"><img src="../Images/delete_icon.png" alt="Delete Task"/></asp:LinkButton>
                                <asp:LinkButton ID="lnkStatus" runat="server" Visible='<%# ButtonDisplayTaskActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandName="STATUS" CommandArgument='<%# Eval("ID") %>'><img src="../Images/change_status_icon.png" alt="Status change"/></asp:LinkButton>

                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divTaskDetailsDialog">
        <asp:UpdatePanel ID="upTaskDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 15px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Parent Task</label>
                        <asp:Literal ID="litTask" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Task Title</label>
                        <asp:TextBox runat="server" ID="txtTaskTitle" Style="height: 16px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Task title can not be empty."
                            ControlToValidate="txtTaskTitle" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Description can not be empty."
                            ControlToValidate="txtDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Task mapping
                        </label>
                        <asp:TextBox runat="server" ID="txtTask" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; width: 570px; border: 1px solid gray; height: 200px;" id="dvTask">
                            <asp:Repeater ID="rptTask" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="TaskSelectAll" Text="Select All" runat="server" onclick="checkAllTask(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnTaskRepeater" Text="Ok" Style="float: left" OnClick="btnTaskRepeater_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkTask" runat="server" onclick="UncheckTaskHeader();" /></td>
                                        <td style="width: 470px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 460px; padding-bottom: 5px;">
                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblTaskName" runat="server" Text='<%# Eval("Title")%>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Day</label>
                        <asp:TextBox runat="server" ID="txtDueDays" MaxLength="4" Width="80px" type="number" />
                        <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please provide Due Day"
                            ControlToValidate="txtDueDays" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                            ValidationGroup="ComplianceValidationGroup" ErrorMessage="Only numbers allowed in  Due Days."
                            ControlToValidate="txtDueDays" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtDueDays" />
                    </div>

                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Task Conditional
                        </label>
                          <asp:CheckBox ID="ChkIsTaskConditional" runat="server" />
                    </div> 
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Conditional Message</label>
                        <asp:TextBox runat="server" ID="TxtConditionalMessage" Style="height: 16px; width: 390px;" />
                    </div>  
                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            True Condtion</label>
                         <asp:RadioButtonList ID="rbTrueCondtion" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="0" Selected="True" />
                                <asp:ListItem Text="No" Value="1" />
                            </asp:RadioButtonList>
                    </div> 
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Display Both Message</label>
                         <asp:RadioButtonList ID="rbBothMessage" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="0"/>
                                <asp:ListItem Text="No" Value="1" Selected="True"/>
                            </asp:RadioButtonList>
                    </div>   
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Yes Message</label>
                        <asp:TextBox runat="server" ID="txtYesMessage" Style="height: 16px; width: 390px;" />
                    </div> 
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            No Message</label>
                        <asp:TextBox runat="server" ID="txtNoMessage" Style="height: 16px; width: 390px;" />
                    </div> 

                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Sample Form</label>
                                <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                                <asp:FileUpload runat="server" ID="fuSampleFile" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divTaskDetailsDialog').dialog('close');" />
                    </div>

                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnsave" />
            </Triggers>
        </asp:UpdatePanel>


        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
            <p style="color: red;"><strong>Note:</strong> (*) fields Are Compulsory</p>
        </div>
    </div>

    <asp:HiddenField ID="saveopo" runat="server" Value="false" />

    <%--Assingment Start--%>

    <div id="divAssignmentDetailsDialog">
        <asp:UpdatePanel ID="upTaskAssignmentDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskAssignmentDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="TaskAssignmentValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="TaskAssignmentValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="Divbranchlist">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                   Compliance Location
                                </label>
                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" PlaceHolder="Select Applicable Location" autocomplete="off"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" ShowCheckBoxes="All" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                        </asp:TreeView>
                                    </div>
                                </div>
                            </div>

                            <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="DivbranchlistTask">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Reporting Location
                                </label>
                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationTask" PlaceHolder="Select Task Applicable Location" autocomplete="off"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocationTask">
                                        <asp:TreeView runat="server" ID="tvFilterLocationTask" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                            OnSelectedNodeChanged="tvFilterLocationTask_SelectedNodeChanged" Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                        </asp:TreeView>
                                        <asp:CompareValidator ControlToValidate="tbxFilterLocationTask" ErrorMessage="Please select Task Applicable Location."
                                            runat="server" ValueToCompare="Select Task Applicable Location" Operator="NotEqual" ValidationGroup="TaskAssignmentValidationGroup"
                                            Display="None" />
                                    </div>
                                </div>

                                <div style="width: 50%; float: right;">
                                </div>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Start Date</label>
                                <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select start date"
                                    ControlToValidate="tbxStartDate" runat="server" ValidationGroup="TaskAssignmentValidationGroup"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Performer</label>
                                <asp:DropDownList runat="server" ID="ddlPerformerAssign" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Reviewer</label>
                                <asp:DropDownList runat="server" ID="ddlReviewerAssign" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>

                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSaveTaskAssignment" ValidationGroup="TaskAssignmentValidationGroup" OnClick="btnSaveTaskAssignment_Click" CssClass="button" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <asp:Panel ID="Panel2" Width="100%" runat="server" Style="min-height: 200px; max-height: 350px; width: 100%; overflow-y: auto;">
                                    <asp:GridView runat="server" ID="GrdAssigment" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                        CellPadding="4" ForeColor="Black" DataKeyNames="TaskAssignmentID,UserID" OnRowEditing="GrdAssigment_RowEditing" OnRowCancelingEdit="GrdAssigment_RowCancelingEdit"
                                        OnRowUpdating="GrdAssigment_RowUpdating" OnRowDataBound="GrdAssigment_RowDataBound" AllowPaging="True" PageSize="100" Width="100%"
                                        Font-Size="12px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Compliance Branch">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch")%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Reporting Location">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("TaskCustomerBranchName")%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                                                <ItemTemplate>
                                                    <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User" SortExpression="Users" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; width: 300px; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("User") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="overflow: hidden; width: 300px; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:DropDownList ID="ddlUserList" Width="300px" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Role" SortExpression="Role" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_TASKASSIGNMENT" CommandArgument='<%# Eval("TaskAssignmentID") + " , " + Eval("TaskInstanceID") %>'
                                                        OnClientClick="return confirm('Are you certain you want to delete this assignment?');"><img src="../Images/delete_icon.png" alt="Delete Task"/></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Edit" ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%--<asp:BoundField DataField="Branch" HeaderText="Branch" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Width="50px" />
                                            <asp:TemplateField ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_TASK"
                                                        OnClientClick="return confirm('Are you certain you want to delete this assignment?');">
                                                        <img src="../Images/delete_icon.png" alt="Delete Task"/></asp:LinkButton>                                                   
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divComplianceStatusDialog">
        <asp:UpdatePanel ID="upComplianceStatusDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetailsStatus_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        <asp:Label runat="server" ID="Label4" ForeColor="Red"></asp:Label>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Task Title</label>
                        <asp:TextBox runat="server" ID="txtTaskTitleStatus" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Task Description</label>
                        <asp:TextBox runat="server" ID="txtDescriptionStatus" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px" id="div1" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Deactivate Date</label>
                        <asp:TextBox runat="server" CssClass="StartDate" ID="txtDeactivateDate" Style="height: 16px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter deactivate Date."
                            ControlToValidate="txtDeactivateDate" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDeactivateDesc" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Deactivate Description can not be empty."
                            ControlToValidate="txtDeactivateDesc" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSaveDeactivate" OnClick="btnSaveDeactivate_Click" OnClientClick="return Confirm();" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divComplianceStatusDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <%-- <asp:PostBackTrigger ControlID="btnSaveDeactivate" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $('#divComplianceStatusDialog').dialog({
            height: 550,
            width: 800,
            autoOpen: false,
            draggable: true,
            title: "Sub task Details",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    </script>
    <script type="text/javascript">
        function UncheckTaskHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkTask']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkTask']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='TaskSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllTask(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkTask") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
    </script>

    <%--End--%>
</asp:Content>
