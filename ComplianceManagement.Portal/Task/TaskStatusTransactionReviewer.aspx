﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskStatusTransactionReviewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.TaskStatusTransactionReviewer" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {
            initializeDatePicker();
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });

        function initializeDatePicker() {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
            });
        }
        function openInNewTab(url) {
            $("#<%= btnSave.ClientID %>").removeAttr("disabled");
            $("#<%= rdbtnStatus.ClientID %>").removeAttr("disabled");
            $("#<%= tbxRemarks.ClientID %>").removeAttr("disabled");
            $("#<%= tbxDate.ClientID %>").removeAttr("disabled");
            $("#<%= btnReject.ClientID %>").removeAttr("disabled");
            var win = window.open(url, '_blank');
            win.focus();
        }
        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        function enableControls() {
            $("#<%= btnSave.ClientID %>").removeAttr("disabled");
                $("#<%= rdbtnStatus.ClientID %>").removeAttr("disabled");
                $("#<%= tbxRemarks.ClientID %>").removeAttr("disabled");
                $("#<%= tbxDate.ClientID %>").removeAttr("disabled");
                $("#<%= btnReject.ClientID %>").removeAttr("disabled");
            }
    </script>

     <script type="text/javascript">
            function fopendocfileReview(file) {
                $('#modalDocumentReviewerViewer').modal('show'); enableControls();
                $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            function fopendocfile() {
                $('#DocumentPopUp').modal('show');
                $("#<%= docViewerAll.ClientID %>").attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
        }

            $(document).ready(function () {
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#modalDocumentReviewerViewer').modal('hide');
                });

            });
     </script>
    <style>     .topdivlive {
        width: 100%;
        }
        .topdivlivetext {
        width: 69%;
        float: left;
        }
        .topdivliveperformer {
    width: 16%;
}
        .topdivliveimage {
        width: 6%;
        float: right !important;
        }
         .subhead {
    font-size: 14px;
    font-weight: 500;
}
          .doctaks{border: none;height: 25px;background-image: url(../img/icon-download.png)!important;width: 17px!important;display: block !important;}
     </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManagerTaskPerformer" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upTaskReviewer" runat="server" UpdateMode="Conditional" OnLoad="upTaskReviewer_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="vsTaskReviewer" Style="padding-left: 5%" runat="server" 
                                class="alert alert-block alert-danger fade in" ValidationGroup="TaskReviewerValidationGroup" />

                            <asp:CustomValidator ID="cvTaskReviewer" runat="server" class="alert alert-block alert-danger fade in"
                                EnableClientScript="true" ValidationGroup="TaskReviewerValidationGroup" Display="None"  style="padding-left:48px"/>
                            <asp:HiddenField runat="server" ID="hdnTaskInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnTaskScheduledOnID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnID" />
                        </div>
                        <div class="clearfix" style="margin-bottom: 10px"></div>
                        <div id="divTaskDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskDetails">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskDetails">
                                                <h2>Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                         <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 63)
                                    { %>
                                <div id="collapseTaskDetails" class="in">
                                 <%}
                                    else
                                 { %>
                                    <div id="collapseTaskDetails" class="collapse">
                                <%} %>
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Task</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskTitle" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskDesc" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Compliance</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskCompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Reviewer</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskReviewer" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblTaskDueDate" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;display:none;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                 <asp:Label ID="lblPeriod1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Sample Form</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                        <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                        <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divTaskSubTask" runat="server" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Sub Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">                                            
                                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                    <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                        AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                        OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                 <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("TaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblCbranchId" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Task">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis;  width: 250px;">
                                                                        <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Compliance">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Location">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Period">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Performer">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">                                                                      
                                                                        <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" 
                                                                         ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                         Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'   ></asp:Label>
                                                                        
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Reviewer">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                        <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                            Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Due Date">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                            </asp:LinkButton>
                                                                            <asp:Label ID="lblSlashReview1" Text="/" Style="color: blue;" runat="server" />
                                                                            <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                                Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                             <asp:Image ID="chkDocument" ImageUrl="../Images/View-icon-new.png"   data-toggle="tooltip" CssClass="Documentsbtask"  ToolTip="Click to download subtasks documents" runat="server" OnClick="javascript:SelectheaderDOCCheckboxes(this)"   /> 
                                                                            <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                             <asp:Label ID="lblsubtaskDocuments" CssClass="subtaskDocumentlist" runat="server" style="display:none;"></asp:Label>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                            <asp:PostBackTrigger ControlID="btnSubTaskDocView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                             <EmptyDataTemplate >
                                                            No Record Found
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>

                         <div id="divLinkTaskSubTask" runat="server" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion" href="#collapseLinkTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseLinkTaskSubTask">
                                                <h2>Link Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseLinkTaskSubTask" class="collapse in">                                            
                                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                    <asp:GridView runat="server" ID="gridLinkSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                        AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                        OnRowCommand="gridLinkSubTask_RowCommand" OnRowDataBound="gridLinkSubTask_RowDataBound" AutoPostBack="true">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                     <asp:Label ID="lblLinkIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Task">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLinkTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Compliance">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLinkShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Location">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLinkLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Period">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLinkPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Performer">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">                                                                      
                                                                        <asp:Label ID="lblLinkPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" 
                                                                         ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                         Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'   ></asp:Label>
                                                                        
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Reviewer">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                        <asp:Label ID="lblLinkReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                            Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Due Date">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblLinkScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                        <asp:Label ID="lblLinkStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="upLinkSubTaskDownloadView" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="btnLinkSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                            </asp:LinkButton>
                                                                            <asp:Label ID="lblLinkSlashReview1" Text="/" Style="color: blue;" runat="server" />
                                                                            <asp:LinkButton CommandName="View" runat="server" ID="btnLinkSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                                Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                            <asp:Label ID="CompLinkDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnLinkSubTaskDocDownload" />
                                                                            <asp:PostBackTrigger ControlID="btnLinkSubTaskDocView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                             <EmptyDataTemplate >
                                                            No Record Found
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div id="divReviewerTaskDocument" class="row Dashboard-white-widget">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#docDownloadView">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#docDownloadView"></a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>

                                    <div id="docDownloadView" class="panel-collapse collapse in">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;" id="fieldsetdownloadreview" runat="server">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 15%; font-weight: bold;">Versions</td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <table width="100%" style="text-align: left">
                                                                <thead>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:UpdatePanel ID="upTaskDocument" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:Repeater ID="rptComplianceVersion" runat="server" 
                                                                                        OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                                       OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                                        <HeaderTemplate>
                                                                                            <table id="tblComplianceDocumnets">
                                                                                                <thead>
                                                                                                    Task Related Documents
                                                                                                </thead>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                                <ContentTemplate>
                                                                                                    <tr>  
                                                                                                         <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+Eval("Version")%>' ID="lblDocumentVersion"
                                                                                                            runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                        <td>                                                                                                    
                                                                                                        <td>
                                                                                                            <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version")%>' OnClientClick='javascript:enableControls()'
                                                                                                                ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                            </asp:LinkButton>
                                                                                                            <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                                            <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("TaskScheduleOnID")+ ","+ Eval("Version") + ","+ Eval("FileID")%>'
                                                                                                                Text="View" Style="width: 150px; font-size: 13px; color: blue" runat="server" Font-Underline="false" />
                                                                                                            <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                            <asp:Label ID="lblTaskpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                            <asp:LinkButton CommandName="RedirectURL" ID="lblInternalpathDownload" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") %>'
                                                                                                                OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                                Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                runat="server" Font-Underline="false" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            </table>
                                                                                        </FooterTemplate>
                                                                                    </asp:Repeater>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="rptComplianceVersion" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="UpdateTaskStatus" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateTaskStatus">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateTaskStatus">
                                                <h2>Update Task Status</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>


                                        <div id="collapseUpdateTaskStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <label id="lblStatus" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                            <td style="width: 21%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:RadioButtonList ID="rdbtnStatus" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                                <asp:RequiredFieldValidator Style="display: none" ID="rfvStatus" CssClass="alert alert-block alert-danger fade in"
                                                                    runat="server" ValidationGroup="TaskReviewerValidationGroup" ControlToValidate="rdbtnStatus"
                                                                    ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <div style="margin-bottom: 7px" id="divDated1" runat="server">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 83%;">
                                                                    <asp:TextBox runat="server" ID="tbxDate" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                                        runat="server" ID="RequiredFieldValidator3" ValidationGroup="TaskReviewerValidationGroup" Display="None" />
                                                                </td>
                                                            </div>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                                    <asp:Button Text="Submit" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                                        ValidationGroup="TaskReviewerValidationGroup" />                                                    
                                                    <asp:Button Text="Reject" runat="server" ID="btnReject" Style="margin-left: 15px" CssClass="btn btn-primary" OnClick="btnReject_Click"
                                                        ValidationGroup="TaskReviewerValidationGroup"  />
                                                    <asp:Button Text="Close" ID="btnClose" Style="margin-left: 15px; display:none;" CssClass="btn btn-primary" data-dismiss="modal" runat="server" />

                                                    <asp:Button Text="Go To Portal" ID="lnkgotoportal"  Visible="false" OnClick="lnkgotoportal_Click"
                                                        Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divTaskAuditLog" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskAuditLog">

                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskAuditLog">
                                                <h2>Audit Log</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>


                                        <div id="collapseTaskAuditLog" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionLogHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="None" OnPageIndexChanging="grdTransactionLogHistory_OnPageIndexChanging"
                                                            BorderWidth="0px" DataKeyNames="TaskTransactionID">
                                                            <%-- OnRowCommand="grdTransactionHistory_RowCommand"--%>
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                                <div></div>
                                                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                <asp:Button ID="btnDownload" runat="server" Style="display: none" />
                                                <%--OnClick="btnDownload_Click"--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                </Triggers>
            </asp:UpdatePanel>

            <div>
                <div class="modal fade" id="modalDocumentReviewerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptTaskCompliancedocument" runat="server" OnItemCommand="rptTaskCompliancedocument_ItemCommand"
                                                                   OnItemDataBound="rptTaskCompliancedocument_ItemDataBound">                                                                  
                                                                    <HeaderTemplate>
                                                                        <table id="tblTaskComplianceDocumnets">
                                                                            <thead>
                                                                                <th>Versions</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblTaskDocumentView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptTaskCompliancedocument" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>                                                        
                                                    </td>                                                    
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="modal fade" id="ConfirmationDocumentModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <div style="width: 10%; float: right">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                                <p style="font-size: 20px">Document(s)</p>
                            </div>
                        </div>
                        <div class="modal-body">                            
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div  >
                                            <div style="width:100%; margin-left: 3%;">                                                
                                                 <div id="taskDocslower" style="padding: 4px; width:98%; overflow:auto; color: #333; padding-left: 0px;">

                                                </div>                                              
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:PostBackTrigger ControlID="lblNo" />--%>
                                    </Triggers>
                                </asp:UpdatePanel>                           
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="DocumentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="$('#DocumentPopUp').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

             <iframe id="filedownload" style="display:none;" src="about:blank"></iframe>
        <script type="text/javascript">
            function SelectheaderDOCCheckboxes(headerchk) {
                debugger;
                $('#taskDocslower').hide();
                var spanparent = $(headerchk).parent('div');
               // var tr = $(spanparent).parent('.subtaskDocumentlist');
                var subtaskDocumentlistobj = $(spanparent).find('.subtaskDocumentlist');
                
                var count = 0;
                 
                $('#taskDocslower').html($(subtaskDocumentlistobj).html());
                $('#ConfirmationDocumentModel').modal('show');
                $('#taskDocslower').show();
                  
            }
            var nomessage = '';
            var yesmessage = '';

            function SelectheaderCheckboxes(headerchk) {
                $('#taskslower').hide();
                $('#lblYes').show();
                $('#lblNo').show();
                var spanparent = $(headerchk).parent('.sbtask');
                var tr = $(spanparent).parent('div');
                var subtasklistobj = $(tr).find('.subtasklist');
                
                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
           
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                         
                        $('#taskslower').html($(subtasklistobj).html());
                        if ($(spanparent).attr('data-msg') == '') {
                            $('#lblYes').removeAttr("href");
                        } else {
                            $('#idProceed').html($(spanparent).attr('data-msg'));
                            if ($(spanparent).attr('data-yes') == "True" || $(spanparent).attr('data-yes') == "1") {
                                $('#lblNo').removeAttr("href");
                               // $('#lblYes').attr('href', "javascript:__doPostBack('lblNo', '')");
                                yesmessage = $(spanparent).attr('data-yesmessage');
                                 
                            }
                            if ($(spanparent).attr('data-no') == "True" || $(spanparent).attr('data-no') == "1") {
                                $('#lblYes').removeAttr("href");
                                //$('#lblNo').attr('href', "javascript:__doPostBack('lblNo', '')");
                            
                                nomessage = $(spanparent).attr('data-nomessage');
                            }
                        }
                        $('#ConfirmationModel').modal('show');
                       
                    }
                }
            }

            function callOnButtonYes() {
              
                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                    //    inputList[i].checked = false;
                    }
                }
                if ($('#lblYes').attr("href") == null || $('#lblYes').attr("href") == undefined || $('#lblYes').attr("href") =='') {
                    $('#taskslower').show();
                    $('#lblYes').hide();
                    $('#lblNo').hide();
                    $('#ConfirmationModel').modal('show');
                    return false;
                } 
                else {
                    
                    var r = confirm(yesmessage);
                
                    if (r == true) {
                        $('#ConfirmationModel').modal('hide');
                        return true;
                    } else {
                        $('#ConfirmationModel').modal('show');
                        return false;
                    } 
                }
                $('#ConfirmationModel').modal('hide');
                initializeDatePicker();
            }
            function callOnButtonNo() {
                var count = 0;
                var gvcheck = document.getElementById("<%=gridSubTask.ClientID %>");
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var gv = document.getElementById("<%= gridSubTask.ClientID %>");
                var inputList = gv.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        count = count + 1;
                    }
                }
               
                initializeDatePicker();
                if ($('#lblNo').attr("href") == null || $('#lblNo').attr("href") == undefined || $('#lblNo').attr("href") =='') 
                {
                    $('#taskslower').show();
                    $('#lblYes').hide();
                    $('#lblNo').hide();
                    $('#ConfirmationModel').modal('show');
                    return false;
                }
               
                else
                {
                  
                    var r = confirm(nomessage);
                    
                    if (r == true) {
                        $('#ConfirmationModel').modal('hide');
                        return true;
                    } else {
                        $('#ConfirmationModel').modal('show');
                       
                        return false;
                    }
                }
                $('#ConfirmationModel').modal('hide');
            }

            function downloadTaskSummary(obj)
            {
              var formonth=$(obj).attr('data-formonth');
              var instanceId = $(obj).attr('data-inid');
              var scheduleOnID = $(obj).attr('data-scid');
              $('#filedownload').attr("src","/task/downloadtaskdoc.aspx?taskScheduleOnID=" + scheduleOnID + "&r=" + Math.random());
              
            }
            $(document).ready(function () {
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#modalDocumentPerformerViewer').modal('hide');
                });
            });
            for (var k = 0; k < $('.sbtask').length; k++) {
                var obj = $('.sbtask')[k];
                if ($(obj).attr('data-attr') == "True" || $(obj).attr('data-attr') == "1")
                {
                    $(obj).css("display", "block");
                }
            }
           function openTaskSummary(obj) {
              
               var taskInstanceID = $(obj).attr('data-inid');
               var taskScheduleOnID = $(obj).attr('data-scid');
                ShowDialog(taskInstanceID, taskScheduleOnID)
            } 

            function ShowDialog(taskInstanceID, taskScheduleOnID) {
                $('#DocumentPopUp').modal('show');
                $('.modal-dialog').css('width', '95%');
                $('#docViewerAll').attr('width', '100%');
                $('#docViewerAll').attr('height', '550px');
                $('#docViewerAll').attr('src', "/Task/TaskStatusTransactionReviewer.aspx?TID=" + taskInstanceID + "&TSOID=" + taskScheduleOnID);
            };
        </script>
        </div>
    </form>
</body>
</html>
