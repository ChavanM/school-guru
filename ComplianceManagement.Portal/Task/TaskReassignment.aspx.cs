﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskReassignment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlNewUsers.BorderColor = Color.Empty;

            if (!IsPostBack)
            {
                BindLocationFilter();

                btnSaveAssignment.Visible = false;

                txtAssigmnetFilter.Text = string.Empty;
                List<int> a = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID).ToList();

                if (AuthenticationHelper.CustomerID == 63)
                {
                    ddlDocType.SelectedValue = "1";
                    ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("0"));
                }

                if (a.Contains(4))
                {
                    divModifyAssignment.Visible = true;
                    User user = UserManagement.GetByID(AuthenticationHelper.UserID);

                    BindNewUserList(user.CustomerID ?? -1, ddlNewUsers);

                    BindTaskData();
                    Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                }
                else
                {
                    divModifyAssignment.Visible = false;
                }

                GetPageDisplaySummary();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
        }

        private void BindTaskData()
        {
            try
            {
                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);
                string filter = txtAssigmnetFilter.Text;
                List<SP_TaskInstanceTransactionStatutoryView_Result> datasource = new List<SP_TaskInstanceTransactionStatutoryView_Result>();
                List<SP_TaskInstanceTransactionInternalView_Result> datasource1 = new List<SP_TaskInstanceTransactionInternalView_Result>();

                //statutory
                if (ddlDocType.SelectedValue == "0")
                {
                    grdTaskReassign.DataSource = null;
                    grdTaskReassign.DataBind();
                    datasource = TaskManagment.GetAllAssignedPerformerListOfStatutoty(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, location, filter).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();//,location,filter

                    grdTaskReassign.DataSource = datasource;
                    grdTaskReassign.DataBind();

                    Session["TotalRows"] = datasource.Count;
                }
                //Internal
                if (ddlDocType.SelectedValue == "1")
                {
                    grdTaskReassign.DataSource = null;
                    grdTaskReassign.DataBind();

                    datasource1 = TaskManagment.GetAllAssignedPerformerListOfInternal(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID,location, filter).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList(); //, location, filter

                    grdTaskReassign.DataSource = datasource1;
                    grdTaskReassign.DataBind();

                    Session["TotalRows"] = datasource1.Count;
                }
                upModifyAssignment.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                if (AuthenticationHelper.CustomerID == 63)
                {
                    ddlDocType.SelectedValue = "1";
                }

                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";
                if (ddlDocType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlDocType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                var AllUsers = UserManagement.GetAllUserCustomerID(customerID, null);

                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddllist.DataSource = AllUsers;
                ddllist.DataBind();

                ddllist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ddlDocType.SelectedValue == "0")
                {
                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                }
                else if (ddlDocType.SelectedValue == "1")
                {
                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindTaskData();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {

            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SelectedPageNo.Text = "1";

            if (ddlDocType.SelectedValue == "0")
            {
                ViewState["AssignedTaskID"] = null;
                grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTaskReassign.PageIndex = 0;
            }
            else if (ddlDocType.SelectedValue == "1")
            {
                ViewState["AssignedTaskID"] = null;
                grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTaskReassign.PageIndex = 0;
            }

            //Reload the Grid
            BindTaskData();

            GetPageDisplaySummary();

            ShowSelectedRecords(sender, e);
        }

        protected void ddlNewUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewUsers.SelectedValue != "-1")
                {
                    ddlNewUsers.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void grdTaskReassign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox headerchk = (CheckBox) grdTaskReassign.HeaderRow.FindControl("chkTaskHeader");
                    CheckBox childchk = (CheckBox) e.Row.FindControl("chkTask");
                    childchk.CheckedChanged += new System.EventHandler(this.Childchk_CheckedChanged);

                    //childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdComplianceDocument')");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void Childchk_CheckedChanged(object sender, EventArgs e)
        {
            ShowSelectedRecords(sender, e);
        }
        protected void grdTaskReassign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdTaskReassign.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(AuthenticationHelper.UserID)))
                {
                    User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    BindTaskData();
                }
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskReassign_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        

        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_value"].Split(',');
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                var AdminUser = UserManagement.GetCompanyAdminDataByCustID(customerID);
                string AdminName = string.Empty;
                string AdminEmail = string.Empty;
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                string UserName = user.FirstName + " " + user.LastName;
                foreach (var item in AdminUser)
                {
                    AdminName = item.FirstName + " " + item.LastName;
                    AdminEmail = item.Email;
                }

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    if (ddlNewUsers.SelectedValue != "-1")
                    {
                        if (ddlDocType.SelectedValue == "0") //Statutory
                        {
                            #region Statutory

                            CheckBoxValueSaved();

                            List<Tuple<int, string, int, string>> chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedTaskID"];

                            if (chkList != null)
                            {
                                TaskManagment.ReplaceUserForTaskAssignmentNew(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlNewUsers.SelectedValue), chkList);

                                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlNewUsers.SelectedValue));
                                string portalURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalURL = Urloutput.URL;
                                }
                                else
                                {
                                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                foreach (var cil in chkList)
                                {
                                    var oldUser = UserManagement.GetByID(Convert.ToInt32(cil.Item3));
                                    //Admin approver Mail
                                    string ReplyEmailAddressName1 = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                    string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Task_ReassignPerformer
                                                      .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                                      .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                                      .Replace("@ComDescription", cil.Item4)
                                                      .Replace("@User", AdminName)
                                                      .Replace("@URL", Convert.ToString(portalURL))
                                                      .Replace("@ChangedBy", UserName)
                                                      .Replace("@From", ReplyEmailAddressName1);
                                    //string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Task_ReassignPerformer
                                    //                    .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                    //                    .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                    //                    .Replace("@ComDescription", cil.Item4)
                                    //                    .Replace("@User", AdminName)
                                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                    .Replace("@ChangedBy", UserName)
                                    //                    .Replace("@From", ReplyEmailAddressName1);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { AdminEmail }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Task.", message1); }).Start();
                                    //End Mail

                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskNewOwner
                                                          .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                                          .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                                          .Replace("@URL", Convert.ToString(portalURL))
                                                          .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskNewOwner
                                        //                    .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Task.", message); }).Start();
                                    }


                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesOldOwner
                                                         .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                         .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                                         .Replace("@URL", Convert.ToString(portalURL))
                                                         .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesOldOwner
                                        //                    .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Task.", message); }).Start();
                                    }
                                }
                                //BindUsers();
                                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                ViewState["AssignedTaskID"] = null;
                            }
                            else
                            {
                                CustomModifyAsignment.IsValid = false;
                                CustomModifyAsignment.ErrorMessage = "Please select at list one task for proceed.";
                            }

                            #endregion
                        }
                        else if (ddlDocType.SelectedValue == "1") //Internal
                        {
                            #region internal

                            CheckBoxValueSaved();
                            List<Tuple<int, string, int, string>> chkIList = (List<Tuple<int, string, int, string>>) ViewState["AssignedTaskID"];

                            if (chkIList != null)
                            {
                                TaskManagment.ReplaceUserForTaskAssignmentNew(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlNewUsers.SelectedValue), chkIList);
                                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlNewUsers.SelectedValue));
                                string portalURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalURL = Urloutput.URL;
                                }
                                else
                                {
                                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                foreach (var cil in chkIList)
                                {
                                    var oldUser = UserManagement.GetByID(Convert.ToInt32(cil.Item3));

                                    string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Task_ReassignPerformer
                                                    .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                                    .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                                    .Replace("@ComDescription", " ")
                                                    .Replace("@User", AdminName)
                                                    .Replace("@URL", Convert.ToString(portalURL))
                                                    .Replace("@From", UserName);

                                    //string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Task_ReassignPerformer
                                    //                    .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                    //                    .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                    //                    .Replace("@ComDescription", " ")
                                    //                    .Replace("@User", AdminName)
                                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                    .Replace("@From", UserName);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { AdminEmail }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Task.", message1); }).Start();



                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignInternalTaskNewOwner
                                                           .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                                           .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                                           .Replace("@URL", Convert.ToString(portalURL))
                                                           .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignInternalTaskNewOwner
                                        //                    .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Task.", message); }).Start();
                                    }


                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskOldOwner
                                                           .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                           .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                                           .Replace("@URL", Convert.ToString(portalURL))
                                                           .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskOldOwner
                                        //                    .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Task.", message); }).Start();
                                    }
                                }
                                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                ViewState["AssignedTaskID"] = null;
                            }
                            else
                            {
                                CustomModifyAsignment.IsValid = false;
                                CustomModifyAsignment.ErrorMessage = "Please select at list one task for proceed.";
                            }

                            #endregion
                        }

                        // User users = UserManagement.GetByID(AuthenticationHelper.UserID);

                        //if (ddlDocType.SelectedValue == "-1")
                        //{
                            BindTaskData();
                        //}
                        //else if (ddlDocType.SelectedValue == "0")
                        //{
                        //    BindTaskData();
                        //}
                    }
                    else
                    {
                        ddlNewUsers.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                        cvNewUsers.IsValid = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "0")
                {
                    CheckBoxValueSaved();

                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    //Reload the Grid
                    BindTaskData();

                    AssignCheckBoxexValue();
                }
                else if (ddlDocType.SelectedValue == "1")
                {
                    CheckBoxValueSaved();

                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    //Reload the Grid
                    BindTaskData();

                    AssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "0")
                {
                    CheckBoxValueSaved();

                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    BindTaskData();

                    AssignCheckBoxexValue();
                }
                else if (ddlDocType.SelectedValue == "1")
                {
                    CheckBoxValueSaved();

                    grdTaskReassign.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdTaskReassign.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    BindTaskData();

                    AssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void chkTaskHeader_CheckedChanged(object sender, EventArgs e)
        {
            if (ddlDocType.SelectedValue == "0")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdTaskReassign.HeaderRow.FindControl("chkTaskHeader");

                foreach (GridViewRow row in grdTaskReassign.Rows)
                {
                    CheckBox chkTask = (CheckBox) row.FindControl("chkTask");

                    if (ChkBoxHeader.Checked)
                        chkTask.Checked = true;
                    else
                        chkTask.Checked = false;
                }
            }

            else if (ddlDocType.SelectedValue == "1")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdTaskReassign.HeaderRow.FindControl("chkTaskHeader");

                foreach (GridViewRow row in grdTaskReassign.Rows)
                {
                    CheckBox chkICompliances = (CheckBox) row.FindControl("chkTask");

                    if (ChkBoxHeader.Checked)
                        chkICompliances.Checked = true;
                    else
                        chkICompliances.Checked = false;
                }
            }

            ShowSelectedRecords(sender, e);
        }

        protected void chkTask_CheckedChanged(object sender, EventArgs e)
        {
            int Count = 0;

            if (ddlDocType.SelectedValue == "-1")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdTaskReassign.HeaderRow.FindControl("chkTaskHeader");

                foreach (GridViewRow row in grdTaskReassign.Rows)
                {
                    CheckBox chkTask = (CheckBox) row.FindControl("chkTask");

                    if (chkTask.Checked)
                        Count++;
                }

                if (Count == grdTaskReassign.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;
            }

            else if (ddlDocType.SelectedValue == "0")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdTaskReassign.HeaderRow.FindControl("chkTaskHeader");

                foreach (GridViewRow row in grdTaskReassign.Rows)
                {
                    CheckBox chkTask = (CheckBox) row.FindControl("chkTask");

                    if (chkTask.Checked)
                        Count++;
                }

                if (Count == grdTaskReassign.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;
            }

            ShowSelectedRecords(sender, e);
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            if (ddlDocType.SelectedValue == "0")
            {
                CheckBoxValueSaved();
                AssignCheckBoxexValue();
            }
            else if (ddlDocType.SelectedValue == "1")
            {
                CheckBoxValueSaved();
                AssignCheckBoxexValue();
            }
        }

        private void CheckBoxValueSaved()
        {
            List<Tuple<int, string, int, string>> chkList = new List<Tuple<int, string, int, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdTaskReassign.Rows)
            {
                index = Convert.ToInt32(grdTaskReassign.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox) gvrow.FindControl("chkTask")).Checked;
                string role = ((Label) gvrow.FindControl("lblRole")).Text;
                string olduserid = ((Label) gvrow.FindControl("lblUserID")).Text;
                var description = ((Label) gvrow.FindControl("lblDescription")).Text;
                //Tuple<int, string>  = new Tuple<int, string>();
                if (ViewState["AssignedTaskID"] != null)
                    chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedTaskID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid)) && entry.Item4.Equals(description)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, string, int, string>(index, role, Convert.ToInt32(olduserid), description));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedTaskID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<Tuple<int, string, int, string>> chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedTaskID"];

            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdTaskReassign.Rows)
                {
                    int index = Convert.ToInt32(grdTaskReassign.DataKeys[gvrow.RowIndex].Value);
                    string role = ((Label) gvrow.FindControl("lblRole")).Text;
                    string olduserid = ((Label) gvrow.FindControl("lblUserID")).Text;

                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.ToString().Trim().Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid))).FirstOrDefault();

                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkTask");
                        myCheckBox.Checked = true;
                    }
                }

                if (chkList.Count > 0)
                {
                    lblTotalSelected.Text = chkList.Count + " Selected";
                    btnSaveAssignment.Visible = true;
                }
                else if (chkList.Count == 0)
                {
                    lblTotalSelected.Text = "";
                    btnSaveAssignment.Visible = false;
                }
            }

            else
            {
                lblTotalSelected.Text = "";
                btnSaveAssignment.Visible = false;
            }
        }


    }
}