﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class CaptchaImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
                Response.Clear();
                int height = 30;
                int width = 100;
                Bitmap bmp = new Bitmap(width, height);
                RectangleF rectf = new RectangleF(10, 5, 0, 0);
                StringFormat strFormat = new StringFormat();
                Graphics g = Graphics.FromImage(bmp);
                g.Clear(Color.White);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["captcha"])))
                {

                   // g.DrawString(Session["captcha"].ToString(), new Font("Arial", 14, FontStyle.Strikeout), Brushes.Green, rectf);
                    g.DrawString(Session["captcha"].ToString(), new Font("Arial", 14, FontStyle.Strikeout), Brushes.CornflowerBlue, rectf);
                    g.DrawRectangle(new Pen(Color.LightBlue), 1, 1, width - 2, height - 2);
                    g.Flush();
                    Response.ContentType = "image/jpeg";
                    bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
                    g.Dispose();
                    bmp.Dispose();
                }
                else
                {
                    SetCapctha();
                    g.DrawString(Session["captcha"].ToString(), new Font("Arial", 14, FontStyle.Strikeout), Brushes.CornflowerBlue, rectf);
                    //g.DrawString(Session["captcha"].ToString(), new Font("Arial", 14, FontStyle.Strikeout), Brushes.Green, rectf);
                    g.DrawRectangle(new Pen(Color.LightBlue), 1, 1, width - 2, height - 2);
                    g.Flush();
                    Response.ContentType = "image/jpeg";
                    bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
                    g.Dispose();
                    bmp.Dispose();
                }
               
            
        }

        void SetCapctha()
        {

            try
            {
                //txtcode.Text = string.Empty;
                Random random = new Random();
                //string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                string combination = "123456789";
                StringBuilder captcha = new StringBuilder();
                for (int i = 0; i < 6; i++)
                    captcha.Append(combination[random.Next(combination.Length)]);
                Session["captcha"] = null;
                Session["captcha"] = captcha.ToString();
                //imgCaptcha.ImageUrl = "~/CaptchaImage.aspx?" + DateTime.Now.Ticks.ToString();

            }
            catch
            {
                throw;
            }

        }
    }
}