﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="~/InternalCompliance/ChangeInternalComplianceScheduleOn.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.ChangeInternalComplianceScheduleOn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlCompliance.ClientID %>").combobox();
            $("#<%= ddlPeriod.ClientID %>").combobox();

        }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <center>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                        ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                </div>
                <table runat="server" width="70%">
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Customer Name</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCustomer" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                              <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Customer"
                                    ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Compliance
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCompliance" AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                               <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Compliance"
                                    ControlToValidate="ddlCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Period
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlPeriod" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                              <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Period"
                                    ControlToValidate="ddlPeriod" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>

                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                             <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                           <asp:TextBox runat="server" CssClass="StartDate" ID="txtSchedueleDate" Style="height: 16px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter Scheduele Date."
                            ControlToValidate="txtSchedueleDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>

                  
                    <tr>
                        <td colspan="4" align="center" runat="server">
                            <br />
                            <br />
                            <asp:Panel ID="Panel1" Width="100%" Height="400px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50"
                                    Font-Size="12px">
                                   <%-- OnRowEditing="grdCompliances_RowEditing" OnRowCancelingEdit ="grdCompliances_RowCancelingEdit"
                                        OnRowUpdating ="grdCompliances_RowUpdating"--%>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Branch Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduleOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>

                                                </div>
                                            </ItemTemplate>
                                           <%-- <EditItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:UpdatePanel ID="UpStartDate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtSchedueleOn" CssClass="StartDate" runat="server"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="txtSchedueleOn" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                </div>
                                            </EditItemTemplate>--%>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Edit" ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                      <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                         <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
