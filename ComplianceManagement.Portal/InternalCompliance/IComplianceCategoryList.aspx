﻿<%@ Page Title="Internal Compliance Category" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="IComplianceCategoryList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.IComplianceCategoryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upIComplianceCategoryList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                       <td class="td1">
                               <div id="lblcustomer" runat="server">
                                <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 1px;margin-left:15px">
                               Select Customer:
                            </label>
                                   </div>
                        </td>
                        <td class="td2">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: -36px; height: 2px; width: 280px;height:24px;width:385px;"
                             OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddIComplianceCategory" OnClick="btnAddIComplianceCategory_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdIComplianceCategory" AutoGenerateColumns="false"
                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdIComplianceCategory_RowCreated"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdIComplianceCategory_Sorting"
                Width="100%" Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdIComplianceCategory_RowCommand"
                OnPageIndexChanging="grdIComplianceCategory_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" ItemStyle-Height="20px" SortExpression="Name" />
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_COMPLIANCE_CATEGORY"
                                CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance Category" title="Edit Compliance Category" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_COMPLIANCE_CATEGORY"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this compliance category?');"><img src="../Images/delete_icon.png" alt="Delete Compliance Category" title="Delete Compliance Category" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divIComplianceCategoryDialog">
        <asp:UpdatePanel ID="upIComplianceCategory" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                    </div>
                      <div style="margin-bottom: 7px" id="Div1" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;margin-top:-2px">
                                    Customer:</label>
                                <asp:DropDownList ID="ddlcustomernew" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 258px;margin-left: 0px; margin-top: -4px;"
                                CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlcustomernew"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="oplValidationGroup"
                                    Display="None" />
                            </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 250px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="ComplianceCategoryValidationGroup"
                            ErrorMessage="Please enter a valid category name." ControlToValidate="tbxName"
                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="tbxDescription" Style="height: 40px; width: 250px;" TextMode="MultiLine" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 57px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divIComplianceCategoryDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divIComplianceCategoryDialog').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Internal Compliance Category",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
