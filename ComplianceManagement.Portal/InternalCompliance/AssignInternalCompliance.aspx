﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="AssignInternalCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.AssignInternalCompliance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="float: left; margin-top: 10px; margin-left: 50px; margin-top: 5px;">
        <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div id="FilterLocationdiv" runat="server" style="float: left; margin-left: 100px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>
                <div runat="server" id="divFilterUsers" style="margin-left: 20px; margin-top: 5px; float: left;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>

                <div style="float: right; margin-top: 10px; margin-right: 15px">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddComplianceType" OnClick="btnAddComplianceType_Click" />
                </div>
                <br />
                <br />
                <br />

                <asp:GridView runat="server" ID="grdComplianceType" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdComplianceType_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="14" Width="100%" OnSorting="grdComplianceType_Sorting"
                    Font-Size="12px" DataKeyNames="InternalComplianceInstanceID" OnPageIndexChanging="grdComplianceType_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="User" HeaderText="User" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="User" />
                        <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-HorizontalAlign="Center" SortExpression="Role" />
                        <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="StartDate">
                            <ItemTemplate>
                                <%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy"):""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAssignComplianceDialog">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList ID="ddlUsers" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select User." ControlToValidate="ddlUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" Height="200px" Width="394px"
                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                                <asp:CompareValidator ID="CompareValidator2" ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                         
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Compliance Category</label>
                                <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceCatagory_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Compliance Type</label>
                                <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                                </asp:DropDownList>
                               <%-- <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Compliance Type." ControlToValidate="ddlComplianceType"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />--%>
                            </div>
                            <div style="margin-bottom: 7px; clear: both">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Start Date</label>
                                <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 16px; width: 390px;" AutoPostBack="true" OnTextChanged="tbxStartDate_TextChanged" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Approver</label>
                                <asp:CheckBox runat="server" ID="cbApprover" onclick="canChange();" />
                                <asp:Button runat="server" Style="display: none" ID="btnRefresh" OnClick="btnRefresh_Click" />
                            </div>

                            <div style="margin-bottom: 7px" runat="server" id="Filtercompliance" visible="false">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Filter
                                </label>
                                <asp:TextBox runat="server" ID="tbxFilter" Style="height: 16px; width: 390px;" MaxLength="50" AutoPostBack="true"
                                    OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                    <Columns>

                                        <asp:TemplateField HeaderText="SrNo">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <%#Container.DataItemIndex + 1 %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Internal Compliance" SortExpression="IShortDescription">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("IShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartDate" CssClass="StartDate" runat="server" ReadOnly="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkPerformerheader" Text="Performer" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkPerformer" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkReviewer1header" Text="Reviewer" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkReviewer1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>


                            </div>


                        </ContentTemplate>

                    </asp:UpdatePanel>

                    <%-- <asp:Label ID="lblNote" runat="server" Text="*Please hover on the section's name to see compliance description." style="font-family:Verdana;font-size:10px;"></asp:Label>--%>
                    <div style="margin-bottom: 7px; float: right; margin-right: 454px; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignComplianceDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 0px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divAssignComplianceDialog').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Assign Compliance",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
            $("#<%= ddlUsers.ClientID %>").combobox();
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();

        }


        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function canChange() {
            var postback = true;

            if ($('#<%= cbApprover.ClientID %>').is(":checked")) {
                var gvcheck = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
                if (gvcheck.rows.length > 1) {
                    var postback = confirm("Are you sure you want to set the user as 'Approver' for all compliances listed below?");

                } else {

                    alert("Please select at list one compliance to assign approvar.");
                    postback = false;
                }
                if (!postback) {
                    $('#<%= cbApprover.ClientID %>').attr("checked", false);
                }
            }

            if (postback) {
                $('#<%= btnRefresh.ClientID %>').click();
        }
    }


    function SelectheaderCheckboxes(headerchk) {
        var rolecolumn;

        var chkheaderid = headerchk.id.split("_");


        if (chkheaderid[2] == "chkPerformerheader") {
            rolecolumn = 3;
        }
        else if (chkheaderid[2] == "chkReviewer1header") {
            rolecolumn = 4;
        }
        //            else if (chkheaderid[2] == "chkReviewer2header") {
        //                rolecolumn = 5;
        //            }

        var gvcheck = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
        var i;

        if (headerchk.checked) {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }

        else {
            for (i = 0; i < gvcheck.rows.length; i++) {
                if (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].disabled == false)
                    gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }
    }

    function Selectchildcheckboxes(header) {
        var i;
        var count = 0;
        var rolecolumn;
        var gvcheck = document.getElementById("<%=grdComplianceRoleMatrix.ClientID %>");
        var headerchk = document.getElementById(header);
        var chkheaderid = header.split("_");

        if (chkheaderid[2] == "chkPerformerheader") {
            rolecolumn = 3;
        }
        else if (chkheaderid[2] == "chkReviewer1header") {
            rolecolumn = 4;
        }

        var rowcount = gvcheck.rows.length;

        for (i = 1; i < gvcheck.rows.length - 1; i++) {
            if (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked) {
                count++;
            }
        }

        if (count == gvcheck.rows.length - 2) {
            headerchk.checked = true;
        }
        else {
            headerchk.checked = false;
        }
    }


    function initializeConfirmDatePicker(date) {
        var startDate = new Date();
        $('#<%= tbxStartDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            numberOfMonths: 1
        });

    }

    function txtactListClicked() {
        $('#<%= btnRefresh.ClientID %>').click();
    }

    function checkAll(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }

    function UncheckHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {

            rowCheckBoxHeader[0].checked = false;
        }
    }

    function preventBack() { window.history.forward(); }
    setTimeout("preventBack()", 10);
    window.onunload = function () { null };
    </script>
</asp:Content>
