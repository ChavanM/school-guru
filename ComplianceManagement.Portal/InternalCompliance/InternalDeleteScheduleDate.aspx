﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="InternalDeleteScheduleDate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.InternalDeleteScheduleDate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <style type="text/css">
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 300px;
        }
    </style>
    <script type="text/javascript">

        function Confirm() {
            var a = document.getElementById("BodyContent_tbxStartDate").value;
            if (a != "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var Date = document.getElementById("<%=tbxStartDate.ClientID %>").value;
                confirm_value.value = "";
                confirm_value
                if (confirm("Start date is " + Date + ", to continue  click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Start date should not be blank..!");
            }
        };
        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
          
            $("#<%= ddlInternalCompliance.ClientID %>").combobox();
            $("#<%= ddlCustomer.ClientID %>").combobox();

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <div id="divActDialog" style="height: auto;">
        <asp:UpdatePanel ID="upAct" runat="server" UpdateMode="Conditional" OnLoad="upAct_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                </div>
                <table runat="server" width="30%" align="center">
                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Customer 
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCustomer" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Customer"
                                ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                     
                    </tr>

                    <tr>
                        <td class="auto-style5">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Location</label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" AutoPostBack="true" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 342px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="240px"
                                    Style="overflow: auto" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:CompareValidator ID="CompareValidator3" ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                        </td>
                    </tr>
                   

                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Compliance
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlInternalCompliance" OnSelectedIndexChanged="ddlInternalCompliance_SelectedIndexChanged" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Compliance"
                                ControlToValidate="ddlInternalCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                      
                    </tr>

                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Start Date
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">


                            <asp:TextBox runat="server" ID="tbxStartDate" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                            <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                                ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />


                        </td>
                     
                    </tr>

                    <tr>
                        <td colspan="4" align="center" runat="server">

                            <asp:Panel ID="Panel1" Width="100%" Height="300px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdInternalCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="1000"
                                    Font-Size="12px">

                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Branch Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        

                                        <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduleOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>

                                                </div>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                   


                </table>
                <table>
                    <tr>
                         <td class="td1" align="left" style="margin-bottom: 4px">
                            <asp:Button ID="btndelete" runat="server" OnClick="btndelete_Click" OnClientClick="return confirm('Are you sure!! You want to Delete schedules ?');" Style="width: 80px; height: 30px; margin-left: 520px; margin-bottom: 4px" Text="Delete" ValidationGroup="ComplianceValidationGroup" />

                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:Button ID="Button1" runat="server" OnClick="btnKeep_Click" OnClientClick="return confirm('Are you sure!! You want to Keep schedules ?');" Style="width: 80px; height: 30px; margin-left: 60px; margin-bottom: 4px" Text="Keep" ValidationGroup="ComplianceValidationGroup" />
                        </td>
                    </tr>
                </table>


            </ContentTemplate>

            <Triggers>
                <asp:PostBackTrigger ControlID="btndelete" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>


    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function initializeDatePicker(todayDate) {

            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;

            $(".StartDate").datepicker({
                //showOn: 'button',
                // buttonImageOnly: true,
                //buttonImage: '../Images/Cal.png',
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
                ,
                beforeShow: function (input, inst) {

                    var date = $("#<%= tbxStartDate.ClientID %>").val();

                    if (date.toString().trim() != '') {

                        //startDate = date;
                        setTimeout(function () {
                            inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
                        }, 100);
                    }


                    // $(".StartDate").datepicker('setDate', startDate);
                    $(".StartDate").datepicker('setDate', date);

                },
                onClose: function (dateText, inst) {

                    if (dateText != null) {
                        $("#<%= tbxStartDate.ClientID %>").val(dateText);
                }
                }
            });

        $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
            $(this).attr('title', 'Select Start Date');
        });
    }
    </script>
</asp:Content>
