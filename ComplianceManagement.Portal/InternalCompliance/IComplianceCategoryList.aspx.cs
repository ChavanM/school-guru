﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class IComplianceCategoryList : System.Web.UI.Page
    {
        public static int UserID;
        public static int CustomerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlcustomernew.Enabled = true;
            if (!IsPostBack)
            {
                UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomers(UserID);
                BindCustomerNew(UserID);

                if (AuthenticationHelper.Role == "IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    customerdiv.Visible = true;
                    lblcustomer.Visible = true;
                    Div1.Visible = true;
                }
                else
                {
                    Div1.Visible = false;
                    lblcustomer.Visible = false;
                    customerdiv.Visible = false;
                    ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                }
               // ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                BindComplianceCategories();
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceCategories();
        }
        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(UserID);
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerNew(int UserID)
        {
            try
            {
                ddlcustomernew.DataTextField = "Name";
                ddlcustomernew.DataValueField = "ID";

                ddlcustomernew.DataSource = Assigncustomer.GetAllCustomer(UserID);
                ddlcustomernew.DataBind();
                ddlcustomernew.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceCategories()
        {
            try
            {
                if(AuthenticationHelper.Role=="IMPT")
                {
                    int custoerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    grdIComplianceCategory.DataSource = ComplianceCategoryManagement.GetAll(custoerID, tbxFilter.Text);
                    grdIComplianceCategory.DataBind();
                    upIComplianceCategoryList.Update();
                }
                if (ViewState["CustomerID"] != null)
                {
                    grdIComplianceCategory.DataSource = ComplianceCategoryManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]),tbxFilter.Text);
                    grdIComplianceCategory.DataBind();
                    upIComplianceCategoryList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int categoryID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_COMPLIANCE_CATEGORY"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceCategoryID"] = categoryID;

                    InternalCompliancesCategory internalcategory = ComplianceCategoryManagement.GetInternalComplianceByID(categoryID);
                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        ddlcustomernew.SelectedValue = Convert.ToString(internalcategory.CustomerID);
                        ddlcustomernew.Enabled = false;
                    }
                    else
                    {
                        ViewState["CustomerID"] = internalcategory.CustomerID;
                    }
                    tbxName.Text = internalcategory.Name;
                    tbxDescription.Text = internalcategory.Description;
                    tbxDescription.ToolTip = internalcategory.Description;

                    upIComplianceCategory.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceCategoryDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE_CATEGORY"))
                {
                    //if (ComplianceTypeManagement.InternalComplianceCategoryUsed(categoryID))
                    //{
                    bool isExist = Business.ComplianceTypeManagement.InternalComplianceCategoryUsed(categoryID);
                    if (isExist == false)
                    {
                        ComplianceCategoryManagement.DeleteInternalCategory(categoryID);
                        BindComplianceCategories();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Compliance category is associated with one or more compliances, can not be deleted.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdIComplianceCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdIComplianceCategory.PageIndex = e.NewPageIndex;
                BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnAddIComplianceCategory_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    CustomerID = Convert.ToInt32(ddlcustomernew.SelectedValue);
                }
                else
                {
                    ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                }
                ddlcustomernew.SelectedValue = "-1";
                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;

                upIComplianceCategory.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceCategoryDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdIComplianceCategory.PageIndex = 0;
                BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlcustomernew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (customerID == -1)
                {
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                InternalCompliancesCategory category = new InternalCompliancesCategory()
                {
                    Name = tbxName.Text,
                    Description = tbxDescription.Text,
                    CustomerID = customerID,
                    IsDeleted =false
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    category.ID = Convert.ToInt32(ViewState["ComplianceCategoryID"]);
                }
                
                if (ComplianceCategoryManagement.Exists(category,customerID))
                {
                    cvDuplicateEntry.ErrorMessage = "Category already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    ComplianceCategoryManagement.Create(category);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ComplianceCategoryManagement.Update(category);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divIComplianceCategoryDialog\").dialog('close')", true);
                BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceCategory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlcustomernew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                var complianceCategoryList = ComplianceCategoryManagement.GetAll(customerID,tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    complianceCategoryList = complianceCategoryList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceCategoryList = complianceCategoryList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdIComplianceCategory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdIComplianceCategory.Columns.IndexOf(field);
                    }
                }

                grdIComplianceCategory.DataSource = complianceCategoryList;
                grdIComplianceCategory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdIComplianceCategory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
    }
}