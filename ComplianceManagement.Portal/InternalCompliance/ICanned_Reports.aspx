﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ICanned_Reports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.ICanned_Reports" %>

<%@ Register Src="~/Controls/InternalCanned_ReportPerformer.ascx" TagName="InternalCanned_ReportPerformer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalCanned_ReportReviewer.ascx" TagName="InternalCanned_ReportReviewer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalCanned_ReportApprover.ascx" TagName="InternalCanned_ReportApprover"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <div style="margin: 10px 20px 10px 10px">
    <div>
        Role :
        <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow"
            OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Performer" Selected="True" />
            <asp:ListItem Text="Reviewer" />
            <asp:ListItem Text="Approver" />
        </asp:RadioButtonList>
       </div>
       <div style="width:100px;float:left;margin-left: 335px;margin-top: -30px;">   
         <asp:LinkButton runat="server"  ID="lbtnExportExcel" style="margin-top:15px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
          title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
      </div>
     </div>
   
    <vit:InternalCanned_ReportPerformer runat="server" ID="udcInternalCanned_ReportPerformer" />
    <vit:InternalCanned_ReportReviewer runat="server" ID="udcInternalCanned_ReportReviewer" Visible="false" />
    <vit:InternalCanned_ReportApprover runat="server" ID="udcInternalCanned_ReportApprover" Visible="false" />


     <script type="text/javascript">
         $(function () {
             initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type = "text/javascript" >
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
</script>
</asp:Content>
