﻿<%@ Page Title="Internal Compliances" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="InternalComplianceList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.InternalComplianceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function fopendocfileReview() {
            if ($('#BodyContent_hdnFile').val() != '') {
                window.open('../docviewerSample.aspx?docurl=' + $('#BodyContent_hdnFile').val() + "&Internalsatutory=I", '_blank', '', '');
            }
        }
        $(function () {
            $(document).tooltip();
        });
        $(document).ready(function () {
            if (document.getElementById('BodyContent_saveopo').value == "true") {

                $('#divInternalComplianceDetailsDialog').dialog({
                    height: 670,
                    width: 800,
                    autoOpen: false,
                    draggable: true,
                    title: "Compliance Details",
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    }
                });
                newfun();
            }
        });
        function newfun() {

            $("#divInternalComplianceDetailsDialog").dialog('open');

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <%--  <div style="float: right; margin-top: 10px; margin-right: 70px; margin-top: 5px;">
      
    </div>--%>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <div id="customerdiv" runat="server">
                          <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: 0px;height:22px;width:218px;"
                         OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"   CssClass="txtbox" AutoPostBack="true"/>
                        </div>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;margin-left:-111px"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;margin-left:-27px"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;margin-left:-50px"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RadioButton ID="rdFunctionBased" Text="Function Based" AutoPostBack="true" Width="110px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdFunctionBased_CheckedChanged" />
                    </td>
                    <td>
                        <asp:RadioButton ID="rdChecklist" Text="Checklist" AutoPostBack="true" Width="100px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdChecklist_CheckedChanged" />
                    </td>
                          <td>
                        <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
                    </td>
                    </td>
                      <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" Visible="false" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" />
                    </td>
                </tr>
               <tr>
                      <td style="width: 25%; padding-right: 86px;" align="right"  >
                        <asp:TextBox runat="server" ID="tbxFilter" Width="210px" MaxLength="50" AutoPostBack="true" margin-right="-180px" style="float:left;margin-right: -1106px;"  
                            OnTextChanged="tbxFilter_TextChanged" placeholder="Type to filter" />
                      </td>
                </tr>
             <%--   <tr>
                     <td class="td2">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: 0px;height:22px;width:218px;"
                         OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"   CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                    </tr>--%>
            </table>
            <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnSorting="grdCompliances_Sorting"
                Font-Size="12px" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                <%--  DataKeyNames="ID"--%>
                <Columns>
                    <asp:TemplateField HeaderText="ID" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="60px" SortExpression="ID">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                <asp:Label ID="LabelID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="60px" SortExpression="IComplianceTypeName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("IComplianceTypeName") %>' ToolTip='<%# Eval("IComplianceTypeName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="60px" SortExpression="IComplianceCategoryName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("IComplianceCategoryName") %>' ToolTip='<%# Eval("IComplianceCategoryName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="IShortDescription">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="140px" SortExpression="IUploadDocument">
                        <ItemTemplate>
                            <%# Convert.ToBoolean(Eval("IUploadDocument")) ? "Yes" : "No"%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Irisk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="Irisk" />
                    <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="180px" SortExpression="Frequency" />
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance" title="Edit Compliance" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance" title="Delete Compliance" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ID") %>'
                                Visible='<%# ViewSchedule(Eval("Frequency"), Eval("IComplianceType"),Eval("ISubComplianceType"),Eval("CheckListTypeID")) %>'><img src="../Images/package_icon.png" alt="Display Schedule Information" title="Display Schedule Information" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
          <Triggers>
                <asp:PostBackTrigger ControlID="ddlCustomer" />
            </Triggers>
    </asp:UpdatePanel>
    <div id="divInternalComplianceDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                <%--      <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                           Customer: </label>
                        <%--<asp:DropDownList runat="server" ID="ddlcustomernew" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                          OnSelectedIndexChanged="ddlcustomernew_SelectedIndexChanged" CssClass="txtbox" />--%>
                         <%--     <asp:DropDownList runat="server" ID="ddlcustomernew" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                         OnSelectedIndexChanged="ddlcustomernew_SelectedIndexChanged"   CssClass="txtbox" AutoPostBack="true"/>
                       
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Customer."
                            ControlToValidate="ddlcustomernew" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>--%>
                    <div runat="server" id="divAct" style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act Name."
                            ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Type Name</label>
                        <asp:DropDownList runat="server" ID="ddlIComplinceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="cvIComplinceType" ErrorMessage="Please select internal compliance type name."
                            ControlToValidate="ddlIComplinceType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Category Name</label>
                        <asp:DropDownList runat="server" ID="ddlICategory" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="cvIComplinceCategory" ErrorMessage="Please select internal compliance category name."
                            ControlToValidate="ddlICategory" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="rfvIShortDescription" ErrorMessage="Please enter short description."
                            ControlToValidate="txtShortDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                      <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;margin-left: 10px">
                            Short Form</label>
                        <asp:TextBox runat="server" ID="txtShortForm" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                              <%--      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please enter Short Form."
                            ControlToValidate="txtShortForm" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="txtdetaileddescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter Detail description."
                            ControlToValidate="txtdetaileddescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Based</label>
                        <asp:CheckBox ID="chkEvent" runat="server" AutoPostBack="true" OnCheckedChanged="chkEvent_CheckedChanged" CssClass="txtbox" />
                    </div>
                    <div style="margin-bottom: 7px" id="divOccourrence" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Occourrence</label>
                        <asp:RadioButtonList ID="rbtnlstCompOcc" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbtnlstCompOcc_SelectedIndexChanged">
                            <asp:ListItem Text="One Time" Value="0" />
                            <asp:ListItem Text="Recurring" Value="1" Selected="True" />
                        </asp:RadioButtonList>
                    </div>

                    <div style="margin-bottom: 7px" id="divCompType" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;margin-left:-43px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                            <asp:ListItem Text="Function based" Value="0" />
                            <asp:ListItem Text="Checklist" Value="1" />
                            <asp:ListItem Text="Time Based" Value="2" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" id="divScheduleType" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Schedule Type</label>
                        <asp:DropDownList runat="server" ID="ddlScheduleType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Selected="True" Text="After" Value="0" />
                            <asp:ListItem Text="In Between" Value="1" />
                            <asp:ListItem Text="Before" Value="2" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" id="divChecklist" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Checklist Type</label>
                        <asp:DropDownList runat="server" ID="ddlChklstType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlChklstType_SelectedIndexChanged">
                            <asp:ListItem Text="One Time based Checklist" Value="0" />
                            <asp:ListItem Text="Function based Checklist" Value="1" />
                            <asp:ListItem Text="Time Based Checklist" Value="2" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; clear: both" id="divOnetimeDate" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            One Time Date</label>
                        <asp:TextBox runat="server" ID="tbxOnetimeduedate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="rfvOnetimeduedate" ErrorMessage="Please enter One time Due Date."
                            ControlToValidate="tbxOnetimeduedate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divTimebasedTypes" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Timely Based Type</label>
                        <asp:RadioButtonList ID="rbTimebasedTypes" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbTimebasedTypes_SelectedIndexChanged">
                            <asp:ListItem Text="Fixed Gap" Value="0" Selected="True" />
                            <asp:ListItem Text="Periodically Based" Value="1" />
                        </asp:RadioButtonList>
                    </div>

                    <div style="margin-bottom: 7px;" id="divComplianceDueDays" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Due(Day)</label>
                        <asp:TextBox runat="server" ID="txtEventDueDate" MaxLength="4" Width="80px" />
                        <asp:RegularExpressionValidator ID="rgexEventDueDate" ControlToValidate="txtEventDueDate"
                            runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                        <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter compliance Due(Day)."
                            ControlToValidate="txtEventDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                        <%--Enabled="false"--%>
                    </div>


                    <div runat="server" id="divFunctionBased">
                        <div id="divNonEvents" runat="server">
                            <div style="margin-bottom: 7px" id="divFrequency" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Frequency</label>
                                <asp:DropDownList runat="server" ID="ddlFrequency" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please select frequency." ControlToValidate="ddlFrequency" ID="cvfrequency"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Due Date</label>
                                <asp:DropDownList runat="server" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vaDueDate" ErrorMessage="Please Select Due Date."
                                    ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" Enabled="false" />
                            </div>
                            <div style="margin-bottom: 7px" id="vivWeekDueDays" runat="server" visible="false">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Week Due Day</label>
                                <asp:DropDownList runat="server" ID="ddlWeekDueDay" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please Select Week Due Day." ControlToValidate="ddlWeekDueDay" ID="vaDueWeekDay"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                        </div>

                    </div>
                    <div style="margin-bottom: 7px" id="divReminderType">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Reminder Type</label>
                        <asp:RadioButtonList ID="rbReminderType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbReminderType_SelectedIndexChanged">
                            <asp:ListItem Text="Standard" Value="0" Selected="True" />
                            <asp:ListItem Text="Custom" Value="1" />
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px;" id="divForCustome" runat="server" visible="false">
                        <div style="float: left; width: 30%; overflow: hidden; margin-left: 200px;">
                            <label>
                                Before(In Days)
                            </label>
                            <asp:TextBox runat="server" ID="txtReminderBefore" MaxLength="4" Width="80px" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtReminderBefore"
                                runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                            <asp:RequiredFieldValidator ID="rfvforcustomeReminder" ErrorMessage="Please enter reminder before value."
                                ControlToValidate="txtReminderBefore" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" Enabled="false" />
                        </div>
                        <div style="overflow: hidden;">
                            <label>
                                Gap(In Days)
                            </label>
                            <asp:TextBox runat="server" ID="txtReminderGap" MaxLength="4" Width="80px" />
                        </div>

                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Priority Type</label>
                        <asp:DropDownList runat="server" ID="ddlRiskType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="High" Value="0" />
                            <asp:ListItem Text="Low" Value="2" />
                            <asp:ListItem Text="Medium" Value="1" />
                            <asp:ListItem Text="Critical" Value="3" />

                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Priority type."
                            ControlToValidate="ddlRiskType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 19px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Compliance Document Mandatory?</label>
                        <asp:CheckBox ID="chkComplianceDocumentMandatory" runat="server" CssClass="txtbox" />
                    </div>
                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Upload Document</label>
                                <asp:CheckBox runat="server" ID="chkDocument" CssClass="txtbox" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Required Forms</label>
                                <asp:TextBox runat="server" ID="tbxRequiredForms" Style="height: 16px; width: 390px;" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Sample Form</label>
                                <%-- <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />--%>
                                <asp:FileUpload AllowMultiple="true" runat="server" ID="fuSampleFile" />
                                <asp:HiddenField runat="server" ID="hdnFile" />
                            </div>
                            <div style="margin-bottom: 7px" id="divSampleForm" runat="server">
                                <asp:Panel ID="Panel2" Width="100%" Height="100px" ScrollBars="Vertical" runat="server">
                                    <asp:GridView runat="server" ID="grdSampleForm" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%"
                                        Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdSampleForm_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                                            <asp:TemplateField HeaderText="Form Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lbkDownload" runat="server" CommandName="DownloadSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("IComplianceID") %>'><img src="../Images/downloaddoc.png" alt="Download"/></asp:LinkButton>
                                                            <asp:LinkButton ID="lbkDelete" runat="server" CommandName="DeleleSampleForm" CommandArgument='<%# Eval("ID") + "," + Eval("IComplianceID") %>'
                                                                OnClientClick="return confirm('Are you sure you want to delete this Sampleform?');"><img src="../Images/delete_icon.png" alt="Delete Form"/></asp:LinkButton>
                                                            <asp:LinkButton ID="lbkView" runat="server" OnClientClick="fopendocfileReview()" CommandArgument='<%# Eval("ID") + "," + Eval("IComplianceID") %>'><img src="../Images/package_icon.png" alt="View Form"/></asp:LinkButton>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbkDownload" />
                                                            <asp:AsyncPostBackTrigger ControlID="lbkDelete" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div style="margin-bottom: 7px; margin-left: 210px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divInternalComplianceDetailsDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 0px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divStartMonth" style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Year</label>
                        <asp:DropDownList runat="server" ID="ddlStartMonth" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Calender Year"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Financial Year"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="Div1" runat="server" style="margin-bottom: 7px; padding-left: 50px">
                                <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th style="width: 150px; border: 1px solid gray">For Period
                                                </th>
                                                <th style="width: 200px; border: 1px solid gray">Day
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <tr>
                                                <td align="center" style="border: 1px solid gray">
                                                    <%# Eval("ForMonthName")%>
                                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                                </td>
                                                <td align="center" style="border: 1px solid gray">
                                                    <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" />
                                                    <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                                </td>
                                            </tr>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 142px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click"
                            CssClass="button" />
                        <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    <script type="text/javascript">
        $(function () {
            $('#divInternalComplianceDetailsDialog').dialog({
                height: 670,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
            $("#<%= ddlIComplinceType.ClientID %>").combobox();
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlFrequency.ClientID %>").combobox();
            $("#<%= ddlRiskType.ClientID %>").combobox();
            $("#<%= ddlDueDate.ClientID %>").combobox();
            $("#<%= ddlICategory.ClientID %>").combobox();
            $("#<%= ddlChklstType.ClientID %>").combobox();
            $("#<%= ddlWeekDueDay.ClientID %>").combobox();
            $("#<%= ddlAct.ClientID %>").combobox();
         //    $("#<%= ddlCustomer.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }


        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= tbxOnetimeduedate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "minDate", startDate);
                }
            });
                if (date != null) {
                    $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
