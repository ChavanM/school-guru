﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class ChangeInternalComplianceScheduleOn : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomers(userID);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
        }
        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = InternalComplianceManagement.GetAllInternalComplainceAssignedCustomers(UserID);
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCompliances(long CustomerID)
        {
            try
            {
                ddlCompliance.DataTextField = "Name";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = InternalComplianceManagement.GetAllAssignedInternalCompliances(CustomerID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSchedulePeriods(long ComplianceID)
        {
            try
            {
                ddlPeriod.DataTextField = "Name";
                ddlPeriod.DataValueField = "ID";

                ddlPeriod.DataSource = InternalComplianceManagement.GetAllInternalComplianceAssignedPeriods(ComplianceID);
                ddlPeriod.DataBind();

                ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlCustomer.SelectedValue) != -1)
            {
                BindCompliances(Convert.ToInt32(ddlCustomer.SelectedValue));
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlCompliance.SelectedValue) != -1)
            {
                BindSchedulePeriods(Convert.ToInt32(ddlCompliance.SelectedValue));
            }

        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Convert.ToInt32(ddlCustomer.SelectedValue) != -1 && Convert.ToInt32(ddlCompliance.SelectedValue) != -1 && Convert.ToString(ddlPeriod.SelectedItem.Text) != "< Select >")
                {

                    long Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                    string Period = Convert.ToString(ddlPeriod.SelectedItem.Text);

                    var ScheduleList = (from row1 in entities.InternalCompliances
                                        join row2 in entities.InternalComplianceInstances
                                        on row1.ID equals row2.InternalComplianceID
                                        join row3 in entities.InternalComplianceAssignments
                                        on row2.ID equals row3.InternalComplianceInstanceID
                                        join row4 in entities.InternalComplianceScheduledOns
                                        on row3.InternalComplianceInstanceID equals row4.InternalComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false && row6.ID == Customerid
                                        && row1.ID == complianced
                                        && row4.ForMonth.Equals(Period)
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                        select new
                                        {
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            row1.IShortDescription,
                                            row4.ForMonth,
                                            row4.ScheduledOn
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();
                }
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                    string Period = Convert.ToString(ddlPeriod.SelectedItem.Text);
                    long CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                    var complianceInstances = (from row in entities.InternalComplianceInstances
                                               where row.IsDeleted == false && row.InternalComplianceID == complianced
                                               select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                    //ComplianceInstances
                    complianceInstances.ForEach(complianceInstancesentry =>
                    {
                        var complianceScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                                     where row.InternalComplianceInstanceID == complianceInstancesentry.ID 
                                                     && row.ForMonth == Period && row.IsActive==true 
                                                     && row.IsUpcomingNotDeleted==true
                                                     select row).ToList();
                         //ComplianceScheduleOns
                        complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                        {

                            var complianceTransactions = (from row in entities.InternalComplianceTransactions
                                                          where row.InternalComplianceInstanceID == complianceScheduleOnssentry.InternalComplianceInstanceID
                                                                  && row.InternalComplianceScheduledOnID == complianceScheduleOnssentry.ID
                                                          select row).ToList();
                            if (complianceTransactions.Count == 1)
                            {
                                DateTime NewscheduleOnDate = DateTime.ParseExact(txtSchedueleDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                DateTime OldscheduleOnDate = complianceScheduleOnssentry.ScheduledOn;

                                log_ChangeScheduleOn Changescheduleon = new log_ChangeScheduleOn()
                                {
                                    ComplianceID = complianced,
                                    Period = Period,
                                    OldDueDate = OldscheduleOnDate,
                                    NewDueDate = NewscheduleOnDate,
                                    ComplianceInstanceID = complianceScheduleOnssentry.InternalComplianceInstanceID,
                                    CustomerID = CustomerID,
                                    BranchID = Convert.ToInt32(complianceInstancesentry.CustomerBranchID),
                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                    craetedOn = DateTime.Now,
                                    ScheduleOnID = complianceScheduleOnssentry.ID,
                                    flag = "I",
                                };
                                CreateComplianceScheduleOn(Changescheduleon);
                                Business.InternalComplianceManagement.UpdateComplianceScheduleOn(complianceInstancesentry.InternalComplianceID, complianceInstancesentry.ID, complianceScheduleOnssentry.ID, OldscheduleOnDate, NewscheduleOnDate, Convert.ToInt32(complianceScheduleOnssentry.ForPeriod));
                            }
                        });
                    });

                    //ddlCustomer.SelectedValue = "-1";
                    //ddlCompliance.SelectedValue = "-1";
                    ddlPeriod.SelectedValue = "-1";
                    cvDuplicateEntry.IsValid = false;
                    grdCompliances.DataSource = null;
                    txtSchedueleDate.Text = "";
                    grdCompliances.DataBind();
                    cvDuplicateEntry.ErrorMessage = "Schedule Changed successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateComplianceScheduleOn(log_ChangeScheduleOn MSCO)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.log_ChangeScheduleOn.Add(MSCO);
                entities.SaveChanges();
            }
        }


    }
}