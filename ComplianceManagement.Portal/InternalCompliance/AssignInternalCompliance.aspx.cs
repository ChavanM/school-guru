﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class AssignInternalCompliance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;
                btnAddComplianceType.Visible = true;
                BindLocationFilter();
                //BindComplianceInstances();
                BindComplianceCategories();
                BindTypes();
                BindUsers(ddlUsers);
                BindUsers(ddlFilterUsers);
                BindLocation();
                tbxBranch.Attributes.Add("readonly", "readonly");
                AddFilter();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindComplianceMatrix("N");
            if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
            {
                setDateToGridView();
            }
        }

        private void BindTypes()
        {
            try
            {
                int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);

                ddlComplianceType.DataTextField = "Name";
                ddlComplianceType.DataValueField = "ID";

                ddlComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(CustomerID);
                ddlComplianceType.DataBind();

                ddlComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceInstances(int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }

                var dataSource = Business.InternalComplianceManagement.GetAllAssignedInstances(AuthenticationHelper.CustomerID, branchID, userID);

                grdComplianceType.PageIndex = pageIndex;
                grdComplianceType.DataSource = dataSource;
                grdComplianceType.DataBind();

                if (isBranchChanged)
                {
                    if (branchID != -1)
                    {
                        BindUsers(ddlFilterUsers, dataSource.Select(entry => entry.UserID).Distinct().ToList());
                    }
                    else
                    {
                        BindUsers(ddlFilterUsers);
                    }

                    ddlFilterUsers.SelectedValue = "-1";
                }

                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
                TreeviewTextHighlightrd(Convert.ToInt32(tvBranches.SelectedNode.Value));
                grdComplianceRoleMatrix.PageIndex = 0;
                BindComplianceMatrix("N");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            grdComplianceRoleMatrix.PageIndex = 0;
            //BindComplianceMatrix("N"); //comment by rahul on 22 FEB 2016
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N");
            }
            else
            {
                BindComplianceMatrix("Y");
            }
            Filtercompliance.Visible = true;

        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            grdComplianceRoleMatrix.PageIndex = 0;
           // BindComplianceMatrix("N"); //comment by rahul on 22 FEB 2016

            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N");
            }
            else
            {
                BindComplianceMatrix("Y");
            }

            Filtercompliance.Visible = true;

        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            //BindComplianceMatrix("Y");
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N");
            }
            else
            {
                BindComplianceMatrix("Y");
            }
            Filtercompliance.Visible = true;
        }

        private void BindComplianceMatrix(string flag)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int branchID = -1;
                if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                {
                    branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                }
                
                if (complianceTypeID != -1 || complianceCatagoryID != -1)
                {
                    if (flag == "Y")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.InternalComplianceManagement.GetByType(customerID,complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID, tbxFilter.Text.Trim());
                    }
                    else
                    {
                        grdComplianceRoleMatrix.DataSource = Business.InternalComplianceManagement.GetByType(customerID,complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID);
                    }
                    grdComplianceRoleMatrix.DataBind();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if(AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                  }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceInstances(isBranchChanged: true);
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceInstances();
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                User user = UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue));
                int? branch = null;
                if (user.CustomerBranchID != null)
                {
                    branch = user.CustomerBranchID;

                    var customerBranch = CustomerBranchManagement.GetByID(Convert.ToInt64(user.CustomerBranchID));
                    tbxBranch.Text = customerBranch.Name;
                }
                else
                {
                    tbxBranch.Text = "< Select >";
                }
                TreeviewTextHighlightrd(branch);
            }
            catch (Exception)
            {

            }

        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceList = new List<ComplianceAsignmentProperties>();
                SaveCheckedValues();
                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                List<Tuple<InternalComplianceInstance, InternalComplianceAssignment>> assignments = new List<Tuple<InternalComplianceInstance, InternalComplianceAssignment>>();
                ComplianceAsignmentProperties rmdata = new ComplianceAsignmentProperties();
                long userID = Convert.ToInt64(ddlUsers.SelectedValue);
                //int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                int branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                Dictionary<int, DateTime> startDates = new Dictionary<int, DateTime>();
                grdComplianceRoleMatrix.AllowPaging = false;
                BindComplianceMatrix("N");
                grdComplianceRoleMatrix.DataBind();

                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                    int complianceID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[row.RowIndex]["ID"]);
                    DateTime scheduledOn;
                    if (complianceList != null)
                    {
                        rmdata = complianceList.Where(t => t.ComplianceId == complianceID).FirstOrDefault();
                    }
                    else
                    {
                        rmdata = null;
                    }

                    if (rmdata != null)
                    {
                        scheduledOn = DateTime.ParseExact(rmdata.StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(tbxStartDate.Text))
                        {
                            scheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                            scheduledOn = DateTime.ParseExact(txt.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }

                    startDates.Add(complianceID, scheduledOn);
                }

                grdComplianceRoleMatrix.AllowPaging = true;
                BindComplianceMatrix("N");
                grdComplianceRoleMatrix.DataBind();

                if (cbApprover.Checked)
                {
                    startDates.ToList().ForEach(entry =>
                    {
                        InternalComplianceInstance instance = new InternalComplianceInstance();
                        instance.InternalComplianceID = entry.Key;
                        instance.CustomerBranchID = branchID;
                        instance.StartDate = entry.Value;

                        InternalComplianceAssignment assignment = new InternalComplianceAssignment();
                        assignment.UserID = userID;
                        assignment.RoleID = RoleManagement.GetByCode("APPR").ID;

                        if (!(entry.Value.Date.Equals(DateTime.MinValue)))
                        {
                            assignments.Add(new Tuple<InternalComplianceInstance, InternalComplianceAssignment>(instance, assignment));
                        }
                    });
                }
                else
                {
                    if (complianceList != null)
                    {
                        for (int i = 0; i < complianceList.Count; i++)
                        {

                            InternalComplianceInstance instance = new InternalComplianceInstance();
                            instance.InternalComplianceID = complianceList[i].ComplianceId;
                            instance.CustomerBranchID = branchID;
                            instance.StartDate = DateTime.ParseExact(complianceList[i].StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            if (complianceList[i].Performer)
                            {

                                InternalComplianceAssignment assignment = new InternalComplianceAssignment();
                                assignment.UserID = userID;
                                assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                                assignments.Add(new Tuple<InternalComplianceInstance, InternalComplianceAssignment>(instance, assignment));
                            }
                            if (complianceList[i].Reviewer1)
                            {

                                InternalComplianceAssignment assignment1 = new InternalComplianceAssignment();
                                assignment1.UserID = userID;
                                assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                assignments.Add(new Tuple<InternalComplianceInstance, InternalComplianceAssignment>(instance, assignment1));
                            }

                            //if (complianceList[i].Reviewer2)
                            //{
                            //    InternalComplianceAssignment assignment2 = new InternalComplianceAssignment();
                            //    assignment2.UserID = userID;
                            //    assignment2.RoleID = RoleManagement.GetByCode("RVW2").ID;
                            //    assignments.Add(new Tuple<InternalComplianceInstance, InternalComplianceAssignment>(instance, assignment2));
                            //}

                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                        setDateToGridView();
                    }


                }
                if (assignments.Count != 0)
                {
                    Business.InternalComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignComplianceDialog\").dialog('close');", true);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                    setDateToGridView();
                }

                BindComplianceInstances();
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }
                tbxFilter.Text = "";
                Filtercompliance.Visible = false;
                ViewState["CHECKED_ITEMS"] = null;
                ddlUsers.SelectedValue = "-1";
                ddlComplianceType.SelectedValue = "-1";
                ddlComplianceCatagory.SelectedValue = "-1";
                tbxStartDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.DataBind();
                cbApprover.Checked = false; ;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignComplianceDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select >";
                ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindComplianceInstances(pageIndex: e.NewPageIndex);
        }

        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
                ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
                if (ViewState["pagefilter"] != null)
                {
                    if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                    {
                        divFilterUsers.Visible = false;
                        FilterLocationdiv.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                    {
                        divFilterUsers.Visible = true;
                        FilterLocationdiv.Visible = false;
                    }
                    else
                    {
                        divFilterUsers.Visible = false;
                        FilterLocationdiv.Visible = false;
                        if (AuthenticationHelper.Role == "EXCT")
                        {
                            btnAddComplianceType.Visible = false;
                        }

                        BindComplianceInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }

                var assignmentList = Business.InternalComplianceManagement.GetAllAssignedInstances(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceType.Columns.IndexOf(field);
                    }
                }

                grdComplianceType.DataSource = assignmentList;
                grdComplianceType.DataBind();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();
                BindComplianceMatrix("N");
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
                {
                    setDateToGridView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SaveCheckedValues()
        {
            try
            {

                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkPerformer")).Checked;
                    complianceProperties.Reviewer1 = ((CheckBox)gvrow.FindControl("chkReviewer1")).Checked;
                    //complianceProperties.Reviewer2 = ((CheckBox)gvrow.FindControl("chkReviewer2")).Checked;
                    complianceProperties.StartDate = Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString();
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                    if (complianceProperties.Performer || complianceProperties.Reviewer1)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkPerformer");
                            CheckBox chkReviewer1 = (CheckBox)gvrow.FindControl("chkReviewer1");
                            //CheckBox chkReviewer2 = (CheckBox)gvrow.FindControl("chkReviewer2");
                            TextBox txtStartDate = (TextBox)gvrow.FindControl("txtStartDate");
                            chkPerformer.Checked = rmdata.Performer;
                            chkReviewer1.Checked = rmdata.Reviewer1;
                            //chkReviewer2.Checked = rmdata.Reviewer2;
                            txtStartDate.Text = rmdata.StartDate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    if (cbApprover.Checked)
                    {
                        CheckBox chkPerformerheader = (CheckBox)e.Row.FindControl("chkPerformerheader");
                        CheckBox chkReviewer1header = (CheckBox)e.Row.FindControl("chkReviewer1header");
                        chkPerformerheader.Enabled = false;
                        chkReviewer1header.Enabled = false;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int branchID = -1;
                    if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                    {
                        branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                    }
                    long InternalComplianceId = Convert.ToInt64(grdComplianceRoleMatrix.DataKeys[e.Row.RowIndex].Values[0].ToString());
                    List<InternalComplianceAssignment> AssignedCompliances = ComplianceManagement.Business.InternalComplianceManagement.GetAssignedCompliances(InternalComplianceId, branchID);

                    DateTime startDate = DateTime.Now;
                    TextBox txtStartDate = (TextBox)e.Row.FindControl("txtStartDate");
                    txtStartDate.Text = startDate.ToString("dd-MM-yyyy");
                    CheckBox chkPerformer = (CheckBox)e.Row.FindControl("chkPerformer");
                    CheckBox chkReviewer1 = (CheckBox)e.Row.FindControl("chkReviewer1");
                   
                    if (cbApprover.Checked)
                    {
                        if (chkPerformer.Checked)
                            chkPerformer.Checked = false;
                        if (chkReviewer1.Checked)
                            chkReviewer1.Checked = false;

                        chkPerformer.Enabled = false;
                        chkReviewer1.Enabled = false;
                     
                    }

                    CheckBox headerchk = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkPerformerheader");
                    CheckBox childchk = (CheckBox)e.Row.FindControl("chkPerformer");
                    childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "')");

                    CheckBox chkReviewer1header = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkReviewer1header");
                    CheckBox chkRev1 = (CheckBox)e.Row.FindControl("chkReviewer1");
                    chkRev1.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + chkReviewer1header.ClientID + "')");

                    if (AssignedCompliances != null)
                    {
                        InternalComplianceAssignment assignmentAsPerformer = AssignedCompliances.Where(entry => entry.RoleID == 3).FirstOrDefault();
                        InternalComplianceAssignment assignmentAsReviewer1 = AssignedCompliances.Where(entry => entry.RoleID == 4).FirstOrDefault();
                        InternalComplianceAssignment assignmentAsReviewer2 = AssignedCompliances.Where(entry => entry.RoleID == 5).FirstOrDefault();
                        if (assignmentAsPerformer != null)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(assignmentAsPerformer.UserID));
                            chkPerformer.Checked = true;
                            chkPerformer.Enabled = false;
                            chkPerformer.ToolTip = "This compliance is already assigned to " + user.FirstName + " " + user.LastName + ".";
                        }


                        if (assignmentAsReviewer1 != null)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(assignmentAsReviewer1.UserID));
                            chkReviewer1.Checked = true;
                            chkReviewer1.Enabled = false;
                            chkReviewer1.ToolTip = "This compliance is already assigned to " + user.FirstName + " " + user.LastName + ".";
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int branchID = -1;
                if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                {
                    branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                }
                var ComplianceRoleMatrixList = Business.InternalComplianceManagement.GetByType(customerID, complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID);
                
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                    }
                }


                SaveCheckedValues();
                grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        private void BindComplianceCategories()
        {
            try
            {
                int CustomerID =Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID); 

                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll(CustomerID);
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxStartDate_TextChanged(object sender, EventArgs e)
        {
            setDateToGridView();
        }

        private void setDateToGridView()
        {
            try
            {
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                    txt.Text = tbxStartDate.Text;
                }

                DateTime date = DateTime.Now;
                if (!string.IsNullOrEmpty(tbxStartDate.Text.Trim()))
                {
                    date = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AssignedInternalCompliances");
                        DataTable ExcelData = null;
                        BindComplianceInstances();
                        DataView view = new System.Data.DataView((grdComplianceType.DataSource as List<InternalComplianceAssignedInstancesView>).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "Branch", "ShortDescription", "User", "Role", "StartDate");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = "Assigned Internal Compliances Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;


                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].Value = "Location";

                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;                       
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].Value = "Short Description";

                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;

                        //exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["D4"].Value = "Act";
                        //exWorkSheet.Cells["D4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;


                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;

                        
                        //exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["F4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E4"].Value = "Start Date";

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 6 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=InternalAssignedInstancesReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                    }
                    catch (Exception ex)
                    {
                    }
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void TreeviewTextHighlightrd(int? branch)
        {


            foreach (TreeNode node in tvBranches.Nodes)
            {
                var list = GetChildren(node);
                List<TreeNode> tn = list.Where(nd => nd.Text.Contains("<div")).ToList();
                if (tn != null)
                {
                    foreach (TreeNode t in tn)
                    {
                        t.Text = Regex.Replace(t.Text, "<.*?>", string.Empty);
                    }

                }

                TreeNode td = null;
                if (branch != null)
                    td = list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault();

                if (td != null)
                    list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault().Text = "<div style=\"font-weight:bold\">" + td.Text + "</div>";
            }

        }
    }
}