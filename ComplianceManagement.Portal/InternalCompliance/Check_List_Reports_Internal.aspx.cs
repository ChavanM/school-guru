﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class Check_List_Reports_Internal : System.Web.UI.Page
    {
        static int CustomerID;
        public static string role;
        protected static bool IsNotCompiled;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                { // STT Change- Add Status
                    IsNotCompiled = false;
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    long CustId = AuthenticationHelper.CustomerID;
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == CustId.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }
                    role = Request.QueryString["role"];

                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                    BindStatus();
                    BindTypes();
                    BindCategories();
                    //BindActList();
                    BindLocationFilter();
                   
                    FillData(role);

                    if (role == "Performer")
                    {
                        liPerformer.Text = "Performer";
                    }
                    else
                    {
                        liPerformer.Text = "Reviewer";
                    }

                    GetPageDisplaySummary();
                    if (tvFilterLocationPerformer.SelectedValue != "-1")
                        Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                    else
                        Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;

                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        private void BindLocationFilter()
        {
            try
            {              
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                 var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, "S");
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPerformer.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPerformer.Nodes.Add(node);
                }
                tvFilterLocationPerformer.CollapseAll();
                divFilterLocationPerformer.Style.Add("display", "none");
                tvFilterLocationPerformer_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocationPerformer_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPerformer.Text = tvFilterLocationPerformer.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindCategories()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                var CatList = ActManagement.GetAllAssignedCategory(CustomerID, AuthenticationHelper.UserID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypeList = ActManagement.GetAllAssignedType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindActList()
        //{
        //    try
        //    {
        //        ddlAct.Items.Clear();
        //        int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
        //        int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

        //        List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
        //        ddlAct.DataTextField = "Name";
        //        ddlAct.DataValueField = "ID";
        //        ddlAct.DataSource = ActList;
        //        ddlAct.DataBind();

        //        ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        private void BindStatus()
        {
            try
            {

                if (IsNotCompiled == true)
                {
                    foreach (CheckListCannedReportPerformerAddedNotCompliedStatus r in Enum.GetValues(typeof(CheckListCannedReportPerformerAddedNotCompliedStatus)))
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CheckListCannedReportPerformerAddedNotCompliedStatus), r), r.ToString());
                        ddlStatus.Items.Add(item);
                    }
                }
                else
                {
                    foreach (CheckListCannedReportPerformer r in Enum.GetValues(typeof(CheckListCannedReportPerformer)))
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CheckListCannedReportPerformer), r), r.ToString());
                        ddlStatus.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private void BindComplianceType()
        {
            try
            {
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                ddlType.DataSource = CustomerManagement.GetComplainceType();
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceCategory()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = CustomerManagement.GetComplainceCategory();
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindAct()
        //{
        //    try
        //    {
        //        ddlAct.DataTextField = "Name";
        //        ddlAct.DataValueField = "ID";

        //        ddlAct.DataSource = CustomerManagement.GetAct();
        //        ddlAct.DataBind();

        //        ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {              
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPerformer", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPerformer", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected string GetPerformer(int compliancectatusid, long scheduledonid, long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = InternalDashboardManagement.GetCheckListUserName(compliancectatusid, scheduledonid, complianceinstanceid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
                return "";
            }
        }
        protected string GetReviewer(int compliancectatusid, long scheduledonid, long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = InternalDashboardManagement.GetCheckListUserName(compliancectatusid, scheduledonid, complianceinstanceid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CheckListCannedReport");
                    DataTable ExcelData = null;
                    string filter = "";
                    String LocationName = String.Empty;

                    DateTime CurrentDate = DateTime.Today.Date;

                    if (Session["LocationName"].ToString() != "")
                        LocationName = Session["LocationName"].ToString();

                    DataView view = new System.Data.DataView(GetGrid());

                    //ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "CloseDate", "Risk", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");

                    ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "ShortDescription", "ForMonth", "InternalScheduledOn", "Risk", "Status", "InternalComplianceStatusID", "InternalScheduledOnID", "InternalComplianceInstanceID", "Remarks", "SequenceID","IShortForm");

                    ExcelData.Columns.Add("RiskName");
                    ExcelData.Columns.Add("Performer");
                    ExcelData.Columns.Add("Reviewer");
                 //  ExcelData.Columns.Add("IShortForm");

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        item["Performer"] = GetPerformer(Convert.ToInt32(item["InternalComplianceStatusID"].ToString()), Convert.ToInt32(item["InternalScheduledOnID"].ToString()), Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                        item["Reviewer"] = GetReviewer(Convert.ToInt32(item["InternalComplianceStatusID"].ToString()), Convert.ToInt32(item["InternalScheduledOnID"].ToString()), Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                        if (item["Risk"].ToString() == "0")
                            item["RiskName"] = "High";
                        else if (item["Risk"].ToString() == "1")
                            item["RiskName"] = "Medium";
                        else if (item["Risk"].ToString() == "3")
                            item["RiskName"] = "Critical";
                        else if (item["Risk"].ToString() == "2")
                            item["RiskName"] = "Low";

                        if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date > CurrentDate)
                            item["Status"] = "Upcoming";
                        else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date <= CurrentDate)
                            item["Status"] = "Overdue";
                        else if (item["Status"].ToString() == "ClosedTimely")
                            item["Status"] = "ClosedTimely";
                    }

                    ExcelData.Columns.Remove("InternalComplianceStatusID");
                    ExcelData.Columns.Remove("InternalScheduledOnID");
                    ExcelData.Columns.Remove("InternalComplianceInstanceID");
                    ExcelData.Columns.Remove("Risk");


                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                    exWorkSheet.Cells["B1:C1"].Merge = true;
                    exWorkSheet.Cells["B1"].Value = LocationName;

                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Value = "Report Name:";

                    exWorkSheet.Cells["B2:C2"].Merge = true;

                    exWorkSheet.Cells["B2"].Value = "Report of Checklist Internal Compliances";
           
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                    exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");                 

                    //Load Data From Excel
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                    exWorkSheet.Cells["B5"].Value = "Location";
                    //exWorkSheet.Cells["C5"].Value = "Act";
                    //exWorkSheet.Cells["D5"].Value = "Category Name";
                    exWorkSheet.Cells["C5"].Value = "Short Description";

                    //exWorkSheet.Cells["F5"].Value = "Detail Description";
                    exWorkSheet.Cells["D5"].Value = "Period";
                    exWorkSheet.Cells["E5"].Value = "Due Date"; 
                    //exWorkSheet.Cells["I5"].Value = "Close Date";
                    exWorkSheet.Cells["F5"].Value = "Status";
                    exWorkSheet.Cells["G5"].Value = "Remark";
                    exWorkSheet.Cells["H5"].Value = "Label";
                    exWorkSheet.Cells["I5"].Value = "IShortForm";
                    exWorkSheet.Cells["J5"].Value = "Risk";
                    exWorkSheet.Cells["K5"].Value = "Performer";
                    exWorkSheet.Cells["L5"].Value = "Reviewer";
                  


                    //Heading
                    if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                    {
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Numberformat.Format = "dd-MMM-yyyy";
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L5"].Style.Font.Bold = true;

                        int colIdx = ExcelData.Columns.IndexOf("InternalComplianceID") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        //colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                        //exWorkSheet.Column(colIdx).Width = 25;

                        //colIdx = ExcelData.Columns.IndexOf("ComplianceCategoryName") + 1;
                        //exWorkSheet.Column(colIdx).Width = 35;

                        colIdx = ExcelData.Columns.IndexOf("ShortDescription") + 1;
                        exWorkSheet.Column(colIdx).Width = 35;

                        //colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                        //exWorkSheet.Column(colIdx).Width = 100;

                        colIdx = ExcelData.Columns.IndexOf("ForMonth") + 1;
                        exWorkSheet.Column(colIdx).Width = 10;

                        colIdx = ExcelData.Columns.IndexOf("InternalScheduledOn") + 1;
                        exWorkSheet.Column(colIdx).Width = 15;

                        //colIdx = ExcelData.Columns.IndexOf("CloseDate") + 1;
                        //exWorkSheet.Column(colIdx).Width = 25;

                        colIdx = ExcelData.Columns.IndexOf("Performer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Risk") + 1;
                        exWorkSheet.Column(colIdx).Width = 10;

                        colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Remarks") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("SequenceID") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("IShortForm") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Reviewer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                     



                    }

                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 12])
                    {
                        col.Style.WrapText = true;
                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.AutoFitColumns();

                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col[5, 2, 5 + ExcelData.Rows.Count, 12].Style.Numberformat.Format = "dd/MMM/yyyy";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Checklist_InternalCannedReport.xlsx");

                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "ClosedTimely")
                            e.Row.Cells[6].Text = "ClosedTimely";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CheckListCannedReportPerformer>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DataTable GetGrid()
        {
            FillData(role);
            if (role == "Performer")
            { 
            return (grdComplianceTransactions.DataSource as List<InternalComplianceInstanceCheckListTransactionView>).ToDataTable();
            }
            else
            {
                return (grdComplianceTransactions.DataSource as List<InternalComplianceInstanceCheckListTransactionView>).ToDataTable();
            }
        }
        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CheckListCannedReportPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillData(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }           
        }
        protected void FillData(string role)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);

                CheckListCannedReportPerformerAddedNotCompliedStatus status1 = new CheckListCannedReportPerformerAddedNotCompliedStatus();
                CheckListCannedReportPerformer status = new CheckListCannedReportPerformer();
                if (IsNotCompiled == true)
                {
                    status1 = (CheckListCannedReportPerformerAddedNotCompliedStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                }
                else
                {
                    status = (CheckListCannedReportPerformer)Convert.ToInt16(ddlStatus.SelectedIndex);
                }
                //int location = Convert.ToInt32(ddlLocation.SelectedValue);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    if (role == "Performer")
                    {

                        if (IsNotCompiled == true)
                        {
                            var CheckList = InternalCanned_ReportManagement.GetCannedReportInternalDataForPerformerAddedNotCompliedStatus(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, risk, status1, location, type, category, dtfrom, dtTo);
                            grdComplianceTransactions.DataSource = CheckList;
                            Session["TotalRowsPerformer"] = CheckList.Count();
                            grdComplianceTransactions.DataBind();
                        }
                        else
                        {
                            var CheckList = InternalCanned_ReportManagement.GetCannedReportInternalDataForPerformer(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, risk, status, location, type, category, dtfrom, dtTo);
                            grdComplianceTransactions.DataSource = CheckList;
                            Session["TotalRowsPerformer"] = CheckList.Count();
                            grdComplianceTransactions.DataBind();
                        }
                    }
                    else
                    {

                        if (IsNotCompiled == true)
                        {
                            var CheckList = InternalCanned_ReportManagement.GetCannedReportInternalDataForReviewerAddedNotCompliedStatus(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, risk, status1, location, type, category, dtfrom, dtTo);
                            grdComplianceTransactions.DataSource = CheckList;
                            Session["TotalRowsPerformer"] = CheckList.Count();
                            grdComplianceTransactions.DataBind();
                        }
                        else
                        {
                            var CheckList = InternalCanned_ReportManagement.GetCannedReportInternalDataForReviewer(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, risk, status, location, type, category, dtfrom, dtTo);
                            grdComplianceTransactions.DataSource = CheckList;
                            Session["TotalRowsPerformer"] = CheckList.Count();
                            grdComplianceTransactions.DataBind();
                        }
                    }

                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
               // ddlAct.SelectedValue = "-1";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                divAdvSearch.Visible = false;
                FillData(role);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }
        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                grdComplianceTransactions.PageSize = int.Parse(((DropDownList)sender).SelectedValue);
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role);

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }              
                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRowsPerformer"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRowsPerformer"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRowsPerformer"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }        
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CheckListCannedReportPerformer status = (CheckListCannedReportPerformer)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);

                grdComplianceTransactions.Visible = true;
                FillData(role);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }
        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                SelectedPageNo.Text = "1";
                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }
                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }
                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }
                //if (ddlAct.SelectedValue != "-1")
                //{
                //    Act = ddlAct.SelectedItem.Text.Length;

                //    if (lblAdvanceSearchScrum.Text != "")
                //    {
                //        if (Act >= 30)
                //            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                //        else
                //            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                //    }
                //    else
                //    {
                //        if (Act >= 30)
                //            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                //        else
                //            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                //    }
                //}
                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                FillData(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
    }
}