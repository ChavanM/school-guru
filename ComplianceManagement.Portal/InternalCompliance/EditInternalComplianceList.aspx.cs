﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Text.RegularExpressions;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class EditInternalComplianceList : System.Web.UI.Page
    {
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    customerdiv.Visible = true;
                    lblcustomer.Visible = true;
                    BindCustomers(UserID);
                }
                else
                {
                    customerdiv.Visible = false;
                    lblcustomer.Visible = false;
                }

                BindCategories(ddlICategory);
                BindICategories(ddlComplinceCatagory);
                BindTypes();
                BindIType();
                BindDueDates();
                BindFrequencies();
                BindFilterFrequencies();
                BindCompliances();
                BindDueDays();

            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCategories(ddlICategory);
            BindICategories(ddlComplinceCatagory);
            BindFrequencies();
            BindFilterFrequencies();
            BindTypes();
            BindIType();
            BindCompliances();
        }
        private void BindCustomers(int userid)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                lblErrorMassage.Text = string.Empty;
                txtShortDescription.Text = string.Empty;
                txtShortForm.Text = string.Empty;
                tbxRequiredForms.Text = string.Empty;
                ddlIComplinceType.SelectedIndex = ddlICategory.SelectedIndex = -1;
                ddlComplianceType.SelectedIndex = ddlFrequency.SelectedIndex = ddlDueDate.SelectedIndex = 0;
                chkDocument.Checked = false;
                lblSampleForm.Text = "< Not selected >";
                divCompType.Visible = true;
                //ddlComplianceType.Visible = true;
                ddlComplianceType_SelectedIndexChanged(null, null);
                rbReminderType.SelectedValue = "0";
                divForCustome.Visible = false;
                txtReminderBefore.Text = string.Empty;
                txtReminderGap.Text = string.Empty;
                ddlRiskType.SelectedIndex = -1;

                divChecklist.Visible = false;
                rbtnlstCompOcc.SelectedValue = "1";
                divOnetimeDate.Visible = false;
                divComplianceDueDays.Visible = false;
                rbReminderType.Items[0].Enabled = true;
                divTimebasedTypes.Visible = false;// added by rahul on 9 JAn 2016


                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //DateTime dtEffectiveDate = new DateTime();

                //if (txtEffectiveDate.Text != null)
                //{
                //    dtEffectiveDate = DateTime.ParseExact(txtEffectiveDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //}

                Business.Data.InternalCompliance Icompliance = new Business.Data.InternalCompliance()
                {
                    IComplianceTypeID = Convert.ToInt32(ddlIComplinceType.SelectedValue),
                    IComplianceCategoryID = Convert.ToInt32(ddlICategory.SelectedValue),
                    IShortDescription = txtShortDescription.Text,
                    IShortForm=txtShortForm.Text,
                    IUploadDocument = chkDocument.Checked,
                    IComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                    IRequiredFrom = tbxRequiredForms.Text,
                    IRiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                    IComplianceOccurrence = Convert.ToInt32(rbtnlstCompOcc.SelectedValue),
                    //IOneTimeDate = dtOneTimeDate,
                    //IComplianceOccourrence = Convert.ToInt32(rbtnlstCompOcc.SelectedValue),
                    //IOneTimeDate = dtOneTimeDate,
                    //EffectiveDate = dtEffectiveDate,
                    //DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue)
                };

                if (divOnetimeDate.Visible == false)
                {
                    if (Icompliance.IComplianceType == 0 || Icompliance.IComplianceType == 2)//function based or time based
                    {
                        if (Icompliance.IComplianceType == 2)
                        {
                            if (rbTimebasedTypes.SelectedValue.Equals("0"))
                            {
                                Icompliance.ISubComplianceType = 0;
                                Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text);
                            }
                            else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                            {
                                Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                Icompliance.ISubComplianceType = 1;
                                Icompliance.IDueDate = null;
                            }

                        }
                        else
                        {
                            Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                            Icompliance.IDueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                        }
                    }
                    else  ////////////////////SACHIN added////////////////////////////////////////
                    {
                        Icompliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);

                        //------checklist------
                        if (Icompliance.CheckListTypeID == 1 || Icompliance.CheckListTypeID == 2)//function based or time based
                        {
                            if (Icompliance.CheckListTypeID == 2)//time based
                            {

                                if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                                {
                                    //Icompliance.EventID = null;
                                    Icompliance.IFrequency = null;
                                    Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text);
                                    Icompliance.ISubComplianceType = 0;
                                }
                                else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                                {
                                    //Icompliance.EventID = null;
                                    Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    Icompliance.IDueDate = null;
                                    Icompliance.ISubComplianceType = 1;
                                }
                                else
                                {
                                    //Icompliance.EventID = null;
                                    Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    Icompliance.IDueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                    Icompliance.ISubComplianceType = 2;
                                }
                            }
                            else
                            {
                                
                                Icompliance.IFrequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                Icompliance.IDueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                            }
                        }
                        ////////////////////SACHIN added////////////////////////////////////////

                    }
                }
                else
                {
                    string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                    DateTime dtOneTimeDate = new DateTime();
                    if (Onetimedate != "")
                    {
                        dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Icompliance.IOneTimeDate = dtOneTimeDate;
                    }
                }
              //  }
                InternalComplianceForm form = null;
                if (chkDocument.Checked)
                {
                    if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                    {
                        form = new InternalComplianceForm()
                        {
                            Name = fuSampleFile.FileName,
                            FileData = fuSampleFile.FileBytes
                        };
                    }
                }


                Icompliance.IReminderType = Convert.ToByte(rbReminderType.SelectedValue);
                if (rbReminderType.SelectedValue.Equals("1"))
                {

                    Icompliance.IReminderBefore = Convert.ToInt32(txtReminderBefore.Text);
                    Icompliance.IReminderGap = Convert.ToInt32(txtReminderGap.Text);
                }


                if ((int) ViewState["Mode"] == 1)
                {
                    Icompliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                }

                Icompliance.CustomerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;

                if ((int) ViewState["Mode"] == 0)
                {
                    Icompliance.CreatedBy = AuthenticationHelper.UserID;
                    Icompliance.UpdatedBy = AuthenticationHelper.UserID;
                    Business.ComplianceManagement.Create(Icompliance, form);
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    #region[Compliance Log]
                    //Compliance_Log Table

                    string compliancetype = string.Empty;
                    string Frequency = string.Empty;
                    string duedate = string.Empty;

                    if (ddlFrequency.SelectedValue != null)
                    {
                        Frequency = Convert.ToString(ddlFrequency.SelectedValue);
                    }
                    else
                    {
                        Frequency = "";
                    }
                    if (Icompliance.IComplianceType == 1)
                    {
                        if (Icompliance.CheckListTypeID != null)
                        {
                            Icompliance.IComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue);
                            Icompliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);
                            compliancetype = Icompliance.IComplianceType + " checklist type=" + Icompliance.CheckListTypeID;
                        }
                    }
                    else
                    {
                        Icompliance.IComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue);
                        compliancetype = Convert.ToString(ddlComplianceType.SelectedValue);
                    }
                    if (ddlDueDate.SelectedValue != null)
                    {
                        duedate = Convert.ToString(ddlDueDate.SelectedValue);
                    }
                    else
                    {
                        duedate = " ";
                    }
                    var NewData1 = Business.ComplianceManagement.ComplianceNewData(Frequency, duedate, compliancetype);
                    var OldData1 = ViewState["OldData"];

                    Compliance_Log comp = new Compliance_Log()
                    {
                        OldData = Convert.ToString(OldData1),
                        NewData = Convert.ToString(NewData1),
                        UpdatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                        ComplianceID = Icompliance.ID,
                        Type = "I"

                    };
                    Business.ComplianceManagement.CreateCompliance_Log(comp);

                    #endregion
                    Business.InternalComplianceManagement.EditCompliance(Icompliance, form, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    Business.ComplianceManagement.Update(Icompliance, form);
                }

                BindCompliances();
                upCompliancesList.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('close')", true);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void OpenScheduleInformation(Business.Data.InternalCompliance compliance)
        {
            try
            {
                ViewState["ComplianceID"] = compliance.ID;
                ViewState["Frequency"] = compliance.IFrequency.Value;
                if (compliance.IDueDate != null)
                {
                    ViewState["Day"] = compliance.IDueDate.Value;
                }

                if ((compliance.IFrequency.Value == 0 || compliance.IFrequency.Value == 1))
                    divStartMonth.Visible = false;
                else
                    divStartMonth.Visible = true;

                var scheduleList = Business.ComplianceManagement.GetInternalScheduleByIComplianceID(compliance.ID);
                if (scheduleList.Count == 0)
                {
                    Business.ComplianceManagement.GenerateScheduleForInternalCompliance(compliance.ID, compliance.ISubComplianceType);
                    scheduleList = Business.ComplianceManagement.GetInternalScheduleByIComplianceID(compliance.ID);
                }

                int step = 0;
                if (compliance.IFrequency.Value == 0)
                    step = 0;
                else if (compliance.IFrequency.Value == 1)
                    step = 2;
                else if (compliance.IFrequency.Value == 2)
                    step = 5;
                else if (compliance.IFrequency.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.IFrequency.Value == 0 ? ((Month) entry.ForMonth).ToString() : ((Month) entry.ForMonth).ToString() + " - " + ((Month) ((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();

                //if (compliance.EffectiveDate == null)
                //{
                //    lblEffectivedate.Text = "";
                //    lblEffectivedate.ForeColor = System.Drawing.Color.Red;
                //    lblEffectivedate.Text = "Please Enter Effective Date First";
                //}
                //else
                //{
                //    lblEffectivedate.Text = "";
                //    lblEffectivedate.Text = Convert.ToDateTime(compliance.EffectiveDate).ToString("dd-MMM-yyyy");
                //}

                if (divStartMonth.Visible)
                {

                    if (compliance.ISubComplianceType == 1)
                    {
                        if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
                            ddlStartMonth.SelectedValue = Convert.ToString(1);
                        else
                            ddlStartMonth.SelectedValue = Convert.ToString(4);
                    }
                    else
                    {
                        ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                    }
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField) entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList) entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month) Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });


                    Month month = (Month) Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Frequency frequency = (Frequency) Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField) entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList) entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                if (frequency == Frequency.FourMonthly)
                                    specialMonth = startMonth + (step * i) + 4;
                                else
                                    specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {
                            if (frequency == Frequency.FourMonthly)
                                specialMonth = 4;
                            else
                                specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == Frequency.Monthly ? ((Month) month).ToString() : ((Month) month).ToString() + " - " + ((Month) ((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList) e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList) e.Item.FindControl("ddlMonths");

                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month) Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte) month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int complianceID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    lblErrorMassage.Text = "";
                    var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);

                    if (compliance != null)
                    {

                        var complianceForm = Business.ComplianceManagement.GetInternalComplianceFormByID(complianceID);

                        ViewState["Mode"] = 1;
                        ViewState["ComplianceID"] = complianceID;

                        ddlIComplinceType.SelectedValue = compliance.IComplianceTypeID.ToString();
                        if (compliance.IComplianceCategoryID != null)
                        {
                            ddlICategory.SelectedValue = compliance.IComplianceCategoryID.ToString();
                        }
                        txtShortDescription.Text = compliance.IShortDescription;
                        txtShortForm.Text = compliance.IShortForm;
                        chkDocument.Checked = compliance.IUploadDocument ?? false;
                        ddlComplianceType.SelectedValue = compliance.IComplianceType.ToString();
                        tbxRequiredForms.Text = compliance.IRequiredFrom;
                        ddlRiskType.SelectedValue = compliance.IRiskType.ToString();

                        ddlComplianceType_SelectedIndexChanged(null, null);

                        #region[Compliance_log]
                        string frequency = string.Empty;
                        string compliancetype = string.Empty;
                        string duedate = string.Empty;
                        if (compliance.IDueDate != null)
                        {
                            duedate = Convert.ToString(compliance.IDueDate);
                        }
                        else
                        {
                            duedate = "";
                        }
                        if (ddlComplianceType.SelectedValue != null)
                        {
                            if (ddlComplianceType.SelectedValue == "1")
                            {
                                ddlComplianceType.SelectedValue = Convert.ToString(compliance.IComplianceType);
                                ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                                compliancetype = compliance.IComplianceType + " checklist type=" + compliance.CheckListTypeID;
                            }
                            else
                            {
                                compliancetype = ddlComplianceType.SelectedValue;
                            }

                        }
                        else
                        {
                            compliancetype = " ";
                        }
                        if (compliance.IFrequency != null)
                        {
                            ddlFrequency.SelectedValue = Convert.ToString(compliance.IFrequency);
                            frequency = Convert.ToString(compliance.IFrequency);
                        }
                        else
                        {
                            frequency = "";

                        }


                        var OldData = Business.ComplianceManagement.ComplianceOldData(frequency, duedate, compliancetype);
                        ViewState["OldData"] = OldData;

                        #endregion

                        if (complianceForm != null)
                        {
                            lblSampleForm.Text = complianceForm.Name;
                        }
                        else
                        {
                            lblSampleForm.Text = "< Not selected >";
                        }

                        if (compliance.IComplianceOccurrence == 1)
                        {
                            divCompType.Visible = true;
                            divTimebasedTypes.Visible = false;
                            divFunctionBased.Visible = true;
                            divOnetimeDate.Visible = false;
                            rbtnlstCompOcc.SelectedValue = "1";
                            rbReminderType.Items[0].Enabled = false;
                            if (compliance.IComplianceType == 0 || compliance.IComplianceType == 2) //function based or time based
                            {

                                ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

                                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                {
                                    vivDueDate.Visible = false;
                                }
                                else
                                {
                                    vivDueDate.Visible = true;
                                }
                                vivWeekDueDays.Visible = false;
                                if (ddlFrequency.SelectedValue == "8")
                                {
                                    ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                    vivWeekDueDays.Visible = true;
                                }
                                else
                                {
                                    vivWeekDueDays.Visible = false;
                                }

                                if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
                                {
                                    if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                    {
                                        vivDueDate.Visible = false;
                                    }
                                    else
                                    {
                                        vivDueDate.Visible = true;
                                    }
                                    ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                }
                                else
                                {
                                    if (compliance.IDueDate != null && compliance.IDueDate != -1)
                                    {
                                        if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                        {
                                            vivDueDate.Visible = false;
                                        }
                                        else
                                        {
                                            vivDueDate.Visible = true;
                                        }
                                        if (compliance.IDueDate <= 31)
                                        {
                                            ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                        }

                                    }

                                }
                                if (compliance.IComplianceType == 2)
                                {
                                    divTimebasedTypes.Visible = true;
                                    if (compliance.ISubComplianceType == 0)
                                    {
                                        divNonEvents.Visible = false;
                                        divFrequency.Visible = false;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = false;

                                        divComplianceDueDays.Visible = true;
                                        txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();

                                    }
                                    else if (compliance.ISubComplianceType == 1)
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = true;

                                        divComplianceDueDays.Visible = false;
                                        txtEventDueDate.Text = null;

                                    }
                                    else
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;

                                        cvfrequency.Enabled = true;

                                    }
                                    rbTimebasedTypes.Visible = true;
                                    rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
                                }

                            }

                            else// checklist
                            {
                                ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                                //------checklist------
                                if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                                {
                                    divOnetimeDate.Visible = false;
                                    divFunctionBased.Visible = true;
                                    divFrequency.Visible = true;
                                    divComplianceDueDays.Visible = false;
                                    divNonEvents.Visible = true;
                                    txtEventDueDate.Text = string.Empty;
                                    ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

                                    if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
                                    {
                                        ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                    }
                                    else
                                    {
                                        if (compliance.IDueDate != null && compliance.IDueDate != -1)
                                        {
                                            if (compliance.IDueDate <= 31)
                                            {
                                                ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
                                            }
                                            else
                                            {
                                                txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                            }
                                        }
                                    }
                                    if (compliance.CheckListTypeID == 2)
                                    {
                                        divTimebasedTypes.Visible = true;
                                        if (compliance.ISubComplianceType == 0)//fixed gap -- time based
                                        {
                                            divNonEvents.Visible = false;
                                            divFunctionBased.Visible = false;
                                            divFrequency.Visible = false;
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = false;
                                            divComplianceDueDays.Visible = true;
                                            rgexEventDueDate.Enabled = true;
                                            txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                        }
                                        else if (compliance.ISubComplianceType == 1)
                                        {
                                            divNonEvents.Visible = true;
                                            divFunctionBased.Visible = true;
                                            divFrequency.Visible = true;
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = true;
                                            divComplianceDueDays.Visible = false;
                                            rgexEventDueDate.Enabled = false;
                                        }
                                        else
                                        {
                                            divNonEvents.Visible = true;
                                            divFrequency.Visible = true;
                                            vivDueDate.Visible = false;
                                            rfvEventDue.Enabled = false;
                                            cvfrequency.Enabled = true;
                                            divComplianceDueDays.Visible = true;
                                            rgexEventDueDate.Enabled = true;
                                            txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
                                        }

                                        rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
                                    }
                                    // }

                                }
                                else//one time checklist
                                {

                                    divComplianceDueDays.Visible = false;
                                    divNonEvents.Visible = true;
                                    txtEventDueDate.Text = string.Empty;

                                    divTimebasedTypes.Visible = false;
                                    divFunctionBased.Visible = false;
                                    divOnetimeDate.Visible = true;

                                    tbxOnetimeduedate.Text = compliance.IOneTimeDate != null ? compliance.IOneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                                    divComplianceDueDays.Visible = false;
                                    txtEventDueDate.Text = null;
                                }
                            }
                        }
                        else
                        {
                            divFunctionBased.Visible = false;
                            divCompType.Visible = false;
                            rbtnlstCompOcc.SelectedValue = "0";
                            divOnetimeDate.Visible = true;
                            divChecklist.Visible = false;
                            tbxOnetimeduedate.Text = compliance.IOneTimeDate != null ? compliance.IOneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        rbReminderType.SelectedValue = Convert.ToString(compliance.IReminderType);
                        if (compliance.IReminderType == 0)
                        {
                            divForCustome.Visible = false;
                        }
                        else
                        {
                            divForCustome.Visible = true;
                        }

                        txtReminderBefore.Text = Convert.ToString(compliance.IReminderBefore);
                        txtReminderGap.Text = Convert.ToString(compliance.IReminderGap);

                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(complianceID);
                    if (chkexistsComplianceschedule == false)
                    {
                        Business.ComplianceManagement.DeleteInternalCompliance(complianceID);
                        BindCompliances();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned,can not deleted.";
                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);
                    OpenScheduleInformation(compliance);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        //protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        int complianceID = Convert.ToInt32(e.CommandArgument);
        //        if (e.CommandName.Equals("EDIT_COMPLIANCE"))
        //        {
        //            lblErrorMassage.Text = "";
        //            var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);

        //            //var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceID);
        //            var complianceForm = Business.ComplianceManagement.GetInternalComplianceFormByID(complianceID);

        //            ViewState["Mode"] = 1;
        //            ViewState["ComplianceID"] = complianceID;

        //            ddlIComplinceType.SelectedValue = compliance.IComplianceTypeID.ToString();
        //            if (compliance.IComplianceCategoryID != null)
        //            {
        //                ddlICategory.SelectedValue = compliance.IComplianceCategoryID.ToString();
        //            }
        //            txtShortDescription.Text = compliance.IShortDescription;
        //            chkDocument.Checked = compliance.IUploadDocument ?? false;
        //            ddlComplianceType.SelectedValue = compliance.IComplianceType.ToString();
        //            tbxRequiredForms.Text = compliance.IRequiredFrom;
        //            ddlRiskType.SelectedValue = compliance.IRiskType.ToString();
        //            ddlComplianceType_SelectedIndexChanged(null, null);
        //            if (complianceForm != null)
        //            {
        //                lblSampleForm.Text = complianceForm.Name;
        //            }
        //            else
        //            {
        //                lblSampleForm.Text = "< Not selected >";
        //            }

        //            if (compliance.IComplianceOccurrence == 1)
        //            {
        //                divCompType.Visible = true;
        //                divTimebasedTypes.Visible = false;                        
        //                divFunctionBased.Visible = true;
        //                divOnetimeDate.Visible = false;
        //                rbtnlstCompOcc.SelectedValue = "1";
        //                rbReminderType.Items[0].Enabled = false;
        //                if (compliance.IComplianceType == 0 || compliance.IComplianceType == 2) //function based or time based
        //                {

        //                    ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

        //                    if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
        //                    {
        //                        vivDueDate.Visible = true;
        //                        ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
        //                    }
        //                    else
        //                    {
        //                        if (compliance.IDueDate != null && compliance.IDueDate != -1)
        //                        {
        //                            vivDueDate.Visible = true;
        //                            if (compliance.IDueDate <= 31)
        //                            {
        //                                ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
        //                            }

        //                        }

        //                    }
        //                    if (compliance.IComplianceType == 2)
        //                    {
        //                        divTimebasedTypes.Visible = true;
        //                        if (compliance.ISubComplianceType == 0)
        //                        {
        //                            divNonEvents.Visible = false;
        //                            divFrequency.Visible = false;
        //                            vivDueDate.Visible = false;

        //                            cvfrequency.Enabled = false;

        //                            divComplianceDueDays.Visible = true;
        //                            txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();

        //                        }
        //                        else if (compliance.ISubComplianceType == 1)
        //                        {
        //                            divNonEvents.Visible = true;
        //                            divFrequency.Visible = true;
        //                            vivDueDate.Visible = false;

        //                            cvfrequency.Enabled = true;

        //                            divComplianceDueDays.Visible = false;
        //                            txtEventDueDate.Text = null;

        //                        }
        //                        else
        //                        {
        //                            divNonEvents.Visible = true;
        //                            divFrequency.Visible = true;
        //                            vivDueDate.Visible = false;

        //                            cvfrequency.Enabled = true;

        //                        }
        //                        rbTimebasedTypes.Visible = true;
        //                        rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
        //                    }

        //                }

        //                else// checklist
        //                {
        //                    ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
        //                    //------checklist------
        //                    if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
        //                    {
        //                        divOnetimeDate.Visible = false;

        //                        divFunctionBased.Visible = true;
        //                        divFrequency.Visible = true;                             
        //                        divComplianceDueDays.Visible = false;
        //                        divNonEvents.Visible = true;                                
        //                        txtEventDueDate.Text = string.Empty;
        //                        ddlFrequency.SelectedValue = (compliance.IFrequency ?? 0).ToString();

        //                        if (compliance.IDueDate != null && compliance.ISubComplianceType == 0 && compliance.ISubComplianceType == 2)
        //                        {
        //                            ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
        //                        }
        //                        else
        //                        {
        //                            if (compliance.IDueDate != null && compliance.IDueDate != -1)
        //                            {
        //                                if (compliance.IDueDate <= 31)
        //                                {
        //                                    ddlDueDate.SelectedValue = (compliance.IDueDate ?? 0).ToString();
        //                                }
        //                                else
        //                                {
        //                                    txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
        //                                }
        //                            }
        //                        }


        //                        //if (compliance.ComplianceType == 2)
        //                        if (compliance.CheckListTypeID == 2)
        //                        {
        //                            divTimebasedTypes.Visible = true;
        //                            if (compliance.ISubComplianceType == 0)//fixed gap -- time based
        //                            {
        //                                divNonEvents.Visible = false;
        //                                divFunctionBased.Visible = false;
        //                                //divNonComplianceType.Visible = false;
        //                                divFrequency.Visible = false;
        //                                vivDueDate.Visible = false;
        //                                rfvEventDue.Enabled = false;
        //                                cvfrequency.Enabled = false;
        //                                divComplianceDueDays.Visible = true;
        //                                rgexEventDueDate.Enabled = true;
        //                                txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
        //                            }
        //                            else if (compliance.ISubComplianceType == 1)
        //                            {
        //                                divNonEvents.Visible = true;
        //                                divFunctionBased.Visible = true;
        //                                divFrequency.Visible = true;                                        
        //                                vivDueDate.Visible = false;
        //                                rfvEventDue.Enabled = false;
        //                                cvfrequency.Enabled = true;
        //                                divComplianceDueDays.Visible = false;
        //                                rgexEventDueDate.Enabled = false;
        //                            }
        //                            else
        //                            {
        //                                divNonEvents.Visible = true;
        //                                divFrequency.Visible = true;
        //                                vivDueDate.Visible = false;
        //                                rfvEventDue.Enabled = false;
        //                                cvfrequency.Enabled = true;
        //                                divComplianceDueDays.Visible = true;
        //                                rgexEventDueDate.Enabled = true;
        //                                txtEventDueDate.Text = (compliance.IDueDate ?? 0).ToString();
        //                            }

        //                            rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.ISubComplianceType);
        //                        }
        //                        // }

        //                    }
        //                    else//one time checklist
        //                    {

        //                        divComplianceDueDays.Visible = false;
        //                        divNonEvents.Visible = true;                                
        //                        txtEventDueDate.Text = string.Empty;                            
        //                        divTimebasedTypes.Visible = false;
        //                        divFunctionBased.Visible = false;
        //                        divOnetimeDate.Visible = true;

        //                        tbxOnetimeduedate.Text = compliance.IOneTimeDate != null ? compliance.IOneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
        //                        divComplianceDueDays.Visible = false;
        //                        txtEventDueDate.Text = null;                                

        //                    }

        //                }
        //            }            
        //            rbReminderType.SelectedValue = Convert.ToString(compliance.IReminderType);
        //            if (compliance.IReminderType == 0)
        //            {
        //                divForCustome.Visible = false;
        //            }
        //            else
        //            {
        //                divForCustome.Visible = true;
        //            }

        //            txtReminderBefore.Text = Convert.ToString(compliance.IReminderBefore);
        //            txtReminderGap.Text = Convert.ToString(compliance.IReminderGap);

        //            upComplianceDetails.Update();
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divInternalComplianceDetailsDialog\").dialog('open')", true);


        //        }
        //        else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
        //        {
        //            Business.ComplianceManagement.DeleteInternalCompliance(complianceID);
        //            BindCompliances();
        //        }
        //        else if (e.CommandName.Equals("SHOW_SCHEDULE"))
        //        {
        //            var compliance = Business.ComplianceManagement.GetInternalComplianceByID(complianceID);
        //            OpenScheduleInformation(compliance);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFunctionBased.Visible = vaDueDate.Enabled = (ddlComplianceType.SelectedValue == "0" || ddlComplianceType.SelectedValue == "2");

                if (ddlComplianceType.SelectedValue == "0")//function based
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = true;
                    divTimebasedTypes.Visible = false;
                    divChecklist.Visible = false;
                    divOnetimeDate.Visible = false;
                }
                else if (ddlComplianceType.SelectedValue.Equals("2"))//timebased
                {
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = true;
                    if (rbTimebasedTypes.SelectedValue == "0")
                    {
                        divComplianceDueDays.Visible = true;
                    }
                    else
                    {
                        divComplianceDueDays.Visible = false;
                    }
                    rbTimebasedTypes.SelectedValue = "0";
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divChecklist.Visible = false;
                    divOnetimeDate.Visible = false;
                }
                else//checklist
                {
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divChecklist.Visible = true;
                    divOnetimeDate.Visible = true;
                    ddlChklstType.SelectedValue = "0";
                    if (ddlChklstType.SelectedValue == "0")//one time
                    {
                        rbReminderType.SelectedValue = "1";
                        rbReminderType.Enabled = false;
                        //rbReminderType.Items[0].Enabled = false;
                        divForCustome.Visible = true;
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbTimebasedTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;

                    cvfrequency.Enabled = false;
                    divComplianceDueDays.Visible = true;

                }
                else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;

                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = false;

                }
                else
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;

                    cvfrequency.Enabled = true;


                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                //if (lblEffectivedate.Text == "" || lblEffectivedate.Text == "Please Enter Effective Date First")
                //{
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "Effective Cannot be Blank.";
                //}
                //else
                //{
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    //bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    //if (chkexistsComplianceschedule == true)
                    //{
                    //    cvDuplicateEntry.IsValid = false;
                    //    cvDuplicateEntry.ErrorMessage = "Internal Compliance assigned cannot be change Calender/Financial Year.";
                    //}
                    //else
                    //{

                    List<InternalComplianceSchedule> scheduleList = new List<InternalComplianceSchedule>();

                    for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                    {
                        RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                        HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                        HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                        DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                        DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                        scheduleList.Add(new InternalComplianceSchedule()
                        {
                            ID = Convert.ToInt32(hdnID.Value),
                            IComplianceID = Convert.ToInt64(ViewState["ComplianceID"]),
                            ForMonth = Convert.ToInt32(hdnForMonth.Value),
                            SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                        });
                    }

                    Business.ComplianceManagement.UpdateInternalComplianceScheduleInformation(scheduleList);
                    Business.InternalComplianceManagement.UpdateComplianceUpdatedOn(Convert.ToInt64(ViewState["ComplianceID"]));
                    //DateTime dEffectiveDate = Convert.ToDateTime(lblEffectivedate.Text.Trim());

                    //Business.InternalComplianceManagement.ComplianceScheduleOnUpdateAsNotActive(Convert.ToInt64(ViewState["ComplianceID"]), AuthenticationHelper.UserID, dEffectiveDate);
                    //Business.InternalComplianceManagement.UpdateOldData(Convert.ToInt64(ViewState["ComplianceID"]), AuthenticationHelper.UserID, AuthenticationHelper.User, dEffectiveDate);


                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                    upSchedulerRepeter.Update();
                    //}
                }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                //Effective Date
                if (DateTime.TryParseExact(txtEffectiveDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerEffectiveDate", string.Format("initializeDatePickerEffectiveDate(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerEffectiveDate", "initializeDatePickerEffectiveDate(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                //{
                //    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;
                //    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");
                //    AsyncPostBackTrigger apbt = new AsyncPostBackTrigger();
                //    apbt.ControlID = ddlMonths.UniqueID;
                //    upComplianceScheduleDialog.Triggers.Add(apbt);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            //rfvFile.Enabled = chkDocument.Checked;
            if (!chkDocument.Checked)
            {
                lblSampleForm.Text = "< Not selected >";
            }
        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                //if (rdFunctionBased.Checked)
                //    CmType = 0;
                //if (rdChecklist.Checked)
                //    CmType = 1;
                int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, CustomerID, tbxFilter.Text);
                //var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.IRiskType == 0)
                        risk = "High";
                    else if (complianceInfo.IRiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.IRiskType == 2)
                        risk = "Low";
                    else if (complianceInfo.IRiskType == 3)
                        risk = "Critical";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.IComplianceTypeName,
                        complianceInfo.IComplianceCategoryName,
                        complianceInfo.IShortDescription,
                        complianceInfo.IShortForm,
                        complianceInfo.IComplianceType,
                        complianceInfo.IUploadDocument,
                        complianceInfo.IRequiredFrom,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int) complianceInfo.IFrequency : -1)),
                        Risk = risk,
                        complianceInfo.ISubComplianceType,
                        complianceInfo.CreatedOn,
                        complianceInfo.CheckListTypeID
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }

                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(customerID);    //(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID))
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories(DropDownList ddlCategory)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                //int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll(customerID);
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindICategories(DropDownList ddlCategory)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                //int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll(customerID);
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindFrequencies()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";


                ddlFrequency.DataSource = Enumerations.GetAll<Frequency>();
                ddlFrequency.DataBind();
                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompliances()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                int CmType = -1;
                //if (rdFunctionBased.Checked)
                //    CmType = 0;
                //if (rdChecklist.Checked)
                //    CmType = 1;
                int CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                //var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue),Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                var compliancesData = Business.ComplianceManagement.GetAllInternalCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, customerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.IRiskType == 0)
                        risk = "High";
                    else if (complianceInfo.IRiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.IRiskType == 2)
                        risk = "Low";
                    else if (complianceInfo.IRiskType == 3)
                        risk = "Critical";

                    string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency));
                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.IComplianceTypeName,
                        complianceInfo.IComplianceCategoryName,
                        complianceInfo.IShortDescription,
                        complianceInfo.IShortForm,
                        complianceInfo.IComplianceType,
                        complianceInfo.IUploadDocument,
                        complianceInfo.IRequiredFrom,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int) complianceInfo.IFrequency : -1)),
                        Risk = risk,
                        complianceInfo.ISubComplianceType,
                        complianceInfo.CreatedOn,
                        complianceInfo.CheckListTypeID
                    });
                }
                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlChklstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbReminderType.SelectedValue = "0";
            rbReminderType.Enabled = false;
            divOnetimeDate.Visible = true;
            if (ddlChklstType.SelectedValue == "0")//one time
            {

                divOnetimeDate.Visible = true;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = false;
                divNonEvents.Visible = false;
                //divNonComplianceType.Visible = false;


                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = false;

            }
            else if (ddlChklstType.SelectedValue.Equals("1"))//function based
            {

                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = false;

                divFunctionBased.Visible = true;
                divNonEvents.Visible = true;
                divFrequency.Visible = true;
                vivDueDate.Visible = true;
                divTimebasedTypes.Visible = false;
                divComplianceDueDays.Visible = false;
                //divNonComplianceType.Visible = false;



            }
            else if (ddlChklstType.SelectedValue.Equals("2"))//time based
            {

                divFunctionBased.Visible = true;
                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                //divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;
            }
            else
            {
                divFunctionBased.Visible = true;
                divOnetimeDate.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                //divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;

            }


        }

        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private void BindIType()

        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlIComplinceType.DataTextField = "Name";
                ddlIComplinceType.DataValueField = "ID";

                ddlIComplinceType.DataSource = ComplianceTypeManagement.GetAllNVP(Convert.ToInt32(customerID));   //UserManagement.GetByID(AuthenticationHelper.UserID).
                ddlIComplinceType.DataBind();

                ddlIComplinceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool ViewSchedule(object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {

                if (Convert.ToString(frequency) == "Daily" || Convert.ToString(frequency) == "Weekly")
                {
                    return false;
                }
                else if(Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1 || Convert.ToInt32(CheckListTypeID) == 2)
                        {
                            if (frequency != null)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else if (frequency != null)
                     return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {


                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsInternalComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Internal Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetInternalComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbReminderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    divForCustome.Visible = true;

                }
                else
                {
                    divForCustome.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtnlstCompOcc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbtnlstCompOcc.SelectedValue.Equals("0"))
                {
                    divCompType.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divOnetimeDate.Visible = true;
                    //rdodomiantype.Items.Remove(rdodomiantype.Items.FindByValue("1"));
                    //rbReminderType.Items.Remove(rbReminderType.Items.FindByValue("0"));
                    rbReminderType.Items[0].Enabled = false;
                    rbReminderType.SelectedValue = "1";
                    divForCustome.Visible = true;
                }
                else
                {
                    divCompType.Visible = true;
                    divTimebasedTypes.Visible = true;
                    divFunctionBased.Visible = true;
                    divOnetimeDate.Visible = false;
                    //rbReminderType.Items.Add(rbReminderType.Items.FindByValue("0"));
                    rbReminderType.Items[0].Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        //protected void tbxOnetimeduedate_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        if (!string.IsNullOrEmpty(tbxOnetimeduedate.Text.Trim()))
        //        {
        //            date = DateTime.ParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

        //        }


        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }

        //}
    }
}