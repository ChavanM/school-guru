﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="Edit_InternalComplianceScheduleOn.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.Edit_InternalComplianceScheduleOn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>

    <style type="text/css">
        .td1 {
            width: 25%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 25%;
        }

        .td4 {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="70%">
                    <tr>
                        <td class="td1" align="left">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;">
                                Select Location:
                            </label>
                        </td>
                        <td class="td2" align="left">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" runat="server">
                            <br />
                            <br />
                            <asp:Panel ID="Panel1" Width="100%" Height="420px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" 
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" 
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" 
                                    OnRowCommand="grdComplianceRoleMatrix_RowCommand" 
                                    Font-Size="12px" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="Compliance Type" SortExpression="ComplianceType">
                                            <ItemTemplate>                                               
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceType" runat="server" Text='<%# Eval("ComplianceType")%>'></asp:Label>
                                                       <asp:Label ID="lblInternalComplianceID" runat="server" Visible="false" Text='<%# Eval("InternalComplianceID")%>'></asp:Label>
                                                       <asp:Label ID="lblInternalComplianceInstanceID" runat="server" Visible="false" Text='<%# Eval("InternalComplianceInstanceID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Compliance Category" SortExpression="CompliancesCategory" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCompliancesCategory" runat="server" Text='<%# Eval("CompliancesCategory") %>' ToolTip='<%# Eval("CompliancesCategory") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Location" SortExpression="CustomerBranch">
                                            <ItemTemplate>                                               
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="CustomerBranch" runat="server" Text='<%# Eval("CustomerBranch")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="ShortDescription" SortExpression="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="StartDate" SortExpression="StartDate" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                             <asp:TemplateField HeaderText="EffectiveDate" SortExpression="EffectiveDate" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblEffectiveDate" runat="server" Text='<%# Convert.ToDateTime(Eval("EffectiveDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_SCHEDULE" CommandArgument='<%# Eval("InternalComplianceID") + "," + Eval("InternalComplianceInstanceID") %>'><img src="../Images/edit_icon.png" alt="Edit Schedule" title="Edit Compliance" /></asp:LinkButton>
                                         </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divInternalComplianceDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" OnLoad="upComplianceDetails_Load">   
            <ContentTemplate>
                <asp:Panel runat="server" HorizontalAlign="Center" ScrollBars="Auto"> 
                 <asp:GridView runat="server" ID="grdSchedule" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" 
                                    Font-Size="12px" DataKeyNames="ID"  OnRowUpdating="grdSchedule_RowUpdating">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ScheduledOn" SortExpression="Sections">
                                            <ItemTemplate>                                               
                                                <div style="overflow: hidden; text-overflow: ellipsis; width: 200px; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ForMonth" SortExpression="ForMonth" >
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:100px;">
                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Select" SortExpression="Users">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                       <asp:CheckBox ID="chkSchedule" Width="100px" runat="server">
                                                    </asp:CheckBox>
                                                </div>
                                            </ItemTemplate>
                                           <%-- <EditItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:CheckBox ID="chkSchedule" Width="100px" runat="server">
                                                    </asp:CheckBox>
                                                </div>
                                            </EditItemTemplate>--%>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="" ShowHeader="false">
                                            <ItemTemplate >
                                                <asp:LinkButton ID="btnedit" runat="server"  CommandName="Update" Text="Save"></asp:LinkButton>
                                            </ItemTemplate>
                                          <%--  <EditItemTemplate>
                                                <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>--%>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

      <script type="text/javascript">
        $(function () {
            $('#divInternalComplianceDetailsDialog').dialog({
                height: 500,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Compliance Schedule Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

        });
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
