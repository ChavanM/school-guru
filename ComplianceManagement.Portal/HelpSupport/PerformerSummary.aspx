﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    

     <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
     <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>

    <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
    .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
    .img1{
        border:ridge;
        border-color:lightgrey;
    }
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
   width:1170px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>  
</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                        <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul><br/><br/>
           </div>

          <div class="col-sm-9 MainContainer">
              <div>
                
                     <h3> Performer Summary</h3>
                    <p style="font-size:14px;">   At a glance you can view the status of compliance assigned to you. You can also collapse and hide this section.
                  </p>
              </div>
      <br />
     <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	    	<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						 <b>Upcoming</b>
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<p  style="font-size:14px;"> All Statutory and Internal compliances having due date in next 30 days will be shown under Upcoming Compliances.</p><br /><br />
                         <img class="img1" style="width: 700px;margin-left:70px; height: 250px" src="../ImagesHelpCenter/Performer_Upcoming_1_Screenshot.png" /><br /><br /><br />
                            <p>  <b>Question  -</b>  How to perform the upcoming compliances? <br />                          
                              <b>Answer –</b> Following process will help you perform compliance-  </p>
                            <ul >
                                <li style="list-style-type:decimal"> Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                <li style="list-style-type:decimal">You will be redirected to list of compliances page; all upcoming compliance will be featured.</li>
                                <li style="list-style-type:decimal">You can set filter for Risk, Status and Location and view specific compliance or list of compliance.</li>
                                <li style="list-style-type:decimal">Also, you can do Advance search to view particular compliance or list of compliance.</li>
                                <li style="list-style-type:decimal">To perform compliance, you need to click on Action icon.</li> <br /><br />
                                <img class="img1" style="width: 700px;margin-left:30px;height: 450px" src="../perfreviewerUpdatedscreens/Performer_Upcoming_2_Screenshot.png" />
                                 <%--<img class="img1" style="width: 700px;margin-left:50px;height: 450px" src="../ImagesHelpCenter/Performer_Upcoming_2_Screenshot.png" /><br /><br />
                               --%> <li style="list-style-type:decimal"> When clicked on Action icon you will view a window that holds Act Details and its documents, Compliance details, Additional details, <br />
                                    and to update the compliance status using following steps,<br /><br />
                                <img class="img1" style="width: 720px;margin-left:50px; height: 350px" src="../ImagesHelpCenter/Performer_Upcoming_3_Screenshot.png" />  <br /><br />
                                    <ul >
                                        <li style="list-style-type:lower-alpha">Select Status from the following options:
                                            <ul >
                                                <li style="list-style-type:lower-roman">Complied but pending review</li>
                                                <li style="list-style-type:lower-roman">Complied delayed but pending for review</li>
                                                <li style="list-style-type:lower-roman">In Progress</li>
                                            </ul>
                                        </li>
                                        <li style="list-style-type:lower-alpha">Upload Compliance Document</li>
                                        <li style="list-style-type:lower-alpha">Upload Working file, its optional</li>
                                        <li style="list-style-type:lower-alpha">Enter Date</li>
                                        <li style="list-style-type:lower-alpha"> Click Submit</li>
                                        <li style="list-style-type:lower-alpha">Audit Log</li>
                                        
                                    </ul>
                                </li>
                            </ul>
                     
                     &nbsp;&nbsp; 7.  All asterisk marked fields are mandatory to fill, others are optional.
   
         
                      
               </div>

			</div>

	</div>
         
			<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						 <b>Overdue</b>
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
					 
                     <img class="img1" style="width: 600px;margin-left:50px;  height: 200px" src="../ImagesHelpCenter/Performer_Overdue_1_Screenshot.png" /><br /><br />
                            <p style="color: #666666;">
                            <b>Question  -</b>  How can I perform overdue compliances?    <br />                       
                              <b>Answer –</b> Following process will help you,    <br /> </p> 
                                <ul >
                                    <li style="list-style-type:decimal">Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                    <li style="list-style-type:decimal">You will be redirected to list of compliances. And to perform compliance you need to click on Action icon.
                                       </li><br /><br />
                                    <img class="img1" style="width: 700px;margin-left:50px;height: 450px" src="../perfreviewerUpdatedscreens/Performer_Upcoming_2_Screenshot.png" />

                                    <li style="list-style-type:decimal">
                                        When clicked on Action icon you will view a window that will hold Act details, Compliance details, Additional details, and Update compliance <br />
                                        where you need update the compliance status using following steps,<br /><br />
                                         <img class="img1" style="width: 720px;margin-left:50px; height: 350px" src="../ImagesHelpCenter/Performer_Upcoming_3_Screenshot.png" />  <br /><br />
                               
                                        <ul >
                                            <li style="list-style-type:lower-alpha">Select Status from the following options:
                                                <ul >
                                                    <li style="list-style-type:lower-roman">Complied but pending review</li>
                                                    <li style="list-style-type:lower-roman"> Complied delayed but pending for review</li>
                                                    <li style="list-style-type:lower-roman">In Progress</li>
                                                </ul>
                                            </li>
                                            <li style="list-style-type:lower-alpha">Upload Compliance Document</li>
                                            <li style="list-style-type:lower-alpha">Upload Working file, its optional</li>
                                            <li style="list-style-type:lower-alpha">Enter Date</li>
                                            <li style="list-style-type:lower-alpha">Enter Remarks</li>
                                            <li style="list-style-type:lower-alpha">Click Submit</li>
                                        </ul>
                                    </li>
                                    <li style="list-style-type:decimal">All asterisk marked fields are mandatory to fill, others are optional.</li>
                                </ul>                                                                                         
                                                                                                         
                              
                      
               </div>

			</div>

	</div>

            <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Checklist</b>
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
                    All Statutory and Internal ongoing compliances till present day will be shown under Checklist.<br /><br />
                                <img class="img1" style="width: 600px;margin-left:80px; height: 200px" src="../ImagesHelpCenter/Performer_Checklist_1_Screenshot.png" /><br /><br />
                              <p><b>Question –</b> How to perform the Checklist compliances? <br />    
                                <b>Answer-</b> Following is the process to perform Checklist compliance, <br /></p>
                                <ul >
                                    <li style="list-style-type:decimal">Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                    <li style="list-style-type:decimal">You will be redirected to list of compliances. You can set filter to view specific compliance or list of compliances.<br />
                                         The Advance Search helps you search more specific compliance or list of compliances.</li>
                                    <li style="list-style-type:decimal"> And to perform checklist compliance you need to click on check box and hit submit button, you can also select multiple check box to perform multiple compliances.</li><br /><br />
                                   <%-- <img class="img1" style="width: 700px;margin-left:50px;  height: 300px" src="../ImagesHelpCenter/Performer_Checklist_2_Screenshot.png" />--%>
                                    <img  class="img1" style="width: 700px;margin-left:50px;  height: 300px" src="../perfreviewerUpdatedscreens/Performer_Checklist_2_Screenshot.png" />
                                    <br /><br /><br />
                                    <li style="list-style-type:decimal">To view compliance details, click on Details icon.</li><br /><br />
                                   <%-- <img class="img1" style="width: 700px;margin-left:50px; height: 300px" src="../ImagesHelpCenter/Performer_Checklist_3_Screenshot.png" />--%>
                                    <img class="img1" style="width: 700px;margin-left:50px; height: 300px" src="../perfreviewerUpdatedscreens/Performer_Checklist_3_Screenshot.png" />  <br /><br /><br />
                                    <li style="list-style-type:decimal">You can also write remark and upload documents for compliance and submit it.</li> <br /><br /><br />
                                      <%--<img class="img1" style="width: 700px;margin-left:50px; height: 300px"src="../ImagesHelpCenter/Performer_Checklist_4_Screenshot.png" />--%>
                                    <img class="img1" style="width: 700px;margin-left:50px; height: 300px" src="../perfreviewerUpdatedscreens/Performer_Checklist_4_Screenshot.png" />   <br /><br />

                                 
                                </ul>
                                
                    
						<b>Not Applicable Checklist</b><br />
			
                    You can select Not Applicable as a Status for Checklist compliance.<br />
                    <b>How to use?</b><br />
                                         <ul>
                                            <li style="list-style-type:lower-alpha">Click on the count under Checklist (Statutory and Internal) and Performer will be redirected to My Workspace/Checklist</li>

                                            <li style="list-style-type:lower-alpha">To opt multiple compliance as Not Applicable- Tick the checkbox under Action column and Click on Not Applicable button</li><br /><br />
                                             <img class="img1" style="width: 720px;margin-left:50px; height: 350px" src="../perfreviewerUpdatedscreens/Performer_Checklist_5_Screenshot.png" /><br /><br />
                                            <li style="list-style-type:lower-alpha"> To opt Not Applicable for particular compliance, Click on Action icon under Detail column, and in appeared window, Click on Not Applicable the button. </li><br />
                                             <img class="img1" style="width: 720px;margin-left:50px; height: 350px" src="../perfreviewerUpdatedscreens/Performer_Checklist_6_Screenshot.png" />
                                        </ul>
                                  




					 </div>
                </div>

            </div>

            <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Rejected</b>
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">
                    All Statutory and Internal compliances rejected by Reviewer will be shown under Rejected.<br /><br />
                                <img class="img1" style="width: 700px;margin-left:50px;  height: 200px" src="../ImagesHelpCenter/Performer_Rejected_1_Screenshot.png" /><br /><br />
                                 
                                <p>
                                   <b>Question - </b> How to perform  can perform the compliance that was rejected by Reviewer?     <br />   
                                    <b>Answer- </b>Following is the process to perform rejected compliance, <br /> </p>
                                <ul >
                                    <li style="list-style-type:decimal">Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                    <li style="list-style-type:decimal">You will be redirected to list of compliances. And to perform compliance you need to click on Action icon.</li><br />
                                     <%--<img class="img1" style="width: 700px;margin-left:30px; height: 250px" src="../ImagesHelpCenter/Performer_Upcoming_1_Screenshot.png" />--%><br /><br />
                                     <img class="img1" style="width: 700px;margin-left:50px;height: 450px" src="../perfreviewerUpdatedscreens/Performer_Upcoming_2_Screenshot.png" />
                                    <br /><br />
                                     <li style="list-style-type:decimal">When clicked on Action icon you will view the Compliance details, Additional details, and update the compliance status using following steps,
                                        <ul >
                                            <li style="list-style-type:lower-alpha">Select Status from the following options:
                                                <ul >
                                                    <li style="list-style-type:lower-roman">Complied but pending review</li>
                                                    <li style="list-style-type:lower-roman">Complied delayed but pending for review</li>
                                                    <li style="list-style-type:lower-roman">In Progress</li>
                                                </ul>
                                            </li>
                                            <li style="list-style-type:lower-alpha">Upload Compliance Document</li>
                                            <li style="list-style-type:lower-alpha">Upload Working file, its optional</li>
                                            <li style="list-style-type:lower-alpha">Enter Date</li>
                                            <li style="list-style-type:lower-alpha">Enter Remarks</li>
                                            <li style="list-style-type:lower-alpha">Click Submit</li>
                                        </ul>
                                    </li>
                                    <li style="list-style-type:decimal">All asterisk marked fields are mandatory to fill, others are optional.</li>
                                </ul>
					 </div>
                </div>
             </div>

            <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFive">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Pending For Review</b>
					</a>
				</h4>
			</div>
			<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
				<div class="panel-body">
                    All Statutory and Internal compliances performed by Performer but not yet reviewed by Reviewer will be shown under Pending for Review.  <br /><br />
                                <img class="img1" style="width: 700px;margin-left:50px;  height: 250px" src="../ImagesHelpCenter/Performer_Pending%20for%20review_1_Screenshot.png" /><br /><br />
                            <p style="color: #666666;"></p>
                                <p><b>Question-</b> Can I view the compliance I have performed that are not reviewed?  <br />
                                <b>Answer-</b> Following process will help you view pending for review compliance.  <br />
                                    <ul >
                                        <li style="list-style-type:decimal">Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                        <li style="list-style-type:decimal">You will be redirected to compliance or list of compliances that are not yet reviewed by Reviewer, you can set filter and Advanced filter to view specific compliance or list of compliance.</li>
                                        <img class="img1" style="width: 700px;margin-left:50px;height: 450px" src="../perfreviewerUpdatedscreens/Performer_Pending%20for%20review_2_Screenshot.png" />
                                    </ul>
					 </div>
                </div>
             </div>

         
	</div>
	
	
</div>
</div>

 </div><!-- container -->
    </form>

     

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
