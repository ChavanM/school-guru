﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
   <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>


     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
     .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1170px;
 
  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>

      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>   
</head>
<body>
    <form id="form1" runat="server">
   <div class="container">
      
        <div class="row content"> 

        <div class="col-sm-3 sidenav">

            <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>Performer Workspace</b></a>
                        <li style="font-size:14px" ><a href="../HelpSupport/Reviewerworkspace.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>Reviewer Workspace</b></a>
                        <%--   
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>--%>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul>
        </div>

       
           
          <div class="col-sm-9 MainContainer" style="line-height:25px;">
              <br /> <b> My Workspace –</b> Once you click on My Workspace, you will see bifurcation as Compliance and License.<br />
              <img  class="img1" style="width: 700px;margin-left:50px;height: 350px" src="../ImagesHelpCenter/Performer_My%20Workspace_1_Screenshot.png" /><br />
           
            <br />
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                   <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingZero">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
						<i class="short-full glyphicon glyphicon-plus"></i>
					   <b><li style="list-style-type:disc">Compliance can be performed using following steps,</li></b>
					</a>
				</h4>
			</div>
	    		<div id="collapseZero" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingZero">
				<div class="panel-body">
                            <%-- <img class="img1" style="width: 700px;margin-left:50px;height: 350px" src="../ImagesHelpCenter/Performer_My%20Workspace_2_Compliance_Screenshot.png" /><br /><br />
                          --%> <ul >
                               <li style="list-style-type:decimal">Click on My Workspace tab of menu and select Compliance option, and you will be redirected to page that features all the compliances assigned.</li>
                               <li style="list-style-type:decimal">You can search for compliance or list of compliance by setting Filter and to search more specific compliance or list of compliance you can use Advanced Filter.</li>
                               <li style="list-style-type:decimal">To perform a compliance, click on Action icon. </li>

                               <%--<img class="img1" style="width: 700px;margin-left:70px; height: 250px" src="../ImagesHelpCenter/Performer_Upcoming_1_Screenshot.png" /><br /><br />
                              --%> <img class="img1" style="width: 700px;margin-left:50px;height: 450px" src="../perfreviewerUpdatedscreens/Performer_Upcoming_2_Screenshot.png" /><br />
                                     

                               <li style="list-style-type:decimal">When clicked on Action icon you will view the Compliance details, Additional details, and update the compliance status using following steps,
                                   <ul >
                                       <li style="list-style-type:lower-alpha">Select Status from the following options:
                                           <ul >
                                               <li style="list-style-type:lower-roman"> Complied but pending review</li>
                                               <li style="list-style-type:lower-roman">Complied delayed but pending for review</li>
                                               <li style="list-style-type:lower-roman">In Progress</li>
                                           </ul>
                                       </li>
                                       <li style="list-style-type:lower-alpha">Upload Compliance Document</li>
                                       <li style="list-style-type:lower-alpha"> Upload Working file, its optional</li>
                                       <li style="list-style-type:lower-alpha">Enter Date</li>
                                       <li style="list-style-type:lower-alpha">Enter Remarks</li>
                                       <li style="list-style-type:lower-alpha">Click Submit</li>
                                       <li style="list-style-type:lower-alpha">Audit log</li>
                                  </ul>
                               </li>
                               <li style="list-style-type:decimal">All asterisk marked fields are mandatory to fill, others are optional.</li>
                              <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../perfreviewerUpdatedscreens/Performer_My%20Workspace_3_More%20links_Screenshot.png" />
                               <%--<img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Performer_My%20Workspace_3_More%20links_Screenshot.png" /><br /><br />
                        --%>   </ul>
                       
			 </div>		 
        </div>
                </div>
                         <br />
             <p>On My Workspace you will also find More link dropdown, with the help of dropdown you can use features – Update penalty, Revise compliance and Task. And in following manner you can use these features,</p>
                     <br />        
                   <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b>Update Penalty</b>
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
                     Performer can update the Interest and Penalty amount for a compliance. And this amount will be reviewed and confirmed by Reviewer.  Following is the process to update penalty,  
                     <ul >
                         <li style="list-style-type:lower-roman">Click on More link dropdown and select Update Penalty.</li>
                         <li style="list-style-type:lower-roman">You will be redirected to page that features list of compliance or compliance, you can set filter of Risk type and Location to search for specific compliance or list of compliance.</li><br />
                         <%--<img class="img1" style="width: 750px;margin-left:5px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workscpace_4_Update%20Penalty_Screenshot.png" />
                        --%> <img  style="width: 750px;margin-left:5px; height: 450px" src="../perfreviewerUpdatedscreens/Performer_My%20Workspace_2_Compliance_Screenshot.png" />   <br /><br />  
                         <li style="list-style-type:lower-roman"> For more specific search of compliance use Advanced Search.</li>
                         <li style="list-style-type:lower-roman">To update penalty, click on Action icon and fill in Interest, Penalty amount and save.</li><br />
                         <img style="width: 550px;margin-left:-5px; height: 280px" src="../perfreviewerUpdatedscreens/Performer_My%20Workspace_3_More%20links_Screenshot.png" />
                         <%--<img class="img1" style="width: 750px;margin-left:-5px; height: 580px" src="../ImagesHelpCenter/Performer_My%20Workscpace_5_Update%20Penalty_Screenshot.png" />  <br /><br />    
                     --%></ul>
					 </div>
                </div>
             </div>
              
                  <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Revise Compliance</b>
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">
                    If you want to revise the date and document for compliance that is already performed or yet to perform, then you can use this feature. Following is the process,<br /><br />
                       <img class="img1" style="width: 750px;margin-left:30px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workscpace_6_Revise%20Compliance_Screenshot.png" /><br /><br />
                              <ul >
                                  <li style="list-style-type:lower-roman">Click on More links dropdown and select Revise compliance option, you will be redirected to page that holds compliance list.</li>
                                  <li style="list-style-type:lower-roman">To search the particular compliance set filter of Risk, Status and Location. For more specific search of compliance use Advanced Search.</li>
                                  <li style="list-style-type:lower-roman">To revise the date and document, click on Action icon and enter values in Revise date, add remark and upload document and save. You can also upload working files.</li>
                                  <li style="list-style-type:lower-roman">The asterisk marked fields are compulsory to fill.  </li><br /> <br /> 
                                  <img class="img1" style="width: 720px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workscpace_7_Revise%20Compliance_Screenshot.png" /><br /> <br /> 
 
                              </ul>
					 </div>
                </div>
             </div>
              
                  <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFive">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b>My Leave</b>
					</a>
				</h4>
			</div>
			<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
				<div class="panel-body">
                    <p>You can assign compliance to another Performer for the leave duration. All the compliances assigned to you till date will be reflected in new Performer dashboard, only for the leave duration. And once your leave duration is complete, then on that particular date all the assigned compliance will be visible on your dashboard.  In following manner, you can use this feature,</p>
                             <ul >
                                 <li style="list-style-type:lower-roman">Go to My Workspace and click on Compliance option  </li>
                                 <li style="list-style-type:lower-roman"> After getting redirected to My Workspace page, click on ‘More Links’ dropdown and select My Leave.</li>
                                 <li style="list-style-type:lower-roman">Once clicked on My Leave, you will be redirected to My leave page. Here you can add new leave and view previous leave record.</li>
                                 <li style="list-style-type:lower-roman">Add New – This feature allows you to add leave in AVACOM.
                                     <ul >
                                         <li style="list-style-type:decimal">To add leave, click on Add New button</li><br /><br />
                                         <img class="img1" style="width: 700px;margin-left:5px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Workspace_8_My%20Leave_Screenshot.png" /><br /><br />
                                         <li style="list-style-type:decimal">A pop-up will appear, in pop-up you have to select Start date and End date of leave, and from dropdown select New User Performer (one you want to assign)</li>
                                         <li style="list-style-type:decimal">Once done click on save button</li><br /><br />
                                         <img class="img1" style="width: 700px;margin-left:5px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Workspace_9_My%20leave_Screenshot.png" /><br /><br />
                                         <li style="list-style-type:decimal">In case you are unable to find the Performer in given list, then internally ask your Admin to add Performer </li>
   
                                     </ul>
                                 </li>
                                 <li style="list-style-type:lower-roman">Once you add leave in system, a notification email will be sent to Reviewer and new Performer.</li>
                                 <li style="list-style-type:lower-roman">In case Reviewer want to change the Performer, he can reach Company Admin to reassign it to another Performer.</li>
                             </ul>
                        
					 </div>
                </div>
             </div>    


                   <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSix">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b>Task</b>
					</a>
				</h4>
			</div>
			<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
				<div class="panel-body">
                     You can Complete the Task and also, you can Create the Task and its Sub Task in following manner,<br /><br />
                      <ul >
                          <li style="list-style-type:lower-roman">To Complete Task click on More link dropdown and select Task, you will be redirected to page that holds previous task or list of tasks.</li><br /> <br />
                          <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_10_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">Now, click on Action icon and window will appear, here fill in mandatory (asterisk marked) details under Update Task Status and submit. On this window you can also view Task details and Audit log.</li><br /><br />
                          <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_11_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">To Create new Task, click on More link and select Task Details, after getting redirected to Task details page, click on Add new button.</li><br /><br />
                         <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_12_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">A window will appear, you need to fill in details and save. In this way new Task will be created.</li><br /><br />
                          <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_13_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">On same Task Details page, you can create Sub task, and to create click on sub-task link and you will be redirected to page Sub task Details, here click on Add New button.</li><br /><br />
                           <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_14_Task_Screenshot.png" /><br /><br />
                           <img class="img1" style="width: 750px;margin-left:10px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workspace_15_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">A window will appear, fill in details and sub-task will be created.</li> <br /><br />
                           <img class="img1" style="margin-left:10px;width: 750px"  src="../ImagesHelpCenter/Performer_My%20Workspace_16_Task_Screenshot.png" /><br /><br />
                          <li style="list-style-type:lower-roman">You can also edit and delete task. Click on Edit icon to edit task and click on Delete icon to delete Task.</li>
                          <li style="list-style-type:lower-roman">To view assignment, click on Show Assignment icon.</li>
                       
                           </ul>
                       
					 </div>
                </div>
             </div>
<br />

   <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThreenew">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThreenew" aria-expanded="true" aria-controls="collapseThreenew">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b><li style="list-style-type:disc">License</li></b>
					</a>
				</h4>
			</div>
			<div id="collapseThreenew" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThreenew">
				<div class="panel-body">
                     <p> You can review the License as per following process,</p>
                              <ul >
                              <li style="list-style-type:decimal">Select License in My Workspace, you will be redirected to page that holds License to be reviewed.</li>
                              <li style="list-style-type:decimal">You can set filter to search for specific License or list of License.</li><br />
                                  <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workspace_17_License_Screenshot.png" /><br /><br />
                                  <li>To review License, click on Action icon. You will be redirected to window that will hold Compliance and License details, License documents to be reviewed, Audit log and section to update status. In Update License Status section you have to add remark and Approve/Reject the status.</li><br />
                                 <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workspace_18_License_Screenshot.png" />
                             </ul>
					 </div>
                </div>
             </div>




                </div>

               


         </div>
   </div>
</div>

    </form>

    

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
