﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
      <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
     <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;
      
    }
    .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
 
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  
  </style>
    
    <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
 width:1170px;

}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}
   img {
    border: 1px solid;
}

* {
  box-sizing: border-box;
}

    </style>

  
  <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		/*color: #fff;*/
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color:black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>   
</head>
<body >
    <form id="form1" runat="server">
      <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul> </div>

          <div class="col-sm-9 MainContainer"  style="line-height:20px;">
     

              <h3>Reviewer Summary</h3>
              <br />
              <p style="font-size:14px"> At a glance you can view the status of compliance assigned to you. You can also collapse and hide this section.
                        </p>		

      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
           <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b> Due but Not Submitted</b>
					</a>
				</h4>
			</div>
			 <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
                  All Statutory and Internal compliances crossed the due date but not yet performed by Performer will be shown under Due but Not Submitted.
                                   <p> <b>Question-</b> How to view compliances that are Due but not submitted by Performer?   <br />   
                                   <b> Answer –</b> In following manner you can view the compliances that are not performed by Performer,<br /> </p>
                                   <ul >
                                       <li style="list-style-type:lower-roman">Click On Count (Showing Number of Compliances-Statutory or Internal).</li><br /><br />
                                       <img class="img1" style="width: 700px;margin-left:30px; height: 250px" src="../ImagesHelpCenter/Reviewer%20Summary_Due%20but%20not%20submitted_1_Screenshot.png" /><br /><br />
                                       <li style="list-style-type:lower-roman">You will be redirected to list of compliances page, all compliance falling under Due but Not Submitted will be featured.</li>
                                       <li style="list-style-type:lower-roman">You can set filter for Compliance type, Risk, Status and Location and view specific compliance or list of compliance.</li>
                                       <li style="list-style-type:lower-roman">Also, you can do Advance search and view more particular compliance or list of compliance. </li><br />
                                       <img class="img1" style="width: 700px;margin-left:10px; height: 350px" src="../perfreviewerUpdatedscreens/Reviewer%20Summary_Due%20but%20not%20submitted_2_Screenshot.png" />
                                       <%-- <img class="img1" style="width: 700px;margin-left:10px; height: 350px" src="../ImagesHelpCenter/Reviewer%20Summary_Due%20but%20not%20submitted_2_Screenshot.png" />--%><br /><br />
                                       <li style="list-style-type:lower-roman">As a Reviewer you do not have any role to complete the compliance. Unless the Performer completes the compliance, you cannot review the compliance. This tab just indicates the number of compliances pending from the point of Performer.</li>
                                   </ul>
                     </div>
                </div>
         </div> <br />

          <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b> Pending for review </b>
					</a>
				</h4>
			</div>
			 <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
               
                                    <p> All Statutory and Internal compliances which were performed will be shown under Pending for review.	<br />
                                     <b>Question-</b> How to review the Pending for review compliance?     <br />
                                    <b>Answer-</b> You can view pending for review compliance is following manner,<br /> 
                                        <ul >
                                            <li style="list-style-type:lower-roman">Click On Count (Showing Number of Compliances-Statutory or Internal).</li><br />
                                            <img class="img1" style="width: 700px;margin-left:30px; height: 250px" src="../ImagesHelpCenter/Reviewer%20Summary_Pending%20for%20Review_1_Screenshot.png" /><br /><br />
                                            <li style="list-style-type:lower-roman">You will be redirected to list of compliances. You can set filter for desired search and also use Advanced search in case of specific search.   </li>
                                            <li style="list-style-type:lower-roman">To review the compliance, you need to click on Action icon.</li><br />
                                            <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer%20Summary_%20Checklist_2_Screenshot.png" />
                                           <%-- <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Reviewer%20Summary_Pending%20for%20Review_2_Screenshot.png" />   <br /><br />
                                           --%> <li style="list-style-type:lower-roman">When clicked on Action icon you will view the window that holds Compliance details, Additional details and Audit logs. </li><br />
                                         <%--   <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Reviewer%20Summary_Pending%20for%20Review_3_Screenshot.png" />--%>
                                            <%--<img src="../perfreviewerUpdatedscreens/Reviewer%20Summary_%20Checklist_3_Screenshot.png" />--%>
                                            <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Reviewer%20Summary_Pending%20for%20Review_3_Screenshot.png" />     
                                            <br /><br />
                                            <li style="list-style-type:lower-roman">Reviewer need to use following process to review the compliance, 
                                                <ul >
                                                    <li style="list-style-type:lower-alpha">Review compliance document- Reviewer need to download /view compliance document version and based on it, you will approve or reject the Performer’s work. Without download/view the document, reviewer will be unable to Approve or Reject the compliance. <br />
                                                        Once done with review you need to follow below steps  </li>
                                                    <li style="list-style-type:lower-alpha"> Select Status from the following options: 
                                                        <ul >
                                                            <li style="list-style-type:lower-roman"> Closed-Delayed- if compliance performed by performer after due date. Follow below steps 
                                                                <ul >
                                                                   <li style="list-style-type:decimal">If the value for penalty or interest is known, Enter the Interest and penalty amount, incase not known, check the<br />
                                                                        ‘Value is not known at this moment option’.</li>
                                                                   <li style="list-style-type:decimal"> Enter date </li>
                                                                   <li style="list-style-type:decimal"> Enter Remark </li> 
                                                                </ul>
                                                             </li>
                                                            <li style="list-style-type:lower-roman">Closed-Timely- if compliance performed by Performer within due date. Follow below steps  
                                                                <ul>
                                                                    <li> Enter date</li>
                                                                    <li>Enter remark </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="list-style-type:lower-alpha"> To approve the compliance click Approve button </li>
                                                    <li style="list-style-type:lower-alpha">To reject the compliance click Reject button</li>
                                                   <li style="list-style-type:lower-alpha">You can close in case you are not planning to review the compliance. </li>
                                                </ul>
                                            </li>
                                            <li style="list-style-type:lower-roman"> All asterisk marked fields are mandatory to fill and also reviewing the document is mandatory.</li>
                                        </ul>
                                 
                     </div>
                </div>
         </div>  <br />

          <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						   <b>Checklist </b>
					</a>
				</h4>
			</div>
			 <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				   <div class="panel-body">
                      
                               <p>All Statutory and Internal Checklist compliances will be shown under Checklist.<br />
                               <b>Question-</b> How to perform the Checklist compliances? <br />   
                               <b>Answer-</b> In following manner you can review checklist compliance, </p>
                                   <ul >
                                       <li style="list-style-type:decimal">Click On Count (Showing Number of Compliances-Statutory or Internal).</li><br />
                                       <img class="img1" style="width: 700px;margin-left:30px; height: 250px" src="../ImagesHelpCenter/Reviewer%20Summary_%20Checklist_1_Screenshot.png" /><br /><br />
                                       <li style="list-style-type:decimal">You will be redirected to list of compliances. And to review compliance you need to click on Overview icon. Reviewer has no role with respect to review of compliances. He can just have an overview of the checklists assigned and in case any document is attached to the checklist,he can review and download the same.</li><br />
                                       <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer%20Summary_%20Checklist_2_Screenshot.png" />
                                        <%--<img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Reviewer%20Summary_%20Checklist_2_Screenshot.png" /><br /><br />
                                      --%>
                                        <li style="list-style-type:decimal">A window will appear holding the details of compliance, you will see different tabs Summary, Details, Historical Documents, Updates, Audit logs. And two sections - Current Documents and Comments, following are the use,
                                           <ul >
                                               <li style="list-style-type:lower-alpha"> Compliance Overview- This will feature basic details of compliance. </li>
                                               <li style="list-style-type:lower-alpha"> Documents- All the documents related to compliance will be featured. </li>
                                               <li style="list-style-type:lower-alpha">Audits- The details about the audit, done for the compliance will be featured in this section. </li><br />
                                                
                                              <img class="img1" style="width: 700px;margin-left:10px; height: 400px" src="../ImagesHelpCenter/Reviewer%20Summary_%20Checklist_3_Screenshot.png" /><br /><br />
                                           <b>Not Applicable Checklist-</b> Reviewer can view Not applicable Checklist.<br />

                                      How to view not applicable checklist?<br />

                                      1.Go to My Workspace/Checklist, select Not applicable status in filter.<br />
                                     <img class="img1" style="width: 700px;margin-left:10px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer%20Summary_%20Checklist_4_Screenshot.png" /><br /><br />
                                      2. List of not applicable compliances will be populated below.<br />


                                           </ul>
                                       </li>

                                   </ul>
                   </div>
                </div>


          </div> <br />

         <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>  Rejected </b>
					</a>
				</h4>
			</div>
			 <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">
               
                                 
                                    <p> All Statutory and Internal compliances rejected by you will be shown under Rejected. In following manner, you can view the rejected compliance.</p>
                                   <ul >
                                       <li style="list-style-type:decimal"> Click On Count (Showing Number of Compliances-Statutory or Internal).</li>
                                       <li style="list-style-type:decimal">You will be redirected to list of compliances that you have rejected.</li><br /><br />
                                       <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Reviewer%20Summary_Rejected_1_Screenshot.png" /><br /><br />
                                   </ul>
                                 
                     </div>
                </div>
         </div>
      </div>
    </div>
</div>
      </div>
    </form>
    

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
