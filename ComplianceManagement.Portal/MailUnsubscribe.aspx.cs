﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Linq;
using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.UI;
using System.Collections.Generic;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class MailUnsubscribe : System.Web.UI.Page
    {
        protected static int UID;
        protected static string Date;
        public static string key = "A!9HHhi%XjjYY4YP2@Nob009X";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["auth"]))
                {
                    UID = Convert.ToInt32(Decrypt(Request.QueryString["auth"]));

                    User LoggedUser = UserManagement.GetByID(UID);
                    //User user = GetDetailUser(UID);
                    if (LoggedUser != null)
                    {
                        LabelUserName.Text = LoggedUser.FirstName + ' ' + LoggedUser.LastName;
                        
                        if (LoggedUser.ImagePath != null)
                        {
                            ProfilePic.Src = LoggedUser.ImagePath;                            
                        }
                        else
                        {
                            ProfilePic.Src = "../UserPhotos/DefaultImage.png";                            
                        }
                    }
                }
            }
        }
        public static string Decrypt(string cipher)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        public static User GetDetailUser(int UId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Users
                             where (row.ID == UId)
                               && row.IsActive == true
                               && row.IsDeleted == false
                             select row).FirstOrDefault();

                return data;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {


                Business.Data.EmailUnsubscription emailUnsubscription = new Business.Data.EmailUnsubscription()
                {
                    UserID = UID,
                    Type = "DailyUpdateNotification",
                    IsUnsubscribe = false,
                    CreatedDate = DateTime.Now,
                };
                if (!Business.DailyUpdateManagment.CheckUnsubscribeEntry(emailUnsubscription.UserID))
                {
                    if (Business.DailyUpdateManagment.EmailUnsubscribe(emailUnsubscription))
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "You been unsubscribe successfully";
                    }
                }
                else
                {
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "You have already unsubscribed";

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}