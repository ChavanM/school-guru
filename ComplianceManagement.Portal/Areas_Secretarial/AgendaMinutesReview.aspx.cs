﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class AgendaMinutesReview : System.Web.UI.Page
    {
        IAgendaMinutesReviewService objIAgendaMinutesReviewService = new AgendaMinutesReviewService();
        //public AgendaMinutesReview(IMeeting_Service objMeeting_Service)
        //{
        //    objIMeeting_Service = objMeeting_Service;
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if(Request.QueryString["mappingId"] != null && Request.QueryString["taskId"] != null && Request.QueryString["assignmentId"] != null)
                {
                    long mappingId = Convert.ToInt64(Request.QueryString["mappingId"]);
                    long taskId = Convert.ToInt64(Request.QueryString["taskId"]);
                    long assignmentId = Convert.ToInt64(Request.QueryString["assignmentId"]);
                    long agendaReviewId = Convert.ToInt64(Request.QueryString["agendaReviewId"]);
                    if (mappingId > 0)
                    {
                        var agendaDetails = objIAgendaMinutesReviewService.GetAgendaReviewDetails(mappingId, taskId, assignmentId, agendaReviewId);
                        if(agendaDetails!=null)
                        {
                            txtAgendaHeading.Text = agendaDetails.AgendaFormatHeading;
                            theEditor.Content = agendaDetails.AgendaFormat;
                            reResolutionFormat.Content = agendaDetails.ResolutionFormat;
                            ViewState["meetingAgendaMappingId"] = agendaDetails.MeetingAgendaMappingID;
                            ViewState["taskId"] = taskId;
                            ViewState["assignmentId"] = assignmentId;
                            ViewState["agendaReviewId"] = agendaReviewId;
                        }
                    }
                }

                bool canAcceptChanges = false;
                if (Request.QueryString["changesFlag"] != null)
                {
                    canAcceptChanges = Request.QueryString["changesFlag"] == "1" ? true : false;
                }
                ViewState["canAcceptChanges"] = canAcceptChanges ? "1" : "0";
                SetDefaultConfiguratorValues(canAcceptChanges);
            }
        }
        private void SetDefaultConfiguratorValues(bool canAcceptChanges)
        {
            //TrackChangesSettings settings = theEditor.TrackChangesSettings;

            theEditor.TrackChangesSettings.Author = AuthenticationHelper.User;
            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;

            theEditor.TrackChangesSettings.UserCssId = "reU1";
            theEditor.TrackChangesSettings.CanAcceptTrackChanges = canAcceptChanges;
            theEditor.EnableTrackChanges = true;

            reResolutionFormat.TrackChangesSettings.Author = AuthenticationHelper.User;
            reResolutionFormat.TrackChangesSettings.CanAcceptTrackChanges = canAcceptChanges;

            reResolutionFormat.TrackChangesSettings.UserCssId = "reU1";
            reResolutionFormat.TrackChangesSettings.CanAcceptTrackChanges = canAcceptChanges;
            reResolutionFormat.EnableTrackChanges = true;
        }

        protected void btnSaveAgendaDetails_Click(object sender, EventArgs e)
        {
            var obj = new AgendaReviewVM();
            var userId = AuthenticationHelper.UserID;

            obj.AgendaReviewId = Convert.ToInt64(ViewState["agendaReviewId"]);
            obj.TaskId = Convert.ToInt64(ViewState["taskId"]);
            obj.TaskAssignmentId = Convert.ToInt64(ViewState["assignmentId"]);
            obj.MeetingAgendaMappingID = Convert.ToInt64(ViewState["meetingAgendaMappingId"]);
            obj.AgendaFormat = theEditor.Content;
            obj.ResolutionFormat = reResolutionFormat.Content;

            bool canAcceptChanges = (Convert.ToString(ViewState["canAcceptChanges"]) == "1" ? true : false);
            var result = objIAgendaMinutesReviewService.SaveAgendaReviewDeails(obj, canAcceptChanges, userId);
            if(result != null)
            {
                divsuccess.Visible = false;
                diverror.Visible = false;
                if (result.Success)
                {
                    divsuccess.InnerHtml = result.Message;
                    divsuccess.Visible = true;
                    ViewState["agendaReviewId"] = result.AgendaReviewId;

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "refreshDocfun", "refreshDoc();", true);
                }
                else if (result.Error)
                {
                    diverror.InnerHtml = result.Message;
                    diverror.Visible = true;
                }

            }
        }
    }
}