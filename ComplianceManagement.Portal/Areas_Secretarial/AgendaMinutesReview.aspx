﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgendaMinutesReview.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.AgendaMinutesReview" validateRequest="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <!-- Bootstrap CSS -->
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <%--<link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />--%>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>    
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="~/NewCSS/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Newjs/bootstrap-toggle.min.js"></script>


    <script>
        var editor, range;
        function OnClientLoad(sender, args) {
            editor = sender;
        }

        function OnClientSelectionChange(sender, args) {
            range = editor.getSelection().getRange(true);
        }
    </script>
    <style>
        .ml85 {
            margin-left: 93px;
        }
    </style>

<script type="text/javascript">
    $(document).ready(function () {
        window.parent.ReviewWindowOnLoad();
    });
    function refreshDoc() {
        window.parent.myfunctionRefreshDoc();
    }
</script>

</head>

<body style="background: white;">
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible" runat="server" id="divsuccess" visible="false">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <label runat="server" id="lblsuccess"></label>
                </div>

                <div class="alert alert-danger alert-dismissible" runat="server" id="diverror" visible="false">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <label runat="server" id="lblerror"></label>
                </div>
            </div>
        </div>

        <div class="row hidden">
            <div class="col-md-12 colpadding0">
                <lable class="control-label ml85">Heading</lable>
            </div>
        </div>

        <div class="row form-group hidden">
            <div class="col-md-12 colpadding0">
                <asp:TextBox ID="txtAgendaHeading" runat="server" Width="810px" CssClass="ml85"></asp:TextBox>
            </div>
        </div>

        <div class="tab" role="tabpanel">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#agendaFormat">Agenda Template</a></li>
                <li><a data-toggle="tab" href="#agendaResolution">Resolution Template</a></li>
             </ul>
                <div class="tab-content">
                <div id="agendaFormat" class="tab-pane fade in active">
                    <div class="row colpadding0">
            <div class="col-md-12 colpadding0">

                <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                    Width="100%"
                    Height="420px" ToolsFile="~/Areas_Secretarial/ToolsFile.xml"
                    ContentFilters="DefaultFilters, PdfExportFilter"
                    SkinID="WordLikeExperience" EditModes="All">
                    <RealFontSizes>
                        <telerik:EditorRealFontSize Value="12pt" />
                        <telerik:EditorRealFontSize Value="18pt" />
                        <telerik:EditorRealFontSize Value="22px" />
                    </RealFontSizes>
                    <ExportSettings>
                        <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                            PageHeader="Some header text for DOCX documents" />
                        <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                            HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                    </ExportSettings>
                    <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                        UserCssId="reU0"></TrackChangesSettings>
                    <Content>                  
                    </Content>
                </telerik:RadEditor>

            </div>
        </div>
                </div>

                <div id="agendaResolution" class="tab-pane fade">
                    <div class="row colpadding0">
                        <div class="col-md-12 colpadding0">
                            <telerik:RadEditor RenderMode="Lightweight" ID="reResolutionFormat" StripFormattingOptions="MSWord" EnableTrackChanges="true" runat="server"
                Width="100%"
                Height="420px" ToolsFile="~/Areas_Secretarial/ToolsFile.xml"
                ContentFilters="DefaultFilters, PdfExportFilter"
                SkinID="WordLikeExperience" EditModes="All">
                <RealFontSizes>
                    <telerik:EditorRealFontSize Value="12pt" />
                    <telerik:EditorRealFontSize Value="18pt" />
                    <telerik:EditorRealFontSize Value="22px" />
                </RealFontSizes>
                <ExportSettings>
                    <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8"
                        PageHeader="Some header text for DOCX documents" />
                    <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13"
                        HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                </ExportSettings>
                <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true"
                    UserCssId="reU0"></TrackChangesSettings>
                <Content>                  
                </Content>
            </telerik:RadEditor>

                        </div>
                    </div>
            </div>
                
            </div>
            

        </div>


        <div class="row colpadding0 mt30">
            <div class="col-md-12 colpadding0" style="text-align: center">
                <button type="button" class="btn btn-primary hidden"><span class=""></span>Save</button>
                <asp:Button class="btn btn-primary" ID="btnSaveAgendaDetails" OnClick="btnSaveAgendaDetails_Click" runat="server" Text="Save" />
            </div>
        </div>

        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
        </telerik:RadAjaxLoadingPanel>

    </form>

</body>
</html>
