﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgendaDocumentViewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.AgendaDocumentViewer" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="overflow-y:hidden;">
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <table style="width:100%">
            <tr>
                <td style="width:20%; vertical-align:top; display:none;">
                    <asp:Label ID="lblFileName" runat="server" Text="" ></asp:Label>
                </td>
                <td style="width:90%">
                    <div>
                        <GleamTech:DocumentViewer runat="server" Width="100%" ID="doccontrol" FullViewport="false" searchControl="true" SidePaneVisible="true" Height="560px"
                            DownloadAsPdfEnabled="true" DisableHeaderIncludes="false" ClientLoad="documentViewerLoad" ToolbarVisible="true"  />
                    </div>   
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

<script type="text/javascript">
    function documentViewerLoad(sender, e) {
        var documentViewer = sender; //sender parameter will be the DocumentViewer instance

        documentViewer.setZoomLevel(1); // Set zoom to 100%
    }
</script>

