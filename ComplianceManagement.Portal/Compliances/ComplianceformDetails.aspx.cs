﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceformDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindCustomersList();
            }
        }

        private void BindCustomersList()
        {
            try
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                var details = Assigncustomer.GetAllCustomer(userID);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = details;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlCustomer.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            int customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
            var compliancesQuery = Business.ComplianceManagement.GetComplianceFormDetails(customerid);

            grdComplianceForm.DataSource = compliancesQuery;
            grdComplianceForm.DataBind();

        }

        protected void grdComplianceForm_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdComplianceForm.PageIndex = e.NewPageIndex;
            BindGrid();
        }

     

        public static List<SP_GetComplianceFormDetails_Result> GetComplianceDocuments(long Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SP_GetComplianceFormDetails(Customerid)
                                   select row).ToList();

                return queryResult;
            }
        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            int Customerid = -1;

            if (ddlCustomer.SelectedValue != "-1")
            {
                 Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
            }
            var lstDocument = GetComplianceDocuments(Customerid);

            if (ddlCustomer.SelectedValue != "-1")
            {
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    lstDocument = GetComplianceDocuments(Customerid);

                    if (lstDocument.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in lstDocument)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath));
                            //string filePath = Path.Combine(Server.MapPath(file.DocPath), Path.GetExtension(file.DocName));
                            if (file.FilePath != null)
                            {
                                int idx = file.DocumentName.LastIndexOf('.');
                                string str = file.DocumentName.Substring(0, idx) + "_" + i + "." + file.DocumentName.Substring(idx + 1);
                                //string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                string ComType = file.ComplianceTypename;
                                string ComCategory = file.ComplianceCategory;
                                string ComplianceId = Convert.ToString(file.ComplianceID);
                                string State = file.State;

                                if(ComType== "Central")
                                {
                                    ComplianceZip.AddEntry("Compliance Document" + "/" + ComType + "/" + ComCategory + "/" + ComplianceId + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                    i++;
                                }
                                else
                                {
                                    ComplianceZip.AddEntry("Compliance Document" + "/" + ComType + "/" + State + "/" + ComCategory + "/" + ComplianceId + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                    i++;
                                }
                               
                               
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(Filedata);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                    }
                }
            }



        }
    }
}