﻿<%@ Page Title="widget Master" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="widget_master.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.widget_master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript">
        function initializeCombobox() {
        }
        function Validate(sender, args) {
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkACT']:checked").length
            if (rowCheckBoxSelected == 0 || rowCheckBoxSelected == "undefined") {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .ui-state-active {
            border: 1px solid #fad42e !important;
            background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
            color: #363636 !important;
        }

        .ui-datepicker-div {
            top: 482.453px !important;
        }

        .vdsummary {
    font-family: Lucida Grande,Lucida Sans,Arial,sans-serif;
    font-size: 13px;
    padding-top: 9px;
    font-weight: bolder;
    border: solid 1px red;
    background-color: #ffe8eb;
    margin-top: 7px;
    width: 98%;
}

        .gvPager span {
    text-decoration:underline !important;
}
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <div style="margin-bottom: 4px">
        <asp:ValidationSummary runat="server" CssClass="vdsummary"
            ValidationGroup="ComplianceInstanceValidationGroup" />
        <div align="center" style="margin-top: 7px; font-family: Arial; font-size: 10pt">
          <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
          <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>         
        <asp:CustomValidator ID="cvDuplicateEntry2" runat="server" EnableClientScript="False" 
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
    </div>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upWidgetList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <table width="100%">
                <tr>
                    <td class="td3">
                             <div id="lblcustomer" runat="server">
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Customer :
                            </label>
                                 </div>
                        </td>
                        <td class="td4">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 27px; width: 258px;margin-left: -15px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                    <td align="left" style="width: 15%">
                        <asp:DropDownList runat="server" ID="ddlfilterWidget" Style="padding: 0px; margin: 0px; height: 27px; width: 220px;margin-left: 15px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterWidget_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">
                        <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;margin-left: 10px;">
                                Filter :
                            </label>
                        </td>
                        <td class="td2">
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" style="margin-left: -90px;" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />

                    </td>

                    <td align="right" style="width: 20%">
                        <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;margin-left: 18px;">
                         <asp:FileUpload ID="fnuploadwidget" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select file to upload." ControlToValidate="fnuploadwidget"
                                runat="server" Display="None" ValidationGroup="ComplianceInstanceValidationGroup" />
                    </td>
                        </label>
                    </td>
                    <td style="width: 20%">
                         <asp:Button Text="Upload" runat="server" ID="btnUpload" style="margin-left: 38px;" ValidationGroup="ComplianceInstanceValidationGroup"
                            OnClick="btnUpload_Click" CssClass="button"  />  
                    </td>
                     <td align="right" style="width: 9%">
                        <u><a href="../../ExcelFormat/WidgetSampleUploadFormat.xlsx">Sample Format</a></u>
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddWidget" OnClick="btnAddWidget_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdWidget" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" style="margin-top: 20px;"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdWidget_RowCommand" OnPageIndexChanging="grdWidget_PageIndexChanging">
                <PagerStyle CssClass ="gvPager" />
                <Columns>
                    <asp:TemplateField HeaderText="Sr No">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ID" HeaderText="Widget ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="80px" />
                    <asp:TemplateField HeaderText="Widget Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 900px">
                                <asp:Label runat="server" Text='<%# Eval("WidgetName") %>' ToolTip='<%# Eval("WidgetName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Widget" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Widget" title="Edit Widget" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Widget" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Widget?');"><img src="../Images/delete_icon.png" alt="Delete Widget" title="Delete Widget" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            
              <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="divActDialog" style="height: auto;">
        <asp:UpdatePanel ID="upWidget" runat="server" UpdateMode="Conditional" OnLoad="upWidget_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="CustomDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>

                    <div id="customerdrop" runat="server" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomerNew" Style="padding: 0px; margin: 0px; height: 33px; width: 673px;"
                            OnSelectedIndexChanged="ddlCustomerNew_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                        <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomerNew"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                        Display="None" />

                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 30px; width: 673px;" MaxLength="100" ToolTip="" TextMode="MultiLine" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ActValidationGroup" Display="None" />

                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Act
                        </label>
                        <div>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one Act."
                                ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="ActValidationGroup" Display="None"></asp:CustomValidator>
                        </div>
                        <asp:TextBox runat="server" ID="txtACTName" Style="padding: 0px; margin: 0px; height: 30px; width: 673px;"
                              CssClass="txtbox" OnTextChanged="txtACTName_TextChanged" />
                        <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px; width: 690px;" id="dvACT">
                            <asp:Repeater ID="rptActs" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ActSelectAll" Text="All" runat="server" onclick="checkAllAct(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            <td>
                                                <asp:CustomValidator ID="cValidation" runat="server"
                                                    ErrorMessage="Invalid"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkACT" runat="server" onclick="UncheckHeaderAct();" /></td>
                                        <td style="width: 560px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px; padding-bottom: 5px;">
                                                <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <%--  compliance --%>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance
                        </label>
                         <div>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Please select at least one Compliance."
                                ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="ActValidationGroup" Display="None"></asp:CustomValidator>
                        </div>
                        <asp:TextBox runat="server" ID="txtComplince" Style="padding: 0px; margin: 0px; height: 30px; width: 673px;"
                            CssClass="txtbox" />
                        
                        <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px; width: 690px;" id="dvCompliance">
                            <asp:Repeater ID="rptCompliances" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterSubTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ComplianceSelectAll" Text="All"
                                                    runat="server" onclick="checkAllCompliance(this)" /></td>
                                            <td style="width: 282px;">
                                               <asp:Button runat="server" ID="btnRepeatersub" Text="Ok" Style="float: left" OnClick="btnRefreshnew_Click" /></td>
                                       </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkcompliance" runat="server" onclick="UncheckHeaderCompliance();" /></td>
                                        <td style="width: 560px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px; padding-bottom: 5px;">
                                                <asp:Label ID="lblcomplianceid" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblcomplianceName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div id="DivSave" style="margin-bottom: 7px; margin-top: 4%; margin-left: 300px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave"
                            OnClick="btnSave_Click" CssClass="button" ValidationGroup="ActValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>


    

    <script type="text/javascript">
        $(function () {
            $('#divActDialog').dialog({
                width: 900,
                height: 400,
                autoOpen: false,
                draggable: true,
                title: "Widget",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divActDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divActDialog").dialog('open');

        }
        function initializeCombobox() {
           <%-- $("#<%= ddlCategory.ClientID %>").combobox();
            $("#<%= ddlType.ClientID %>").combobox();
            $("#<%= ddlLocationType.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
            $("#<%= ddlCity.ClientID %>").combobox();
            $("#<%= ddlDepartment.ClientID %>").combobox();
            $("#<%= ddlMinistry.ClientID %>").combobox();
            $("#<%= ddlRegulator.ClientID %>").combobox();
            $("#<%= ddlActDocType.ClientID %>").combobox();--%>
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function checkAllAct(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkACT") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function UncheckHeaderAct() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkACT']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkACT']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ActSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }


        function UncheckHeaderCompliance() {
            var rowCheckBox = $("#RepeaterSubTable input[id*='chkcompliance']");
            var rowCheckBoxSelected = $("#RepeaterSubTable input[id*='chkcompliance']:checked");
            var rowCheckBoxHeader = $("#RepeaterSubTable input[id*='ComplianceSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllCompliance(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkcompliance") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function initializeJQueryUI1(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
