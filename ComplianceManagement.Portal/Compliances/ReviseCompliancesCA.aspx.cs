﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Globalization;
using System.Collections;
using Ionic.Zip;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ReviseCompliancesCA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReviseCompliance(DateTime.Now, DateTime.Now);
                BindTypes();
                BindCategories();
                BindActList();
                BindCompliances();
            }
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindReviseCompliance(DateTime startdate, DateTime enddate, int pageIndex = 0)
        {
            try
            {
                List<ComplianceInstanceTransactionView> ReviseCompliances = new List<ComplianceInstanceTransactionView>();
                CmpInstancesSaveCheckedValues();
                List<long> complianceIds = new List<long>();
                int userID = AuthenticationHelper.UserID;
                int? customerID = -1;
                if (userID > 0 && AuthenticationHelper.Role != "SADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                }

                ReviseCompliances = ComplianceManagement.Business.ComplianceManagement.GetReviseCompliances(AuthenticationHelper.UserID);
                List<ComplianceAssignment> roles = new List<ComplianceAssignment>();

                if (ViewState["checkedCompliancesInstances"] != null)
                {
                    foreach (var gvrow in (ArrayList)ViewState["checkedCompliancesInstances"])
                    {
                        complianceIds.Add(Convert.ToInt32(gvrow));
                    }
                    ReviseCompliances = ReviseCompliances.Where(entry => complianceIds.Contains(entry.ComplianceID)).ToList();
                }

                if (startdate.Date != DateTime.Now.Date && enddate.Date != DateTime.Now.Date)
                {
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.ScheduledOn >= startdate && entry.ScheduledOn <= enddate).ToList();
                }


                grdRviseCompliances.PageIndex = pageIndex;
                grdRviseCompliances.DataSource = ReviseCompliances.Where(entry=>entry.ComplianceStatusID!=11).ToList();
                grdRviseCompliances.DataBind();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReviseFilter\").dialog('close');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRviseCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ReviseCompliance")
                {

                    tbxDate.Text = string.Empty;
                    tbxRemarks.Text = string.Empty;
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ViewState["ScheduledOnID"] = commandArg[0];
                    ViewState["ComplianceInstanceID"] = commandArg[1];
                    var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(Convert.ToInt32(commandArg[0]));
                    lblShorDescription.Text = complianceInfo.ShortDescription;
                    BindTransactions(Convert.ToInt32(commandArg[0]));
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate(); $(\"#divReviseCompliance\").dialog('open')", true);
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRviseCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   
                    LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("lnkReviseCompliances");
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                    e.Row.ToolTip = "Click on row to revise Compliance";
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSetFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlComplinceCatagory.SelectedIndex = -1;
                ddlFilterComplianceType.SelectedIndex = -1;
                ViewState["checkedCompliancesInstances"] = null;
                BindCompliances();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReviseFilter\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRviseCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CmpSaveCheckedValues();
            BindReviseCompliance(DateTime.Now, DateTime.Now, pageIndex: e.NewPageIndex);
            //CmpPopulateCheckedValues();
        }

        private void CmpPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdRviseCompliances.Rows)
                {
                    LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                    int index = Convert.ToInt32(lblDownLoadfile.CommandArgument); ;
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdRviseCompliances.Rows)
            {
                LinkButton lblDownLoadfile = (LinkButton)gvrow.FindControl("lblDownLoadfile");
                index = Convert.ToInt32(lblDownLoadfile.CommandArgument);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliances"] = userdetails;
        }

        private void CmpInstancesPopulateCheckedValues()
        {
            ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliancesInstances"];

            if (complianceDetails != null && complianceDetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {

                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    if (complianceDetails.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkFilterCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        private void CmpInstancesSaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {

                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkFilterCompliances")).Checked;

                // Check in the Session
                if (ViewState["checkedCompliancesInstances"] != null)
                    userdetails = (ArrayList)ViewState["checkedCompliancesInstances"];
                if (result)
                {
                    if (!userdetails.Contains(index))
                        userdetails.Add(index);
                }
                else
                    userdetails.Remove(index);
            }
            if (userdetails != null && userdetails.Count > 0)
                ViewState["checkedCompliancesInstances"] = userdetails;
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlComplinceCatagory.DataTextField = "Name";
                ddlComplinceCatagory.DataValueField = "ID";

                ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplinceCatagory.DataBind();

                ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind all acts.
        /// </summary>
        private void BindActList()
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);

                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCompliances(int pageIndex = 0)
        {
            try
            {

                int complianceCatagoryID = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
                int complianceTypeID = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);

                List<int> actIds = new List<int>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }

                //if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
                //{
                grdComplianceInstances.PageIndex = pageIndex;
                grdComplianceInstances.DataSource = Business.ComplianceManagement.GetComplianceForRevise(complianceTypeID, complianceCatagoryID, false, -1, actIds);
                grdComplianceInstances.DataBind();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpInstancesSaveCheckedValues();
            BindCompliances(pageIndex: e.NewPageIndex);
            CmpInstancesPopulateCheckedValues();
        }

        protected void grdComplianceInstances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    //CheckBox headerchk = (CheckBox)grdComplianceInstances.HeaderRow.FindControl("chkFilterCompliancesHeader");
                    //CheckBox childchk = (CheckBox)e.Row.FindControl("chkFilterCompliances");
                    //childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdComplianceInstances')");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();

                if (strtdate != "" && enddate != "")
                {
                    DateTime startdate1 = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime enddate1 = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    BindReviseCompliance(startdate1, enddate1);
                }
                else
                {
                    BindReviseCompliance(DateTime.Now, DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                try
                {
                    long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(ViewState["ScheduledOnID"])).ComplianceStatusID;

                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(Convert.ToInt32(ViewState["ScheduledOnID"])),
                        ComplianceInstanceId = Convert.ToInt64(Convert.ToInt32(ViewState["ComplianceInstanceID"])),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 11,
                        StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldPerformerID;
                    }

                    List<FileData> files = new List<FileData>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                    HttpFileCollection fileCollection = Request.Files;
                    bool blankfileCount = true;
                    if (fileCollection.Count > 0)
                    {
                        int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(ViewState["ComplianceInstanceID"]));
                        string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(ViewState["ScheduledOnID"]));

                        string directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + Convert.ToInt32(ViewState["ScheduledOnID"]) + "/" + version);
                        DocumentManagement.CreateDirectory(directoryPath);

                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection[i];
                            string[] keys = fileCollection.Keys[i].Split('$');
                            String fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                            {
                                fileName = "ComplianceDoc_" + uploadfile.FileName;
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                            }
                            else
                            {
                                fileName = "WorkingFiles_" + uploadfile.FileName;
                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                            }

                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                            if (uploadfile.ContentLength > 0)
                            {
                                FileData file = new FileData()
                                {
                                    Name = fileName,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey.ToString(),
                                    Version = version,
                                    VersionComment = tbxRemarks.Text,
                                    VersionDate = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                    FileSize = uploadfile.ContentLength,
                                };

                                files.Add(file);

                            }
                            else
                            {
                                if(!string.IsNullOrEmpty(uploadfile.FileName))
                                blankfileCount = false;
                            }
                        }
                    }

                    bool flag = false;
                    if (blankfileCount)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        if (StatusID != null && StatusID > 3 && StatusID != 10)
                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                        else
                            flag = DocumentManagement.SaveReviseCompliances(files, list, Filelist, Convert.ToInt32(ViewState["ScheduledOnID"]), customerID);
                    }else
                       {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                      }

                    //bool flag = true;
                    if (flag != true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate(); $(\"#divReviseCompliance\").dialog('close')", true);
                    BindReviseCompliance(DateTime.Now, DateTime.Now);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //version history

        protected void grdReviseVersionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdReviseVersionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdReviseVersionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviseVersionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(ViewState["ScheduledOnID"])).Select(x => new
                {
                    ID = x.ID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                }).GroupBy(entry => entry.Version).Select(entry=>entry.FirstOrDefault()).ToList();

                if (direction == SortDirection.Ascending)
                {
                    documentVersionData = documentVersionData.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    documentVersionData = documentVersionData.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdReviseVersionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdReviseVersionHistory.Columns.IndexOf(field);
                    }
                }

                grdReviseVersionHistory.DataSource = documentVersionData;
                grdReviseVersionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                var documentVersionData = DocumentManagement.GetFileData1(ScheduledOnID).Select(x => new
                                                                                                {
                                                                                                    ID = x.ID,
                                                                                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                                                                                    VersionDate = x.VersionDate,
                                                                                                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                                                                                                }).GroupBy(entry=>entry.Version).Select(entry=>entry.FirstOrDefault()).ToList();

                grdReviseVersionHistory.DataSource = documentVersionData;
                grdReviseVersionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}

