﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ChangeDueDatecompliance : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                vsLicenseListPage.CssClass = "alert alert-danger";
                BindCustomersList(userID);
                BindActs();
               
            }
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindComplianceFilter();
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindComplianceFilter()
        {
            try
            {
                int customerid = -1;
                int duedateid = -1;
                int actid = -1;
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlduedate.SelectedValue) && Convert.ToString(ddlduedate.SelectedValue) != "-1")
                {
                    duedateid = Convert.ToInt32(ddlduedate.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFilterAct.SelectedValue) && Convert.ToString(ddlFilterAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlFilterAct.SelectedValue);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    grdClient.DataSource = null;
                    grdClient.DataBind();
                    var ClientcomplianceDetails = (from row in entities.SP_ClientBasedDueDate(customerid)
                                                   select row).ToList();

                    if (ClientcomplianceDetails.Count > 0)
                    {
                        //if (duedateid != -1)
                        //{
                        //    //ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.Frequency == frequencyid).ToList();
                        //    ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.DueDate == duedateid).ToList();
                        //}
                        if (actid != -1)
                        {
                            ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ActID == actid).ToList();
                        }
                        if (!string.IsNullOrEmpty(tbxFilter.Text))
                        {
                            if (CheckInt(tbxFilter.Text))
                            {
                                int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ComplianceID == a).ToList();
                            }
                            else
                            {
                                ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            }
                        }
                        grdClient.DataSource = ClientcomplianceDetails;
                        grdClient.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterFrequencies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
     
        private void BindCustomersList(int UserID)
        {
            try
            {
                //  var details= CustomerManagement.GetAll(-1);
                var details = Assigncustomer.GetAllCustomer(userID);
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                ddlCustomerList.DataSource = details;
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlCustomerList.SelectedIndex = -1;

                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = details;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlFilterCustomer.SelectedIndex = -1;

                //ddlUploadCustomerList.DataTextField = "Name";
                //ddlUploadCustomerList.DataValueField = "ID";
                //ddlUploadCustomerList.DataSource = details;
                //ddlUploadCustomerList.DataBind();
                //ddlUploadCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                //ddlUploadCustomerList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        
        private void BindActs()
        {
            try
            {
                var ActDetails = ChecklistFrequency.GetAllAct();
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActDetails;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlAct.SelectedIndex = -1;

                ddlFilterAct.DataTextField = "Name";
                ddlFilterAct.DataValueField = "ID";
                ddlFilterAct.DataSource = ActDetails;
                ddlFilterAct.DataBind();
                ddlFilterAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlFilterAct.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                long actid = -1;
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (actid != -1)
                {
                    BindShortDescription(Convert.ToInt64(ddlAct.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindShortDescription(long ActID)
        {
            try
            {
                ddlshortdescription.DataTextField = "ShortDescription";
                ddlshortdescription.DataValueField = "ID";

                ddlshortdescription.DataSource = GetShortdescription(ActID);
                ddlshortdescription.DataBind();

                ddlshortdescription.Items.Insert(0, new ListItem("< Select Short Description >", "-1"));
                ddlshortdescription.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<object> GetShortdescription(long ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var description = (from row in entities.Compliances
                                   where row.IsDeleted == false 
                                   //&& row.ComplianceType == 1
                                   //&& row.Frequency != null
                                   && row.ActID == ActID
                                   orderby row.ShortDescription ascending
                                   select new { ID = row.ID, ShortDescription = row.ShortDescription }).OrderBy(entry => entry.ShortDescription).ToList<object>();


                return description;
            }
        }

        private void BindDueDates(int duedate)
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlduedate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                    ddlduedate.SelectedValue = Convert.ToString(duedate);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlshortdescriptionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var compliance =GetCompliance(Convert.ToInt64(ddlshortdescription.SelectedValue));
                BindDueDates(compliance);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetCompliance(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MappedIDs = (from row in entities.Compliances
                                      where row.ID == ComplianceID
                                      select (int)row.DueDate).FirstOrDefault();
                return MappedIDs;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long customerid = -1;
                int duedateid = -1;
                long actid = -1;
                long complianceid = -1;

                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue) && Convert.ToString(ddlCustomerList.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlduedate.SelectedValue) && Convert.ToString(ddlduedate.SelectedValue) != "-1")
                {
                    duedateid = Convert.ToInt32(ddlduedate.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlshortdescription.SelectedValue) && Convert.ToString(ddlshortdescription.SelectedValue) != "-1")
                {
                    complianceid = Convert.ToInt32(ddlshortdescription.SelectedValue);
                }
                //int frequency = GetFrequency(complianceid);
                var compliance = Business.ComplianceManagement.GetByCustomerspecificcomplianceID(complianceid);
                int? frequency = compliance.Frequency;
                int? subcompliancetype = compliance.SubComplianceType;
                if (customerid != -1 && duedateid != -1 && actid != -1 && complianceid != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        CustomerSpecificCompliance cust = new CustomerSpecificCompliance()
                        {
                            CustomerID = Convert.ToInt32(customerid),
                            ComplianceId = Convert.ToInt32(complianceid),
                            ActID = Convert.ToInt32(actid),
                            DueDate = duedateid,
                            CreatedBy = AuthenticationHelper.UserID,
                            CretaedOn = DateTime.Now,
                            IsActive = true,
                            Frequencyid= frequency,
                            SubComplianceType=subcompliancetype
                        };
                        if ((int)ViewState["Mode"] == 1)
                        {
                            cust.ID = Convert.ToInt32(ViewState["CBCTID"]);
                        }

                        if ((int)ViewState["Mode"] == 0)
                        {
                            if (Exists(cust))
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "compliance  due date already exists for this client.";
                                vsLicenseListPage.CssClass = "alert alert-danger";
                                return;
                            }
                            entities.CustomerSpecificCompliances.Add(cust);
                            entities.SaveChanges();


                            GenerateDefaultScheduleForComplianceID(cust.ComplianceId, true,Convert.ToInt32(cust.CustomerID));
                         

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record saved successfully";
                            vsLicenseListPage.CssClass = "alert alert-success";
                            //clearfilter();
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (!DueDateExists(customerid, complianceid, duedateid))
                            {
                               
                                Update(cust);
                                GenerateDefaultScheduleForComplianceID(complianceid, true,Convert.ToInt32(cust.CustomerID));

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record updated successfully";
                                vsLicenseListPage.CssClass = "alert alert-success";
                                //clearfilter();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Compliance can not be updated ,because it is tagged to compliances which are already performed.";
                                vsLicenseListPage.CssClass = "alert alert-danger";
                            }
                        }
                        BindComplianceFilter();
                        upComplianceTypeList.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                vsLicenseListPage.CssClass = "alert alert-danger";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int  GetFrequency(long complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Compliances
                            where row.ID == complianceid
                            select (int)row.Frequency).FirstOrDefault();

                return data;

               
            }
        }
        public static void GenerateDefaultScheduleForComplianceID(long complianceID, bool deleteOldData,long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //if (!(subcomplincetype == 0))
                //{
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.CustomerWiseComplianceSchedules
                                   where row.ComplianceID == complianceID
                                   && row.Customerid==customerid
                                   select row.ID).ToList();
                        ids.ForEach(entry =>
                        {
                            CustomerWiseComplianceSchedule schedule = (from row in entities.CustomerWiseComplianceSchedules
                                                           where row.ID == entry
                                                           && row.Customerid==customerid
                                                           select row).FirstOrDefault();

                            entities.CustomerWiseComplianceSchedules.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    var compliance = (from row in entities.CustomerSpecificCompliances
                                      where row.ComplianceId == complianceID
                                      && row.CustomerID==customerid
                                      select row).First();
                var compliancedata = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).First();

                if(compliance.Frequencyid != compliancedata.Frequency)
                {
                    int newfrequency = Convert.ToInt32(compliancedata.Frequency);
                    compliance.Frequencyid = newfrequency;
                    entities.SaveChanges();
                }


                int step = 1;

                    switch ((Frequency)compliancedata.Frequency)
                    {
                        case Frequency.Quarterly:
                            step = 3;
                            break;
                        case Frequency.FourMonthly:
                            step = 4;
                            break;
                        case Frequency.HalfYearly:
                            step = 6;
                            break;
                        case Frequency.Annual:
                            step = 12;
                            break;
                        case Frequency.TwoYearly:
                            step = 12;
                            break;
                        case Frequency.SevenYearly:
                            step = 12;
                            break;
                    }


                    int SpecialMonth;
                    for (int month = 1; month <= 12; month += step)
                    {
                        CustomerWiseComplianceSchedule complianceShedule = new CustomerWiseComplianceSchedule();
                        complianceShedule.ComplianceID = Convert.ToInt32(complianceID);
                        complianceShedule.Customerid = Convert.ToInt32(customerid);
                        complianceShedule.ForMonth = month;

                    if (compliance.SubComplianceType == 1 || compliance.SubComplianceType == 2)
                    {
                        int monthCopy = month;
                        if (monthCopy == 1)
                        {
                            monthCopy = 12;
                            complianceShedule.ForMonth = monthCopy;
                        }
                        else
                        {
                            monthCopy = month - 1;
                            complianceShedule.ForMonth = monthCopy;
                        }

                        if (compliancedata.Frequency == 3 || compliancedata.Frequency == 5 || compliancedata.Frequency == 6)
                        {
                            monthCopy = 3;
                            complianceShedule.ForMonth = monthCopy;
                        }

                        int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                        complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");
                       

                    }
                    else
                    {
                        SpecialMonth = month;
                            if (compliancedata.Frequency == 0 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliancedata.Frequency == 1 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliancedata.Frequency == 2 && SpecialMonth == 7)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliancedata.Frequency == 4 && SpecialMonth == 9)
                            {
                                SpecialMonth = 1;
                            }
                            else if ((compliancedata.Frequency == 3 || compliancedata.Frequency == 5 || compliancedata.Frequency == 6) && SpecialMonth == 1)
                            {
                                SpecialMonth = 1;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }

                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(compliance.DueDate.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = compliance.DueDate.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                    }
                    entities.CustomerWiseComplianceSchedules.Add(complianceShedule);

                    }

                    entities.SaveChanges();
                //}
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void btnAddFrequency_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearfilter();
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divClientduedateDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void clearfilter()
        {
       
            ddlshortdescription.ClearSelection();
            ddlAct.ClearSelection();
            ddlCustomerList.ClearSelection();
            ddlduedate.ClearSelection();
            
        }
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CBCTID"] = ID;
                    clearfilter();
                    var ltlm = GetByID(ID);
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.CustomerID)))
                    {
                        ddlCustomerList.SelectedValue = Convert.ToString(ltlm.CustomerID);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.ActID)))
                    {
                        ddlAct.SelectedValue = Convert.ToString(ltlm.ActID);
                        BindShortDescription(Convert.ToInt64(ddlAct.SelectedValue));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.ComplianceId)))
                    {
                        ddlshortdescription.SelectedValue = Convert.ToString(ltlm.ComplianceId);
                    }

                    BindDueDates(ltlm.DueDate);
                  
                  
                    upComplianceDetails.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divClientduedateDetailsDialog\").dialog('open')", true);
                }
                //else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                //{
                //    string Args = e.CommandArgument.ToString();
                //    string[] arg = Args.ToString().Split(';');
                //    long ID = -1;
                //    long Customerid = -1;
                //    long Complianceid = -1;
                //    long duedate = -1;
                //    if (arg.Length > 2)
                //    {
                //        ID = Convert.ToInt32(arg[0]);
                //        Customerid = Convert.ToInt32(arg[1]);
                //        Complianceid = Convert.ToInt32(arg[2]);
                //        duedate= Convert.ToInt32(arg[3]);
                //        //if (!DueDateExists(Customerid, Complianceid,duedate))
                //        //{
                //            Delete(ID, AuthenticationHelper.UserID);
                //            cvDuplicateEntry1.IsValid = false;
                //            cvDuplicateEntry1.ErrorMessage = "Record deleted successfully";
                //            VSCompliance.CssClass = "alert alert-success";
                //            BindComplianceFilter();
                //        //}
                //        //else
                //        //{
                //        //    cvDuplicateEntry1.IsValid = false;
                //        //    cvDuplicateEntry1.ErrorMessage = "Compliance can not be deleted ,because it is tagged to compliances which are already performed.";
                //        //}
                //    }
                //}
                else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    int compliancedueID = Convert.ToInt32(e.CommandArgument);
                    var ltlm = GetByID(compliancedueID);
                    var compliance = Business.ComplianceManagement.GetByID(ltlm.ComplianceId);
                    OpenScheduleInformation(ltlm,Convert.ToInt32(compliance.Frequency));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void OpenScheduleInformation(Business.Data.CustomerSpecificCompliance compliance,int frequency)
        {
            try
            {
                //saveopo.Value = "true";
                ViewState["ComplianceID"] = compliance.ComplianceId;
                ViewState["CustomerID"] = compliance.CustomerID;
                ViewState["Frequency"] = compliance.Frequencyid.Value;
                if (compliance.DueDate != null)
                {
                    ViewState["Day"] = compliance.DueDate;
                }

                if ((compliance.Frequencyid.Value == 0 || compliance.Frequencyid.Value == 1))
                    divStartMonth.Visible = false;
                else
                    divStartMonth.Visible = true;

                //var scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
                if(frequency!=compliance.Frequencyid)
                {
                    GenerateDefaultScheduleForComplianceID(compliance.ComplianceId, true, Convert.ToInt32(compliance.CustomerID));
                }
            
                //UpdateFrequency(compliance.ComplianceId, Convert.ToInt32(compliance.CustomerID));
                var scheduleList = GetScheduleByComplianceID(compliance.ComplianceId,Convert.ToInt32(compliance.CustomerID));
                if (scheduleList.Count == 0)
                {
                    GenerateDefaultScheduleForComplianceID(compliance.ComplianceId, true,Convert.ToInt32(compliance.CustomerID));
                    scheduleList = GetScheduleByComplianceID(compliance.ComplianceId, Convert.ToInt32(compliance.CustomerID));
                }

                int step = 0;
                if (compliance.Frequencyid.Value == 0)
                    step = 0;
                else if (compliance.Frequencyid.Value == 1)
                    step = 2;
                else if (compliance.Frequencyid.Value == 2)
                    step = 5;
                else if (compliance.Frequencyid.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.Frequencyid.Value == 0 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();

                if (divStartMonth.Visible)
                {
                    if (compliance.SubComplianceType == 1)
                    {
                        if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
                            ddlStartMonth.SelectedValue = Convert.ToString(1);
                        else
                            ddlStartMonth.SelectedValue = Convert.ToString(4);
                    }
                    else
                    {
                        ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                    }
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void UpdateFrequency(int complianceID, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.CustomerSpecificCompliances
                                  where row.ComplianceId == complianceID
                                  && row.CustomerID == customerid
                                  select row).First();
                var compliancedata = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).First();


                if (compliance.Frequencyid != compliancedata.Frequency)
                {
                    int newfrequency = Convert.ToInt32(compliancedata.Frequency);
                    compliance.Frequencyid = newfrequency;
                    entities.SaveChanges();
                }
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        public static void Delete(long ID, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CustomerSpecificCompliance ltlmToUpdate = (from row in entities.CustomerSpecificCompliances
                                                              where row.ID == ID
                                                              select row).FirstOrDefault();


                //ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsActive = false;
                //ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static CustomerSpecificCompliance GetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.CustomerSpecificCompliances
                           where row.ID == ID && row.IsActive==true
                           select row).FirstOrDefault();
                return data;
            }
        }
       
        public static bool DueDateExists(long CustomerID, long ComplianceID,long duedate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerSpecificCompliances
                             where row.ComplianceId==ComplianceID && row.CustomerID==CustomerID
                             && row.DueDate==duedate
                             select row).ToList();

                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool Exists(CustomerSpecificCompliance ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerSpecificCompliances
                             where row.IsActive == true
                             && row.CustomerID == ltlm.CustomerID
                             && row.ComplianceId == ltlm.ComplianceId
                             && row.ActID==ltlm.ActID
                             //&& row.DueDate==ltlm.DueDate
                             //&& row.Frequencyid==ltlm.Frequencyid
                             select row);

                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static void UploadUpdate(ClientBasedCheckListFrequency ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientBasedCheckListFrequency ltlmToUpdate = (from row in entities.ClientBasedCheckListFrequencies
                                                              where row.ClientID == ltlm.ClientID
                                                              && row.ComplianceID == ltlm.ComplianceID
                                                              select row).FirstOrDefault();

                ltlmToUpdate.CFrequency = ltlm.CFrequency;
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void Update(CustomerSpecificCompliance ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CustomerSpecificCompliance ltlmToUpdate = (from row in entities.CustomerSpecificCompliances
                                                              where row.ID == ltlm.ID
                                                              select row).FirstOrDefault();

                ltlmToUpdate.DueDate = ltlm.DueDate;
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static List<CustomerWiseComplianceSchedule> GetScheduleByComplianceID(long complianceID,long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.CustomerWiseComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    && row.Customerid==customerid
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        //public static void GenerateDefaultScheduleForComplianceID(long complianceID, byte? subcomplincetype, long customerid, byte frequency, bool deleteOldData = false)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        if (!(subcomplincetype == 0))
        //        {
        //            if (deleteOldData)
        //            {
        //                var ids = (from row in entities.CustomerWiseComplianceSchedules
        //                           where row.Customerid == customerid && row.ComplianceID == complianceID
        //                           select row.ID).ToList();
        //                ids.ForEach(entry =>
        //                {
        //                    var schedule = (from row in entities.CustomerWiseComplianceSchedules
        //                                    where row.ID == entry
        //                                    select row).FirstOrDefault();
        //                    entities.CustomerWiseComplianceSchedules.Remove(schedule);
        //                });
        //                entities.SaveChanges();
        //            }

        //            var compliance = (from row in entities.CustomerSpecificCompliances
        //                              where row.ID == complianceID
        //                              && row.CustomerID==customerid
        //                              select row).First();
        //            int step = 1;
        //            switch ((Frequency)frequency)
        //            {
        //                case Frequency.Quarterly:
        //                    step = 3;
        //                    break;
        //                case Frequency.FourMonthly:
        //                    step = 4;
        //                    break;
        //                case Frequency.HalfYearly:
        //                    step = 6;
        //                    break;
        //                case Frequency.Annual:
        //                    step = 12;
        //                    break;
        //                case Frequency.TwoYearly:
        //                    step = 12;
        //                    break;
        //                case Frequency.SevenYearly:
        //                    step = 12;
        //                    break;
        //            }
        //            byte startMonth = 4;
        //            if (frequency == 0)
        //            {
        //                startMonth = Convert.ToByte(1);
        //            }
        //            //else if (frequency == 4)
        //            //{
        //            //    startMonth = Convert.ToByte(1);
        //            //}
        //            else if (frequency == 1)
        //            {
        //                startMonth = Convert.ToByte(1);
        //            }
        //            int SpecialMonth;
        //            for (int month = startMonth; month <= 12; month += step)
        //            {
        //                 CustomerWiseComplianceSchedule complianceShedule = new CustomerWiseComplianceSchedule();
        //                complianceShedule.ComplianceID = Convert.ToInt32(complianceID);
        //                complianceShedule.ForMonth = month;
        //                complianceShedule.Customerid = Convert.ToInt32(customerid);
        //                if (subcomplincetype == 1 || subcomplincetype == 2)
        //                {
        //                    int monthCopy = month;
        //                    if (monthCopy == 1)
        //                    {
        //                        monthCopy = 12;
        //                        complianceShedule.ForMonth = monthCopy;
        //                    }
        //                    else
        //                    {
        //                        monthCopy = month - 1;
        //                        complianceShedule.ForMonth = monthCopy;
        //                    }
        //                    if (frequency == 3 || frequency == 5 || frequency == 6)
        //                    {
        //                        monthCopy = 3;
        //                        complianceShedule.ForMonth = monthCopy;
        //                    }
        //                    int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
        //                    complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");
        //                }
        //                else
        //                {
        //                    SpecialMonth = month;
        //                    if (frequency == 0 && SpecialMonth == 12)
        //                    {
        //                        SpecialMonth = 1;
        //                    }
        //                    else if (frequency == 1 && SpecialMonth == 10)
        //                    {
        //                        SpecialMonth = 1;
        //                    }
        //                    else if (frequency == 2 && SpecialMonth == 7)
        //                    {
        //                        SpecialMonth = 1;
        //                    }
        //                    else if (frequency == 2 && SpecialMonth == 10)
        //                    {
        //                        SpecialMonth = 4;
        //                    }
        //                    else if (frequency == 4 && SpecialMonth == 9)
        //                    {
        //                        SpecialMonth = 1;
        //                    }
        //                    else if (frequency == 4 && SpecialMonth == 12)
        //                    {
        //                        SpecialMonth = 4;
        //                    }

        //                    //else if (frequency == 4 && SpecialMonth == 10)
        //                    //{
        //                    //    SpecialMonth = 9;
        //                    //}
        //                    else if ((frequency == 3 || frequency == 5 || frequency == 6) && SpecialMonth == 4)
        //                    {
        //                        SpecialMonth = 4;
        //                    }
        //                    else
        //                    {
        //                        SpecialMonth = SpecialMonth + step;
        //                    }

        //                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
        //                    if (Convert.ToInt32(compliance.DueDate.ToString("D2")) > lastdate)
        //                    //(Convert.ToInt32(compliance.DueDate.Value.ToString("D2")) > lastdate)

        //                    {
        //                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
        //                    }
        //                    else
        //                    {
        //                        complianceShedule.SpecialDate = (Convert.ToInt32(compliance.DueDate.ToString("D2")) + SpecialMonth.ToString("D2"));
        //                    }
        //                }
        //                entities.CustomerWiseComplianceSchedules.Add(complianceShedule);
        //            }
        //            entities.SaveChanges();

        //        }
        //    }
        //}

        #region Upload                
        private bool SheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadClientBasedDueDate"))
                    {
                        if (sheet.Name.Trim().Equals("UploadClientBasedDueDate") || sheet.Name.Trim().Equals("UploadClientBasedDueDate") || sheet.Name.Trim().Equals("UploadClientBasedDueDate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }

        public static void GetCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();
            var ComplianceList = HttpContext.Current.Cache["ComplianceListData"];
            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 360;
                    Records = (from C in entities.Compliances
                                   //join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                                   //join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                               where
                               C.IsDeleted == false
                               && C.EventFlag == null
                               && C.Status == null
                               && C.ComplinceVisible == true
                               && C.ComplianceType == 1
                               select C).ToList();

                    HttpContext.Current.Cache.Insert("ComplianceListData", Records); // add it to cache
                }
            }
        }
        public bool ComplainceExists(int comID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ComplianceListData"];
                //if (Records.Count > 0)
                //{
                    var query = (from row in entities.Compliances
                                 where row.ID == comID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                //}
                //else
                //{
                //    return false;
                //}
            }
        }

        public bool CustomerExists(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
              
                    var query = (from row in entities.Customers
                                 where row.ID == CustomerID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
               
            }
        }
        public bool ActExists(int actID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                    var query = (from row in entities.Acts
                                 where row.ID== actID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
               
            }
        }

        public int getActID(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ActID = -1;
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ActID).FirstOrDefault();
                    ActID = query;
                }
                return ActID;
            }
        }
        public partial class CBCTempTable
        {
            public long ComplianceId { get; set; }
            public long ActId { get; set; }

            public long customerid { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry1.IsValid = false;
            cvDuplicateEntry1.ErrorMessage = finalErrMsg;
        }
        protected void btnUploadSave_Click(object sender, EventArgs e)
        {


            if (FU_Upload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FU_Upload.FileName);
                    FU_Upload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = SheetsExitsts(xlWorkbook, "UploadClientBasedDueDate");
                            if (flag == true)
                            {
                                if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                {
                                    HttpContext.Current.Cache.Remove("ComplianceListData");
                                }
                                GetCompliance();
                                ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["UploadClientBasedDueDate"];
                                List<string> errorMessage = new List<string>();
                                List<string> saveerrorMessage = new List<string>();
                                List<CBCTempTable> lstTemptable = new List<CBCTempTable>();

                                List<CustomerSpecificCompliance> CBDuedate = new List<CustomerSpecificCompliance>();

                                if (xlWorksheetvalidation != null)
                                {
                                    if (GetDimensionRows(xlWorksheetvalidation) != 0)
                                    {
                                        int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                                        #region Validations
                                        int valduedate = -1;
                                        int valcomplianceid = -1;
                                        int valactid = -1;
                                        int valcustid = -1;
                                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                        {
                                            valcomplianceid = -1;
                                            valactid = -1;
                                            valduedate = -1;

                                        

                                            #region 1 ComplianceID
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                            {
                                                valcomplianceid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim());
                                            }
                                            if (valcomplianceid == 0 || valcomplianceid == -1)
                                            {
                                                errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                                            }
                                            else
                                            {
                                                if (ComplainceExists(valcomplianceid) == false)
                                                {
                                                    errorMessage.Add("ComplianceID not Defined in the System at row number-" + rowNum);
                                                }
                                                else
                                                {
                                                    if (!lstTemptable.Any(x => x.ComplianceId == valcomplianceid))
                                                    {
                                                        CBCTempTable tt = new CBCTempTable();
                                                        tt.ComplianceId = valcomplianceid;
                                                        lstTemptable.Add(tt);
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Compliance with this ComplianceID (" + valcomplianceid + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region 2 ActID
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                            {
                                                valactid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim());
                                            }
                                            if (valactid == 0 || valactid == -1)
                                            {
                                                errorMessage.Add("Required ActID at row number-" + rowNum);
                                            }
                                            else
                                            {
                                                if (ActExists(valactid) == false)
                                                {
                                                    errorMessage.Add("ActID not Defined in the System  at row number-" + rowNum);
                                                }
                                                else
                                                {
                                                    if (!lstTemptable.Any(x => x.ActId == valactid))
                                                    {
                                                        CBCTempTable tt = new CBCTempTable();
                                                        tt.ActId = valactid;
                                                        lstTemptable.Add(tt);
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Act with this ActID (" + valactid + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region 3 Duedate
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()))
                                            {
                                                valduedate = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 3].Text.Trim());
                                            }
                                            if (valduedate == 0 || valduedate == -1)
                                            {
                                                errorMessage.Add("Required DueDate at row number-" + rowNum);
                                            }

                                            #endregion

                                            #region 4 CustomerID
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim()))
                                            {
                                                string Customername=Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 4].Text.Trim());
                                                var custid = Business.ComplianceManagement.GetCustomerNameByID(Customername);
                                                valcustid = Convert.ToInt32(custid.ID);
                                            }
                                            if (valcustid == 0 || valcustid == -1)
                                            {
                                                errorMessage.Add("Required Customer Name at row number-" + rowNum);
                                            }
                                            else
                                            {
                                                if (CustomerExists(valcustid) == false)
                                                {
                                                    errorMessage.Add("Customer Name not Defined in the System  at row number-" + rowNum);
                                                }
                                                else
                                                {
                                                    if (!lstTemptable.Any(x => x.customerid == valcustid))
                                                    {
                                                        CBCTempTable tt = new CBCTempTable();
                                                        tt.customerid = valcustid;
                                                        lstTemptable.Add(tt);
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Customer with this Customer Name (" + valcustid + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                                    }
                                                }
                                            }
                                            #endregion

                                            #endregion


                                            if (errorMessage.Count > 0)
                                            {
                                                ErrorMessages(errorMessage);
                                            }
                                            else
                                            {
                                                #region Save
                                                string noupdatedid = string.Empty;
                                                for (int saverowNum = 2; saverowNum <= xlrow2; saverowNum++)
                                                {
                                                    int customerid = -1;
                                                    int duedateid = -1;
                                                    int actid = -1;
                                                    int complianceid = -1;

                                                    

                                                    
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 1].Text.ToString().Trim()))
                                                    {
                                                        complianceid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 1].Text.Trim());
                                                    }
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 2].Text.ToString().Trim()))
                                                    {
                                                        actid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 2].Text.Trim());
                                                    }
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 3].Text.ToString().Trim()))
                                                    {
                                                        duedateid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 3].Text.Trim());
                                                    }
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 4].Text.ToString().Trim()))
                                                    {
                                                        customerid = Convert.ToInt32(valcustid);
                                                    }
                                                    //int frequency = GetFrequency(complianceid);
                                                    var compliance = Business.ComplianceManagement.GetByCustomerspecificcomplianceID(complianceid);
                                                    int? frequency = compliance.Frequency;
                                                    int? subcompliancetype = compliance.SubComplianceType;
                                                    if (customerid != -1 && duedateid != -1 && actid != -1 && complianceid != -1)
                                                    {
                                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                        {
                                                            CustomerSpecificCompliance CBCF = new CustomerSpecificCompliance();

                                                            CBCF.ComplianceId = complianceid;
                                                            CBCF.ActID = actid;
                                                            CBCF.DueDate = duedateid;
                                                            CBCF.CreatedBy = AuthenticationHelper.UserID;
                                                            CBCF.CretaedOn = DateTime.Now;
                                                            CBCF.IsActive = true;
                                                            CBCF.CustomerID = customerid;
                                                            CBCF.Frequencyid = frequency;
                                                            CBCF.SubComplianceType = subcompliancetype;
                                                            if (Exists(CBCF))
                                                            {

                                                                cvDuplicateEntry1.IsValid = false;
                                                                cvDuplicateEntry1.ErrorMessage = "Record Already exists";

                                                            }
                                                            else
                                                            {
                                                                entities.CustomerSpecificCompliances.Add(CBCF);
                                                                entities.SaveChanges();


                                                                GenerateDefaultScheduleForComplianceID(CBCF.ComplianceId, true,Convert.ToInt32(CBCF.CustomerID));
                                                                cvDuplicateEntry1.IsValid = false;
                                                                cvDuplicateEntry1.ErrorMessage = "Record saved successfully";
                                                                VSCompliance.CssClass = "alert alert-success";
                                                            }
                                                        }
                                                    }
                                                }
                                                if (saveerrorMessage.Count > 0)
                                                {
                                                    ErrorMessages(saveerrorMessage);
                                                }
                                                noupdatedid = string.Empty;
                                                #endregion


                                                if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ComplianceListData");
                                                }
                                            }
                                        }
                                    }

                                    else
                                    {
                                        cvDuplicateEntry1.IsValid = false;
                                        cvDuplicateEntry1.ErrorMessage = "No Data Found in Excel Document.";
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
                    LoggerMessage.InsertLog(ex, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        #endregion

       

        protected bool ViewSchedule(object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (Convert.ToString(frequency) == "7" || Convert.ToString(frequency) == "8")
                {
                    return false;
                }
                //else if(EventID != null)
                //{
                //    return false;
                //}
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }

        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Frequency frequency = (Frequency)Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                if (frequency == Frequency.FourMonthly)
                                    specialMonth = startMonth + (step * i) + 4;
                                else
                                    specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {
                            if (frequency == Frequency.FourMonthly)
                                specialMonth = 4;
                            else
                                specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == Frequency.Monthly ? ((Month)month).ToString() : ((Month)month).ToString() + " - " + ((Month)((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");


                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //saveopo.Value = "true";
                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });

                    Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {

                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        List<CustomerWiseComplianceSchedule> scheduleList = new List<CustomerWiseComplianceSchedule>();
                        int customerid = Convert.ToInt32(ViewState["CustomerID"]);
                        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                        {
                            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                            HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                            DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                            DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");
                            
                            scheduleList.Add(new CustomerWiseComplianceSchedule()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                ComplianceID = Convert.ToInt32(ViewState["ComplianceID"]),
                                ForMonth = Convert.ToInt32(hdnForMonth.Value),
                                SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2")),
                                Customerid=customerid
                                
                                
                            });
                        }
                       
                        Business.ComplianceManagement.UpdateCustomerwiseComplianceUpdatedOn(Convert.ToInt64(ViewState["ComplianceID"]), customerid);
                        Business.ComplianceManagement.UpdateCustomerwiseScheduleInformation(scheduleList, customerid);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                //saveopo.Value = "true";
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetCustomerwiseComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}