﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ChangeDueDatecompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ChangeDueDatecompliance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;            
            padding: 0;
        }
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
    width: 212px;
}
         /*.ui-widget-content {
             color: red;
         }*/
    </style>
      <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

     <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
           
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterCustomerList_SelectedIndexChanged">
                        </asp:DropDownList>

                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlFilterCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    </td>
                    
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterAct" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;margin-left: 10px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterAct_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 28%; padding-right: 26px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="190px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                 
                    <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddFrequency" OnClick="btnAddFrequency_Click" />
                    </td>
                    <td>
                         <u><a href="../../ExcelFormat/UploadClientBasedDueDate.xlsx">Sample Format - Client Based Due Date</a></u>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; padding-right: 20px;" align="right">
                         <asp:FileUpload ID="FU_Upload" runat="server" />
                    </td>
                    <td class="newlink" align="right" style="width: 70px">
                        
                        <asp:Button Text="Upload" runat="server" ID="btnUploadSave" OnClick="btnUploadSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                    </td>
                    <td>
                        
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
              <div style="margin: 5px 5px;">
                    <div style="margin-bottom: 4px">
                        <%--<asp:ValidationSummary ID="vsLicenseListPage1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup1" />

                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>

                        <asp:ValidationSummary runat="server" CssClass="vdsummary" ID="VSCompliance"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />

                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                 </div>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdClient" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                        <%-- <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="Compliance ID" />--%>
                         <asp:TemplateField HeaderText="Compliance ID" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="ComplianceID">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ComplianceID") %>' ToolTip='<%# Eval("ComplianceID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Avacom Frequency" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="FrequencyName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("AvacomFrequencyName") %>' ToolTip='<%# Eval("AvacomFrequencyName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DueDate" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="Due Date">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("DueDate") %>' ToolTip='<%# Eval("DueDate") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Due Date" title="Edit Due Date"/></asp:LinkButton>
                               <%-- <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE"                                     
                                    CommandArgument='<%# Eval("ID")+";"+ Eval("CustomerID")+";"+Eval("ComplianceID")+";"+Eval("DueDate") %>'
                                    OnClientClick="return confirm('Are you sure you want to delete this entry of compliance due date?');"><img src="../Images/delete_icon.png" alt="Delete Due Date" title="Delete Due Date"/></asp:LinkButton>--%>
                                  <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ID") %>'
                                    Visible='<%# ViewSchedule(Eval("Frequency"), Eval("ComplianceType"),Eval("SubComplianceType"),Eval("CheckListTypeID")) %>'><img src="../Images/package_icon.png" alt="Display Schedule Information" title="Display Schedule Information"/></asp:LinkButton>
                            </ItemTemplate>
                          <%--  <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />--%>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadSave" />
        </Triggers>
    </asp:UpdatePanel>

    <div id="divClientduedateDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>

                <div style="margin: 5px 5px 80px;">
                  
                      <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" ID="vsLicenseListPage" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>


                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer Name</label>
                        <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlCustomerList" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act"
                            ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:DropDownList runat="server" ID="ddlshortdescription" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlshortdescriptionList_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select short Description"
                            ControlToValidate="ddlshortdescription" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                           <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date</label>
                        <asp:DropDownList runat="server" ID="ddlduedate" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" >
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select short Description"
                            ControlToValidate="ddlduedate" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 210px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divClientduedateDetailsDialog').dialog('close');" />
                    </div>
                   
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

      <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divStartMonth" style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Year</label>
                        <asp:DropDownList runat="server" ID="ddlStartMonth" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Calender Year"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Financial Year"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" style="margin-bottom: 7px; padding-left: 50px">
                                <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th style="width: 150px; border: 1px solid gray">For Period
                                                </th>
                                                <th style="width: 200px; border: 1px solid gray">Day
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <tr>
                                                <td align="center" style="border: 1px solid gray">
                                                    <%# Eval("ForMonthName")%>
                                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                                </td>
                                                <td align="center" style="border: 1px solid gray">
                                                    <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" />
                                                    <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                                </td>
                                            </tr>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 142px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveSchedule"
                            CssClass="button"   OnClick="btnSaveSchedule_Click" />
                       
                        <asp:Button Text="Reset" runat="server" ID="btnReset" 
                            CssClass="button"  OnClick="btnReset_Click" />
                       
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

      <script type="text/javascript">
        $(function () {
            $('#divClientduedateDetailsDialog').dialog({
                height: 450,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Change Due Date",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });     
    </script>

   <%-- <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>--%>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlCustomerList.ClientID %>").combobox();
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlshortdescription.ClientID %>").combobox();
            $("#<%= ddlshortdescription.ClientID %>").combobox();
            $("#<%= ddlduedate.ClientID %>").combobox();
            $("#<%= ddlFilterAct.ClientID %>").combobox();
            $("#<%= ddlFilterCustomer.ClientID %>").combobox();
            
            
        }
    </script>

</asp:Content>
