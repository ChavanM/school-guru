﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Assign_Compliance_Activate : System.Web.UI.Page
    {
        protected int CustomerID = 0;
        public static List<long> locationList = new List<long>();
        public static List<int> Branchlist = new List<int>();
        public static int userID;
        protected static string role;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                role = Convert.ToString(AuthenticationHelper.Role);
                Bindcustomer(userID);
                BindLocationFilter();
                
                tbxFilterLocation.Text = "< Select >";
                if(AuthenticationHelper.Role=="IMPT")
                {
                    divc.Visible = true;
                  lblcustomer.Visible = true;
                }
                else
                {
                    divc.Visible = false;
                  lblcustomer.Visible = false;
                }
               
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
        }
        public void Bindcustomer(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomer.DataSource = GetCustomer();
                ddlCustomer.DataSource = GetAllCustomer(UserID);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        public static object GetAllCustomer(int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row1.UserID == userID
                             && row.IsDeleted == false
                             && row1.IsDeleted == false
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.ToUpper().Contains(filter) || entry.BuyerName.ToUpper().Contains(filter) || entry.BuyerEmail.ToUpper().Contains(filter));
                }

                return users.Distinct().ToList();
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("TempAssignmentReportStatutory");
                        DataTable ExcelData = null;
                        DateTime CurrentDate = DateTime.Today.Date;
                        DataView view = new System.Data.DataView((DataTable)Session["grdTempDetailData"]);
                        
                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "CustomerBranchID", "DepartmentID",
                            "UserID", "Location", "ShortDescription", "Users", "Role", "ActName", "Sections", "Department", "SequenceID");

                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "ComplianceID";
                        exWorkSheet.Cells["A1"].AutoFitColumns(30);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "CustomerBranchID";
                        exWorkSheet.Cells["B1"].AutoFitColumns(30);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "DepartmentID";
                        exWorkSheet.Cells["C1"].AutoFitColumns(30);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "UserID";
                        exWorkSheet.Cells["D1"].AutoFitColumns(30);


                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "Location";
                        exWorkSheet.Cells["E1"].AutoFitColumns(50);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "Short Description";
                        exWorkSheet.Cells["F1"].AutoFitColumns(50);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "User";
                        exWorkSheet.Cells["G1"].AutoFitColumns(50);

                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H1"].Value = "Role";
                        exWorkSheet.Cells["H1"].AutoFitColumns(30);


                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I1"].Value = "Act Name";
                        exWorkSheet.Cells["I1"].AutoFitColumns(30);

                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J1"].Value = "Sections";
                        exWorkSheet.Cells["J1"].AutoFitColumns(30);

                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K1"].Value = "Department";
                        exWorkSheet.Cells["K1"].AutoFitColumns(30);

                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L1"].Value = "Label";
                        exWorkSheet.Cells["L1"].AutoFitColumns(30);

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 12])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=TempAssignmentReportStatutory.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DataTable GetGrid()
        {
            try
            {
                return (grdComplianceRoleMatrix.DataSource as List<SP_TempAssignment_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;
        }
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerID = -1;
                // int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                }
                else if(AuthenticationHelper.Role=="IMPT")
                {   
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                   
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                //   int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                // var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);//
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyLocation(customerID);
                //   var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                //TreeNode node = new TreeNode("< All >", "-1");
                //node.Selected = true;
                //tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                //tvFilterLocation_SelectedNodeChanged(null, null);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

               tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
             //   nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindGrid(nCustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {

                //  tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
                //   nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindGrid(nCustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxStartDate_TextChanged(object sender, EventArgs e)
        {
            setDateToGridView();
        }
     
        private void BindGrid(int CustomerBranchID)
        {
            try
            {
                int customerID = -1;
                
              if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                    locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

                List<SP_TempAssignment_Result> dataSource = new List<SP_TempAssignment_Result>();
                if (locationList.Count > 0)
                {
                    dataSource = Business.ComplianceManagement.GetTempAssignedDetails(customerID, locationList, "");
                }
                else
                {
                    dataSource = Business.ComplianceManagement.GetTempAssignedDetails(customerID, CustomerBranchID, "");
                }
                
                grdComplianceRoleMatrix.Visible = true;
                grdComplianceRoleMatrix.DataSource = dataSource;
                grdComplianceRoleMatrix.DataBind();
                Session["grdTempDetailData"] = (grdComplianceRoleMatrix.DataSource as List<SP_TempAssignment_Result>).ToDataTable();
                grdToDeactivate.DataSource = null;
                grdToDeactivate.Visible = false;
                grdToDeactivate.DataBind();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDeactivateGrid(int CustomerBranchID=-1)
        {
            try
            {
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                var dataSource = Business.ComplianceManagement.GetDetailsToDeactive(locationList);

                grdToDeactivate.Visible = true;
                grdToDeactivate.DataSource = dataSource;
                grdToDeactivate.DataBind();

                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.Visible = false;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        

        protected void chkActivateSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                CheckBox chkActivateSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkActivateSelectAll");
                foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
                {
                    CheckBox chkActivate = (CheckBox)row.FindControl("chkActivate");
                    if (chkActivateSelectAll.Checked == true)
                    {
                        chkActivate.Checked = true;
                    }
                    else
                    {
                        chkActivate.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void chkActivate_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chkActivateSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkActivateSelectAll");
                int countCheckedCheckbox = 0;
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                    if (((CheckBox)row.FindControl("chkActivate")).Checked)
                    {

                        countCheckedCheckbox = countCheckedCheckbox + 1;

                    }

                }
                if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
                {
                    chkActivateSelectAll.Checked = true;
                }
                else
                {
                    chkActivateSelectAll.Checked = false;
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (tbxStartDate.Text != "")
                {
                    SaveCheckedValues();

                    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    BindGrid(nCustomerBranchID);

                    grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                    grdComplianceRoleMatrix.DataBind();
                    if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
                    {
                        setDateToGridView();
                    }
                    PopulateCheckedValues();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select start date";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<TempComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        //int index = Convert.ToInt32(((Label)gvrow.FindControl("lblComplianceID")).Text);
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkActivate");
                            chkPerformer.Checked = rmdata.Performer;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
            //    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            //    var ComplianceRoleMatrixList = Business.ComplianceManagement.GetTempAssignedDetails(nCustomerBranchID);
               
            //    if (direction == SortDirection.Ascending)
            //    {
            //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Descending;
            //    }
            //    else
            //    {
            //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Ascending;
            //    }


            //    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
            //    {
            //        if (field.SortExpression == e.SortExpression)
            //        {
            //            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
            //        }
            //    }


            //    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
            //    grdComplianceRoleMatrix.DataBind();
               
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //public SortDirection direction
        //{
        //    get
        //    {
        //        if (ViewState["dirState"] == null)
        //        {
        //            ViewState["dirState"] = SortDirection.Ascending;
        //        }
        //        return (SortDirection)ViewState["dirState"];
        //    }
        //    set
        //    {
        //        ViewState["dirState"] = value;
        //    }
        //}
        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddMatrixSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


        private void SaveCheckedValues()
        {
            try
            {

                List<TempComplianceAsignmentProperties> complianceList = new List<TempComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    TempComplianceAsignmentProperties complianceProperties = new TempComplianceAsignmentProperties();
                    complianceProperties.ID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkActivate")).Checked;
                    complianceProperties.ComplianceId = Convert.ToInt32(((Label)gvrow.FindControl("lblComplianceID")).Text);
                    complianceProperties.Role = ((Label)gvrow.FindControl("lblRole")).Text;
                    complianceProperties.UserID = Convert.ToInt32(((Label)gvrow.FindControl("lblUserID")).Text);
                    var deptid = ((Label)gvrow.FindControl("lblDepartmentID")).Text;
                    if (!string.IsNullOrEmpty(deptid))
                    {
                        complianceProperties.DepartmentID = Convert.ToInt32(((Label)gvrow.FindControl("lblDepartmentID")).Text);
                    }

                    var OwnerID = ((Label)gvrow.FindControl("lblOwnerID")).Text;
                    if (!string.IsNullOrEmpty(OwnerID) && OwnerID !="0")
                    {
                        complianceProperties.Cer_OwnerID = Convert.ToInt32(((Label)gvrow.FindControl("lblOwnerID")).Text);
                    }

                    var OfficerID = ((Label)gvrow.FindControl("lblOfficerID")).Text;
                    if (!string.IsNullOrEmpty(OwnerID) && OfficerID != "0")
                    {
                        complianceProperties.Cer_OfficerID = Convert.ToInt32(((Label)gvrow.FindControl("lblOfficerID")).Text);
                    }

                    var sequenceid = ((Label)gvrow.FindControl("lblSequenceID")).Text;
                    if (!string.IsNullOrEmpty(sequenceid))
                    {
                        complianceProperties.SequenceID = ((Label)gvrow.FindControl("lblSequenceID")).Text;
                    }
                    complianceProperties.CustomerBranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblCustomerBranchId")).Text);
                    complianceProperties.StartDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString(); // ((CheckBox)gvrow.FindControl("chkActivate")).Checked;
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;

                    if (complianceProperties.Performer)
                    {
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == complianceProperties.ID).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        TempComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ID == complianceProperties.ID).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void SaveCheckedValues()
        //{
        //    try
        //    {

        //        List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
        //        foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
        //        {
        //            ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
        //            complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
        //            // complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkPerformer")).Checked;
        //            //complianceProperties.Reviewer1 = ((CheckBox)gvrow.FindControl("chkReviewer1")).Checked;
        //            //complianceProperties.Reviewer2 = ((CheckBox)gvrow.FindControl("chkReviewer2")).Checked;
        //            //complianceProperties.StartDate = Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString();
        //            // Check in the Session
        //            if (ViewState["CHECKED_ITEMS"] != null)
        //                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

        //            if (complianceProperties.Performer || complianceProperties.Reviewer1)
        //            {
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
        //                if (rmdata != null)
        //                {
        //                    complianceList.Remove(rmdata);
        //                    complianceList.Add(complianceProperties);
        //                }
        //                else
        //                {
        //                    complianceList.Add(complianceProperties);
        //                }
        //            }
        //            else
        //            {
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
        //                if (rmdata != null)
        //                    complianceList.Remove(rmdata);
        //            }

        //        }
        //        if (complianceList != null && complianceList.Count > 0)
        //            ViewState["CHECKED_ITEMS"] = complianceList;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void PopulateCheckedValues()
        //{
        //    try
        //    {
        //        List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

        //        if (complianceList != null && complianceList.Count > 0)
        //        {
        //            foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
        //            {
        //                int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
        //                ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
        //                if (rmdata != null)
        //                {
        //                    CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkPerformer");
        //                    CheckBox chkReviewer1 = (CheckBox)gvrow.FindControl("chkReviewer1");
        //                    //CheckBox chkReviewer2 = (CheckBox)gvrow.FindControl("chkReviewer2");
        //                    TextBox txtStartDate = (TextBox)gvrow.FindControl("txtStartDate");
        //                    chkPerformer.Checked = rmdata.Performer;
        //                    chkReviewer1.Checked = rmdata.Reviewer1;
        //                    //chkReviewer2.Checked = rmdata.Reviewer2;
        //                    txtStartDate.Text = rmdata.StartDate;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceList = new List<TempComplianceAsignmentProperties>();
                SaveCheckedValues();
                complianceList = ViewState["CHECKED_ITEMS"] as List<TempComplianceAsignmentProperties>;
                //  int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //   int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                int customerID = -1;
                // int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (complianceList != null)
                {
                    if (chkToActiveInstance.Checked == true)
                    {
                        List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                        for (int i = 0; i < complianceList.Count; i++)
                        {

                            int TempAssignmentID = complianceList[i].ID;
                            int complianceID = complianceList[i].ComplianceId;
                            int branchID = complianceList[i].CustomerBranchID;
                            DateTime dtStartDate = Convert.ToDateTime(complianceList[i].StartDate);
                            string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                            int userID = complianceList[i].UserID;

                            ComplianceInstance instance = new ComplianceInstance();
                            instance.ComplianceId = complianceID;
                            instance.CustomerBranchID = branchID;
                            instance.ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            if (complianceList[i].DepartmentID != 0)
                            {
                                long departmentID = complianceList[i].DepartmentID;
                                instance.DepartmentID = departmentID;
                            }
                            else
                            {
                                instance.DepartmentID = null;
                            }
                            if (!string.IsNullOrEmpty(complianceList[i].SequenceID))
                            {
                                string sequenceID = complianceList[i].SequenceID;
                                instance.SequenceID = sequenceID;
                            }
                            else
                            {
                                instance.SequenceID = null;
                            }
                            if (complianceList[i].Role == "Performer")
                            {

                                ComplianceAssignment assignment = new ComplianceAssignment();
                                assignment.UserID = userID;
                                assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                            }
                            if (complianceList[i].Role == "Reviewer")
                            {

                                ComplianceAssignment assignment1 = new ComplianceAssignment();
                                assignment1.UserID = userID;
                                assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                            }
                            if (complianceList[i].Role == "Approver")
                            {
                                ComplianceAssignment assignment1 = new ComplianceAssignment();
                                assignment1.UserID = userID;
                                assignment1.RoleID = RoleManagement.GetByCode("APPR").ID;
                                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                            }

                            if (complianceList[i].Cer_OwnerID != 0)
                            {
                                int cer_OwnerID = complianceList[i].Cer_OwnerID;
                                instance.Cer_OwnerUserID = cer_OwnerID;
                            }
                            else
                            {
                                instance.Cer_OwnerUserID = null;
                            }

                            if (complianceList[i].Cer_OfficerID != 0)
                            {
                                int cer_OfficerID = complianceList[i].Cer_OfficerID;
                                instance.Cer_OfficerUserID = cer_OfficerID;
                            }
                            else
                            {
                                instance.Cer_OfficerUserID = null;
                            }

                            Business.ComplianceManagement.DeactiveTempAssignmentDetails(TempAssignmentID);
                        }
                        if (assignments.Count != 0)
                        {
                            Business.ComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, customerID);
                            //  cvDuplicateEntry.IsValid = false;
                            // cvDuplicateEntry.ErrorMessage = "Compliance(s) Activated Successfully.";
                            // ValidationSummary2.CssClass = "alert alert-success";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Compliance(s) Assigned Successfully.", "Showalert();", true);

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                            setDateToGridView();
                        }
                        BindGrid(-1);
                    }
                    else
                    {
                        for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
                        {

                            GridViewRow row = grdToDeactivate.Rows[i];
                            int TempAssignmentID = Convert.ToInt32(grdToDeactivate.DataKeys[row.RowIndex]["ID"]);
                            Label lblComplianceInstanceID = (Label)row.FindControl("lblComplianceInstanceID");
                            CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");

                            if (chkDeActivate.Checked)
                            {
                                int dComplianceInstanceID = Convert.ToInt32(lblComplianceInstanceID.Text);
                                Business.ComplianceManagement.ActivateTempAssignmentDetails(TempAssignmentID, dComplianceInstanceID);
                            }
                        }
                        BindDeactivateGrid();

                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                    //setDateToGridView();
                }


                tbxStartDate.Text = "";
                ViewState["CHECKED_ITEMS"] = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {


        //        int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //        if (chkToActiveInstance.Checked == true)
        //        {
        //            List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();
        //            //int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
        //            {

        //                //List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

        //                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
        //                int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[row.RowIndex]["ID"]);
        //                Label lblComplianceID = (Label)row.FindControl("lblComplianceID");
        //                int complianceID = Convert.ToInt32(lblComplianceID.Text);
        //                //int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim()).ID;
        //                TextBox txtStartDate = (TextBox)row.FindControl("txtStartDate");
        //                //DateTime dtStartDate = Convert.ToDateTime(txtStartDate.Text);
        //                CheckBox chkActivate = (CheckBox)row.FindControl("chkActivate");
        //                Label lblRole = (Label)row.FindControl("lblRole");
        //                Label lblUserID = (Label)row.FindControl("lblUserID");
        //                int userID = Convert.ToInt32(lblUserID.Text);
        //                if (chkActivate.Checked)
        //                {

        //                    ComplianceInstance instance = new ComplianceInstance();
        //                    instance.ComplianceId = complianceID;
        //                    instance.CustomerBranchID = branchID;
        //                    instance.ScheduledOn = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                    //if (complianceList[i].Performer)
        //                    if (lblRole.Text == "Performer")
        //                    {

        //                        ComplianceAssignment assignment = new ComplianceAssignment();
        //                        assignment.UserID = userID;
        //                        assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
        //                    }
        //                    if (lblRole.Text == "Reviewer")
        //                    {

        //                        ComplianceAssignment assignment1 = new ComplianceAssignment();
        //                        assignment1.UserID = userID;
        //                        assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
        //                    }
        //                    if (lblRole.Text == "Approver")
        //                    {
        //                        ComplianceAssignment assignment1 = new ComplianceAssignment();
        //                        assignment1.UserID = userID;
        //                        assignment1.RoleID = RoleManagement.GetByCode("APPR").ID;
        //                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
        //                    }
        //                    Business.ComplianceManagement.DeactiveTempAssignmentDetails(TempAssignmentID);
        //                }
        //            }
        //            if (assignments.Count != 0)
        //            {
        //                Business.ComplianceManagement.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);

        //            }
        //            else
        //            {
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
        //                setDateToGridView();
        //            }


        //            BindGrid(branchID);
                    
        //        }
        //        else
        //        {
        //            for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
        //            {

        //                GridViewRow row = grdToDeactivate.Rows[i];
        //                int TempAssignmentID = Convert.ToInt32(grdToDeactivate.DataKeys[row.RowIndex]["ID"]);
        //                Label lblComplianceInstanceID = (Label)row.FindControl("lblComplianceInstanceID");
        //                CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");

        //                if (chkDeActivate.Checked)
        //                {
        //                    int dComplianceInstanceID = Convert.ToInt32(lblComplianceInstanceID.Text);
        //                    Business.ComplianceManagement.ActivateTempAssignmentDetails(TempAssignmentID, dComplianceInstanceID);
        //                }
        //            }
        //            BindDeactivateGrid(branchID);

        //        }

        //        tbxStartDate.Text = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
       

        //}

        private void setDateToGridView()
        {
            try
            {
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                    txt.Text = tbxStartDate.Text;
                }

                DateTime date = DateTime.Now;
                if (!string.IsNullOrEmpty(tbxStartDate.Text.Trim()))
                {
                    date = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkToActiveInstance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToActiveInstance.Checked == true)
            {
                if (tvFilterLocation.SelectedValue != "")
                {
                    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    BindGrid(nCustomerBranchID);
                }
                chkToDeActiveInstance.Checked = false;

            }
            else
            {
                grdComplianceRoleMatrix.Visible = false;
                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.DataBind();
            }
            
        }

        protected void chkToDeActiveInstance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToDeActiveInstance.Checked == true)
            {
                if (chkToDeActiveInstance.Checked == true)
                {
                    if (tvFilterLocation.SelectedValue != "")
                    {
                        int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        BindDeactivateGrid(nCustomerBranchID);
                    }
                    chkToActiveInstance.Checked = false;
                }
            }
            else
            {
                grdToDeactivate.Visible = false;
                grdToDeactivate.DataSource = null;
                grdToDeactivate.DataBind();
            }
        }

        protected void chkDeActivateSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkDeActivateSelectAll = (CheckBox)grdToDeactivate.HeaderRow.FindControl("chkDeActivateSelectAll");
            foreach (GridViewRow row in grdToDeactivate.Rows)
            {
                CheckBox chkDeActivate = (CheckBox)row.FindControl("chkDeActivate");
                if (chkDeActivateSelectAll.Checked == true)
                {
                    chkDeActivate.Checked = true;
                }
                else
                {
                    chkDeActivate.Checked = false;
                }
            }

        }

        protected void chkDeActivate_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkDeActivateSelectAll = (CheckBox)grdToDeactivate.HeaderRow.FindControl("chkDeActivateSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdToDeactivate.Rows.Count; i++)
            {
                GridViewRow row = grdToDeactivate.Rows[i];
                if (((CheckBox)row.FindControl("chkDeActivate")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;

                }

            }
            if (countCheckedCheckbox == grdToDeactivate.Rows.Count)
            {
                chkDeActivateSelectAll.Checked = true;
            }
            else
            {
                chkDeActivateSelectAll.Checked = false;
            }
           
          
        }
        protected void grdToDeactivate__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdToDeactivate_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //    int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                //    var ComplianceRoleMatrixList = Business.ComplianceManagement.GetTempAssignedDetails(nCustomerBranchID);

                //    if (direction == SortDirection.Ascending)
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Descending;
                //    }
                //    else
                //    {
                //        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //        direction = SortDirection.Ascending;
                //    }


                //    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                //    {
                //        if (field.SortExpression == e.SortExpression)
                //        {
                //            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                //        }
                //    }


                //    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                //    grdComplianceRoleMatrix.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //public SortDirection direction
        //{
        //    get
        //    {
        //        if (ViewState["dirState"] == null)
        //        {
        //            ViewState["dirState"] = SortDirection.Ascending;
        //        }
        //        return (SortDirection)ViewState["dirState"];
        //    }
        //    set
        //    {
        //        ViewState["dirState"] = value;
        //    }
        //}
        protected void grdToDeactivate_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddMatrixSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }
        protected void grdToDeactivate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindDeactivateGrid(nCustomerBranchID);

                grdToDeactivate.PageIndex = e.NewPageIndex;
                grdToDeactivate.DataBind();
               


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



      
    }
}