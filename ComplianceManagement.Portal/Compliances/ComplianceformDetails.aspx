﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceformDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceformDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
             <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
            <div id="divcustomer" runat="server"  style="float: left; margin-top: 5px; margin-left: 15px;">
                <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left;">
                    Select Customer:</label>
                <asp:DropDownList runat="server" ID="ddlCustomer" Height="22px" Width="220px" margin-left="7px" CssClass="txtbox" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Customer."
                            ControlToValidate="ddlCustomer" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
               

            </div>
             <div style="float: left; margin-top: 5px; margin-left: 45px;">
                <asp:Button id="btndownload" runat="server" OnClick="btndownload_Click" Text="Download" ValidationGroup="ComplianceValidationGroup"  />
            </div>
      
           
            <div style="width: 100%">

                <div id="divCompliancesForm" style="margin-top: 10px; margin-bottom: 10px">
                    <asp:GridView runat="server" ID="grdComplianceForm" AutoGenerateColumns="false" AllowSorting="true"
                        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"  OnPageIndexChanging="grdComplianceForm_PageIndexChanging"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="14"
                        Width="100%" Font-Size="12px">
                        <Columns>
                            <asp:BoundField HeaderText="Compliance ID" HeaderStyle-Width="5%" DataField="ComplianceID" SortExpression="ComplianceID" />
                            <asp:TemplateField HeaderText="Compliance Type" ItemStyle-Width="10%" SortExpression="ComplianceTypename">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                        <asp:Label ID="lblComplianceTypename" runat="server" Text='<%# Eval("ComplianceTypename") %>' ToolTip='<%# Eval("ComplianceTypename") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State" ItemStyle-Width="10%" SortExpression="State" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                        <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>' ToolTip='<%# Eval("State") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" ItemStyle-Width="10%" SortExpression="ComplianceCategory" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                        <asp:Label ID="lblComplianceCategory" runat="server" Text='<%# Eval("ComplianceCategory") %>' ToolTip='<%# Eval("ComplianceCategory") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Act" ItemStyle-Width="10%" SortExpression="ActName" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                        <asp:Label ID="lblActName" runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="10%" SortExpression="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sections" ItemStyle-Width="10%" SortExpression="Sections" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                        <asp:Label ID="lblSections" runat="server" Text='<%# Eval("Sections") %>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Frequency" ItemStyle-Width="10%" SortExpression="Frequency" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                        <asp:Label ID="lblFrequency" runat="server" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Compliance Type" ItemStyle-Width="10%" SortExpression="ComplianceType" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                        <asp:Label ID="lblComplianceType" runat="server" Text='<%# Eval("ComplianceType") %>' ToolTip='<%# Eval("ComplianceType") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                          

                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

            </div>
        </ContentTemplate>
       <Triggers>
                <asp:PostBackTrigger ControlID="btndownload" />
            </Triggers>
    </asp:UpdatePanel>
</asp:Content>
