﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CustomerAssignment : System.Web.UI.Page
    {
        public static List<long?> actList = new List<long?>();
        private int userId = AuthenticationHelper.UserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Boolean isAccess = false;
                if(AuthenticationHelper.UserID == 8877 || AuthenticationHelper.UserID == 198 || AuthenticationHelper.UserID == 10924 || AuthenticationHelper.UserID == 6966 || AuthenticationHelper.UserID == 9036)
                {
                    isAccess = true;
                }
                
                if (isAccess == false)
                {
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                    {

                        if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                        {
                            BindUsers();
                            BindCustomer();
                            BindGrids();
                            actList.Clear();
                        }
                        else
                        {
                            //added by rahul on 12 June 2018 Url Sequrity
                            FormsAuthentication.SignOut();
                            Session.Abandon();
                            FormsAuthentication.RedirectToLoginPage();
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
            }
        }
  
        public static List<Customer> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.ServiceProviderID == 95 
                             select row); 
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static Object GetAllUser()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.RoleID==12  
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();
                 return query;                                                                
            }
        }
        private void BindCustomer()
        {
            try
            {   
                rptCustomers.DataSource = GetAll().ToList();
                rptCustomers.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<GetAllUserData_Result> GetAllData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.GetAllUserData()
                                           select row).ToList(); 
                return complianceCategorys ;
                
            }
        }
        private void BindGrids()
        {
            try
            {     
                  var Customerlist = GetAllData();
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        Customerlist = Customerlist.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        Customerlist = Customerlist.Where(entry => entry.UserName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.CustomerName.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    }
                }
               grdCustomer.DataSource = Customerlist;
                grdCustomer.DataBind();
                upWidget.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static CustomerAssignmentDetail GetByID(int id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.CustomerAssignmentDetails
                           where row.ID == id && row.IsDeleted == false
                           select row).SingleOrDefault();

                return act;
            }
        }
        public static List<int> GetAllAssignedID(long id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MappedIDs = (from row in entities.CustomerAssignmentDetails
                                    where row.ID == id
                                    select (int)row.CustomerID).ToList();

                return MappedIDs;
            }
        }
        public static void DeleteCustomerData(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserMastertoDelete = (from row in entities.CustomerAssignmentDetails
                                          where row.ID == ID
                                          select row).FirstOrDefault();
                if (UserMastertoDelete != null)
                {
                    UserMastertoDelete.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
        }
        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Widget"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["ID"] = ID;
                    CustomerAssignmentDetail customermaster = GetByID(ID);
                    ddlNewUsers.SelectedValue = customermaster.UserID.Value.ToString();
                    txtComplince.Text = "< Select >";
                    var vGetMappedIDs = GetAllAssignedID(ID); 
                    foreach (RepeaterItem aItem in rptCustomers.Items)
                    {
                        CheckBox chkCustomer = (CheckBox)aItem.FindControl("chkCustomer");
                        chkCustomer.Checked = false;
                        CheckBox CustomerSelectAll = (CheckBox)rptCustomers.Controls[0].Controls[0].FindControl("CustomerSelectAll");

                        for (int i = 0; i <= vGetMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblcustomerid")).Text.Trim() == vGetMappedIDs[i].ToString())
                                chkCustomer.Checked = true;
                        }
                        //if ((rptCustomers.Items.Count) == (vGetMappedIDs.Count))
                        //    CustomerSelectAll.Checked = true;
                    
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divActDialog\").dialog('open')", true);
                    upWidget.Update();                          
                }
                else if (e.CommandName.Equals("DELETE_Widget"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    DeleteCustomerData(ID);
                    BindGrids();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static CustomerAssignmentDetail SelectCustomer(int userID = -1, int customerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignmentData = (from row in entities.CustomerAssignmentDetails
                                              where row.UserID == userID && row.CustomerID == customerID
                                              && row.IsDeleted==false
                                              select row).FirstOrDefault();
                return AssignmentData;
            }
        }


        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = e.NewPageIndex;
                BindGrids();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindUsers()
        {
            try
            {
                ddlNewUsers.DataTextField = "Name";
                ddlNewUsers.DataValueField = "ID";

                ddlNewUsers.DataSource = GetAllUser();
                ddlNewUsers.DataBind();

                ddlNewUsers.Items.Insert(0, new ListItem("< Select User >", "-1"));  
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlUserlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAddWidget_Click(object sender, EventArgs e)
        {
            try
            {     
                ViewState["Mode"] = 0;
                saveopo.Value = "false";
                ddlNewUsers.SelectedValue = "-1";
                txtComplince.Text = "< Select Customer>";
                foreach (RepeaterItem aItem in rptCustomers.Items)
                {
                    CheckBox chkcompliance = (CheckBox)aItem.FindControl("chkCustomer");
                    chkcompliance.Checked = false;
                    CheckBox ComplianceSelectAll = (CheckBox)rptCustomers.Controls[0].Controls[0].FindControl("CustomerSelectAll");
                    ComplianceSelectAll.Checked = false;
                }
                upWidget.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
           
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindGrids();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void UpdateCustomerMaster(int UserID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CustomerAssignmentDetail widgetmaster = (from row in entities.CustomerAssignmentDetails
                                             where row.UserID == UserID
                                           //  && row.IsDeleted == false
                                             && row.CustomerID == customerID
                                             select row).FirstOrDefault();
                if (widgetmaster != null)
                {
                    widgetmaster.UserID = UserID;
                    widgetmaster.CustomerID = customerID;
                    entities.SaveChanges();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int userID = -1;
                int ID = -1;
                if (!string.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                {
                    if (ddlNewUsers.SelectedValue != "-1")
                    {
                        userID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                    }
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    ID = Convert.ToInt32(ViewState["ID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    foreach (RepeaterItem aItem in rptCustomers.Items)
                    {
                        var cid1 = Convert.ToInt32(((Label)aItem.FindControl("lblcustomerid")).Text.Trim());
                        var data = SelectCustomer(userID, cid1);
                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCustomer");

                        if (chkCompliance.Checked)
                        {
                            if (data == null)
                            {
                                var cid = Convert.ToInt32(((Label)aItem.FindControl("lblcustomerid")).Text.Trim());
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    CustomerAssignmentDetail objcategory = new CustomerAssignmentDetail()
                                    {
                                        // WidgetID = obj.ID,
                                        UserID = userID,
                                        CustomerID = cid,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        IsDeleted = false,
                                        CreatedDate = DateTime.Now,
                                        UpdatedDate = DateTime.Now,
                                        UpdatedBy = AuthenticationHelper.UserID

                                    };
                                    entities.CustomerAssignmentDetails.Add(objcategory);
                                    entities.SaveChanges();
                                }
                                saveopo.Value = "true";
                                CustomDuplicateEntry.IsValid = false;
                                CustomDuplicateEntry.ErrorMessage = "Customer Assigned successfully";
                            }
                            else
                            {
                                saveopo.Value = "true";
                                CustomDuplicateEntry.IsValid = false;
                                CustomDuplicateEntry.ErrorMessage = "Customer already Assigned";
                                //vsLicenseListPage.CssClass = "alert alert-success";  
                            }

                            //vsLicenseListPage.CssClass = "alert alert-success";  
                        }

                    }
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    List<CustomerAssignmentDetail> objCustomer = new List<CustomerAssignmentDetail>();
                    if (!string.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                    {
                        if (ddlNewUsers.SelectedValue != "-1")
                        {
                            userID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                        }
                    }
                    var Groupdetails = GetByID(ID);
                    if (Groupdetails != null)
                    {
                        foreach (RepeaterItem aItem in rptCustomers.Items)
                        {
                            CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCustomer");
                            var cid = Convert.ToInt32(((Label)aItem.FindControl("lblcustomerid")).Text.Trim());
                            UpdateCustomerMaster(userID, cid);
                            if (chkCompliance.Checked)
                            {     
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        CustomerAssignmentDetail objcategory = new CustomerAssignmentDetail()
                                        {    
                                            UserID = userID,
                                            CustomerID = cid,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            IsDeleted = false,
                                            CreatedDate = DateTime.Now,
                                            UpdatedDate = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID
                                        };
                                        objCustomer.Add(objcategory);
                                    CreateOrUpdateAssignedMapping(objCustomer, userID,cid, true);
                                        entities.SaveChanges();
                                    }
                                    saveopo.Value = "true";
                                    CustomDuplicateEntry.IsValid = false;
                                    CustomDuplicateEntry.ErrorMessage = "Customer Assigned updated successfully";
                                } 
                        }
                    }
                }
                BindGrids();
                upWidgetList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateOrUpdateAssignedMapping(List<CustomerAssignmentDetail> customerassign, int userid,int customerid, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                   
                    List<CustomerAssignmentDetail> mappedSubIndustrys = (from row in entities.CustomerAssignmentDetails
                                                                         where row.UserID == userid
                                                                         && row.CustomerID==customerid
                                                             select row).ToList();

                    foreach (CustomerAssignmentDetail Subindustry in mappedSubIndustrys)
                    {
                        entities.CustomerAssignmentDetails.Remove(Subindustry);
                        entities.SaveChanges();
                    }

                    int lastID = (from row in entities.CustomerAssignmentDetails
                                  orderby row.ID descending
                                  select row.ID).FirstOrDefault();
                }

                foreach (CustomerAssignmentDetail mappingSubIndustry in customerassign)
                {
                    entities.CustomerAssignmentDetails.Add(mappingSubIndustry);
                    entities.SaveChanges();
                }
            }
        }

       
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActDialog\").dialog('close')", true);
            BindGrids();
        }
     

        protected void upWidget_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //   ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeACTList", string.Format("initializeJQueryUI('{0}', 'dvACT');", txtACTName.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComplianceList", string.Format("initializeJQueryUI1('{0}', 'dvCompliance');", txtComplince.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideACTList", "$(\"#dvACT\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideComplianceList", "$(\"#dvCompliance\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
    
    }       
}
