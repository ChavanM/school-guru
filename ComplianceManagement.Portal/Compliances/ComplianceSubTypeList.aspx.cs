﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceSubTypeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }


        public void BindGrid()
        {
            var GridData = Business.ComplianceManagement.GetComSubTypeData();
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            grdCompliances.DataSource = GridData;
            grdCompliances.DataBind();
        }

        protected void ddlNatureOfCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;
                ddlNatureOfCompliance.SelectedIndex = -1;

                upComSubType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#DivComplianceSubTypeList\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    ViewState["Mode"] = 1;
                    int complianceSubID = Convert.ToInt32(e.CommandArgument);
                    var ComEditData = Business.ComplianceManagement.GetComSubTypeDataByID(complianceSubID);
                    if (ComEditData.Count > 0)
                    {
                        foreach (var item in ComEditData)
                        {
                            ddlNatureOfCompliance.SelectedValue = Convert.ToString(item.NatureOfComplianceID);
                            tbxComsubID.Text = Convert.ToString(item.ID);
                            tbxName.Text = item.Name;
                            tbxDescription.Text = item.Description;
                        }
                    }
                    upComSubType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#DivComplianceSubTypeList\").dialog('open')", true);
                    BindGrid();
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    int complianceSubID = Convert.ToInt32(e.CommandArgument);
                    Business.ComplianceManagement.DeleteComSubTypeDataByID(complianceSubID);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Compliance Sub Type Deleted Successfully.";
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int NatureComID = -1;
                if (ddlNatureOfCompliance.SelectedValue != "-1")
                {
                    NatureComID = Convert.ToInt32(ddlNatureOfCompliance.SelectedValue);
                }
                ComplianceSubType objComSubType = new ComplianceSubType()
                {
                    NatureOfComplianceID = Convert.ToByte(NatureComID),
                    Name = tbxName.Text,
                    Description = tbxDescription.Text
                };
                if ((int) ViewState["Mode"] == 0)
                {
                    Business.ComplianceManagement.CreateComSubTypeList(objComSubType);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#DivComplianceSubTypeList\").dialog('close')", true);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Compliance Sub Type Saved Successfully.";
                    tbxComsubID.Text = string.Empty;
                    tbxName.Text = string.Empty;
                    tbxDescription.Text = string.Empty;
                    ddlNatureOfCompliance.SelectedIndex = -1;
                }

                if ((int) ViewState["Mode"] == 1)
                {
                    objComSubType.ID = Convert.ToInt32(tbxComsubID.Text);
                    Business.ComplianceManagement.UpdateComSubTypeList(objComSubType);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#DivComplianceSubTypeList\").dialog('close')", true);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Compliance Sub Type Updated Successfully.";
                    tbxComsubID.Text = string.Empty;
                    tbxName.Text = string.Empty;
                    tbxDescription.Text = string.Empty;
                    ddlNatureOfCompliance.SelectedIndex = -1;
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public  string NutureofCompName(int NatureOfComplianceID)
        {
            // string Names = com.VirtuosoITech.ComplianceManagement.Business.Data.Enumerations.GetEnumByID(NaturCompID);
            //string Names =(string)  NatureOfCompliance.(NaturCompID);
            //return Names;
            string stringValue = string.Empty;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var query = (from row in entities.NatureOfCompliances
                             where row.ID == NatureOfComplianceID
                             select row.Name).FirstOrDefault();

                stringValue = query;
                return stringValue;
            }
        }

    }
}