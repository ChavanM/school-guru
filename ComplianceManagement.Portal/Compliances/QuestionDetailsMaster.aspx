﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="QuestionDetailsMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.QuestionDetailsMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--  <link href="~/select2/select2.min.css" rel="stylesheet" />--%>
    <link href="../content/bootstrap.css" rel="stylesheet" />
    <%--<link href="../content/bootstrap.css" rel="stylesheet" />--%>
    <link href="../content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../masters/bootstrap.min.js"></script>
    <script type="text/javascript" src="../select2/select2.min.js"></script>
    <script type="text/javascript" src="../select2/select2.full.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -6px;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px !important;
            color: inherit;
        }

        .btn .caret {
            margin-left: 170px;
        }

        ul.dropdown-menu.dropdown-menu-form.ng-scope {
            width: 280px;
        }

        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
            height: 28px;
            padding-top: 3px;
            width: 280px;
            padding-right: 116px;
            /*margin-left: 50px;*/
        }

        .dropdown-menu .divider {
            height: 1px;
            margin: 3px 0;
            overflow: hidden;
            background-color: #e5e5e5;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,0.2);
            border-radius: 6px;
            outline: 0;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,0.5);
            box-shadow: 0 3px 9px rgba(0,0,0,0.5);
            background-clip: padding-box;
            height: 369px;
        }

        .select2-container--open .select2-dropdown {
            left: 0;
            /*margin-left: -66px;*/
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: grey !important;
            line-height: 28px;
            /*margin-right: 48px;*/
            text-align: left;
        }

        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        .k-grid tbody tr {
            height: 38px;
        }
    </style>
    <script type="text/javascript">
        function BindStatus() {
            $("#dropdownQuestionName").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                optionLabel: "Select Question",
                dataTextField: "MasterName",
                dataValueField: "MasterName",
                change: function (e) {
                    FilterAll();
                },
                dataSource:
                    {
                        severFiltering: true,
                        transport: {
                            read: '../Question/GetMastersfromDB'
                        },
                    }
            });
        }
        function BindControl() {
            $("#dropdownControlName").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                optionLabel: "Select Control",
                dataTextField: "ControlName",
                dataValueField: "ControlName",
                change: function (e) {
                    FilterAll();
                },
                dataSource:
                    {
                        severFiltering: true,
                        transport: {
                            read: '../Question/GetAnswerControl'
                        },
                    }
            });
        }
        function FilterAll() {
            if (($("#dropdownControlName").val() != 0 && $("#dropdownControlName").val() != -1 && $("#dropdownControlName").val() != "")
                ) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownControlName").val() != 0 && $("#dropdownControlName").val() != -1 && $("#dropdownControlName").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "ControlName", operator: "eq", value: $("#dropdownControlName").val()
                    });
                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }
        $(document).ready(function () {
            Bindgrid();
            BindStatus();
            BindControl();
        });
        Bindgrid();
        BindStatus();
        BindControl();
        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: '../Question/getAll'
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    if (this.ReminderStatus == 0) { template: "#=pending" }
                },
                //dataBound: OnGridDataBoundAdvanced,
                columns: [
                    //{
                    //    title: "Sr",
                    //    template: "#= ++record #",
                    //    width: "58px",
                    //},

                     { hidden: true, field: "ControlID", title: "ControlID" },

            {
                field: "QuestionID", title: 'QuestionID',
                attributes: {
                    style: 'white-space: nowrap;'

                }, filterable: {
                    extra: false,
                    operators: {
                        string: {
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains"
                        }
                    }
                }
            },

            {
                field: "QuestionName", title: 'Question Name',
                attributes: {
                    style: 'white-space: nowrap;'

                }, filterable: {
                    extra: false,
                    operators: {
                        string: {
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains"
                        }
                    }
                }
            },
                {
                    field: "ControlName", title: 'Control Name',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    command: [
                        { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                        { name: "edit1", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-deleteuser" },
                        //{ name: "download", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                        { name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" }
                    ], title: "Action", lock: true, width: "15%;",// width: 150,
                }
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Delete";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-view",
                content: function (e) {
                    return "View";
                }
            });           
        }

        function OpenOverdueComplainceList(e) {
            $('#divreports').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '650px');
            $('.modal-dialog').css('width', '98%');
            $('#showdetails').attr('src', "../Compliances/Questions.aspx");
            e.preventDefault();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4" style="margin-left: -18px; display: inline-block">
                <h1 class="modal-title" style="text-align: left; height: 20px; font-size: 16px;"><b>Question Master</b></h1>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-" style="text-align: right; display: inline-block; margin-left: 125px">

                <button class="btn btn-primary btn-sm " id="lnkOverdueCompliance" style="margin-left: 27px; margin-bottom: 6px;"
                    onclick="OpenOverdueComplainceList(event)">
                    <span class="glyphicon glyphicon-plus"></span>
                    Add</button>


            </div>
        </div>
    </div>
    

    <div class="row" style="padding-bottom: 10px; margin-left: -4px; padding-top: 2px;">
        <input id="dropdownControlName" data-placeholder="Status" style="width: 15%;" />
    </div>


    <div class="row" style="padding-top: 12px; margin-right: -11px;">
        <div id="grid" style="width:95%; margin-left : 10px; margin-right: 10px;"></div>
    </div>

    
      <script type="text/javascript">
               
        function fchangeheight(obj,cal)
        {
            $('iframe#showdetails').removeAttr('height');
            $('iframe#showdetails').attr('height',cal)
        }
    </script>

    <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 98%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -12px;" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="blank.html" scrolling="no" width="98%" height="100%" frameborder="0"></iframe>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
