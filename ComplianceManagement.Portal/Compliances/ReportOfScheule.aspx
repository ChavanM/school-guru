﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ReportOfScheule.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ReportOfScheule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <style type="text/css">
        /*.custom-combobox-input {
            margin-left:150px;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>


            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlfiltercustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfiltercustomer_SelectedIndexChanged">
                        </asp:DropDownList>


                    </td>

                    <td>
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" Visible="true" />
                    </td>
                </tr>

            </table>

            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdreportschedule" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdreportschedule_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />

                        <asp:TemplateField HeaderText="Customer Name" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="CustomerName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="User Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="Username">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("Username") %>' ToolTip='<%# Eval("Username") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Frequency" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="FrequencyName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDITSchedule" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE"
                                    CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you sure you want to delete this entry ?');"><img src="../Images/delete_icon.png" alt="Delete" title="Delete"/></asp:LinkButton>

                            </ItemTemplate>                           
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary" ID="vscomListPage"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>

                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select Customer."
                            ControlToValidate="ddlCustomer" runat="server" ValidationGroup="ComplianceValidationGroup1" InitialValue="-1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList runat="server" ID="ddlUser" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select User."
                            ControlToValidate="ddlUser" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Frequency</label>
                        <asp:DropDownList runat="server" ID="ddlFrequency" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="Monthly" Value="0" />
                            <asp:ListItem Text="Weekly" Value="1" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select frequency."
                            ControlToValidate="ddlFrequency" runat="server" ValidationGroup="ComplianceValidationGroup1" InitialValue="-1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divmonthlyday" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Day</label>
                        <asp:DropDownList runat="server" ID="ddlmonthlyday" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please select  day."
                            ControlToValidate="ddlmonthlyday" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divweeklyday" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Day</label>
                        <asp:DropDownList runat="server" ID="ddlweeklyday" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" AutoPostBack="true">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="Sunday" Value="0" />
                            <asp:ListItem Text="Monday" Value="1" />
                            <asp:ListItem Text="Tuesday" Value="2" />
                            <asp:ListItem Text="Wenesday" Value="3" />
                            <asp:ListItem Text="Thursday" Value="4" />
                            <asp:ListItem Text="Friday" Value="5" />
                            <asp:ListItem Text="Saturday" Value="6" />

                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please select  day."
                            ControlToValidate="ddlweeklyday" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>


                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" OnClick="btnSave_Click" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    <script type="text/javascript">
        $(function () {
            $('#divComplianceScheduleDialog').dialog({
                height: 500,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Compliance Schedule Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            initializeCombobox();
        });
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlUser.ClientID %>").combobox();
            $("#<%= ddlFrequency.ClientID %>").combobox();
            $("#<%= ddlmonthlyday.ClientID %>").combobox();
            $("#<%= ddlweeklyday.ClientID %>").combobox();
            $("#<%= ddlfiltercustomer.ClientID %>").combobox();



        }
    </script>
</asp:Content>
