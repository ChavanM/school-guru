﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ChangeRiskofCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ChangeRiskofCompliance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript">
      $(function () {
          $(document).tooltip();
      });
        var gridViewId = '#<%= grdCompliances.ClientID %>';
        function checkAll(selectAllCheckbox) {
            //get all checkbox and select it
            $('td :checkbox', gridViewId).prop("checked", selectAllCheckbox.checked);
        }
        function unCheckSelectAll(selectCheckbox) {
            //if any item is unchecked, uncheck header checkbox as also
            if (!selectCheckbox.checked)
                $('th :checkbox', gridViewId).prop("checked", false);
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function validateCheckBoxes() {
            debugger;
            var isValid = false;
            var gridView = document.getElementById('<%= grdCompliances.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            //alert("Please select atleast one checkbox");
            document.getElementById("lblcompliance").innerHTML = "Please select atleast one compliance";
            return false;
        }
      function OnTreeClick(evt) {
          var src = window.event != window.undefined ? window.event.srcElement : evt.target;
          var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
          if (isChkBoxClick) {
              var parentTable = GetParentByTagName("table", src);
              var nxtSibling = parentTable.nextSibling;
              if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
              {
                  if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                  {
                      //check or uncheck children at all levels
                      CheckUncheckChildren(parentTable.nextSibling, src.checked);
                  }
              }
              //check or uncheck parents at all levels
              CheckUncheckParents(src, src.checked);
          }
      }
    </script>
        <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
        
        .ui-state-active{
                border: 1px solid #fad42e !important;
                background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
                color: #363636 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional"  OnLoad="upComplianceList_Load">
        <ContentTemplate>


             <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 0px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMsg1" runat="server" Text=""></asp:Label>
                                  
                                    <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>

           
            <table width="100%">
                <tr>
                    <td style="width:15%;">
                             <div id="lblcustomer" runat="server">
                            <label style="width: 130px; margin-left: 10px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Customer :
                            </label>
                                 </div>
                        </td>
                        <td style="width:35%;">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 27px; width: 300px;margin-right: 2px;margin-left: -28px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup"
                                Display="None" />
                                </div>
                        </td>

                     <td>
                        <asp:DropDownList runat="server" ID="ddlAct" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" 
                            Style="padding: 0px; margin: 0px; height: 28px; width: 320px;" CssClass="txtbox" AutoPostBack="true"  />
                                          
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceType" Style="padding: 0px; margin: 0px; height: 28px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true"  OnSelectedIndexChanged="ddlComplinceType_SelectedIndexChanged">
                            <asp:ListItem Text="< Select Compliance Type >" Value="-1" />
                            <asp:ListItem Value="0">Statutory </asp:ListItem>
                            <asp:ListItem Value="1">Checklist </asp:ListItem>
                            <asp:ListItem Value="2">Event based </asp:ListItem>
                        </asp:DropDownList>
                    </td>
              
                    <td class="td1">
                        <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;margin-left: -31px;">
                                Filter :
                            </label>
                        </td>
                    <td class="td2">
                        <asp:TextBox runat="server" ID="tbxFilter" OnTextChanged="tbxFilter_TextChanged" Width="250px" style="margin-left: -94px;height: 20px;" MaxLength="50" AutoPostBack="true" />
                    </td>
                     <td style="display:none;">
                           <asp:LinkButton ID="lnkSampleFormat" runat="server" class="newlink" data-placement="bottom" data-toggle="tooltip" Font-Underline="True"  Text="Sample Format(Excel)" ToolTip="Download Sample Excel Document Format for User/ Customer Branch Upload"></asp:LinkButton>
                            
                       
                    </td>
                    <td style="width: 15%; padding-right: 20px;" align="right">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                </asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExport" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                   
                 
                   
                </tr>
                <tr>
                 
                    <tr>
                      
                         <td class="td1" align="right">
                             <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: -2px;">
                                Select Location:
                            </label>
                        </td>

                        <td class="td2" align="left">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin-left:-29px; height: 22px; width: 296px;"
                                CssClass="txtbox" />
                            <div style="margin-left: -29px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true"  ShowCheckBoxes="All" onclick="OnTreeClick(event)" 
                                    >
                                </asp:TreeView>
                                   <div id="bindelement" style="background: white;height: 292px;display:none; width: 390px;border: 1px solid;overflow: auto;"></div>
    
                                <asp:Button ID="btnlocation" runat="server"  OnClick="btnlocation_Click" Text="select"/> 
                                    <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear"  /> 
                            </div>
                        </td>
                    <%--<td>--%>
                        <%--<div style="margin-bottom: 7px" id="dvSampleForm" runat="server">--%>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                           </td>
                          <td class="td1">
                          <asp:FileUpload ID="MasterFileUpload" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                        <%--</div>--%>
                          </td>
                    <%--</td>--%>
                    
                     <td>
                     
                        <asp:Button ID="btnUploadFile" runat="server" OnClick="btnUploadFile_Click"  ValidationGroup="oplValidationGroup" Text="Upload"  />
                   
                    </td>
                  </tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="18" Width="100%"
                    Font-Size="12px" DataKeyNames="ID" OnRowDataBound="grdCompliances_RowDataBound" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                         <asp:BoundField DataField="ID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ID" />                        
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblComplianceInstenaceID" runat="server"  Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                <asp:Label ID="lblComplianceID" runat="server"  Text='<%# Eval("ID") %>'></asp:Label>
                                 <asp:Label ID="lblcrisk" runat="server"  Text='<%# Eval("ClientRisk") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      
                        <asp:TemplateField  HeaderText="Location" ItemStyle-Width="200px">
                             <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                            
                        <asp:BoundField DataField="FrequencyName" HeaderText="Frequency" ItemStyle-Width="180px" />                      
                        <asp:BoundField DataField="AvacomRisk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="AvacomRisk" />
                        <asp:TemplateField HeaderText="Client Risk" ItemStyle-Width="100px">                        
                            <ItemTemplate>                              
                                    <asp:DropDownList ID="ddlrisk" runat="server">
                                    </asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="50px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="Chkselection" runat="server" onclick="unCheckSelectAll(this)" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
            <div style="margin-bottom: 7px; margin-left: 620px; margin-top: 10px;">
                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btn_SaveClick" CssClass="button" ValidationGroup="cvUploadUtilityPage" />
            </div>
        </ContentTemplate>
           <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
