﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ReportOfScheule : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomersList(userID);               
            }               
        }
        private void BindCustomersList(int UserID)
        {
            try
            {               
                var details = Assigncustomer.GetAllCustomer(userID);

                ddlfiltercustomer.DataTextField = "Name";
                ddlfiltercustomer.DataValueField = "ID";
                ddlfiltercustomer.DataSource = details;
                ddlfiltercustomer.DataBind();
                ddlfiltercustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlfiltercustomer.SelectedIndex = -1;

                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = details;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlCustomer.SelectedIndex = -1;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindUserList(int custid)
        {
            try
            {               
                var details = GetAllNonAdminUser(custid);
                ddlUser.DataTextField = "Name";
                ddlUser.DataValueField = "ID";
                ddlUser.DataSource = details;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("< Select User >", "-1"));
                ddlUser.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static object GetAllNonAdminUser(int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var users = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID==custid
                             && row.RoleID == 7
                             select new
                             {
                                 row.ID,
                                 Name=row.FirstName + " " +  row.LastName
                             });
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }

        private void BindDays()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlmonthlyday.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }

                ddlmonthlyday.Items.Insert(0, new ListItem("< Select>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                ddlCustomer.SelectedValue = "-1";
                ddlUser.SelectedValue = "-1";
                ddlmonthlyday.SelectedValue = "-1";
                ddlweeklyday.SelectedValue = "-1";
                ddlFrequency.SelectedValue = "-1";
                saveopo.Value = "false";
                ViewState["Mode"] = 0;
                upComplianceScheduleDetails.Update();                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long customerid = -1;
                long frequencyid = -1;
                long userid = -1;
                long weeklyday = -1;
                long monthlyday = -1;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && Convert.ToString(ddlCustomer.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlUser.SelectedValue) && Convert.ToString(ddlUser.SelectedValue) != "-1")
                {
                    userid = Convert.ToInt32(ddlUser.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue) && Convert.ToString(ddlFrequency.SelectedValue) != "-1")
                {
                    frequencyid = Convert.ToInt32(ddlFrequency.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlweeklyday.SelectedValue) && Convert.ToString(ddlweeklyday.SelectedValue) != "-1")
                {
                    weeklyday = Convert.ToInt32(ddlweeklyday.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlmonthlyday.SelectedValue) && Convert.ToString(ddlmonthlyday.SelectedValue) != "-1")
                {
                    monthlyday = Convert.ToInt32(ddlmonthlyday.SelectedValue);
                }
             
               
                if (customerid != -1 && userid != -1 && frequencyid != -1 )
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        tbl_Reportofscheduledetails cust = new tbl_Reportofscheduledetails()
                        {
                            Customerid = Convert.ToInt32(customerid),
                            Userid = Convert.ToInt32(userid),
                            Frequency = Convert.ToInt32(frequencyid),
                            monthlyday = Convert.ToInt32(monthlyday),
                            weeklyday= Convert.ToInt32(weeklyday),
                            CreatedBy = AuthenticationHelper.UserID,
                            CrateOn = DateTime.Now,
                            IsActive = true,
                           
                        };
                        if ((int)ViewState["Mode"] == 1)
                        {
                            cust.ID = Convert.ToInt32(ViewState["CBCTID"]);
                        }

                        if ((int)ViewState["Mode"] == 0)
                        {
                           
                            entities.tbl_Reportofscheduledetails.Add(cust);
                            entities.SaveChanges();

                            CustomValidator2.IsValid = false;
                            CustomValidator2.ErrorMessage = "Record saved successfully";
                            vscomListPage.CssClass = "alert alert-success";
                            
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            
                                Update(cust);
                                CustomValidator2.IsValid = false;
                                CustomValidator2.ErrorMessage = "Record updated successfully";
                                vscomListPage.CssClass = "alert alert-success";
                           
                        }
                        BindComplianceScheduleReport();
                        upComplianceTypeList.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Something went wrong, Please try again";
                vscomListPage.CssClass = "alert alert-danger";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Update(tbl_Reportofscheduledetails ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_Reportofscheduledetails ltlmToUpdate = (from row in entities.tbl_Reportofscheduledetails
                                                           where row.ID == ltlm.ID
                                                           select row).FirstOrDefault();

                ltlmToUpdate.Customerid = ltlm.Customerid;
                ltlmToUpdate.Userid = ltlm.Userid;
                ltlmToUpdate.Frequency = ltlm.Frequency;
                ltlmToUpdate.weeklyday = ltlm.weeklyday;
                ltlmToUpdate.monthlyday = ltlm.monthlyday;
                entities.SaveChanges();
            }
        }
        private void BindComplianceScheduleReport()
        {
            try
            {
                int customerid = -1;
              
                if (!string.IsNullOrEmpty(ddlfiltercustomer.SelectedValue) && Convert.ToString(ddlfiltercustomer.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlfiltercustomer.SelectedValue);
                }
               

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    grdreportschedule.DataSource = null;
                    grdreportschedule.DataBind();
                    var ClientcomplianceDetails = (from row in entities.SP_ReportOfSchedule(customerid)
                                                   select row).ToList();

                    if (ClientcomplianceDetails.Count > 0)
                    {
                        grdreportschedule.DataSource = ClientcomplianceDetails;
                        grdreportschedule.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdreportschedule_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EDITSchedule"))
            {
                int ID = Convert.ToInt32(e.CommandArgument);
                ViewState["Mode"] = 1;
                ViewState["CBCTID"] = ID;
                var ltlm = GetByID(ID);
                ddlCustomer.SelectedValue = ltlm.Customerid.ToString();             
                BindUserList(ltlm.Customerid);
                ddlUser.SelectedValue = ltlm.Userid.ToString();

                if (!string.IsNullOrEmpty(Convert.ToString(ltlm.Frequency)))
                {
                    ddlFrequency.SelectedValue = ltlm.Frequency.ToString();
                    if (Convert.ToInt32(ddlFrequency.SelectedValue) == 0)
                    {
                        divmonthlyday.Visible = true;
                        divweeklyday.Visible = false;
                        BindDays();
                        if (!string.IsNullOrEmpty(Convert.ToString(ltlm.monthlyday)))
                        {
                            ddlmonthlyday.SelectedValue = Convert.ToString(ltlm.monthlyday);
                        }
                    }
                    else if (Convert.ToInt32(ddlFrequency.SelectedValue) == 1)
                    {
                        divweeklyday.Visible = true;
                        divmonthlyday.Visible = false;
                        if (!string.IsNullOrEmpty(Convert.ToString(ltlm.weeklyday)))
                        {
                            ddlweeklyday.SelectedValue = Convert.ToString(ltlm.weeklyday);
                        }
                    }
                }
                upComplianceScheduleDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            else if (e.CommandName.Equals("DELETE"))
            {
                int ID = Convert.ToInt32(e.CommandArgument);
                Delete(ID);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Record deleted successfully";
                vscomListPage.CssClass = "alert alert-success";
                BindComplianceScheduleReport();
            }
        }
        public static void Delete(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_Reportofscheduledetails ltlmToUpdate = (from row in entities.tbl_Reportofscheduledetails
                                                           where row.ID == ID
                                                           select row).FirstOrDefault();               
                ltlmToUpdate.IsActive = false;
               entities.SaveChanges();
            }
        }
        public static tbl_Reportofscheduledetails GetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_Reportofscheduledetails
                            where row.ID == ID && row.IsActive == true
                            select row).FirstOrDefault();
                return data;
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int custid = -1;
            custid = Convert.ToInt32(ddlCustomer.SelectedValue);
            BindUserList(custid);
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Convert.ToInt32(ddlFrequency.SelectedValue)==0)
            {
                divmonthlyday.Visible = true;
                divweeklyday.Visible = false;
                BindDays();
            }
            else if(Convert.ToInt32(ddlFrequency.SelectedValue) == 1)
            {
                divweeklyday.Visible = true;
                divmonthlyday.Visible = false;
            }
        }

        protected void ddlfiltercustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceScheduleReport();
        }
    }
}