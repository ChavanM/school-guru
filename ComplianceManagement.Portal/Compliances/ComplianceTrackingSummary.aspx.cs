﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Text;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceTrackingSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    BindMyCompliances(customerID);
                    BindPendingCompliances(customerID);
                    BindCompliancesByLocation(customerID);

                    if (AuthenticationHelper.Role == "EXCT")
                    {
                        hpAssignLocation.Visible = hpAssignUser.Visible = hpAssignCompliance.Visible = false;
                    }
                    else
                    {
                        hpAssignLocation.Visible = hpAssignUser.Visible = hpAssignCompliance.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }

        }

        private void BindPendingCompliances(int customerID)
        {
            try
            {
                chrtCompliances.DataSource = SummaryManagement.GetTransactionsSummaryByRoleForChart(customerID, new List<int>() { RoleManagement.GetByCode("PERF").ID }, AuthenticationHelper.Role == "CADMN" ? null : (int?)AuthenticationHelper.UserID);
                chrtCompliances.DataBind();

                chrtComplianceReview.DataSource = SummaryManagement.GetTransactionsSummaryByRoleForChart(customerID, new List<int>() { RoleManagement.GetByCode("RVW1").ID, RoleManagement.GetByCode("RVW2").ID }, AuthenticationHelper.Role == "CADMN" ? null : (int?)AuthenticationHelper.UserID);
                chrtComplianceReview.DataBind();

                chrtComplianceApproval.DataSource = SummaryManagement.GetTransactionsSummaryByRoleForChart(customerID, new List<int>() { RoleManagement.GetByCode("APPR").ID }, AuthenticationHelper.Role == "CADMN" ? null : (int?)AuthenticationHelper.UserID);
                chrtComplianceApproval.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindMyCompliances(int customerID)
        {
            try
            {
                chrtMyCompliances.DataSource = SummaryManagement.GetTransactionsSummaryForChart(customerID, AuthenticationHelper.Role == "CADMN" ? null : (int?)AuthenticationHelper.UserID);
                chrtMyCompliances.DataBind();

                calHighlights.VisibleDate = DateTime.Now;
                calHighlightsNext.VisibleDate = DateTime.Now.AddMonths(1);
                calHighlightsNext2.VisibleDate = DateTime.Now.AddMonths(2);

                var compliances = Business.ComplianceManagement.GetTransactionsByUserID(AuthenticationHelper.UserID);
                for (int i = 0; i < compliances.Count; i++)
                {
                    var item = compliances[i];
                    calHighlights.SelectedDates.Add((DateTime)item.ScheduledOn);
                    calHighlightsNext.SelectedDates.Add((DateTime)item.ScheduledOn);
                    calHighlightsNext2.SelectedDates.Add((DateTime)item.ScheduledOn);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCompliancesByLocation(int customerID)
        {
            try
            {
                chrtByLocation.DataSource = SummaryManagement.GetCompliancesSummaryForChart(customerID, AuthenticationHelper.Role == "CADMN" ? null : (int?)AuthenticationHelper.UserID);
                chrtByLocation.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}