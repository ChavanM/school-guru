﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class User_Group : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDepartment.Attributes.Add("readonly", "readonly");
                txtDepartment.Text = "< Select >";
                chkIMEINumber_Checkedchanged(null, null);
                chkMobileAccess_Checkedchanged(null, null);
                chkotp_Checkedchanged(null, null);
                ScriptManager.RegisterStartupScript(this.upModifyUser, this.upModifyUser.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    BindIPDetailsList(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    BindIPAddress(Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }     
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                gridview.PageIndex = 0;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    BindIPDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                var data=UserGroupMasterGetByNameAuditUser(txtName.Text, AuthenticationHelper.CustomerID);
                if (data !=null)
                {                   
                    txtName.Text = data.GroupName;
                    txtRememberMe.Text = Convert.ToString(data.RememberMe);
                    txtPasswordExpiry.Text = Convert.ToString(data.PasswordExpiry);
                    txtremark.Text = Convert.ToString(data.Remark);
                    if (data.OTP == true)
                    {
                        chkotp.Checked = true;
                    }
                    else
                    {
                        chkotp.Checked = false;
                    }
                    if (data.MobileAccess == true)
                    {
                        chkMobileAccess.Checked = true;
                        chkIMEINumber.Visible = true;
                    }
                    else
                    {
                        chkMobileAccess.Checked = false;
                        chkIMEINumber.Visible = false;
                    }
                    if (data.IMEINumber == true)
                    {
                        chkIMEINumber.Checked = true;
                    }
                    else
                    {
                        chkIMEINumber.Checked = false;
                    }
                    if (chkotp.Checked)
                    {
                        data.OTP = true;
                    }
                    else
                    {
                        data.OTP = false;
                    }
                    var ipAddressID = GetIpAddressId((int)data.Id);
                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                        chkDepartment.Checked = false;
                        CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                        for (int i = 0; i <= ipAddressID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIPBlockID")).Text.Trim() == ipAddressID[i].ToString())
                            {
                                chkDepartment.Checked = true;
                            }
                        }
                        if ((rptDepartment.Items.Count) == (ipAddressID.Count))
                        {
                            DepartmentSelectAll.Checked = true;
                        }
                        else
                        {
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                else
                {
                    //txtName.Text = string.Empty;
                    txtRememberMe.Text = string.Empty;
                    txtPasswordExpiry.Text = string.Empty;
                    txtremark.Text = string.Empty;                   
                    chkotp.Checked = false;                                       
                    chkMobileAccess.Checked = false;
                    chkIMEINumber.Visible = false;                  
                    chkIMEINumber.Checked = false;                                                                        
                    try
                    {
                        if (rptDepartment.Items.Count > 0)
                        {
                            foreach (RepeaterItem aItem in rptDepartment.Items)
                            {
                                CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                chkDepartment.Checked = false;
                                CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                                DepartmentSelectAll.Checked = false;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }


                }
            }
            
        }
            
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != -1)
                {
                    List<mst_User_Group> objGroupMapping = new List<mst_User_Group>();
                    Business.Data.Mst_Group obj = new Mst_Group()
                    {
                        GroupName = txtName.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        CustomerID = (int)customerID
                    };
                    if (!string.IsNullOrEmpty(txtPasswordExpiry.Text))
                    {
                        obj.PasswordExpiry = Convert.ToInt32(txtPasswordExpiry.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(txtRememberMe.Text))
                    {
                        obj.RememberMe = Convert.ToInt32(txtRememberMe.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(txtremark.Text))
                    {
                        obj.Remark = txtremark.Text.Trim();
                    }
                    if (chkMobileAccess.Checked == true)
                    {
                        chkIMEINumber.Visible = true;
                        chkMobileAccess.Checked = true;
                        obj.MobileAccess = true;
                    }
                    else
                    {
                        chkMobileAccess.Checked = false;
                        chkIMEINumber.Visible = false;
                    }
                    if (chkIMEINumber.Checked == true)
                    {
                        obj.IMEINumber = true;
                    }
                    else
                    {
                        chkIMEINumber.Checked = false;
                    }
                    if (chkotp.Checked == true)
                    {
                        chkotp.Checked = true;
                        obj.OTP = true;
                    }
                    else
                    {
                        chkotp.Checked = false;
                    }
                    if ((int)ViewState["Mode"] == 1)
                    {
                        obj.Id = Convert.ToInt32(ViewState["GroupID"]);
                    }
                    if ((int)ViewState["Mode"] == 0)
                    {
                        #region Add
                        var Groupdetails = GroupDetailsByName(obj.GroupName, customerID);
                        if (Groupdetails != null)
                        {
                            obj.Id = Groupdetails.Id;
                            UpdateUserGroupMaster(obj);
                            List<int> DeptIds = new List<int>();
                            foreach (RepeaterItem aItem in rptDepartment.Items)
                            {
                                CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                if (chkDepartment.Checked)
                                {
                                    DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()));
                                    mst_User_Group GroupMapping = new mst_User_Group()
                                    {
                                        GroupID = Groupdetails.Id,
                                        IPAddressID = Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()),
                                        IsActive = true,
                                        CustomerID = (int)customerID,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedOn = null,
                                        UpdatedBy = null
                                    };
                                    objGroupMapping.Add(GroupMapping);
                                    CustomModify.IsValid = false;
                                    CustomModify.ErrorMessage = "User Group details saved successfully";
                                    vsLicenseListPage.CssClass = "alert alert-success";
                                }
                            }
                            clearAll();
                        }
                        else
                        {
                            CreateUserGroupMaster(obj);
                            List<int> DeptIds = new List<int>();
                            foreach (RepeaterItem aItem in rptDepartment.Items)
                            {
                                CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                if (chkDepartment.Checked)
                                {
                                    DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()));
                                    mst_User_Group GroupMapping = new mst_User_Group()
                                    {
                                        GroupID = obj.Id,
                                        IPAddressID = Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()),
                                        IsActive = true,
                                        CustomerID = (int)customerID,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        UpdatedOn = null,
                                        UpdatedBy = null
                                    };
                                    objGroupMapping.Add(GroupMapping);
                                    CustomModify.IsValid = false;
                                    CustomModify.ErrorMessage = "User Group details saved successfully";
                                    vsLicenseListPage.CssClass = "alert alert-success";
                                }
                            }
                            clearAll();
                        }
                        #endregion
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        var Groupdetails = GroupDetailsByID(obj.Id, customerID);
                        if (Groupdetails != null)
                        {
                            if (Groupdetails.GroupName == obj.GroupName)
                            {
                                UpdateUserGroupMaster(obj);
                                foreach (RepeaterItem aItem in rptDepartment.Items)
                                {
                                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                    if (chkDepartment.Checked)
                                    {
                                        mst_User_Group GroupMapping = new mst_User_Group()
                                        {
                                            GroupID = obj.Id,
                                            IPAddressID = Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()),
                                            IsActive = true,
                                            CustomerID = (int)customerID,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedOn = null,
                                            UpdatedBy = null
                                        };
                                        objGroupMapping.Add(GroupMapping);
                                        CustomModify.IsValid = false;
                                        CustomModify.ErrorMessage = "User Group Details updated successfully";
                                        vsLicenseListPage.CssClass = "alert alert-success";
                                    }
                                }
                                clearAll();
                            }
                            else
                            {
                                var GroupdetailsName = GroupDetailsByName(obj.GroupName, customerID);
                                if (GroupdetailsName == null)
                                {
                                    UpdateUserGroupMaster(obj);
                                    foreach (RepeaterItem aItem in rptDepartment.Items)
                                    {
                                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                        if (chkDepartment.Checked)
                                        {
                                            mst_User_Group GroupMapping = new mst_User_Group()
                                            {
                                                GroupID = obj.Id,
                                                IPAddressID = Convert.ToInt32(((Label)aItem.FindControl("lblIPBlockID")).Text.Trim()),
                                                IsActive = true,
                                                CustomerID = (int)customerID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedOn = null,
                                                UpdatedBy = null
                                            };
                                            objGroupMapping.Add(GroupMapping);
                                            CustomModify.IsValid = false;
                                            CustomModify.ErrorMessage = "User Group Details updated successfully";
                                            vsLicenseListPage.CssClass = "alert alert-success";
                                        }
                                    }
                                    clearAll();
                                }
                                else
                                {
                                    CustomModify.IsValid = false;
                                    CustomModify.ErrorMessage = "User Group Name Already Exist";
                                    vsLicenseListPage.CssClass = "alert alert-danger";
                                }
                            }
                        }               
                    }
                    if (objGroupMapping.Count > 0)
                    {
                        var groupid = Convert.ToInt32(ViewState["GroupID"]);
                        Business.IPAddBlock.DeselectAllUserGroupMapping(groupid);
                        Business.IPAddBlock.CreateUserGroupMapping(objGroupMapping);
                    }
                    BindIPDetailsList(customerID);
                    upDepeList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                vsLicenseListPage.CssClass = "alert alert-danger";
            }
        }            
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearAll();
                int customerID = -1;
                chkIMEINumber.Visible = false;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    BindIPDetailsList(customerID);
                }
                try
                {
                    if (rptDepartment.Items.Count > 0)
                    {
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyUser\").dialog('open');", true);
                string message = "";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" + message + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkMobileAccess_Checkedchanged(object sender, EventArgs e)
        {
            if (chkMobileAccess.Checked)
            {
                chkMobileAccess.Checked = true;
                chkIMEINumber.Visible = true;
            }
            else
            {
                chkMobileAccess.Checked = false;
                chkIMEINumber.Visible = false;
            }
        }
        protected void chkIMEI_Checkedchanged(object sender, EventArgs e)
        {
            if (chkIMEINumber.Checked)
            {
                chkIMEINumber.Checked = true;
            }
            else
            {
                chkIMEINumber.Checked = false;
            }

        }
        protected void chkotp_Checkedchanged(object sender, EventArgs e)
        {
            if(chkotp.Checked)
            {
                chkotp.Checked = true;
            }
            else
            {
                chkotp.Checked = false;
            }
        }
        protected void chkIMEINumber_Checkedchanged(object sender, EventArgs e)
        {
            if (chkIMEINumber.Checked)
            {
                chkIMEINumber.Checked = true;
            }
            else
            {
                chkIMEINumber.Checked = false;
            }
        }
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void gridview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridview.PageIndex = e.NewPageIndex;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    BindIPDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void gridview_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                var IPID = Convert.ToInt32(commandArgs[0]);
                var GroupID = Convert.ToInt32(commandArgs[1]);
                if (e.CommandName.Equals("EDIT_IPBlock"))
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                    if (customerID != -1)
                    {
                        ViewState["Mode"] = 1;
                        ViewState["BlockIPID"] = IPID;
                        ViewState["GroupID"] = GroupID;
                        var ipAddressID = GetIpAddressId(GroupID);
                        Mst_Group mst = UserGroupMasterGetByIDAuditUser(GroupID, customerID);
                        txtName.Text = mst.GroupName;
                        txtRememberMe.Text = Convert.ToString(mst.RememberMe);
                        txtPasswordExpiry.Text = Convert.ToString(mst.PasswordExpiry);
                        txtremark.Text =Convert.ToString(mst.Remark);
                        if (mst.OTP == true)
                        {
                            chkotp.Checked = true;
                        }
                        else
                        {
                            chkotp.Checked = false;
                        }
                        if (mst.MobileAccess == true)
                        {
                            chkMobileAccess.Checked = true;
                            chkIMEINumber.Visible = true;
                        }
                        else
                        {
                            chkMobileAccess.Checked = false;
                            chkIMEINumber.Visible = false;
                        }
                        if (mst.IMEINumber == true)
                        {
                            chkIMEINumber.Checked = true;
                        }
                        else
                        {
                            chkIMEINumber.Checked = false;
                        }
                        if (chkotp.Checked)
                        {
                            mst.OTP = true;
                        }
                        else
                        {
                            mst.OTP = false;
                        }
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            for (int i = 0; i <= ipAddressID.Count - 1; i++)
                            {
                                if (((Label)aItem.FindControl("lblIPBlockID")).Text.Trim() == ipAddressID[i].ToString())
                                {
                                    chkDepartment.Checked = true;
                                }
                            }
                            if ((rptDepartment.Items.Count) == (ipAddressID.Count))
                            {
                                DepartmentSelectAll.Checked = true;
                            }
                            else
                            {
                                DepartmentSelectAll.Checked = false;
                            }
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyUser\").dialog('open');", true);
                        upModifyUser.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_IPBlock"))
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (customerID != -1)
                    {
                        DeleteUserGroupMasterAudit(IPID, GroupID, customerID);
                        DeleteMstGroupMasterAudit(GroupID, customerID);
                        BindIPDetailsList(customerID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Method
        public void clearAll()
        {
            txtName.Text = string.Empty;
            txtPasswordExpiry.Text = string.Empty;
            txtRememberMe.Text = string.Empty;
            txtremark.Text = string.Empty;
            chkIMEINumber.Checked = false;
            chkMobileAccess.Checked = false;
            chkotp.Checked = false;

        }
        private void BindIPDetailsList(int CustomerID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    var UserMasterList = IPAddBlock.GetAllUserMasterList(customerID);
                    var filter = tbxFilter.Text.Trim();
                    if (!string.IsNullOrEmpty(filter))
                    {                      
                        UserMasterList = UserMasterList.Where(
                              entry => entry.Name.ToUpper().Trim().Contains(filter.ToUpper().Trim())
                           || entry.IPAddress.Contains(filter)
                           || entry.IPName.ToUpper().Trim().Contains(filter.ToUpper().Trim())
                           || (entry.Remark != null && entry.PasswordExpiry.ToString() == filter)
                           || (entry.Remark != null && entry.RememberMe.ToString() == filter)
                           || (entry.Remark != null && entry.Remark.ToUpper().Trim() == filter.ToUpper().Trim())).ToList();
                    }
                    gridview.DataSource = UserMasterList;
                    Session["TotalRows"] = UserMasterList.Count;
                    gridview.DataBind();
                    upModifyUser.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindIPAddress(int CustomerID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    rptDepartment.DataSource = IPAddBlock.GetAll(customerID);
                    rptDepartment.DataBind();
                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                        if (!chkDepartment.Checked)
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                    DepartmentSelectAll.Checked = true;
                    clearAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static Mst_Group GroupDetailsByName(string groupname, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Mst_Group
                             where
                             row.GroupName.ToUpper().Trim().Equals(groupname.ToUpper().Trim())
                             && row.IsActive == true
                             && row.CustomerID == customerid
                             select row).FirstOrDefault();
                return query;

            }
        }
        public static Mst_Group GroupDetailsByID(long groupID, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Mst_Group
                             where
                             row.Id== groupID
                             && row.IsActive == true
                             && row.CustomerID == customerid
                             select row).FirstOrDefault();
                return query;

            }
        }        
        public static void CreateUserGroupMaster(Mst_Group mst)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Mst_Group.Add(mst);
                entities.SaveChanges();
            }
        }
        public static void UpdateUserGroupMaster(Mst_Group mst)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Mst_Group deptMasterToUpdate = (from row in entities.Mst_Group
                                                where row.Id == mst.Id
                                                && row.IsActive == true
                                                && row.CustomerID == mst.CustomerID
                                                select row).FirstOrDefault();
                if (deptMasterToUpdate != null)
                {
                    deptMasterToUpdate.GroupName = mst.GroupName;
                    deptMasterToUpdate.PasswordExpiry = mst.PasswordExpiry;
                    deptMasterToUpdate.RememberMe = mst.RememberMe;
                    deptMasterToUpdate.Remark = mst.Remark;
                    deptMasterToUpdate.MobileAccess = mst.MobileAccess;
                    deptMasterToUpdate.IMEINumber = mst.IMEINumber;
                    deptMasterToUpdate.OTP = mst.OTP;
                    deptMasterToUpdate.UpdatedBy = mst.UpdatedBy;
                    deptMasterToUpdate.UpdatedOn = DateTime.Now;                    
                    entities.SaveChanges();
                }
            }
        }
        public static Mst_Group UserGroupMasterGetByNameAuditUser(string groupName, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstIP = (from row in entities.Mst_Group
                             where row.GroupName.ToUpper().Trim() == groupName.ToUpper().Trim()
                             && row.CustomerID == customerID
                             && row.IsActive == true
                             select row).FirstOrDefault();
                return MstIP;
            }
        }
        public static Mst_Group UserGroupMasterGetByIDAuditUser(int groupID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstIP = (from row in entities.Mst_Group
                             where row.Id == groupID                            
                             && row.CustomerID == customerID
                             && row.IsActive == true
                             select row).FirstOrDefault();
                return MstIP;
            }
        }
        
        public static List<long> GetIpAddressId(int GroupID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IpaddressMapped = (from row in entities.mst_User_Group
                                       where row.GroupID == GroupID
                                       && row.IsActive == true
                                       select row.IPAddressID).ToList();

                return IpaddressMapped;
            }
        }
        public static void DeleteUserGroupMasterAudit(int Id, int groupID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserMastertoDelete = (from row in entities.mst_User_Group
                                          where row.GroupID == groupID
                                          && row.Id == Id
                                           && row.CustomerID == CustomerID
                                          select row).FirstOrDefault();
                if (UserMastertoDelete != null)
                {
                    UserMastertoDelete.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }
        public static void DeleteMstGroupMasterAudit(int groupID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.mst_User_Group
                            where row.GroupID == groupID
                             && row.CustomerID == CustomerID
                              && row.IsActive == true
                            select row).ToList();

                if (data.Count==0)
                {
                    var UserMastertoDelete = (from row in entities.Mst_Group
                                              where row.Id == groupID
                                               && row.CustomerID == CustomerID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();

                    if (UserMastertoDelete != null)
                    {
                        UserMastertoDelete.IsActive = false;
                        entities.SaveChanges();
                    }
                }                
            }
        }
        #endregion
    }
}