﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.Threading;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ChangeComplianceScheduleOn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindActs();
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
        }
        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActManagement.GetAllAssignedActs();
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCompliances(long actID)
        {
            try
            {
                ddlCompliance.DataTextField = "Name";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = ActManagement.GetAllAssignedCompliances(actID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSchedulePeriods(long ActID,long ComplianceID)
        {
            try
            {
                ddlPeriod.DataTextField = "Name";
                ddlPeriod.DataValueField = "ID";

                ddlPeriod.DataSource = ActManagement.GetAllAssignedPeriods(ActID,ComplianceID);
                ddlPeriod.DataBind();

                ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlAct.SelectedValue) != -1)
            {
                BindCompliances(Convert.ToInt32(ddlAct.SelectedValue));
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlCompliance.SelectedValue) != -1)
            {
                BindSchedulePeriods(Convert.ToInt32(ddlAct.SelectedValue),Convert.ToInt32(ddlCompliance.SelectedValue));
                txtNotificationRemark.Text = "";
            }

        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Convert.ToInt32(ddlAct.SelectedValue) != -1 && Convert.ToInt32(ddlCompliance.SelectedValue) != -1 && Convert.ToString(ddlPeriod.SelectedItem.Text) != "< Select >")
                {

                    long actid = Convert.ToInt32(ddlAct.SelectedValue);
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                    string Period = Convert.ToString(ddlPeriod.SelectedItem.Text);

                    var ScheduleList = (from row in entities.Acts
                                        join row1 in entities.Compliances
                                        on row.ID equals row1.ActID
                                        join row2 in entities.ComplianceInstances
                                        on row1.ID equals row2.ComplianceId
                                        join row3 in entities.ComplianceAssignments
                                        on row2.ID equals row3.ComplianceInstanceID
                                        join row4 in entities.ComplianceScheduleOns
                                        on row3.ComplianceInstanceID equals row4.ComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false && row1.ActID == actid
                                        && row1.ID == complianced
                                        && row4.ForMonth.Equals(Period)
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                        select new
                                        {
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            ActName = row.Name,                                              
                                            row1.ShortDescription,
                                            row4.ForMonth,
                                            row4.ScheduleOn,
                                            row4.ComplianceInstanceID,
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();
                }
                txtNotificationRemark.Text = "";
            }
        }
        protected void chkNotification_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNotification.Checked)
            {
                divNotification.Visible = true;
                txtNotificationRemark.Enabled = true;
                txtNotificationRemark.Text = "Due Date of Compliance \"" + ddlCompliance.SelectedItem.Text + "\" has been changed to \"" + txtSchedueleDate.Text + "\" for period \"" + ddlPeriod.SelectedItem.Text + "\"";
            }
            else
            {
                divNotification.Visible = false;
                txtNotificationRemark.Enabled = false;
                txtNotificationRemark.Text = "Due Date of Compliance \"" + ddlCompliance.SelectedItem.Text + "\" has been changed to \"" + txtSchedueleDate.Text + "\" for period \"" + ddlPeriod.SelectedItem.Text + "\"";
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool ischecked = false;
                foreach (GridViewRow gvrow in grdCompliances.Rows)
                {
                    var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                    if (checkbox.Checked)
                    {
                        ischecked = true;
                        break;
                    }
                }
                if (ischecked)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long actid = Convert.ToInt32(ddlAct.SelectedValue);
                        long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                        string Period = Convert.ToString(ddlPeriod.SelectedItem.Text);
                      

                        foreach (GridViewRow gvrow in grdCompliances.Rows)
                        {
                            var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                            if (checkbox.Checked)
                            {
                                var lblComplianceInstenaceID = gvrow.FindControl("lblComplianceInstenaceID") as Label;

                                var CID = Convert.ToInt32(lblComplianceInstenaceID.Text);
                                //ComplianceInstances
                                var complianceInstances = (from row in entities.ComplianceInstances
                                                           where row.IsDeleted == false && row.ComplianceId == complianced
                                                           && row.ID == CID
                                                           select row).FirstOrDefault();

                                if (complianceInstances != null)
                                {
                                    var CSCHdetails = (from row in entities.ComplianceScheduleOns
                                                       where row.ComplianceInstanceID == complianceInstances.ID
                                                       && row.ForMonth == Period && row.IsActive == true
                                                       && row.IsUpcomingNotDeleted == true
                                                       select row).FirstOrDefault();
                                    if (CSCHdetails != null)
                                    {

                                        var complianceTransactionslist = (from row in entities.ComplianceTransactions
                                                                          where row.ComplianceInstanceId == CSCHdetails.ComplianceInstanceID
                                                                                && row.ComplianceScheduleOnID == CSCHdetails.ID
                                                                          select row).ToList();
                                        if (complianceTransactionslist.Count > 0)
                                        {
                                            if (complianceTransactionslist.Count == 1)
                                            {
                                                DateTime NewscheduleOnDate = DateTime.ParseExact(txtSchedueleDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                DateTime OldscheduleOnDate = CSCHdetails.ScheduleOn;

                                                long customerID = (from row in entities.CustomerBranches
                                                                   where row.ID == complianceInstances.CustomerBranchID
                                                                   select row.CustomerID).FirstOrDefault();
                                                log_ChangeScheduleOn Changescheduleon = new log_ChangeScheduleOn()
                                                {
                                                    ComplianceID = complianced,
                                                    Period = Period,
                                                    OldDueDate = OldscheduleOnDate,
                                                    NewDueDate = NewscheduleOnDate,
                                                    ComplianceInstanceID = CSCHdetails.ComplianceInstanceID,
                                                    CustomerID = customerID,
                                                    BranchID = Convert.ToInt32(complianceInstances.CustomerBranchID),
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    craetedOn = DateTime.Now,
                                                    ScheduleOnID = CSCHdetails.ID,
                                                    flag = "S",
                                                };
                                                if (chkApplicable.Checked == true)
                                                {
                                                    Changescheduleon.SendNotification = Convert.ToBoolean(1);
                                                }
                                                else
                                                {
                                                    Changescheduleon.SendNotification = Convert.ToBoolean(0);
                                                }
                                                CreateComplianceScheduleOn(Changescheduleon);
                                                Business.ComplianceManagement.UpdateComplianceScheduleOn(complianceInstances.ComplianceId, complianceInstances.ID, CSCHdetails.ID, OldscheduleOnDate, NewscheduleOnDate, Convert.ToInt32(CSCHdetails.ForPeriod));
                                            }
                                            else
                                            {
                                                DateTime NewscheduleOnDate = DateTime.ParseExact(txtSchedueleDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                DateTime OldscheduleOnDate = CSCHdetails.ScheduleOn;

                                               

                                                var RCT = (from row in entities.RecentComplianceTransactionViews
                                                           where row.ComplianceInstanceId == CSCHdetails.ComplianceInstanceID
                                                                 && row.ComplianceScheduleOnID == CSCHdetails.ID
                                                           select row).FirstOrDefault();
                                                
                                                int result = DateTime.Compare(RCT.StatusChangedOn.Value.Date, NewscheduleOnDate.Date);
                                                if (result == 0 || result<0)
                                                {

                                                    var scheduleontoupdate = (from row in entities.ComplianceScheduleOns
                                                                              where row.ID == CSCHdetails.ID
                                                                              select row).FirstOrDefault();

                                                    if (scheduleontoupdate != null)
                                                    {
                                                        scheduleontoupdate.ScheduleOn = NewscheduleOnDate;
                                                        entities.SaveChanges();
                                                    }


                                                    if (RCT.ComplianceStatusID == 3)
                                                    {

                                                        var transactiontoupdate = (from row in complianceTransactionslist
                                                                                   where row.ID == RCT.ComplianceTransactionID
                                                                                      && row.ComplianceScheduleOnID == CSCHdetails.ID
                                                                                   select row).FirstOrDefault();
                                                        if (transactiontoupdate != null)
                                                        {
                                                            transactiontoupdate.StatusId = 2;
                                                            entities.SaveChanges();
                                                        }


                                                        long customerID = (from row in entities.CustomerBranches
                                                                           where row.ID == complianceInstances.CustomerBranchID
                                                                           select row.CustomerID).FirstOrDefault();
                                                        log_ChangeScheduleOn Changescheduleon = new log_ChangeScheduleOn()
                                                        {
                                                            ComplianceID = complianced,
                                                            Period = Period,
                                                            OldDueDate = OldscheduleOnDate,
                                                            NewDueDate = NewscheduleOnDate,
                                                            ComplianceInstanceID = CSCHdetails.ComplianceInstanceID,
                                                            CustomerID = customerID,
                                                            BranchID = Convert.ToInt32(complianceInstances.CustomerBranchID),
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            craetedOn = DateTime.Now,
                                                            ScheduleOnID = CSCHdetails.ID,
                                                            flag = "S",
                                                        };
                                                        if (chkApplicable.Checked == true)
                                                        {
                                                            Changescheduleon.SendNotification = Convert.ToBoolean(1);
                                                        }
                                                        else
                                                        {
                                                            Changescheduleon.SendNotification = Convert.ToBoolean(0);
                                                        }
                                                        CreateComplianceScheduleOn(Changescheduleon);
                                                        Business.ComplianceManagement.UpdateComplianceScheduleOn(complianceInstances.ComplianceId, complianceInstances.ID, CSCHdetails.ID, OldscheduleOnDate, NewscheduleOnDate, Convert.ToInt32(CSCHdetails.ForPeriod));

                                                    }
                                                    else //if (RCT.ComplianceStatusID == 5 || RCT.ComplianceStatusID == 7 || RCT.ComplianceStatusID == 9 || RCT.ComplianceStatusID == 9)
                                                    {
                                                        int stid = -1;
                                                        if (RCT.ComplianceStatusID == 5)
                                                        {
                                                            stid = 4;
                                                        }
                                                        else
                                                        {
                                                            stid = (int)RCT.ComplianceStatusID;
                                                        }
                                                        var transactiontoupdate = (from row in complianceTransactionslist
                                                                                   where row.ID == RCT.ComplianceTransactionID
                                                                                      && row.ComplianceScheduleOnID == CSCHdetails.ID
                                                                                   select row).FirstOrDefault();
                                                        if (transactiontoupdate != null)
                                                        {
                                                            transactiontoupdate.StatusId = stid;
                                                            entities.SaveChanges();
                                                        }

                                                        long customerID = (from row in entities.CustomerBranches
                                                                           where row.ID == complianceInstances.CustomerBranchID
                                                                           select row.CustomerID).FirstOrDefault();
                                                        log_ChangeScheduleOn Changescheduleon = new log_ChangeScheduleOn()
                                                        {
                                                            ComplianceID = complianced,
                                                            Period = Period,
                                                            OldDueDate = OldscheduleOnDate,
                                                            NewDueDate = NewscheduleOnDate,
                                                            ComplianceInstanceID = CSCHdetails.ComplianceInstanceID,
                                                            CustomerID = customerID,
                                                            BranchID = Convert.ToInt32(complianceInstances.CustomerBranchID),
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            craetedOn = DateTime.Now,
                                                            ScheduleOnID = CSCHdetails.ID,
                                                            flag = "S",
                                                        };
                                                        if (chkApplicable.Checked == true)
                                                        {
                                                            Changescheduleon.SendNotification = Convert.ToBoolean(1);
                                                        }
                                                        else
                                                        {
                                                            Changescheduleon.SendNotification = Convert.ToBoolean(0);
                                                        }
                                                        CreateComplianceScheduleOn(Changescheduleon);
                                                        Business.ComplianceManagement.UpdateComplianceScheduleOn(complianceInstances.ComplianceId, complianceInstances.ID, CSCHdetails.ID, OldscheduleOnDate, NewscheduleOnDate, Convert.ToInt32(CSCHdetails.ForPeriod));

                                                    }
                                                }
                                                else
                                                {
                                                    var scheduleontoupdate = (from row in entities.ComplianceScheduleOns
                                                                              where row.ID == CSCHdetails.ID
                                                                              select row).FirstOrDefault();

                                                    if (scheduleontoupdate != null)
                                                    {
                                                        scheduleontoupdate.ScheduleOn = NewscheduleOnDate;
                                                        entities.SaveChanges();
                                                    }
                                                    long customerID = (from row in entities.CustomerBranches
                                                                       where row.ID == complianceInstances.CustomerBranchID
                                                                       select row.CustomerID).FirstOrDefault();
                                                    log_ChangeScheduleOn Changescheduleon = new log_ChangeScheduleOn()
                                                    {
                                                        ComplianceID = complianced,
                                                        Period = Period,
                                                        OldDueDate = OldscheduleOnDate,
                                                        NewDueDate = NewscheduleOnDate,
                                                        ComplianceInstanceID = CSCHdetails.ComplianceInstanceID,
                                                        CustomerID = customerID,
                                                        BranchID = Convert.ToInt32(complianceInstances.CustomerBranchID),
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        craetedOn = DateTime.Now,
                                                        ScheduleOnID = CSCHdetails.ID,
                                                        flag = "S",
                                                    };
                                                    if (chkApplicable.Checked == true)
                                                    {
                                                        Changescheduleon.SendNotification = Convert.ToBoolean(1);
                                                    }
                                                    else
                                                    {
                                                        Changescheduleon.SendNotification = Convert.ToBoolean(0);
                                                    }
                                                    CreateComplianceScheduleOn(Changescheduleon);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                      
                        #region Send Notification
                        if (chkNotification.Checked == true && txtNotificationRemark.Text != "")
                        {
                            long NotificationID = 0;

                            Notification newNotification = new Notification()
                            {
                                ActID = actid,
                                ComplianceID = complianced,
                                Type = "Compliance",
                                Remark = txtNotificationRemark.Text,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now.Date,
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now.Date,
                            };


                            Business.ComplianceManagement.CreateNotification(newNotification);
                            NotificationID = newNotification.ID;


                            if (NotificationID != 0)
                            {
                                //Get All Users Compliance to Assigned 
                                List<long> ListUsers = Business.ComplianceManagement.GetAllUsersAssignedByComplianceID(complianced);
                                ListUsers = ListUsers.Distinct().ToList();
                                //For each User Create Entry in Notification Table
                                ListUsers.ForEach(EachUser =>
                                {
                                    UserNotification UNF = new UserNotification()
                                    {
                                        NotificationID = NotificationID,
                                        UserID = Convert.ToInt32(EachUser),
                                        IsRead = false,
                                    };

                                    if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                        Business.ComplianceManagement.CreateUserNotification(UNF);
                                    else
                                        Business.ComplianceManagement.UpdateNotification(newNotification);
                                });

                                #region Notification App
                                //add in EditComplianceListMaster after edit Compliance
                                //code for send notification to mobile                          
                                // using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    List<string> details = new List<string>();
                                    if (ListUsers.Count > 0)
                                    {
                                        details.Clear();
                                        foreach (var item in ListUsers)
                                        {
                                            var deviceIDList = (from row in entities.MaintainDeviceIDs
                                                                where row.Product.Equals("C") && row.UserID == item && row.Status.Equals("Active")
                                                                select row.DeviceToken).FirstOrDefault();

                                            if (!string.IsNullOrEmpty(deviceIDList))
                                            {
                                                details.Add(deviceIDList);
                                            }
                                        }
                                        details = details.Where(x => x != "null").ToList();
                                        string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                        if (arr1.Length > 0)
                                        {
                                            string chk = System.Text.RegularExpressions.Regex.Replace(txtNotificationRemark.Text, "<[^>]*>", "");
                                            NotificationManagement.SendNotificationMobile(arr1, chk, "Compliance Updates");
                                        }
                                        details.Clear();
                                        details = null;
                                    }
                                }
                                #endregion

                                #region Research Mail Send
                                if (1 == 2)
                                {
                                    var researchemailnotification = Business.ComplianceManagement.GetAllUsersAssignedResearchByComplianceID(complianced);
                                    if (researchemailnotification.Count > 0)
                                    {
                                        foreach (var item in researchemailnotification)
                                        {
                                            if (CustomerManagement.IsCustomerActive(Convert.ToInt64(item.CustomerID)))
                                            {
                                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(item.CustomerID)).Name;
                                                string portalurl = string.Empty;
                                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                                if (Urloutput != null)
                                                {
                                                    portalurl = Urloutput.URL;
                                                }
                                                else
                                                {
                                                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                                }

                                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ResearchNotification
                                                                   .Replace("@User", item.Name)
                                                                   .Replace("@ShortDescription", item.ShortDescription)
                                                                   .Replace("@Remark", txtNotificationRemark.Text.Trim())
                                                                   .Replace("@URL", Convert.ToString(portalurl))
                                                                   .Replace("@From", ReplyEmailAddressName);

                                                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ResearchNotification
                                                //                    .Replace("@User", item.Name)
                                                //                    .Replace("@ShortDescription", item.ShortDescription)
                                                //                    .Replace("@Remark", txtNotificationRemark.Text.Trim())
                                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                //                    .Replace("@From", ReplyEmailAddressName);

                                                new Thread(() =>
                                                {
                                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"],
                                     (new string[] { item.Email }).ToList(), null, null, "AVACOM Notification for compliances change.", message);
                                                }).Start();
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                
                        if (!string.IsNullOrEmpty(ddlCompliance.SelectedValue) &&  !string.IsNullOrEmpty(ddlAct.SelectedValue))
                        {
                            if (ddlCompliance.SelectedValue != "-1" && ddlAct.SelectedValue != "-1")
                            {
                                BindSchedulePeriods(actid, complianced);
                            }
                        }


                        ddlPeriod.SelectedValue = "-1";
                        cvDuplicateEntry.IsValid = false;
                        grdCompliances.DataSource = null;
                        txtSchedueleDate.Text = "";
                        grdCompliances.DataBind();
                        cvDuplicateEntry.ErrorMessage = "Schedule Changed successfully.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please check at least one checkbox.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateComplianceScheduleOn(log_ChangeScheduleOn MSCO)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.log_ChangeScheduleOn.Add(MSCO);
                entities.SaveChanges();
            }
                        
            try
            {
                if (MSCO.ScheduleOnID != null && MSCO.NewDueDate != null)
                    Business.ComplianceManagement.ChangeScheduleOn_Secretarial(Convert.ToInt64(MSCO.ScheduleOnID), Convert.ToDateTime(MSCO.NewDueDate));
            }
            catch (Exception ex)
            {
                ContractManagement.InsertErrorMsg_DBLog("ASPX-ChangeScheduleOn_Secretarial", "ASPX-ChangeScheduleOn_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


    }
}