﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceCertificateTemplate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceCertificateTemplate" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" />

    <link href="Style/css/StyleSheetTextEditor.css" rel="stylesheet" />

    <link href="Styles.css" rel="stylesheet" />
    <script type="text/javascript">

        var editor, range;
        function OnClientLoad(sender, args) {
            editor = sender;
        }

        function OnClientSelectionChange(sender, args) {
            range = editor.getSelection().getRange(true);
        }


        function btn_click() {
            debugger;
            var TemplateName = $("#<%=ddlContractTemplate.ClientID%>").val();
            TemplateName = TemplateName.toString();
            //TemplateName = TemplateName.replace(/[}{]/g, '');
            editor.pasteHtml(TemplateName);

            return false;
        }



    </script>


    <style type="text/css">
        .dropdownclass {
            color: rgb(142, 142, 147);
            background-color: rgb(255, 255, 255);
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(199, 199, 204);
            width: 24%;
            height: 32px !important;
            margin-right: 6%;
            border-image: initial;
            border-radius: 4px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
 
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     <asp:UpdatePanel ID="upcomList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                          <asp:DropDownList runat="server" ID="ddlCustomerFilter" Style="padding: 0px; margin: 0px; height: 22px; width: 350px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlCustomerFilter_SelectedIndexChanged"  AutoPostBack="true"/>
                    </td>
                    <td align="right" style="width: 20%">
                       
                    </td>
                    <td align="right" style="width: 25%">
                     
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddOwner_Click"  />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdTemplate" AutoGenerateColumns="false" GridLines="Vertical" 
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdTemplate_RowCommand" OnRowEditing="grdTemplate_RowEditing">
                <Columns>
                      <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Version" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                <asp:Label runat="server" Text='<%# Eval("Version") %>' ToolTip='<%# Eval("Version") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("FromDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="End Date" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                      
                      <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Certificate Template" title="Edit Certificate Template" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Certificate Template?');"><img src="../Images/delete_icon.png" alt="Delete Certificate Template" title="Delete Certificate Template" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div id="divOfficerdetailsDialog" style="height: auto;">
        <asp:UpdatePanel ID="upcom" runat="server" UpdateMode="Conditional" OnLoad="upcom_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComValidationGroup" Display="None" />
                    </div>

                    
                    <div style="margin-left: 330px; margin-top: 28px;">
                        <asp:Label runat="server" Text="Template Fields"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlContractTemplate" CssClass="dropdownclass" Style="width: 180px; height: 32px !important; margin-right: 6%">
                        </asp:DropDownList>
                        <asp:Button ID="Button2" Style="margin-right: 0.3%; margin-left: -40px;" runat="server" AutoPostback="false" CssClass="btn btn-primary" Text="Insert" OnClientClick="btn_click(); return false;" />
                    </div>

                     <div runat="server" id="div3" style="margin-bottom: 10px;margin-left: 330px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                           Version </label>
                        <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label>
                    </div>

                    <div runat="server" id="div1" style="margin-bottom: 7px; margin-left: 330px;">
                        <div>
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">
                                From Date</label>
                            <asp:TextBox runat="server" ID="tbxFromDate" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                       
                            <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                                ControlToValidate="tbxFromDate" runat="server" ValidationGroup="ComValidationGroup"
                                Display="None" />
                        </div>
                        <div style="margin-left: 292px;margin-top: -27px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                End Date</label>
                            <asp:TextBox runat="server" ID="tbxEndDate" Style="padding: 0px; margin: 0px; height: 30px; width: 150px; margin-left: -56px;" CssClass="StartDate" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select End Date."
                                ControlToValidate="tbxEndDate" runat="server" ValidationGroup="ComValidationGroup"
                                Display="None" />

                         <%--   <asp:CompareValidator ID="CompareValidator1" ValidationGroup="ComValidationGroup" ForeColor="Red" runat="server"
                                ControlToValidate="tbxFromDate" ControlToCompare="tbxEndDate" Operator="LessThan" Type="Date"
                                ErrorMessage="Start date must be less than End date."></asp:CompareValidator>--%>
                        </div>
                    </div>


                    <div class="demo-containers" style="margin-top: 10px; margin-left: 78px">
                        <div class="demo-container">
                            <telerik:RadEditor RenderMode="Lightweight" ID="theEditor" EnableTrackChanges="true" runat="server" Width="1000px"
                                OnClientLoad="OnClientLoad" Height="600px" ToolsFile="~/ToolsFile.xml" OnExportContent="RadEditor1_ExportContent" ContentFilters="DefaultFilters, PdfExportFilter">

                                <ExportSettings>
                                    <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8" PageHeader="Some header text for DOCX documents" />
                                    <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13" HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                                </ExportSettings>

                                <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true" UserCssId="reU0"></TrackChangesSettings>

                            </telerik:RadEditor>
                        </div>
                    </div>
                    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="theEditor" LoadingPanelID="RadAjaxLoadingPanel1" />
                                    <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                    </telerik:RadAjaxLoadingPanel>

                    <div style="text-align: center; margin-top: 15px; margin-bottom: 15px;">


                        <asp:Button ID="Button1" runat="server" Text="Download" OnClick="Button1_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                    </div>

                    <div runat="server" id="divHtml"></div>
                </div>
            </ContentTemplate>
            <Triggers>
                  <asp:PostBackTrigger ControlID="Button1" />
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>
    
      <script type="text/javascript">
          function initializeDatePicker1(date) {
              var startDate = new Date();
              $(".StartDate").datepicker({
                  dateFormat: 'dd-mm-yy',
                  setDate: startDate,
                  numberOfMonths: 1
              });
          }

          function setDate() {
              $(".StartDate").datepicker();
          }

          $(function () {
              $('#divOfficerdetailsDialog').dialog({
                  width: 1200,
                  height: 700,
                  autoOpen: false,
                  draggable: true,
                  title: "Compliance Certificate Template",
                  open: function (type, data) {
                      $(this).parent().appendTo("form");
                  }
              });
              if (document.getElementById('BodyContent_saveopo').value == "true") {
                  newfun();
                  document.getElementById('BodyContent_saveopo').value = "false";
              }
              else {
                  $("#divOfficerdetailsDialog").dialog('close');
              }
          });

          function newfun() {
              $("#divOfficerdetailsDialog").dialog('open');

          }
          function initializeCombobox() {
           

          }

          function checkOwnerAll(cb) {
              var ctrls = document.getElementsByTagName('input');
              for (var i = 0; i < ctrls.length; i++) {
                  var cbox = ctrls[i];
                  if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompowner") > -1) {
                      cbox.checked = cb.checked;
                  }
              }
          }

          function UncheckOwnerHeader() {
              var rowCheckBox = $("#RepeaterTable input[id*='chkCompowner']");
              var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompowner']:checked");
              var rowCheckBoxHeader = $("#RepeaterTable input[id*='OwnerSelectAll']");
              if (rowCheckBox.length == rowCheckBoxSelected.length) {
                  rowCheckBoxHeader[0].checked = true;
              } else {

                  rowCheckBoxHeader[0].checked = false;
              }
          }

          function initializeJQueryUI1(textBoxID, divID) {
              $("#" + textBoxID).unbind('click');

              $("#" + textBoxID).click(function () {
                  $("#" + divID).toggle("blind", null, 500, function () { });
              });
          }

          function initializeJQueryUI(textBoxID, divID) {
              $("#" + textBoxID).unbind('click');

              $("#" + textBoxID).click(function () {
                  $("#" + divID).toggle("blind", null, 500, function () { });
              });
          }
      </script>
</asp:Content>
