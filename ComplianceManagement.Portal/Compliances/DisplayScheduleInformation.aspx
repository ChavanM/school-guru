﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="DisplayScheduleInformation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.DisplayScheduleInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
          $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlCompliance.ClientID %>").combobox();
           

        }
    </script>
    <style type="text/css">
      .ui-autocomplete 
      {
          width: 617px;
          max-height: 200px;
      }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
      <div style="float: right; margin-right: 7px; margin-top: 5px;">
                <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="btnExport_Click"><img src="../Images/excel.png" alt="Export to Excel"
                  title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
            </div>
      <table runat="server" width="70%" align="center" style="margin-top: 10px;">
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Act Name</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlAct" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 25px; width: 300px;"
                                CssClass="txtbox" />
                            
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Compliance
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCompliance" AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 25px; width: 300px;"
                                CssClass="txtbox" />
                              
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>

                     <tr>
                        <td colspan="4" align="center" runat="server">
                            <br />
                            <br />
                            <asp:Panel ID="Panel1" Width="100%" Height="400px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="1000"
                                    Font-Size="12px">                                  
                                    <Columns>
                                            <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComplianceInstenaceID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        
                                     
                                        
                                         <asp:TemplateField HeaderText="Compliance Id" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceId") %>' ToolTip='<%# Eval("ComplianceId") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Branch Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Description" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduleOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduleOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>

                                                </div>
                                            </ItemTemplate>
                                          
                                        </asp:TemplateField>
                                      
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
          </table>
</asp:Content>
