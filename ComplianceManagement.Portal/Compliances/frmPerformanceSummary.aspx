﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="frmPerformanceSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.frmPerformanceSummary" %>

<%@ Register Src="~/Controls/PerformanceSummaryforReviewer.ascx" TagName="PerformanceSummaryforReviewer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/PerformanceSummaryforPerformer.ascx" TagName="PerformanceSummaryforPerformer"
    TagPrefix="vit" %>

<%@ Register Src="~/Controls/InternalPerformance_SummaryforReviewer.ascx" TagName="InternalPerformanceSummaryforReviewer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalPerformance_SummaryforPerformer.ascx" TagName="InternalPerformanceSummaryforPerformer"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../Style/Dashboard.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
            initializeRadioButtonsList($("#<%= rdInternalaRoleList.ClientID %>"));           
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Statutory" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="Internal" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server" Visible="false"
                    OnClick="Tab2_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table style="width: 100%; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div>
                                        <div style="margin: 10px 20px 10px 105px">
                                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>

                                            <div>
                                                <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                    OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:RadioButtonList>

                                            </div>
                                        </div>

                                    </div>

                                    <div>
                                        <table width="100%" style="height: 35px">
                                            <tr>
                                                <td>
                                                    <div style="margin-bottom: 4px">
                                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:PlaceHolder runat="server" ID="phSummaryControl" />
                                        <vit:PerformanceSummaryforPerformer ID="ucPerformanceSummaryforPerformer" runat="server" Visible="false"></vit:PerformanceSummaryforPerformer>
                                        <vit:PerformanceSummaryforReviewer ID="ucPerformanceSummaryforReviewer" runat="server" Visible="false"></vit:PerformanceSummaryforReviewer>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table style="width: 100%; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div style="margin: 10px 20px 10px 90px">
                                        <asp:Label ID="lblErrorMessageInternal" runat="server" Style="color: Red"></asp:Label>

                                        <div>
                                            <asp:RadioButtonList runat="server" ID="rdInternalaRoleList" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                OnSelectedIndexChanged="rdInternalaRoleList_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:RadioButtonList>

                                        </div>
                                    </div>
                                    <div>
                                        <table width="100%" style="height: 35px">
                                            <tr>
                                                <td>
                                                    <div style="margin-bottom: 4px">
                                                        <asp:CustomValidator ID="cvDuplicateEntryInternal" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lbltagLineInternal" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <asp:PlaceHolder runat="server" ID="phSummaryControlInternal" />
                                        <vit:InternalPerformanceSummaryforPerformer ID="UcInternalPerformanceSummaryforPerformer" runat="server" Visible="false"></vit:InternalPerformanceSummaryforPerformer>
                                        <vit:InternalPerformanceSummaryforReviewer ID="UcInternalPerformanceSummaryforReviewer" runat="server" Visible="false"></vit:InternalPerformanceSummaryforReviewer>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
</asp:Content>
