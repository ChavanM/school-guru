﻿<%@ Page Title="Revise Compliances" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" Async="true" EnableEventValidation="false" CodeBehind="ReviseCompliances.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ReviseCompliances" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Dashboard-white-widget {
            background: #fff;
            margin-bottom: 20px;
            border-radius: 10px;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            fhead('Revise Compliance');
            initializeDatePicker();
        });

        function fopendocfileReviewReviewerPopUp() {
            $('#DocumentReviewPopUp111').modal('show');
        }

        function fopendocfileReviewReviewer(file) {
            debugger;
            $('#DocumentReviewPopUp111').modal('show');
            $('#ContentPlaceHolder1_docViewerReviewAll111').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function REcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($('#ContentPlaceHolder1_grdGstMappedCompliance_chkComplianceAll').is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);

                } else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                }
            }
        }

        function REchildcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($($('span.classrem > input')[i]).is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);
                    break;
                }
                else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                }
            }
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        $(function () {
            $('#divReviseCompliance').dialog({
                height: 600,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Revise Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeReviseDate(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }


        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= txtAdvStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                onClose: function (startDate) {
                    $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "minDate", startDate);
              }
            });

              $("#<%= txtAdvEndDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,

            });


            if (date != null) {
            <%--  $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
             $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);--%>
          }
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        var validFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            for (var i = 0; i < FileUpload1.length; i++) {
                var fileExtension = FileUpload1[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }
            if (fuSampleFile.length > 0 && fuSampleFile[0].size == 0) {
                isValidFile = false;
            }
            if (FileUpload1.length > 0 && FileUpload1[0].size == 0) {
                isValidFile = false;
            }
            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file error. System does not support uploaded file.Please upload another file.";
            }
            return isValidFile;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upDocumentDownload_Load">
        <ContentTemplate>

            <div class="row Dashboard-white-widget" style="padding: 0px 0px 0px 0px !important;">
                <div class="dashboard">

                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="VDTOP" runat="server" class="alert alert-block alert-danger fade in" />
                        <asp:CustomValidator ID="CVTop" runat="server" EnableClientScript="False" Display="None" />
                    </div>

                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel" style="margin-bottom: -1%;">
                       
                        <div class="panel-body">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-2 colpadding0 entrycount" style="width: 15%;">
                                    <div class="col-md-3 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    
                                </div>
                                <div class="col-md-2 colpadding0" style="margin-left: -3%;">
                                    <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select" Style="width: 110px;">
                                        <asp:ListItem Text="Risk" Value="-1" />
                                        <asp:ListItem Text="Critical" Value="3" />
                                        <asp:ListItem Text="High" Value="0" />
                                        <asp:ListItem Text="Medium" Value="1" />
                                        <asp:ListItem Text="Low" Value="2" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2 colpadding0" style="margin-left: -4.5%;">
                                    <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 select_location" 
                                        Style="width: 130px;"></asp:DropDownList>
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-left: -2.4%; width:33.5% !important">
                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 325px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>
                                     
                                    <div class="col-md-1 colpadding0" style="width:6.5%">
                                        <asp:Button ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-search" runat="server" Text="Apply" />
                                    </div>
                                    <div class="col-md-1 colpadding0" style="width:13%">
                                        <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
                                    </div>
                                    <div class="col-md-1 colpadding0" style="float: right;">
                                        <asp:LinkButton runat="server" CssClass="btn btn-search" OnClick="linkbutton_onclick">Back</asp:LinkButton>
                                    </div>
                                </div>
                                <!--advance search starts-->
                                <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width: 1000px">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>

                                            <div class="modal-body" style="margin-left: 50px">
                                                <h2 style="text-align: center; margin-top: 10px;">Advanced Search</h2>


                                                <div class="col-md-12 colpadding0">
                                                    <div class="table-advanceSearch-selectOpt">
                                                        <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="table-advanceSearch-selectOpt">
                                                        <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                                        <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:Panel ID="PanelSearchType" runat="server">
                                                        <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                                                        </div>
                                                    </asp:Panel>
                                                    <div class="clearfix"></div>
                                                    <asp:Panel ID="Panel1" runat="server">
                                                        <div id="Div1" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Height="35px" Width="200px" Style="padding-left: 7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtAdvStartDate" CssClass="StartDate" OnClientClick="callOnButtonYes1()" />
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Height="35px" Width="200px" Style="padding-left: 7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtAdvEndDate" CssClass="StartDate" />
                                                        </div>
                                                    </asp:Panel>
                                                    <div class="clearfix"></div>

                                                    <div class="table-advanceSearch-buttons" style="height: 30px; margin: 10px auto;">
                                                        <asp:Button Text="Search" class="btn btn-search" runat="server" OnClientClick="return hidediv();" ValidationGroup="DocumentsValidation" OnClick="Submit" />
                                                        <button type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                        <div class="clearfix"></div>

                                                    </div>

                                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="alert alert-block alert-danger fade in"
                                                        ValidationGroup="DocumentsValidation" />
                                                    <asp:CustomValidator ID="cvDocuments" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                                        ValidationGroup="DocumentsValidation" Display="None" />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--advance search ends-->
                                <!-- Advance Search scrum-->
                                <div class="clearfix"></div>
                                <div class="col-md-12 AdvanceSearchScrum">
                                    <div id="divAdvSearch" runat="server" visible="false">
                                        <p>
                                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                                        </p>
                                        <p>
                                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                                        </p>
                                    </div>
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                </div>
                            </div>
                    </div>

                    <div class="clearfix"></div>

                    <div style="margin-bottom: 4px">
                        <asp:GridView runat="server" ID="grdRviseCompliances" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                            BorderWidth="0px"
                            OnRowCommand="grdRviseCompliances_RowCommand" AllowPaging="True" PageSize="10" Width="100%" DataKeyNames="ScheduledOnID"
                            CssClass="table" OnRowDataBound="grdRviseCompliances_RowDataBound">
                            <Columns>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                            <asp:Label ID="lblShortDiscription" runat="server" Text='<%# Eval("ShortDescription") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Performer" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerformer" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID"),"S") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Reviewer">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID"),"S") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Due Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Period">
                                    <ItemTemplate>
                                        <%# Eval("ForMonth")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReviseCompliances" runat="server" CommandName="ReviseCompliance" CommandArgument='<%# Eval("ScheduledOnID") +" , "+ Eval("ComplianceInstanceID") %>'
                                            ToolTip="Revise Compliance."><a data-toggle="modal" href="#Newaddremider"/><img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>'  data-toggle="tooltip" data-placement="bottom" alt="Revise Compliance" title="Revise Compliance" /></asp:LinkButton>
                                        <%--<a data-toggle="modal" href="#Newaddremider">--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>

                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0">
                        </div>

                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 20px;">
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>

                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>

                    </div>
                    </section>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <%--<div id="divReviseCompliance">--%>
    <div>
        <div class="modal fade" id="Newaddremider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 750px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ReviseComplianceDetailsGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>

                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Compliance Details</legend>
                                    <asp:Label ID="lblShorDescription" runat="server" Style="display: block; float: left; font-size: 16px; color: #333; margin-bottom: 5px;">
                                    </asp:Label>
                                </fieldset>

                                <div runat="server" id="divGSTComplianceList">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                        <legend>Compliance mapped Other Locations</legend>
                                        <asp:Panel runat="server" ScrollBars="Auto" Height="250px">
                                            <asp:GridView runat="server" ID="grdGstMappedCompliance" AutoGenerateColumns="false" AllowSorting="true"
                                                ShowHeaderWhenEmpty="true"
                                                AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                            <asp:Label ID="lblInstanceID" runat="server" Visible="false" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                            <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                            <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                                            <asp:Label ID="lblCustomerBranchID" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                    Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label ID="LabelBranch" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Due Date">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Action">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkComplianceAll" Text="Select All" runat="server" onclick="REcheckAll(this);" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkCompliance" runat="server" CssClass="classrem" onclick="REchildcheckAll(this);" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </fieldset>
                                </div>

                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                    <legend>Revise Compliance Details</legend>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Revise Date</label>
                                        <asp:TextBox runat="server" ID="tbxDate" Width="100px" class="form-control" />
                                        <asp:RequiredFieldValidator ErrorMessage="Provide Date." ControlToValidate="tbxDate"
                                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                    </div>

                                    <div style="margin-bottom: 7px" class="form-group">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Remarks</label>
                                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" Style="height: 50px; width: 388px;" class="form-control" />
                                        <asp:RequiredFieldValidator ErrorMessage="Required Remark." ControlToValidate="tbxRemarks"
                                            runat="server" ID="RequiredFieldValidator2" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                    </div>
                                    <% if (UploadDocumentLink == "True")
                                        {%>
                                    <div style="margin-bottom: 7px; margin-top: 10px;" runat="server" id="div5">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Upload Compliance Document(s) Link</label>
                                        <asp:TextBox runat="server" ID="TxtComplianceDocument" class="form-control" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-top: 10px;" runat="server" id="div6">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Upload Working Document(s) Link</label>
                                        <asp:TextBox runat="server" ID="TxtWorkingDocument" class="form-control" />
                                    </div>
                                    <%}%>
                                    <div style="margin-bottom: 7px; margin-top: 10px;" runat="server" id="divUploadDocument">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Upload Compliance Document(s)</label>
                                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" Style="color: black;" />
                                        <asp:Label ID="Label2" Style="font-size: 11px; color: #333;" runat="server" Text="(Only PDF, Excel, Doc, JPEG and jpg documents allowed)"></asp:Label>
                                        <asp:RequiredFieldValidator ErrorMessage="Select Document to Upload." ControlToValidate="fuSampleFile"
                                            runat="server" ID="rfvFile" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                                    </div>

                                    <div style="margin-bottom: 7px; margin-top: 10px;" runat="server" id="divWorkingfiles">
                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                            Upload Working Files(s)</label>
                                        <asp:FileUpload ID="FileUpload1" Multiple="Multiple" runat="server" Style="color: black;" />
                                        <asp:Label ID="Label3" runat="server" Style="font-size: 11px; color: #333;" Text="(Only PDF, Excel, Doc, JPEG and jpg documents allowed)"></asp:Label>
                                    </div>

                                    <div class="table-advanceSearch-buttons">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" class="btn btn-search" OnClientClick="if (!ValidateFile()) return false;"
                                            ValidationGroup="ReviseComplianceDetailsGroup" OnClick="btnSave_Click" />
                                        <asp:Button Text="Close" runat="server" ID="btnClose" class="btn btn-search" data-dismiss="modal" OnClick="btnClose_Click" />
                                    </div>
                                </fieldset>
                                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                    <asp:GridView runat="server" ID="grdReviseVersionHistory" AutoGenerateColumns="false" 
                                        AllowSorting="true"
                                        AllowPaging="true" PageSize="12" GridLines="none" CssClass="table"
                                        OnPageIndexChanging="grdReviseVersionHistory_OnPageIndexChanging"
                                        OnRowCommand="grdReviseVersionHistory_RowCommand"
                                        CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px"
                                        DataKeyNames="ID">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Revise Date" ItemStyle-Width="15%">
                                                <ItemTemplate>
                                                    <%# Eval("VersionDate") != null ? Convert.ToDateTime(Eval("VersionDate")).ToString("dd-MMM-yyyy") : ""%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Version" HeaderText="Version" ItemStyle-Width="10%" />
                                            <asp:BoundField DataField="VersionComment" HeaderText="Remark" />
                                           <asp:TemplateField HeaderText="" ItemStyle-Width="15%">
                                                <ItemTemplate>                                                                                                                                                         
                                                    <asp:LinkButton ID="lnkDoCViewDel" runat="server" CommandName="Delete_Document" 
                                                        CommandArgument='<%# Eval("ID") %>' Visible='<%# CanChangeStatus("A") %>'>
                                                        <img src="../Images/delete_icon_new.png" alt="Delete Document" title="Delete Document" data-toggle="tooltip" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View" ItemStyle-Width="15%">
                                                <ItemTemplate>                                                                                                                                                         
                                                    <asp:LinkButton ID="lnkDoCView" runat="server" CommandName="View" 
                                                        CommandArgument='<%# Eval("ID") + ","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'>
                                                        <img src="../Images/view-doc.png" alt="View Document" title="Viewsss Document" data-toggle="tooltip" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="DocumentReviewPopUp111" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#DocumentReviewPopUp111').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                            OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                    runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerReviewAll111" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
