﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Questions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <link href="../content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../masters/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-multiselect.js"></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=lstActs]').multiselect
            ({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for Acts..',
                nSelectedText: ' - Act(s) selected',
                enableFiltering: true,
                //includeSelectAllOption: true,
                //nonSelectedText: 'Select Acts' // Here you can change with your desired text as per your requirement.
            });

            $('[id*=lstCompliances]').multiselect
          ({
              includeSelectAllOption: true,
              numberDisplayed: 2,
              buttonWidth: '100%',
              enableCaseInsensitiveFiltering: true,
              filterPlaceholder: 'Type to Search for Compliances..',
              nSelectedText: ' - Compliance(s) selected',
              enableFiltering: true,

              //includeSelectAllOption: true,
              //nonSelectedText: 'Select Compliances' // Here you can change with your desired text as per your requirement.
          });

            $('[id*=lsttypeofquestions]').multiselect
           ({
               includeSelectAllOption: true,
               nonSelectedText: 'Select Type of Question' // Here you can change with your desired text as per your requirement.
           });

        });



    </script>

    <style type="text/css">
        button.multiselect.dropdown-toggle.btn.btn-default {
            width: 565px;
            text-align: left;
        }

        b.caret {
            float: right;
            /*margin-top: 22px;*/
            margin-top: 8px;
        }

        .caret {
            margin-left: 5px !important;
        }

        label.checkbox {
            width: 560px;
        }

        .multiselect-container > li > a > label {
            max-width: 100%;
            overflow: hidden;
            /*text-overflow: ellipsis; //use this if you want to shorten*/
            white-space: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container">

                    <div class="row" style="padding-bottom: 10px; margin-left: -4px; padding-top: 2px;">
                        <div class="col-md-12">
                            <div style="float: left; margin: 10px; width: 130px;">
                                <label for="lblQuestion">Question</label>
                            </div>
                            <div style="width: 50%; float: left; margin: 10px">
                                <input type="text" style="display: inline; width: 100%; color: gray; height: 28px;" class="form-control" name="QuestionName" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 10px; margin-left: -4px; padding-top: 2px;">
                        <div class="col-md-12">
                            <div style="float: left; margin: 10px; width: 130px;">
                                <label for="lblAct">Select Act</label>
                            </div>
                            <div style="width: 50%; float: left; margin: 10px">
                                <asp:ListBox ID="lstActs" Width="100%" CssClass="dropdown dropdown-menu" runat="server" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="lstActs_SelectedIndexChanged"></asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 10px; margin-left: -4px; padding-top: 2px;">
                        <div class="col-md-12">
                            <div style="float: left; margin: 10px; width: 130px;">
                                <label for="lblCompliance">Select Compliance</label>
                            </div>
                            <div style="width: 50%; float: left; margin: 10px">
                                <asp:ListBox ID="lstCompliances" Width="100%" CssClass="dropdown dropdown-menu" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 10px; margin-left: -4px; padding-top: 2px;">
                        <div class="col-md-12">
                            <div style="float: left; margin: 10px; width: 130px;">
                                <label for="lbltypeofquestion">Type Of Question</label>
                            </div>
                            <div style="width: 50%; float: left; margin: 10px">
                                <asp:ListBox ID="lsttypeofquestions" Width="100%" CssClass="dropdown dropdown-menu" runat="server" SelectionMode="Multiple">
                                    <asp:ListItem Text="Value" Value="1" />
                                    <asp:ListItem Text="<" Value="2" />
                                    <asp:ListItem Text=">" Value="3" />
                                    <asp:ListItem Text="=" Value="4" />
                                    <asp:ListItem Text="In between" Value="5" />
                                </asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">

                        <asp:Button Text="Submit Students" CssClass="btn btn-default btn-success" runat="server" />
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

</body>
</html>
