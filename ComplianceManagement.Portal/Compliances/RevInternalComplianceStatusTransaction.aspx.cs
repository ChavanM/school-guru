﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class RevInternalComplianceStatusTransaction : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        static string sampleFormPath1 = "";
        public static string TaskInternalDocViewPath = "";
        public static List<SP_TaskInstanceTransactionInternalView_Result> MastersTasklistinternal = new List<SP_TaskInstanceTransactionInternalView_Result>();
        public static List<TaskDocumentsView> MastersTaskDocumentslistinternal = new List<TaskDocumentsView>();
        protected bool IsDocumentReviewCompulsary = false;        
        public static string LicenseDocPath = "";
        protected static string status;
        public string ISmail;
        protected void Page_Load(object sender, EventArgs e)
        {
            ISmail = "0";
            try
            {
                ISmail = Convert.ToString(Request.QueryString["ISM"]);
                if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                {
                    lnkgotoportal.Visible = true;
                }
                else
                {
                    lnkgotoportal.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                ISmail = "0";
            }
            if (!IsPostBack)
            {
                status = Convert.ToString(Request.QueryString["status"].ToString());
                OpenTransactionPage(Convert.ToInt32(Request.QueryString["SOID"].ToString()), Convert.ToInt32(Request.QueryString["CID"].ToString()));
            }
        }
        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var ICSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            if (RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                if (status == "Registered" || status == "Validity Expired")
                {

                }
                else
                {
                    btnSave3.Visible = false;
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "This compliance is already closed.";
                }
            }
            //else
            //{
            //    btnSave3.Visible = true;
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        lblLicenseType.Text = string.Empty;
                        lblLicenseNumber.Text = string.Empty;
                        lblLicenseTitle.Text = string.Empty;
                        lblApplicationDate.Text = string.Empty;
                        lblStartdate.Text = string.Empty;
                        lblEndDate.Text = string.Empty;

                        txtLicenseTitle.Text = string.Empty;
                        txtLicenseNo.Text = string.Empty;
                        txtStartDate.Text = string.Empty;
                        txtEndDate.Text = string.Empty;

                        lblRisk3.Text = string.Empty;
                        int customerbranchID = -1;
                        ViewState["complianceInstanceID"] = ScheduledOnID;
                        var complianceCategory = Business.InternalComplianceManagement.GetInternalComplianceCategoryByInstanceID(ScheduledOnID);
                        var AllComData = Business.ComplianceManagement.GetPeriodLocationPerformerInternal(ScheduledOnID, complianceInstanceID);
                        var complianceInfo = Business.InternalComplianceManagement.GetInternalComplianceByInstanceID(ScheduledOnID);
                        var complianceForm = Business.InternalComplianceManagement.GetInternalComplianceFormByID(complianceInfo.ID);
                        var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID(ScheduledOnID);
                        var showHideButton = BindSubTasks(ScheduledOnID, 4, customerbranchID);

                        if (AllComData != null)
                        {
                            customerbranchID = AllComData.CustomerBranchID;
                            lblLocation.Text = AllComData.Branch;
                            lblDueDate.Text = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MMM-yyyy");
                            hiddenDueDateReviewInternal.Value = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MM-yyyy");
                            if (AuthenticationHelper.CustomerID == 63)
                            {
                                lblPeriod1.Text = Business.ComplianceManagement.PeriodReplace(AllComData.ForMonth);
                                lblPeriod.Text = AllComData.ForMonth;
                            }
                            else
                            {
                                lblPeriod1.Text = AllComData.ForMonth;
                                lblPeriod.Text = AllComData.ForMonth;
                            }
                        }
                        if (complianceInfo != null)
                        {
                            var LicenseID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                             where row.ComplianceInstanceID == complianceInstanceID
                                             select row.LicenseID).FirstOrDefault();

                            var MasterTransction = InternalLicenseMgmt.GetPreviousInternalLicenseDetail(LicenseID);
                            if (MasterTransction != null)
                            {
                                Session["LicenseID"] = MasterTransction.LicenseID;
                                lblLicenseType.Text = MasterTransction.LicensetypeName;
                                lblLicenseNumber.Text = MasterTransction.LicenseNo;
                                lblLicenseTitle.Text = MasterTransction.Licensetitle;
                                lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                                lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                                lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                                txtCost.Text = Convert.ToString(MasterTransction.Cost);
                            var LicensedocumentVersionData = InternalLicenseMgmt.GetInternalFileData(LicenseID, -1).Select(x => new
                                {
                                    ID = x.ID,
                                    ScheduledOnID = x.ScheduledOnID,
                                    LicenseID = x.LicenseID,
                                    FileID = x.FileID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                    ISLink = x.ISLink,
                                    FilePath = x.FilePath,
                                    FileName = x.FileName
                                }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                                rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                rptLicenseVersion.DataBind();

                                rptLicenseDocumnets.DataSource = null;
                                rptLicenseDocumnets.DataBind();
                            }

                            var recentlicenstransactionStatus = (from row in entities.RecentInternalLicenseTransactionViews
                                                                 where row.LicenseID == LicenseID
                                                                 select row).FirstOrDefault();

                            if (recentlicenstransactionStatus.StatusID == 7)
                            {
                                fieldsetRenewal.Visible = true;
                                var instancedetails = (from row in entities.Lic_tbl_InternalLicenseInstance
                                                       where row.ID == LicenseID
                                                       select row).FirstOrDefault();
                                if (instancedetails != null)
                                {
                                    txtLicenseTitle.Text = instancedetails.LicenseTitle;
                                    txtLicenseNo.Text = instancedetails.LicenseNo;
                                    txtStartDate.Text = instancedetails.StartDate != null ? instancedetails.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                                    txtEndDate.Text = instancedetails.EndDate != null ? instancedetails.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                                    hdnlicremindbeforenoofdays.Value = instancedetails.RemindBeforeNoOfDays.ToString();
                                    hdnlicensetypeid.Value = instancedetails.LicenseTypeID.ToString();
                                }
                            }
                            else
                            {
                                fieldsetRenewal.Visible = false;
                            }
                            var LICInfo = GetByLICID(LicenseID);
                            txtFileno.Text = LICInfo.FileNO;
                            txtphysicalLocation.Text = LICInfo.PhysicalLocation;
                            lblComplianceID3.Text = Convert.ToString(complianceInfo.ID);
                            lblCategory.Text = Convert.ToString(complianceCategory.Name);
                            lblComplianceDiscription3.Text = complianceInfo.IShortDescription;
                            lblFrequency3.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int)complianceInfo.IFrequency : -1));
                            string risk = Business.InternalComplianceManagement.GetRiskType(complianceInfo);
                            lblRiskType3.Text = Business.InternalComplianceManagement.GetRisk(complianceInfo);
                            lblRisk3.Text = risk;
                            if (risk == "HIGH")
                            {
                                divRiskType3.Attributes["style"] = "background-color:red;";
                            }
                            else if (risk == "MEDIUM")
                            {
                                divRiskType3.Attributes["style"] = "background-color:yellow;";
                            }
                            else if (risk == "LOW")
                            {
                                divRiskType3.Attributes["style"] = "background-color:green;";
                            }
                        }

                        if (complianceInfo.IsDocumentRequired == false)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "enableControls()", true);
                        }
                        if (complianceInfo.IUploadDocument == true && complianceForm != null)
                        {
                            lbDownloadSample3.Text = "<u style=color:blue>Click here</u> to download sample form.";
                            lbDownloadSample3.CommandArgument = complianceForm.IComplianceID.ToString();
                            lbDownloadSample3.Text = "Download";
                            lblNote.Visible = true;
                            lnkViewSampleForm3.Visible = true;
                            lblSlash1.Visible = true;
                            sampleFormPath1 = complianceForm.Name;
                            lnkViewSampleForm3.Visible = true;
                            lblSlash1.Visible = true;

                            #region save samle form
                            string Filename = complianceForm.Name;
                            string filePath = sampleFormPath1;
                            if (filePath != null)
                            {
                                try
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    bw.Write(complianceForm.FileData);
                                    bw.Close();
                                    lblpathsample3.Text = FileName;
                                }
                                catch (Exception ex)
                                {

                                }
                                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(Convert.ToInt32(lbDownloadSample3.CommandArgument));

                                rptComplianceSampleView1.DataSource = complianceform;
                                rptComplianceSampleView1.DataBind();

                            }
                            #endregion
                        }
                        else
                        {
                            lblNote.Visible = false;
                            lblSlash1.Visible = false;
                            lnkViewSampleForm3.Visible = false;
                            sampleFormPath1 = "";
                        }


                        //var documentVersionData = DocumentManagement.GetFileDataInternal(Convert.ToInt32(RecentComplianceTransaction.InternalComplianceScheduledOnID)).Select(x => new
                        //{
                        //    ID = x.ID,
                        //    ScheduledOnID = x.InternalComplianceScheduledOnID,
                        //    FileID = x.FileID,
                        //    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                        //    VersionDate = x.VersionDate,
                        //    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                        //}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        var documentVersionData = DocumentManagement.GetFileDataInternal(Convert.ToInt32(RecentComplianceTransaction.InternalComplianceScheduledOnID)).Select(x => new
                        {
                            ID = x.ID,
                            ScheduledOnID = x.InternalComplianceScheduledOnID,
                            FileID = x.FileID,
                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            VersionDate = x.VersionDate,
                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                            ISLink = x.ISLink,
                            FilePath = x.FilePath,
                            FileName = x.FileName
                        }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                        rptComplianceVersion3.DataSource = documentVersionData;
                        rptComplianceVersion3.DataBind();

                        rptComplianceDocumnets3.DataSource = null;
                        rptComplianceDocumnets3.DataBind();

                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();


                        BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                        //added by rahul on 17 JAN 2019
                        if (RecentComplianceTransaction.Status == "Complied but pending review")
                        {
                            //Complied but pending review
                            rdbtnStatus3.SelectedValue = "4";
                        }
                        else
                        {
                            //Complied Delayed but pending review
                            rdbtnStatus3.SelectedValue = "5";
                        }
                        BindTransactions(ScheduledOnID);
                        tbxRemarks3.Text = string.Empty;
                        tbxDate3.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                        hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                        hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                        rdbtnStatus3.Attributes.Add("disabled", "disabled");
                        tbxRemarks3.Attributes.Add("disabled", "disabled");
                        tbxDate3.Attributes.Add("disabled", "disabled");
                        btnSave3.Attributes.Add("disabled", "disabled");
                        btnReject3.Attributes.Add("disabled", "disabled");

                        upComplianceDetails3.Update();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
           // }
        }
        public static Lic_tbl_InternalLicenseInstance_Log GetByLICID(long LICID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Lic_tbl_InternalLicenseInstance_Log
                            where row.LicenseID == LICID
                            select row).FirstOrDefault();

                return data;
            }
        }
        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_TaskInstanceTransactionInternalView_Result> masterdocumentData = new List<SP_TaskInstanceTransactionInternalView_Result>();

                    masterdocumentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                          where row.RoleID == roleID
                                          && row.CustomerBranchID == CustomerBranchid
                                          && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                          select row).ToList();

                    var documentData = (from row in masterdocumentData
                                        where row.ParentID == null
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                  
                    var TaskdocumentData = (from row in entities.TaskDocumentsViews
                                            where row.CustomerID == customerID && row.TaskType == 2
                                            select row).ToList();
                    if (TaskdocumentData.Count > 0)
                    {
                        TaskdocumentData = TaskdocumentData.Where(entry => entry.TaskStatusID == 4 || entry.TaskStatusID == 5).ToList();
                        MastersTaskDocumentslistinternal = TaskdocumentData;
                    }
                    MastersTasklistinternal = masterdocumentData;

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();
                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        protected void rptComplianceDocumnets3_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls();", true);
                }
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersion3_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls();", true);
                }
                else
                {

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptComplianceDocumnets3.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptComplianceDocumnets3.DataBind();

                            rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName,
                                FileID = x.FileID
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptComplianceVersion3.DataSource = documentVersionData;
                            rptComplianceVersion3.DataBind();

                            upComplianceDetails3.Update();


                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Regin 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();


                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                         string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Regin 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {

                                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        // string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                            if (ComplianceFileData.Count > 0)
                            {

                                rptIComplianceVersionView.DataSource = ComplianceFileData.OrderBy(entry => entry.Version);
                                rptIComplianceVersionView.DataBind();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            UpdatePanel2.Update();
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            i++;
                                            UpdatePanel2.Update();
                                            lblMessage.Text = "";
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalfileReview('" + CompDocReviewPath + "');", true);
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    rptIComplianceVersionView.DataSource = ComplianceFileData.OrderBy(entry => entry.Version);
                                    rptIComplianceVersionView.DataBind();

                                    //comment by rahul on 20 JAN 2017
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            UpdatePanel2.Update();
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            i++;
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            UpdatePanel2.Update();
                                            lblMessage.Text = "";
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalfileReview('" + CompDocReviewPath + "');", true);
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("RedirectURL"))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls();", true);
            }
            else
                DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        

        protected void grdTransactionHistory3_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.InternalComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory3.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory3.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory3.DataSource = complianceTransactionHistory;
                grdTransactionHistory3.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory3_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory3.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory3.DataSource = Business.InternalComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory3.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {

                rdbtnStatus3.Items.Clear();
                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (ComplianceStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus3.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus3.Visible = allowedStatusList.Count > 0 ? true : false;
                divDated3.Visible = allowedStatusList.Count > 0 ? true : false;
                divCost3.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<InternalComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.InternalComplianceAssignments
                                     where row.InternalComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        protected void btnSave3_Click(object sender, EventArgs e)
        {
            var ICSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            if (RecentComplianceTransaction.ComplianceStatusID == 4 || RecentComplianceTransaction.ComplianceStatusID == 5)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance already performed.";
            }
            else
            {
                #region Save Code
                try
                {
                    int StatusID = 0;
                    if (lblStatus3.Visible)
                        StatusID = Convert.ToInt32(rdbtnStatus3.SelectedValue);
                    else
                        StatusID = Convert.ToInt32(ComplianceManagement.Business.InternalComplianceManagement.GetClosedTransactionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID);

                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                    {
                        InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = StatusID,
                        StatusChangedOn = DateTime.ParseExact(tbxDate3.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks3.Text
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    Business.InternalComplianceManagement.CreateTransaction(transaction);
                    //Update License Status
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                         where row.ComplianceInstanceID == ComplianceInstanceID
                                         select row.LicenseID).FirstOrDefault();

                        var recentlicenstransactionStatus = (from row in entities.RecentInternalLicenseTransactionViews
                                                             where row.LicenseID == LicenseID
                                                             select row).FirstOrDefault();

                        if (recentlicenstransactionStatus.StatusID == 7)
                        {
                            bool saveSuccess = false;
                            long recurringdays = 0;
                            long licensetypeid = 0;
                            if (!string.IsNullOrEmpty(hdnlicremindbeforenoofdays.Value))
                            {
                                recurringdays = Convert.ToInt64(hdnlicremindbeforenoofdays.Value);
                            }
                            if (!string.IsNullOrEmpty(hdnlicensetypeid.Value))
                            {
                                licensetypeid = Convert.ToInt64(hdnlicensetypeid.Value);
                            }

                            #region Save License Instance 
                            Lic_tbl_InternalLicenseInstance licenseRecord = new Lic_tbl_InternalLicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                            Lic_tbl_InternalLicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_InternalLicenseInstance_Log()
                            {
                                CustomerID = AuthenticationHelper.CustomerID,
                                UpdatedBy = AuthenticationHelper.UserID,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                                LicenseID = LicenseID,
                                RemindBeforeNoOfDays = recurringdays,
                                LicenseTypeID = licensetypeid,
                                IsStatutory = true,
                            };
                            if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                            {
                                licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                            }
                            if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                            {
                                licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                            }
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }

                            if (!string.IsNullOrEmpty(txtCost.Text))
                            {
                                licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);
                                newLic_tbl_LicenseInstance_Log.Cost = Convert.ToDecimal(txtCost.Text);
                            }

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                            {
                                licenseRecord.FileNO = Convert.ToString(txtFileno.Text);
                            }


                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                            licenseRecord.ID = LicenseID;
                            InternalLicenseMgmt.UpdateLicenseNew(licenseRecord);
                            InternalLicenseMgmt.CreateInternalLicenseLog(newLic_tbl_LicenseInstance_Log);
                            int statusId = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(txtStartDate.Text)) || !string.IsNullOrEmpty(Convert.ToString(txtEndDate.Text)))
                            {
                                if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date)
                                {
                                    statusId = 3;        //Expired
                                }
                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                {
                                    statusId = 4;            //Expiring
                                }
                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                {
                                    statusId = 2;            //Active
                                }
                                else
                                {
                                    statusId = 3;        //Expired
                                }
                            }
                            else
                                statusId = 1;       //Draft                            


                            string status = string.Empty;
                            if (statusId == 2)
                            {
                                status = "Active";
                            }
                            else if (statusId == 3)
                            {
                                status = "Expired";
                            }
                            else if (statusId == 4)
                            {
                                status = "Expiring";
                            }
                            //Lic_tbl_InternalLicenseStatusTransaction newStatusRecord = new Lic_tbl_InternalLicenseStatusTransaction()
                            //{
                            //    CustomerID = (int)AuthenticationHelper.CustomerID,
                            //    LicenseID = LicenseID,
                            //    StatusID = statusId,
                            //    StatusChangeOn = DateTime.Now,
                            //    IsActive = true,
                            //    CreatedBy = AuthenticationHelper.UserID,
                            //    CreatedOn = DateTime.Now,
                            //    UpdatedBy = AuthenticationHelper.UserID,
                            //    UpdatedOn = DateTime.Now,
                            //    Remark = "Change license status to " + status + ""
                            //};
                            //saveSuccess = InternalLicenseMgmt.CreateInternalLicenseStatusTransaction(newStatusRecord);

                            Lic_tbl_InternalLicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_InternalLicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = statusId,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to " + status + ""
                            };
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            entities.Lic_tbl_InternalLicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();

                            #endregion

                            #region Statutory Non Statutory Compliances Entry                       
                            try
                            {
                                bool statutorySuccess = false;
                                #region Statutory
                                try
                                {
                                    long complianceScheduleOnId = 0;
                                    long complianceTransactionId = 0;
                                    statutorySuccess = false;
                                    if (ComplianceInstanceID > 0)
                                    {
                                        var AssignedRole = GetAssignedUsers((int)ComplianceInstanceID);
                                        if (recurringdays == 0)
                                        {
                                            InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                            {
                                                ScheduledOn = licenseRecord.EndDate.Value.Date,
                                                InternalComplianceInstanceID = ComplianceInstanceID,
                                                IsActive = true,
                                                IsUpcomingNotDeleted = true,
                                            };
                                            complianceScheduleOnId = InternalLicenseMgmt.CreateInternalComplianceScheduleOn(complianceScheduleOn);

                                            Lic_tbl_InternalLicenseStatusTransaction newStatusRecord = new Lic_tbl_InternalLicenseStatusTransaction()
                                            {
                                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                                LicenseID = LicenseID,
                                                StatusID = statusId,
                                                StatusChangeOn = DateTime.Now,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                Remark = "Change license status to " + status + "",
                                                InternalComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            saveSuccess = InternalLicenseMgmt.CreateInternalLicenseStatusTransaction(newStatusRecord);

                                            Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                IsActivation = "Active",
                                                IsStatutoryORInternal = "S"
                                            };
                                            InternalLicenseMgmt.CreateLicenseLicenseInternalComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);


                                            InternalComplianceTransaction complianceTransaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = ComplianceInstanceID,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                InternalComplianceScheduledOnID = complianceScheduleOnId,
                                            };
                                            complianceTransactionId = InternalLicenseMgmt.CreateInternalComplianceTransaction(complianceTransaction);

                                            if (AssignedRole.Count > 0)
                                            {
                                                foreach (var roles in AssignedRole)
                                                {
                                                    if (roles.RoleID != 6)
                                                    {
                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                         select RT).ToList();

                                                        reminders.ForEach(day =>
                                                        {
                                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                            {
                                                                ComplianceAssignmentID = roles.ID,
                                                                ReminderTemplateID = day.ID,
                                                                ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                            };
                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                            entities.InternalComplianceReminders.Add(reminder);

                                                        });
                                                    }
                                                }
                                                entities.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            #region Application Days
                                            DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                            Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(recurringdays));
                                            InternalComplianceScheduledOn complianceScheduleOn = new InternalComplianceScheduledOn()
                                            {
                                                ScheduledOn = Applicationdate.Date,
                                                InternalComplianceInstanceID = ComplianceInstanceID,
                                                IsActive = true,
                                                IsUpcomingNotDeleted = true,
                                            };
                                            complianceScheduleOnId = InternalLicenseMgmt.CreateInternalComplianceScheduleOn(complianceScheduleOn);


                                            Lic_tbl_InternalLicenseStatusTransaction newStatusRecord = new Lic_tbl_InternalLicenseStatusTransaction()
                                            {
                                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                                LicenseID = LicenseID,
                                                StatusID = statusId,
                                                StatusChangeOn = DateTime.Now,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                Remark = "Change license status to " + status + "",
                                                InternalComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            saveSuccess = InternalLicenseMgmt.CreateInternalLicenseStatusTransaction(newStatusRecord);

                                            Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                IsActivation = "Application",
                                                IsStatutoryORInternal = "S"
                                            };
                                            InternalLicenseMgmt.CreateLicenseLicenseInternalComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                            InternalComplianceTransaction complianceTransaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = ComplianceInstanceID,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                InternalComplianceScheduledOnID = complianceScheduleOnId,
                                            };
                                            complianceTransactionId = InternalLicenseMgmt.CreateInternalComplianceTransaction(complianceTransaction);

                                            if (AssignedRole.Count > 0)
                                            {
                                                foreach (var roles in AssignedRole)
                                                {
                                                    if (roles.RoleID != 6)
                                                    {
                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                         select RT).ToList();

                                                        reminders.ForEach(day =>
                                                        {
                                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                                            {
                                                                ComplianceAssignmentID = roles.ID,
                                                                ReminderTemplateID = day.ID,
                                                                ComplianceDueDate = Applicationdate.Date,
                                                                RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                            };
                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                            entities.InternalComplianceReminders.Add(reminder);

                                                        });
                                                    }
                                                }
                                                entities.SaveChanges();
                                            }
                                            #endregion
                                        }
                                    }
                                    if (ComplianceInstanceID > 0)
                                        statutorySuccess = true;
                                    saveSuccess = statutorySuccess;

                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    saveSuccess = false;
                                }

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            #endregion
                        }
                        else
                        {

                            Lic_tbl_InternalLicenseInstance licenseRecord1 = new Lic_tbl_InternalLicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                           
                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecord1.Cost = Convert.ToDecimal(txtCost.Text);

                            licenseRecord1.ID = LicenseID;
                            LicenseMgmt.UpdateInternalLicenseCost(licenseRecord1);

                            Lic_tbl_InternalLicenseInstance_Log licenseRecordLog = new Lic_tbl_InternalLicenseInstance_Log()
                            {
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };

                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecordLog.Cost = Convert.ToDecimal(txtCost.Text);

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                licenseRecordLog.FileNO = Convert.ToString(txtFileno.Text);

                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecordLog.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                            licenseRecordLog.LicenseID = LicenseID;
                            LicenseMgmt.UpdateInternalLicenseLogNew(licenseRecordLog);

                            Lic_tbl_InternalLicenseStatusTransaction license = new Lic_tbl_InternalLicenseStatusTransaction()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = 5,
                                StatusChangeOn = DateTime.ParseExact(tbxDate3.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsActive = true,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedOn = DateTime.Now,
                                Remark = "Change license status to applied.",
                                InternalComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            };
                            entities.Lic_tbl_InternalLicenseStatusTransaction.Add(license);
                            entities.SaveChanges();


                            Lic_tbl_InternalLicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_InternalLicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = 5,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to applied."
                            };
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            entities.Lic_tbl_InternalLicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();
                        }
                    }
                    try
                    {
                        Business.InternalComplianceManagement.BindInternalDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                    }
                    catch { }
                    upComplianceDetails3.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ClosePop();", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        protected void btnReject3_Click(object sender, EventArgs e)
        {
            var ICSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            if (RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 8)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance already rejected.";
            }
            else
            {
                #region Save Code
                try
                {

                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                    {
                        InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 6,
                        StatusChangedOn = DateTime.ParseExact(tbxDate3.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks3.Text
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    bool isSucessfull=Business.InternalComplianceManagement.CreateTransaction(transaction);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                         where row.ComplianceInstanceID == ComplianceInstanceID
                                         select row.LicenseID).FirstOrDefault();


                        Lic_tbl_InternalLicenseStatusTransaction license = new Lic_tbl_InternalLicenseStatusTransaction()
                        {
                            CustomerID = (int)AuthenticationHelper.CustomerID,
                            LicenseID = LicenseID,
                            StatusID = 8,
                            StatusChangeOn = DateTime.ParseExact(tbxDate3.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            IsActive = true,
                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            CreatedOn = DateTime.Now,
                            UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            UpdatedOn = DateTime.Now,
                            Remark = "Change license status to Rejected.",
                            InternalComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        };
                        entities.Lic_tbl_InternalLicenseStatusTransaction.Add(license);
                        entities.SaveChanges();

                        Lic_tbl_InternalLicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_InternalLicenseAudit_Log()
                        {
                            CustomerID = (int)AuthenticationHelper.CustomerID,
                            LicenseID = LicenseID,
                            StatusID = 6,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            Remark = "Change license status to Rejected."
                        };
                        if (!string.IsNullOrEmpty(txtStartDate.Text))
                        {
                            newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                        }
                        if (!string.IsNullOrEmpty(txtEndDate.Text))
                        {
                            newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                        }
                        entities.Lic_tbl_InternalLicenseAudit_Log.Add(newLicenseInstance_Log);
                        entities.SaveChanges();

                    }
                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }
                    var Compliance = ComplianceManagement.Business.InternalComplianceManagement.GetInstanceTransactionInternalCompliance(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    if (Compliance != null)
                    {
                        int customerID = -1;
                        customerID = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID)).CustomerID ?? 0;
                        string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        var user = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID));
                        string portalurl = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            portalurl = Urloutput.URL;
                        }
                        else
                        {
                            portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }
                        string message = Settings.Default.EMailTemplate_RejectedCompliance
                                           .Replace("@User", Compliance.User)
                                           .Replace("@ComplianceDescription", Compliance.ShortDescription)
                                           .Replace("@Role", Compliance.Role)
                                           .Replace("@ScheduledOn", Compliance.InternalScheduledOn.ToString("dd-MMM-yyyy"))
                                           .Replace("@From", ReplyEmailAddressName)
                                           .Replace("@URL", Convert.ToString(portalurl));
                        //string message = Settings.Default.EMailTemplate_RejectedCompliance
                        //                    .Replace("@User", Compliance.User)
                        //                    .Replace("@ComplianceDescription", Compliance.ShortDescription)
                        //                    .Replace("@Role", Compliance.Role)
                        //                    .Replace("@ScheduledOn", Compliance.InternalScheduledOn.ToString("dd-MMM-yyyy"))
                        //                    .Replace("@From", ReplyEmailAddressName)
                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Notification :: Internal Compliance has been rejected.", message);                                               
                    }
                    if (isSucessfull)
                    {
                        try
                        {
                            Business.InternalComplianceManagement.BindInternalDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        }
                        catch { }
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Rejected Sucessfully.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        protected void grdTransactionHistory3_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.InternalComplianceManagement.GetFile(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload3_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample3_Click(object sender, EventArgs e)
        {
            try
            {
              
                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample3.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample3.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FileData != null)
                        {
                            string ext = Path.GetExtension(file.Name);
                            string[] filename = file.Name.Split('.');
                            //string str = filename[0] + i + "." + filename[1];
                            string str = filename[0] + i + "." + ext;
                            ComplianceZip.AddEntry(str, file.FileData);
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_InternalSampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails3_Load(object sender, EventArgs e)
        {
            try
            {
                //DateTime date = DateTime.MinValue;
                //if (DateTime.TryParseExact(tbxDate3.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                //}


                //if (lblDueDate.Text != "")
                //{
                //    if (rdbtnStatus3.SelectedValue == "5") //Closed-Delayed
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDateReviewInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        Date = Date.AddDays(1);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueReviewInternal", string.Format("initializeDatePickerOverDueReviewInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //    else if (rdbtnStatus3.SelectedValue == "4") //Closed-Timely 
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDateReviewInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeReviewInternal", string.Format("initializeDatePickerInTimeReviewInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory3_RowCreated(object sender, GridViewRowEventArgs e)
        {
           
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                tbxRemarks3.Text = tbxDate3.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        //filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    //string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets3_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathInternallink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblInternalCompDocpathDownload");

                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }

        protected void rptComplianceVersion3_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashInternalReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblInternalpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDocInternal");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblInternalpathDownload");

                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc1");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion1");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc1");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion1");
            //    scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            //}
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblInternalWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblInternalWorkCompDocpathDownload");

                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                    //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                    //lblMessage.Text += fileName + "Saved Successfully<br>";
                }
            }
        }

        protected void rptIComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                if (AWSData != null)
                {
                    #region AWS Storage
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                    List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                    ComplianceDocument = DocumentManagement.GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("View"))
                    {
                        if (ComplianceFileData.Count > 0)
                        {
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.SecretKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {                                   
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                    }
                                    else
                                    {
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        CompDocReviewPath = filePath1;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        i++;                                      
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalfileReview('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Normal Storage
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                    List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                    ComplianceDocument = DocumentManagement.GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("View"))
                    {
                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        i++;
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalfileReview('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalfileReviewPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                             
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str,  DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptTaskVersionViewInternal1.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptTaskVersionViewInternal1.DataBind();

                                     
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                TaskInternalDocViewPath = filePath1;
                                                TaskInternalDocViewPath = TaskInternalDocViewPath.Substring(2, TaskInternalDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal1('" + TaskInternalDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel3.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal1();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptTaskVersionViewInternal1.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptTaskVersionViewInternal1.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                TaskInternalDocViewPath = FileName;
                                                TaskInternalDocViewPath = TaskInternalDocViewPath.Substring(2, TaskInternalDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal1('" + TaskInternalDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel3.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal1();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptTaskVersionViewInternal1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptTaskVersionViewInternal1.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptTaskVersionViewInternal1.DataBind();

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                TaskInternalDocViewPath = filePath1;
                                                TaskInternalDocViewPath = TaskInternalDocViewPath.Substring(2, TaskInternalDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal1('" + TaskInternalDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal1();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptTaskVersionViewInternal1.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptTaskVersionViewInternal1.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }

                                                bw.Close();

                                                TaskInternalDocViewPath = FileName;
                                                TaskInternalDocViewPath = TaskInternalDocViewPath.Substring(2, TaskInternalDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal1('" + TaskInternalDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal1();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }

                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview1");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");
                    Label lblTaskID = (Label)e.Row.FindControl("lblTaskID");
                    Label lblCbranchId = (Label)e.Row.FindControl("lblCbranchId");
                    Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                    Label lblsubtaskDocuments = (Label)e.Row.FindControl("lblsubtaskIntDocuments");
                    Image chkDocument = (Image)e.Row.FindControl("chkIntDocument");

                    int taskId = Convert.ToInt32(lblTaskID.Text);
                    int CbranchId = Convert.ToInt32(lblCbranchId.Text);

                    if (MastersTasklistinternal.Count > 0)
                    {
                        var documentData = (from row in MastersTasklistinternal
                                            where row.ParentID == (long?)taskId
                                             && row.ForMonth == lblForMonth.Text &&
                                             row.CustomerBranchID == (long?)CbranchId
                                             && row.RoleID == 4
                                            select row).ToList();

                        string strDocumentSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        int DocCounter = 0;
                        foreach (var item in documentData)
                        {
                            string disp = "none";
                            if (item.UserID == AuthenticationHelper.UserID && (item.TaskStatusID == 1 || item.TaskStatusID == 8))
                            {
                                disp = "block";
                            }
                            #region Document
                            if (MastersTaskDocumentslistinternal.Count > 0)
                            {
                                var taskdocumentData = (from row in MastersTaskDocumentslistinternal
                                                        where row.ForMonth == item.ForMonth &&
                                                         row.CustomerBranchID == item.CustomerBranchID
                                                         && row.TaskScheduleOnID == item.TaskScheduledOnID
                                                        select row).ToList();
                                if (taskdocumentData.Count > 0)
                                {
                                    DocCounter = 1;
                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                    strDocumentSubtasks += "</div>";
                                }
                            }
                            #endregion

                            if (1 == 1)
                            {
                                var documentData1 = (from row in MastersTasklistinternal
                                                     where
                                                      row.ForMonth == lblForMonth.Text &&
                                                      row.CustomerBranchID == (long?)CbranchId
                                                      && row.RoleID == 4
                                                       && row.ParentID == item.TaskID
                                                     select row).ToList();
                                foreach (var item1 in documentData1)
                                {
                                    disp = "none";
                                    if (item1.UserID == AuthenticationHelper.UserID && (item1.TaskStatusID == 1 || item1.TaskStatusID == 8))
                                    {
                                        disp = "block";
                                    }
                                    //style='display:"+ disp + "'
                                    #region Document1
                                    if (MastersTaskDocumentslistinternal.Count > 0)
                                    {
                                        var taskdocumentData1 = (from row in MastersTaskDocumentslistinternal
                                                                 where row.ForMonth == item1.ForMonth &&
                                                                  row.CustomerBranchID == item1.CustomerBranchID
                                                                  && row.TaskScheduleOnID == item1.TaskScheduledOnID
                                                                 select row).ToList();
                                        if (taskdocumentData1.Count > 0)
                                        {
                                            DocCounter = 1;
                                            strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item1.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                            strDocumentSubtasks += "</div>";
                                        }
                                    }
                                    #endregion

                                    var documentData2 = (from row in MastersTasklistinternal
                                                         where
                                                           row.ForMonth == lblForMonth.Text &&
                                                          row.CustomerBranchID == (long?)CbranchId
                                                          && row.RoleID == 4
                                                           && row.ParentID == item1.TaskID
                                                         select row).ToList();
                                    foreach (var item2 in documentData2)
                                    {
                                        disp = "none";
                                        if (item2.UserID == AuthenticationHelper.UserID && (item2.TaskStatusID == 1 || item2.TaskStatusID == 8))
                                        {
                                            disp = "block";
                                        }
                                        //style='display:"+ disp + "'
                                        #region Document2
                                        if (MastersTaskDocumentslistinternal.Count > 0)
                                        {
                                            var taskdocumentData2 = (from row in MastersTaskDocumentslistinternal
                                                                     where row.ForMonth == item2.ForMonth &&
                                                                      row.CustomerBranchID == item2.CustomerBranchID
                                                                      && row.TaskScheduleOnID == item2.TaskScheduledOnID
                                                                     select row).ToList();
                                            if (taskdocumentData2.Count > 0)
                                            {
                                                strDocumentSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                strDocumentSubtasks += "</div>";
                                            }
                                        }
                                        #endregion

                                        var documentData3 = (from row in MastersTasklistinternal
                                                             where
                                                               row.ForMonth == lblForMonth.Text &&
                                                              row.CustomerBranchID == (long?)CbranchId
                                                              && row.RoleID == 4
                                                               && row.ParentID == item2.TaskID
                                                             select row).ToList();
                                        foreach (var item3 in documentData3)
                                        {
                                            disp = "none";
                                            if (item3.UserID == AuthenticationHelper.UserID && (item3.TaskStatusID == 1 || item3.TaskStatusID == 8))
                                            {
                                                disp = "block";
                                            }
                                            //style='display:"+ disp + "'
                                            #region Document3
                                            if (MastersTaskDocumentslistinternal.Count > 0)
                                            {
                                                var taskdocumentData3 = (from row in MastersTaskDocumentslistinternal
                                                                         where row.ForMonth == item3.ForMonth &&
                                                                          row.CustomerBranchID == item3.CustomerBranchID
                                                                          && row.TaskScheduleOnID == item3.TaskScheduledOnID
                                                                         select row).ToList();
                                                if (taskdocumentData3.Count > 0)
                                                {
                                                    DocCounter = 1;
                                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item3.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button'  onclick ='downloadTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                    strDocumentSubtasks += "</div>";
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        if (DocCounter == 0)
                        {
                            chkDocument.Visible = false;
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    InternalComplianceForm complianceForm = Business.InternalComplianceManagement.GetSelectedInternalComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    string Filename = complianceForm.Name;
                    string filePath = Filename;
                    if (filePath != null)
                    {
                        try
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            //string[] path = filePath.Split('/');
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm.FileData);
                            bw.Close();
                            lblpathsample3.Text = FileName;

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfile3('" + lblpathsample3.Text + "');", true);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView1 = (LinkButton)e.Item.FindControl("lblSampleView1");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView1);
            }
        }

        protected void rdbtnStatus3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (rdbtnStatus3.SelectedValue == "4") //Closed-Timely
                //{
                //    IsRemarkCompulsary3 = false;
                //    DateTime Date = DateTime.ParseExact(hiddenDueDateReviewInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeReviewInternal", string.Format("initializeDatePickerInTimeReviewInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //}
                //else if (rdbtnStatus3.SelectedValue == "5") //Closed-Delayed
                //{
                //    IsRemarkCompulsary3 = true;
                //    DateTime Date = DateTime.ParseExact(hiddenDueDateReviewInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    Date = Date.AddDays(1);
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueReviewInternal", string.Format("initializeDatePickerOverDueReviewInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region License Documents
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview1");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc1");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetInternalLicenseDocument_Result> ComplianceFileData = new List<SP_GetInternalLicenseDocument_Result>();
                    List<SP_GetInternalLicenseDocument_Result> ComplianceDocument = new List<SP_GetInternalLicenseDocument_Result>();
                    ComplianceDocument = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                FileID = x.FileID,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                             
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<SP_GetInternalLicenseDocument_Result> CMPDocuments = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                        Session["LicenseID"] = commandArg[0];
                        if (CMPDocuments != null)
                        {
                            CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                            List<SP_GetInternalLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetInternalLicenseDocument_Result entityData = new SP_GetInternalLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptLicenseVersionView.DataBind();
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in ComplianceFileData)
                                    {

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer2.Text = "";
                                                lblMessageReviewer2.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                LicenseDocPath = filePath1;
                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                                lblMessageReviewer2.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer2.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptLicenseVersionView.DataBind();

                                    foreach (var file in ComplianceFileData)
                                    {

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer2.Text = "";
                                                lblMessageReviewer2.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                LicenseDocPath = FileName;

                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer2.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer2.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<SP_GetInternalLicenseDocument_Result> CMPDocuments = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["LicenseID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<SP_GetInternalLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            SP_GetInternalLicenseDocument_Result entityData = new SP_GetInternalLicenseDocument_Result();
                            entityData.Version = "1.0";
                            entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer2.Text = "";
                                            lblMessageReviewer2.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            LicenseDocPath = filePath1;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer2.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer2.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer2.Text = "";
                                            lblMessageReviewer2.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            LicenseDocPath = FileName;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer2.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer2.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion
    }
}