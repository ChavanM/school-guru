﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceSubTypeList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceSubTypeList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#DivComplianceSubTypeList').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Compliance Sub Type",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
        <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
             <table width="100%" cellpadding="10px">
                <tr>
                     <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnSave" OnClick="btnSave_Click" Visible="true"/>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  />
                        <asp:TemplateField HeaderText="Nature Of Compliance" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# NutureofCompName(Convert.ToInt32(Eval("NatureOfComplianceID"))) %>' ToolTip='<%# Eval("NatureOfComplianceID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left"  HeaderStyle-HorizontalAlign="Left" >
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" visible="false" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center"/>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311"/>
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
     <div id="DivComplianceSubTypeList">
        <asp:UpdatePanel ID="upComSubType" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; display:none;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                           ID</label>
                        <asp:TextBox runat="server" ID="tbxComsubID" Style="height: 16px; width: 250px;" MaxLength="100" ReadOnly="true" />
                    </div>
                    <div runat="server" id="divNatureOfCompliance" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Nature Of Compliance</label>
                        <asp:DropDownList runat="server" ID="ddlNatureOfCompliance" OnSelectedIndexChanged="ddlNatureOfCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 254px;"
                            CssClass="txtbox">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="Canteen" Value="12" />
                            <asp:ListItem Text="Certificates/Licensing" Value="8" />
                            <asp:ListItem Text="Cleanliness" Value="10" />
                            <asp:ListItem Text="Examination" Value="13" />
                            <asp:ListItem Text="Holiday List" Value="9" />
                            <asp:ListItem Text="Inspection" Value="5" />
                            <asp:ListItem Text="Maintenance" Value="14" />
                            <asp:ListItem Text="Meeting" Value="6" />
                            <asp:ListItem Text="Minimum Wages" Value="11" />
                            <asp:ListItem Text="Notices/Correspondences" Value="15" />
                            <asp:ListItem Text="Payments" Value="1" />
                            <asp:ListItem Text="Registers" Value="7" />
                            <asp:ListItem Text="Report" Value="3" />
                            <asp:ListItem Text="Returns" Value="0" />
                            <asp:ListItem Text="Safety and Welfare" Value="16" />
                            <asp:ListItem Text="Training" Value="4" />
                            <asp:ListItem Text="Others" Value="17" />
                        </asp:DropDownList>
                        <asp:CompareValidator ErrorMessage="Please select Nature of Compliance." ControlToValidate="ddlNatureOfCompliance"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 250px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="ComplianceCategoryValidationGroup"
                            ErrorMessage="Please enter a valid category name." ControlToValidate="tbxName"
                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px; display:none;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="tbxDescription" Style="height: 40px; width: 250px;" TextMode="MultiLine" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 57px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" CssClass="button"
                            ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#DivComplianceSubTypeList').dialog('close');"/>
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
