﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.IO;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceLabelReport : System.Web.UI.Page
    {
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    divcustomer.Visible = true;
                    BindCustomers(UserID);
                }
                else
                {
                    BindLocationFilter();
                    dlFilters_SelectedIndexChanged(null, null);
                }
                tbxFilterLocation.Text = "< Select >";
            }
        }
        private void BindCustomers(int userid)
        {
            try
            {
                ddlcustomer.DataTextField = "Name";
                ddlcustomer.DataValueField = "ID";

                ddlcustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlcustomer.DataBind();

                ddlcustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                int customerid = -1;
                String FileName = String.Empty;
                FileName = "StatutoryLabelComplianceReport";

                DataTable dataToExport = GetGrid();
                dataToExport.Columns.Remove("ComplianceInstanceID");
                dataToExport.Columns.Remove("CustomerBranchID");
                dataToExport.Columns.Remove("CustomerID");
                dataToExport.Columns.Remove("ComplianceType");
                dataToExport.Columns.Remove("EventFlag");
            
                
                if (dataToExport == null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "EmptyDataForExport", " $(function () { alert('No data available for export...'); });", true);
                    return;
                }
                if (AuthenticationHelper.Role != "IMPT")
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerid = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                using (ExcelPackage pck = new ExcelPackage())
                {
                    var cname = CustomerManagement.CustomerGetByIDName(customerid);

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Compliance");
                    ws.Cells["A1"].Style.Font.Bold = true;
                    ws.Cells["A1"].Value = "Customer Name:";

                    ws.Cells["B1:C1"].Merge = true;
                    ws.Cells["B1"].Value = cname;

                    ws.Cells["A2"].Style.Font.Bold = true;
                    ws.Cells["A2"].Value = "Report Name:";

                    ws.Cells["B2:C2"].Merge = true;
                    ws.Cells["B2"].Value = "Statutory Compliance Label Report";

                    ws.Cells["A3"].Style.Font.Bold = true;
                    ws.Cells["A3"].Value = "Report Generated On:";

                    ws.Cells["B3:C3"].Merge = true;
                    ws.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                    ws.Cells["A5"].Style.Font.Bold = true;
                    ws.Cells["B5"].Style.Font.Bold = true;
                    ws.Cells["C5"].Style.Font.Bold = true;
                    ws.Cells["D5"].Style.Font.Bold = true;
                    ws.Cells["E5"].Style.Font.Bold = true;
                    ws.Cells["F5"].Style.Font.Bold = true;
                    ws.Cells["G5"].Style.Font.Bold = true;
                    ws.Cells["H5"].Style.Font.Bold = true;
                    ws.Cells["I5"].Style.Font.Bold = true;

                    ws.Cells["J5"].Style.Font.Bold = true;
                    ws.Cells["K5"].Style.Font.Bold = true;
                    ws.Cells["L5"].Style.Font.Bold = true;
                    ws.Cells["M5"].Style.Font.Bold = true;
                    ws.Cells["N5"].Style.Font.Bold = true;
                    //ws.Cells["O5"].Style.Font.Bold = true;

                    //ws.Cells["P5"].Style.Font.Bold = true;
                    //ws.Cells["Q5"].Style.Font.Bold = true;

                    ws.SelectedRange["A5:N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.SelectedRange["A5:N5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                    using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 14])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.AutoFitColumns();

                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                    using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 1 + dataToExport.Columns.Count])
                    {
                        col.AutoFitColumns(30);
                    }
                    using (ExcelRange col = ws.Cells[5, 13, 5 + dataToExport.Rows.Count, 13])
                    {
                        col[5, 13, 5 + dataToExport.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                    }
                   
                    ws.Cells["A5"].LoadFromDataTable(dataToExport, true);
                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEventCannedReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEventCannedReport.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventCannedReport_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int branchID = -1;
                int customerid = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                else
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var assignmentList = GetComplianceAssigned(customerid, branchID).ToList();

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdEventCannedReport.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventCannedReport.Columns.IndexOf(field);
                    }
                }
                grdEventCannedReport.DataSource = assignmentList;
                grdEventCannedReport.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventCannedReport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tbxFilterLocation.Text = "< Select >";
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation.SelectedNode.Value = "-1";
                if (AuthenticationHelper.Role != "IMPT")
                {
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            dlFilters_SelectedIndexChanged(null, null);
        }

        public DataTable GetGrid()
        {
            try
            {
                dlFilters_SelectedIndexChanged(null, null);
                return (grdEventCannedReport.DataSource as List<Sp_LabelReport_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;

        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int branchID = -1;
                int customerid = -1;
                if (tvFilterLocation.SelectedNode.Value != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                else
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                grdEventCannedReport.DataSource = GetComplianceAssigned(customerid, branchID).ToList();
                grdEventCannedReport.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<Sp_LabelReport_Result> GetComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_LabelReport(CustomerID, "S").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.CustomerBranchID == branch).ToList();
                }
                return result;
            }
        }
        protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
            dlFilters_SelectedIndexChanged(null, null);
        }
    }
}