﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

 
using System.IO;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;
using System.Reflection;
using System.Windows.Media;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using System.Windows;
using System.Text;
using Telerik.Web.UI;
using System.Data;
using Telerik.Windows.Documents.Flow.Model.Shapes;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CertificatecomplianceNew : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static string Path;
        protected static int RoleID;
        protected static string roles;
        protected static int RoleFlag;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Authorization;
        protected static string riskid;
        protected static string riskid1;
        protected static string riskid2;
        protected static string riskid3;
        protected static string StatusID;
        protected static string StatusID1;
        protected static string StatusID2;
        protected static string StatusID3;
        protected static string StatusID4;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!IsPostBack)
            {
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

                if (AuthenticationHelper.Role == "EXCT")
                {
                    roles = "PRA";
                }
                else
                {
                    roles = Convert.ToString(AuthenticationHelper.Role);
                }

                int catid = -1;
                int deptid = -1;
                int branchid = -1;
                //catid = Convert.ToInt32(dropdownlistComplianceCategory.SelectedValue);
                //deptid = Convert.ToInt32(ddlFilterDepartment.SelectedValue);
                //branchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            
        }
        protected void RadDisplayControl_ExportContent(object sender, EditorExportingArgs e)
        {
            try
            {


                //ExportType exportType = e.ExportType;

                //if (exportType == ExportType.Word)
                //{
                //    string exportedOutput = e.ExportOutput;

                //    Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                //    DocxFormatProvider docxProvider = new DocxFormatProvider();
                //    RadFlowDocument document = docxProvider.Import(output);
                //    document.Sections.AddSection();
                //    //Header defaultHeader = document.Sections.First().Headers.Add();
                //    //Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                //    //defaultHeader.Document.Sections.First().HeaderTopMargin = 20;
                //    // Create an image 
                //    FloatingImage floatingImage = new FloatingImage(document);
                //    string pathToImage = "~/Images/ca.png";
                //    // Assign a source to the image 
                //    floatingImage.Image.ImageSource =
                //        new Telerik.Windows.Documents.Media.ImageSource(File.ReadAllBytes(Server.MapPath(pathToImage)), "png");
                //    floatingImage.Image.Width = 150;
                //    floatingImage.Image.Height = 100;
                //    floatingImage.HorizontalPosition.Alignment = 0;

                //    Header defaultHeader = document.Sections.First().Headers.Add();
                //    Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                //    defaultHeader.Document.Sections.First().HeaderTopMargin = 10;
                //    // Insert the image at the desired position in a Paragraph                 
                //    defaultHeaderParagraph.Inlines.Insert(0, floatingImage);

                //    defaultHeaderParagraph.TextAlignment = Alignment.Left;
                //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SeriviceProvideName"])))
                //    {
                //        defaultHeaderParagraph.Inlines.AddRun(Convert.ToString(ViewState["SeriviceProvideName"]));
                //    }

                //    //editor.InsertLine("");
                //    Paragraph defaultHeaderParagraph2 = defaultHeader.Blocks.AddParagraph();
                //    defaultHeaderParagraph2.Inlines.AddRun("Chartered Accountants");
                //    //editor.InsertLine("");

                //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SeriviceProviderAddress"])))
                //    {
                //        Paragraph defaultHeaderParagraph3 = defaultHeader.Blocks.AddParagraph();
                //        //defaultHeaderParagraph3.Inlines.AddRun("Office No 102 / 103, Beena Apartments, Behind Chrysallis Institute, S. B.Road,Pune – 411016");
                //        defaultHeaderParagraph3.Inlines.AddRun(Convert.ToString(ViewState["SeriviceProviderAddress"]));
                //    }

                //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SeriviceProviderPANNo"])))
                //    {
                //        //editor.InsertLine("");
                //        Paragraph defaultHeaderParagraph4 = defaultHeader.Blocks.AddParagraph();
                //        //defaultHeaderParagraph4.Inlines.AddRun("Pan No. AAIFR3176Q");
                //        defaultHeaderParagraph4.Inlines.AddRun("Pan No." + Convert.ToString(ViewState["SeriviceProviderPANNo"]));
                //    }
                //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SeriviceProviderGSTN"])))
                //    {
                //        //editor.InsertLine("");              
                //        Paragraph defaultHeaderParagraph5 = defaultHeader.Blocks.AddParagraph();
                //        //defaultHeaderParagraph5.Inlines.AddRun("GSTN. 27AAIFR3176Q1ZF");
                //        defaultHeaderParagraph5.Inlines.AddRun("GSTN." + Convert.ToString(ViewState["SeriviceProviderGSTN"]));
                //    }

                //    Footer defaultFooter = document.Sections.First().Footers.Add();
                //    Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                //    defaultFooterParagraph.TextAlignment = Alignment.Right;
                //    // defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                //    Byte[] modifiedOutput = docxProvider.Export(document);
                //    string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                //    e.ExportOutput = finalOutput;
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btn_Click(object sender, EventArgs e)
        {
            try
            {
                string val = datastring.Text.ToString();
                RadDisplayControl.Content = datastring.Text.ToString();

                RadDisplayControl.TrackChangesSettings.Author = "Performer";
                RadDisplayControl.TrackChangesSettings.CanAcceptTrackChanges = true;
                RadDisplayControl.TrackChangesSettings.UserCssId = "reU9";
                RadDisplayControl.TrackChangesSettings.CanAcceptTrackChanges = false;
                RadDisplayControl.EnableTrackChanges = false;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}