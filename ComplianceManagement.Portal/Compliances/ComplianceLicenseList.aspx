﻿<%@ Page Title="License(s) List :: My Workspace " Language="C#" MasterPageFile="~/NewCompliance.Master"
    AutoEventWireup="true" CodeBehind="ComplianceLicenseList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceLicenseList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $($('.btndiv').parent('td')).css('padding', "8px 0px 8px 4px");
            setactivemenu('leftworkspacemenu');
            fhead('License');
        });

        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });

        $(window).bind("load", function () {
            $('#updateProgress').hide();
        });

        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });
        function closeIframe() {           
            $('#divShowDialog').modal('hide');            
        }
        function closeModal() {            
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }
        function ShowDialogPerformer(CID, SOID, status) {
            var modalHeight = screen.height - 150;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/compliances/perCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" +status);
        };
        function ShowDialogInternalPerformer(CID, SOID, status) {
            var modalHeight = screen.height - 150;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/compliances/PerInternalCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" + status);
        };
        function ShowLicenseDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 250;

            if (modalHeight < 0)
                modalHeight = 300;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/LicenseDetailsPageView.aspx?AccessID=" + LicenseInstanceID);
        };

        function ShowInternalLicenseDialog(LicenseInstanceID) {
            var modalHeight = screen.height - 250;

            if (modalHeight < 0)
                modalHeight = 300;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/LicenseManagement/aspxPages/InternalLicenseDetailsPage.aspx?AccessID=" + LicenseInstanceID);
        };

        
        function ShowDialogReviewer(CID, SOID, status) {
            var modalHeight = screen.height - 150;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/compliances/revCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" + status);
        };
        function ShowDialogInternalReviewer(CID, SOID, status) {
            var modalHeight = screen.height - 150;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/compliances/revInternalCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" + status);
        };
        

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });


    </script>
    <script type="text/javascript">
        function setTabActive(id) {
            if (id == "ContentPlaceHolder1_liReviewer") {
                $('#ContentPlaceHolder1_liPerformer').removeClass("active");
            }
            else {
                $('#ContentPlaceHolder1_liReviewer').removeClass("active");
            }
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

    </script>
    <style type="text/css">
        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }
    </style>
    <style type="text/css">
        .form-control {
            height: 33px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-md-12 colpadding0 text-right">
                
            <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                   <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom col-md-6 plr0">
                                    License Detail(s)</label>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                            </div>

                             <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 550px; overflow-y: hidden;">
                                <iframe id="showdetails" src="about:blank" width="100%" height="550px" frameborder="0" style="margin-top:-18px;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="col-lg-12 col-md-12">
                <section class="panel">
                    <%--Header Section--%>
                    <header class="panel-heading tab-bg-primary ">
                        <ul id="rblRole1" class="nav nav-tabs">
                            <%if (roles != null)
                                {
                            %>
                            <% if (roles.Contains(3))%>
                            <%{%>
                            <li class="dummyval">
                                <asp:LinkButton ID="liPerformer" OnClick="btnPerformer_Click" runat="server">Performer</asp:LinkButton>
                            </li>
                            <%}%>
                            <% if (roles.Contains(4))%>
                            <%{%>
                            <li class="dummyval">
                                <asp:LinkButton ID="liReviewer" OnClick="btnReviewer_Click" runat="server">Reviewer</asp:LinkButton>
                            </li>
                            <%}%>
                            <%}%>                            
                            <div class="col-md-6 dummyval"  style="margin-left: 81.5%; margin-top:-3.4%; font-size:15px;">                                                              
                                <button class="form-control m-bot15" type="button" style="margin-top: 0px; position: absolute; width: 170px;"  data-toggle="dropdown">More Links
                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                <ul class="dropdown-menu" style="top:0px !important;width:171px !important;margin: 33px 15px 0 !important; min-width:170px !important; ">                                                   
                                    <li>  
                                        <a id="lnkcompliancelink" runat="server"  href="../Controls/frmUpcomingCompliancess.aspx">Compliance Workspace</a>
                                    </li>                                                               
                                </ul>                                                                
                            </div>
                        </ul>
                    </header>
                    <div class="clearfix"></div>

                    <div class="panel-body">

                        <div class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="LicenseListPageValidationGroup" />
                                    <asp:CustomValidator ID="cvErrorLicenseListPage" runat="server" EnableClientScript="False"
                                        ValidationGroup="LicenseListPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div class="row">


                                    <div class="col-md-12 colpadding0">                                          
                                       <div class="col-md-1 colpadding0 entrycount" style="width:12%;">                                    
                                            <div class="col-md-3 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize"
                                                 AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                                 class="form-control m-bot15" Style="width: 64px; float: left;margin-left:10px;">
                                                <asp:ListItem Text="5"/>
                                                <asp:ListItem Text="10" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>

                                          
                                       <div class="col-md-1 form-group" style="width:12%; padding-left: 0px;">
                                                     <asp:DropDownList runat="server" ID="ddlComplianceType"  class="form-control m-bot15" 
                                                         style="width:105px;" 
                                                         OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">                
                                                        <asp:ListItem Text="Statutory" Value="S" />
                                                        <asp:ListItem Text="Internal" Value="I" />                                                        
                                                    </asp:DropDownList>         
                                                    </div>

                                        <div class="col-md-3 pl0" style="width:30%; padding-left: 0px;">                                            
                                            <asp:DropDownListChosen runat="server" ID="ddlLicenseType"
                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged" AutoPostBack="true"
                                                class="form-control" Width="100%" />
                                        </div>
                                        <div class="col-md-2 pl0 form-group" style="width:15%; padding-left: 0px;">                                           
                                            <asp:DropDownListChosen runat="server" ID="ddlLicenseStatus"
                                                AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                OnSelectedIndexChanged="ddlLicenseStatus_SelectedIndexChanged" AutoPostBack="true"
                                                 class="form-control" Width="100%">
                                               <%-- class="form-control" Width="100%">
                                                <asp:ListItem Text="Status" Value="Status" />
                                                <asp:ListItem Text="Active" Value="Active" />
                                                <asp:ListItem Text="Expired" Value="Expired" />
                                                <asp:ListItem Text="Expiring" Value="Expiring" />
                                                <asp:ListItem Text="Applied" Value="Applied" />--%>
                                            </asp:DropDownListChosen>
                                        </div>
                                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                            <ContentTemplate>
                                                <div class="col-md-4 pl0" style="width:30%; padding-left: 0px;">                                                    
                                                    <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Click to Select" autocomplete="off" CssClass="form-control" Width="100%" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 100%" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                                            Style="margin-top: -15px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                                            ShowLines="true"
                                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                        </asp:TreeView>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                                                                
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-12 colpadding0">
                                        <asp:GridView runat="server" ID="grdLicenseList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="LicenseID"
                                            OnRowCommand="grdLicenseList_RowCommand" OnSorting="grdLicenseList_Sorting" OnRowCreated="grdLicenseList_RowCreated">
                                            <Columns>                                               
                                                 <asp:TemplateField HeaderText="Location" ItemStyle-Width="15%" SortExpression="CustomerBrach">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBrach") %>' ToolTip='<%# Eval("CustomerBrach") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="License Type" ItemStyle-Width="20%" SortExpression="LicensetypeName">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicensetypeName") %>' ToolTip='<%# Eval("LicensetypeName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="License No." ItemStyle-Width="20%" SortExpression="LicenseNo">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseNo") %>' ToolTip='<%# Eval("LicenseNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Title" ItemStyle-Width="15%" SortExpression="LicenseTitle">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LicenseTitle") %>' ToolTip='<%# Eval("LicenseTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="App Due Date" ItemStyle-Width="15%"  SortExpression="ApplicationDate">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MMM-yyyy") : "" %>'
                                                                ToolTip='<%# Eval("ApplicationDate") != DBNull.Value ? Convert.ToDateTime(Eval("ApplicationDate")).ToString("dd-MMM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               

                                                <asp:TemplateField HeaderText="End Date" ItemStyle-Width="15%"  SortExpression="EndDate">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : "" %>'
                                                                ToolTip='<%# Eval("EndDate") != DBNull.Value ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : "" %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>      
                                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="15%"  SortExpression="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                          
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center"  >
                                                    <ItemTemplate >
                                                        <div class="btndiv">
                                                        <asp:LinkButton ID="lnkEditLicense" runat="server" OnClick="lnkEditLicense_Click"
                                                             CommandArgument='<%# Eval("LicenseID")+","+Eval("ComplianceInstanceID")+","+Eval("ComplianceScheduleOnID")+","+Eval("Status") %>'
                                                            Visible='<%# CanChangeStatus((long)Eval("LicenseID"),(long)Eval("ComplianceInstanceID"),(long)Eval("ComplianceScheduleOnID"),(string)Eval("Status")) %>'
                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Edit">                                   
                                                            <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/>
                                                        </asp:LinkButton>                                                      
                                                            </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-10 colpadding0">
                                            <div runat="server" id="DivRecordsScrum" style="color: #999">
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                 <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right colpadding0">
                                            
                                        </div>
                                        <div class="col-md-1 text-right colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div>
                                         <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                                        </asp:LinkButton>
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
