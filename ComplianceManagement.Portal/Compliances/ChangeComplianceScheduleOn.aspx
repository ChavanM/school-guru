﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ChangeComplianceScheduleOn.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ChangeComplianceScheduleOn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlCompliance.ClientID %>").combobox();
            $("#<%= ddlPeriod.ClientID %>").combobox();

        }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <center>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                        ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                </div>
                <table runat="server" width="70%">
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Act Name</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlAct" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                              <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act"
                                    ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Compliance
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCompliance" AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                               <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Compliance"
                                    ControlToValidate="ddlCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Period
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlPeriod" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                              <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Period"
                                    ControlToValidate="ddlPeriod" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>

                    <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                             <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                           <asp:TextBox runat="server" CssClass="StartDate" ID="txtSchedueleDate" Style="height: 16px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter Scheduele Date."
                            ControlToValidate="txtSchedueleDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                   <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
	                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
	                            Send Notification</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                        <asp:CheckBox runat="server" ID="chkNotification" CssClass="txtbox" OnCheckedChanged="chkNotification_CheckedChanged" AutoPostBack="true" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>

                       <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
	                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
	                             Applicable for selected Customer</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                        <asp:CheckBox runat="server" ID="chkApplicable" CssClass="txtbox" AutoPostBack="true" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                     <tr  id="divNotification" runat="server" visible="false">
                        <td class="td1" align="left" style="margin-bottom: 4px">
                           <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
	                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
	                        Notification Remark</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                             <asp:TextBox runat="server" ID="txtNotificationRemark" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                  
                    <tr>
                        <td colspan="4" align="center" runat="server">
                            <br />
                            <br />
                            <asp:Panel ID="Panel1" Width="100%" Height="400px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="1000"
                                    Font-Size="12px">                                  
                                    <Columns>
                                            <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComplianceInstenaceID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        
                                         <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chkselection" runat="server" onclick="unCheckSelectAll(this)" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Branch Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduleOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduleOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>

                                                </div>
                                            </ItemTemplate>
                                          
                                        </asp:TemplateField>
                                      
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                      <tr>
                        <td class="td1" align="left" style="margin-bottom: 4px">
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                         <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        </td>
                        <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        var gridViewId = '#<%= grdCompliances.ClientID %>';
        function checkAll(selectAllCheckbox) {
            //get all checkbox and select it
            $('td :checkbox', gridViewId).prop("checked", selectAllCheckbox.checked);
        }
        function unCheckSelectAll(selectCheckbox) {
            //if any item is unchecked, uncheck header checkbox as also
            if (!selectCheckbox.checked)
                $('th :checkbox', gridViewId).prop("checked", false);
        }

        function validateCheckBoxes() {
            debugger;
            var isValid = false;
            var gridView = document.getElementById('<%= grdCompliances.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            //alert("Please select atleast one checkbox");
            document.getElementById("lblcompliance").innerHTML = "Please select atleast one compliance";
            return false;
        }
    </script>
</asp:Content>
