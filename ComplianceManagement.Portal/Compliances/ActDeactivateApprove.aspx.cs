﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActDeactivateApprove : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.UserID == 8877 || AuthenticationHelper.UserID == 9339)
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";

                    BindCategories();
                    BindTypes();
                    BindCompliancesNew();
                   
                }
                else
                {
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
            }
        }

        public string ActActiveOrInActive(int ActID)
        {
            try
            {
                string result = "Active";
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Act_Deactivation
                            where row.ID == ActID
                            select row).FirstOrDefault();
                if (data.Status == "D" || data.Status == null)
                {
                    result = "Active";
                }
                else
                {
                    result = "DeActive";
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "DeActive";
        }

       protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("STATUS"))
                {
                    int actID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetAct(actID);

                    ViewState["Mode"] = 1;
                    ViewState["ActID"] = actID;
                    BindStatusActs();
                    ddlStatusAct.SelectedValue = compliance.Old_Act_ID.ToString();
                    tbxCategoryName.Text = compliance.CategoryName;
                    txtComplianceType.Text = compliance.ComplianceTypeName;
                    txtDeactivateDesc.Text = compliance.DeactivationDescription;

                    string deactivateOn = "";
                    if (compliance.DeactivationDate != null)
                    {
                        deactivateOn = Convert.ToDateTime(compliance.DeactivationDate).ToString("dd-MM-yyyy");
                    }

                    txtDeactivateDate.Text = deactivateOn;
                    
                    upComplianceStatusDetails.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceStatusDialog\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliancesNew();
        }

        protected void upComplianceDetailsStatus_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
    
        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       private void BindCategories()
        {
            try
            {
                ddlComplinceCatagory.DataTextField = "Name";
                ddlComplinceCatagory.DataValueField = "ID";

                ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplinceCatagory.DataBind();

                ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindCompliancesNew()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.Act_Deactivation
                                        join row1 in entities.Acts on row.Old_Act_ID equals row1.ID
                                        join row2 in entities.ComplianceCategories on row1.ComplianceCategoryId equals row2.ID
                                        join row3 in entities.ComplianceTypes on row1.ComplianceTypeId equals row3.ID
                                        where row1.IsDeleted == false && row.Status == "D"
                                        select new
                                        {
                                            row.ID
                                                ,
                                            row.Old_Act_ID
                                                ,
                                            ActName = row1.Name
                                                ,
                                            CategoryName = row2.Name
                                                ,
                                            TypeName = row3.Name
                                                ,
                                            row1.ComplianceCategoryId
                                                ,
                                            row1.ComplianceTypeId
                                            
                                        });

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()));
                }

                if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                {
                    int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == a);
                }
                if (Convert.ToInt32(ddlComplinceCatagory.SelectedValue) != -1)
                {
                    int b = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == b);
                }
                
                var compliances = compliancesQuery.ToList();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {

                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderBy(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CategoryName")
                    {
                        compliances = compliances.OrderBy(entry => entry.CategoryName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "TypeName")
                    {
                        compliances = compliances.OrderBy(entry => entry.TypeName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CategoryName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.CategoryName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "TypeName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.TypeName).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdCompliances.DataSource = compliances;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
        }

 
        private void BindStatusActs()
        {
            try
            {
                ddlStatusAct.DataTextField = "Name";
                ddlStatusAct.DataValueField = "ID";

                ddlStatusAct.DataSource = ActManagement.GetAllNVP();
                ddlStatusAct.DataBind();

                ddlStatusAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocation(int eventID)
        {
            try
            {
                var subEvents = EventManagement.GetAllHierarchy(eventID);
                if (subEvents[0].Children.Count > 0)
                {
                   foreach (var item in subEvents)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
   
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }

        protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                int ActID = Convert.ToInt32(ViewState["ActID"]);
                DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                var ComplianceList = DeactivationAct.GetCompliance(ActID);

                foreach (var item in ComplianceList)
                {
                    Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(item.ID), Date, "A", txtDeactivateDesc.Text);
                    Business.ComplianceManagement.UpdateComplianceDate(Convert.ToInt32(item.ID));
                    Business.ComplianceManagement.RemoveUpcomingSchudule(Convert.ToInt32(item.ID), Date);
                }

                Business.DeactivationAct.ChangeActStatus(Convert.ToInt32(ActID), Date, "A", txtDeactivateDesc.Text);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Business.DeactivationAct.RejectActStatus(Convert.ToInt32(ViewState["ActID"]));

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}