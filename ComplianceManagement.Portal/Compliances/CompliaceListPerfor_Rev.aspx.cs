﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CompliaceListPerfor_Rev : System.Web.UI.Page
    {
        protected static int CustomerID = 0;
        protected static string queryStringFlag = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindGrid();
                BindTreeView(tvFilterLocation);
                
                Panel2.Visible = false;
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindGrid();
        }
        
        private void BindTreeView(TreeView tvLocation)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                }
                tvLocation.ExpandAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            bindGrid();
        }
        
        protected void grdComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdComplianceType.PageIndex = e.NewPageIndex;
            bindGrid();
        }

        protected void grdComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        public void bindGrid()
        {
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                queryStringFlag = "C";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Event Based")
            {
                queryStringFlag = "E";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                queryStringFlag = "I";
            }
            else if (ddlComplianceType.SelectedItem.Text == "CheckList")
            {
                queryStringFlag = "CH";
            }


            //int Location = -1;
            //if (tvFilterLocation.SelectedValue != "-1" || tvFilterLocation.SelectedValue != "")
            //{

            //    Location = Convert.ToInt32(tvFilterLocation.SelectedValue);
            //}

            int Location = -1;
            if (tvFilterLocation.SelectedValue != "-1")
            {
                if (tvFilterLocation.SelectedValue != "")
                {
                    Location = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
            }

            CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
            if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                var GridData = Business.ComplianceManagement.GetAllComwisePerformerandRev(CustomerID, queryStringFlag, Location);
                grdComplianceType.DataSource = null;
                grdComplianceType.DataSource = GridData;
                grdComplianceType.DataBind();
                Session["grdComplienceTypeData"] = (grdComplianceType.DataSource as List<CompliceWisePerAndRevList>).ToDataTable();
                // tbxFilterLocation.Text = "< Select >";
                // ForceCloseFilterBranchesTreeView();
            }
            else
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
                var GridData = Business.ComplianceManagement.GetAllComwisePerformerandRevInternal(CustomerID,Location);
                grdComplianceTypeInternal.DataSource = null;
                grdComplianceTypeInternal.DataSource = GridData;
                grdComplianceTypeInternal.DataBind();
                ForceCloseFilterBranchesTreeView();
                Session["grdComplienceTypeData"] = (grdComplianceType.DataSource as List<CompliceWisePerAndRevList>).ToDataTable();
            }



        }

        protected void grdComplianceTypeInternal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdComplianceTypeInternal.PageIndex = e.NewPageIndex;
            bindGrid();
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void btnExporttoExcel_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    String FileName = String.Empty;
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Compliance Report");
                    DataTable ExcelData = null;
                    //var tet = grdComplianceType.DataSource();
                    bindGrid();
                    DataView view = new System.Data.DataView((DataTable) Session["grdComplienceTypeData"]);
                    // DataView view = new System.Data.DataView((grdComplianceType.DataSource as List<CompliceWisePerAndRevList>).ToDataTable());
                    ExcelData = view.ToTable("Selected", false, "ComplianceID", "ShortDescription", "Branch", "StartDate", "Performer", "Reviewer");

                    var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                    FileName = "Compliance Assignment Report";

                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer.Name;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A3"].Value = FileName;
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "ID";
                    exWorkSheet.Cells["A5"].AutoFitColumns(15);

                    //exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    //exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    //exWorkSheet.Cells["B5"].Value = "Compliance Instance ID";
                    //exWorkSheet.Cells["B5"].AutoFitColumns(25);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Description";
                    exWorkSheet.Cells["B5"].AutoFitColumns(25);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Location";
                    exWorkSheet.Cells["C5"].AutoFitColumns(25);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Start Date";
                    exWorkSheet.Cells["D5"].AutoFitColumns(50);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Performer";
                    exWorkSheet.Cells["E5"].AutoFitColumns(40);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Reviewer";
                    exWorkSheet.Cells["F5"].AutoFitColumns(40);

                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 6])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                       // col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col[5, 4, 5 + ExcelData.Rows.Count, 10].Style.Numberformat.Format = "dd/MMM/yyyy";
                        
                    }
                    


                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ComplianceReport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
    }
}