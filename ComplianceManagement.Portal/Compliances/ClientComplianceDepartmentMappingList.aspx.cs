﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ClientComplianceDepartmentMappingList : System.Web.UI.Page
    {

        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ComplianceID";
                BindCustomers();
                BindCustomersList();
                BindMapping();
                BindDepartment();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;
                txtDepartment.Attributes.Add("readonly", "readonly");
                txtDepartment.Text = "< Select >";
            }
        }

        private void BindDepartment()
        {
            try
            {
                if (ddlCustomer.SelectedValue != "" && ddlCustomer.SelectedValue != null)
                {
                    int customerID = -1;
                    //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    rptDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    rptDepartment.DataBind();

                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");

                        if (!chkDepartment.Checked)
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                    DepartmentSelectAll.Checked = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = Business.CustomerManagement.GetAll(null);
                ddlCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomersList()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                ddlCustomerList.DataSource = Business.CustomerManagement.GetAll(null);
                ddlCustomerList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            ddlType.SelectedValue = "0";
            txtComplianceIDList.Text = string.Empty;
            Add();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
        }
        public void Add()
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlCustomer.Enabled = true;
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region user Detail

        protected void grdMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //int customerid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[0]);
                //int productid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[1]);
                //if (e.CommandName.Equals("EDIT_USER"))
                //{
                //    EditUserInformation(customerid, productid);
                //}
                //else if (e.CommandName.Equals("DELETE_USER"))
                //{
                //    CommanClass.Delete(customerid, productid);
                //    CommanClass.ProductMappingRiskDelete(customerid, productid);
                //    BindMapping();
                //}                                                     
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindMapping()
        {
            try
            {
                int customerID = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = Business.DepartmentHeadManagement.GetAllCliantComplianceDepartmentMapping(customerID,ddlcompType.SelectedItem.Text);
                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ComplianceID")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.ComplianceID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.CustomerName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "DepartmentName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.DepartmentName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ComplianceID")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.ComplianceID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.CustomerName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "DepartmentName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.DepartmentName).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdMapping.DataSource = null;
                grdMapping.DataBind();
                grdMapping.DataSource = productmappinglist;
                grdMapping.DataBind();
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdMapping.PageIndex = e.NewPageIndex;
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = Business.ComplianceManagement.GetAllCliantMappingCompliance(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    productmappinglist = productmappinglist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    productmappinglist = productmappinglist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdMapping.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdMapping.Columns.IndexOf(field);
                    }
                }
                grdMapping.DataSource = null;
                grdMapping.DataBind();
                grdMapping.DataSource = productmappinglist;
                grdMapping.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #endregion
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdMapping.PageIndex = 0;
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(1);", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool checkFlag = false;
                List<DeptCompliance> CCMList = new List<DeptCompliance>();
                List<DeptInternalCompliance> CCMListInternal = new List<DeptInternalCompliance>();
                List<string> ComplianceTextboxList = new List<string>();
                List<long> ComplianceList = new List<long>();
                List<long> DepartmentID = new List<long>();
                ComplianceTextboxList = txtComplianceIDList.Text.Split(',').ToList();
                ComplianceList = ComplianceTextboxList.Select(long.Parse).ToList();
                #region Department
                foreach (RepeaterItem aItem in rptDepartment.Items)
                {
                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                    if (chkDepartment.Checked)
                    {
                        DepartmentID.Add(Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()));
                    }
                }
                #endregion

                if (ddlType.SelectedValue=="0")
                {
                    if (DepartmentID.Count > 0)
                    {
                        foreach (var deptID in DepartmentID)
                        {
                            foreach (long clist in ComplianceList)
                            {
                                DeptCompliance CCM = new DeptCompliance();
                                CCM.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                                CCM.ComplianceID = Convert.ToInt32(clist);
                                CCM.DepartmentID = Convert.ToInt32(deptID);
                                CCM.CreatedBy = AuthenticationHelper.UserID;
                                CCMList.Add(CCM);
                            }
                        }
                    }
                    else
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Select Department.";
                        return;
                    }
                    checkFlag = Business.DepartmentHeadManagement.CreateDeptCompliance(CCMList);
                }
                else
                {
                    if (DepartmentID.Count > 0)
                    {
                        foreach (var deptID in DepartmentID)
                        {
                            foreach (long clist in ComplianceList)
                            {
                                DeptInternalCompliance CCM = new DeptInternalCompliance();
                                CCM.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                                CCM.InternalComplianceID = Convert.ToInt32(clist);
                                CCM.DepartmentID = Convert.ToInt32(deptID);
                                CCM.CreatedBy = AuthenticationHelper.UserID;
                                CCMListInternal.Add(CCM);
                            }
                        }
                    }
                    else
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Select Department.";
                    }
                    checkFlag = Business.DepartmentHeadManagement.CreateDeptInternalCompliance(CCMListInternal);
                }
                
                if (checkFlag)
                {
                    txtComplianceIDList.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRepeater_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void ddlcompType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}