﻿<%@ Page Title="Canned Reports" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="Completed_NotCompleted_ChecklistReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Completed_NotCompleted_ChecklistReports" %>

<%@ Register Src="~/Controls/CannedReportPerformer.ascx" TagName="CannedReportPerformer"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div style="margin: 10px 20px 10px 10px">
    <div>
        Role :
        <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow"
            OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Performer" Selected="True" />           
        </asp:RadioButtonList>
       </div>
       <div style="width:100px;float:left;margin-left: 335px;margin-top: -30px;">   
         <asp:LinkButton runat="server"  ID="lbtnExportExcel" style="margin-top:15px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
          title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
      </div>
     </div>
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="float: right; margin-right: 10px; margin-top: -35px;">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none;
                        color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
        </div>
        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdComplianceTransactions_RowCreated"
            BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdComplianceTransactions_Sorting"
            Width="100%" Font-Size="12px" DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Location" ItemStyle-Width="20%" SortExpression="Branch">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" SortExpression="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Width="10%" SortExpression="Role"/>
             <%--   <asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="10%" />--%>
                <asp:TemplateField HeaderText="Scheduled On" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ForMonth" HeaderText="For Month" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Status"
                    ItemStyle-Width="15%" />
                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Status"
                    ItemStyle-Width="15%" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <pagersettings position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
