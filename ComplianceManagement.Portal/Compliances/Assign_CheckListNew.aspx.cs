﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Assign_CheckListNew : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (AuthenticationHelper.Role == "IMPT")
                {
                    Bindcustomer(userID);
                }
                else
                {
                    Bindcustomer(userID);
                    BindLocationFilter();
                    BindComplianceCategories();
                    BindTypes();
                    BindDepartment();
                    BindUsers(ddlFilterPerformer);
                    BindUsers(ddlFilterReviewer);
                   // BindUsers(ddlFilterApprover);
                    BindActList();
                    AddFilter();
                    tbxFilterLocation.Text = "< Select >";
                    txtactList.Text = "< Select >";
                    BindApprover();
                }
            }
        }
        private void BindApprover(List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                rptApprover.DataSource = users;
                rptApprover.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDepartment()
        {
            try
            {
                int customerID = -1;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }  
                ddlFilterDepartment.DataTextField = "Name";
                ddlFilterDepartment.DataValueField = "ID";
                ddlFilterDepartment.Items.Clear();
                var deptdetails = CompDeptManagement.FillDepartment(customerID);
                ddlFilterDepartment.DataSource = deptdetails;
                ddlFilterDepartment.DataBind();
                ddlFilterDepartment.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
            BindComplianceCategories();
            BindTypes();
            BindDepartment();
            BindUsers(ddlFilterPerformer);
            BindUsers(ddlFilterReviewer);
            //BindUsers(ddlFilterApprover);
            BindApprover();
            BindActList();
        }
        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
                ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
                if (ViewState["pagefilter"] != null)
                {
                    if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                    {
                        //divFilterUsers.Visible = true;
                        //FilterLocationdiv.Visible = false;
                    }
                    else
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = false;
                        //if (AuthenticationHelper.Role == "EXCT")
                        //{
                        //    btnAddComplianceType.Visible = false;
                        //}

                        //BindComplianceInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeApproverList", string.Format("initializeJQueryUI1('{0}', 'dvapprover');", txtapprover.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideAprroverList", "$(\"#dvapprover\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindLocationFilter()
        {
            try
            {
              
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                tbxFilterLocation.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                //TreeNode node = new TreeNode("< All >", "-1");
                //node.Selected = true;
                //tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
                //tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            //BindComplianceInstances(isBranchChanged: true);
            ConditionsBindComplianceMatrix();
        }

        public void Bindcustomer(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                if (AuthenticationHelper.Role == "IMPT")
                {
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(UserID);
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                }
                else
                {
                    int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomerData(customerID);
                    ddlCustomer.SelectedValue = Convert.ToString(customerID);
                    ddlCustomer.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
              
        
        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            ConditionsBindComplianceMatrix();
        }

        private void ConditionsBindComplianceMatrix()
        {
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N", "N");
            }
            else
            {
                BindComplianceMatrix("Y", "N");
            }
        }

        private void BindComplianceMatrix(string flag, string IFCheckedEvent)
        {
            try
            {
                //  int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int branchID = -1;
                if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                {
                    branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                }
                List<int> actIds = new List<int>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }
                if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
                {

                    if (flag == "N" && IFCheckedEvent == "N")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID, false, branchID, actIds);
                    }
                    else if (flag == "Y" && IFCheckedEvent == "N")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID, false, branchID, actIds, tbxFilter.Text.Trim());
                    }
                    grdComplianceRoleMatrix.DataBind();
                    
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlComplianceType.DataTextField = "Name";
                ddlComplianceType.DataValueField = "ID";

                ddlComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlComplianceType.DataBind();

                ddlComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActList()
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int customerID = -1;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue); 
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
               
                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        //int branchID = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                        int branchID = -1;
                        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        }
                        if (branchID != -1)
                        {
                            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                        }
                    }
                }

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID);                
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                    }
                }
                SaveCheckedValues();
                grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                if (chkEvent.Checked == true)
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "Y");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "Y");
                    }
                }
                else
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "N");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "N");
                    }
                }

                //BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            //BindComplianceMatrix("N", "N");
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            //Filtercompliance.Visible = true;
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix("N", "N");

            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {

            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach(GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                 {
                     countCheckedCheckbox = countCheckedCheckbox + 1;                    
                 }
               
            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
       }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                int customerID = -1;
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
               
                var complianceList = new List<ComplianceAsignmentProperties>();
                SaveCheckedValues();
                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                List<TempAssignmentTableCheckList> Tempassignments = new List<TempAssignmentTableCheckList>();
                int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                grdComplianceRoleMatrix.AllowPaging = true;
                BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.DataBind();
                int DepartmentID = -1;
                if (!string.IsNullOrEmpty(ddlFilterDepartment.SelectedValue))
                {
                    if (ddlFilterDepartment.SelectedValue != "-1")
                    {
                        DepartmentID = Convert.ToInt32(ddlFilterDepartment.SelectedValue);
                    }
                }
                if (complianceList != null)
                {
                    for (int i = 0; i < complianceList.Count; i++)
                    {
                        if (complianceList[i].Performer)
                        {
                            if (ddlFilterPerformer.SelectedValue != null)
                            {
                                TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                TempAssP.ComplianceId = complianceList[i].ComplianceId;
                                TempAssP.CustomerBranchID = branchID;
                                TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                TempAssP.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                if (DepartmentID == -1 || DepartmentID == 0)
                                {
                                    TempAssP.DepartmentID = null;
                                }
                                else
                                {
                                    TempAssP.DepartmentID = DepartmentID;
                                }
                                Tempassignments.Add(TempAssP);
                            }
                            if (ddlFilterReviewer.SelectedValue != null)
                            {
                                bool flag = true;
                               // int CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                var details = Business.ComplianceManagement.GetCustomerID(customerID);
                                if (details != null)
                                {
                                    var Compliancedetails = Business.ComplianceManagement.GetDetailsCustomerID(customerID, Convert.ToInt32(complianceList[i].ComplianceId));
                                    if (Compliancedetails != null)
                                    {
                                        if (!Compliancedetails.IsApproverCompulsory)
                                        {
                                            flag = false;
                                        }
                                    }
                                }
                                if (flag)
                                {
                                    TempAssignmentTableCheckList TempAssR = new TempAssignmentTableCheckList();
                                    TempAssR.ComplianceId = complianceList[i].ComplianceId;
                                    TempAssR.CustomerBranchID = branchID;
                                    TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                    TempAssR.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                                    TempAssR.IsActive = true;
                                    TempAssR.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssR.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssR.DepartmentID = DepartmentID;
                                    }
                                    Tempassignments.Add(TempAssR);
                                }

                            }
                            foreach (RepeaterItem aitem in rptApprover.Items)
                            {
                                var lblapproverid= Convert.ToInt32(((Label)aitem.FindControl("lblapproverid")).Text.Trim());
                                CheckBox chkApprover = (CheckBox)aitem.FindControl("chkApprover");
                                if (chkApprover.Checked)
                                {
                                    TempAssignmentTableCheckList TempAssA = new TempAssignmentTableCheckList();
                                    TempAssA.ComplianceId = complianceList[i].ComplianceId;
                                    TempAssA.CustomerBranchID = branchID;
                                    TempAssA.RoleID = RoleManagement.GetByCode("APPR").ID;
                                    TempAssA.UserID = Convert.ToInt32(lblapproverid);
                                    TempAssA.IsActive = true;
                                    TempAssA.CreatedOn = DateTime.Now;
                                    if (DepartmentID == -1 || DepartmentID == 0)
                                    {
                                        TempAssA.DepartmentID = null;
                                    }
                                    else
                                    {
                                        TempAssA.DepartmentID = DepartmentID;
                                    }
                                    Tempassignments.Add(TempAssA);
                                }


                            }
                            //if (ddlFilterApprover.SelectedValue != null)
                            //{
                                
                            //}
                        }
                    }
                    if (Tempassignments.Count != 0)
                    {
                        Business.ComplianceManagement.AddDetailsTempAssignmentTableCheckList(Tempassignments);
                        //  cvDuplicateEntry.IsValid = false;
                        //   cvDuplicateEntry.ErrorMessage = "Checklist(s) Assigned Successfully.";

                        ScriptManager.RegisterStartupScript(this, GetType(), "Compliance(s) Assigned Successfully.", "Showalert();", true);
                        ClearSelection(); 
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                }

                ConditionsBindComplianceMatrix();
                ViewState["CHECKED_ITEMS"] = null;
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }

        private void ClearSelection()
        {
            tbxFilterLocation.Text = "< Select >";
            ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.SelectedValue = "-1";
            //ddlFilterApprover.SelectedValue = "-1";
            ddlComplianceCatagory.SelectedValue = "-1";
            ddlComplianceType.SelectedValue = "-1";
            ddlFilterDepartment.SelectedValue = "-1";
        }

        private void SaveCheckedValues()
        {
            try
            {

                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;                                   
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");                                                  
                            chkPerformer.Checked = rmdata.Performer;                                                    
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}