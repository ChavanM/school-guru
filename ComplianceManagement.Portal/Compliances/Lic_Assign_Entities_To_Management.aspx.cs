﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Lic_Assign_Entities_To_Management : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected int UserID = -1;
        //protected int customerid = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(AuthenticationHelper.Role=="IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    divcustomer.Visible = true;
                    divaddcustomer.Visible = true;
                    divdelcustomer.Visible = true;
                    BindCustomers(UserID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    BindLicenseType(customerid);
                    BindLocationFilter();
                    BindLocation();
                    BindDelLocation();
                    BindUsers(ddlUsers, customerid);
                    BindFilterUsers(ddlFilterUsers, customerid);
                    BindDelUsers(delddluser, customerid);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    BindLicenseType(customerid);
                    BindLocationFilter();
                    BindLocation();
                    BindDelLocation();
                    BindUsers(ddlUsers, customerid);
                    BindFilterUsers(ddlFilterUsers, customerid);
                    BindDelUsers(delddluser, customerid);
                }
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;                                  
                ForceCloseFilterBranchesTreeView();
                tbxBranch.Attributes.Add("readonly", "readonly");
                TextBox1.Attributes.Add("readonly", "readonly");
                tbxFilterLocation.Text = "< Select >";

            }
        }
        private void BindCustomers(int userid)
        {
            try
            {
                var customerdata = Assigncustomer.GetAllCustomer(userid);
                ddlcustomer.DataTextField = "Name";
                ddlcustomer.DataValueField = "ID";

                ddlcustomer.DataSource = customerdata;
                ddlcustomer.DataBind();

                ddlcustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                ddlFiltercustomer.DataTextField = "Name";
                ddlFiltercustomer.DataValueField = "ID";

                ddlFiltercustomer.DataSource = customerdata;
                ddlFiltercustomer.DataBind();

                ddlFiltercustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                delddlcustomer.DataTextField = "Name";
                delddlcustomer.DataValueField = "ID";

                delddlcustomer.DataSource = customerdata;
                delddlcustomer.DataBind();

                delddlcustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        //sandesh code start
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }


        protected void btnlocationpop_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear1pop_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvBranches.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvBranches.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        //protected void RetrieveNodes(TreeNode node)
        //{

        //    if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
        //    {
        //        if (!locationList.Contains(Convert.ToInt32(node.Value)))
        //            locationList.Add(Convert.ToInt32(node.Value));
        //    }

        //    foreach (TreeNode tn in node.ChildNodes)
        //    {

        //        if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
        //        {
        //            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
        //                locationList.Add(Convert.ToInt32(tn.Value));

        //        }

        //        if (tn.ChildNodes.Count != 0)
        //        {
        //            for (int i = 0; i < tn.ChildNodes.Count; i++)
        //            {
        //                RetrieveNodes(tn.ChildNodes[i]);
        //            }
        //        }
        //    }
        //}
        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerid = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                else
                {
                   
                    if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)) &&  ddlFiltercustomer.SelectedValue != "-1")
                    {
                        customerid = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                    }
                   
                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerid)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerid)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
      

        //sandesh code end
        private void BindLicenseType(int customerID)
        {
            try
            {
                var licensetype= LicenseTypeMasterManagement.GetLicenseTypeCustomerwise(customerID,"S");
                ddlLicenseType.DataTextField = "Name";
                ddlLicenseType.DataValueField = "ID";

                ddlLicenseType.DataSource = licensetype;
                ddlLicenseType.DataBind();

                ddlLicenseType.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlLicenseType.Items.Insert(1, new ListItem("Select All", "All"));

                delddlLicenseType.DataTextField = "Name";
                delddlLicenseType.DataValueField = "ID";

                delddlLicenseType.DataSource = licensetype;
                delddlLicenseType.DataBind();

                delddlLicenseType.Items.Insert(0, new ListItem("< Select >", "-1"));
                delddlLicenseType.Items.Insert(1, new ListItem("Select All", "All"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }   
        private void BindComplianceEntityInstances()
        {
            try
            {
                int branchID = -1;                
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }
                int customerID = -1;
                if (AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
               

                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<SP_LIC_ComplianceAssignmentEntitiesView_Result> masterlist = new List<SP_LIC_ComplianceAssignmentEntitiesView_Result>();
                if (locationList.Count > 0)
                {
                    masterlist = LIC_Assign_Entity_Management.SelectAllEntitiesList(customerID, userID, locationList);
                }
                else
                {
                    masterlist = LIC_Assign_Entity_Management.SelectAllEntities(customerID, userID, branchID);
                }
                grdAssignEntities.DataSource = masterlist;
                grdAssignEntities.DataBind();

                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDelLocation()
        {
            try
            {
                int custid = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    custid = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    custid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    custid = Convert.ToInt32(delddlcustomer.SelectedValue);
                }
                TreeView1.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(custid);
                TreeNode node = new TreeNode("All", "-2");
                node.SelectAction = TreeNodeSelectAction.Select;

                TreeView1.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;

                    BindBranchesHierarchy(node, item);
                    TreeView1.Nodes.Add(node);
                }

                TreeView1.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox1.Text = TreeView1.SelectedNode != null ? TreeView1.SelectedNode.Text : "< select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "&(\"#divBranches1\").hide(\"bind\",null,500,function (){});", true);
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                tvBranches.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }
                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);                
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                 //    tvFilterLocation_SelectedNodeChanged(null, null);
               
                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
        //private void BindUsers(DropDownList ddlUserList, DropDownList ddlFilterUserList,DropDownList delddlUserList,int customerID, List<long> ids = null)
        //{
        //    try
        //    {
        //        int complianceProductType = 0;

        //        if (AuthenticationHelper.Role == "CADMN")
        //        {

        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //            complianceProductType = AuthenticationHelper.ComplianceProductType;
        //            //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
        //        }
        //        else
        //        //{
        //        //    complianceProductType = AuthenticationHelper.ComplianceProductType;
        //        //    if ((!string.IsNullOrEmpty(ddlcustomer.SelectedValue)) && ddlcustomer.SelectedValue != "-1")
        //        //    {
        //        //        customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
        //        //    }
        //        //    if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)) && ddlFiltercustomer.SelectedValue != "-1")
        //        //    {
        //        //        customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
        //        //    }
        //        //    if ((!string.IsNullOrEmpty(delddlcustomer.SelectedValue)) && delddlcustomer.SelectedValue != "-1")
        //        //    {
        //        //        customerID = Convert.ToInt32(delddlcustomer.SelectedValue);
        //        //    }
        //        //}



        //        ddlUserList.DataTextField = "Name";
        //        ddlUserList.DataValueField = "ID";
        //        ddlUserList.Items.Clear();

        //        ddlFilterUserList.DataTextField = "Name";
        //        ddlFilterUserList.DataValueField = "ID";
        //        ddlFilterUserList.Items.Clear();

        //        delddlUserList.DataTextField = "Name";
        //        delddlUserList.DataValueField = "ID";            
        //        delddlUserList.Items.Clear();

        //        var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);
        //        //var filterusers = users;

        //        users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });
        //        ddlUserList.DataSource = users;
        //        ddlUserList.DataBind();

        //        //filterusers.Insert(0, new { ID = -1, Name = ddlFilterUserList == ddlFilterUsers ? "< Select >" : "< All >" });
        //        ddlFilterUserList.DataSource = users;
        //        ddlFilterUserList.DataBind();

        //        delddlUserList.DataSource = users;
        //        delddlUserList.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        private void BindUsers(DropDownList ddlUserList, int customerID, List<long> ids = null)
        {
            try
            {

                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);


                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFilterUsers(DropDownList ddlFilterUserList, int customerID, List<long> ids = null)
        {
            try
            {

                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
              else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                ddlFilterUserList.DataTextField = "Name";
                ddlFilterUserList.DataValueField = "ID";
                ddlFilterUserList.Items.Clear();


                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);

                users.Insert(0, new { ID = -1, Name = ddlFilterUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlFilterUserList.DataSource = users;
                ddlFilterUserList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDelUsers(DropDownList deleteUserList, int customerID, List<long> ids = null)
        {
            try
            {

                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(delddlcustomer.SelectedValue);
                }

                deleteUserList.DataTextField = "Name";
                deleteUserList.DataValueField = "ID";
                deleteUserList.Items.Clear();

                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);

                users.Insert(0, new { ID = -1, Name = deleteUserList == ddlUsers ? "< Select >" : "< All >" });

                deleteUserList.DataSource = users;
                deleteUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
               
                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlUsers.SelectedValue);
                }
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                locationList.Clear();
                for (int i = 0; i < this.tvBranches.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvBranches.Nodes[i]);
                }
                if (locationList.Count > 0)
                {
                    if (ddlLicenseType.SelectedValue.Equals("All"))
                    {
                        var licensetypelist = LicenseTypeMasterManagement.GetLicenseTypeCustomerwise(customerID,"S");
                        List<LIC_EntitiesAssignment> licassignmentEntities = new List<LIC_EntitiesAssignment>();

                        foreach (LIC_StatutoryInternalEntitiesAssignment_Result type in licensetypelist)
                        {
                            for (int i = 0; i < locationList.Count; i++)
                            {
                                int lictypeId = Convert.ToInt32(type.ID);
                                var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(locationList[i]), userID, lictypeId);
                                if (data != null)
                                {
                                }
                                else
                                {
                                    LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                    objEntitiesAssignment.UserID = userID;
                                    objEntitiesAssignment.BranchID = Convert.ToInt32(locationList[i]);
                                    objEntitiesAssignment.LicenseTypeID = lictypeId;
                                    objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                    LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                }
                            }
                        }
                    }
                    else if (!ddlLicenseType.SelectedValue.Equals("All"))
                    {
                        int licensetypeId = Convert.ToInt32(ddlLicenseType.SelectedValue);
                        for (int i = 0; i < locationList.Count; i++)
                        {
                            var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(locationList[i]), userID, licensetypeId);

                            if (data != null)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                            }
                            else
                            {
                                LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = locationList[i];
                                objEntitiesAssignment.LicenseTypeID = licensetypeId;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                            }
                        }
                    }
                }              
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveOLD_Click(object sender, EventArgs e)
        {
            try
            {

                int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                int branchId = Convert.ToInt32(tvBranches.SelectedNode.Value);
                int cuid = -1;
                if ((!string.IsNullOrEmpty(ddlcustomer.SelectedValue)))
                {
                    cuid = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                if (!ddlLicenseType.SelectedValue.Equals("All"))
                {
                    if (branchId == -2)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var customerBranches = (from row in entities.CustomerBranches
                                                    where row.IsDeleted == false && row.CustomerID==AuthenticationHelper.CustomerID
                                                    select row);
                            foreach (var node in customerBranches)
                            {
                                int licensetypeId = Convert.ToInt32(ddlLicenseType.SelectedValue);
                                var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(node.ID), userID, licensetypeId);
                                if (data != null)
                                {
                                }
                                else
                                {
                                    LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                    objEntitiesAssignment.UserID = userID;
                                    objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                                    objEntitiesAssignment.LicenseTypeID = licensetypeId;
                                    objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                    LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        int licensetypeId = Convert.ToInt32(ddlLicenseType.SelectedValue);
                        var data = LIC_Assign_Entity_Management.SelectEntity(branchId, userID, licensetypeId);
                        if (data != null)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                        }
                        else
                        {
                            LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                            objEntitiesAssignment.UserID = userID;
                            objEntitiesAssignment.BranchID = branchId;
                            objEntitiesAssignment.LicenseTypeID = licensetypeId;
                            objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                            LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                        }
                    }
                }
                else
                {
                    if (branchId == -2)
                    {
                        var licensetypelist = LicenseTypeMasterManagement.GetLicenseTypeCustomerwise(cuid,"S");                        
                        List<LIC_EntitiesAssignment> licassignmentEntities = new List<LIC_EntitiesAssignment>();
                        foreach (LIC_StatutoryInternalEntitiesAssignment_Result type in licensetypelist)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var customerBranches = (from row in entities.CustomerBranches
                                                        where row.IsDeleted == false && row.CustomerID == AuthenticationHelper.CustomerID
                                                        select row);
                                foreach (var node in customerBranches)
                                {
                                    int lictypeId = Convert.ToInt32(type.ID);
                                    var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(node.ID), userID, lictypeId);
                                    if (data != null)
                                    {
                                    }
                                    else
                                    {
                                        LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                        objEntitiesAssignment.UserID = userID;
                                        objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                                        objEntitiesAssignment.LicenseTypeID = lictypeId;
                                        objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                        LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                    }
                                }
                            }
                        }                       
                    }
                    else
                    {
                        var licensetypelist = LicenseTypeMasterManagement.GetLicenseTypeCustomerwise(cuid, "S");
                       
                        List<LIC_EntitiesAssignment> assignmentEntities = new List<LIC_EntitiesAssignment>();
                        foreach (LIC_StatutoryInternalEntitiesAssignment_Result type in licensetypelist)
                        {
                            var data = LIC_Assign_Entity_Management.SelectEntity(branchId, userID, (int)type.ID);
                            if (!(data != null))
                            {
                                LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = branchId;
                                objEntitiesAssignment.LicenseTypeID = type.ID;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                assignmentEntities.Add(objEntitiesAssignment);
                            }
                        }
                        LIC_Assign_Entity_Management.Create(assignmentEntities);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                    }
                }               
                BindComplianceEntityInstances();                             
            }
            catch (Exception ex)
            {
               
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.tvBranches.Nodes.Count; i++)
                {
                    ChkBoxClear(this.tvBranches.Nodes[i]);
                }

                lblErrorMassage.Text = "";
                ddlUsers.SelectedValue = "-1";
                ddlLicenseType.SelectedValue = "-1";
                ddlcustomer.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();           
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                //if (tvFilterLocation.SelectedValue != "-1")
                //{
                //    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                //}

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);

                var assignmentList = LIC_Assign_Entity_Management.SelectAllEntities(customerID,userID,branchID);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAssignEntities.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                    }
                }

                grdAssignEntities.DataSource = assignmentList;
                grdAssignEntities.DataBind();
                tbxFilterLocation.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


        #region Delete User
        protected void upCompliance_LoadDelete(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesDelete", string.Format("initializeJQueryUI('{0}', 'divBranches1');", TextBox1.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void delddluser_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void btnDeleteComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (TreeView1.SelectedNode != null)
                {
                    TreeView1.SelectedNode.Selected = false;
                }

                Label3.Text = "";
                delddluser.SelectedValue = "-1";
                delddlLicenseType.SelectedValue = "-1";
                delddlcustomer.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('open');", true);
                TextBox1.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                long Uid = -1;
                long LicenseTypeId = -1;
                long branchid = -1;
                // long branchId = Convert.ToInt32(TreeView1.SelectedNode.Value);
                Uid = Convert.ToInt32(delddluser.SelectedValue);
                if (!TextBox1.Text.Equals("< Select Location >"))
                {
                    if (!delddlLicenseType.SelectedItem.Text.Equals("< Select >"))
                    {
                        if (TextBox1.Text.Equals("All"))
                        {
                            if (delddlLicenseType.SelectedItem.Text.Equals("Select All"))
                            {
                                //Branch and LicenseType All selected
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var data = (from row in entities.LIC_EntitiesAssignment
                                                where row.UserID == Uid
                                                select row).ToList();

                                    foreach (var item in data)
                                    {
                                        entities.LIC_EntitiesAssignment.RemoveRange(entities.LIC_EntitiesAssignment.Where(x => x.ID == item.ID));
                                        entities.SaveChanges();
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('close');", true);

                                    }
                                }
                            }
                            else
                            {
                                LicenseTypeId = Convert.ToInt32(delddlLicenseType.SelectedValue);
                                //Branch all and LicenseType selected
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var data = (from row in entities.LIC_EntitiesAssignment
                                                where row.UserID == Uid
                                                && row.LicenseTypeID == LicenseTypeId
                                                select row).ToList();

                                    foreach (var item in data)
                                    {
                                        entities.LIC_EntitiesAssignment.RemoveRange(entities.LIC_EntitiesAssignment.Where(x => x.ID == item.ID));
                                        entities.SaveChanges();
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('close');", true);

                                    }
                                }
                            }

                        }


                        //Branch Selected LicenseType all
                        if (delddlLicenseType.SelectedItem.Text.Equals("Select All"))
                        {
                            long branchId = Convert.ToInt32(TreeView1.SelectedNode.Value);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var data = (from row in entities.LIC_EntitiesAssignment
                                            where row.UserID == Uid
                                            && row.BranchID == branchId
                                            select row).ToList();

                                foreach (var item in data)
                                {
                                    entities.LIC_EntitiesAssignment.RemoveRange(entities.LIC_EntitiesAssignment.Where(x => x.ID == item.ID));
                                    entities.SaveChanges();
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('close');", true);

                                }
                            }

                        }
                        else
                        {
                            //Branch selected LicenseType selected

                            if (!TextBox1.Text.Equals("All"))
                            {
                                if (TreeView1.SelectedNode.Value != "-1")
                                {
                                    branchid = Convert.ToInt32(TreeView1.SelectedNode.Value);
                                }
                                if (!String.IsNullOrEmpty(delddluser.SelectedValue))
                                {
                                    if (delddluser.SelectedValue != "-1")
                                    {
                                        Uid = Convert.ToInt32(delddluser.SelectedValue);
                                    }
                                }
                                if (!String.IsNullOrEmpty(delddlLicenseType.SelectedValue))
                                {
                                    if (delddlLicenseType.SelectedValue != "-1")
                                    {
                                        LicenseTypeId = Convert.ToInt32(delddlLicenseType.SelectedValue);
                                    }
                                }
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    //  long branchId = Convert.ToInt32(TreeView1.SelectedNode.Value);
                                    var AssignID = (from row in entities.LIC_EntitiesAssignment
                                                    where row.UserID == Uid && row.BranchID == branchid && row.LicenseTypeID == LicenseTypeId
                                                    select row).FirstOrDefault();

                                    entities.LIC_EntitiesAssignment.Remove(AssignID);
                                    entities.SaveChanges();
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('close');", true);

                                }

                            }

                        }

                    }
                }

                BindComplianceEntityInstances();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        #endregion

        protected void ddlFiltercustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if ((!string.IsNullOrEmpty(ddlFiltercustomer.SelectedValue)) && ddlFiltercustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlFiltercustomer.SelectedValue);
                }
                BindLocationFilter();
                BindFilterUsers(ddlFilterUsers, customerID);
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

           
        }

       
        protected void delddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if ((!string.IsNullOrEmpty(delddlcustomer.SelectedValue)) && delddlcustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(delddlcustomer.SelectedValue);
                }
                BindLicenseType(customerID);
                BindDelUsers(delddluser, customerID);

                BindDelLocation();
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
          
        }

        protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if ((!string.IsNullOrEmpty(ddlcustomer.SelectedValue)) && ddlcustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlcustomer.SelectedValue);
                }
                BindLicenseType(customerID);
                BindUsers(ddlUsers, customerID);
                BindLocation();
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            
        }
    }
}