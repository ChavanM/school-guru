﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ActGroup.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ActGroup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActGroupList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
              <tr>
                  <td align="right" style="width: 88%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true" Height="20px"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                   <td class="newlink" align="right" style="width: 10%">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddEvent" OnClick="btnAddClick" Visible="true" />
                    </td>
              </tr>
           </table>
            <%-- <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdActGroup" AutoGenerateColumns="false" GridLines="Vertical" 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" 
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" 
                    Font-Size="12px" OnRowCommand="grdActGroup_RowCommand" OnPageIndexChanging="grdActGroup_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Height="25px" HeaderStyle-Height="20px">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label ID="Label222" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                      
                        <asp:TemplateField ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="Edit_ActGroup"  CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Act group" title="Edit Act group" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="Delete_ActGroup" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Act group name?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete Act group" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
           <%-- </asp:Panel>--%>

        </ContentTemplate>
    </asp:UpdatePanel>

      <div id="divActGroupDialog">
        <asp:UpdatePanel ID="upActGroup" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="ActGroupValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActGroupValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 180px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="txtName" Style="width: 300px;" MaxLength="500" ToolTip="Name" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="txtName"
                            runat="server" ValidationGroup="ActGroupValidationGroup" Display="None" />
                    </div>
                  

                  
                    <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 30px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"  CssClass="button"
                            ValidationGroup="ActGroupValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divActGroupDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divActGroupDialog').dialog({
                height: 350,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Act Group",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
    </script>
</asp:Content>
