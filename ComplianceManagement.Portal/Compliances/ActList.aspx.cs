﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActList : System.Web.UI.Page
    {
        public static List<long?> BranchList = new List<long?>();
        public static List<long?> CompanyTypeList = new List<long?>();
        public static List<long?> BusinessActivityTypeist = new List<long?>();
        public static List<long?> ActApplicability = new List<long?>();

        private int userId = AuthenticationHelper.UserID;
        public static string ActDocReviewPath = "";
        public bool MGM_KEy;
        protected static int customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT"|| AuthenticationHelper.Role=="RREV" || AuthenticationHelper.Role == "RPER" || AuthenticationHelper.Role == "UPDT")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT") {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
           
            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }

                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {
                        BindIndustry();
                        BindCompanyType();
                        BindBusinessActivityType();
                        BindAct_Applicability();
                        BindLocationType();
                        BindActGroup();
                        BindCountry();
                        // BindSubIndustry(BranchList);
                        BindCategories(ddlCategory);
                        BindTypes(ddlType);
                        BindCategories(ddlfilterCatagory);
                        BindTypes(ddlFilterType);
                        BindStates();
                        BindCities();
                        BindActs();
                        BindActDepartment();
                        BindMinistry();
                        BindRegulator();
                        BindActDocType();
                        BranchList.Clear();
                        if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("RREV")))
                        {
                            btnAddAct.Visible = true;
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
               

                              
            }
           // saveopo.Value = "false";
        }
        public static List<ActGroupMaster> GetActgroupdata()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ActGroupMasters
                            where row.IsActive== true
                            select row).ToList();
                return data.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindActGroup()
        {
            try
            {
                ddlActGroup.DataTextField = "Name";
                ddlActGroup.DataValueField = "ID";

                ddlActGroup.DataSource = GetActgroupdata();
                ddlActGroup.DataBind();

                ddlActGroup.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCities()
        {
            try
            {
                ddlCity.DataSource = null;
                ddlCity.DataBind();
                ddlCity.ClearSelection();

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                ddlCity.DataBind();

                ddlCity.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = AddressManagement.GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCountry()
        {
            try
            {
                ddlcountry.DataTextField = "Name";
                ddlcountry.DataValueField = "ID";

                ddlcountry.DataSource = AddressManagement.GetAllCountry();
                ddlcountry.DataBind();

                ddlcountry.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTypes(DropDownList ddlList)
        {
            try
            {
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";

                ddlList.DataSource = ComplianceTypeManagement.GetAll();
                ddlList.DataBind();

                ddlList.Items.Insert(0, new ListItem("< Select Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCategories(DropDownList ddlList)
        {
            try
            {
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";

                ddlList.DataSource = ComplianceCategoryManagement.GetAll();
                ddlList.DataBind();

                ddlList.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActDepartment()
        {
            try
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = ActManagement.GetAllActDepartment();
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("< Select Department>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActDocType()
        {
            try
            {
                ddlActDocType.DataTextField = "Name";
                ddlActDocType.DataValueField = "ID";
                ddlActDocType.DataSource = ActManagement.GetAllActDocumentType();
                ddlActDocType.DataBind();
                ddlActDocType.Items.Insert(0, new ListItem("< Select DocumentType>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        private void BindMinistry()
        {
            try
            {
                ddlMinistry.DataTextField = "Name";
                ddlMinistry.DataValueField = "ID";
                ddlMinistry.DataSource = ActManagement.GetAllMinistry();
                ddlMinistry.DataBind();
                ddlMinistry.Items.Insert(0, new ListItem("< Select Ministry>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindRegulator()
        {
            try
            {
                ddlRegulator.DataTextField = "Name";
                ddlRegulator.DataValueField = "ID";
                ddlRegulator.DataSource = ActManagement.GetAllRegulator();
                ddlRegulator.DataBind();
                ddlRegulator.Items.Insert(0, new ListItem("< Select Regulator>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        private void BindActs()
        {
            try
            {
                grdAct.DataSource = ActManagement.GetAll(Convert.ToInt32(ddlfilterCatagory.SelectedValue), Convert.ToInt32(ddlFilterType.SelectedValue), tbxFilter.Text);
                grdAct.DataBind();
                upActList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_ACT"))
                {
                    divDate.Visible = false;
                    rfvDate.Enabled = false;

                   // divSubIndustry.Visible = true;

                    btnActDocSave.Visible = true;
                    btnSave.Enabled = true;
                    int actID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["ActID"] = actID;
                    BindGridForFile(actID);
                    Act act = ActManagement.GetByID(actID);

                    tbxName.Text = act.Name;
                    tbxName.ToolTip = act.Name;

                    tbxdescription.Text = act.Description;
                    tbxdescription.ToolTip = act.Description;

                    txtshortform.Text = act.ShortForm;
                    txtshortform.ToolTip = act.ShortForm;

                    ddlCategory.SelectedValue = act.ComplianceCategoryId.Value.ToString();
                    ddlType.SelectedValue = act.ComplianceTypeId.Value.ToString();

                    if (act.CountryID.HasValue)
                    {
                        if (act.CountryID == 1)
                        {
                            ddlType.Enabled = true;
                        }
                        else
                        {
                            ddlType.Enabled = false;
                        }
                        ddlcountry.SelectedValue = act.CountryID.Value.ToString();
                    }
                    else
                    {
                        ddlcountry.SelectedValue = "-1";
                    }

                    if (act.duedatetype != null)
                        ddlDueDateType.SelectedValue = act.duedatetype.Value.ToString();
                    
                    if (act.StateID.HasValue)
                    {
                        ddlState.SelectedValue = act.StateID.Value.ToString();
                    }
                    else
                    {
                        ddlState.SelectedValue = "-1";
                    }
                    ddlState_SelectedIndexChanged(null, null);

                    if (act.CityID.HasValue)
                    {
                        ddlCity.SelectedValue = act.CityID.Value.ToString();
                    }

                    if (act.Act_DeptID.HasValue)
                    {
                        ddlDepartment.SelectedValue=Convert.ToString(act.Act_DeptID);
                    }

                    if (act.LocationTypeID.HasValue)
                    {
                        ddlLocationType.SelectedValue = Convert.ToString(act.LocationTypeID);
                    }
                    if (act.MinistryID.HasValue)
                    {
                        ddlMinistry.SelectedValue = Convert.ToString(act.MinistryID);
                    }
                    if (act.RegulatorID.HasValue)
                    {
                        ddlRegulator.SelectedValue = Convert.ToString(act.RegulatorID);
                    }
                    if (act.ActGroupId.HasValue)
                    {
                        ddlActGroup.SelectedValue = act.ActGroupId.Value.ToString();
                    }
                    else
                    {
                        ddlActGroup.SelectedValue = "-1";
                    }
                    if (Convert.ToString(ddlType.SelectedItem.Text).Trim().ToUpper() == "CENTRAL")
                    {
                        ddlState.SelectedValue = "-1";
                        ddlCity.SelectedValue = "-1";
                        divState.Visible = false;
                        divCity.Visible = false;
                    }
                    else
                    {
                        divState.Visible = true;
                        divCity.Visible = true;
                    }

                    if(act.StartDate != null)
                    {
                        tbxStartDate.Text = Convert.ToDateTime(act.StartDate).ToString("dd-MM-yyyy");
                    }

                    #region Industry
                    txtIndustry.Text = "< Select >";
                    var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(actID);
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                                    chkIndustry.Checked = true;
                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                            IndustrySelectAll.Checked = true;
                        else
                           // IndustrySelectAll.Checked = false;
                        txtSubIndustry.Text = "<Select>";
                        foreach (var item in vGetIndustryMappedIDs)
                        {
                            int industryID = Convert.ToInt32(item);
                            BranchList.Add(industryID);
                        }
                        if (BranchList.Count >= 0)
                        {
                            
                            divSubIndustry.Visible = true;
                            BindSubIndustry(BranchList);
                        }

                     }
                    #endregion


                    #region Sub Industry

                    txtSubIndustry.Text = "<Select>";
                    var vGetSubIndustryMappedIDs = ActManagement.GetActSubIndustryMappedID(actID);
                    foreach (RepeaterItem aItem in rptSubIndustry.Items)
                    {
                        CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");
                        chksubIndustry.Checked = false;
                        CheckBox SubIndustrySelectAll = (CheckBox)rptSubIndustry.Controls[0].Controls[0].FindControl("SubIndustrySelectAll");

                        for (int i = 0; i <= vGetSubIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID1")).Text.Trim() == vGetSubIndustryMappedIDs[i].ToString())
                                chksubIndustry.Checked = true;
                        }
                        if ((rptSubIndustry.Items.Count) == (vGetSubIndustryMappedIDs.Count))
                       // if(vGetSubIndustryMappedIDs.Count>0)
                            SubIndustrySelectAll.Checked = true;
                        else
                            SubIndustrySelectAll.Checked = false;
                    }

                    #endregion


                    #region Company Type
                    txtCompanyType.Text = "< Select >";
                    var vGetActCompanyTypeIDs = ActManagement.GetActCompanyTypeMappedID(actID);
                    foreach (RepeaterItem aItem in rptCompanyType.Items)
                    {
                        CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");
                        chkCompanyType.Checked = false;
                        CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");

                        for (int i = 0; i <= vGetActCompanyTypeIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim() == vGetActCompanyTypeIDs[i].ToString())
                                chkCompanyType.Checked = true;
                        }
                        if ((rptCompanyType.Items.Count) == (vGetActCompanyTypeIDs.Count))
                            CompanyTypeSelectAll.Checked = true;                       
                    }
                    #endregion


                    #region Business Activity Type
                    txtBusinessActivityType.Text = "< Select >";
                    var vGetActBusinessActivityIDs = ActManagement.GetActBusinessActivityMappedID(actID);
                    foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                    {
                        CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                        chkBusinessActivityType.Checked = false;
                        CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim() == vGetActBusinessActivityIDs[i].ToString())
                                chkBusinessActivityType.Checked = true;
                        }
                        if ((rptBusinessActivityType.Items.Count) == (vGetActBusinessActivityIDs.Count))
                            BusinessActivityTypeSelectAll.Checked = true;
                    }
                    #endregion


                    #region Business Activity Type
                    txtActApplicability.Text = "< Select >";
                    var vGetActApplicabilityIDs = ActManagement.GetActApplicabilityMappedID(actID);
                    foreach (RepeaterItem aItem in rptActApplicability.Items)
                    {
                        CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");
                        chkActApplicability.Checked = false;
                        CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim() == vGetActApplicabilityIDs[i].ToString())
                                chkActApplicability.Checked = true;
                        }
                        if ((rptActApplicability.Items.Count) == (vGetActApplicabilityIDs.Count))
                            ActApplicabilitySelectAll.Checked = true;
                    }
                    #endregion


                    upAct.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_ACT"))
                {
                    int actID = Convert.ToInt32(e.CommandArgument);
                    bool isComplianceExists = ActManagement.ExistsCompliancesForAct(actID);
                    if (!isComplianceExists)
                    {
                        ActManagement.Delete(actID);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Unable to delete Act, since Compliances are exists for the Act');", true);
                    }

                    BindActs();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindGridForFile(long actID)
        {
            try
            {
                if (actID > 0)
                {
                    var getFileName = ActManagement.getFileNamebyID(Convert.ToInt32(actID));
                    if (getFileName.Count > 0)
                    {                        
                        GrdforFileName.DataSource = getFileName.OrderBy(x => x.FileName);
                        GrdforFileName.DataBind();
                        GrdShowForFile.Visible = true;
                        //DivSave.Style.Add("margin-top", "16%");
                    }
                    else
                    {
                        GrdforFileName.DataSource = null;
                        GrdforFileName.DataBind();
                        GrdShowForFile.Visible = false;
                        //DivSave.Style.Remove("margin-top");
                    }
                }
                else
                {
                    GrdforFileName.DataSource = null;
                    GrdforFileName.DataBind();
                    GrdShowForFile.Visible = false;
                    //DivSave.Style.Remove("margin-top");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAct.PageIndex = e.NewPageIndex;
                BindActs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddAct_Click(object sender, EventArgs e)
        {
            try
            {
               
                divDate.Visible = false;
                rfvDate.Enabled = false;
                btnActDocSave.Visible = false;

                ViewState["Mode"] = 0;
                btnSave.Enabled = true;
                tbxName.Text = string.Empty;
                tbxdescription.Text=string.Empty;
                txtshortform.Text = string.Empty;
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                
                ddlState.SelectedValue = "-1";
                ddlcountry.SelectedValue = "1";
                ddlCity.SelectedValue = "-1";
                divState.Visible = false;
                divCity.Visible = false;
                //GrdforFileName.DataSource = null;
               // GrdforFileName.DataBind();               
                tbxStartDate.Text = string.Empty;
                saveopo.Value = "false";
                Label2.Text = string.Empty;
                ddlDepartment.SelectedValue = "-1";
                ddlLocationType.SelectedValue = "-1";
                ddlMinistry.SelectedValue = "-1";
                ddlRegulator.SelectedValue = "-1";
                ddlActDocType.SelectedValue = "-1";
                //chkFrequency.Checked = false;
                txtIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
              
                 txtSubIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptSubIndustry.Items)
                {
                    CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");
                    chksubIndustry.Checked = false;
                    CheckBox SubIndustrySelectAll = (CheckBox)rptSubIndustry.Controls[0].Controls[0].FindControl("SubIndustrySelectAll");
                    SubIndustrySelectAll.Checked = false;
                }

                txtCompanyType.Text = "< Select >";
                foreach (RepeaterItem aItem in rptCompanyType.Items)
                {
                    CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");
                    chkCompanyType.Checked = false;
                    CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");
                    CompanyTypeSelectAll.Checked = false;
                }


                txtBusinessActivityType.Text = "< Select >";
                foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                {
                    CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                    chkBusinessActivityType.Checked = false;
                    CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");
                    BusinessActivityTypeSelectAll.Checked = false;
                }


                txtActApplicability.Text = "< Select >";
                foreach (RepeaterItem aItem in rptActApplicability.Items)
                {
                    CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");
                    chkActApplicability.Checked = false;
                    CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");
                    ActApplicabilitySelectAll.Checked = false;
                }
                BindGridForFile(-1);

                upAct.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdAct.PageIndex = 0;
                BindActs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static Act_SubIndustryMapping GetByMappedSubIndustryID(int actId, int IndustryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var SubIndustry= (from row in entities.Act_SubIndustryMapping
                                    where row.ActID == actId && row.IndustryID == IndustryID
                                    select row).FirstOrDefault();

                return SubIndustry;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int DepartmentID = -1;
                int Ministry = -1;
                int Regulator = -1;
                int ActDocType = -1;
                int LocationTypeID = -1;
                int ActGID = -1;
                int country = -1;
                if (!string.IsNullOrEmpty(ddlLocationType.SelectedValue))
                {
                    if (ddlLocationType.SelectedValue != "-1")
                    {
                        LocationTypeID = Convert.ToInt32(ddlLocationType.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                {
                    if (ddlDepartment.SelectedValue != "-1")
                    {
                        DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlMinistry.SelectedValue))
                {
                    if (ddlMinistry.SelectedValue != "-1")
                    {
                        Ministry = Convert.ToInt32(ddlMinistry.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlRegulator.SelectedValue))
                {
                    if (ddlRegulator.SelectedValue != "-1")
                    {
                        Regulator = Convert.ToInt32(ddlRegulator.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                {
                    if (ddlActDocType.SelectedValue != "-1")
                    {
                        ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlActGroup.SelectedValue))
                {
                    if (ddlActGroup.SelectedValue != "-1")
                    {
                        ActGID = Convert.ToInt32(ddlActGroup.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlcountry.SelectedValue))
                {
                    if (ddlcountry.SelectedValue != "-1")
                    {
                        country = Convert.ToInt32(ddlcountry.SelectedValue);
                    }
                }

                //ddlActGroup.SelectedValue = act.ActGroupId.Value.ToString();
                Act act = new Act()
                {
                    Name = tbxName.Text,
                    Description = tbxdescription.Text,
                    ShortForm = txtshortform.Text,
                    ComplianceTypeId = Convert.ToInt32(ddlType.SelectedValue),
                    ComplianceCategoryId = Convert.ToInt32(ddlCategory.SelectedValue),
                    CreatedBy = AuthenticationHelper.UserID,
                    ProductCode = "C",
                    Act_DeptID=DepartmentID,
                    RegulatorID=Regulator,
                    MinistryID=Ministry,
                    LocationTypeID= LocationTypeID,
                    duedatetype = Convert.ToInt32(ddlDueDateType.SelectedValue),
                    ActGroupId= ActGID,
                };

                string ComplianceType = ComplianceTypeManagement.GetByID((int)act.ComplianceTypeId).Name;
                if (ComplianceType != "National" && ComplianceType != "Central")
                {
                    act.StateID = ddlState.SelectedValue != "-1" ? Convert.ToInt32(ddlState.SelectedValue) : (int?)null;
                    act.State = ddlState.SelectedValue != "-1" ? ddlState.SelectedItem.Text : null;
                    if (ComplianceType == "State")
                    {
                        act.CityID = ddlCity.SelectedValue != "-1" ? Convert.ToInt32(ddlCity.SelectedValue) : (int?)null;
                        act.City = ddlCity.SelectedValue != "-1" ? ddlCity.SelectedItem.Text : null;
                    }
                }

                if(!string.IsNullOrEmpty(tbxStartDate.Text))
                    act.StartDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                #region check industry
                Boolean chkIndustryFlag = false;
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    if (chkIndustry.Checked)
                    {
                        chkIndustryFlag = true;
                        break;
                    }
                }
                #endregion


                #region check Sub industry
                Boolean chkSubIndustryFlag = false;
                foreach (RepeaterItem aItem in rptSubIndustry.Items)
                {
                    CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");
                    if (chksubIndustry.Checked)
                    {
                        chkSubIndustryFlag = true;
                        break;
                    }
                }
                #endregion
                if (chkSubIndustryFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one Sub industry.";
                    cvDuplicateEntry.IsValid = false;
                    //return;
                }
                if (chkIndustryFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one industry.";
                    cvDuplicateEntry.IsValid = false;
                    //return;
                }
                else
                {
                    if ((int)ViewState["Mode"] == 1)
                    {
                        act.ID = Convert.ToInt32(ViewState["ActID"]);
                    }

                    bool isExists = ActManagement.Exists(act);

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (isExists)
                        {
                            saveopo.Value = "true";
                            cvDuplicateEntry.ErrorMessage = "Act name already exists.";
                            cvDuplicateEntry.IsValid = false;
                        }
                        else
                        {
                            #region Add                 
                            int _objActID = ActManagement.Create(act);

                            if (_objActID > 0)
                            {
                                int newActID;
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    try
                                    {
                                        var ActPresent = entities.CheckActPresent(act.Name);
                                        if (ActPresent.Count() <= 0)
                                        {
                                            ObjectParameter newactid = new ObjectParameter("newActid", typeof(int));
                                            entities.actapproval(act.Name, newactid);
                                            newActID = Convert.ToInt32(newactid.Value);
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                Act_Document act_Document = null;
                                HttpFileCollection fileCollection = Request.Files;
                                if (fileCollection.Count > 0)
                                {
                                    #region file upload
                                    int count = 1;
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = null;
                                        uploadfile = fileCollection[i];
                                        string fileName = uploadfile.FileName;
                                        string directoryPath = null;

                                        if (!string.IsNullOrEmpty(fileName))
                                        {
                                            string version = string.Empty;

                                            version = Convert.ToString(count) + ".0";
                                            directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(_objActID) + "/" + version);
                                            //directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(_objActID));

                                            DocumentManagement.CreateDirectory(directoryPath);
                                            string finalPath = Path.Combine(directoryPath, uploadfile.FileName);
                                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                            fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                         
                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            DateTime? dtVerstion = null;
                                            if (txtActDocVerstionDate.Text != "")
                                            {
                                                dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                                            }

                                            act_Document = new Act_Document()
                                            {
                                                Act_ID = _objActID,
                                                FileName = fileCollection[i].FileName,
                                                //FileData = bytes,
                                                FilePath = finalPath,
                                                IsActive = true,
                                                CreatedOn = DateTime.Now,
                                                Createdby = userId,
                                                UpdatedOn = DateTime.Now,
                                                Updatedby = userId,
                                                Version = version,
                                                VersionDate = DateTime.Now,
                                                Act_TypeID = ActDocType,
                                                Act_TypeVersionDate = dtVerstion,
                                            };
                                            var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                            count++;
                                        }
                                    }
                                    #endregion
                                }

                                #region add Industry                            
                                List<Act_IndustryMapping> mappingInustries = new List<Act_IndustryMapping>();

                                foreach (RepeaterItem aItem in rptIndustry.Items)
                                {
                                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                                    
                                    if (chkIndustry.Checked)
                                    {
                                        Act_IndustryMapping ActIndustryMapping = new Act_IndustryMapping()
                                        {
                                            ActID = act.ID,
                                            IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                            IsActive = true,
                                            EditedDate = DateTime.Now,
                                            EditedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        mappingInustries.Add(ActIndustryMapping);
                                        
                                    }
                                }

                                ActManagement.CreateOrUpdateActIndustryMapping(mappingInustries, act.ID, false);
                                #endregion

                                #region add Sub Industry  
                                List<Act_SubIndustryMapping> mappingSubInustries = new List<Act_SubIndustryMapping>();
                             
                                var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(act.ID);

                                foreach (var Industries in vGetIndustryMappedIDs)
                                {
                                    int industryID = Convert.ToInt32(Industries);
                                   // var subIndustryList = ActManagement.GetSubIndustryMappedID(act.ID, industryID);

                                    foreach (RepeaterItem aItem in rptSubIndustry.Items)
                                    {
                                        CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");

                                        int subIndustryId = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID1")).Text.Trim());
                                        var subIndlist= ActManagement.GetSubIndustryMappedID(industryID,subIndustryId);
                                        // var subIndustryId1 = Convert.ToInt64(subIndlist);
                                        foreach (var item in subIndlist)
                                        {


                                            if (chksubIndustry.Checked)
                                            {
                                                Act_SubIndustryMapping ActSubIndustryMapping = new Act_SubIndustryMapping()
                                                {
                                                    ActID = act.ID,
                                                    IndustryID = Convert.ToInt32(industryID),
                                                    SubIndustryID = Convert.ToInt32(item),
                                                    //SubIndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID1")).Text.Trim()),
                                                    IsActive = true,
                                                    EditedDate = DateTime.Now,
                                                    EditedBy = AuthenticationHelper.UserID,
                                                };
                                                mappingSubInustries.Add(ActSubIndustryMapping);
                                            }
                                        }
                                    }
                                }

                                ActManagement.CreateOrUpdateActSubIndustryMapping(mappingSubInustries, act.ID, false);
                                #endregion


                                #region add CompanyType                            
                                List<Act_CompanyTypeMapping> mappingCompanyTypelist = new List<Act_CompanyTypeMapping>();

                                foreach (RepeaterItem aItem in rptCompanyType.Items)
                                {
                                    CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                                    if (chkCompanyType.Checked)
                                    {
                                        Act_CompanyTypeMapping mappingCompanyType = new Act_CompanyTypeMapping()
                                        {
                                            ActID = act.ID,
                                            TypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim()),
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        mappingCompanyTypelist.Add(mappingCompanyType);

                                    }
                                }

                                ActManagement.CreateOrUpdateCompanyTypeMapping(mappingCompanyTypelist, act.ID, false);
                                #endregion

                                #region add Business Activity                              
                                List<Act_BusinessActivityMapping> mappingBusinessActivitylist = new List<Act_BusinessActivityMapping>();

                                foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                                {
                                    CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                                    if (chkBusinessActivityType.Checked)
                                    {
                                        Act_BusinessActivityMapping mappingBusinessActivity = new Act_BusinessActivityMapping()
                                        {
                                            ActID = act.ID,
                                            BATypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim()),
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        mappingBusinessActivitylist.Add(mappingBusinessActivity);

                                    }
                                }

                                ActManagement.CreateOrUpdateBusinessActivityMapping(mappingBusinessActivitylist, act.ID, false);
                                #endregion

                                #region add Act Applicability                              
                                List<Act_ActApplicabilityMapping> mappingActApplicabilitylist = new List<Act_ActApplicabilityMapping>();

                                foreach (RepeaterItem aItem in rptActApplicability.Items)
                                {
                                    CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                                    if (chkActApplicability.Checked)
                                    {
                                        Act_ActApplicabilityMapping mappingActApplicability = new Act_ActApplicabilityMapping()
                                        {
                                            ActID = act.ID,
                                            Act_ApplicabilityID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim()),
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        mappingActApplicabilitylist.Add(mappingActApplicability);

                                    }
                                }

                                ActManagement.CreateOrUpdateActApplicabilityMapping(mappingActApplicabilitylist, act.ID, false);
                                #endregion
                            }

                            if (isExists == false && _objActID > 0)
                            {
                                saveopo.Value = "true";
                                cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                                cvDuplicateEntry.IsValid = false;
                                //btnSave.Enabled = false;
                                BindGridForFile(_objActID);
                            }
                            #endregion
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        #region Edit
                        int actID = ActManagement.Update(act);

                        if (actID > 0)
                        {
                            Act_Document act_Document = null;
                            HttpFileCollection fileCollection = Request.Files;
                            if (fileCollection.Count > 0)
                            {
                                #region file upload
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = null;
                                    uploadfile = fileCollection[i];
                                    string fileName = uploadfile.FileName;
                                    string directoryPath = null;

                                    if (!string.IsNullOrEmpty(fileName))
                                    {
                                        string version = string.Empty;
                                        int count = 1;

                                        version = ActManagement.GetFileVersion(actID);

                                        if (!string.IsNullOrEmpty(version))
                                        {
                                            version = version.Substring(0, version.IndexOf("."));
                                            Int32.TryParse(version, out count);
                                            count++;
                                        }
                                        version = Convert.ToString(count) + ".0";

                                        directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(actID) + "/" + version);
                                        //directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(actID));

                                        DocumentManagement.CreateDirectory(directoryPath);
                                        string finalPath = Path.Combine(directoryPath, uploadfile.FileName);
                                        finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                        fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                        act_Document = new Act_Document()
                                        {
                                            Act_ID = actID,
                                            FileName = fileCollection[i].FileName,
                                            //FileData = bytes,
                                            FilePath = finalPath,
                                            IsActive = true,
                                            CreatedOn = DateTime.Now,
                                            Createdby = userId,
                                            UpdatedOn = DateTime.Now,
                                            Updatedby = userId,
                                            Version = version,
                                            VersionDate = DateTime.Now
                                        };
                                        var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                        count++;
                                    }
                                }
                                #endregion
                            }

                            #region Industry

                            List<Act_IndustryMapping> mappingInustries = new List<Act_IndustryMapping>();

                            foreach (RepeaterItem aItem in rptIndustry.Items)
                            {
                                CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                                if (chkIndustry.Checked)
                                {
                                    Act_IndustryMapping ActIndustryMapping = new Act_IndustryMapping()
                                    {
                                        ActID = act.ID,
                                        IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                        IsActive = true,
                                        EditedDate = DateTime.Now,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    mappingInustries.Add(ActIndustryMapping);
                                }
                            }

                            ActManagement.CreateOrUpdateActIndustryMapping(mappingInustries, actID, true);
                            #endregion

                            #region Sub Industry
                            BranchList.Clear();
                            List<Act_SubIndustryMapping> mappingSubInustries = new List<Act_SubIndustryMapping>();
                            //var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(actID);
                            //foreach (var Industries in vGetIndustryMappedIDs)
                            //{
                            //    int industryID = Convert.ToInt32(Industries);
                             var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(act.ID);

                             foreach (var Industries in vGetIndustryMappedIDs)
                             {
                                int industryID = Convert.ToInt32(Industries);
                                foreach (RepeaterItem aItem in rptSubIndustry.Items)
                                {
                                    CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");
                                    int subIndustryId = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID1")).Text.Trim());
                                    var subIndlist = ActManagement.GetSubIndustryMappedID(industryID, subIndustryId);
                                    // var subIndustryId1 = Convert.ToInt64(subIndlist);
                                    foreach (var item in subIndlist)
                                    {
                                        if (chksubIndustry.Checked)
                                        {
                                            Act_SubIndustryMapping ActSubIndustryMapping = new Act_SubIndustryMapping()
                                            {
                                                ActID = act.ID,
                                                IndustryID = Convert.ToInt32(industryID),
                                                SubIndustryID=Convert.ToInt32(item),
                                               // SubIndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID1")).Text.Trim()),
                                                IsActive = true,
                                                EditedDate = DateTime.Now,
                                                EditedBy = Convert.ToInt32(Session["userID"]),
                                            };
                                            mappingSubInustries.Add(ActSubIndustryMapping);
                                        }
                                    }
                                }
                            }

                            ActManagement.CreateOrUpdateActSubIndustryMapping(mappingSubInustries, actID, true);
                            #endregion

                            #region add CompanyType                            
                            List<Act_CompanyTypeMapping> mappingCompanyTypelist = new List<Act_CompanyTypeMapping>();

                            foreach (RepeaterItem aItem in rptCompanyType.Items)
                            {
                                CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                                if (chkCompanyType.Checked)
                                {
                                    Act_CompanyTypeMapping mappingCompanyType = new Act_CompanyTypeMapping()
                                    {
                                        ActID = act.ID,
                                        TypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim()),
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    mappingCompanyTypelist.Add(mappingCompanyType);

                                }
                            }

                            ActManagement.CreateOrUpdateCompanyTypeMapping(mappingCompanyTypelist, act.ID, true);
                            #endregion

                            #region add Business Activity                              
                            List<Act_BusinessActivityMapping> mappingBusinessActivitylist = new List<Act_BusinessActivityMapping>();

                            foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                            {
                                CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                                if (chkBusinessActivityType.Checked)
                                {
                                    Act_BusinessActivityMapping mappingBusinessActivity = new Act_BusinessActivityMapping()
                                    {
                                        ActID = act.ID,
                                        BATypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim()),
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    mappingBusinessActivitylist.Add(mappingBusinessActivity);

                                }
                            }

                            ActManagement.CreateOrUpdateBusinessActivityMapping(mappingBusinessActivitylist, act.ID, true);
                            #endregion

                            #region add Act Applicability                              
                            List<Act_ActApplicabilityMapping> mappingActApplicabilitylist = new List<Act_ActApplicabilityMapping>();

                            foreach (RepeaterItem aItem in rptActApplicability.Items)
                            {
                                CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                                if (chkActApplicability.Checked)
                                {
                                    Act_ActApplicabilityMapping mappingActApplicability = new Act_ActApplicabilityMapping()
                                    {
                                        ActID = act.ID,
                                        Act_ApplicabilityID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim()),
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    mappingActApplicabilitylist.Add(mappingActApplicability);

                                }
                            }

                            ActManagement.CreateOrUpdateActApplicabilityMapping(mappingActApplicabilitylist, act.ID, true);
                            #endregion

                            saveopo.Value = "true";
                            cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                            cvDuplicateEntry.IsValid = false;
                            BindGridForFile(actID);
                        }
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActDialog\").dialog('close')", true);
            BindActs();
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string type = ComplianceTypeManagement.GetByID(Convert.ToInt32(ddlType.SelectedValue)).Name;
                //if (type != "State" && type != "Central" && type != "National")
                if (type == "State")
                {
                    //divCity.Visible = ddlState.SelectedValue != "-1";
                    if (ddlState.SelectedValue != "-1")
                    {
                        BindCities();
                        ddlCity.SelectedValue = "-1";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upAct_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeSubIndustryList", string.Format("initializeJQueryUI1('{0}', 'dvsubIndustry');", txtSubIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideSubIndustryList", "$(\"#dvsubIndustry\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeCompanyTypeList", string.Format("initializeJQueryUI('{0}', 'dvCompanyType');", txtCompanyType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideCompanyTypeList", "$(\"#dvCompanyType\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBusinessActivityTypeList", string.Format("initializeJQueryUI('{0}', 'dvBusinessActivityType');", txtBusinessActivityType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideBusinessActivityTypeList", "$(\"#dvBusinessActivityType\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActApplicabilityList", string.Format("initializeJQueryUI('{0}', 'dvActApplicability');", txtActApplicability.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActApplicabilityList", "$(\"#dvActApplicability\").hide(\"blind\", null, 5, function () { });", true);



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlcountry.SelectedValue == "1")
                {
                    ddlType.Enabled = true;
                }
                else
                {
                    ddlType.SelectedValue = "2";
                    ddlType.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!(ddlType.SelectedItem.Text.Equals("National") || ddlType.SelectedItem.Text.Equals("Central") || ddlType.SelectedItem.Text.Equals("< Select Type >")))
                {
                    cmpvState.Enabled = true;
                    divState.Visible = true;
                    divCity.Visible = true;
                }
                else
                {
                    cmpvState.Enabled = false;
                    divState.Visible = false;
                    divCity.Visible = false;
                }

                if (ddlType.SelectedItem.Text == "State")
                {
                    cmpvState.Enabled = true;
                    divState.Visible = true;
                    divCity.Visible = true;
                }

                ddlState.SelectedValue = "-1";
                ddlCity.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlfilterCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAct_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var actList = ActManagement.GetAll(Convert.ToInt32(ddlfilterCatagory.SelectedValue), Convert.ToInt32(ddlFilterType.SelectedValue), tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    actList = actList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    actList = actList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAct.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAct.Columns.IndexOf(field);
                    }
                }

                grdAct.DataSource = actList;
                grdAct.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAct_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdAct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (!AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("UPDT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("RPER"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        btnSave.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("RREV"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                        btnSave.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void GrdforFileName_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int actID = -1;
                Label2.Text = string.Empty;

                if (e.CommandName.Equals("DownloadActDocument"))
                {
                    #region Download
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    actID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    var file = ActManagement.getFiledtls(ID);

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
                    // Response.BinaryWrite(file.FileData);
                    Response.TransmitFile(Server.MapPath(file.FilePath));
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    #endregion
                }
                else if (e.CommandName.Equals("DeleleActDocument"))
                {
                    #region Delete
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    actID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    long deletefiledata = ActManagement.deleteFiledtls(ID, userId);
                    if (deletefiledata > 0)
                    {
                        BindGridForFile(actID);
                        saveopo.Value = "true";
                        cvDuplicateEntry.ErrorMessage = "File Deleted Sucessfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("ViewActDocument"))
                {
                    #region View
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    actID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);

                    var ActDocument = ActManagement.getFileNamebyID(actID);
                    ActDocument = ActDocument.Where(entry => entry.ID == ID).ToList();
                    if (ActDocument.Count > 0)
                    {
                        foreach (var file in ActDocument)
                        {
                            GrdforFileName.DataSource = ActDocument.OrderBy(entry => entry.Version);
                            GrdforFileName.DataBind();

                            string filePath = Server.MapPath(file.FilePath);
                            
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
                                {
                                    
                                    Label2.Text = extension.ToUpper().Trim() + " file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }                                    

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    bw.Write(DocumentManagement.ReadDocFiles(filePath));
                                    bw.Close();

                                    ActDocReviewPath = FileName;

                                    ActDocReviewPath = ActDocReviewPath.Substring(2, ActDocReviewPath.Length - 2);

                                    Label2.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + ActDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                Label2.Text = "There is no file to preview";
                                break;
                            }                           
                        }
                       
                    }
                    #endregion
                }

                //BindGridForFile(actID);
                upAct.Update();
                saveopo.Value = "true";
               
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            divSubIndustry.Visible = true;
            saveopo.Value = "true";
           // BranchList.Clear();
            foreach (RepeaterItem aItem in rptIndustry.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkIndustry");
              
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim());
                        BranchList.Add(IndustryID);
                    }

                }
            }
            BindSubIndustry(BranchList);
         

        }

        protected void btnRefreshnew_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
        }
        public static Industry GetByID(int IndustryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Industryname = (from row in entities.Industries
                                      where row.ID == IndustryID
                                      select row).SingleOrDefault();

                return Industryname;
            }
        }

        private void BindSubIndustry(List<long?> BranchList)
        {
            try
            {
                rptSubIndustry.DataSource = CustomerBranchManagement.GetAllSubIndustry(BranchList).ToList();
                rptSubIndustry.DataBind();

                //foreach (RepeaterItem aItem in rptSubIndustry.Items)
                //{
                //    CheckBox chksubIndustry = (CheckBox)aItem.FindControl("chksubIndustry");

                //    if (!chksubIndustry.Checked)
                //    {
                //        chksubIndustry.Checked = true;
                //    }
                //}
                //CheckBox SubIndustrySelectAll = (CheckBox)rptSubIndustry.Controls[0].Controls[0].FindControl("SubIndustrySelectAll");
                //SubIndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void OnCheckBox_Changed(object sender, EventArgs e)
        //{
        //    divSubIndustry.Visible = true;
        //    //BindSubIndustry();
        //}
       
        private void BindIndustry()
        {
            try
            {
                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnActDocSave_Click(object sender, EventArgs e)
        {
            try
            {
                Act_Document act_Document = null;
                HttpFileCollection fileCollection = Request.Files;
                int ActDocType = -1;
                int actID = Convert.ToInt32(ViewState["ActID"]);

                if (fileCollection.Count > 0)
                {
                    #region file upload
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        string fileName = uploadfile.FileName;
                        string directoryPath = null;

                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string version = string.Empty;
                            int count = 1;

                            version = ActManagement.GetFileVersion(actID);

                            if (!string.IsNullOrEmpty(version))
                            {
                                version = version.Substring(0, version.IndexOf("."));
                                Int32.TryParse(version, out count);
                                count++;
                            }
                            version = Convert.ToString(count) + ".0";

                            directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(actID) + "/" + version);
                            //directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(actID));

                            DocumentManagement.CreateDirectory(directoryPath);
                            string finalPath = Path.Combine(directoryPath, uploadfile.FileName);
                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                            fileCollection[i].SaveAs(Server.MapPath(finalPath));

                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                          
                            if (!string.IsNullOrEmpty(ddlActDocType.SelectedValue))
                            {
                                if (ddlActDocType.SelectedValue != "-1")
                                {
                                    ActDocType = Convert.ToInt32(ddlActDocType.SelectedValue);
                                }
                            }

                            DateTime ?dtVerstion=null;
                            if (txtActDocVerstionDate.Text != "")
                            {
                                dtVerstion = Convert.ToDateTime(txtActDocVerstionDate.Text);
                            }
                            act_Document = new Act_Document()
                            {
                                Act_ID = actID,
                                FileName = fileCollection[i].FileName,
                                //FileData = bytes,
                                FilePath = finalPath,
                                IsActive = true,
                                CreatedOn = DateTime.Now,
                                Createdby = userId,
                                UpdatedOn = DateTime.Now,
                                Updatedby = userId,
                                Version = version,
                                VersionDate = DateTime.Now,
                                Act_TypeID = ActDocType,
                                Act_TypeVersionDate = dtVerstion,
                        };
                            var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                            count++;
                        }
                    }
                    #endregion
                }

                saveopo.Value = "true";
                BindGridForFile(actID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlActDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divDate.Visible = false;
                rfvDate.Enabled = false;
                txtActDocVerstionDate.Text = "";
                if (ddlActDocType.SelectedValue != "-1" && ddlActDocType.SelectedValue != "1")
                {
                    divDate.Visible = true;
                    rfvDate.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Added by rahul on 18 MAy 2020
        protected void btnRepeaterCompanyType_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            CompanyTypeList.Clear();
            foreach (RepeaterItem aItem in rptCompanyType.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkCompanyType");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim());
                        CompanyTypeList.Add(CompanyTypeID);
                    }

                }
            }
        }
        protected void btnBusinessActivityType_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            BusinessActivityTypeist.Clear();
            foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim());
                        BusinessActivityTypeist.Add(CompanyTypeID);
                    }

                }
            }
        }
        protected void btnActApplicability_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            ActApplicability.Clear();
            foreach (RepeaterItem aItem in rptActApplicability.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkActApplicability");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim());
                        ActApplicability.Add(CompanyTypeID);
                    }

                }
            }
        }
        public static List<CompanyType> GetAllCompanyType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.CompanyTypes.Where(a => a.ID != 4).ToList(); ;

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<BusinessActivityType> GetAllBusinessActivityType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.BusinessActivityTypes.ToList(); 

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<LocationType> GetAllLocationType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.LocationTypes.ToList();

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }


        public static List<Act_Applicability> GetAllAct_Applicability()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Act_Applicability.ToList();

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindCompanyType()
        {
            try
            {
                rptCompanyType.DataSource = GetAllCompanyType();
                rptCompanyType.DataBind();

                foreach (RepeaterItem aItem in rptCompanyType.Items)
                {
                    CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                    if (!chkCompanyType.Checked)
                    {
                        chkCompanyType.Checked = true;
                    }
                }
                CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");
                CompanyTypeSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBusinessActivityType()
        {
            try
            {
                rptBusinessActivityType.DataSource = GetAllBusinessActivityType();
                rptBusinessActivityType.DataBind();

                foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                {
                    CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                    if (!chkBusinessActivityType.Checked)
                    {
                        chkBusinessActivityType.Checked = true;
                    }
                }
                CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");
                BusinessActivityTypeSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";

                ddlLocationType.DataSource = GetAllLocationType();
                ddlLocationType.DataBind();

                ddlLocationType.Items.Insert(0, new ListItem("< Select Location Type>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindAct_Applicability()
        {
            try
            {
                rptActApplicability.DataSource = GetAllAct_Applicability();
                rptActApplicability.DataBind();

                foreach (RepeaterItem aItem in rptActApplicability.Items)
                {
                    CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                    if (!chkActApplicability.Checked)
                    {
                        chkActApplicability.Checked = true;
                    }
                }
                CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");
                ActApplicabilitySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
    }
}