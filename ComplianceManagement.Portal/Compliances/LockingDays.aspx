﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="LockingDays.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.LockingDays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Reports / Locking Details');
            setactivemenu('Myreport');
            fmaters1();
        });

    </script>

    <script type="text/C#">
        $(document).ready(function () {
            fhead('Locking Details');
            setactivemenu('Locking');
            fmaters1();
            var startDate = new Date();
            $(function () {
                $('input[id*=txtUpdateDate]').datepicker({
                    dateFormat: 'mm-dd-yy',
                    numberOfMonths: 1,
                    minDate: startDate,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        });
    </script>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 0px -1px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        input[class=k-textbox], input[type=radio] {
            width: 94%;
            margin-left: 3px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            padding-right: 3px;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        #grid1 .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #EBEBEB;
            border-color: #a6a6ad;
            color: #535b6a;
        }
    </style>

    <script type="text/javascript">
        function FilterGrid() {
            var finalSelectedfilter = { logic: "and", filters: [] };
            if ($("#dropdownlistComplianceType").val() == "S" || $("#dropdownlistComplianceType").val() == "SC" || $("#dropdownlistComplianceType").val() == "EB" || $("#dropdownlistComplianceType").val() == "EBC") {
                if ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "ActID", operator: "eq", value: $("#dropdownACT").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }
            }
            if ($("#dropdownCompliance").val() != undefined && $("#dropdownCompliance").val() != null && $("#dropdownCompliance").val() != "") {
                var ComplinceIDFilter = { logic: "or", filters: [] };
                ComplinceIDFilter.filters.push({
                    field: "ComplianceID", operator: "eq", value: $("#dropdownCompliance").val()
                });
                finalSelectedfilter.filters.push(ComplinceIDFilter);
            }
            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdownACT").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
            //fCreateStoryBoard('dropdownACT', 'filterAct', 'loc');            
            //fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            //fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }


            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        function fopenAlert() {
            alert('Record Save Successfully.');
            //alert("Compliance Unlock");
            //$('#divOverView1').modal('show');

            return false;
        }
        function CloseClearPopup() {
            window.location.reload();
            //OR history.go(0);
            return false;
        }

    </script>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }


        function ComplianceType() {
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" },
                    { text: "Event Based", value: "EB" },
                    { text: "Statutory CheckList", value: "SC" },
                    { text: "Internal CheckList", value: "IC" },
                    { text: "Event Based CheckList", value: "EBC" }
                ],
                index: 0,
                change: function (e) {
                    if ($("#dropdownlistComplianceType").val() == "S" || $("#dropdownlistComplianceType").val() == "EB" || $("#dropdownlistComplianceType").val() == "SC" || $("#dropdownlistComplianceType").val() == "EBC") {
                        $('#dvdropdownact').css('display', 'block');
                        //BindAct();
                    }
                    else {
                        $('#dvdropdownact').css('display', 'none');
                    }
                    BindGrid();
                    BindCompliance();
                }
            });
        }

        function BindAct() {
            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                    //fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
                    BindCompliance();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/BindActLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=S'
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("400");
                }
            });

        }
        function BindCompliance() {

            var ActID = -1;
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != undefined && $("#dropdownlistComplianceType").val() == "S") {
                ActID = $("#dropdownACT").val();
            }
            //alert(ActID);
            $("#dropdownCompliance").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Compliance",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=' + $("#dropdownlistComplianceType").val() + '&ActID=' + ActID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/BindLockingComplianceList?UserID=<% =UId%>&CustID=<% =CustId%>&Type=' + $("#dropdownlistComplianceType").val() + '&ActID=' + ActID                        
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("300");
                }
            });

        }

        function editAll() {
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }
        function calculateDates(first, second) {
            var diff = Math.round((second - first) / 1000 / 60 / 60 / 24); //Difference in days
            return diff - 2;
        }
        function updatedMinDates() {
            var month = kendo.date.today().getMonth() + 1;
            var getmonth = month < 10 ? '0' + month : '' + month;

            var getday = kendo.date.today().getDate();
            var day = getday < 10 ? '0' + getday : '' + getday;
            return kendo.date.today().getFullYear() + "-" + getmonth + "-" + day;
        }

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetLocakingDaysDetail?UserID=<% =UId%>&CustID=<%=CustId%>&Type=' + $("#dropdownlistComplianceType").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetLocakingDaysDetail?UserID=<% =UId%>&CustID=<%=CustId%>&Type=' + $("#dropdownlistComplianceType").val()
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                remark: { validation: { required: true, } },
                                ActivateDate: { type: "date", validation: { required: true } },
                                UserID: { type: "text" }
                            }
                        }
                    },
                    //schema: {
                    //    data: function (response) {
                    //        return response;
                    //    },
                    //    total: function (response) {
                    //        return response.length;
                    //    }
                    //},
                    pageSize: 10
                },
                excelExport: function (e) {
                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                    e.workbook.fileName = "Locking Details.xlsx";
                    e.workbook.sheets[0].title = $("#dropdownlistComplianceType").data("kendoDropDownList").text() + "_Locking Details"
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                //height: 523,
                dataBound: function (e) {
                    editAll();
                },
                sortable: true,
                filterable: true,
                groupable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                editable: false,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    {
                        selectable: true,
                        width: "30px"
                    },
                    {
                        hidden: true, field: "ID", title: "ID", width: "8%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        //hidden: true,
                        width: "12%;",
                        field: "BranchName", title: 'Location',

                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "15%;",
                        field: "ShortDescription", title: 'Compliance',

                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "12%;",
                        field: "ActName", title: 'Act Name',
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "13%;",
                        field: "PerformerName", title: 'Performer',
                        //hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "12%;", field: "DueDate", title: "Due Date",
                        //type: "date",
                        //format: "{0:dd-MMM-yyyy}",
                        template: "#= kendo.toString(kendo.parseDate(DueDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "12%;", field: "DueDay",
                        template: "#= calculateDates(kendo.parseDate(DueDate, 'yyyy-MM-dd'), kendo.parseDate(new Date(), 'yyyy-MM-dd')) #",
                        title: "Due Day",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        template: "<input data-bind='value:remark'  class='k-textbox' />",
                        title: "Remark",
                        width: "12%",
                    },
                    {
                        template: "<input id='ActivateDate' data-bind='value:ActivateDate' min='#= updatedMinDates() #' name='ActivateDate' type='date' class='k-textbox'/>",
                        format: "{0:yyyy-MM-dd}",
                        title: "Unlock Date",
                        width: "15%"
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overviewMain" }
                        ], title: "Action", lock: true, width: "6%", headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]
            });

            function dateTimeEditor1(container, options) {
                $('<input type="text" id="ActivateDate"  name="ActivateDate" required/>')
                    .appendTo(container)
                    .kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: kendo.toString(new Date(options.model.ActivateDate), 'dd-MM-yyyy'),
                        min: kendo.date.today(),
                    });
            }
            $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
                document.getElementById("<%=SelectedID.ClientID%>").value = "";
                document.getElementById("<%=txtDueDate.ClientID%>").value = "";
                document.getElementById("<%=txtPerformerName.ClientID%>").value = "";
                document.getElementById("<%=txtRemark.ClientID%>").value = "";
                document.getElementById("<%=txtUpdateDate.ClientID%>").value = "";

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#divOverView1').modal('show');
                $('.modal-dialog').css('width', '947px');
                document.getElementById("<%=SelectedID.ClientID%>").value = item.ID;
                document.getElementById("<%=txtDueDate.ClientID%>").value = kendo.toString(kendo.parseDate(item.DueDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy');
                document.getElementById("<%=txtPerformerName.ClientID%>").value = item.PerformerName;
                BindAuditlogGrid(item.ID);
                return true;
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Unlock Compliance";
                }
            });

            $("#grid").kendoTooltip({
                filter: ("th:nth-child(n+2)"),
                //filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: ("td:nth-child(n+2)"),
                //filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");


        }

        function BindAuditlogGrid(DetailID) {
            var gridexist = $('#gridauditlog').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#gridauditlog').empty();

            var grid = $("#gridauditlog").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetSubLocakingDaysDetail?UserID=<% =UId%>&CustID=<%=CustId%>&LocakingDetailID=' + DetailID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetSubLocakingDaysDetail?UserID=<% =UId%>&CustID=<%=CustId%>&LocakingDetailID=' + DetailID
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 5
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                sortable: false,
                groupable: false,
                filterable: false,
                columnMenu: false,
                pageable: true,
                reorderable: false,
                resizable: false,
                multi: true,
                selectable: false,
                columns: [
                    {
                        width: "5%;", field: "DueDate", title: "Log Date",
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(CreatedOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    },
                    { width: "5%;", field: "DueDay", title: "Due Day" },
                    {
                        width: "15%;",
                        field: "Remark", title: 'Remark',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });
        }


        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }

        function BindGridApply(e) {
            ComplianceType();
            BindGrid();
            FilterGrid();
            e.preventDefault();
        }
        $(document).ready(function () {
            ComplianceType();
            if ($("#dropdownlistComplianceType").val() != "" && $("#dropdownlistComplianceType").val() != undefined) {
                if ($("#dropdownlistComplianceType").val() == "S") {
                    $('#dvdropdownact').css('display', 'block');
                }
                else {
                    $('#dvdropdownact').css('display', 'none');
                }
            }
            else {
                $('#dvdropdownact').css('display', 'block');
            }
            BindGrid();
            BindAct();
            BindCompliance();

        });
        function exportReportGenerate(e) {

            var grid = $("#grid").data("kendoGrid");
            grid.saveAsExcel();
            e.preventDefault();
            return false;
        }

        function selectedDocumentMain(e) {
            e.preventDefault();
            var rows = $('#grid :checkbox:checked');
            var grid = $('#grid').data("kendoGrid");

            if (rows.length == 0) {
                alert('please select at least one row with data');
                return;
            }
            if (rows.length > 0) {
                var validatopnFlag = 0;
                var things = [];
                $.each(rows, function () {
                    var item = grid.dataItem($(this).closest("tr"));
                    if (item.ActivateDate == undefined || item.ActivateDate == null
                        || item.ActivateDate == "") {
                        validatopnFlag = 1;
                    }
                    item.UserID =<% =UId%>
                        things.push(item);
                });

                if (validatopnFlag == 0) {
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '<% =Path%>/Data/MultipleUpdateLocking',
                            data: JSON.stringify(things),
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                            success: function (data) {
                                if (data[0].Message == "Save") {
                                    alert('Record Save Successfully.');
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    $("#grid").data("kendoGrid").refresh();
                                    $('#grid').data('kendoGrid')._selectedIds = {};
                                    $('#grid').data('kendoGrid').clearSelection();
                                }
                            },
                            failure: function (response) {
                                $('#result').html(response);
                            }
                        });
                }
                else {
                    alert('Please select unlock date on selected row');
                    return;
                }
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
            <div class="col-md-12 colpadding0">
                <div class="col-md-2" style="width: 20%; padding-left: 9px;">
                    <input id="dropdownlistComplianceType" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 28%; padding-left: 4px; display: none;" id="dvdropdownact">
                    <input id="dropdownACT" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 25%; padding-left: 4px;">
                    <input id="dropdownCompliance" style="width: 100%;" />
                </div>
                <div class="col-md-2" style="width: 20%; padding-left: 4px;">

                    <button id="exportReport" style="height: 28px; margin-top: 1px;" onclick="exportReportGenerate(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                </div>

                <%--<div class="col-md-6 colpadding0 text-right" style="margin-left: 411px; margin-top: -30px;">
                          <button id="exportReport" style="height: 28px;margin-top: 1px;" onclick="exportReportGenerate(event)"  data-placement="bottom" ><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                       </div>--%>
                <div class="col-md-2" style="width: 20%; padding-left: 9px;">
                </div>
                <div class="col-md-2" style="width: 20%; padding-left: 9px;">
                    <button id="dvbtndownloadDocumentMain" style="float: right; margin-right: -757px; margin-top: -29px; height: 28px;" onclick="selectedDocumentMain(event)">Submit</button>
                </div>
            </div>
        </div>
        <%--                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterAct">&nbsp;</div>--%>

        <div id="grid" style="margin-top: 10px;"></div>
    </div>
    <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 600px;">

                    <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="VSCasePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceValidationGroup1" />
                            <asp:CustomValidator ID="cvCasePopUp" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceValidationGroup1" Display="none" class="alert alert-block alert-danger fade in" />
                        </asp:Panel>
                    </div>
                    <div style="margin-bottom: 7px">
                        <h2>Unlock Compliance</h2>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Performer Name</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtPerformerName" class="form-control" Style="width: 167px;" ReadOnly="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Due Date</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtDueDate" placeholder="DD-MM-YYYY"
                                            class="form-control" Style="width: 167px;" ReadOnly="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Update Date for Unlock</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" ID="txtUpdateDate" placeholder="DD-MM-YYYY"
                                            class="form-control" Style="width: 167px;" />
                                        <asp:RequiredFieldValidator ErrorMessage="Please Enter Date." ControlToValidate="txtUpdateDate"
                                            runat="server" ID="RequiredFieldValidator4" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                        <label style="font-weight: bold; vertical-align: text-top;">Remark</label>
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                    <td style="width: 73%;">
                                        <asp:TextBox runat="server" TextMode="MultiLine" ID="txtRemark" CssClass="form-control" autocomplete="off" Width="280px" />
                                        <%--  <asp:RequiredFieldValidator ErrorMessage="Please Enter Remark." ControlToValidate="txtRemark"
                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 5px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <asp:TextBox runat="server" ID="SelectedID" Style="display: none;" />
                                    </td>
                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;"></td>
                                    <td>
                                        <asp:Button Text="Submit" runat="server" ID="btnSave"
                                            OnClick="btnSave_Click" ValidationGroup="ComplianceValidationGroup1" CssClass="btn btn-search" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div>
                        <h2>Audit Log</h2>
                    </div>
                    <div id="gridauditlog" style="border: none;"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
