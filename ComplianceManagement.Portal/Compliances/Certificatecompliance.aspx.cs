﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Certificatecompliance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindCategories();
                BindTreeView(tvFilterLocation);
                BindDepartment();
                tbxFilterLocation.Text = "< Select >";
               
              
            }
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

        }
        public static object FillDepartment(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where
                             //row.IsDeleted == false &&
                             row.CustomerID == CustomerId
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        private void BindDepartment()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ddlFilterDepartment.DataTextField = "Name";
                ddlFilterDepartment.DataValueField = "ID";
                ddlFilterDepartment.Items.Clear();
                var deptdetails =FillDepartment(customerID);
                ddlFilterDepartment.DataSource = deptdetails;
                ddlFilterDepartment.DataBind();
                ddlFilterDepartment.Items.Insert(0, new ListItem("< Select Department >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";

                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();

                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
               
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTreeView(TreeView tvLocation)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                }

                tvLocation.ExpandAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

      
        public static string getname(long userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userid
                            select row).FirstOrDefault();

                return user.FirstName + ' ' + user.LastName;
            }
        }
        public static string designation(long userid)
        {
             using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var designationdata = (from row in entities.Users
                                       where row.ID==userid
                                       select row.Designation).SingleOrDefault();
                return designationdata;
            }
        }
        protected void ExportToWord(object sender, EventArgs e)
        {
            int catid = -1;
            int deptid = -1;
            int branchid = -1;
            catid = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
            //deptid = Convert.ToInt32(ddlFilterDepartment.SelectedValue);
            branchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            int userid = Convert.ToInt32(AuthenticationHelper.UserID);
           // string userdata= getname(userid).ToString();
            var username= getname(userid);
            
            string designationname = designation(userid);
            DateTime dtfrom = DateTime.Now;
            DateTime dtTo = DateTime.Now;
            DateTime currentdate1 = DateTime.Now;

            DateTime currentdate = currentdate1.Date;
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
           



            if (txtStartDate.Text == "")
            {
                dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            if (txtEndDate.Text == "")
            {
                dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data1 = (from row in entities.ComplianceCertificateTransactions
                             where row.CustomerID==customerid
                             select row.TemplateContent).FirstOrDefault();
                if (data1 != null)
                {

                    var data12 = (from row in entities.SP_ComplianceCertificate(catid, deptid,branchid,dtfrom,dtTo)
                                  select row).ToList();
                    if(data12.Count==0)
                    {
                        // LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Data not found.";
                    }
                    else
                    {
                        #region Complied Act List

                        List<string> ActList = data12.Where(x => x.Status.Equals("Complied")).Select(x => x.Name).ToList();
                        ActList = ActList.Distinct().ToList();

                        int cnt = 1;
                        System.Text.StringBuilder strExportforActList = new System.Text.StringBuilder();
                        strExportforActList.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforActList.Append(@"<Tr>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Sr.No.");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"<Td>");
                        strExportforActList.Append(@"Act Name");
                        strExportforActList.Append(@"</Td>");
                        strExportforActList.Append(@"</Tr>");
                        ActList.ForEach(eachSection =>
                        {
                            strExportforActList.Append(@"<Tr>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(cnt);
                            strExportforActList.Append(@" </Td>");
                            strExportforActList.Append(@"<Td>");
                            strExportforActList.Append(eachSection);
                            strExportforActList.Append(@"</Td>");
                            cnt++;
                        });
                        strExportforActList.Append(@"</Table>");

                        #endregion
                        
                        #region non Complied Act List

                        List<string> nonCompliedActList = data12.Where(x => x.Status.Equals("NonComplied")).Select(x=>x.Name).ToList();
                        nonCompliedActList = nonCompliedActList.Distinct().ToList();
                        int cnt1 = 1;
                        System.Text.StringBuilder strExportforNonCompliedList = new System.Text.StringBuilder();
                        strExportforNonCompliedList.Append(@"<Table id='hrdftrtbl'  border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforNonCompliedList.Append(@"<Tr>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Sr.No.");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"<Td>");
                        strExportforNonCompliedList.Append(@"Act Name");
                        strExportforNonCompliedList.Append(@"</Td>");
                        strExportforNonCompliedList.Append(@"</Tr>");
                        nonCompliedActList.ForEach(eachSection =>
                        {
                            strExportforNonCompliedList.Append(@"<Tr>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(cnt1);
                            strExportforNonCompliedList.Append(@" </Td>");
                            strExportforNonCompliedList.Append(@"<Td>");
                            strExportforNonCompliedList.Append(eachSection);
                            strExportforNonCompliedList.Append(@"</Td>");
                            cnt1++;
                        });
                        strExportforNonCompliedList.Append(@"</Table>");

                        #endregion

                        #region Complied

                        var Compliedlist = data12.Where(x => x.Status.Equals("Complied")).ToList();
                        
                        System.Text.StringBuilder strExportforComplied = new System.Text.StringBuilder();
                        strExportforComplied.Append(@"<Table id='hrdftrtbl' margin-left='-20px' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                        strExportforComplied.Append(@"<Tr>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"ComplianceID");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Act Name");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Location");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"ComplianceType");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Remarks");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Reportno");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Closure");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Division Vertical");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(@"Keywords");
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"</Tr>");
                     Compliedlist.ForEach(eachSection =>
                    {
                        strExportforComplied.Append(@"<Tr>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.ComplianceID);
                        strExportforComplied.Append(@" </Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Name);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Location);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.ComplianceType);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Remarks);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Reportno);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Closure);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.DivisionVertical);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"<Td>");
                        strExportforComplied.Append(eachSection.Keywords);
                        strExportforComplied.Append(@"</Td>");
                        strExportforComplied.Append(@"</Tr>");
                    });
                        strExportforComplied.Append(@"</Table>");

                        #endregion

                        #region non Complied
                        var NonCompliedlist = data12.Where(x => x.Status.Equals("NonComplied")).ToList();
                     
                        System.Text.StringBuilder strExportforNonComplied = new System.Text.StringBuilder();
                       strExportforNonComplied.Append(@"<Table id='hrdftrtbl' border='1' width='100%' cellspacing='0' cellpadding='0'>");
                       strExportforNonComplied.Append(@"<Tr>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"ComplianceID");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Act Name");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Location");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"ComplianceType");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Remarks");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Reportno");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Closure");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Division Vertical");
                       strExportforNonComplied.Append(@"</Td>");
                       strExportforNonComplied.Append(@"<Td>");
                       strExportforNonComplied.Append(@"Keywords");
                       strExportforNonComplied.Append(@"</Td>");
                        strExportforNonComplied.Append(@"</Tr>");
                        NonCompliedlist.ForEach(eachSection =>
                        {
                            strExportforNonComplied.Append(@"<Tr>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ComplianceID);
                            strExportforNonComplied.Append(@" </Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Name);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Location);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.ComplianceType);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Remarks);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Reportno);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Keywords);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.DivisionVertical);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"<Td>");
                            strExportforNonComplied.Append(eachSection.Keywords);
                            strExportforNonComplied.Append(@"</Td>");
                            strExportforNonComplied.Append(@"</Tr>");                            
                        });
                        strExportforNonComplied.Append(@"</Table>");
                        #endregion

                        
                        string relacedataactnondemo = data1.Replace("{{NonCompliedActList}}", strExportforNonCompliedList.ToString());
                        string relacedataactdemo = relacedataactnondemo.Replace("{{CompliedActList}}", strExportforActList.ToString());
                        string relacedataComplieddemo = relacedataactdemo.Replace("{{CompliedList}}", strExportforComplied.ToString());
                        string relacedatanonComplieddemo = relacedataComplieddemo.Replace("{{NonCompliedList}}", strExportforNonComplied.ToString());
                        // string replacedatedate = relacedata("{{EndDate}}", strExporttoWord.ToString());
                        string relacedataenddate = relacedatanonComplieddemo.Replace("{{EndDate}}", txtEndDate.Text);
                        string relacedatadesignation = relacedataenddate.Replace("{{UserDesignation}}", designationname);
                        string replacecurrentdate = relacedatadesignation.Replace("{{StartDate}}", currentdate.ToString("dd/MM/yyyy"));
                        string relacedata = replacecurrentdate.Replace("{{UserName}}", username.ToString());





                        HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/msword";
                    string strFileName = "ComplianceCertificate" + ".doc";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

                    StringBuilder strHTMLContent = new StringBuilder();
                    strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
                    strHTMLContent.Append(relacedata);
                    strHTMLContent.Append("</body></html>");

                    HttpContext.Current.Response.Write(strHTMLContent);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
            }
        }
      }
    }
}