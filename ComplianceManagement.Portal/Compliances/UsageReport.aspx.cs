﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class UsageReport : System.Web.UI.Page
    {
        public static List<int> Branchlist = new List<int>();
        public static List<long> UserList = new List<long>();
        protected int CustomerID = 0;
        protected static List<Int32> roles;
        public static bool IsInternalUsageReportVisible;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    BindLocationFilter();

                    bool IsInternalUsageReportVisible = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "InternalUsageReport");

                    if(IsInternalUsageReportVisible == false)
                    {
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByText("Internal"));
                    }
                      
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    if(roles.Contains(3))
                    {
                        ListItem li = new ListItem("Performer", "2");
                        ddlStatus.Items.Add(li);
                    }

                    if (roles.Contains(4))
                    {
                        ListItem li = new ListItem("Reviewer", "3");
                        ddlStatus.Items.Add(li);
                    }
                    //ExportAllData();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(CustomerID);

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, CustomerID, AuthenticationHelper.Role, "S");

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    // BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }


                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static int MonthDiff(DateTime d1, DateTime d2)
        {
            int retVal = 0;

            if (d1.Month < d2.Month)
            {
                retVal = (d1.Month + 12) - d2.Month;
                retVal += ((d1.Year - 1) - d2.Year) * 12;
            }
            else
            {
                retVal = (d1.Month - d2.Month) + 1;
                retVal += (d1.Year - d2.Year) * 12;
            }
            return retVal;
        }
        public void ExportAllData()
        {
            try
            {
                if (ddlComplianceType.SelectedValue == "0")
                {
                    #region Statutory
                    DateTime fromDatePeroid = new DateTime();
                    DateTime toDatePeroid = new DateTime();
                    DateTime dateResult;
                    DateTime dateResult1;

                    if (DateTime.TryParse(txtStartDate.Text, out dateResult))
                    {
                        if (DateTime.TryParse(txtEndDate.Text, out dateResult1))
                        {
                            fromDatePeroid = Convert.ToDateTime(txtStartDate.Text);
                            toDatePeroid = Convert.ToDateTime(txtEndDate.Text);
                            if (fromDatePeroid > toDatePeroid)
                            {
                                cvDuplicateEntry.ErrorMessage = "From date should not be greater than To Date.";
                                cvDuplicateEntry.IsValid = false;
                                DateTime date = DateTime.Now;
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                                return;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.ErrorMessage = "From Date and To Date should not be blank.";
                            cvDuplicateEntry.IsValid = false;
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            return;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "From Date and To Date should not be blank.";
                        cvDuplicateEntry.IsValid = false;
                        DateTime date = DateTime.Now;
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                        return;
                    }

                    var NoOfMonth = MonthDiff(toDatePeroid, fromDatePeroid);
                    string user_Roles = AuthenticationHelper.Role;
                    Branchlist.Clear();
                    GetAllHierarchy(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(tvFilterLocation.SelectedValue));

                    List<SP_UsageReports_Result> Master_SP_UsageReports = new List<SP_UsageReports_Result>();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (ddlStatus.SelectedValue == "2") //Performer
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), "PER", fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }
                        else if (ddlStatus.SelectedValue == "3") //Reviewer
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), "REV", fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }
                        else if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), user_Roles, fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }

                    }
                    var usersfrom = (from row in Master_SP_UsageReports
                                     select row.UserID).ToList();
                    if (ddlStatus.SelectedValue == "3") //Reviewer
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var userhier = (from NCCP in entities.ComplianceAssignments
                                            join CF in entities.ComplianceInstances
                                            on NCCP.ComplianceInstanceID equals CF.ID

                                            where NCCP.UserID == AuthenticationHelper.UserID
                                            select NCCP.ComplianceInstanceID).Distinct().ToList();

                            usersfrom = (from NCCP in entities.ComplianceAssignments
                                         join CF in entities.ComplianceInstances
                                         on NCCP.ComplianceInstanceID equals CF.ID

                                         where userhier.Contains(NCCP.ComplianceInstanceID)
                                         select NCCP.UserID).Distinct().ToList();
                        }


                    }
                    else if (user_Roles.Contains("MGMT"))
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            usersfrom = (from NCCP in entities.ComplianceAssignments
                                         join CF in entities.ComplianceInstances
                                         on NCCP.ComplianceInstanceID equals CF.ID
                                         join CF1 in entities.EntitiesAssignments
                                         on CF.CustomerBranchID equals CF1.BranchID
                                         where CF1.UserID == AuthenticationHelper.UserID
                                         select NCCP.UserID).Distinct().ToList();
                        }
                    }



                    #region Define DataTable
                    DataTable dtLogingDatail = new DataTable();
                    DataTable dtLastLogingDatails = new DataTable();
                    DataTable dtLogingDatailOpen = new DataTable();
                    DataTable dtCompleteCompliance = new DataTable();
                    DataTable dtAfterDueDate = new DataTable();
                    DataTable dtReviewerData = new DataTable();

                    dtLogingDatail.Columns.Add("SLNO", typeof(string));
                    dtLogingDatail.Columns.Add("UserID", typeof(string));
                    dtLogingDatail.Columns.Add("UserName", typeof(string));
                    dtLogingDatail.Columns.Add("Location", typeof(string));
                    dtLogingDatail.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtLogingDatail.Columns.Add("Month" + i, typeof(string));
                    }

                    //Last Loging
                    dtLastLogingDatails.Columns.Add("SLNO", typeof(string));
                    dtLastLogingDatails.Columns.Add("UserID", typeof(string));
                    dtLastLogingDatails.Columns.Add("UserName", typeof(string));
                    dtLastLogingDatails.Columns.Add("Location", typeof(string));
                    dtLastLogingDatails.Columns.Add("Role", typeof(string));
                    dtLastLogingDatails.Columns.Add("LastLogingTime", typeof(DateTime));

                    //Open
                    dtLogingDatailOpen.Columns.Add("SLNO", typeof(string));
                    dtLogingDatailOpen.Columns.Add("UserID", typeof(string));
                    dtLogingDatailOpen.Columns.Add("UserName", typeof(string));
                    dtLogingDatailOpen.Columns.Add("Location", typeof(string));
                    dtLogingDatailOpen.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtLogingDatailOpen.Columns.Add("Month" + i, typeof(string));
                    }

                    //After
                    dtCompleteCompliance.Columns.Add("SLNO", typeof(string));
                    dtCompleteCompliance.Columns.Add("UserID", typeof(string));
                    dtCompleteCompliance.Columns.Add("UserName", typeof(string));
                    dtCompleteCompliance.Columns.Add("Location", typeof(string));
                    dtCompleteCompliance.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtCompleteCompliance.Columns.Add("Month" + i, typeof(string));
                    }


                    //Download
                    dtAfterDueDate.Columns.Add("SLNO", typeof(string));
                    dtAfterDueDate.Columns.Add("UserID", typeof(string));
                    dtAfterDueDate.Columns.Add("UserName", typeof(string));
                    dtAfterDueDate.Columns.Add("Location", typeof(string));
                    dtAfterDueDate.Columns.Add("Role", typeof(string));
                    dtAfterDueDate.Columns.Add("BeforeDays", typeof(string));
                    dtAfterDueDate.Columns.Add("ThreeDays", typeof(string));
                    dtAfterDueDate.Columns.Add("SevenDays", typeof(string));
                    dtAfterDueDate.Columns.Add("FifteenDays", typeof(string));
                    dtAfterDueDate.Columns.Add("ThirtyDays", typeof(string));
                    dtAfterDueDate.Columns.Add("MoreThanThirtyDays", typeof(string));

                    dtReviewerData.Columns.Add("SLNO", typeof(string));
                    dtReviewerData.Columns.Add("UserID", typeof(string));
                    dtReviewerData.Columns.Add("UserName", typeof(string));
                    dtReviewerData.Columns.Add("Location", typeof(string));
                    dtReviewerData.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtReviewerData.Columns.Add("Month" + i, typeof(string));
                    }

                    #endregion

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        #region Compliance Complete within Due Days
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> Performerstatus = new List<int>();
                            Performerstatus.Add(2);
                            Performerstatus.Add(4);
                            Performerstatus.Add(7);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;


                            if (ddlStatus.SelectedValue == "2") //Performer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                                usersfrom = (from row in FilteredData
                                             select row.UserID).ToList();
                            }
                            else if (ddlStatus.SelectedValue == "3") //Reviewer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                                usersfrom = (from row in FilteredData
                                             select row.UserID).ToList();
                            }
                            else
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }

                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            }

                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> PerformerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }


                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate
                                                          select data).ToList();
                                        var Mont1hToltal = DataMonth1.Select(e => e.ScheduledOn).Count();
                                        var Month1ActualData = DataMonth1.Where(e => e.submittedcnt != null).Count();
                                        // var Month1ActualData = DataMonth1.Where(e => Performerstatus.Contains(e.ComplianceStatusID)).Count();
                                        PerformerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtCompleteCompliance.Rows.Add(Count, item, UserName, UserLocation, Role);

                                    int p = 0;
                                    for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                    {
                                        dtCompleteCompliance.Rows[RowCount1][j] = PerformerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion

                        #region After Due date Completed

                        List<int> StatusList = new List<int>();
                        StatusList.Add(3);
                        StatusList.Add(5);
                        List<int> Performerstatus1 = new List<int>();
                        Performerstatus1.Add(2);
                        Performerstatus1.Add(4);
                        Performerstatus1.Add(7);

                        List<int> notinstatusid = new List<int>();
                        notinstatusid.Add(10);
                        notinstatusid.Add(11);
                        notinstatusid.Add(12);
                        notinstatusid.Add(13);
                        notinstatusid.Add(14);
                        if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;

                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }



                            FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            var DistinctUserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in DistinctUserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    string BeforeDays = string.Empty;
                                    string ThreeDays = string.Empty;
                                    string SevenDays = string.Empty;
                                    string FifteenDays = string.Empty;
                                    string ThirtyDays = string.Empty;
                                    string MoreThanThirtyDays = string.Empty;
                                    Count++;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }

                                    #region After Due Date Data Assign
                                    // On Or Before Due Date
                                    var DataMonthBeforeDue = (from data in FilteredData
                                                              where data.UserID == item
                                                              //&& data.closeDate <= data.ScheduledOn 
                                                              //&& Performerstatus1.Contains(data.ComplianceStatusID)
                                                              select data).ToList();
                                    var Month1ActualData = DataMonthBeforeDue.Where(e => e.submittedcnt != null).Count();

                                    // BeforeDays = Convert.ToString(DataMonthBeforeDue.Select(e => e.ScheduledOn).Count());
                                    BeforeDays = Convert.ToString(Month1ActualData);
                                    // In 3 Days
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate > data.ScheduledOn
                                                      && data.closeDate <= data.ScheduledOn.AddDays(3)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    ThreeDays = Convert.ToString(DataMonth1.Select(e => e.ScheduledOn).Count());

                                    // In 7 Days
                                    var DataMonth2 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(3)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(7)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    SevenDays = Convert.ToString(DataMonth2.Select(e => e.ScheduledOn).Count());

                                    // In 15 Days
                                    var DataMonth3 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(7)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(15)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    FifteenDays = Convert.ToString(DataMonth3.Select(e => e.ScheduledOn).Count());

                                    // In 30 Days
                                    var DataMonth4 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(15)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(30)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    ThirtyDays = Convert.ToString(DataMonth4.Select(e => e.ScheduledOn).Count());

                                    // More Than 30 Days
                                    var DataMonth5 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(30)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(365)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    MoreThanThirtyDays = Convert.ToString(DataMonth5.Select(e => e.ScheduledOn).Count());

                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtAfterDueDate.Rows.Add(Count, item, UserName, UserLocation, Role, BeforeDays, ThreeDays, SevenDays, FifteenDays, ThirtyDays, MoreThanThirtyDays);
                                }
                            }
                        }

                        #endregion

                        #region Open Compliance
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> Openstatus = new List<int>();
                            Openstatus.Add(1);
                            Openstatus.Add(10);
                            Openstatus.Add(11);
                            Openstatus.Add(12);
                            Openstatus.Add(13);
                            Openstatus.Add(14);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;
                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }
                            FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> PerformerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }


                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate
                                                          select data).ToList();
                                        var MonthOpenCount = DataMonth1.Where(e => Openstatus.Contains(e.ComplianceStatusID)).Count();
                                        PerformerMonthList.Add(Convert.ToString(MonthOpenCount));
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtLogingDatailOpen.Rows.Add(Count, item, UserName, UserLocation, Role);

                                    int p = 0;
                                    for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                    {
                                        dtLogingDatailOpen.Rows[RowCount1][j] = PerformerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion

                        #region Reviewer Compliance Complete
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> StatusList1 = new List<int>();
                            StatusList1.Add(2);
                            StatusList1.Add(3);
                            StatusList1.Add(4);
                            StatusList1.Add(5);
                            StatusList1.Add(11);
                            List<int> StatusListClose = new List<int>();
                            StatusListClose.Add(4);
                            StatusListClose.Add(5);
                            StatusListClose.Add(11);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;
                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }
                            //var ReviewerData = FilteredData.Where(entry => entry.RoleID == 4).ToList();

                            if (ddlStatus.SelectedValue == "2") //Performer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                            }
                            else if (ddlStatus.SelectedValue == "3") //Reviwer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                            }
                            else
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                            }
                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> ReviewerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }

                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where
                                                          data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate
                                                          //&& data.closeDate >= startDate
                                                          //&& data.closeDate <= endDate
                                                          select data).ToList();

                                        //var ScheduleOnIDList = DataMonth1.Select(e => e.ScheduledOnID).Distinct().ToList();

                                        //var PerformerData = (from data in FilteredData
                                        //                     where
                                        //                     data.RoleID == 3
                                        //                     && ScheduleOnIDList.Contains(data.ScheduledOnID)
                                        //                     && data.closeDate >= startDate
                                        //                     && data.closeDate <= endDate
                                        //                     select data).ToList();

                                        var Mont1hToltal = DataMonth1.Where(e => StatusList1.Contains(e.ComplianceStatusID)).Count();
                                        var Month1ActualData = DataMonth1.Where(e => StatusListClose.Contains(e.ComplianceStatusID)).Count();
                                        ReviewerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtReviewerData.Rows.Add(Count, item, UserName, UserLocation, Role);
                                    int p = 0;
                                    for (int j = 5; j < dtReviewerData.Columns.Count; j++)
                                    {
                                        dtReviewerData.Rows[RowCount1][j] = ReviewerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion
                    }


                    #region Loging Details
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (user_Roles.Contains("MGMT"))
                        {
                            int Count = 0;
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = (from row in entities.Sp_AVACOM_LoginDetailHistory(Convert.ToInt32(AuthenticationHelper.CustomerID), fromDatePeroid, toDatePeroid)
                                                select row).ToList();

                            var UserList12 = FilteredData.Where(e => usersfrom.Contains(e.UserID)).ToList();
                            var UserList1 = UserList12.Select(e => e.UserID).Distinct().ToList();
                            int RowCount1 = 0;
                            foreach (var item in UserList1)//UserList
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> LogingMonthList = new List<string>();
                                Count++;
                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.LastLoginDate >= startDate
                                                      && data.LastLoginDate <= endDate
                                                      select data).ToList();
                                    var Mont1hToltal = DataMonth1.Select(e => e.LastLoginDate).Count();
                                    LogingMonthList.Add(Convert.ToString(Mont1hToltal));
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();

                                var Userlist = FilteredData.Where(e => e.UserID == item).FirstOrDefault();
                                if (Userlist != null)
                                {
                                    UserName = Userlist.UserName;
                                }
                                if (string.IsNullOrEmpty(Role))
                                {
                                    using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                    {
                                        Role = (from row in entities1.Users
                                                join row1 in entities1.Roles
                                                on row.RoleID equals row1.ID
                                                where row.ID == item
                                                select row1.Name).FirstOrDefault();
                                    }
                                    if (Role == "Company Admin" || Role == "Management")
                                    {
                                    }
                                    else
                                    {
                                        Role = string.Empty;
                                    }
                                }

                                dtLogingDatail.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtLogingDatail.Columns.Count; j++)
                                {
                                    dtLogingDatail.Rows[RowCount1][j] = Convert.ToString(LogingMonthList[p]);
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion

                    #region Last Login Details
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            var FilteredData1 = (from row in entities.Sp_AVACOM_LoginDetailHistory_LastLogin(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                 select row).ToList();

                            // FilteredData = FilteredData.Where(e => UserList.Contains(e.UserID)).ToList();
                            var FilteredData2 = FilteredData1.Where(e => usersfrom.Contains(e.UserID)).ToList();


                            int Count = 1;
                            foreach (var item in FilteredData2)
                            {
                                var Role = GetRole(item.UserID);
                                if (string.IsNullOrEmpty(Role))
                                {
                                    using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                    {
                                        Role = (from row in entities1.Users
                                                join row1 in entities1.Roles
                                                on row.RoleID equals row1.ID
                                                where row.ID == item.UserID
                                                select row1.Name).FirstOrDefault();
                                    }
                                    if (Role == "Company Admin" || Role == "Management")
                                    {
                                    }
                                    else
                                    {
                                        Role = string.Empty;
                                    }
                                }
                                dtLastLogingDatails.Rows.Add(Count++, item.UserID, item.UserName, item.Location, Role, item.LastLoginTime);
                            }
                        }
                    }
                    #endregion

                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                        if (user_Roles.Contains("MGMT"))
                        {
                            ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Number of User logins in Month");
                            ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Last Login Date Report");

                            #region Each Month Loging Details
                            DataView view3 = new System.Data.DataView(dtLogingDatail);
                            DataTable ExcelData3 = null;
                            //ExcelData3 = view3.ToTable("Selected", false, "SLNO", "UserName", "Location", "Role", "Month1", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9", "Month10", "Month11", "Month12");

                            ExcelData3 = view3.ToTable();

                            exWorkSheet3.Cells["A1:B1"].Merge = true;
                            exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet3.Cells["C1:D1"].Merge = true;
                            exWorkSheet3.Cells["C1"].Value = cname;

                            exWorkSheet3.Cells["A2:B2"].Merge = true;
                            exWorkSheet3.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A2"].Value = "Report Name:";

                            exWorkSheet3.Cells["C2:D2"].Merge = true;
                            exWorkSheet3.Cells["C2"].Value = "Number of User logins in Month";

                            exWorkSheet3.Cells["A3:B3"].Merge = true;
                            exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A3"].Value = "Report Generated On:";

                            exWorkSheet3.Cells["C3:D3"].Merge = true;
                            exWorkSheet3.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                            if (dtLogingDatail.Rows.Count > 0)
                            {
                                exWorkSheet3.Cells["A6"].LoadFromDataTable(ExcelData3, false);
                            }
                            exWorkSheet3.Cells["A5"].Value = "SrNo";
                            exWorkSheet3.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["A5"].AutoFitColumns(10);
                            exWorkSheet3.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet3.Cells["B5"].Value = "UserID";
                            exWorkSheet3.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["B5"].AutoFitColumns(15);
                            exWorkSheet3.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet3.Cells["C5"].Value = "User Name";
                            exWorkSheet3.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["C5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet3.Cells["D5"].Value = "Location";
                            exWorkSheet3.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["D5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet3.Cells["E5"].Value = "Role";
                            exWorkSheet3.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["E5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            int AsciiValue = 70;
                            bool CheckAsciiVal = false;
                            for (int i = 0; i < NoOfMonth; i++)
                            {
                                DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                                var ColVal = string.Empty;
                                if (NoOfMonth > 21)
                                {
                                    if (AsciiValue == 91)
                                    {
                                        CheckAsciiVal = true;
                                        AsciiValue = 65;
                                    }
                                    if (CheckAsciiVal)
                                    {
                                        ColVal = "A" + Convert.ToChar(AsciiValue);
                                    }
                                    else
                                    {
                                        ColVal = Convert.ToChar(AsciiValue).ToString();
                                    }

                                    exWorkSheet3.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells[ColVal + "5"].AutoFitColumns(15);
                                    exWorkSheet3.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    AsciiValue++;
                                }
                                else
                                {
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].AutoFitColumns(15);
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    AsciiValue++;
                                }
                            }

                            int count3 = Convert.ToInt32(ExcelData3.Rows.Count) + 5;
                            using (ExcelRange col = exWorkSheet3.Cells[5, 1, count3, NoOfMonth + 5])
                            {
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            using (ExcelRange col = exWorkSheet3.Cells[5, 6, count3, NoOfMonth + 5])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            #endregion

                            #region Last Login

                            DataView view4 = new System.Data.DataView(dtLastLogingDatails);
                            DataTable ExcelData4 = null;
                            ExcelData4 = view4.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "LastLogingTime");

                            foreach (DataRow item in ExcelData4.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["LastLogingTime"])))
                                {
                                    item["LastLogingTime"] = Convert.ToDateTime(item["LastLogingTime"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet4.Cells["A1:B1"].Merge = true;
                            exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet4.Cells["C1:D1"].Merge = true;
                            exWorkSheet4.Cells["C1"].Value = cname;

                            exWorkSheet4.Cells["A2:B2"].Merge = true;
                            exWorkSheet4.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A2"].Value = "Report Name:";

                            exWorkSheet4.Cells["C2:D2"].Merge = true;
                            exWorkSheet4.Cells["C2"].Value = "Last Login Date Report";

                            exWorkSheet4.Cells["A3:B3"].Merge = true;
                            exWorkSheet4.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A3"].Value = "Report Generated On:";

                            exWorkSheet4.Cells["C3:D3"].Merge = true;
                            exWorkSheet4.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                            if (dtLastLogingDatails.Rows.Count > 0)
                            {
                                exWorkSheet4.Cells["A6"].LoadFromDataTable(ExcelData4, false);
                            }
                            exWorkSheet4.Cells["A5"].Value = "SrNo";
                            exWorkSheet4.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["A5"].AutoFitColumns(10);
                            exWorkSheet4.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["B5"].Value = "UserID";
                            exWorkSheet4.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["B5"].AutoFitColumns(15);
                            exWorkSheet4.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["C5"].Value = "User Name";
                            exWorkSheet4.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["C5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet4.Cells["D5"].Value = "Location";
                            exWorkSheet4.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["D5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet4.Cells["E5"].Value = "Role";
                            exWorkSheet4.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["E5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["F5"].Value = "Last Login";
                            exWorkSheet4.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["F5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            int count4 = Convert.ToInt32(ExcelData4.Rows.Count) + 5;
                            using (ExcelRange col = exWorkSheet4.Cells[5, 1, count4, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            using (ExcelRange col = exWorkSheet4.Cells[5, 5, count4, 6])
                            {
                                col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            #endregion
                        }
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Performance Report - In Time (P)");
                        ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Ageing of System Updation");
                        ExcelWorksheet exWorkSheet6 = exportPackge.Workbook.Worksheets.Add("Performance Report- OverDue (P)");
                        ExcelWorksheet exWorkSheet5 = exportPackge.Workbook.Worksheets.Add("Performance Report (Reviewer)");

                        #region Compliance Due Date Detail of Year
                        DataView view = new System.Data.DataView(dtCompleteCompliance);
                        DataTable ExcelData = null;
                        ExcelData = view.ToTable();
                        exWorkSheet1.Cells["A1:B1"].Merge = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet1.Cells["C1:D1"].Merge = true;
                        exWorkSheet1.Cells["C1"].Value = cname;

                        exWorkSheet1.Cells["A2:B2"].Merge = true;
                        exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A2"].Value = "Report Name:";

                        exWorkSheet1.Cells["C2:D2"].Merge = true;
                        exWorkSheet1.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                        exWorkSheet1.Cells["A3:B3"].Merge = true;
                        exWorkSheet1.Cells["A3"].Value = "Note:";
                        exWorkSheet1.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet1.Cells["C3:G3"].Merge = true;
                        exWorkSheet1.Cells["C3"].Value = "Month Wise Summary of Compliance completed " + " 'In Time'" + " as against compliances due in that Period";

                        exWorkSheet1.Cells["A4:B4"].Merge = true;
                        exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet1.Cells["C4:D4"].Merge = true;
                        exWorkSheet1.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtCompleteCompliance.Rows.Count > 0)
                        {
                            exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData, false);
                        }
                        exWorkSheet1.Cells["A5"].Value = "SrNo";
                        exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet1.Cells["B5"].Value = "UserID";
                        exWorkSheet1.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet1.Cells["C5"].Value = "User Name";
                        exWorkSheet1.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells["D5"].Value = "Location";
                        exWorkSheet1.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells["E5"].Value = "Role";
                        exWorkSheet1.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue1 = 70;
                        bool CheckAsciiVal1 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue1 == 91)
                                {
                                    CheckAsciiVal1 = true;
                                    AsciiValue1 = 65;
                                }
                                if (CheckAsciiVal1)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue1);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue1).ToString();
                                }

                                exWorkSheet1.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet1.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet1.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet1.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue1++;
                            }
                            else
                            {
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Bold = true;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Size = 12;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].AutoFitColumns(15);
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue1++;
                            }
                        }

                        int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet1.Cells[5, 1, count1, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        using (ExcelRange col = exWorkSheet1.Cells[5, 6, count1, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        #region After Due Date Complate

                        DataView view1 = new System.Data.DataView(dtAfterDueDate);
                        DataTable ExcelData1 = null;
                        ExcelData1 = view1.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "BeforeDays", "ThreeDays", "SevenDays", "FifteenDays", "ThirtyDays", "MoreThanThirtyDays");

                        exWorkSheet2.Cells["A1:B1"].Merge = true;
                        exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet2.Cells["C1:D1"].Merge = true;
                        exWorkSheet2.Cells["C1"].Value = cname;

                        exWorkSheet2.Cells["A2:B2"].Merge = true;
                        exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A2"].Value = "Report Name:";

                        exWorkSheet2.Cells["C2:D2"].Merge = true;
                        exWorkSheet2.Cells["C2"].Value = "Ageing of System Updation (From Due Date)";

                        exWorkSheet2.Cells["A3:B3"].Merge = true;
                        exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet2.Cells["C3:D3"].Merge = true;
                        exWorkSheet2.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtAfterDueDate.Rows.Count > 0)
                        {
                            exWorkSheet2.Cells["A6"].LoadFromDataTable(ExcelData1, false);
                        }
                        exWorkSheet2.Cells["A5"].Value = "SrNo";
                        exWorkSheet2.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet2.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["B5"].Value = "UserID";
                        exWorkSheet2.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet2.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["C5"].Value = "User Name";
                        exWorkSheet2.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet2.Cells["D5"].Value = "Location";
                        exWorkSheet2.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet2.Cells["E5"].Value = "Role";
                        exWorkSheet2.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["F5"].Value = "On Or Before Due Date";
                        exWorkSheet2.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["F5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["G5"].Value = "1 - 3 Days";
                        exWorkSheet2.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["G5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["G5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["H5"].Value = "4 - 7 Days";
                        exWorkSheet2.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["H5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["H5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["I5"].Value = "8 - 15 Days";
                        exWorkSheet2.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["I5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["I5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["J5"].Value = "16 - 30 Days";
                        exWorkSheet2.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["J5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["J5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["K5"].Value = "> 30 days";
                        exWorkSheet2.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["K5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["K5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["K5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int count2 = Convert.ToInt32(ExcelData1.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet2.Cells[5, 1, count2, 11])
                        {
                            col.Style.WrapText = true;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }
                        #endregion

                        #region Open/Overdue Compliance
                        DataView view6 = new System.Data.DataView(dtLogingDatailOpen);
                        DataTable ExcelData6 = null;
                        ExcelData6 = view6.ToTable();
                        exWorkSheet6.Cells["A1:B1"].Merge = true;
                        exWorkSheet6.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet6.Cells["C1:D1"].Merge = true;
                        exWorkSheet6.Cells["C1"].Value = cname;

                        exWorkSheet6.Cells["A2:B2"].Merge = true;
                        exWorkSheet6.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A2"].Value = "Report Name:";

                        exWorkSheet6.Cells["C2:D2"].Merge = true;
                        exWorkSheet6.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                        exWorkSheet6.Cells["A3:B3"].Merge = true;
                        exWorkSheet6.Cells["A3"].Value = "Note:";
                        exWorkSheet6.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet6.Cells["C3:G3"].Merge = true;
                        exWorkSheet6.Cells["C3"].Value = "Month Wise  Summary of " + " 'Over due Compliance'" + " as against compliances due in that Period";

                        exWorkSheet6.Cells["A4:B4"].Merge = true;
                        exWorkSheet6.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet6.Cells["C4:D4"].Merge = true;
                        exWorkSheet6.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtLogingDatailOpen.Rows.Count > 0)
                        {
                            exWorkSheet6.Cells["A6"].LoadFromDataTable(ExcelData6, false);
                        }
                        exWorkSheet6.Cells["A5"].Value = "SrNo";
                        exWorkSheet6.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet6.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet6.Cells["B5"].Value = "UserID";
                        exWorkSheet6.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet6.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet6.Cells["C5"].Value = "User Name";
                        exWorkSheet6.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet6.Cells["D5"].Value = "Location";
                        exWorkSheet6.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet6.Cells["E5"].Value = "Role";
                        exWorkSheet6.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue6 = 70;
                        bool CheckAsciiVal6 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue6 == 91)
                                {
                                    CheckAsciiVal6 = true;
                                    AsciiValue6 = 65;
                                }
                                if (CheckAsciiVal6)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue6);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue6).ToString();
                                }

                                exWorkSheet6.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet6.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet6.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet6.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet6.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue6++;
                            }
                            else
                            {
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Bold = true;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Size = 12;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].AutoFitColumns(15);
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue6++;
                            }
                        }

                        int count6 = Convert.ToInt32(ExcelData6.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet6.Cells[5, 1, count6, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }

                        using (ExcelRange col = exWorkSheet6.Cells[5, 6, count6, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        #region Compliance Due Date Detail of Year Reviewer
                        DataView view5 = new System.Data.DataView(dtReviewerData);
                        DataTable ExcelData5 = null;
                        ExcelData5 = view5.ToTable();

                        exWorkSheet5.Cells["A1:B1"].Merge = true;
                        exWorkSheet5.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet5.Cells["C1:D1"].Merge = true;
                        exWorkSheet5.Cells["C1"].Value = cname;

                        exWorkSheet5.Cells["A2:B2"].Merge = true;
                        exWorkSheet5.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A2"].Value = "Report Name:";

                        exWorkSheet5.Cells["C2:D2"].Merge = true;
                        exWorkSheet5.Cells["C2"].Value = "User Wise Performance Report (Reviewer)";

                        exWorkSheet5.Cells["A3:B3"].Merge = true;
                        exWorkSheet5.Cells["A3"].Value = "Note:";
                        exWorkSheet5.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet5.Cells["C3:G3"].Merge = true;
                        exWorkSheet5.Cells["C3"].Value = "Month Wise Summary of Compliance reviewed as against compliances Submitted in that Period";

                        exWorkSheet5.Cells["A4:B4"].Merge = true;
                        exWorkSheet5.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet5.Cells["C4:D4"].Merge = true;
                        exWorkSheet5.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtReviewerData.Rows.Count > 0)
                        {
                            exWorkSheet5.Cells["A6"].LoadFromDataTable(ExcelData5, false);
                        }
                        exWorkSheet5.Cells["A5"].Value = "SrNo";
                        exWorkSheet5.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet5.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet5.Cells["B5"].Value = "UserID";
                        exWorkSheet5.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet5.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet5.Cells["C5"].Value = "User Name";
                        exWorkSheet5.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet5.Cells["D5"].Value = "Location";
                        exWorkSheet5.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet5.Cells["E5"].Value = "Role";
                        exWorkSheet5.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue2 = 70;
                        bool CheckAsciiVal2 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue2 == 91)
                                {
                                    CheckAsciiVal2 = true;
                                    AsciiValue2 = 65;
                                }
                                if (CheckAsciiVal2)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue2);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue2).ToString();
                                }

                                exWorkSheet5.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet5.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet5.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet5.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue2++;
                            }
                            else
                            {
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Bold = true;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Size = 12;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].AutoFitColumns(15);
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue2++;
                            }
                        }

                        int count5 = Convert.ToInt32(ExcelData5.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet5.Cells[5, 1, count5, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }

                        using (ExcelRange col = exWorkSheet5.Cells[5, 6, count5, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        string locatioName = cname + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                        Response.AddHeader("content-disposition", "attachment;filename=UsageReport-" + locatioName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    #endregion 
                }
                else if (ddlComplianceType.SelectedValue == "1")
                {
                    #region Internal
                    DateTime fromDatePeroid = new DateTime();
                    DateTime toDatePeroid = new DateTime();
                    DateTime dateResult;
                    DateTime dateResult1;

                    if (DateTime.TryParse(txtStartDate.Text, out dateResult))
                    {
                        if (DateTime.TryParse(txtEndDate.Text, out dateResult1))
                        {
                            fromDatePeroid = Convert.ToDateTime(txtStartDate.Text);
                            toDatePeroid = Convert.ToDateTime(txtEndDate.Text);
                            if (fromDatePeroid > toDatePeroid)
                            {
                                cvDuplicateEntry.ErrorMessage = "From date should not be greater than To Date.";
                                cvDuplicateEntry.IsValid = false;
                                DateTime date = DateTime.Now;
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                                return;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.ErrorMessage = "From Date and To Date should not be blank.";
                            cvDuplicateEntry.IsValid = false;
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            return;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "From Date and To Date should not be blank.";
                        cvDuplicateEntry.IsValid = false;
                        DateTime date = DateTime.Now;
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                        return;
                    }

                    var NoOfMonth = MonthDiff(toDatePeroid, fromDatePeroid);
                    string user_Roles = AuthenticationHelper.Role;
                    Branchlist.Clear();
                    GetAllHierarchy(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(tvFilterLocation.SelectedValue));

                    List<SP_UsageInternalReports_Result> Master_SP_UsageReports = new List<SP_UsageInternalReports_Result>();
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (ddlStatus.SelectedValue == "2") //Performer
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageInternalReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), "PER", fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }
                        else if (ddlStatus.SelectedValue == "3") //Reviewer
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageInternalReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), "REV", fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }
                        else if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            Master_SP_UsageReports = (from row in entities.SP_UsageInternalReports(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), user_Roles, fromDatePeroid, toDatePeroid)
                                                      select row).ToList();
                        }

                    }
                    var usersfrom = (from row in Master_SP_UsageReports
                                     select row.UserID).ToList();
                    if (ddlStatus.SelectedValue == "3") //Reviewer
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var userhier = (from NCCP in entities.InternalComplianceAssignments
                                            join CF in entities.ComplianceInstances
                                            on NCCP.InternalComplianceInstanceID equals CF.ID

                                            where NCCP.UserID == AuthenticationHelper.UserID
                                            select NCCP.InternalComplianceInstanceID).Distinct().ToList();

                            usersfrom = (from NCCP in entities.InternalComplianceAssignments
                                         join CF in entities.InternalComplianceInstances
                                         on NCCP.InternalComplianceInstanceID equals CF.ID

                                         where userhier.Contains(NCCP.InternalComplianceInstanceID)
                                         select NCCP.UserID).Distinct().ToList();
                        }


                    }
                    else if (user_Roles.Contains("MGMT"))
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            usersfrom = (from NCCP in entities.InternalComplianceAssignments
                                         join CF in entities.InternalComplianceInstances
                                         on NCCP.InternalComplianceInstanceID equals CF.ID
                                         join CF1 in entities.EntitiesAssignmentInternals
                                         on CF.CustomerBranchID equals CF1.BranchID
                                         where CF1.UserID == AuthenticationHelper.UserID
                                         select NCCP.UserID).Distinct().ToList();
                        }
                    }



                    #region Define DataTable
                    DataTable dtLogingDatail = new DataTable();
                    DataTable dtLastLogingDatails = new DataTable();
                    DataTable dtLogingDatailOpen = new DataTable();
                    DataTable dtCompleteCompliance = new DataTable();
                    DataTable dtAfterDueDate = new DataTable();
                    DataTable dtReviewerData = new DataTable();

                    dtLogingDatail.Columns.Add("SLNO", typeof(string));
                    dtLogingDatail.Columns.Add("UserID", typeof(string));
                    dtLogingDatail.Columns.Add("UserName", typeof(string));
                    dtLogingDatail.Columns.Add("Location", typeof(string));
                    dtLogingDatail.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtLogingDatail.Columns.Add("Month" + i, typeof(string));
                    }

                    //Last Loging
                    dtLastLogingDatails.Columns.Add("SLNO", typeof(string));
                    dtLastLogingDatails.Columns.Add("UserID", typeof(string));
                    dtLastLogingDatails.Columns.Add("UserName", typeof(string));
                    dtLastLogingDatails.Columns.Add("Location", typeof(string));
                    dtLastLogingDatails.Columns.Add("Role", typeof(string));
                    dtLastLogingDatails.Columns.Add("LastLogingTime", typeof(DateTime));

                    //Open
                    dtLogingDatailOpen.Columns.Add("SLNO", typeof(string));
                    dtLogingDatailOpen.Columns.Add("UserID", typeof(string));
                    dtLogingDatailOpen.Columns.Add("UserName", typeof(string));
                    dtLogingDatailOpen.Columns.Add("Location", typeof(string));
                    dtLogingDatailOpen.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtLogingDatailOpen.Columns.Add("Month" + i, typeof(string));
                    }

                    //After
                    dtCompleteCompliance.Columns.Add("SLNO", typeof(string));
                    dtCompleteCompliance.Columns.Add("UserID", typeof(string));
                    dtCompleteCompliance.Columns.Add("UserName", typeof(string));
                    dtCompleteCompliance.Columns.Add("Location", typeof(string));
                    dtCompleteCompliance.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtCompleteCompliance.Columns.Add("Month" + i, typeof(string));
                    }


                    //Download
                    dtAfterDueDate.Columns.Add("SLNO", typeof(string));
                    dtAfterDueDate.Columns.Add("UserID", typeof(string));
                    dtAfterDueDate.Columns.Add("UserName", typeof(string));
                    dtAfterDueDate.Columns.Add("Location", typeof(string));
                    dtAfterDueDate.Columns.Add("Role", typeof(string));
                    dtAfterDueDate.Columns.Add("BeforeDays", typeof(string));
                    dtAfterDueDate.Columns.Add("ThreeDays", typeof(string));
                    dtAfterDueDate.Columns.Add("SevenDays", typeof(string));
                    dtAfterDueDate.Columns.Add("FifteenDays", typeof(string));
                    dtAfterDueDate.Columns.Add("ThirtyDays", typeof(string));
                    dtAfterDueDate.Columns.Add("MoreThanThirtyDays", typeof(string));

                    dtReviewerData.Columns.Add("SLNO", typeof(string));
                    dtReviewerData.Columns.Add("UserID", typeof(string));
                    dtReviewerData.Columns.Add("UserName", typeof(string));
                    dtReviewerData.Columns.Add("Location", typeof(string));
                    dtReviewerData.Columns.Add("Role", typeof(string));
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        dtReviewerData.Columns.Add("Month" + i, typeof(string));
                    }

                    #endregion

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        #region Compliance Complete within Due Days
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> Performerstatus = new List<int>();
                            Performerstatus.Add(2);
                            Performerstatus.Add(4);
                            Performerstatus.Add(7);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;


                            if (ddlStatus.SelectedValue == "2") //Performer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                                usersfrom = (from row in FilteredData
                                             select row.UserID).ToList();
                            }
                            else if (ddlStatus.SelectedValue == "3") //Reviewer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                                usersfrom = (from row in FilteredData
                                             select row.UserID).ToList();
                            }
                            else
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }

                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            }

                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> PerformerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }


                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate
                                                          select data).ToList();
                                        var Mont1hToltal = DataMonth1.Select(e => e.ScheduledOn).Count();
                                        var Month1ActualData = DataMonth1.Where(e => e.submittedcnt != null).Count();
                                        // var Month1ActualData = DataMonth1.Where(e => Performerstatus.Contains(e.ComplianceStatusID)).Count();
                                        PerformerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtCompleteCompliance.Rows.Add(Count, item, UserName, UserLocation, Role);

                                    int p = 0;
                                    for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                    {
                                        dtCompleteCompliance.Rows[RowCount1][j] = PerformerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion

                        #region After Due date Completed

                        List<int> StatusList = new List<int>();
                        StatusList.Add(3);
                        StatusList.Add(5);
                        List<int> Performerstatus1 = new List<int>();
                        Performerstatus1.Add(2);
                        Performerstatus1.Add(4);
                        Performerstatus1.Add(7);

                        List<int> notinstatusid = new List<int>();
                        notinstatusid.Add(10);
                        notinstatusid.Add(11);
                        notinstatusid.Add(12);
                        notinstatusid.Add(13);
                        notinstatusid.Add(14);
                        if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;

                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }



                            FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            var DistinctUserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in DistinctUserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    string BeforeDays = string.Empty;
                                    string ThreeDays = string.Empty;
                                    string SevenDays = string.Empty;
                                    string FifteenDays = string.Empty;
                                    string ThirtyDays = string.Empty;
                                    string MoreThanThirtyDays = string.Empty;
                                    Count++;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }

                                    #region After Due Date Data Assign
                                    // On Or Before Due Date
                                    var DataMonthBeforeDue = (from data in FilteredData
                                                              where data.UserID == item
                                                              //&& data.closeDate <= data.ScheduledOn 
                                                              //&& Performerstatus1.Contains(data.ComplianceStatusID)
                                                              select data).ToList();
                                    var Month1ActualData = DataMonthBeforeDue.Where(e => e.submittedcnt != null).Count();

                                    // BeforeDays = Convert.ToString(DataMonthBeforeDue.Select(e => e.ScheduledOn).Count());
                                    BeforeDays = Convert.ToString(Month1ActualData);
                                    // In 3 Days
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate > data.ScheduledOn
                                                      && data.closeDate <= data.ScheduledOn.AddDays(3)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    ThreeDays = Convert.ToString(DataMonth1.Select(e => e.ScheduledOn).Count());

                                    // In 7 Days
                                    var DataMonth2 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(3)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(7)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    SevenDays = Convert.ToString(DataMonth2.Select(e => e.ScheduledOn).Count());

                                    // In 15 Days
                                    var DataMonth3 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(7)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(15)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    FifteenDays = Convert.ToString(DataMonth3.Select(e => e.ScheduledOn).Count());

                                    // In 30 Days
                                    var DataMonth4 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(15)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(30)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    ThirtyDays = Convert.ToString(DataMonth4.Select(e => e.ScheduledOn).Count());

                                    // More Than 30 Days
                                    var DataMonth5 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.closeDate >= data.ScheduledOn.AddDays(30)
                                                      && data.closeDate <= data.ScheduledOn.AddDays(365)
                                                      && !notinstatusid.Contains(data.ComplianceStatusID)
                                                      select data).ToList();
                                    MoreThanThirtyDays = Convert.ToString(DataMonth5.Select(e => e.ScheduledOn).Count());

                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtAfterDueDate.Rows.Add(Count, item, UserName, UserLocation, Role, BeforeDays, ThreeDays, SevenDays, FifteenDays, ThirtyDays, MoreThanThirtyDays);
                                }
                            }
                        }

                        #endregion

                        #region Open Compliance
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> Openstatus = new List<int>();
                            Openstatus.Add(1);
                            Openstatus.Add(10);
                            Openstatus.Add(11);
                            Openstatus.Add(12);
                            Openstatus.Add(13);
                            Openstatus.Add(14);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;
                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }
                            FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> PerformerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }


                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate
                                                          select data).ToList();
                                        var MonthOpenCount = DataMonth1.Where(e => Openstatus.Contains(e.ComplianceStatusID)).Count();
                                        PerformerMonthList.Add(Convert.ToString(MonthOpenCount));
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtLogingDatailOpen.Rows.Add(Count, item, UserName, UserLocation, Role);

                                    int p = 0;
                                    for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                    {
                                        dtLogingDatailOpen.Rows[RowCount1][j] = PerformerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion

                        #region Reviewer Compliance Complete
                        if (user_Roles.Contains("MGMT"))
                        {
                            List<int> StatusList1 = new List<int>();
                            StatusList1.Add(2);
                            StatusList1.Add(3);
                            StatusList1.Add(4);
                            StatusList1.Add(5);
                            StatusList1.Add(11);
                            List<int> StatusListClose = new List<int>();
                            StatusListClose.Add(4);
                            StatusListClose.Add(5);
                            StatusListClose.Add(11);
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = Master_SP_UsageReports;
                            if (ddlStatus.SelectedValue == "1")
                            {
                                if (Branchlist.Count > 0)
                                {
                                    FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                                }
                            }
                            //var ReviewerData = FilteredData.Where(entry => entry.RoleID == 4).ToList();

                            if (ddlStatus.SelectedValue == "2") //Performer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                            }
                            else if (ddlStatus.SelectedValue == "3") //Reviwer
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                                FilteredData = FilteredData.Where(entry => entry.UserID == AuthenticationHelper.UserID).ToList();
                            }
                            else
                            {
                                FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                            }
                            UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                            int Count = 0;
                            int RowCount1 = 0;
                            if (FilteredData.Count > 0)
                            {
                                foreach (var item in UserList)
                                {
                                    string Location = string.Empty;
                                    string UserName = string.Empty;
                                    string Role = string.Empty;
                                    List<string> ReviewerMonthList = new List<string>();
                                    Count++;
                                    entities.Database.CommandTimeout = 180;
                                    var objData = (from row in FilteredData
                                                   join row1 in entities.Users
                                                   on row.UserID equals row1.ID
                                                   join role in entities.Roles
                                                   on row.RoleID equals role.ID
                                                   join cust in entities.Customers
                                                   on row.CustomerID equals cust.ID
                                                   where row.UserID == item
                                                   select new UsageReport_Data()
                                                   {
                                                       CustomerName = cust.Name,
                                                       UserName = row1.FirstName + " " + row1.LastName,
                                                       Role = role.Name
                                                   }).FirstOrDefault();

                                    if (objData != null)
                                    {
                                        UserName = objData.UserName;
                                    }

                                    #region 12 month Data Assing
                                    for (int i = 0; i < NoOfMonth; i++)
                                    {
                                        DateTime now = fromDatePeroid.AddMonths(i);
                                        DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                        DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                        var DataMonth1 = (from data in FilteredData
                                                          where
                                                          data.UserID == item
                                                          && data.ScheduledOn >= startDate
                                                          && data.ScheduledOn <= endDate 
                                                          select data).ToList(); 

                                        var Mont1hToltal = DataMonth1.Where(e => StatusList1.Contains(e.ComplianceStatusID)).Count();
                                        var Month1ActualData = DataMonth1.Where(e => StatusListClose.Contains(e.ComplianceStatusID)).Count();
                                        ReviewerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                    }
                                    #endregion

                                    Role = GetRole(item);
                                    var UserLocation = (from data in entities.Users
                                                        join row in entities.CustomerBranches
                                                        on data.CustomerBranchID equals row.ID
                                                        where data.ID == item
                                                        select row.Name).FirstOrDefault();
                                    dtReviewerData.Rows.Add(Count, item, UserName, UserLocation, Role);
                                    int p = 0;
                                    for (int j = 5; j < dtReviewerData.Columns.Count; j++)
                                    {
                                        dtReviewerData.Rows[RowCount1][j] = ReviewerMonthList[p];
                                        p++;
                                    }
                                    RowCount1++;
                                }
                            }
                        }
                        #endregion
                    }


                    #region Loging Details
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (user_Roles.Contains("MGMT"))
                        {
                            int Count = 0;
                            entities.Database.CommandTimeout = 180;
                            var FilteredData = (from row in entities.Sp_AVACOM_LoginDetailHistory(Convert.ToInt32(AuthenticationHelper.CustomerID), fromDatePeroid, toDatePeroid)
                                                select row).ToList();

                            var UserList12 = FilteredData.Where(e => usersfrom.Contains(e.UserID)).ToList();
                            var UserList1 = UserList12.Select(e => e.UserID).Distinct().ToList();
                            int RowCount1 = 0;
                            foreach (var item in UserList1)//UserList
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> LogingMonthList = new List<string>();
                                Count++;
                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.LastLoginDate >= startDate
                                                      && data.LastLoginDate <= endDate
                                                      select data).ToList();
                                    var Mont1hToltal = DataMonth1.Select(e => e.LastLoginDate).Count();
                                    LogingMonthList.Add(Convert.ToString(Mont1hToltal));
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();

                                var Userlist = FilteredData.Where(e => e.UserID == item).FirstOrDefault();
                                if (Userlist != null)
                                {
                                    UserName = Userlist.UserName;
                                }
                                if (string.IsNullOrEmpty(Role))
                                {
                                    using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                    {
                                        Role = (from row in entities1.Users
                                                join row1 in entities1.Roles
                                                on row.RoleID equals row1.ID
                                                where row.ID == item
                                                select row1.Name).FirstOrDefault();
                                    }
                                    if (Role == "Company Admin" || Role == "Management")
                                    {
                                    }
                                    else
                                    {
                                        Role = string.Empty;
                                    }
                                }

                                dtLogingDatail.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtLogingDatail.Columns.Count; j++)
                                {
                                    dtLogingDatail.Rows[RowCount1][j] = Convert.ToString(LogingMonthList[p]);
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion

                    #region Last Login Details
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (user_Roles.Contains("MGMT"))
                        {
                            entities.Database.CommandTimeout = 180;
                            var FilteredData1 = (from row in entities.Sp_AVACOM_LoginDetailHistory_LastLogin(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                 select row).ToList();
                             
                            var FilteredData2 = FilteredData1.Where(e => usersfrom.Contains(e.UserID)).ToList(); 

                            int Count = 1;
                            foreach (var item in FilteredData2)
                            {
                                var Role = GetRole(item.UserID);
                                if (string.IsNullOrEmpty(Role))
                                {
                                    using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                    {
                                        Role = (from row in entities1.Users
                                                join row1 in entities1.Roles
                                                on row.RoleID equals row1.ID
                                                where row.ID == item.UserID
                                                select row1.Name).FirstOrDefault();
                                    }
                                    if (Role == "Company Admin" || Role == "Management")
                                    {
                                    }
                                    else
                                    {
                                        Role = string.Empty;
                                    }
                                }
                                dtLastLogingDatails.Rows.Add(Count++, item.UserID, item.UserName, item.Location, Role, item.LastLoginTime);
                            }
                        }
                    }
                    #endregion

                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                        if (user_Roles.Contains("MGMT"))
                        {
                            ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Number of User logins in Month");
                            ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Last Login Date Report");

                            #region Each Month Loging Details
                            DataView view3 = new System.Data.DataView(dtLogingDatail);
                            DataTable ExcelData3 = null; 
                            ExcelData3 = view3.ToTable();

                            exWorkSheet3.Cells["A1:B1"].Merge = true;
                            exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet3.Cells["C1:D1"].Merge = true;
                            exWorkSheet3.Cells["C1"].Value = cname;

                            exWorkSheet3.Cells["A2:B2"].Merge = true;
                            exWorkSheet3.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A2"].Value = "Report Name:";

                            exWorkSheet3.Cells["C2:D2"].Merge = true;
                            exWorkSheet3.Cells["C2"].Value = "Number of User logins in Month";

                            exWorkSheet3.Cells["A3:B3"].Merge = true;
                            exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A3"].Value = "Report Generated On:";

                            exWorkSheet3.Cells["C3:D3"].Merge = true;
                            exWorkSheet3.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                            if (dtLogingDatail.Rows.Count > 0)
                            {
                                exWorkSheet3.Cells["A6"].LoadFromDataTable(ExcelData3, false);
                            }
                            exWorkSheet3.Cells["A5"].Value = "SrNo";
                            exWorkSheet3.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["A5"].AutoFitColumns(10);
                            exWorkSheet3.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet3.Cells["B5"].Value = "UserID";
                            exWorkSheet3.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["B5"].AutoFitColumns(15);
                            exWorkSheet3.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet3.Cells["C5"].Value = "User Name";
                            exWorkSheet3.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["C5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet3.Cells["D5"].Value = "Location";
                            exWorkSheet3.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["D5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet3.Cells["E5"].Value = "Role";
                            exWorkSheet3.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet3.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet3.Cells["E5"].AutoFitColumns(25);
                            exWorkSheet3.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet3.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet3.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet3.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet3.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            int AsciiValue = 70;
                            bool CheckAsciiVal = false;
                            for (int i = 0; i < NoOfMonth; i++)
                            {
                                DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                                var ColVal = string.Empty;
                                if (NoOfMonth > 21)
                                {
                                    if (AsciiValue == 91)
                                    {
                                        CheckAsciiVal = true;
                                        AsciiValue = 65;
                                    }
                                    if (CheckAsciiVal)
                                    {
                                        ColVal = "A" + Convert.ToChar(AsciiValue);
                                    }
                                    else
                                    {
                                        ColVal = Convert.ToChar(AsciiValue).ToString();
                                    }

                                    exWorkSheet3.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells[ColVal + "5"].AutoFitColumns(15);
                                    exWorkSheet3.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    AsciiValue++;
                                }
                                else
                                {
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].AutoFitColumns(15);
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                    AsciiValue++;
                                }
                            }

                            int count3 = Convert.ToInt32(ExcelData3.Rows.Count) + 5;
                            using (ExcelRange col = exWorkSheet3.Cells[5, 1, count3, NoOfMonth + 5])
                            {
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top; 
                            }
                            using (ExcelRange col = exWorkSheet3.Cells[5, 6, count3, NoOfMonth + 5])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            #endregion

                            #region Last Login

                            DataView view4 = new System.Data.DataView(dtLastLogingDatails);
                            DataTable ExcelData4 = null;
                            ExcelData4 = view4.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "LastLogingTime");

                            foreach (DataRow item in ExcelData4.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["LastLogingTime"])))
                                {
                                    item["LastLogingTime"] = Convert.ToDateTime(item["LastLogingTime"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet4.Cells["A1:B1"].Merge = true;
                            exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A1"].Value = "Customer Name:";

                            exWorkSheet4.Cells["C1:D1"].Merge = true;
                            exWorkSheet4.Cells["C1"].Value = cname;

                            exWorkSheet4.Cells["A2:B2"].Merge = true;
                            exWorkSheet4.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A2"].Value = "Report Name:";

                            exWorkSheet4.Cells["C2:D2"].Merge = true;
                            exWorkSheet4.Cells["C2"].Value = "Last Login Date Report";

                            exWorkSheet4.Cells["A3:B3"].Merge = true;
                            exWorkSheet4.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A3"].Value = "Report Generated On:";

                            exWorkSheet4.Cells["C3:D3"].Merge = true;
                            exWorkSheet4.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                            if (dtLastLogingDatails.Rows.Count > 0)
                            {
                                exWorkSheet4.Cells["A6"].LoadFromDataTable(ExcelData4, false);
                            }
                            exWorkSheet4.Cells["A5"].Value = "SrNo";
                            exWorkSheet4.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["A5"].AutoFitColumns(10);
                            exWorkSheet4.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["B5"].Value = "UserID";
                            exWorkSheet4.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["B5"].AutoFitColumns(15);
                            exWorkSheet4.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["C5"].Value = "User Name";
                            exWorkSheet4.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["C5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet4.Cells["D5"].Value = "Location";
                            exWorkSheet4.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["D5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet4.Cells["E5"].Value = "Role";
                            exWorkSheet4.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["E5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            exWorkSheet4.Cells["F5"].Value = "Last Login";
                            exWorkSheet4.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet4.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet4.Cells["F5"].AutoFitColumns(25);
                            exWorkSheet4.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet4.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet4.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet4.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            exWorkSheet4.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            int count4 = Convert.ToInt32(ExcelData4.Rows.Count) + 5;
                            using (ExcelRange col = exWorkSheet4.Cells[5, 1, count4, 6])
                            {
                                col.Style.WrapText = true;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            using (ExcelRange col = exWorkSheet4.Cells[5, 5, count4, 6])
                            {
                                col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            #endregion
                        }
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Performance Report - In Time (P)");
                        ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Ageing of System Updation");
                        ExcelWorksheet exWorkSheet6 = exportPackge.Workbook.Worksheets.Add("Performance Report- OverDue (P)");
                        ExcelWorksheet exWorkSheet5 = exportPackge.Workbook.Worksheets.Add("Performance Report (Reviewer)");

                        #region Compliance Due Date Detail of Year
                        DataView view = new System.Data.DataView(dtCompleteCompliance);
                        DataTable ExcelData = null;
                        ExcelData = view.ToTable();
                        exWorkSheet1.Cells["A1:B1"].Merge = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet1.Cells["C1:D1"].Merge = true;
                        exWorkSheet1.Cells["C1"].Value = cname;

                        exWorkSheet1.Cells["A2:B2"].Merge = true;
                        exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A2"].Value = "Report Name:";

                        exWorkSheet1.Cells["C2:D2"].Merge = true;
                        exWorkSheet1.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                        exWorkSheet1.Cells["A3:B3"].Merge = true;
                        exWorkSheet1.Cells["A3"].Value = "Note:";
                        exWorkSheet1.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet1.Cells["C3:G3"].Merge = true;
                        exWorkSheet1.Cells["C3"].Value = "Month Wise Summary of Compliance completed " + " 'In Time'" + " as against compliances due in that Period";

                        exWorkSheet1.Cells["A4:B4"].Merge = true;
                        exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet1.Cells["C4:D4"].Merge = true;
                        exWorkSheet1.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtCompleteCompliance.Rows.Count > 0)
                        {
                            exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData, false);
                        }
                        exWorkSheet1.Cells["A5"].Value = "SrNo";
                        exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet1.Cells["B5"].Value = "UserID";
                        exWorkSheet1.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet1.Cells["C5"].Value = "User Name";
                        exWorkSheet1.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells["D5"].Value = "Location";
                        exWorkSheet1.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet1.Cells["E5"].Value = "Role";
                        exWorkSheet1.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet1.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue1 = 70;
                        bool CheckAsciiVal1 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue1 == 91)
                                {
                                    CheckAsciiVal1 = true;
                                    AsciiValue1 = 65;
                                }
                                if (CheckAsciiVal1)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue1);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue1).ToString();
                                }

                                exWorkSheet1.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet1.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet1.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet1.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue1++;
                            }
                            else
                            {
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Bold = true;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Size = 12;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].AutoFitColumns(15);
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue1++;
                            }
                        }

                        int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet1.Cells[5, 1, count1, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top; 
                        }
                        using (ExcelRange col = exWorkSheet1.Cells[5, 6, count1, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        #region After Due Date Complate

                        DataView view1 = new System.Data.DataView(dtAfterDueDate);
                        DataTable ExcelData1 = null;
                        ExcelData1 = view1.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "BeforeDays", "ThreeDays", "SevenDays", "FifteenDays", "ThirtyDays", "MoreThanThirtyDays");

                        exWorkSheet2.Cells["A1:B1"].Merge = true;
                        exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet2.Cells["C1:D1"].Merge = true;
                        exWorkSheet2.Cells["C1"].Value = cname;

                        exWorkSheet2.Cells["A2:B2"].Merge = true;
                        exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A2"].Value = "Report Name:";

                        exWorkSheet2.Cells["C2:D2"].Merge = true;
                        exWorkSheet2.Cells["C2"].Value = "Ageing of System Updation (From Due Date)";

                        exWorkSheet2.Cells["A3:B3"].Merge = true;
                        exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet2.Cells["C3:D3"].Merge = true;
                        exWorkSheet2.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtAfterDueDate.Rows.Count > 0)
                        {
                            exWorkSheet2.Cells["A6"].LoadFromDataTable(ExcelData1, false);
                        }
                        exWorkSheet2.Cells["A5"].Value = "SrNo";
                        exWorkSheet2.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet2.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["B5"].Value = "UserID";
                        exWorkSheet2.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet2.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["C5"].Value = "User Name";
                        exWorkSheet2.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet2.Cells["D5"].Value = "Location";
                        exWorkSheet2.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet2.Cells["E5"].Value = "Role";
                        exWorkSheet2.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["F5"].Value = "On Or Before Due Date";
                        exWorkSheet2.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["F5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["G5"].Value = "1 - 3 Days";
                        exWorkSheet2.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["G5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["G5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["H5"].Value = "4 - 7 Days";
                        exWorkSheet2.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["H5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["H5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["I5"].Value = "8 - 15 Days";
                        exWorkSheet2.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["I5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["I5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["J5"].Value = "16 - 30 Days";
                        exWorkSheet2.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["J5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["J5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet2.Cells["K5"].Value = "> 30 days";
                        exWorkSheet2.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["K5"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["K5"].AutoFitColumns(25);
                        exWorkSheet2.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet2.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet2.Cells["K5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet2.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int count2 = Convert.ToInt32(ExcelData1.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet2.Cells[5, 1, count2, 11])
                        {
                            col.Style.WrapText = true;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }
                        #endregion

                        #region Open/Overdue Compliance
                        DataView view6 = new System.Data.DataView(dtLogingDatailOpen);
                        DataTable ExcelData6 = null;
                        ExcelData6 = view6.ToTable();
                        exWorkSheet6.Cells["A1:B1"].Merge = true;
                        exWorkSheet6.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet6.Cells["C1:D1"].Merge = true;
                        exWorkSheet6.Cells["C1"].Value = cname;

                        exWorkSheet6.Cells["A2:B2"].Merge = true;
                        exWorkSheet6.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A2"].Value = "Report Name:";

                        exWorkSheet6.Cells["C2:D2"].Merge = true;
                        exWorkSheet6.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                        exWorkSheet6.Cells["A3:B3"].Merge = true;
                        exWorkSheet6.Cells["A3"].Value = "Note:";
                        exWorkSheet6.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet6.Cells["C3:G3"].Merge = true;
                        exWorkSheet6.Cells["C3"].Value = "Month Wise  Summary of " + " 'Over due Compliance'" + " as against compliances due in that Period";

                        exWorkSheet6.Cells["A4:B4"].Merge = true;
                        exWorkSheet6.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet6.Cells["C4:D4"].Merge = true;
                        exWorkSheet6.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtLogingDatailOpen.Rows.Count > 0)
                        {
                            exWorkSheet6.Cells["A6"].LoadFromDataTable(ExcelData6, false);
                        }
                        exWorkSheet6.Cells["A5"].Value = "SrNo";
                        exWorkSheet6.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet6.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet6.Cells["B5"].Value = "UserID";
                        exWorkSheet6.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet6.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet6.Cells["C5"].Value = "User Name";
                        exWorkSheet6.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet6.Cells["D5"].Value = "Location";
                        exWorkSheet6.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet6.Cells["E5"].Value = "Role";
                        exWorkSheet6.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet6.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet6.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet6.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet6.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet6.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet6.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet6.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue6 = 70;
                        bool CheckAsciiVal6 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue6 == 91)
                                {
                                    CheckAsciiVal6 = true;
                                    AsciiValue6 = 65;
                                }
                                if (CheckAsciiVal6)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue6);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue6).ToString();
                                }

                                exWorkSheet6.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet6.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet6.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet6.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet6.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet6.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue6++;
                            }
                            else
                            {
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Bold = true;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Size = 12;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].AutoFitColumns(15);
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue6++;
                            }
                        }

                        int count6 = Convert.ToInt32(ExcelData6.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet6.Cells[5, 1, count6, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top; 
                        }

                        using (ExcelRange col = exWorkSheet6.Cells[5, 6, count6, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        #region Compliance Due Date Detail of Year Reviewer
                        DataView view5 = new System.Data.DataView(dtReviewerData);
                        DataTable ExcelData5 = null;
                        ExcelData5 = view5.ToTable();

                        exWorkSheet5.Cells["A1:B1"].Merge = true;
                        exWorkSheet5.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet5.Cells["C1:D1"].Merge = true;
                        exWorkSheet5.Cells["C1"].Value = cname;

                        exWorkSheet5.Cells["A2:B2"].Merge = true;
                        exWorkSheet5.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A2"].Value = "Report Name:";

                        exWorkSheet5.Cells["C2:D2"].Merge = true;
                        exWorkSheet5.Cells["C2"].Value = "User Wise Performance Report (Reviewer)";

                        exWorkSheet5.Cells["A3:B3"].Merge = true;
                        exWorkSheet5.Cells["A3"].Value = "Note:";
                        exWorkSheet5.Cells["A3"].Style.Font.Bold = true;

                        exWorkSheet5.Cells["C3:G3"].Merge = true;
                        exWorkSheet5.Cells["C3"].Value = "Month Wise Summary of Compliance reviewed as against compliances Submitted in that Period";

                        exWorkSheet5.Cells["A4:B4"].Merge = true;
                        exWorkSheet5.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A4"].Value = "Report Generated On:";

                        exWorkSheet5.Cells["C4:D4"].Merge = true;
                        exWorkSheet5.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtReviewerData.Rows.Count > 0)
                        {
                            exWorkSheet5.Cells["A6"].LoadFromDataTable(ExcelData5, false);
                        }
                        exWorkSheet5.Cells["A5"].Value = "SrNo";
                        exWorkSheet5.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet5.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet5.Cells["B5"].Value = "UserID";
                        exWorkSheet5.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet5.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet5.Cells["C5"].Value = "User Name";
                        exWorkSheet5.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet5.Cells["D5"].Value = "Location";
                        exWorkSheet5.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet5.Cells["E5"].Value = "Role";
                        exWorkSheet5.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet5.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet5.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet5.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet5.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet5.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet5.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet5.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue2 = 70;
                        bool CheckAsciiVal2 = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty; ;
                            if (NoOfMonth > 23)
                            {
                                if (AsciiValue2 == 91)
                                {
                                    CheckAsciiVal2 = true;
                                    AsciiValue2 = 65;
                                }
                                if (CheckAsciiVal2)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue2);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue2).ToString();
                                }

                                exWorkSheet5.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet5.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet5.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet5.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue2++;
                            }
                            else
                            {
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Bold = true;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Size = 12;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].AutoFitColumns(15);
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue2++;
                            }
                        }

                        int count5 = Convert.ToInt32(ExcelData5.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet5.Cells[5, 1, count5, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top; 
                        }

                        using (ExcelRange col = exWorkSheet5.Cells[5, 6, count5, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        string locatioName = cname + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                        Response.AddHeader("content-disposition", "attachment;filename=UsageReport-" + locatioName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    #endregion 
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected String GetRole(long UserID)
        {
            try
            {
                String Role = String.Empty;

                int RoleID = -1;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var DistinctRoles = (from row in entities.ComplianceAssignments
                                         where row.UserID == UserID
                                         select row).GroupBy(Entry => Entry.RoleID)
                                         .Select(a => a.FirstOrDefault())
                                         .Select(a => a.RoleID).ToList();

                    if (RoleID != -1)
                        DistinctRoles = DistinctRoles.Where(Entry => Entry == RoleID).ToList();

                    DistinctRoles.ForEach(EachRole =>
                    {
                        var RoleName = (from row in entities.Roles
                                        where row.ID == EachRole
                                        select row.Name).FirstOrDefault();

                        if (RoleName != null)
                        {
                            if (RoleName == "Reviewer1" || RoleName == "Reviewer2")
                                RoleName = "Reviewer";

                            if (!String.IsNullOrEmpty(Role))
                                Role += ", " + RoleName;
                            else
                                Role += RoleName;
                        }
                    });
                }

                return Role;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportAllData();
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public class UsageReport_Data
        {
            public int CustomerID { get; set; }
            public long UserID { get; set; }
            public int RoleID { get; set; }
            public int CustomerBranchID { get; set; }
            public string CustomerName { get; set; }
            public string UserName { get; set; }
            public string CustomerBranchName { get; set; }
            public DateTime ScheduledOn { get; set; }
            public int ComplianceStatusID { get; set; }
            public string Role { get; set; }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
        }
    }
}