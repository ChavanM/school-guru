﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ApproveCompliances : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                BindCategoryActCompliances();
            }
        }

        protected void lstCatagory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverTransactionView catagoryListItem = (RecentApproverTransactionView)e.Item.DataItem;

                var actList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID).Where(entry=>entry.ComplianceCatagoryID==catagoryListItem.ComplianceCatagoryID)
                              .GroupBy(entry1 => entry1.ActID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("repActList");
                LinkButton lbtApprover = (LinkButton)e.Item.FindControl("lbtApprover");
                lbtApprover.Attributes.Add("onclick", string.Format("Confirm('{0}')", catagoryListItem.Catagory));

                if (actList != null)
                {
                    rptModels.DataSource = actList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repActList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverTransactionView actList = (RecentApproverTransactionView)e.Item.DataItem;
                var complinaceList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID).Where(entry => entry.ActID == actList.ActID)
                                     .GroupBy(entry1 => entry1.ComplianceID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("rptCompliancesList");
                if (complinaceList != null)
                {
                    rptModels.DataSource = complinaceList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbtApprover_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                string confirmValue = Request.Form["confirm_value"];

                if (confirmValue != null)
                {
                    if (confirmValue.Equals("Yes"))
                    {
                        int id = Convert.ToInt32(e.CommandArgument);
                        var ApproverTransactionList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID)
                                                     .Where(entry => entry.ComplianceCatagoryID == id).ToList();

                        if (ApproverTransactionList != null)
                        {
                            List<ComplianceTransaction> transactionList = new List<ComplianceTransaction>();
                            foreach (var atl in ApproverTransactionList)
                            {

                                int statusId = atl.ComplianceStatusID == 4 ? 7 : 9;
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceScheduleOnID = atl.ScheduledOnID,
                                    ComplianceInstanceId = atl.ComplianceInstanceID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = statusId,
                                    StatusChangedOn = DateTime.UtcNow,
                                    Remarks = ""
                                };

                                transactionList.Add(transaction);

                            }

                            ComplianceManagement.Business.ComplianceManagement.CreateTransaction(transactionList);

                            lblMessage.Text = "Approved all Compliances of category " + Convert.ToString(e.CommandName);
                            BindCategoryActCompliances();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
           
        }

        private void BindCategoryActCompliances()
        {
            try
            {
                var catagoryList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID)
                                   .GroupBy(entry => entry.ComplianceCatagoryID).Select(en => en.FirstOrDefault()).ToList();
                if (catagoryList != null)
                {
                    rptCatagory.DataSource = catagoryList;
                    rptCatagory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }

   


}