﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CalenderPopup : System.Web.UI.Page
    {
        protected int scheduleonId;
        protected int InstanceId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //dts.Value = Convert.ToString(Request.QueryString["dt"]);
                dts.Value = Convert.ToString(Convert.ToDateTime(Request.QueryString["dt"]));
                scheduleonId = Convert.ToInt32(Request.QueryString["scheduleonId"]);
                InstanceId = Convert.ToInt32(Request.QueryString["InstanceId"]);
                string Interimdays = Convert.ToString(Request.QueryString["Interimdays"]);
                string Type = Convert.ToString(Request.QueryString["CType"]);
                int RoleID = Convert.ToInt32(Request.QueryString["RoleID"]);
                string SLType = Convert.ToString(Request.QueryString["SLType"]);

                if (RoleID == 3 && (Type == "Statutory" || Type == "Event Based"))
                {
                    if (SLType == "LicenseCompliance")
                    {
                        iPerformerFrameLic.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalPerformerLic(" + scheduleonId + "," + InstanceId + "," + "" + ");", true);
                    }
                    else
                    {
                        Session["Interimdays"] = Interimdays;
                        iPerformerFrame.Visible = true;

                        //udcStatusTranscatopn.OpenTransactionPage(scheduleonId, InstanceId);
                        //udcStatusTranscatopn.Visible = true;
                        //udcStatusTranscatopn.OnSaved += (inputForm, args) => { sample(); };
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalPerformer(" + scheduleonId + "," + InstanceId + ");", true);
                    }
                }
                else if (RoleID == 4 && (Type == "Statutory" || Type == "Event Based"))
                {
                    if (SLType == "LicenseCompliance")
                    {
                        iReviewerFrameLic.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalReviewerLic(" + scheduleonId + "," + InstanceId + "," + "" + ");", true);
                    }
                    else
                    {
                        Session["InterimdaysReview"] = Interimdays;
                        iReviewerFrame.Visible = true;
                        //udcReviewerStatusTranscatopn.OpenTransactionPage(scheduleonId, InstanceId);
                        //udcReviewerStatusTranscatopn.Visible = true;
                        //udcReviewerStatusTranscatopn.OnSaved += (inputForm, args) => {  };
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalReviewer(" + scheduleonId + "," + InstanceId + ");", true);
                    }
                }
                else if (RoleID == 3 && Type == "Statutory CheckList")
                {
                    iChecklistPerformerFrame.Visible = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalChecklistPerformer(" + scheduleonId + "," + InstanceId + ");", true);

                    //udcStatusTranscatopnChkList.OpenTransactionPage(scheduleonId, InstanceId);
                    //udcStatusTranscatopnChkList.Visible = true;
                }
                //else if (RoleID == 4 && Type == "Statutory CheckList")
                //{
                //    udcStatusTranscatopn.OpenTransactionPage(scheduleonId, InstanceId);
                //}
                else if (RoleID == 3 && Type == "Internal")
                {
                    if (SLType == "LicenseCompliance")
                    {
                        iInternalPerformerFrameLic.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPerformerLic(" + scheduleonId + "," + InstanceId + "," + "" + ");", true);
                    }
                    else
                    {
                        //udcInternalPerformerStatusTranscation.OpenTransactionPage(scheduleonId, InstanceId);
                        //udcInternalPerformerStatusTranscation.Visible = true;
                        //udcInternalPerformerStatusTranscation.OnSaved += (inputForm, args) => {  };
                        iInternalPerformerFrame.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPerformer(" + scheduleonId + "," + InstanceId + ");", true);
                    }
                }
                else if (RoleID == 4 && Type == "Internal")
                {
                    if (SLType == "LicenseCompliance")
                    {
                        iInternalReviewerFrameLic.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalReviewerLic(" + scheduleonId + "," + InstanceId + "," + "" + ");", true);
                    }
                    else
                    {
                        //udcStatusTranscationInternal.OpenTransactionPage(scheduleonId, InstanceId);
                        //udcStatusTranscationInternal.Visible = true;
                        //udcStatusTranscationInternal.OnSaved += (inputForm, args) => { };
                        iInternalReviewerFrame.Visible = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalReviewer(" + scheduleonId + "," + InstanceId + ");", true);
                    }
                }
                else if (RoleID == 3 && Type == "Internal Checklist")
                {
                    iInternalChecklistPerformerFrame.Visible = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalChecklistPerformer(" + scheduleonId + "," + InstanceId + ");", true);

                    //udcComplianceStatusTransactionInternalChkList.OpenTransactionPage(scheduleonId, InstanceId);
                    //udcComplianceStatusTransactionInternalChkList.Visible = true;
                }
                //else if (RoleID == 4 && Type == "Internal Checklist")
                //{
                //    udcStatusTranscatopn.OpenTransactionPage(scheduleonId, InstanceId);
                //}
            }
        }

        protected void sample()
        { }
    }
}