﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Questions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindActs();
            }
        }

        public void BindCompliances(List<long> Actids )
        {
            try
            {
                lstActs.ClearSelection();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var mastercompliances = entities.Compliances.Where(x => x.IsDeleted == false).ToList();

                    if (Actids.Count>0)
                    {
                       var act = mastercompliances.Where(x => x.IsDeleted == false && Actids.Contains(x.ActID)).Select(model => new {

                           ID = model.ID,
                           Name = model.ShortDescription.Trim()
                       }).ToList();


                        if (act.Count > 0)
                            act = act.OrderBy(row => row.Name).ToList();

                        lstCompliances.DataTextField = "Name";
                        lstCompliances.DataValueField = "ID";

                        lstCompliances.DataSource = act;
                        lstCompliances.DataBind();
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindActs()
        {
            try
            {
                lstActs.ClearSelection();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var act = entities.Acts.Where(x => x.IsDeleted == false).Select(model => new {

                        ID=model.ID, Name= model.Name.Trim() }).ToList();

                    if (act.Count > 0)
                        act = act.OrderBy(row => row.Name).ToList();

                    lstActs.DataTextField = "Name";
                    lstActs.DataValueField = "ID";

                    lstActs.DataSource = act;
                    lstActs.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }

        protected void lstActs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<long> listactid = new List<long>();
                listactid.Clear();
                foreach (ListItem item in lstActs.Items)
                {
                    if (item.Selected)
                    {
                        listactid.Add(Convert.ToInt32(item.Value));
                        //message += "Student Name : " + item.Text + ", Enrollment No : " + item.Value + "\\n";
                    }
                }

                if (listactid.Count > 0)
                {
                    BindCompliances(listactid);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}