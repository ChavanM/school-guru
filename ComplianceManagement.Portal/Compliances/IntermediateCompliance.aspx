﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="IntermediateCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.IntermediateCompliance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });

            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });

          
        
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        
        //function FnSearch() {

        //    var tree = document.getElementById('BodyContent_tvFilterLocation');
        //    var links = tree.getElementsByTagName('a');
        //    var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
        //    var keysrchlen = keysrch.length
        //    if (keysrchlen > 2) {
        //          $('#bindelement').html('');
        //        for (var i = 0; i < links.length; i++) {

        //            var anch = $(links[i]);
        //            var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
        //            var getId = $(anch).attr('id');
        //            var parendNode = '#' + getId + 'Nodes';
        //            var childanchor = $(parendNode).find('a');
        //            if (childanchor.length == 0) {
        //                if (twoletter > -1) {
        //                    var createanchor = '<a href="' + $(anch).attr('href') + '" >' + anch.html() + '</a></br>';
        //                    $('#bindelement').append(createanchor);
        //                }
        //            }

        //        }
        //        $(tree).hide();
        //        $('#bindelement').show();
        //    } else {
        //        $('#bindelement').html('');
        //        $('#bindelement').hide();
        //        $(tree).show();
        //    }

        //}
 
        //sandesh code start
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
        //added by sandesh end


    </script>

    <style type="text/css">
        .td1 {
            width: 25%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 25%;
        }

        .td4 {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="70%">

                    <tr>
                         <td class="td1">
                             <div id="lblcustomer" runat="server" style="margin-left:134px">
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: -3px;margin-left:-134px">
                                Customer:
                            </label>
                                 </div>
                        </td>
                        <td class="td2">
                            <div id="customerdiv" runat="server" style="margin-left:70px">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: -139px; height: 25px; width: 280px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                        <td class="td3" align="left">
                            <div style="margin-right: 164px;">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: -3px;">
                                Select Location:
                            </label>
                                </div>
                        </td>
                        <td class="td4" align="left">
                            <div style="">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;margin-left:-172px"
                                CssClass="txtbox" />
                            <div style="margin-left: -172px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px" 
                                    Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                                    <%--OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">--%>
                                </asp:TreeView>
                                 <%--<div id="bindelement" style="background: white;height: 292px;display:none; width: 390px;border: 1px solid;overflow: auto;"></div>--%>

                                <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>

                        <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="Select" />
                        <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />

                                <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                                </div>
                        </td>
                        <td class="td5" align="right" >
                            <div style="margin-left: -65px;">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: -3px;">
                               Filter:
                            </label>
                                </div>
                        </td>
                        <td class="td6" align="left">
                            <div style="">
                              <asp:TextBox runat="server" ID="tbxFilter" Width="250px" Height="22px" MaxLength="50" AutoPostBack="true" margin-right="-22px"
                            OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12" align="center" runat="server">
                            <asp:Panel ID="Panel1" Width="100%" Height="420px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" OnRowCancelingEdit="grdComplianceRoleMatrix_RowCancelingEdit"
                                    OnRowUpdating="grdComplianceRoleMatrix_RowUpdating" OnRowDeleting="grdComplianceRoleMatrix_RowDeleting" OnRowEditing="grdComplianceRoleMatrix_RowEditing" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID,UserID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" SortExpression="Sections">
                                            <ItemTemplate>                                               
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="Description" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 550px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" SortExpression="Users" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 300px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <div style="overflow: hidden; width: 300px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:DropDownList ID="ddlUserList" Width="300px" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" SortExpression="Role" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>                                                
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit" ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                                <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
