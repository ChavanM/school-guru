﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceMappingCopy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindMappedLocationFilter();
                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);
                BindUsers(ddlFilterApprover);
               
                tbxFilterMappedLocation.Text = "< Select >";
                tbxFilterMappingLocation.Text = "< Select >";
            }

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeJQueryUIMappedBranch", string.Format("initializeJQueryUIMappedBranch('"+ tbxFilterMappedLocation.ClientID + "', 'divFilterMappedLocation');"), true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivMappedBranch", "hideDivMappedBranch('divFilterMappedLocation');", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeJQueryUIMappingBranch", string.Format("initializeJQueryUIMappingBranch('"+ tbxFilterMappingLocation.ClientID + "', 'divFilterMappingLocation');"), true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivMappingBranch", "hideDivMappingBranch('divFilterMappingLocation');", true);
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindMappingLocationFilter()
        {
            try
            {                
                tvMappingLocation.Nodes.Clear();

                int customerID = -1;
                int mappedbranchID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (!(tbxFilterMappedLocation.Text.Trim().Equals("< Select >")))
                {
                    mappedbranchID = CustomerBranchManagement.GetByName(tbxFilterMappedLocation.Text.Trim(), customerID).ID;
                }

                var bracnhes = CustomerBranchManagement.GetAllHierarchyForMappingLocation(customerID, mappedbranchID);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());                    
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);                       
                    tvMappingLocation.Nodes.Add(node);
                }
                int maxDepth = 0;
                DisableNodesForMappingLocationFilter(tvMappingLocation.Nodes,mappedbranchID, ref maxDepth);               

                while (maxDepth > 0)
                {
                    RemoveNodesForMappingLocationFilter(tvMappingLocation.Nodes, mappedbranchID);
                    maxDepth--;
                }

                tvMappingLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void DisableNodesForMappingLocationFilter(TreeNodeCollection nodes,int mappedBranchID,ref int maxDepth)
        {
            List<TreeNode> checkedNodes = new List<TreeNode>();           
            int customerID = -1;
            bool mappedStateId = false;
            bool isParentFrOtherBranches = false;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            foreach (TreeNode node in nodes)
            {
                if (maxDepth < node.Depth)
                        maxDepth = node.Depth;

                mappedStateId = CustomerBranchManagement.ExistsMappedStateIDToBranch(Convert.ToInt32(node.Value),mappedBranchID);
                isParentFrOtherBranches = CustomerBranchManagement.IsParentForOtherBranch(Convert.ToInt32(node.Value));

                if (Convert.ToInt32(node.Value) != customerID)
                {
                    if (Convert.ToInt32(node.Value) == mappedBranchID)
                    {
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (mappedStateId == false)
                    {
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (isParentFrOtherBranches == false && mappedStateId == false)
                    {
                        checkedNodes.Add(node);
                    }                   

                    DisableNodesForMappingLocationFilter(node.ChildNodes, mappedBranchID, ref maxDepth);
                }
                else
                    DisableNodesForMappingLocationFilter(node.ChildNodes, mappedBranchID, ref maxDepth);
            }

            foreach (TreeNode checkedNode in checkedNodes)
            {
                nodes.Remove(checkedNode);
            }
        }

        private void RemoveNodesForMappingLocationFilter(TreeNodeCollection nodes, int mappedBranchID)
        {
            List<TreeNode> checkedNodes = new List<TreeNode>();
            int customerID = -1;
            bool mappedStateId = false;
            bool isParentFrOtherBranches = false;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            foreach (TreeNode node in nodes)
            {
                mappedStateId = CustomerBranchManagement.ExistsMappedStateIDToBranch(Convert.ToInt32(node.Value), mappedBranchID);
                isParentFrOtherBranches = CustomerBranchManagement.IsParentForOtherBranch(Convert.ToInt32(node.Value));

                if (Convert.ToInt32(node.Value) != customerID)
                {
                    if (Convert.ToInt32(node.Value) == mappedBranchID)
                    {
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (mappedStateId == false)
                    {
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (isParentFrOtherBranches == false && mappedStateId == false)
                    {
                        checkedNodes.Add(node);
                    }

                    if (isParentFrOtherBranches == true && mappedStateId == false && node.ChildNodes.Count == 0)
                    {
                        checkedNodes.Add(node);
                    }

                    if (Convert.ToInt32(node.Value) == mappedBranchID && mappedStateId == true && node.ChildNodes.Count == 0)
                    {
                        checkedNodes.Add(node);
                    }

                    RemoveNodesForMappingLocationFilter(node.ChildNodes, mappedBranchID);
                }
                else
                    RemoveNodesForMappingLocationFilter(node.ChildNodes, mappedBranchID);
            }

            foreach (TreeNode checkedNode in checkedNodes)
            {
                nodes.Remove(checkedNode);                
            }
        }
        private void BindMappedLocationFilter()
        {
            try
            {               
                tvMappedLocation.Nodes.Clear();

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForMappedLocation(customerID);               

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);                  
                    tvMappedLocation.Nodes.Add(node);                    
                }                

                int maxDepth = 0;
                DisableNodesForMappedLocationFilter(tvMappedLocation.Nodes, ref maxDepth);

                while (maxDepth > 0)
                {
                    RemoveNodesForMappedLocationFilter(tvMappedLocation.Nodes);
                    maxDepth--;
                }

                tvMappedLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void DisableNodesForMappedLocationFilter(TreeNodeCollection nodes,ref int maxDepth)
        {
            List<TreeNode> checkedNodes = new List<TreeNode>();
            bool mappedBranchId = false;
            bool isParentFrOtherBranches = false;

            int customerID = -1;
            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            foreach (TreeNode node in nodes)
            {
                if (maxDepth < node.Depth)
                    maxDepth = node.Depth;

                mappedBranchId = CustomerBranchManagement.ExistsMappedBranch(Convert.ToInt32(node.Value));
                isParentFrOtherBranches = CustomerBranchManagement.IsParentForOtherBranch(Convert.ToInt32(node.Value));
                                
                if(Convert.ToInt32(node.Value) != customerID)
                {
                    if (mappedBranchId == false)
                    {                        
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (isParentFrOtherBranches == false && mappedBranchId == false)                                          
                        checkedNodes.Add(node);                    

                    DisableNodesForMappedLocationFilter(node.ChildNodes, ref maxDepth);
                }  
                else                
                    DisableNodesForMappedLocationFilter(node.ChildNodes, ref maxDepth);                             
            }

            foreach (TreeNode checkedNode in checkedNodes)
            {
                nodes.Remove(checkedNode);
            }
        }

        private void RemoveNodesForMappedLocationFilter(TreeNodeCollection nodes)
        {
            List<TreeNode> checkedNodes = new List<TreeNode>();
            bool mappedBranchId = false;
            bool isParentFrOtherBranches = false;

            int customerID = -1;
            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            foreach (TreeNode node in nodes)
            {
                mappedBranchId = CustomerBranchManagement.ExistsMappedBranch(Convert.ToInt32(node.Value));
                isParentFrOtherBranches = CustomerBranchManagement.IsParentForOtherBranch(Convert.ToInt32(node.Value));

                if (Convert.ToInt32(node.Value) != customerID)
                {
                    if (mappedBranchId == false)
                    {
                        node.ImageUrl = "";
                        node.NavigateUrl = "";
                        node.SelectAction = TreeNodeSelectAction.None;
                    }

                    if (isParentFrOtherBranches == false && mappedBranchId == false)
                        checkedNodes.Add(node);

                    if (isParentFrOtherBranches == true && mappedBranchId == false && node.ChildNodes.Count == 0)
                    {
                        checkedNodes.Add(node);
                    }                    

                    RemoveNodesForMappedLocationFilter(node.ChildNodes);
                }
                else
                    RemoveNodesForMappedLocationFilter(node.ChildNodes);
            }

            foreach (TreeNode checkedNode in checkedNodes)
            {
                nodes.Remove(checkedNode);
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        private void ConditionsBindComplianceMatrix()
        {           
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N");
            }
            else
            {
                BindComplianceMatrix("Y");
            }
        }

        private void BindComplianceMatrix(string flag)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int MappedbranchID = -1;
                int MappingbranchID = -1;

                if (!(tbxFilterMappedLocation.Text.Trim().Equals("< Select >")))
                {
                    MappedbranchID = CustomerBranchManagement.GetByName(tbxFilterMappedLocation.Text.Trim(), customerID).ID;
                }

                if (!(tbxFilterMappingLocation.Text.Trim().Equals("< Select >")))
                {
                    MappingbranchID = CustomerBranchManagement.GetByName(tbxFilterMappingLocation.Text.Trim(), customerID).ID;
                }

                List<int> actIds = new List<int>();

                if (flag == "N" && MappedbranchID !=-1)
                {
                    grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByMappedLocation(MappedbranchID);
                }

                else if (flag == "Y" && MappedbranchID != -1)
                {
                    grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByMappedLocation(MappedbranchID, tbxFilter.Text.Trim());
                }
                else
                {
                    grdComplianceRoleMatrix.DataSource = null;
                }

                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int MappedbranchID = -1;               

                if (!(tbxFilterMappedLocation.Text.Trim().Equals("< Select >")))
                {
                    MappedbranchID = CustomerBranchManagement.GetByName(tbxFilterMappedLocation.Text.Trim(), customerID).ID;
                }               

                if (MappedbranchID != -1)
                {
                    var ComplianceRoleMatrixList = Business.ComplianceManagement.TempGetByMappedLocation(MappedbranchID);

                    if (direction == SortDirection.Ascending)
                    {
                        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }

                    foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                        }
                    }

                    SaveCheckedValues();
                    grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                    grdComplianceRoleMatrix.DataBind();
                    PopulateCheckedValues();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        //protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //}

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N");
                }
                else
                {
                    BindComplianceMatrix("Y");
                }
               
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N");
            }
            else
            {
                BindComplianceMatrix("Y");
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {            
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N");
                }
                else
                {
                    BindComplianceMatrix("Y");
                }
        }       

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                {
                    countCheckedCheckbox = countCheckedCheckbox + 1;
                }
            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool success = false;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var complianceList = new List<ComplianceAsignmentProperties>();
                var complianceListIntial = new List<ComplianceAsignmentProperties>();
                SaveCheckedValues();

                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                complianceListIntial = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                List<TempAssignmentTable> Tempassignments = new List<TempAssignmentTable>();
                List<TempAssignmentTableCheckList> TempAssignmentTableChkLst = new List<TempAssignmentTableCheckList>();

                int mappingBranchID = CustomerBranchManagement.GetByName(tbxFilterMappingLocation.Text.Trim(), customerID).ID;
                //grdComplianceRoleMatrix.AllowPaging = true;
                //BindComplianceMatrix("N");
                //grdComplianceRoleMatrix.DataBind();

                //Remove mapped compliances for mapping branches
                complianceList = Business.ComplianceManagement.RemoveMappedCompliances(complianceList, mappingBranchID);

                if (complianceListIntial != null)
                {
                    if (complianceList != null && complianceList.Count > 0)
                    {
                        for (int i = 0; i < complianceList.Count; i++)
                        {
                            if (complianceList[i].ComplianceType == 1)
                            {
                                if (complianceList[i].Performer)
                                {
                                    if (ddlFilterPerformer.SelectedValue != null && ddlFilterPerformer.SelectedValue != "-1")
                                    {
                                        TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                        TempAssP.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssP.CustomerBranchID = mappingBranchID;
                                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                        TempAssP.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                                        TempAssP.IsActive = true;
                                        TempAssP.CreatedOn = DateTime.Now;
                                        TempAssignmentTableChkLst.Add(TempAssP);
                                    }
                                    if (ddlFilterReviewer.SelectedValue != null && ddlFilterReviewer.SelectedValue != "-1")
                                    {
                                        TempAssignmentTableCheckList TempAssR = new TempAssignmentTableCheckList();
                                        TempAssR.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssR.CustomerBranchID = mappingBranchID;
                                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                        TempAssR.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                                        TempAssR.IsActive = true;
                                        TempAssR.CreatedOn = DateTime.Now;
                                        TempAssignmentTableChkLst.Add(TempAssR);
                                    }
                                }
                            }
                            else
                            {
                                if (complianceList[i].Performer)
                                {
                                    if (ddlFilterPerformer.SelectedValue != null && ddlFilterPerformer.SelectedValue != "-1")
                                    {
                                        TempAssignmentTable TempAssP = new TempAssignmentTable();
                                        TempAssP.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssP.CustomerBranchID = mappingBranchID;
                                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                        TempAssP.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                                        TempAssP.IsActive = true;
                                        TempAssP.CreatedOn = DateTime.Now;
                                        Tempassignments.Add(TempAssP);
                                    }
                                    if (ddlFilterReviewer.SelectedValue != null && ddlFilterReviewer.SelectedValue != "-1")
                                    {
                                        TempAssignmentTable TempAssR = new TempAssignmentTable();
                                        TempAssR.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssR.CustomerBranchID = mappingBranchID;
                                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                        TempAssR.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                                        TempAssR.IsActive = true;
                                        TempAssR.CreatedOn = DateTime.Now;
                                        Tempassignments.Add(TempAssR);

                                    }
                                    if (ddlFilterApprover.SelectedValue != null && ddlFilterApprover.SelectedValue != "-1")
                                    {
                                        TempAssignmentTable TempAssA = new TempAssignmentTable();
                                        TempAssA.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssA.CustomerBranchID = mappingBranchID;
                                        TempAssA.RoleID = RoleManagement.GetByCode("APPR").ID;
                                        TempAssA.UserID = Convert.ToInt32(ddlFilterApprover.SelectedValue);
                                        TempAssA.IsActive = true;
                                        TempAssA.CreatedOn = DateTime.Now;
                                        Tempassignments.Add(TempAssA);
                                    }
                                }
                            }
                        }

                        if (TempAssignmentTableChkLst.Count != 0)
                        {
                            Business.ComplianceManagement.AddDetailsTempAssignmentTableCheckList(TempAssignmentTableChkLst);
                        }

                        if (Tempassignments.Count != 0)
                        {
                            Business.ComplianceManagement.AddDetailsTempAssignmentTable(Tempassignments);
                        }

                        if (Tempassignments.Count != 0 || TempAssignmentTableChkLst.Count != 0)
                        {
                            success = true;
                            ClearSelection();
                        }

                        if (success)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Compliances Assigned Successfully";
                            cvDuplicateEntry.CssClass = "alert alert-success";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliances Already Assigned.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                }

                BindMappedLocationFilter();
                ConditionsBindComplianceMatrix();
                ViewState["CHECKED_ITEMS"] = null;
                tvMappedLocation.CollapseAll();
            }
            catch (Exception ex)
            {               
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        

        private void ClearSelection()
        {
            tbxFilterMappedLocation.Text = "< Select >";
            tbxFilterMappingLocation.Text = "< Select >";
            ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.SelectedValue = "-1";
            ddlFilterApprover.SelectedValue = "-1";           
        }

        private void SaveCheckedValues()
        {
            try
            {

                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;
                    complianceProperties.ComplianceType = Convert.ToInt32(((Label)gvrow.FindControl("lblComplianceType")).Text);

                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");
                            chkPerformer.Checked = rmdata.Performer;

                            Label lblComplianceType = (Label)gvrow.FindControl("lblComplianceType");
                            lblComplianceType.Text =Convert.ToString(rmdata.ComplianceType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvMappingLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterMappingLocation.Text = tvMappingLocation.SelectedNode.Text;                
                tvMappingLocation.CollapseAll();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivMappingBranch", "hideDivMappingBranch('divFilterMappingLocation');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvMappedLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterMappedLocation.Text = tvMappedLocation.SelectedNode.Text;
                tbxFilterMappingLocation.Text = "< Select >";
                BindMappingLocationFilter();
                ConditionsBindComplianceMatrix();               
                tvMappedLocation.CollapseAll();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}