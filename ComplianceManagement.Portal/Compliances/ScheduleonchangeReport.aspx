﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ScheduleonchangeReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ScheduleonchangeReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                               <asp:LinkButton runat="server" ID="btnExport" Style=" margin-top: 2px; margin-left: 95%;" OnClick="btnExport_Click"><img src="../Images/excel.png" alt="Export to Excel"
                  title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
                                           
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
     <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
     
            <div style="width: 100%">
                <div style="margin-left: 10px;" runat="server">
                    Filter:
                      <asp:TextBox runat="server" ID="tbxFilter" Width="250px"  MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />

                    <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: 80px; height: 22px; width: 280px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
                </div>
              
                                      

                <div id="divNotAssigendCompliances" style="margin-top: 10px; margin-bottom: 10px">
                    <asp:GridView runat="server" ID="grdReport" AutoGenerateColumns="false" AllowSorting="true"
                        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdReport_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" OnSorting="grdReport_Sorting"
                        Width="100%" Font-Size="12px" OnPageIndexChanging="grdReport_PageIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="ComplianceID" HeaderStyle-Width="2%" DataField="ID" SortExpression="ID" />
                              <asp:TemplateField HeaderText="Customer Name" ItemStyle-Width="10%" SortExpression="CustomerName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="10%" SortExpression="BranchName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="10%" SortExpression="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period" ItemStyle-Width="10%" SortExpression="Period" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("Period") %>' ToolTip='<%# Eval("Period") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Olddate" ItemStyle-Width="10%" SortExpression="Olddate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                       <%-- <asp:Label ID="lblOlddate" runat="server" Text='<%# Eval("Olddate") %>' ToolTip='<%# Eval("Olddate") %>'></asp:Label>--%>
                                         <%# Eval("Olddate")!= null?((DateTime)Eval("Olddate")).ToString("dd-MMM-yyyy"):""%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NewDate" ItemStyle-Width="10%" SortExpression="NewDate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                       <%-- <asp:Label ID="lblNewDate" runat="server" Text='<%# Eval("NewDate") %>' ToolTip='<%# Eval("NewDate") %>'></asp:Label>--%>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <%# Eval("NewDate")!= null?((DateTime)Eval("NewDate")).ToString("dd-MMM-yyyy"):""%>
                                    </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreatedBy" ItemStyle-Width="10%" SortExpression="CreatedBy" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedBy") %>' ToolTip='<%# Eval("CreatedBy") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="CreatedDate" ItemStyle-Width="10%" SortExpression="CreatedDate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <%# Eval("CreatedDate")!= null?((DateTime)Eval("CreatedDate")).ToString("dd-MMM-yyyy"):""%>
                                    </div>

                                </ItemTemplate>
                            </asp:TemplateField>
                         
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

            </div>
        </ContentTemplate>
       
    </asp:UpdatePanel>
</asp:Content>
