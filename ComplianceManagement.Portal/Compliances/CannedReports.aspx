﻿<%@ Page Title="My Reports" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true"
    CodeBehind="CannedReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CannedReports" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/CannedReportPerformer.ascx" TagName="CannedReportPerformer"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/CannedReportReviewer.ascx" TagName="CannedReportReviewer"
    TagPrefix="vit" %>
<%--<%@ Register Src="~/Controls/CannedReportApprover.ascx" TagName="CannedReportApprover"
    TagPrefix="vit" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: $('#ContentPlaceHolder1_udcCannedReportPerformer_txtStartDate').val(),
                maxDate: $('#ContentPlaceHolder1_udcCannedReportPerformer_txtEndDate').val(),
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>

    <style type="text/css">
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function hidetree() {
            //if (event.target.value != "Location") {
            //    $('#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer').hide();
            //} 
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 " onclick="hidetree()">
               <section class="panel">
                    
                   <%if (roles.Contains(3) || roles.Contains(4))
                        {
                        %>  
               <header class="panel-heading tab-bg-primary ">
                    <ul id="rblRole1" class="nav nav-tabs">
                                                                 
                            <% if (roles.Contains(3))%>
                            <%{%>
                                <li class="dummyval">                                                     
                                <asp:LinkButton ID="liPerformer"   OnClick="btnPerformer_Click"  runat="server">Performer</asp:LinkButton>
                                </li>
                            <%}%>
                            <% if (roles.Contains(4))%>
                            <%{%>
                                <li class="dummyval">                                                      
                                <asp:LinkButton ID="liReviewer"   OnClick="btnReviewer_Click"  runat="server">Reviewer</asp:LinkButton>
                                </li>
                            <%}%>    
                                                       
                    </ul>
               </header>
                    <%}%>    
                          
                <div class="clearfix" style="clear:both;height:5px;"></div>

                <div class="panel-body" style="padding-top: 3px;padding-bottom: 5px;">
                    <div class="tab-content">
                        <div style="float: right;">
                        <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role!="AUDT")
                        {%> 
                                    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.TaskApplicable=="1")%>
                                    <%{%>
                                        <div style="float:left;width: 132px;margin-top: -19px;">
                                        <li class="dropdown">
                                        <%if (roles.Count>0) {%>
                                            <button class="form-control m-bot15" type="button" style="position: absolute; width: 132px;" data-toggle="dropdown">
                                        <%} else {%>
                                            <button class="form-control m-bot15" type="button" style="margin-top: -1px; position: absolute; width: 132px;" data-toggle="dropdown">                           
                                        <%}%> More Reports
                                        <span class="caret" style="border-top-color: #a4a7ab"></span>
                                        </button>
                                       
                                        <%if (roles.Count>0) {%>
                                        <ul class="dropdown-menu extended"  style="margin-top: -27px; width: 100px !important;">
                                        <%} else {%>
                                        <ul class="dropdown-menu extended"  style="margin-top: 11px; width: 100px !important;">
                                        <%}%>
                                        <li><a id="btntaskDocument" runat="server" href="../Task/TaskReport.aspx">Task Reports</a></li>
                                        </ul>  </li>
                                        </div>
                                    <%}%>
                         <%}%>
                            <div style="margin-left:11px;width: 124px;margin-right: 2px;float: right;">
                                                   <asp:Button ID="btnAdvSearch" Text="Export to Excel" class="btn btn-search" Style="width: 124px;float: right;margin-right: 0px;margin-top: 0px;margin-bottom: 5px;" OnClick="lbtnExportExcel_Click" runat="server" />
                           
                                </div>                            
                    </div>

                        <div class="clearfix" style="clear: both;height: 10px;"></div>

                        <div runat="server" id="performerdocuments" class="tab-pane active">
                            <vit:CannedReportPerformer runat="server" ID="udcCannedReportPerformer" />
                        </div>

                        <div class="clearfix"></div>

                        <div runat="server" id="reviewerdocuments" class="tab-pane">
                            <vit:CannedReportReviewer runat="server" ID="udcCannedReportReviewer" Visible="true" />
                        </div>
                    </div>
                   </div>
            </div>
               </section>
            </div>
        </div>
    
     <div>
                <div class="modal fade" id="divOverViewInternalPerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">

                                <iframe id="OverViewsInternalPerformer" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
                </div>

     <div>
                <div class="modal fade" id="divOverViewPerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">

                                <iframe id="OverViewsPerformer" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
    
    <div>
    <div class="modal fade" id="divOverViewInternalReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="OverViewsInternalReviewer" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                </div>
            </div>
        </div>
    </div>
</div>

    <div>
    <div class="modal fade" id="divOverViewReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="OverViewsReviewer" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                </div>
            </div>
        </div>
    </div>
</div>


    <script type="text/javascript">
        $(document).ready(function () {


             <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role=="MGMT") {%>  
           
                    fhead('My Reports');
                    setactivemenu('leftreportsmenu');

                    if ($('.dummyval').length == 0) {
                        $('#ContentPlaceHolder1_btnAdvSearch').css('margin-top', '0px');
                        $('#ContentPlaceHolder1_btnAdvSearch').css('margin-bottom', '5px');
                    }
            //        fhead('Detailed Report');
            //        setactivemenu('Myreport');
            //        fmaters1();
             <%} else { %> 

                   fhead('My Reports');
                    setactivemenu('leftreportsmenu');
                    
                    if ($('.dummyval').length == 0) {
                        $('#ContentPlaceHolder1_btnAdvSearch').css('margin-top', '0px');
                        $('#ContentPlaceHolder1_btnAdvSearch').css('margin-bottom', '5px');
                    }
             <%}%> 



            //setactivemenu('leftreportsmenu');
            //fhead('My Reports');
            //if ($('.dummyval').length == 0) {

            //    $('#ContentPlaceHolder1_btnAdvSearch').css('margin-top', '0px');
            //    $('#ContentPlaceHolder1_btnAdvSearch').css('margin-bottom', '5px');
            //}
        });
        function instructionsDashboard() {
            var enjoyhint_instance = new EnjoyHint({});

            //simple config. 
            //Only one step - highlighting(with description) "New" button 
            //hide EnjoyHint after a click on the button.
            var enjoyhint_script_steps = [
                {
                    "next #ContentPlaceHolder1_udcCannedReportPerformer_ddlComplianceType": "Select type of compliance internal/Statutory",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_udcCannedReportPerformer_tbxFilterLocationPerformer": "Select location to see filtered data by location",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_udcCannedReportPerformer_btnSearch": "Click to filter data based on selection",
                    showSkip: true,
                }, {
                    "next #ContentPlaceHolder1_udcCannedReportPerformer_ddlpageSize": "Change no. of tasks/compliance in the table",
                    showSkip: true,
                }
              , {
                  "next .btn-advanceSearch": "Perform advanced search",
                  showSkip: true,
              }
                , {
                    "next #ContentPlaceHolder1_btnAdvSearch": "Click to Download report in excel",
                    showSkip: true,
                }
            ];


            //set script config
            enjoyhint_instance.set(enjoyhint_script_steps);

            //run Enjoyhint script
            enjoyhint_instance.run();
        }

    </script>
</asp:Content>
