﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ChangeRiskofCompliance : System.Web.UI.Page
    {
        protected int UserID = -1;
        public static List<long> locationList = new List<long>();
        public static List<int> Branchlist = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {                
                
                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    tbxFilterLocation.Text = "< Select >"; 
                   // BindCustomers(UserID);
                }
                else
                {  
                    BindLocationFilter();
                    tbxFilterLocation.Text = "< Select >";
                }
                BindCustomers(UserID);
                BindActs();
                BindCompliance(); 
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
          //  BindCompliance();
            BindActs();
        }
        public void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomer.DataSource = GetCustomer();
                if (AuthenticationHelper.Role == "IMPT")
                {
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(UserID);
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                }
                else
                {
                    int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomer.DataSource = Assigncustomer.GetAllCustomerData(customerID);
                    ddlCustomer.SelectedValue = Convert.ToString(customerID);
                    ddlCustomer.DataBind();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        private void BindActs()
        {

            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ComplianceDetails = (from row in entities.SP_GetComplianceRisk(customerID)
                                             orderby row.ActName ascending
                                             select new { ID = row.ActID, Name = row.ActName }).OrderBy(entry => entry.Name).ToList<object>();
                    ddlAct.DataTextField = "Name";
                    ddlAct.DataValueField = "ID";
                    ddlAct.DataSource = ComplianceDetails.Distinct().ToList();
                    ddlAct.DataBind();
                    ddlAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";                
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerID = -1;
                // int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void BindCompliance()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
              
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   var ComplianceDetails = (from row in entities.SP_GetComplianceRisk(customerID)
                                             select row).ToList();

                    if (locationList.Count > 0)
                    {
                        ComplianceDetails = ComplianceDetails.Where(entry => locationList.Contains(entry.CustomerBranchID)).ToList();
                    }

                    int actid = -1;
                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    {
                        actid = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                    if (actid != -1)
                    {
                        ComplianceDetails = ComplianceDetails.Where(entry => entry.ActID == actid).ToList();
                    }

                    int compilancetypeid = -1;
                    if (!string.IsNullOrEmpty(ddlComplinceType.SelectedValue))
                    {
                        compilancetypeid = Convert.ToInt32(ddlComplinceType.SelectedValue);
                    }
                    if (compilancetypeid != -1)
                    {
                        if (compilancetypeid == 2)
                        {
                            ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();
                        }
                        else
                        {
                            ComplianceDetails = ComplianceDetails.Where(entry => entry.ComplianceType == compilancetypeid).ToList();
                        }
                    }
                    if (!string.IsNullOrEmpty(tbxFilter.Text))
                    {
                        if (CheckInt(tbxFilter.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                            ComplianceDetails = ComplianceDetails.Where(entry => entry.ID == a).ToList();
                        }
                        else
                        {
                            ComplianceDetails = ComplianceDetails.Where(entry =>
                              entry.ShortDescription.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim())
                              || entry.BranchName.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim())
                              || (entry.ActName != null && entry.ActName.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim()))
                              || (entry.ClientRisk != null && entry.ClientRisk.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim()))
                                ).ToList();                            
                        }
                    }
                    grdCompliances.DataSource = ComplianceDetails;
                    grdCompliances.DataBind();
                    tbxFilterLocation.Text = "< Select >";
                    Session["grdDetailData"] = (grdCompliances.DataSource as List<SP_GetComplianceRisk_Result>).ToDataTable();
                    upCompliancesList.Update();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";

            }
        }
        protected void ddlComplinceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
       
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList DropDownList1 = (e.Row.FindControl("ddlrisk") as DropDownList);
                    DropDownList1.Items.Insert(0, new ListItem("--Select risk--", "-1"));
                    DropDownList1.Items.Insert(1, new ListItem("High", "0"));
                    DropDownList1.Items.Insert(2, new ListItem("Medium", "1"));
                    DropDownList1.Items.Insert(3, new ListItem("Low", "2"));
                    DropDownList1.Items.Insert(4, new ListItem("Critical", "3"));


                    DropDownList ddlrisk = (e.Row.FindControl("ddlrisk") as DropDownList);
                    string Task = (e.Row.FindControl("lblcrisk") as Label).Text;
                    if (!string.IsNullOrEmpty(Task))
                    {
                        ddlrisk.ClearSelection();
                        ddlrisk.Items.FindByText(Task).Selected = true;
                    }                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
              
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {

                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = checkSheetExist(xlWorkbook, "ComplianceRiskDetails");
                            if (matchSuccess)
                            {
                                ProcessUserData(xlWorkbook);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ComplianceRiskDetails'.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }

        }
        public bool ComplainceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.ID == ComplianceID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public partial class CBCTempTable
        {
            public long ComplianceId { get; set; }
        }
        private void ProcessUserData(ExcelPackage xlWorkbook)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long CustomerID = 0;
                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        CustomerID = AuthenticationHelper.CustomerID;
                    }

                    if (CustomerID != 0)
                    {
                        #region Excel
                        ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ComplianceRiskDetails"];
                        if (xlWorksheet != null)
                        {
                            int xlrow2 = xlWorksheet.Dimension.End.Row;
                            string valClientRisk = string.Empty;
                            int valComplianceID = -1;
                            int valComplianceInstanceID = -1;
                            List<string> errorMessage = new List<string>();
                            List<CBCTempTable> lstTemptable = new List<CBCTempTable>();
                            #region Validations
                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                valClientRisk = string.Empty;
                                valComplianceID = -1;
                                valComplianceInstanceID = -1;

                                #region 5 ClientRisk
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    valClientRisk = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                }
                                if (String.IsNullOrEmpty(valClientRisk))
                                {
                                    errorMessage.Add("Required Client Risk at row number-" + rowNum);
                                }
                                else
                                {
                                    if (valClientRisk.Trim().ToUpper() != "HIGH" && valClientRisk.Trim().ToUpper() != "MEDIUM" && valClientRisk.Trim().ToUpper() != "LOW")
                                    {
                                        errorMessage.Add("Please Correct  Client Risk at row number-" + rowNum + " Risk should be High,Medium Low");
                                    }
                                }
                                #endregion
                                #region 6 ComplianceID                                
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text.Trim());
                                }
                                if (valComplianceID == -1 || valComplianceID == 0)
                                {
                                    errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                                }
                                else
                                {
                                    if (ComplainceExists(valComplianceID) == false)
                                    {
                                        errorMessage.Add("ComplianceID not defined in the System  at row number-" + rowNum);
                                    }
                                }
                                #endregion
                                #region 7 ComplianceInstanceID                                
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    valComplianceInstanceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 7].Text.Trim());
                                }
                                if (valComplianceInstanceID == 0 || valComplianceInstanceID == -1)
                                {
                                    errorMessage.Add("Required ComplianceInstanceID at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!lstTemptable.Any(x => x.ComplianceId == valComplianceInstanceID))
                                    {
                                        CBCTempTable tt = new CBCTempTable();
                                        tt.ComplianceId = valComplianceInstanceID;
                                        lstTemptable.Add(tt);
                                    }
                                    else
                                    {
                                        errorMessage.Add("Compliance with this ComplianceInstanceID (" + valComplianceInstanceID + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                    }
                                }
                                #endregion

                            }
                            #endregion
                            //int count = 0;



                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                            }
                            else
                            {
                                #region

                                string Clientrisk = string.Empty;
                                int Complianceid = -1;
                                int ComplianceInstanceID = -1;
                                List<ComplianceInstance> complianceList = new List<ComplianceInstance>();
                                bool sucess = false;
                                for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                {
                                    Clientrisk = string.Empty;
                                    Complianceid = -1;
                                    ComplianceInstanceID = -1;

                                    Complianceid = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text.Trim());
                                    ComplianceInstanceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 7].Text.Trim());
                                    Clientrisk = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                    ComplianceInstance complianceData = new ComplianceInstance();                                

                                    if (Clientrisk.Trim().ToUpper() == "HIGH")
                                    {
                                        complianceData.Risk = 0;
                                    }
                                    else if (Clientrisk.Trim().ToUpper() == "MEDIUM")
                                    {
                                        complianceData.Risk = 1;
                                    }
                                    else if (Clientrisk.Trim().ToUpper() == "LOW")
                                    {
                                        complianceData.Risk = 2;
                                    }
                                    else
                                    {
                                        complianceData.Risk = 3;
                                    }
                                    complianceData.ComplianceId = Complianceid;
                                    complianceData.ID = ComplianceInstanceID;
                                    complianceList.Add(complianceData);
                                }
                                #endregion
                                if (complianceList.Count > 0)
                                {
                                    sucess = UpdateExcelComplianceInstance(complianceList);
                                }
                                if (sucess)
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "Risk updated successfully.";
                                }
                            }// END DB Context

                        }
                        #endregion
                    }//Customerid=0 end
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public bool UpdateExcelComplianceInstance(List<ComplianceInstance> CIList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CIList.ForEach(entry =>
                    {
                        ComplianceInstance ltlmToUpdate = (from row in entities.ComplianceInstances
                                                           where row.ID == entry.ID
                                                           && row.ComplianceId == entry.ComplianceId
                                                           select row).FirstOrDefault();

                        ltlmToUpdate.Risk = entry.Risk;
                        entities.SaveChanges();
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void ErrorMessages(List<string> emsg)
        {          
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }
        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void btn_SaveClick(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool ischecked = false;
                    foreach (GridViewRow gvrow in grdCompliances.Rows)
                    {
                        var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                        if (checkbox.Checked)
                        {
                            ischecked = true;

                            break;
                        }
                    }
                    if (ischecked)
                    {
                        bool sucess = false;
                        List<string> errorMessage = new List<string>();
                        List<ComplianceInstance> complianceList = new List<ComplianceInstance>();
                        foreach (GridViewRow gvrow in grdCompliances.Rows)
                        {
                            var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                            if (checkbox.Checked)
                            {
                                var lblcomplianceID = gvrow.FindControl("lblComplianceID") as Label;
                                var lblComplInstenaceID = gvrow.FindControl("lblComplianceInstenaceID") as Label;
                                long complianceid = Convert.ToInt32(lblcomplianceID.Text);
                                long complianceinstanceid = Convert.ToInt32(lblComplInstenaceID.Text);
                                int riskid = -1;

                                var ddlrisk= gvrow.FindControl("ddlrisk") as DropDownList;
                                if (!string.IsNullOrEmpty(ddlrisk.SelectedValue) && ddlrisk.SelectedValue != "-1")
                                {
                                    riskid = Convert.ToInt32(((DropDownList)gvrow.FindControl("ddlrisk")).SelectedValue);

                                    ComplianceInstance com = new ComplianceInstance()
                                    {
                                        ID = complianceinstanceid,
                                        ComplianceId = complianceid,
                                        Risk = Convert.ToByte(riskid),
                                    };
                                    complianceList.Add(com);
                                } 
                                else
                                {
                                    errorMessage.Add("Risk Not Updated for ComplianceID -" + complianceid);
                                    //cvUploadUtilityPage.IsValid = false;
                                    //cvUploadUtilityPage.ErrorMessage = "please select risk.";
                                    //sucess = false;                                    
                                }                                                                                                      
                            }
                        }
                        upCompliancesList.Update();
                        if (complianceList.Count > 0 )
                        {
                            sucess = UpdateExcelComplianceInstance(complianceList);
                            if (errorMessage.Count>0)
                            {
                                ErrorMessages(errorMessage);
                            }
                            else 
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "Risk updated successfully.";
                            }                           
                        }                                                               
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Please check at least one checkbox.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";                
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ComplianceRiskDetails");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "BranchName", "ActName", "ShortDescription", "AvacomRisk", "ClientRisk", "ID", "ComplianceInstanceID");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Branch Name";
                    exWorkSheet.Cells["A1"].AutoFitColumns(40);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Act Name";
                    exWorkSheet.Cells["B1"].AutoFitColumns(40);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "ShortDescription";
                    exWorkSheet.Cells["C1"].AutoFitColumns(40);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "AvacomRisk";
                    exWorkSheet.Cells["D1"].AutoFitColumns(25);


                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "ClientRisk";
                    exWorkSheet.Cells["E1"].AutoFitColumns(25);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "CID";
                    exWorkSheet.Cells["F1"].AutoFitColumns(25);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "CIID";
                    exWorkSheet.Cells["G1"].AutoFitColumns(25);



                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 7])
                    {

                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ComplianceRiskDetails.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";                
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliance();
            }
            catch (Exception ex)
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool Update(ComplianceInstance ltlm)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ComplianceInstance ltlmToUpdate = (from row in entities.ComplianceInstances
                                                       where row.ID == ltlm.ID
                                                       && row.ComplianceId == ltlm.ComplianceId
                                                       select row).FirstOrDefault();

                    ltlmToUpdate.Risk = ltlm.Risk;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
                
            }
        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                tvFilterLocation.Nodes.Clear();
                tbxFilterLocation.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);  

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindCompliance(); 
        }
        protected void upComplianceList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {

                //  tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
                //   nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }
        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

    }
}