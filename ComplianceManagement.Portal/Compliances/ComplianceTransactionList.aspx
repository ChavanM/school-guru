﻿<%@ Page Title="Compliance Transaction List" Language="C#" MasterPageFile="~/Compliance.Master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="ComplianceTransactionList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceTransactionList" %>

<%@ Register Src="~/Controls/ComplianceReviewerStatusTransactionCA.ascx" TagPrefix="vit" TagName="ComplianceReviewerStatusTransactionCA" %>
<%@ Register Src="~/Controls/ComplianceStatusTransactionCA.ascx" TagPrefix="vit" TagName="ComplianceStatusTransactionCA" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTransactionsList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTransactionsList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td>
                        <div style="margin-left: 30px;">
                            <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
                            title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
                        </div>
                    </td>
                    <td>
                        <label style="width: 100px; margin-left: 20px; display: block; font-size: 13px; color: #333;">
                            Start Date</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 200px;" AutoPostBack="true" OnTextChanged="txtStartDate_TextChanged"
                            MaxLength="200" />
                    </td>
                    <td>
                        <label style="width: 100px; display: block; margin-left: 20px; float: left; font-size: 13px; color: #333;">
                            End Date</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 200px;" MaxLength="200" AutoPostBack="true" OnTextChanged="txtEndDate_TextChanged" />
                    </td>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right"></td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" OnRowCreated="grdComplianceTransactions_RowCreated"
                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowDataBound="grdComplianceTransactions_RowDataBound"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="14" OnSorting="grdComplianceTransactions_Sorting"
                Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnRowCommand="grdComplianceTransactions_RowCommand"
                OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                <asp:Label runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="400px" SortExpression="Description">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                    <asp:TemplateField HeaderText="Scheduled On" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                        <ItemTemplate>
                            <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="For Month" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                        <ItemTemplate>
                            <%# Eval("ForMonth") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Status" />
                    <asp:TemplateField HeaderText="Completed On" SortExpression="StatusChangedOn" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Eval("StatusChangedOn") !=null ? Convert.ToDateTime(Eval("StatusChangedOn")).ToString("dd-MMM-yyyy") : ""%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="30px" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") + "," + Eval("RoleID") %>'><img src="../Images/change_status_icon.png" alt="Change Status" title="Change Status" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function initializeDatePicker() {
        }
        function initializeComboboxUpcoming() {
        }
        function StatusReportInitializeDatePicker(date) {
            var startDate = new Date();
            $("#<%= txtStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                onClose: function (startDate) {
                    $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
            }
            });

            $("#<%= txtEndDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
            }
            });


            if (date != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
    </script>    
    <vit:ComplianceStatusTransactionCA runat="server" ID="udcStatusTranscatopn"></vit:ComplianceStatusTransactionCA>
    <vit:ComplianceReviewerStatusTransactionCA runat="server" ID="udcReviewerStatusTransaction"></vit:ComplianceReviewerStatusTransactionCA>
</asp:Content>
