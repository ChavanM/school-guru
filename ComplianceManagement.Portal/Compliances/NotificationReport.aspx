﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="NotificationReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.NotificationReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <%--  <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

     <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <link href="../NewCSS/basickendo.css" rel="stylesheet" />
   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
       <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>


    <style type="text/css">

        input[type=checkbox], input[type=radio] {
    margin: 4px 4px 0;
    line-height: normal;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }
        #grid .k-grouping-header {
            font-style: italic;
            margin-top: 1px;
            background-color: white;
        }
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
    </style>
    <title></title>

    <script type="text/x-kendo-template" id="template"> 
      
    </script>
     <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Reports /Notification Report');
            setactivemenu('Myreport');
            fmaters1();
     });

    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            debugger;
            BindUsers();
            BindGrid();



            $("#Startdatepicker").kendoDatePicker({
                format:"dd-MM-yyyy",
                change: DateFilterCustom
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy",
                change: DateFilterCustom
            });
      
     });
        var record = 0;
        var total = 0;
    

        function exportReportAdvanced() {
            $("#grid").getKendoGrid().saveAsExcel();
            return false;
        }
   


        function DateFilterCustom() {

            var setStartDate = $("#Startdatepicker").val();
            var setEndDate = $("#Lastdatepicker").val();
            if (setStartDate != null && setStartDate != "") {
                $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
            }
            if (setEndDate != null && setEndDate != "") {
                $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
            }
            FilterAll();
        }

        function BindGrid() {

            debugger;

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/KendoNotificationReport?roles=<%=roles%>&Userid=<%=UId%> ',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                       
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: function (e) {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        this.dataSource.pageSize(10)
                    }
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                   
                },
                columns: [

                      {
                        field: "TypeOfMail", title: 'Type of Mail',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }, autoWidth: true,
                        }

                    },

                    {
                        field: "UserName", title: 'User Name',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    
                    
                   {
                       
                       field: "SentOn", title: 'Mail Sent On',
                        width: "10%",
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                  
                   


                ]
            });

            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

        }

    function BindUsers() {
        $("#dropdownUser").kendoDropDownList({
                filter: "startswith",
                autoClose: true,
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                optionLabel: "Select User",
                change: function (e) {
                    FilterAll();

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                          
                            url: '<% =Path%>/Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =roles%> ',
                            dataType: "json",
                            beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                      
                       }
                   }
               });

        }
        function ApplyBtnAdvancedFilter() {
            BindGrid();
        }

       
        //$("#exportAdvanced").kendoTooltip({
        //    filter: ".k-grid-edit3",
        //    content: function (e) {
        //        return "Export to Excel";
        //    }
        //});
        function FilterAll() {
            debugger;
            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "SentOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "SentOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                });
            }
           
            if (datedetails.length > 0)
            {

                var finalSelectedfilter = { logic: "and", filters: [] };
                   <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "MGMT") {%> 
                

                if ($("#dropdownUser").val() != "" && $("#dropdownUser").val() != null && $("#dropdownUser").val() != undefined) {
                    var UserFilter = { logic: "or", filters: [] };
                    UserFilter.filters.push({
                        field: "UserID", operator: "eq", value: $("#dropdownUser").val()
                    });
                    finalSelectedfilter.filters.push(UserFilter);
                    $('#ClearfilterMain').css('display', 'block');

                }

                     <%}%>
               
             
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {

                    var DateFilter = { logic: "or", filters: [] };

                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "SentOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                        });
                    }
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {

                    var DateFilter = { logic: "or", filters: [] };

                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "SentOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                        });
                    }
                    finalSelectedfilter.filters.push(DateFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                dataSource.pageSize(total);
            }
        }

        function ClearAllFilterMain(e) {

           
            //$("#dropdownTypeNew").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
               <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role =="MGMT") {%> 
            $("#dropdownUser").data("kendoDropDownList").select(0);
                    <%}%>
            //$("#dropdownCategory").data("kendoDropDownList").select(0);
            //$("#dropdownFrequency").data("kendoDropDownList").select(0);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            e.preventDefault();
        }


  function exportReportAdvanced(e) {
            debugger;
            var ReportName = "";
            ReportName = "Notification Report";
            var FileName = "";
            FileName = "NotificationReport";
            //var ReportNameAdd = "Report of " + ReportName + " ";
            var todayDate = moment().format('DD-MMM-YYYY');
           // var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');

            var grid = $("#grid").getKendoGrid();

          
                var rows = [
                    {
                        cells: [
                            { value: "Customer Name:", bold: true },
                            { value: '<% =CustomerName%>' }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Sr.No.", bold: true },
                            { value: "Type of Mail", bold: true },
                            { value: "User Name", bold: true },
                            { value: "Sent On", bold: true ,},
                          

                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [ // dataItem."Whatever Your Attributes Are"
                            { value: '' },
                            { value: dataItem.TypeOfMail },
                            { value: dataItem.UserName },
                            { value: dataItem.SentOn,format:"dd-MMM-yyyy" },
                         
                        ]
                    });

                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 4; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                        //row.cells[16].format = "dd-MMM-yyyy";
                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }

                excelExport(rows, ReportName);
            

        
            function excelExport(rows, ReportName) {
                debugger;
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { autoWidth: true },
                                { autoWidth: true },
                                { width: 250 },
                                { width: 250 },
                                { width: 250 },
                                { width: 250 },
                                { width: 250 },
                                 { width: 250 },

                            
                              

                            ],
                            title: FileName,
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = FileName;
                //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

            }

            e.preventDefault();
        
       }

        $("#exportAdvanced").kendoTooltip({
            filter: ".k-grid-edit3",
            content: function (e) {
                return "Export to Excel";
            }
        });



       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="example">

         <div class="row">
             <div class="col-md-12 colpadding0">
                  <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "MGMT") {%> 
                 <div class="col-md-3" id="dvdropdownlistuser" style="width: 25%; padding-left: 0px;">
                     <input id="dropdownUser" data-placeholder="Risk" style="width: 100%;" />
                 </div>
                  <%}%>
                 <div class="col-md-3" id="dvStartdatepicker" style="width: 25%; padding-left: 0px;">
                     <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                 </div>
                 <div class="col-md-3" id="dvLastdatepicker" style="width: 23%; padding-left: 0px;">
                     <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 100%;" />
                 </div>
                 <div class="col-md-3" style="width: 13%; padding-left: 0px; float: right;">
                     <button type="button" id="exportAdvanced" style="height: 28px; margin-right: 10px; margin-left: -153px;" onclick="exportReportAdvanced(event)" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
          
                      <button id="ClearfilterMain" style="height: 28px; float: right; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>          
                 </div>

             </div>

         </div>

          <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterLastDate">&nbsp;</div>
            <div class="clearfix" style="height: 10px;"></div>
          
            <div id="grid"></div>
        </div>
</asp:Content>
