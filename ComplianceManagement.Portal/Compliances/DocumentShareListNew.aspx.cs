﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class DocumentShareListNew : System.Web.UI.Page
    {
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    foreach (string val in allPossibleListBoxValues)
        //    {
        //        Page.ClientScript.RegisterForEventValidation(myListBox.UniqueID, val);
        //    }
        //    base.Render(writer);
        //}
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "T")
                this.MasterPageFile = "~/ContractProduct.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "S")
                this.MasterPageFile = "~/LicenseManagement.Master";
            else
                this.MasterPageFile = "~/NewCompliance.Master";
        }
        public int ContractID;
        protected static int GetFolderCustId;
        public int customerID;
        // protected long customerID = 0;
        protected static List<Mst_FolderMaster> objFolder;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCTemSec.Visible = false;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            GetFolderCustId = GetClientName(customerID);
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["FilterSearch"]))
                {
                    tbxFilter.Text = Convert.ToString(Request.QueryString["FilterSearch"]);
                }
                int UID = Convert.ToInt32(AuthenticationHelper.UserID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objFolder = (from row in entities.Mst_FolderMaster
                                 where row.CustomerID == customerID
                                 && row.CreatedBy == UID
                                 && row.IsUniverse == 1
                                 select row).ToList();
                }
                Session["TotalDocShareListRows"] = null;
                BindFolderData();
                bindPageNumber();
                SetShowingRecords();
                BindUsers();
                BindListShare();
            }
        }
        private void BindListShare()
        {
            List<ShareDetail> ShareList = new List<ShareDetail>();

            myRepeater.DataSource = ShareList;
            myRepeater.DataBind();
        }
        public static int GetClientName(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "FolderCustID"
                             && row.ClientID == customerID
                            select row.ClientID).FirstOrDefault();
                return data;
            }
        }
        public string ShowUserName(string CreatedById)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                long getUserID = Convert.ToInt64(CreatedById);

                if (getUserID > 0)
                {
                    if (Convert.ToInt64(AuthenticationHelper.UserID) == getUserID)
                    {
                        return "me";
                    }
                    else
                    {
                        var UserObj = (from row in entities.UserCustomerViews
                                       where row.UserID == getUserID && row.CustomerId == customerID
                                       select row).FirstOrDefault();
                        if (UserObj != null)
                        {
                            return UserObj.UserName;
                        }
                        return string.Empty;
                    }
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }
        public string UserAccess(string ID)
        {
            try
            {
                string ouput = "NotAccess";
                int UFPID = Convert.ToInt32(ID);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                int UserID = -1;
                UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);

                UserFolderPermission details = (from row in entities.UserFolderPermissions
                                                where row.FolderID == UFPID && row.IsDeleted == false
                                                 && row.CustomerID == customerID 
                                                 && row.FileID == null
                                                 && row.UserID == UserID
                                                select row).FirstOrDefault();

                if (details != null)
                {
                    if (details.IsView == true)
                    {
                        ouput = "IsviewAccess";
                    }
                    if (details.IsRead == true)
                    {
                        ouput = "IsreadAccess";
                    }
                    if (details.IsWrite == true)
                    {
                        ouput = "Access";
                    }
                    //if (details.IsWrite == true)
                    //{
                    //    ouput = "Access";
                    //}
                }
                return ouput;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }
        private void BindFolderData()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string Searchfilter = string.Empty;
                    Searchfilter = tbxFilter.Text.ToString();

                    var FolderList = entities.SP_GetFolderFileDataPermissionNew(Convert.ToInt64(AuthenticationHelper.UserID), null, customerID, Searchfilter).OrderByDescending(e=>e.createdon).ToList();
                    grdFolderDetail.DataSource = FolderList;
                    grdFolderDetail.DataBind();
                    Session["TotalDocShareListRows"] = null;
                    Session["TotalDocShareListRows"] = Convert.ToInt32(FolderList.Count);
                    upPromotorList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkAddNewFolder_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;  
                txtFolderName.Text = string.Empty;
                btnCreateFolder1.Text = "Create";
                upPromotorList.Update();
                chkboxuniversediv.Visible = true;
                universediv.Visible = true;
                chkIsUnivers.Checked = true;
                //upMailDocument.Update();
                //BindUsers();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenpopup();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void Update(int ContractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                Mst_FolderMaster FolderToUpdate = (from row in entities.Mst_FolderMaster
                                   where row.ID == ContractID
                                   select row).FirstOrDefault(); 
                FolderToUpdate.Name = txtFolderName.Text; 
                FolderToUpdate.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                FolderToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();  
            }  
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                if ((int)ViewState["Mode"] == 1)
                {
                    ContractID = Convert.ToInt32(ViewState["ID"]);
                }
                else
                {
                    ContractID = 0;
                }
                if (AWSData != null)
                {
                    #region AWS
                    divsuccessmsgaCTemSec.Visible = false;
                    int customerID = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    string FinalFolderPath = "DriveFiles\\" + customerID;
                    
                    //string FinalFolderPath = "~/DriveFiles/" + customerID + "/";
                    string UserNameFolder = txtFolderName.Text.ToString().Trim();
                    string CreateNewFolder = FinalFolderPath + "\\" + UserNameFolder;
                
                   IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                   S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, CreateNewFolder);
                    if (ContractID == 0)
                    {
                        if (!di.Exists)
                        {
                            di.Create();
                            //}
                            //if (!Directory.Exists(Server.MapPath(CreateNewFolder)))
                            //{
                            long CreatedFID = -1;
                            //Directory.CreateDirectory(Server.MapPath(CreateNewFolder));
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Mst_FolderMaster ObjFolder = new Mst_FolderMaster();
                                ObjFolder.Name = UserNameFolder.ToString();
                                ObjFolder.CustomerID = customerID;
                                ObjFolder.CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                ObjFolder.CreatedOn = DateTime.Now;
                                ObjFolder.IsDeleted = false;
                                ObjFolder.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                ObjFolder.UpdatedOn = DateTime.Now;
                                entities.Mst_FolderMaster.Add(ObjFolder);
                                entities.SaveChanges();
                                CreatedFID = Convert.ToInt64(ObjFolder.ID);

                                if (CreatedFID > 0)
                                {
                                    UserFolderPermission objFP = new UserFolderPermission()
                                    {
                                        FolderID = Convert.ToInt64(CreatedFID),
                                        FileID = null,
                                        CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false,
                                        UserID = Convert.ToInt64(AuthenticationHelper.UserID),
                                        CustomerID = customerID,
                                        SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                    };
                                    entities.UserFolderPermissions.Add(objFP);
                                    entities.SaveChanges();
                                }
                            }
                            //cvMailDocument.IsValid = false;
                            //cvMailDocument.ErrorMessage = "Folder Created Succesfully";
                            successmsgaCTemSec.Text = "Folder Created Succesfully.";
                            divsuccessmsgaCTemSec.Visible = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);


                            BindFolderData();
                            bindPageNumber();
                            SetShowingRecords();
                            int count = Convert.ToInt32(GetTotalPagesCount());
                            if (count > 0)
                            {
                                int gridindex = grdFolderDetail.PageIndex;
                                string chkcindition = (gridindex + 1).ToString();
                                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                            }
                        }
                        else
                        {
                            cvMailDocument.IsValid = false;
                            cvMailDocument.ErrorMessage = "Folder Name already Exists.";
                        }
                    }
                    else
                    {
                        if ((int)ViewState["Mode"] == 1)
                        {
                            if (Exists(UserNameFolder))
                            {     
                                cvMailDocument.IsValid = false;
                                cvMailDocument.ErrorMessage = "Folder Name already Exists.";
                            }
                            else
                            {
                                // contractFolderID = Convert.ToInt64(ViewState["ID"]);
                                int contractid = Convert.ToInt32(ContractID);
                                Update(contractid);
                                successmsgaCTemSec.Text = "Folder Renamed Succesfully.";
                                divsuccessmsgaCTemSec.Visible = true;
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "fclosepopup();", true);
                                //  ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);

                            }
                            BindFolderData();
                            bindPageNumber();
                            SetShowingRecords();  
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Normal
                    divsuccessmsgaCTemSec.Visible = false;
                    int customerID = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    string FinalFolderPath = "~/DriveFiles/" + customerID + "/";
                    string UserNameFolder = txtFolderName.Text.ToString().Trim();
                    string CreateNewFolder = FinalFolderPath + "/" + UserNameFolder;
                    if (ContractID == 0)
                    {
                        if (!Directory.Exists(Server.MapPath(CreateNewFolder)))
                        {
                            int isuniversfalg = 0;

                            if (chkIsUnivers.Checked)
                                isuniversfalg = 1;

                            long CreatedFID = -1;
                            Directory.CreateDirectory(Server.MapPath(CreateNewFolder));
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Mst_FolderMaster ObjFolder = new Mst_FolderMaster();
                                ObjFolder.Name = UserNameFolder.ToString();
                                ObjFolder.CustomerID = customerID;
                                ObjFolder.CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                ObjFolder.CreatedOn = DateTime.Now;
                                ObjFolder.IsDeleted = false;
                                ObjFolder.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                                ObjFolder.UpdatedOn = DateTime.Now;
                                ObjFolder.IsUniverse = isuniversfalg;
                                entities.Mst_FolderMaster.Add(ObjFolder);
                                entities.SaveChanges();
                                CreatedFID = Convert.ToInt64(ObjFolder.ID);


                                if (CreatedFID > 0)
                                {
                                    UserFolderPermission objFP = new UserFolderPermission()
                                    {
                                        FolderID = Convert.ToInt64(CreatedFID),
                                        FileID = null,
                                        CreatedBy = Convert.ToInt64(AuthenticationHelper.UserID),
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false,
                                        UserID = Convert.ToInt64(AuthenticationHelper.UserID),
                                        CustomerID = customerID,
                                        SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                    };
                                    entities.UserFolderPermissions.Add(objFP);
                                    entities.SaveChanges();
                                }

                                #region univers
                                if (chkIsUnivers.Checked == true)
                                {
                                    long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                                    bool IsViewFile = false;
                                    bool IsReadFile = false;
                                    bool IsWriteFile = false;
                                    bool saveSuccess = false;
                                    List<long> lstUserMapping = new List<long>();
                                    List<long> lstUserRemoveMapping = new List<long>();
                                    #region Userlist
                                    foreach (ListItem eachUser in lstBoxUserforUnivers.Items)
                                    {
                                        if (eachUser.Selected)
                                        {
                                            if (Convert.ToInt32(eachUser.Value) > 0)
                                            {
                                                lstUserMapping.Add(Convert.ToInt64(eachUser.Value));
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(eachUser.Value) > 0)
                                            {
                                                lstUserRemoveMapping.Add(Convert.ToInt64(eachUser.Value));
                                            }

                                        }
                                    }
                                    #endregion

                                    if (lstUserMapping.Count > 0)
                                    {
                                        #region Permission
                                        IsViewFile = true;
                                        IsReadFile = true;
                                        IsWriteFile = false;
                                        #endregion

                                        #region Folder
                                        saveSuccess = false;
                                        long CreatedUserID = (from row in entities.Mst_FolderMaster
                                                              where row.ID == CreatedFID && row.IsDeleted == false
                                                              && row.CreatedBy == UserID && row.CustomerID == customerID
                                                              select (long)row.CreatedBy).FirstOrDefault();
                                        if (CreatedUserID > 0)
                                        {
                                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)CreatedFID)
                                                                       select (long)row).ToList();
                                            List<UserFolderPermission> lstUSerMapping_ToSaveFolder = new List<UserFolderPermission>();
                                            foreach (var itemf in AllFolderIDs)
                                            {
                                                long assignFolderId = Convert.ToInt64(itemf);

                                                lstUserMapping.ForEach(EachUSer =>
                                                {
                                                    UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                                  where row.FolderID == assignFolderId && row.IsDeleted == false
                                                                                                  && row.UserID == EachUSer
                                                                                                  && row.FileID == null
                                                                                                  && row.CustomerID == customerID
                                                                                                  select row).FirstOrDefault();

                                                    if (CheckExistFileAssigne != null)
                                                    {
                                                        CheckExistFileAssigne.IsView = IsViewFile;
                                                        CheckExistFileAssigne.IsRead = IsReadFile;
                                                        CheckExistFileAssigne.IsWrite = IsWriteFile;
                                                        CheckExistFileAssigne.IsDeleted = false;
                                                        entities.SaveChanges();
                                                    }
                                                    else
                                                    {
                                                        UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                        {
                                                            FolderID = Convert.ToInt64(assignFolderId),
                                                            FileID = null,
                                                            CreatedBy = Convert.ToInt64(CreatedUserID),
                                                            CreatedOn = DateTime.Now,
                                                            IsRead = IsReadFile,
                                                            IsView = IsViewFile,
                                                            IsWrite = IsWriteFile,
                                                            IsDeleted = false,
                                                            UserID = Convert.ToInt64(EachUSer),
                                                            CustomerID = Convert.ToInt64(customerID),
                                                            SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                        };
                                                        lstUSerMapping_ToSaveFolder.Add(_UserMappingRecord);
                                                    }
                                                });
                                            }
                                            if (lstUSerMapping_ToSaveFolder.Count > 0)
                                            {
                                                lstUSerMapping_ToSaveFolder.ForEach(eachRecord =>
                                                {
                                                    saveSuccess = false;
                                                    entities.UserFolderPermissions.Add(eachRecord);
                                                    entities.SaveChanges();
                                                    saveSuccess = true;
                                                });
                                            }
                                        }
                                        else
                                        {
                                            UserFolderPermission updatedPermission = (from row in entities.UserFolderPermissions
                                                                                      where row.CustomerID == customerID
                                                                                      && row.FolderID == CreatedFID
                                                                                      && row.IsDeleted == false
                                                                                      && row.FileID == null
                                                                                      && row.UserID == UserID
                                                                                      select row).FirstOrDefault();
                                            if (updatedPermission != null)
                                            {
                                                if (updatedPermission.IsRead == true && updatedPermission.IsWrite == true)
                                                {

                                                }
                                            }
                                        }
                                        #endregion

                                        #region File

                                        List<long> FileIDs = (from row in entities.sp_AllfilesofFolder((int)CreatedFID)
                                                              select (long)row).ToList();

                                        List<UserFolderPermission> lstUSerMapping_ToSave = new List<UserFolderPermission>();
                                        foreach (var item in FileIDs)
                                        {
                                            long FileIds = Convert.ToInt64(item);

                                            UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                                    where row.FileID == FileIds && row.IsDeleted == false
                                                                                    && row.UserID == UserID && row.CustomerID == customerID
                                                                                    select row).FirstOrDefault();
                                            if (uploadedFileIds != null)
                                            {
                                                lstUserMapping.ForEach(EachUSer =>
                                                {
                                                    UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                                  where row.FileID == FileIds && row.IsDeleted == false
                                                                                                  && row.UserID == EachUSer && row.CustomerID == customerID
                                                                                                  select row).FirstOrDefault();

                                                    if (CheckExistFileAssigne != null)
                                                    {

                                                    }
                                                    else
                                                    {
                                                        UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                        {
                                                            FolderID = Convert.ToInt64(uploadedFileIds.FolderID),
                                                            FileID = Convert.ToInt64(FileIds),
                                                            CreatedBy = Convert.ToInt64(uploadedFileIds.CreatedBy),
                                                            CreatedOn = DateTime.Now,
                                                            IsRead = IsReadFile,
                                                            IsWrite = IsWriteFile,
                                                            IsView = IsViewFile,
                                                            IsDeleted = false,
                                                            UserID = Convert.ToInt64(EachUSer),
                                                            CustomerID = Convert.ToInt64(customerID),
                                                            SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                        };
                                                        lstUSerMapping_ToSave.Add(_UserMappingRecord);
                                                    }
                                                });
                                            }
                                        }
                                        if (lstUSerMapping_ToSave.Count > 0)
                                        {
                                            lstUSerMapping_ToSave.ForEach(eachRecord =>
                                            {
                                                saveSuccess = false;
                                                entities.UserFolderPermissions.Add(eachRecord);
                                                entities.SaveChanges();
                                                saveSuccess = true;
                                            });
                                        }
                                        if (saveSuccess)
                                        {
                                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforuserupdate();", true);
                                            // saveSuccess = true;
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                      //  ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforusernotselect();", true);
                                    }
                                }
                                #endregion

                            }
                            //cvMailDocument.IsValid = false;
                            //cvMailDocument.ErrorMessage = "Folder Created Succesfully";
                            successmsgaCTemSec.Text = "Folder Created Succesfully.";
                            divsuccessmsgaCTemSec.Visible = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fclosepopup1();", true);
                            BindFolderData();
                            bindPageNumber();
                            SetShowingRecords();
                            int count = Convert.ToInt32(GetTotalPagesCount());
                            if (count > 0)
                            {
                                int gridindex = grdFolderDetail.PageIndex;
                                string chkcindition = (gridindex + 1).ToString();
                                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                            }

                        }
                        else
                        {
                            cvMailDocument.IsValid = false;
                            cvMailDocument.ErrorMessage = "Folder Name already Exists.";
                        }
                    }
                    else
                    {
                        if ((int)ViewState["Mode"] == 1)
                        {
                            if (Exists(UserNameFolder))
                            {
                                cvMailDocument.IsValid = false;
                                cvMailDocument.ErrorMessage = "Folder Name already Exists.";
                            }
                            else
                            {
                                // contractFolderID = Convert.ToInt64(ViewState["ID"]);
                                int contractid = Convert.ToInt32(ContractID);
                                Update(contractid);
                                successmsgaCTemSec.Text = "Folder Renamed Succesfully.";
                                divsuccessmsgaCTemSec.Visible = true;
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "fclosepopup();", true);

                            }
                            BindFolderData();
                            bindPageNumber();
                            SetShowingRecords();
                        }
                    }
                    #endregion
                }
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Something went wrong, Please try again";               
            }
        }
        public static bool Exists(string foldername)
        {
            Boolean flag;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Mst_FolderMaster
                            where row.Name.ToUpper().Trim() == foldername.ToUpper().Trim()
                            && row.IsDeleted == false
                            select row).FirstOrDefault();

                if (data != null)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                return flag;

            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalDocShareListRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindFolderData();
                bindPageNumber();
                SetShowingRecords();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdFolderDetail.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdFolderDetail.PageIndex = chkSelectedPage - 1;
            grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindFolderData();
            //bindPageNumber();
            SetShowingRecords();
        }
        private void bindSelecedUser(long ExistPermissionFileId)
        {
            long customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ComplianceDBEntities entities = new ComplianceDBEntities();
            long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
            List<ShareDetail> ShareList = new List<ShareDetail>();
            ShareList.Clear();
            if (ViewState["Fileorfolder"].ToString().ToLower() == "file")
            {

            }
            else
            {

                int IsUniverse = CheckIsUniverse(Convert.ToInt32(ExistPermissionFileId), Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (IsUniverse == 1)
                {
                    List<long> FinaluserList = new List<long>();
                    List<long> UserIds = new List<long>();
                    List<long> FileIDs = new List<long>();
                    List<long> FinalOutputList = new List<long>();
                    bool flagFileExist = false;

                    #region FileShare
                    List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistPermissionFileId)
                                             select (long)row).ToList();
                    if (AllFileIDs.Count > 0)
                    {

                        foreach (var item in AllFileIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                   && row.UserID == UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            if (uploadedFileIds.Count > 0)
                            {
                                flagFileExist = true;
                                FileIDs.Add(CheckExistFilePermisssion);
                            }
                        }

                        foreach (var item in FileIDs)
                        {
                            long ExistFileID = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == ExistFileID && row.IsDeleted == false
                                                   && row.UserID != UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            foreach (var items in uploadedFileIds)
                            {
                                UserIds.Add(Convert.ToInt64(items.UserID));
                            }
                        }
                        if (UserIds.Count > 0)
                        {
                            UserIds = UserIds.Distinct().ToList();
                        }

                        int Count = Convert.ToInt32(FileIDs.Count);
                        foreach (var ids in UserIds)
                        {
                            long userIdCheck = Convert.ToInt64(ids);
                            int flag = 0;
                            foreach (var flds in FileIDs)
                            {
                                long fileids = Convert.ToInt64(flds);
                                UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                        where row.FileID == fileids && row.IsDeleted == false
                                                                        && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                        select row).FirstOrDefault();
                                if (uploadedFileIds != null)
                                {
                                    flag = flag + 1;
                                }
                            }
                            if (flag == Count)
                            {
                                FinaluserList.Add(Convert.ToInt64(userIdCheck));
                            }
                        }
                    }
                    #endregion

                    #region folder
                    if (flagFileExist == true)
                    {
                        long CreatedUserID = UserID;

                        //long CreatedUserID = (from row in entities.Mst_FolderMaster
                        //                      where row.ID == ExistPermissionFileId && row.IsDeleted == false
                        //                      && row.CreatedBy == UserID && row.CustomerID == customerID
                        //                      select (long)row.CreatedBy).FirstOrDefault();
                        if (CreatedUserID > 0)
                        {
                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                       select (long)row).ToList();
                            if (AllFolderIDs.Count > 0)
                            {
                                int CountFolderIds = Convert.ToInt32(AllFolderIDs.Count);
                                foreach (var itemUserid in FinaluserList)//ckeck user 
                                {
                                    long userIdCheck = Convert.ToInt64(itemUserid);
                                    int flagFolder = 0;
                                    foreach (var itemFolderid in AllFolderIDs)//check folder
                                    {
                                        long folderids = Convert.ToInt64(itemFolderid);
                                        UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                                where row.FolderID == folderids && row.IsDeleted == false
                                                                                && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                                select row).FirstOrDefault();
                                        if (uploadedFileIds != null)
                                        {
                                            flagFolder = flagFolder + 1;
                                        }
                                    }
                                    if (flagFolder == CountFolderIds)
                                    {
                                        FinalOutputList.Add(Convert.ToInt64(userIdCheck));
                                    }
                                }
                            }
                        }
                        int Cust_Id = Convert.ToInt32(customerID);
                        foreach (var item in FinalOutputList)
                        {

                            var lstUsers = (from row in entities.UserCustomerViews
                                            where row.UserID == item && row.CustomerId == Cust_Id
                                            select row).FirstOrDefault();

                            if (lstUsers != null)
                            {
                                ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                            }
                        }
                    }
                    else
                    {
                        List<long> FinaluserOutputList = new List<long>();
                        List<long> UserFolderIDs = new List<long>();

                        long CreatedUserID = UserID;

                        //long CreatedUserID = (from row in entities.Mst_FolderMaster
                        //                      where row.ID == ExistPermissionFileId && row.IsDeleted == false
                        //                      && row.CreatedBy == UserID && row.CustomerID == customerID
                        //                      select (long)row.CreatedBy).FirstOrDefault();
                        if (CreatedUserID > 0)
                        {
                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                       select (long)row).ToList();
                            if (AllFolderIDs.Count > 0)
                            {
                                foreach (var item in AllFolderIDs)
                                {
                                    long CheckExistFolderPermisssion = Convert.ToInt64(item);
                                    var uploadedFOlderUserIds = (from row in entities.UserFolderPermissions
                                                                 where row.FolderID == CheckExistFolderPermisssion && row.IsDeleted == false
                                                                 && row.CustomerID == customerID && row.FileID == null && row.UserID != UserID
                                                                 select row).ToList();
                                    foreach (var itemFld in uploadedFOlderUserIds)
                                    {
                                        long UIdFloder = Convert.ToInt64(itemFld.UserID);
                                        UserFolderIDs.Add(UIdFloder);
                                    }
                                }
                                if (UserFolderIDs.Count > 0)
                                {
                                    UserFolderIDs = UserFolderIDs.Distinct().ToList();
                                }

                                int CountFLD = Convert.ToInt32(AllFolderIDs.Count);
                                foreach (var ids in UserFolderIDs)
                                {
                                    long userIdCheck = Convert.ToInt64(ids);
                                    int flag = 0;
                                    foreach (var flds in AllFolderIDs)
                                    {
                                        long fileids = Convert.ToInt64(flds);
                                        UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                                where row.FolderID == fileids && row.IsDeleted == false
                                                                                && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                                select row).FirstOrDefault();
                                        if (uploadedFileIds != null)
                                        {
                                            flag = flag + 1;
                                        }
                                    }
                                    if (flag == CountFLD)
                                    {
                                        FinaluserOutputList.Add(Convert.ToInt64(userIdCheck));
                                    }
                                }
                                int Cust_Id = Convert.ToInt32(customerID);
                                foreach (var item in FinaluserOutputList)
                                {
                                    var lstUsers = (from row in entities.UserCustomerViews
                                                    where row.UserID == item && row.CustomerId == Cust_Id
                                                    select row).FirstOrDefault();

                                    if (lstUsers != null)
                                    {
                                        ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    List<long> FinaluserList = new List<long>();
                    List<long> UserIds = new List<long>();
                    List<long> FileIDs = new List<long>();
                    List<long> FinalOutputList = new List<long>();
                    bool flagFileExist = false;

                    #region FileShare
                    List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistPermissionFileId)
                                             select (long)row).ToList();
                    if (AllFileIDs.Count > 0)
                    {

                        foreach (var item in AllFileIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                   && row.UserID == UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            if (uploadedFileIds.Count > 0)
                            {
                                flagFileExist = true;
                                FileIDs.Add(CheckExistFilePermisssion);
                            }
                        }

                        foreach (var item in FileIDs)
                        {
                            long ExistFileID = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == ExistFileID && row.IsDeleted == false
                                                   && row.UserID != UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            foreach (var items in uploadedFileIds)
                            {
                                UserIds.Add(Convert.ToInt64(items.UserID));
                            }
                        }
                        if (UserIds.Count > 0)
                        {
                            UserIds = UserIds.Distinct().ToList();
                        }

                        int Count = Convert.ToInt32(FileIDs.Count);
                        foreach (var ids in UserIds)
                        {
                            long userIdCheck = Convert.ToInt64(ids);
                            int flag = 0;
                            foreach (var flds in FileIDs)
                            {
                                long fileids = Convert.ToInt64(flds);
                                UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                        where row.FileID == fileids && row.IsDeleted == false
                                                                        && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                        select row).FirstOrDefault();
                                if (uploadedFileIds != null)
                                {
                                    flag = flag + 1;
                                }
                            }
                            if (flag == Count)
                            {
                                FinaluserList.Add(Convert.ToInt64(userIdCheck));
                            }
                        }
                    }
                    #endregion

                    #region folder
                    if (flagFileExist == true)
                    {
                        long CreatedUserID = (from row in entities.Mst_FolderMaster
                                              where row.ID == ExistPermissionFileId && row.IsDeleted == false
                                              && row.CreatedBy == UserID && row.CustomerID == customerID
                                              select (long)row.CreatedBy).FirstOrDefault();
                        if (CreatedUserID > 0)
                        {
                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                       select (long)row).ToList();
                            if (AllFolderIDs.Count > 0)
                            {
                                int CountFolderIds = Convert.ToInt32(AllFolderIDs.Count);
                                foreach (var itemUserid in FinaluserList)//ckeck user 
                                {
                                    long userIdCheck = Convert.ToInt64(itemUserid);
                                    int flagFolder = 0;
                                    foreach (var itemFolderid in AllFolderIDs)//check folder
                                    {
                                        long folderids = Convert.ToInt64(itemFolderid);
                                        UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                                where row.FolderID == folderids && row.IsDeleted == false
                                                                                && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                                select row).FirstOrDefault();
                                        if (uploadedFileIds != null)
                                        {
                                            flagFolder = flagFolder + 1;
                                        }
                                    }
                                    if (flagFolder == CountFolderIds)
                                    {
                                        FinalOutputList.Add(Convert.ToInt64(userIdCheck));
                                    }
                                }
                            }
                        }
                        int Cust_Id = Convert.ToInt32(customerID);
                        foreach (var item in FinalOutputList)
                        {

                            var lstUsers = (from row in entities.UserCustomerViews
                                            where row.UserID == item && row.CustomerId == Cust_Id
                                            select row).FirstOrDefault();

                            if (lstUsers != null)
                            {
                                ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                            }
                        }
                    }
                    else
                    {
                        List<long> FinaluserOutputList = new List<long>();
                        List<long> UserFolderIDs = new List<long>();

                        long CreatedUserID = (from row in entities.Mst_FolderMaster
                                              where row.ID == ExistPermissionFileId && row.IsDeleted == false
                                              && row.CreatedBy == UserID && row.CustomerID == customerID
                                              select (long)row.CreatedBy).FirstOrDefault();
                        if (CreatedUserID > 0)
                        {
                            List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistPermissionFileId)
                                                       select (long)row).ToList();
                            if (AllFolderIDs.Count > 0)
                            {
                                foreach (var item in AllFolderIDs)
                                {
                                    long CheckExistFolderPermisssion = Convert.ToInt64(item);
                                    var uploadedFOlderUserIds = (from row in entities.UserFolderPermissions
                                                                 where row.FolderID == CheckExistFolderPermisssion && row.IsDeleted == false
                                                                 && row.CustomerID == customerID && row.FileID == null && row.UserID != UserID
                                                                 select row).ToList();
                                    foreach (var itemFld in uploadedFOlderUserIds)
                                    {
                                        long UIdFloder = Convert.ToInt64(itemFld.UserID);
                                        UserFolderIDs.Add(UIdFloder);
                                    }
                                }
                                if (UserFolderIDs.Count > 0)
                                {
                                    UserFolderIDs = UserFolderIDs.Distinct().ToList();
                                }

                                int CountFLD = Convert.ToInt32(AllFolderIDs.Count);
                                foreach (var ids in UserFolderIDs)
                                {
                                    long userIdCheck = Convert.ToInt64(ids);
                                    int flag = 0;
                                    foreach (var flds in AllFolderIDs)
                                    {
                                        long fileids = Convert.ToInt64(flds);
                                        UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                                where row.FolderID == fileids && row.IsDeleted == false
                                                                                && row.UserID == userIdCheck && row.CustomerID == customerID
                                                                                select row).FirstOrDefault();
                                        if (uploadedFileIds != null)
                                        {
                                            flag = flag + 1;
                                        }
                                    }
                                    if (flag == CountFLD)
                                    {
                                        FinaluserOutputList.Add(Convert.ToInt64(userIdCheck));
                                    }
                                }
                                int Cust_Id = Convert.ToInt32(customerID);
                                foreach (var item in FinaluserOutputList)
                                {
                                    var lstUsers = (from row in entities.UserCustomerViews
                                                    where row.UserID == item && row.CustomerId == Cust_Id
                                                    select row).FirstOrDefault();

                                    if (lstUsers != null)
                                    {
                                        ShareList.Add(new ShareDetail() { UserId = Convert.ToInt32(lstUsers.UserID), UserName = lstUsers.UserName.ToString(), UserPermissionFileId = Convert.ToInt64(ExistPermissionFileId), FileType = "Folder" });
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            
            myRepeater.DataSource = ShareList;
            myRepeater.DataBind();
        }
        protected void myRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "RemoveShare")
            {
                string Type = string.Empty;
                string[] str = e.CommandArgument.ToString().Split(',');
                long fileUploadPermission = Convert.ToInt64(str[0]);
                long USerIDAssign = Convert.ToInt64(str[1]);
                Type = Convert.ToString(str[2]);
                long customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);

                if (fileUploadPermission > 0)
                {
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    if (Type.Equals("File"))
                    {

                    }
                    else
                    {
                        #region File
                        List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int) fileUploadPermission)
                                                 select (long) row).ToList();

                        foreach (var item in AllFileIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                   && row.UserID == USerIDAssign && row.CreatedBy == UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            foreach (var itemx in uploadedFileIds)
                            {
                                long updatepermissionid = Convert.ToInt64(itemx.ID);
                                UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                   where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                                   && row.ID == updatepermissionid && row.UserID == USerIDAssign && row.CustomerID == customerID
                                                                   && row.CreatedBy == UserID
                                                                   select row).FirstOrDefault();
                                if (unsharedoc != null)
                                {
                                    unsharedoc.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion

                        #region Folder
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int) fileUploadPermission)
                                                   select (long) row).ToList();

                        foreach (var item in AllFolderIDs)
                        {
                            long CheckExistFilePermisssion = Convert.ToInt64(item);
                            var uploadedFolderIds = (from row in entities.UserFolderPermissions
                                                   where row.FolderID == CheckExistFilePermisssion && row.IsDeleted == false && row.FileID == null
                                                   && row.UserID == USerIDAssign && row.CreatedBy == UserID && row.CustomerID == customerID
                                                   select row).ToList();
                            foreach (var itemx in uploadedFolderIds)
                            {
                                long updatepermissionid = Convert.ToInt64(itemx.ID);
                                UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                   where row.FolderID == CheckExistFilePermisssion && row.IsDeleted == false && row.FileID == null
                                                                   && row.ID == updatepermissionid && row.UserID == USerIDAssign && row.CustomerID == customerID
                                                                   && row.CreatedBy == UserID
                                                                   select row).FirstOrDefault();
                                if (unsharedoc != null)
                                {
                                    unsharedoc.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
        }
        protected void grdFolderDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               // LinkButton btn = (LinkButton)(sender);
                lblAddnewFolder.InnerText = "Rename Folder";
                btnCreateFolder1.Text = "Rename";
                ViewState["Mode"] = 1;
                if (e.CommandName.Equals("Rename"))
                {
                    int FolderId = Convert.ToInt32(e.CommandArgument);
                    int rowFloderid = Convert.ToInt32(FolderId);

                    ViewState["ID"] = rowFloderid;
                    if (rowFloderid != 0)
                    {
                        var existingfname = GetOldData(rowFloderid);
                        txtFolderName.Text = existingfname.Name;
                        chkboxuniversediv.Visible = false;
                        universediv.Visible = false;
                        chkIsUnivers.Checked = false;
                        upMailDocument.Update();
                        //BindFolderData();
                        //bindPageNumber();
                        //SetShowingRecords();
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenpopup();", true);
                    }
                }
                if (e.CommandName.Equals("Goto_Subfolder"))
                {
                    long FolderID = Convert.ToInt64(e.CommandArgument);
                    string Filter = tbxFilter.Text.ToString();
                    Response.Redirect("~/Compliances/DocumentShareSubList.aspx?FolderID=" + FolderID + "&FilterSearch=" + Filter,false);
                }
                else if (e.CommandName.Equals("SubShareFile"))
                {
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    string[] str = e.CommandArgument.ToString().Split(',');
                    long FileID = Convert.ToInt64(str[0]);
                    ViewState["ExistFileIDShare"] = FileID;
                    ViewState["Fileorfolder"] = str[1];
                    bindSelecedUser(FileID);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenNewPermissionPopup();", true);
                }
                else if (e.CommandName.Equals("ShareFile"))
                {
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    string[] str = e.CommandArgument.ToString().Split(',');
                    long FileID = Convert.ToInt64(str[0]);
                    ViewState["ExistFileIDShare"] = FileID;
                    ViewState["Fileorfolder"] = str[1];
                    bindSelecedUser(FileID);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenNewPermissionPopup();", true);
                }
                else if (e.CommandName.Equals("DeleteDoc"))
                {
                    bool success = false;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long FileID = Convert.ToInt64(commandArgs[0]);
                    string Type = commandArgs[1];
                    if (Type.Equals("File"))
                    {
                        success = DeleteFileDocument(FileID);
                    }
                    else
                    {
                        success = DeleteFolderDocument(FileID);
                    }
                    if (success)
                    {
                        BindFolderData();
                        bindPageNumber();
                        SetShowingRecords();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdFolderDetail.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                        //cvContractDocument.ErrorMessage = "Something went wrong, Please try again";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {

                grdFolderDetail.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindFolderData();
                bindPageNumber();
                SetShowingRecords();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdFolderDetail.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvMailDocument.IsValid = false;
                cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public bool DeleteFolderDocument(long FolderPermissionId)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                string UserNameFolder = string.Empty;
                bool result = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    #region Folder Deleted 
                    Mst_FolderMaster folderpermisionCheck = (from row in entities.Mst_FolderMaster
                                                        where row.ID == FolderPermissionId && row.IsDeleted == false
                                                        && row.CustomerID == customerID && row.CreatedBy == UserID
                                                        select row).FirstOrDefault();
                    if (folderpermisionCheck != null)
                    {
                        List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int) FolderPermissionId)
                                                   select (long) row).ToList();
                        if (AllFolderIDs.Count > 0)
                        {
                            foreach (var item in AllFolderIDs)
                            {
                                var uploadedFileIds = (from row in entities.UserFolderPermissions
                                                       where row.FolderID == item && row.IsDeleted == false
                                                       && row.CreatedBy == UserID && row.FileID == null
                                                       select row).ToList();
                                foreach (var itemdelFolder in uploadedFileIds)
                                {
                                    long updatepermissionid = Convert.ToInt64(itemdelFolder.ID);
                                    UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                                       where row.FolderID == item && row.IsDeleted == false
                                                                       && row.ID == updatepermissionid && row.FileID == null
                                                                       && row.CreatedBy == UserID
                                                                       select row).FirstOrDefault();
                                    if (unsharedoc != null)
                                    {
                                        unsharedoc.IsDeleted = true;
                                        entities.SaveChanges();
                                    }
                                }
                            }                          
                        }
                    }
                    #endregion

                    #region file deleted inside folder
                    List<long> AllFileIDs = (from row in entities.sp_AllfilesofFolder((int) FolderPermissionId)
                                             select (long) row).ToList();

                    foreach (var item in AllFileIDs)
                    {
                        long CheckExistFilePermisssion = Convert.ToInt64(item);
                        var uploadedFileIds = (from row in entities.UserFolderPermissions
                                               where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                               && row.CreatedBy == UserID
                                               select row).ToList();
                        foreach (var itemx in uploadedFileIds)
                        {
                            long updatepermissionid = Convert.ToInt64(itemx.ID);
                            UserFolderPermission unsharedoc = (from row in entities.UserFolderPermissions
                                                               where row.FileID == CheckExistFilePermisssion && row.IsDeleted == false
                                                               && row.ID == updatepermissionid
                                                               && row.CreatedBy == UserID
                                                               select row).FirstOrDefault();
                            if (unsharedoc != null)
                            {
                                unsharedoc.IsDeleted = true;
                                entities.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    Mst_FolderMaster folderpermision = (from row in entities.Mst_FolderMaster
                                                        where row.ID == FolderPermissionId && row.IsDeleted == false
                                                        && row.CustomerID == customerID
                                                        select row).FirstOrDefault();
                    if (folderpermision != null)
                    {
                        if (folderpermision.CreatedBy == UserID)
                        {
                            folderpermision.IsDeleted = true;
                            entities.SaveChanges();
                            UserNameFolder = folderpermision.Name;
                            result = true;


                            if (result)
                            {
                                string FinalFolderPath = "~/DriveFiles/" + customerID + "/";

                                string DeleteFolder = FinalFolderPath + UserNameFolder;

                                string date = DateTime.Now.ToString("ddd MM.dd.yyyy");
                                string time = DateTime.Now.ToString("HH.mm tt");
                                string newname = Server.MapPath(DeleteFolder + "_Delete_" + date + "_" + time);

                                if (Directory.Exists(Server.MapPath(DeleteFolder)))
                                {
                                    Directory.Move(Server.MapPath(DeleteFolder), newname);
                                }
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool DeleteFileDocument(long FilePermissionId)
        {
            try
            {
                long customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                bool result = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    UserFolderPermission filepermision = (from row in entities.UserFolderPermissions
                                                          where row.ID == FilePermissionId && row.IsDeleted == false
                                                          && row.CustomerID == customerID && row.CreatedBy == UserID
                                                          select row).FirstOrDefault();
                    if (filepermision != null)
                    {
                        if (filepermision.CreatedBy == UserID)
                        {
                            filepermision.IsDeleted = true;
                            entities.SaveChanges();
                            result = true;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void grdFolderDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grdFolderDetail.Rows)
            {
                if (row.RowIndex == grdFolderDetail.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Click to select this row.";
                }
            }
        }
        protected void grdFolderDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                long customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long userID = -1;
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                //TextBox tbxCreateIDFile = (TextBox)e.Row.FindControl("tbxCreatedByValue");
                //ImageButton LnkShare = (ImageButton)e.Row.FindControl("LnkShare");
                //ImageButton LnkSubShare = (ImageButton)e.Row.FindControl("LnkSubShare");
                LinkButton lnkEditFolder = (LinkButton)e.Row.FindControl("lnkEditFolder");
                // TextBox tbxCreatedByValue = (TextBox)e.Row.FindControl("tbxCreatedByValue");
                TextBox folderID = (TextBox)e.Row.FindControl("folderID");

                var data = (from row in objFolder
                            where row.ID == Convert.ToInt32(folderID.Text)
                            select row).FirstOrDefault();

                if (data != null)
                {
                    lnkEditFolder.Visible = true;
                }
                else
                {
                    if (customerID == GetFolderCustId)
                    {
                        lnkEditFolder.Visible = true;
                    }
                    else
                    {
                        lnkEditFolder.Visible = false;
                    }
                }
                // if (tbxCreatedByValue.Text != null)
                //{
                //    long createdBy = Convert.ToInt64(tbxCreatedByValue.Text);
                //    if (createdBy == userID)
                //    {
                //        LnkShare.Visible = true;
                //    }
                //    else
                //    {
                //        LnkShare.Visible = false;
                //    }

                //    ComplianceDBEntities entities = new ComplianceDBEntities();

                //    TextBox tbxIDFile = (TextBox)e.Row.FindControl("tbxIDFile");
                //    long FID = Convert.ToInt64(tbxIDFile.Text);

                //    UserFolderPermission objPermissionFile = (from row in entities.UserFolderPermissions
                //                                              where row.FolderID == FID
                //                                                   && row.CustomerID == customerID
                //                                                   && row.UserID == userID
                //                                                   && row.IsDeleted == false
                //                                              select row).FirstOrDefault();
                //    if (objPermissionFile != null)
                //    {
                //        if (objPermissionFile.IsRead == true && objPermissionFile.IsWrite == true)
                //        {
                //            LnkSubShare.Visible = true;
                //        }
                //        else
                //        {
                //            LnkSubShare.Visible = false;
                //        }
                //    }
                //}
                //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdFolderDetail, "Select$" + e.Row.RowIndex);
                //e.Row.ToolTip = "Click to select this row.";
            } 
        }
        public static List<User> GetAllUsers(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             select row).ToList();

                return query.ToList();
            }
        }
        public static Mst_FolderMaster GetOldData(int remarkId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.Mst_FolderMaster
                               where row.ID == remarkId
                               select row).SingleOrDefault();

                return details;
            }
        }
        protected void lnkEditFolder_Click(object sender, EventArgs e)
        {   
                try
                {
                //LinkButton btn = (LinkButton)(sender);
                //lblAddnewFolder.InnerText = "Rename Folder";
                //btnCreateFolder.Text = "Rename";
                //ViewState["Mode"] = 1;
                //int remarkId = Convert.ToInt32(btn.CommandArgument);
                //int rkid = Convert.ToInt32(remarkId);
                //if (remarkId != 0)
                //{
                //    var existingremark = GetOldData(rkid);
                //    txtFolderName.Text = existingremark.Name;
                //}
                upPromotorList.Update();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Server Error Occured. Please try again.";
                }
               
        }
        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = GetAllUsers(customerID);
                var lstUsers = (from row in lstAllUsers
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                lstBoxUser.Items.Clear();
                lstBoxUser.DataValueField = "ID";
                lstBoxUser.DataTextField = "Name";
                lstBoxUser.DataSource = lstUsers;
                lstBoxUser.DataBind();

                lstBoxUserforUnivers.Items.Clear();
                lstBoxUserforUnivers.DataValueField = "ID";
                lstBoxUserforUnivers.DataTextField = "Name";
                lstBoxUserforUnivers.DataSource = lstUsers;
                lstBoxUserforUnivers.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnPermission_Click(object sender, EventArgs e)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                long UserID = Convert.ToInt64(AuthenticationHelper.UserID);
                bool IsViewFile = false;
                bool IsReadFile = false;
                bool IsWriteFile = false;
                bool saveSuccess = false;
                List<long> lstUserMapping = new List<long>();
                List<long> lstUserRemoveMapping = new List<long>();
                long customerID = -1;
                customerID = Convert.ToInt64(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["ExistFileIDShare"] != null)
                {
                    long ExistFileID = Convert.ToInt64(ViewState["ExistFileIDShare"]);
                    if (ViewState["Fileorfolder"].ToString().ToLower() == "file")
                    {
                    }
                    else
                    {
                        #region Userlist
                        foreach (ListItem eachUser in lstBoxUser.Items)
                        {
                            if (eachUser.Selected)
                            {
                                if (Convert.ToInt32(eachUser.Value) > 0)
                                {
                                    lstUserMapping.Add(Convert.ToInt64(eachUser.Value));
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(eachUser.Value) > 0)
                                {
                                    lstUserRemoveMapping.Add(Convert.ToInt64(eachUser.Value));
                                }

                            }
                        }
                        #endregion

                        int IsUniverse = CheckIsUniverse(Convert.ToInt32(ExistFileID), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (IsUniverse == 1)
                        {
                            #region universe
                            if (lstUserMapping.Count > 0)
                            {
                                #region Permission
                                if (ddlPermission1.SelectedValue != null && Convert.ToInt32(ddlPermission1.SelectedValue) > 0)
                                {
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 4)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = false;
                                        IsWriteFile = false;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 1)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = true;
                                        IsWriteFile = false;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 2)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = false;
                                        IsWriteFile = true;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 3)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = true;
                                        IsWriteFile = true;
                                    }
                                }
                                #endregion

                                #region Folder
                                saveSuccess = false;
                                long CreatedUserID = UserID;
                                //long CreatedUserID = (from row in entities.Mst_FolderMaster
                                //                      where row.ID == ExistFileID && row.IsDeleted == false
                                //                      && row.CreatedBy == UserID && row.CustomerID == customerID
                                //                      select (long)row.CreatedBy).FirstOrDefault();
                                if (CreatedUserID > 0)
                                {
                                    List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistFileID)
                                                               select (long)row).ToList();
                                    List<UserFolderPermission> lstUSerMapping_ToSaveFolder = new List<UserFolderPermission>();
                                    foreach (var itemf in AllFolderIDs)
                                    {
                                        long assignFolderId = Convert.ToInt64(itemf);

                                        lstUserMapping.ForEach(EachUSer =>
                                        {
                                            UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                          where row.FolderID == assignFolderId && row.IsDeleted == false
                                                                                          && row.UserID == EachUSer
                                                                                          && row.FileID == null
                                                                                          && row.CustomerID == customerID
                                                                                          select row).FirstOrDefault();

                                            if (CheckExistFileAssigne != null)
                                            {
                                                CheckExistFileAssigne.IsView = IsViewFile;
                                                CheckExistFileAssigne.IsRead = IsReadFile;
                                                CheckExistFileAssigne.IsWrite = IsWriteFile;
                                                CheckExistFileAssigne.IsDeleted = false;
                                                entities.SaveChanges();
                                            }
                                            else
                                            {
                                                UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                {
                                                    FolderID = Convert.ToInt64(assignFolderId),
                                                    FileID = null,
                                                    CreatedBy = Convert.ToInt64(CreatedUserID),
                                                    CreatedOn = DateTime.Now,
                                                    IsRead = IsReadFile,
                                                    IsView = IsViewFile,
                                                    IsWrite = IsWriteFile,
                                                    IsDeleted = false,
                                                    UserID = Convert.ToInt64(EachUSer),
                                                    CustomerID = Convert.ToInt64(customerID),
                                                    SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                };
                                                lstUSerMapping_ToSaveFolder.Add(_UserMappingRecord);
                                            }
                                        });
                                    }
                                    if (lstUSerMapping_ToSaveFolder.Count > 0)
                                    {
                                        lstUSerMapping_ToSaveFolder.ForEach(eachRecord =>
                                        {
                                            saveSuccess = false;
                                            entities.UserFolderPermissions.Add(eachRecord);
                                            entities.SaveChanges();
                                            saveSuccess = true;
                                        });
                                    }
                                }
                                else
                                {
                                    UserFolderPermission updatedPermission = (from row in entities.UserFolderPermissions
                                                                              where row.CustomerID == customerID
                                                                              && row.FolderID == ExistFileID
                                                                              && row.IsDeleted == false
                                                                              && row.FileID == null
                                                                              && row.UserID == UserID
                                                                              select row).FirstOrDefault();
                                    if (updatedPermission != null)
                                    {
                                        if (updatedPermission.IsRead == true && updatedPermission.IsWrite == true)
                                        {

                                        }
                                    }
                                }
                                #endregion

                                #region File

                                List<long> FileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistFileID)
                                                      select (long)row).ToList();

                                List<UserFolderPermission> lstUSerMapping_ToSave = new List<UserFolderPermission>();
                                foreach (var item in FileIDs)
                                {
                                    long FileIds = Convert.ToInt64(item);

                                    UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                            where row.FileID == FileIds && row.IsDeleted == false
                                                                            && row.UserID == UserID && row.CustomerID == customerID
                                                                            select row).FirstOrDefault();
                                    if (uploadedFileIds != null)
                                    {
                                        lstUserMapping.ForEach(EachUSer =>
                                        {
                                            UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                          where row.FileID == FileIds && row.IsDeleted == false
                                                                                          && row.UserID == EachUSer && row.CustomerID == customerID
                                                                                          select row).FirstOrDefault();

                                            if (CheckExistFileAssigne != null)
                                            {

                                            }
                                            else
                                            {
                                                UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                {
                                                    FolderID = Convert.ToInt64(uploadedFileIds.FolderID),
                                                    FileID = Convert.ToInt64(FileIds),
                                                    CreatedBy = Convert.ToInt64(uploadedFileIds.CreatedBy),
                                                    CreatedOn = DateTime.Now,
                                                    IsRead = IsReadFile,
                                                    IsWrite = IsWriteFile,
                                                    IsView = IsViewFile,
                                                    IsDeleted = false,
                                                    UserID = Convert.ToInt64(EachUSer),
                                                    CustomerID = Convert.ToInt64(customerID),
                                                    SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                };
                                                lstUSerMapping_ToSave.Add(_UserMappingRecord);
                                            }
                                        });
                                    }
                                }
                                if (lstUSerMapping_ToSave.Count > 0)
                                {
                                    lstUSerMapping_ToSave.ForEach(eachRecord =>
                                    {
                                        saveSuccess = false;
                                        entities.UserFolderPermissions.Add(eachRecord);
                                        entities.SaveChanges();
                                        saveSuccess = true;
                                    });
                                }
                                if (saveSuccess)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforuserupdate();", true);
                                    // saveSuccess = true;
                                }

                                #endregion
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforusernotselect();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region normal
                            if (lstUserMapping.Count > 0)
                            {
                                #region Permission
                                if (ddlPermission1.SelectedValue != null && Convert.ToInt32(ddlPermission1.SelectedValue) > 0)
                                {
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 4)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = false;
                                        IsWriteFile = false;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 1)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = true;
                                        IsWriteFile = false;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 2)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = false;
                                        IsWriteFile = true;
                                    }
                                    if (Convert.ToInt32(ddlPermission1.SelectedValue) == 3)
                                    {
                                        IsViewFile = true;
                                        IsReadFile = true;
                                        IsWriteFile = true;
                                    }
                                }
                                #endregion

                                #region Folder
                                saveSuccess = false;
                                long CreatedUserID = (from row in entities.Mst_FolderMaster
                                                      where row.ID == ExistFileID && row.IsDeleted == false
                                                      && row.CreatedBy == UserID && row.CustomerID == customerID
                                                      select (long)row.CreatedBy).FirstOrDefault();
                                if (CreatedUserID > 0)
                                {
                                    List<long> AllFolderIDs = (from row in entities.sp_AllFolder((int)ExistFileID)
                                                               select (long)row).ToList();
                                    List<UserFolderPermission> lstUSerMapping_ToSaveFolder = new List<UserFolderPermission>();
                                    foreach (var itemf in AllFolderIDs)
                                    {
                                        long assignFolderId = Convert.ToInt64(itemf);

                                        lstUserMapping.ForEach(EachUSer =>
                                        {
                                            UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                          where row.FolderID == assignFolderId && row.IsDeleted == false
                                                                                          && row.UserID == EachUSer
                                                                                          && row.FileID == null
                                                                                          && row.CustomerID == customerID
                                                                                          select row).FirstOrDefault();

                                            if (CheckExistFileAssigne != null)
                                            {
                                                CheckExistFileAssigne.IsView = IsViewFile;
                                                CheckExistFileAssigne.IsRead = IsReadFile;
                                                CheckExistFileAssigne.IsWrite = IsWriteFile;
                                                CheckExistFileAssigne.IsDeleted = false;
                                                entities.SaveChanges();
                                            }
                                            else
                                            {
                                                UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                {
                                                    FolderID = Convert.ToInt64(assignFolderId),
                                                    FileID = null,
                                                    CreatedBy = Convert.ToInt64(CreatedUserID),
                                                    CreatedOn = DateTime.Now,
                                                    IsRead = IsReadFile,
                                                    IsView = IsViewFile,
                                                    IsWrite = IsWriteFile,
                                                    IsDeleted = false,
                                                    UserID = Convert.ToInt64(EachUSer),
                                                    CustomerID = Convert.ToInt64(customerID),
                                                    SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                };
                                                lstUSerMapping_ToSaveFolder.Add(_UserMappingRecord);
                                            }
                                        });
                                    }
                                    if (lstUSerMapping_ToSaveFolder.Count > 0)
                                    {
                                        lstUSerMapping_ToSaveFolder.ForEach(eachRecord =>
                                        {
                                            saveSuccess = false;
                                            entities.UserFolderPermissions.Add(eachRecord);
                                            entities.SaveChanges();
                                            saveSuccess = true;
                                        });
                                    }
                                }
                                else
                                {
                                    UserFolderPermission updatedPermission = (from row in entities.UserFolderPermissions
                                                                              where row.CustomerID == customerID
                                                                              && row.FolderID == ExistFileID
                                                                              && row.IsDeleted == false
                                                                              && row.FileID == null
                                                                              && row.UserID == UserID
                                                                              select row).FirstOrDefault();
                                    if (updatedPermission != null)
                                    {
                                        if (updatedPermission.IsRead == true && updatedPermission.IsWrite == true)
                                        {

                                        }
                                    }
                                }
                                #endregion

                                #region File

                                List<long> FileIDs = (from row in entities.sp_AllfilesofFolder((int)ExistFileID)
                                                      select (long)row).ToList();

                                List<UserFolderPermission> lstUSerMapping_ToSave = new List<UserFolderPermission>();
                                foreach (var item in FileIDs)
                                {
                                    long FileIds = Convert.ToInt64(item);

                                    UserFolderPermission uploadedFileIds = (from row in entities.UserFolderPermissions
                                                                            where row.FileID == FileIds && row.IsDeleted == false
                                                                            && row.UserID == UserID && row.CustomerID == customerID
                                                                            select row).FirstOrDefault();
                                    if (uploadedFileIds != null)
                                    {
                                        lstUserMapping.ForEach(EachUSer =>
                                        {
                                            UserFolderPermission CheckExistFileAssigne = (from row in entities.UserFolderPermissions
                                                                                          where row.FileID == FileIds && row.IsDeleted == false
                                                                                          && row.UserID == EachUSer && row.CustomerID == customerID
                                                                                          select row).FirstOrDefault();

                                            if (CheckExistFileAssigne != null)
                                            {

                                            }
                                            else
                                            {
                                                UserFolderPermission _UserMappingRecord = new UserFolderPermission()
                                                {
                                                    FolderID = Convert.ToInt64(uploadedFileIds.FolderID),
                                                    FileID = Convert.ToInt64(FileIds),
                                                    CreatedBy = Convert.ToInt64(uploadedFileIds.CreatedBy),
                                                    CreatedOn = DateTime.Now,
                                                    IsRead = IsReadFile,
                                                    IsWrite = IsWriteFile,
                                                    IsView = IsViewFile,
                                                    IsDeleted = false,
                                                    UserID = Convert.ToInt64(EachUSer),
                                                    CustomerID = Convert.ToInt64(customerID),
                                                    SubShareUserID = Convert.ToInt32(AuthenticationHelper.UserID)
                                                };
                                                lstUSerMapping_ToSave.Add(_UserMappingRecord);
                                            }
                                        });
                                    }
                                }
                                if (lstUSerMapping_ToSave.Count > 0)
                                {
                                    lstUSerMapping_ToSave.ForEach(eachRecord =>
                                    {
                                        saveSuccess = false;
                                        entities.UserFolderPermissions.Add(eachRecord);
                                        entities.SaveChanges();
                                        saveSuccess = true;
                                    });
                                }
                                if (saveSuccess)
                                {
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforuserupdate();", true);
                                    // saveSuccess = true;
                                }

                                #endregion
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "msgforusernotselect();", true);
                            }
                            #endregion
                        }
                    }
                    lstBoxUser.ClearSelection();
                    foreach (ListItem listItem in lstBoxUser.Items)
                    {
                        listItem.Selected = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int CheckIsUniverse(int FolderID, int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Mst_FolderMaster
                            where row.CustomerID == CID
                            && row.ID == FolderID
                            select row.IsUniverse).FirstOrDefault();
                return data;
            }
        }
        private void SetShowingRecords()
        {
            if (Session["TotalDocShareListRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalDocShareListRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        //protected void grdFolderDetail_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (customerID == GetFolderCustId)
        //    {
        //        e.Row.Cells[4].Visible = true;
        //    }
        //    else
        //    {
        //        e.Row.Cells[4].Visible = false;
        //    }

        //}
    }
}


