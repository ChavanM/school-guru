﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="CustomerAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CustomerAssignment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript">
        function initializeCombobox() {
        }
        function Validate(sender, args) {
            debugger
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCustomer']:checked").length
            if (rowCheckBoxSelected == 0 || rowCheckBoxSelected == "undefined") {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .ui-state-active {
            border: 1px solid #fad42e !important;
            background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
            color: #363636 !important;
        }

        .ui-datepicker-div {
            top: 482.453px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <div style="margin-bottom: 4px">
        <asp:ValidationSummary runat="server" CssClass="vdsummary"
            ValidationGroup="ComplianceInstanceValidationGroup" />
        <asp:CustomValidator ID="cvDuplicateEntry2" runat="server" EnableClientScript="False" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
    </div>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upWidgetList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" style="width: 20%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" /></td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddWidget" OnClick="btnAddWidget_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Sr No">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="UserName" HeaderText="User Name" ItemStyle-VerticalAlign="Top" ItemStyle-Width="900px" />
                    <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ItemStyle-VerticalAlign="Top" ItemStyle-Width="900px" />
                   
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Widget" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Widget" title="Edit User" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Widget" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Assignment?');"><img src="../Images/delete_icon.png" alt="Delete Widget" title="Delete User" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divActDialog" style="height: auto;">
        <asp:UpdatePanel ID="upWidget" runat="server" UpdateMode="Conditional" OnLoad="upWidget_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="CustomDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User
                        </label>
                          <asp:DropDownList runat="server" ID="ddlNewUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select User." ControlToValidate="ddlNewUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>

                    <%--  customer --%>
                   
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer
                        </label>
                           <%-- <div>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one Customer."
                                ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="ActValidationGroup" Display="None"></asp:CustomValidator>
                        </div>--%>
                        <asp:TextBox runat="server" ID="txtComplince" Style="padding: 0px; margin: 0px; height: 30px; width: 690px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; width: 690px;" id="dvCompliance">
                            <asp:Repeater ID="rptCustomers" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterSubTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="CustomerSelectAll" Text=" All"
                                                    runat="server" onclick="checkAllCustomer(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeatersub" Text="Ok" Style="float: left"  /></td>
                                   </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkCustomer" runat="server" onclick="UncheckHeaderCustomer();" /></td>
                                        <td style="width: 560px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px; padding-bottom: 5px;">
                                                <asp:Label ID="lblcustomerid" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblcustomerName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div id="DivSave" style="margin-bottom: 7px; margin-top: 4%; margin-left: 300px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave"
                            OnClick="btnSave_Click" CssClass="button" ValidationGroup="ActValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divActDialog').dialog({
                width: 900,
                height: 400,
                autoOpen: false,
                draggable: true,
                title: "Customer Assignment",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divActDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divActDialog").dialog('open');

        }
        function initializeCombobox() {
           <%-- $("#<%= ddlCategory.ClientID %>").combobox();
            $("#<%= ddlType.ClientID %>").combobox();
            $("#<%= ddlLocationType.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
            $("#<%= ddlCity.ClientID %>").combobox();
            $("#<%= ddlDepartment.ClientID %>").combobox();
            $("#<%= ddlMinistry.ClientID %>").combobox();
            $("#<%= ddlRegulator.ClientID %>").combobox();
            $("#<%= ddlActDocType.ClientID %>").combobox();--%>
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

    
     
        function UncheckHeaderCustomer() {
            var rowCheckBox = $("#RepeaterSubTable input[id*='chkCustomer']");
            var rowCheckBoxSelected = $("#RepeaterSubTable input[id*='chkCustomer']:checked");
            var rowCheckBoxHeader = $("#RepeaterSubTable input[id*='CustomerSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllCustomer(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCustomer") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function initializeJQueryUI1(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
