﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ActAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ActAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to save data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>


    <style type="text/css">
        .box {
            width: 100%;
        }
        .td1 {
            width: 15%;
        }
        .td2 {
            width: 34%;
        }
        .td3 {
            width: 15%;
        }
        .td4 {
            width: 34%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Add" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="View" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                    OnClick="Tab2_Click" />
                <asp:Button Text="Re-Assign" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional" OnLoad="upActList_Load">
                            <ContentTemplate>
                                <table class="box">
                                    <tr>
                                        <td colspan="4">
                                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                                ValidationGroup="ComplianceValidationGroup" />
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Customer</label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlCustomer" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                                                Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                                        </td>
                                        <td class="td3">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select User</label></td>
                                        <td class="td4">
                                            <asp:DropDownList runat="server" ID="ddlPerformer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Performer."
                                                ControlToValidate="ddlPerformer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroup" Display="None" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Catagory</label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlfilterCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterCatagory_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="td3">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Type</label>
                                        </td>
                                        <td class="td4">
                                            <asp:DropDownList runat="server" ID="ddlFilterType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                &nbsp;</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Filter :</label>

                                        </td>
                                        <td class="td2">
                                            <asp:TextBox runat="server" ID="tbxFilter" Width="390px" MaxLength="50" AutoPostBack="true"
                                                OnTextChanged="tbxFilter_TextChanged" />
                                        </td>
                                        <td class="td3"></td>
                                        <td class="td4"></td>

                                    </tr>
                                    <tr>
                                        <td class="td1" valign="top">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Act Name</label>
                                        </td>

                                        <td colspan="3">
                                            <asp:Panel ID="Panel2" Width="1060px" Height="350px" ScrollBars="Both" runat="server">
                                                <asp:CheckBoxList ID="ActCheckboxList" runat="server" AutoPostBack="false"></asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="padding-left: 280px; padding-top: 30px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                                                ValidationGroup="ComplianceValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <asp:UpdatePanel ID="upActListView" runat="server" UpdateMode="Conditional" OnLoad="upActList_Load">
                            <ContentTemplate>
                                <table class="box">
                                    <tr>
                                        <td colspan="4">
                                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                                ValidationGroup="ComplianceValidationGroupView" />
                                            <asp:CustomValidator ID="cvDuplicateEntryView" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroupView" Display="None" />
                                            <asp:Label runat="server" ID="lblErrorMassageView" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Customer</label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlCustomerView"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerView_SelectedIndexChanged"
                                                Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomerView" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroupView" Display="None" />
                                        </td>
                                        <td class="td3">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                &nbsp;</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Filter :</label>
                                        </td>
                                        <td class="td4">
                                            <asp:TextBox runat="server" ID="tbxFilterView" Width="390px" MaxLength="50" AutoPostBack="true"
                                                OnTextChanged="tbxFilterView_TextChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Both" runat="server">
                                                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="200" Width="100%"
                                                    Font-Size="12px" DataKeyNames="ActId" OnRowCommand="grdCompliances_RowCommand"
                                                    OnPageIndexChanging="grdCompliances_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField DataField="ActId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10px" />
                                                        <asp:TemplateField HeaderText="Act Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="300px">
                                                            <ItemTemplate>
                                                                <div>
                                                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50px">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ActId") %>'
                                                                    OnClientClick="return confirm('Are you certain you want to delete this Assignment?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#CCCC99" />
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                    <PagerSettings Position="Top" />
                                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                    <EmptyDataTemplate>
                                                        No Records Found.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <asp:UpdatePanel ID="upActListAssignment" runat="server" UpdateMode="Conditional" OnLoad="upActList_Load">
                            <ContentTemplate>
                                <table class="box">
                                    <tr>
                                        <td colspan="4">
                                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                                ValidationGroup="ComplianceValidationGroupAssignment" />
                                            <asp:CustomValidator ID="cvDuplicateEntryAssignment" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroupAssignment" Display="None" />
                                            <asp:Label runat="server" ID="lblErrorMassageAssignment" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Customer</label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlCustomerModifyAssignment"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerModifyAssignment_SelectedIndexChanged"
                                                Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please Select Customer."
                                                ControlToValidate="ddlCustomerModifyAssignment" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroupAssignment" Display="None" />
                                        </td>
                                        <td class="td3">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Select Current User</label></td>
                                        <td class="td4">
                                            <asp:DropDownList runat="server" ID="ddlCurrentUser"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlCurrentUser_SelectedIndexChanged"
                                                Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please Select Current User."
                                                ControlToValidate="ddlCurrentUser" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="ComplianceValidationGroupAssignment" Display="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                New User</label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlNewUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                                CssClass="txtbox" />
                                            <asp:CompareValidator ErrorMessage="Please select New User." ControlToValidate="ddlNewUsers"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroupAssignment"
                                                Display="None" />
                                        </td>
                                        <td class="td3">
                                            <label style="width: 10px; font-size: 13px; color: red;">
                                                &nbsp;</label>
                                            <label style="width: 200px; font-size: 13px; color: #333;">
                                                Filter :</label>
                                        </td>
                                        <td class="td4">
                                            <asp:TextBox runat="server" ID="tbxFilterModifyAssignment" Width="390px" MaxLength="50" AutoPostBack="true"
                                                OnTextChanged="tbxFilterModifyAssignment_TextChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Panel ID="Panel3" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                                                <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                                                    GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true"
                                                    BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px"
                                                    OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                                                    DataKeyNames="ActId">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkCompliancesHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkCompliances" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Act Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="600px">
                                                            <ItemTemplate>
                                                                <div>
                                                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#CCCC99" />
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                    <PagerSettings Position="Top" />
                                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSaveAssignment_Click"
                                                CssClass="button" ValidationGroup="ComplianceValidationGroupAssignment" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            var gvcheck = null;
            if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
             var i;
             if (headerchk.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }
         }

         function initializeJQueryUI(textBoxID, divID) {
             $("#" + textBoxID).unbind('click');

             $("#" + textBoxID).click(function () {
                 $("#" + divID).toggle("blind", null, 500, function () { });
             });
         }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function initializeCombobox() {
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlCustomerView.ClientID %>").combobox();
            $("#<%= ddlPerformer.ClientID %>").combobox();
            $("#<%= ddlfilterCatagory.ClientID %>").combobox();
            $("#<%= ddlFilterType.ClientID %>").combobox();
            $("#<%= ddlCustomerModifyAssignment.ClientID %>").combobox();
            $("#<%= ddlCurrentUser.ClientID %>").combobox();
            $("#<%= ddlNewUsers.ClientID %>").combobox();
        }
    </script>
</asp:Content>

