﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="Certificatecompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Certificatecompliance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
     <script type="text/javascript">
      
         function initializeJQueryUI(textBoxID, divID) {
             $("#" + textBoxID).unbind('click');

             $("#" + textBoxID).click(function () {
                 $("#" + divID).toggle("blind", null, 500, function () { });
             });
         }
         ddlFilterDepartment
        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
      
        function hideDivBranch() {
            //$('#divFilterLocation').hide("blind", null, 500, function () { });
        }
       

      
    </script>
   
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="div1" runat="server" style="margin-left:989px;">
        <asp:Button ID="btnExport" runat="server"  Text="Export"  CssClass="btn btn-primary"   OnClick="ExportToWord" ValidationGroup="ComplianceValidationGroup" />
    </div>
     
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div>
                <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                            <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                    <asp:ValidationSummary runat="server" 
                        ValidationGroup="ComplianceValidationGroup" class="alert alert-block alert-danger fade in"/>
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" ></asp:Label>
                                </asp:Panel>
                </div>
               
           <div class="row col-lg-12 col-md-12" style="margin-top: 5px;">
               <%-- <div class="form-group col-md-3">--%>
                    <div class="form-group col-md-3" style="margin-left: -28px;">
                        <asp:DropDownList runat="server" ID="ddlComplianceCatagory" class="form-control"
                           Width="100%" AutoPostBack="true">
                        </asp:DropDownList>
                         <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Compliance category." ControlToValidate="ddlComplianceCatagory"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div class="form-group col-md-2" style="margin-left: -14px;width: 207px; display:none;">
                        <asp:DropDownList runat="server" ID="ddlFilterDepartment" class="form-control"
                           AutoPostBack="true">
                        </asp:DropDownList>
                      <%--   <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select department." ControlToValidate="ddlFilterDepartment"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div class="form-group col-md-3" style="margin-left: -14px;">

                        <asp:TextBox runat="server" ID="tbxFilterLocation" class="form-control" />
                             
                           
                        <div style="margin-left: 30px; position: absolute; z-index: 10" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="250px"
                                Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                         <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Sub-Entity/Location."
                                        ControlToValidate="tbxFilterLocation" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div class="form-group col-md-2" style="margin-left: -14px;">
                         <asp:TextBox runat="server"  placeholder="From Date" class="form-control" ID="txtStartDate" CssClass="StartDate" />
                         <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                            ControlToValidate="txtStartDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div class="form-group col-md-2">
                        <asp:TextBox runat="server"  placeholder="To Date" class="form-control" ID="txtEndDate" CssClass="StartDate" />
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select End Date."
                            ControlToValidate="txtEndDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />

                    </div>
                <%--</div>--%>
     </div>
        </ContentTemplate>

    </asp:UpdatePanel>
 
    
</asp:Content>
