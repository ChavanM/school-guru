﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventDeactivation : System.Web.UI.Page
    {
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                UserID = AuthenticationHelper.UserID;
                BindCustomer(UserID);
               // BindEvent();
               // BindEventNature();
                tbxFilterLocation.Text = "< Select >";
                tbxFilterLocation.Attributes.Add("readonly", "readonly");
                
            }
        }
        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
             
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomer(int UserID)
        {
            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";
            ddlCustomer.DataSource = GetCustomer(UserID);
            ddlCustomer.DataBind();
            ddlCustomer.Items.Insert(0, new ListItem("< Select Customer>", "-1"));
        }

        public static List<object> GetCustomer(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventInstances
                            join row1 in entities.EventScheduleOns
                            on row.ID equals row1.EventInstanceID
                            join row2 in entities.CustomerBranches
                            on row.CustomerBranchID equals row2.ID
                            join row3 in entities.Customers
                            on row2.CustomerID equals row3.ID
                            join row4 in entities.CustomerAssignmentDetails
                            on row3.ID equals row4.CustomerID
                            orderby row3.Name ascending
                            where row.IsDeleted == false &&
                            row1.IsDeleted == false && row2.IsDeleted == false
                            &&row4.IsDeleted==false
                            && row4.UserID==UserID
                            select new
                            {
                                row3.ID,
                                row3.Name
                            }).Distinct().OrderBy(entry => entry.Name).ToList<object>();
                return data;

            }
        }
        private void BindEvent(int branchid)
        {
           
            ddlEvent.DataTextField = "Name";
            ddlEvent.DataValueField = "ID";
            ddlEvent.DataSource = EventManagement.GetEventNameBy(branchid);
            ddlEvent.DataBind();
            ddlEvent.Items.Insert(0, new ListItem("< Select Event>", "-1"));
        }
        private void BindEventNature(long eventid)
        {
            ddlEventNature.DataTextField = "Name";
            ddlEventNature.DataValueField = "ID";
            ddlEventNature.DataSource = EventManagement.GetEventNature(eventid);
            ddlEventNature.DataBind();
            ddlEventNature.Items.Insert(0, new ListItem("< Select Event Nature>", "-1"));
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            tvFilterLocation.Nodes.Clear();
            BindLocationFilter();
        }
        private void BindLocationFilter()
        {
            try
            {
                long customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                var bracnhes = CustomerBranchManagement.GetAllHierarchyEventBased(Convert.ToInt32(customerID));
                foreach(var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "< Select Location >";
            int Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
             BindEvent(Locbranchid);
        }
        protected void ddlEvent_SelectedIndesxChanged(object sender, EventArgs e)
        {
            long eventid = Convert.ToInt32(ddlEvent.SelectedValue);
            BindEventNature(eventid);
        }
       protected  void ddlEventNature_SelectedIndexChanged(object sender ,EventArgs e)
        {
            grdEvent.DataSource = null;
            grdEvent.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(ddlEvent.SelectedValue) && !string.IsNullOrEmpty(ddlEventNature.SelectedValue) && !string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    int eventid = Convert.ToInt32(ddlEvent.SelectedValue);
                    int eventnatureid = Convert.ToInt32(ddlEventNature.SelectedValue);
                    int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    int Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    entities.Database.CommandTimeout = 120;
                    var EventList = (from row in entities.SP_GetEventDeactivate(customerID, Locbranchid, eventid, eventnatureid)
                                     select row).ToList();
                    grdEvent.DataSource = EventList;
                    grdEvent.DataBind();

                }

            }
         }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                int eventid = -1;
                int eventnatureid = -1;
                int Locbranchid = -1;
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlEvent.SelectedValue))
                {
                    eventid = Convert.ToInt32(ddlEvent.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlEventNature.SelectedValue))
                {
                    eventnatureid = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (eventid != -1 && eventnatureid != -1 && Locbranchid != -1 && customerID!=-1)
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 120;
                        var data = (from row in entities.SP_GetEventDeactivate(customerID, Locbranchid, eventid, eventnatureid)

                                    select row).ToList();
                                                   //.GroupBy(entity => entity.EventScheduleID).Select(entity => entity.FirstOrDefault()).ToList();
                        data.ForEach(eventsheduledata =>
                        {
                            var eventinfo = (from row in entities.EventScheduleOns
                                             where row.ID== eventsheduledata.EventScheduleID
                                             select row).ToList();
                            if (eventinfo.Count > 0)
                            {
                                CreateLogData(Convert.ToInt32(eventsheduledata.EventScheduleID),Convert.ToInt32(eventsheduledata.ComplianceScheduleID));
                                UpdateEventscheduledata(eventsheduledata.EventScheduleID);
                                UpdateCompliancescheduledata(eventsheduledata.ComplianceScheduleID);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data updated successfully.";
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data not found.";
                            }
                            
                        });
                    }
                }
                upEvent.Update();
                grdEvent.DataSource = null;
                grdEvent.DataBind();
                ddlCustomer.SelectedValue = "-1";
                ddlEvent.SelectedValue = "-1";
                ddlEventNature.SelectedValue = "-1";
                tbxFilterLocation.Text = "< Select >";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        public void CreateLogData(int eventscheduleid,int compliancescheduleid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    LogEventDeactivation log = new LogEventDeactivation()
                    {
                        EventscheduleonId= eventscheduleid,
                        CompliancescheduleonID= compliancescheduleid,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                    };

                    entities.LogEventDeactivations.Add(log);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
           
           
        public static int UpdateCompliancescheduledata(long comscheduleid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn comdata = (from row in entities.ComplianceScheduleOns
                                             where row.ID == comscheduleid
                                             select row).SingleOrDefault();
                comdata.IsActive= false;
                comdata.IsUpcomingNotDeleted = false;
                entities.SaveChanges();
                return Convert.ToInt32(comdata.ID);
            }
        }
        public static int UpdateEventscheduledata(long eventscheduleid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventScheduleOn eventdata = (from row in entities.EventScheduleOns
                                             where row.ID == eventscheduleid
                                             select row).SingleOrDefault();
                eventdata.IsDeleted = true;
                entities.SaveChanges();
                return Convert.ToInt32(eventdata.ID);
            }
        }

        protected void btnKeep_Click(object sender, EventArgs e)
        {
            try
            {
                int eventid = -1;
                int eventnatureid = -1;
                int Locbranchid = -1;
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlEvent.SelectedValue))
                {
                    eventid = Convert.ToInt32(ddlEvent.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlEventNature.SelectedValue))
                {
                    eventnatureid = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (eventid != -1 && eventnatureid != -1 && Locbranchid != -1 && customerID != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 120;
                        var data = (from row in entities.SP_GetEventDeactivate(customerID, Locbranchid, eventid, eventnatureid)
                                    where row.Status != "Closed-Timely" || row.Status != "Closed-Delayed"
                                    select row).ToList();

                        data.ForEach(eventsheduledata =>
                        {
                            var eventinfo = (from row in entities.EventScheduleOns
                                             where row.ID == eventsheduledata.EventScheduleID
                                             select row).ToList();
                            if (eventinfo.Count > 0)
                            {
                                CreateLogData(Convert.ToInt32(eventsheduledata.EventScheduleID), Convert.ToInt32(eventsheduledata.ComplianceScheduleID));
                                UpdateEventscheduledata(eventsheduledata.EventScheduleID);

                                    var complianceTransactions = (from row in entities.ComplianceTransactions
                                                                  where row.ComplianceScheduleOnID == eventsheduledata.ComplianceScheduleID && row.StatusId != 1
                                                                  select row).ToList();

                                    if (complianceTransactions.Count == 0)
                                    {
                                        DeleteComplianceScheduleOn(eventsheduledata.ComplianceScheduleID);
                                    }
                               
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data updated successfully.";
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data not found.";
                            }

                        });
                    }
                    upEvent.Update();
                    grdEvent.DataSource = null;
                    grdEvent.DataBind();
                    ddlCustomer.SelectedValue = "-1";
                    ddlEvent.SelectedValue = "-1";
                    ddlEventNature.SelectedValue = "-1";
                    tbxFilterLocation.Text = "< Select >";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void DeleteComplianceScheduleOn(long scheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == scheduleOnID// && row.IsActive==true &&
                                                             //row.IsUpcomingNotDeleted==true
                                                             select row).FirstOrDefault();

                complianceScheduleOn.IsActive = false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();

            }

        }

    }
}