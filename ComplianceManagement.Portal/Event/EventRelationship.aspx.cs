﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventRelationship : System.Web.UI.Page
    {
        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();

        ArrayList arraylist3 = new ArrayList();
        ArrayList arraylist4 = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEvents(1);
            }
        }
        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
            {
                var id =Convert.ToInt32(rblRelationship.SelectedValue);
                BindEvents(id);
            }           
        }
        private void BindEvents(int CompanyType)
        {
            try
            {
                ddlEvent.DataTextField = "Name";
                ddlEvent.DataValueField = "ID";

                ddlEvent.DataSource = EventManagement.GetEvent(CompanyType);
                ddlEvent.DataBind();
                ddlEvent.Items.Insert(0, new ListItem("< Select Event >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                //BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRightIntermediateMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstLeftIntermediate.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstLeftIntermediate.Items.Count; i++)
                    {
                        if (lstLeftIntermediate.Items[i].Selected)
                        {
                            if (!arraylist1.Contains(lstLeftIntermediate.Items[i]))
                            {
                                arraylist1.Add(lstLeftIntermediate.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < arraylist1.Count; i++)
                    {
                        if (!lstRightIntermediate.Items.Contains(((ListItem)arraylist1[i])))
                        {
                            lstRightIntermediate.Items.Add(((ListItem)arraylist1[i]));
                        }
                        lstLeftIntermediate.Items.Remove(((ListItem)arraylist1[i]));
                    }
                    lstRightIntermediate.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapping intermediate event to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLeftIntermediateMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstRightIntermediate.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstRightIntermediate.Items.Count; i++)
                    {
                        if (lstRightIntermediate.Items[i].Selected)
                        {
                            if (!arraylist2.Contains(lstRightIntermediate.Items[i]))
                            {
                                arraylist2.Add(lstRightIntermediate.Items[i]);
                            }
                        }
                    }

                    for (int i = 0; i < arraylist2.Count; i++)
                    {
                        if (!lstLeftIntermediate.Items.Contains(((ListItem)arraylist2[i])))
                        {
                            lstLeftIntermediate.Items.Add(((ListItem)arraylist2[i]));
                        }
                        lstRightIntermediate.Items.Remove(((ListItem)arraylist2[i]));
                    }
                    lstLeftIntermediate.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapped intermediate event to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lstLeftIntermediate.Items.Clear();
                lstLeftSubEvent.Items.Clear();

                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                { 
                    var id = Convert.ToInt32(rblRelationship.SelectedValue);
                    var lst = EventManagement.GetIntermediateEventRelationshipMappedID(Convert.ToInt32(ddlEvent.SelectedValue), id);
                    lstRightIntermediate.DataSource = lst;
                    lstRightIntermediate.DataTextField = "Name";
                    lstRightIntermediate.DataValueField = "ID";
                    lstRightIntermediate.DataBind();

                    var lstsubevent = EventManagement.GetSubEventRelationshipMappedID(Convert.ToInt32(ddlEvent.SelectedValue), id);

                    lstRightSubEvent.DataSource = lstsubevent;
                    lstRightSubEvent.DataTextField = "Name";
                    lstRightSubEvent.DataValueField = "ID";
                    lstRightSubEvent.DataBind();

                    BindIntermediateEvent();
                }             
                foreach (ListItem item in lstRightIntermediate.Items)
                {
                    item.Attributes["title"] = item.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindIntermediateEvent()
        {
            try
            {
                lstLeftIntermediate.DataSource = null;
                lstLeftIntermediate.DataBind();
                List<long> actIds = new List<long>();


                if (!String.IsNullOrEmpty(rblRelationship.SelectedValue))
                {
                    var id = Convert.ToInt32(rblRelationship.SelectedValue);
                    var lst = CustomerBranchManagement.GetEventExceptOne(Convert.ToInt32(ddlEvent.SelectedValue), id);
                    lst = lst.GroupBy(a => a.ID).Select(a => a.FirstOrDefault()).ToList();
                    var lst1 = lst;
                    List<long> MappedComplianceList = new List<long>();
                    foreach (ListItem item in lstRightIntermediate.Items)
                    {
                        MappedComplianceList.Add(Convert.ToInt64(item.Value));
                    }

                    lst = lst.Where(entry => !MappedComplianceList.Contains(entry.ID)).ToList();

                    lstLeftIntermediate.DataSource = lst;
                    lstLeftIntermediate.DataTextField = "Name";
                    lstLeftIntermediate.DataValueField = "ID";
                    lstLeftIntermediate.DataBind();

                    List<long> MappedSubEventList = new List<long>();
                    foreach (ListItem item in lstRightIntermediate.Items)
                    {
                        MappedSubEventList.Add(Convert.ToInt64(item.Value));
                    }

                    lst1 = lst1.Where(entry => !MappedSubEventList.Contains(entry.ID)).ToList();

                    lstLeftSubEvent.DataSource = lst1;
                    lstLeftSubEvent.DataTextField = "Name";
                    lstLeftSubEvent.DataValueField = "ID";
                    lstLeftSubEvent.DataBind();
                }               
                foreach (ListItem item in lstLeftIntermediate.Items)
                {
                    item.Attributes["title"] = item.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Business.EventManagement.UpdateEventSubEventMappedID(Convert.ToInt32(ddlEvent.SelectedValue));

                var EventType = Business.EventManagement.GetEventType(Convert.ToInt32(ddlEvent.SelectedValue)).Type;

                for (int i = 0; i < lstRightIntermediate.Items.Count; i++)
                { 
                    EventSubEventMapping eventMapping = new EventSubEventMapping()
                    {
                        ParentEventID = Convert.ToInt32(ddlEvent.SelectedValue),                 
                        IntermediateEventID = Convert.ToInt32(lstRightIntermediate.Items[i].Value),
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                        EventType = "PP",
                        Type =Convert.ToInt32(EventType),
                        IntermediateFlag = true,
                    };
                    Business.ComplianceManagement.CreateEventSubEventMapping(eventMapping);
                }

                for (int i = 0; i < lstRightSubEvent.Items.Count; i++)
                {
                    EventSubEventMapping eventMapping = new EventSubEventMapping()
                    {
                        ParentEventID = Convert.ToInt32(ddlEvent.SelectedValue),
                        SubEventID = Convert.ToInt32(lstRightSubEvent.Items[i].Value),
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = Convert.ToInt32(Session["userID"]),
                        EventType = "PP",
                        Type = Convert.ToInt32(EventType),
                        IntermediateFlag = false,
                    };
                    Business.ComplianceManagement.CreateEventSubEventMapping(eventMapping);
                }

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Save Successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRightSubEventMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstLeftSubEvent.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstLeftSubEvent.Items.Count; i++)
                    {
                        if (lstLeftSubEvent.Items[i].Selected)
                        {
                            if (!arraylist3.Contains(lstLeftSubEvent.Items[i]))
                            {
                                arraylist3.Add(lstLeftSubEvent.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < arraylist3.Count; i++)
                    {
                        if (!lstRightSubEvent.Items.Contains(((ListItem)arraylist3[i])))
                        {
                            lstRightSubEvent.Items.Add(((ListItem)arraylist3[i]));
                        }
                        lstLeftSubEvent.Items.Remove(((ListItem)arraylist3[i]));
                    }
                    lstRightSubEvent.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapping sub event to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLeftSubEventMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstRightSubEvent.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstRightSubEvent.Items.Count; i++)
                    {
                        if (lstRightSubEvent.Items[i].Selected)
                        {
                            if (!arraylist4.Contains(lstRightSubEvent.Items[i]))
                            {
                                arraylist4.Add(lstRightSubEvent.Items[i]);
                            }
                        }
                    }

                    for (int i = 0; i < arraylist4.Count; i++)
                    {
                        if (!lstLeftSubEvent.Items.Contains(((ListItem)arraylist4[i])))
                        {
                            lstLeftSubEvent.Items.Add(((ListItem)arraylist4[i]));
                        }
                        lstRightSubEvent.Items.Remove(((ListItem)arraylist4[i]));
                    }
                    lstLeftSubEvent.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapped sub event to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}