﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Threading;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{ 
    public partial class ClosedEvents : System.Web.UI.Page
    {
        static int Type = 0;
        static int glParentID = 0;
        static int glIntermediateID = 0;
        static int glsubEventID = 0;
        static int PageCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLocationFilter();
                BindGrid();
             
                if (SelectedPageNo.Text == "")
                {
                    SelectedPageNo.Text = "1";
                }
                TotalRows.Value = PageCount.ToString();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);

                var LocationList = CustomerBranchManagement.GetAssignedEventLocationList(AuthenticationHelper.UserID, customerID);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void BindGrid()
        {
            //IEnumerable<Products_Products> x = new List<Products_Products>();
            IEnumerable<SP_ClosedEvent_Result> ClosedList = new List<SP_ClosedEvent_Result>();

            int EventClassificationID = Convert.ToInt32(ddlCheckListType.SelectedValue);
            ClosedList = EventManagement.GetClosedEvents(AuthenticationHelper.UserID, EventClassificationID).ToList();
            int location = -1;
            if (Convert.ToInt32(tvFilterLocation.SelectedValue) != -1)
            {
                location = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if (location != -1)
            {
                ClosedList = ClosedList.Where(entry => entry.CustomerBranchID == location).ToList();
            }

            if (txtSearchType.Text != "")
            {
                ClosedList = ClosedList.Where(entry => entry.Name.Contains(txtSearchType.Text)).ToList();
            }

            grdEventList1.DataSource = ClosedList;
            grdEventList1.DataBind();
            PageCount = ClosedList.Count();
            Session["TotalRows"] = PageCount;

            GetPageDisplaySummary();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventList_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            grdEventList1.EditIndex = e.NewEditIndex;
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            BindGrid(); 
        }

        protected void grdEventList_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            try
            {
                DateTime date1 = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);

                Label txtDate1 = (Label)grdEventList1.Rows[e.RowIndex].FindControl("lblHeldOn");
                string HeldOn = Convert.ToDateTime(txtDate1.Text).ToString("dd-MM-yyyy");

                Session["HeldOn"] = null;
                Session["HeldOn"] = HeldOn;

                int branch = Convert.ToInt32((grdEventList1.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);
                Session["Rbranch"] = null;
                Session["Rbranch"] = branch;

                int EventClassificationID = Convert.ToInt32((grdEventList1.Rows[e.RowIndex].FindControl("lblEventClassificationID") as Label).Text);
                Session["EventClassificationID"] = EventClassificationID;
                if (EventClassificationID == 1)
                {
                    Type = EventManagement.GetCompanyType(Convert.ToInt32(branch));
                }
                else
                {
                    Type = 4;
                }
                long EventScheduledOnID = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblEventScheduleOnID") as Label).Text);
                Session["EventScheduledOnID"] = null;
                Session["EventScheduledOnID"] = EventScheduledOnID;

                long assignmnetID = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblAssignmnetId") as Label).Text);
                Session["assignmnetID"] = null;
                Session["assignmnetID"] = assignmnetID;
                string branchName = (grdEventList1.Rows[e.RowIndex].FindControl("lblBranchName") as Label).Text;
                Session["RbranchName"] = null;
                Session["RbranchName"] = branchName;
                string eventname = (grdEventList1.Rows[e.RowIndex].FindControl("lbleventName") as Label).Text;
                Session["Reventname"] = null;
                Session["Reventname"] = eventname;
                int UserId = Convert.ToInt32((grdEventList1.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);
                Session["RUserId"] = null;
                Session["RUserId"] = UserId;
                int eventId = Convert.ToInt32(grdEventList1.DataKeys[e.RowIndex].Value);
                Session["eventId"] = null;
                Session["eventId"] = eventId;

                Session["EventInstanceID"] = null;
                Session["EventInstanceID"] = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblEventInstanceID") as Label).Text);

                txtDescription.Text = Convert.ToString((grdEventList1.Rows[e.RowIndex].FindControl("lbleventDesc") as Label).Text);

                BindParentEventData(Convert.ToInt32(eventId));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

     
        protected void grdEventList_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            grdEventList1.EditIndex = -1;
            BindGrid();
        }

        protected bool visibleParentEventAddButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                try
                {
                    bool result = false;
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                    int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                         && row1.StatusId != 1
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    else if (EventClassificationID == 2)
                    {
                        var data = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                    join row6 in entities.Acts on row1.ActID equals row6.ID
                                    join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                    join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                    join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                    where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                    && row.SubEventID == 0 && row1.ComplinceVisible == true
                                    && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                    && row4.ID == CustomerBranchID
                                    select row).Distinct().FirstOrDefault();

                        if (data == null)
                        {
                            result = false;
                        }
                        else
                        {
                            var data1 = (from row in entities.ComplianceScheduleOns
                                         join row1 in entities.ComplianceTransactions
                                         on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                         where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                         && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                         && row1.StatusId != 1
                                         select row).FirstOrDefault();

                            if (data1 == null)
                            {
                                result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                                return result;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                return false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleParentEventUpdateButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data1 = (from row in entities.EventCompAssignDays
                             join row1 in entities.EventAssignDates
                             on row.ParentEventID equals row1.ParentEventID
                             where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                             && row.SubEventID == 0 && row1.EventScheduleOnID == EventScheduledOnId
                             select row).FirstOrDefault();
                if (data1 == null)
                {
                    result = false;
                }
                else
                {
                    var data = (from row in entities.ComplianceScheduleOns
                                join row1 in entities.ComplianceTransactions
                                on row.ID equals row1.ComplianceScheduleOnID
                                where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                && row1.StatusId != 1
                                select row).FirstOrDefault();

                    if (data == null)
                    {
                        result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                        if (result == false)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CheckSubEventTransactionComplete(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row6 in entities.Acts on row1.ActID equals row6.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CheckIntermediateEventTransactionComplete(int ParentEventID, int IntermediateID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                                       && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                                  join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                  join row6 in entities.Acts on row1.ActID equals row6.ID
                                                  join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                                  join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                                  join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                                  where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                                                  && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                                  && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                                  && row4.ID == CustomerBranchID
                                                  select row).Distinct().ToList();


                    if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool enableSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleTxtgvChildGridDate(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {

                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleSubEventAddButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {

                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool enableIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleIntermediateSubEventAddButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                        }
                        else
                        {
                            result = false;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleTxtgvIntermediateSubEventGridDate(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else if (EventClassificationID == 2)
                {
                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row6 in entities.Acts on row1.ActID equals row6.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                 && (row6.StateID == row4.StateID || row6.ComplianceTypeId == 2)
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                    {
                        result = false;
                    }
                    else
                    {
                        var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ID equals row1.ComplianceScheduleOnID
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                     && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                        if (data1 == null)
                        {
                            result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                            if (result == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentActivatedEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();

                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glParentID = ParentEventID;
                ViewState["ParentEventID"] = ParentEventID;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                //var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                var compliance = entities.SP_GetParentEventToComplianceAssignWithShortNoticeDays(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetActivatedSubEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var Intermediate = entities.SP_GetActivatedIntermediateEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gv2.DataSource = Intermediate;
                gv2.DataBind();
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    glsubEventID = SubEventID;
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(Session["eventId"]);
                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                    int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                    if (EventClassificationID == 1)
                    {
                        var Compliance = entities.SP_GetIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                    else if (EventClassificationID == 2)
                    {
                        var Compliance = entities.SP_GetNonSecretrialIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvIntermediateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glIntermediateID = IntermediateEventID;
                GridView childGrid1 = (GridView)sender;
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var Compliance = entities.SP_GetActivatedIntermediateSubEvent(IntermediateEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Parentid, Type).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glsubEventID = SubEventID;
                GridView childGrid1 = (GridView)sender;
                //Retreiving the GridView DataKey Value
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();
                //var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    var Compliance = entities.SP_GetComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
                else if (EventClassificationID == 2)
                {
                    var Compliance = entities.SP_GetNonSecretrialComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }

        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblParentStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblParentStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }

            }
        }

        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblStatus.Visible = false;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }

            }
        }

        protected bool CheckCompleteTransactionComplete(int ParentEventID, int IntermediateEventID, int SubEventID, int ComplianceID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                   join row1 in entities.ComplianceTransactions
                                                   on row.ID equals row1.ComplianceScheduleOnID
                                                   join row2 in entities.ComplianceInstances
                                                   on row1.ComplianceInstanceId equals row2.ID
                                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                                   && row.SubEventID == SubEventID && row2.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduledOnId
                                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                                                   select row).ToList();

                if (ComplainceTransCompleteList.Count >= 1)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = System.Drawing.Color.Chocolate;
                    e.Row.CssClass = "Inforamative";
                    lblIntermediateStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = System.Drawing.Color.SlateBlue;
                    e.Row.CssClass = "Actionable";
                    lblIntermediateStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }

            }
        }

       protected void gvIntermediateSubEventGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;

                    int interIndex = masterrow.RowIndex;

                    GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvIntermediateGrid");

                    int ParentEventID = Convert.ToInt32(intermediateGrid.DataKeys[interIndex].Value.ToString());

                    GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);

                    int lblIntermediateEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());

                    int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);

                    var txtDate = childGrid.Rows[e.RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;

                    if (txtDate.Text != "")
                    {
                        ViewState["interIndex"] = interIndex;
                        ViewState["RowIndex"] = e.RowIndex;
                        ViewState["ParentEventID"] = ParentEventID;
                        ViewState["IntermediateEventID"] = lblIntermediateEventID;
                        ViewState["SubEventID"] = SubEventID;
                        ViewState["EventScheduledOnID"] = EventScheduledOnID;

                        GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");

                        //Check shorter notice Compliance
                        List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                        Boolean shorterFlag = false;
                        List<long> lstShortCompliance = new List<long>();
                        foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                            bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                            if (ExistShorterNotice == true)
                            {
                                shorterFlag = true;
                                lstShortCompliance.Add(ComplinaceID);
                                //break;
                            }
                        }

                        if (shorterFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice').modal();", true);
                            upOptionalCompliances.Update();
                        }
                        else
                        {

                            var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;
                            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            EventAssignDate eventAssignDate = new EventAssignDate()
                            {
                                ParentEventID = ParentEventID,
                                EventScheduleOnID = EventScheduledOnID,
                                IntermediateEventID = Convert.ToInt32(lblIntermediateEventID),
                                SubEventID = SubEventID,
                                Date = Date,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                Type = Type,
                            };
                            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                            //GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");
                            // Intermediate Sub event Compliance 
                            foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                {
                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);

                                    string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                                    Boolean FlgCheck = false;
                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, lblIntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    if (FlgCheck == false)
                                    {
                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else
                                    {
                                        int IncludeDays = 0;
                                        var compdetails = EventManagement.GetCompliance(ComplinaceID);
                                        if (compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null))
                                        {
                                            int Days = Convert.ToInt32(days);
                                            if (Days == 0)
                                            {
                                                IncludeDays = 0;
                                            }
                                            else
                                            {
                                                if (Days > 0)
                                                {
                                                    IncludeDays = Days - 1;
                                                }
                                                else
                                                {
                                                    IncludeDays = Days + 1;
                                                }
                                            }
                                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                            EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                            EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                        }
                                    }
                                }
                            }
                            BindParentEventData(Convert.ToInt32(ParentEventID));
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Saved Successfully";
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter activated date";
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                grdEventList1.PageSize = int.Parse(((DropDownList)sender).SelectedValue);

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdEventList1.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList1.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                BindGrid();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                grdEventList1.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList1.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGrid();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                grdEventList1.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList1.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGrid();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
     
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = PageCount.ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void btnClosedEvent_Click(object sender, EventArgs e)
        {
            var ActivatedEventList = EventManagement.GetActivatedEvnts();
            
            ActivatedEventList.ForEach(list =>
            {
                bool CheckEvntClosed = EventManagement.CheckEventClosed(Convert.ToInt32(list.EventScheduledOnId), Convert.ToInt32(list.ID));

                if (CheckEvntClosed == true)
                {
                    bool CheckEntryPresent = EventManagement.CheckClosedEventEntryPresent(Convert.ToInt32(list.UserID), Convert.ToInt32(list.CustomerID), Convert.ToInt32(list.CustomerBranchID), Convert.ToInt32(list.ID), Convert.ToInt32(list.EventScheduledOnId));

                    if (CheckEntryPresent == false)
                    {
                        com.VirtuosoITech.ComplianceManagement.Business.Data.ClosedEventDetail eventData = new com.VirtuosoITech.ComplianceManagement.Business.Data.ClosedEventDetail()
                        {
                            UserID = list.UserID,
                            CustomerID = list.CustomerID,
                            CustomerBranchID = list.CustomerBranchID,
                            EventID = list.ID,
                            ClosedDate = DateTime.Now,
                            EventSchuduleID = list.EventScheduledOnId,
                        };
                        EventManagement.CreateClosedEvent(eventData);


                        com.VirtuosoITech.ComplianceManagement.Business.Data.EventScheduleOn eventScheduleOn = new com.VirtuosoITech.ComplianceManagement.Business.Data.EventScheduleOn()
                        {
                            ClosedDate = DateTime.Now,
                            ID =Convert.ToInt32(list.EventScheduledOnId),
                            ClosedEventFlag = true,
                        };
                         EventManagement.UpdateClosedData(eventScheduleOn);

                    }
                }
            });
        }

        protected void upEventInstanceList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCheckListType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}