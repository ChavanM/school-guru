﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventComplianceList : System.Web.UI.Page
    {
        public bool MGM_KEy;
        protected static int customerid;
        //static int GComplianceID;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV" || AuthenticationHelper.Role == "RPER" || AuthenticationHelper.Role == "UPDT")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }

                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "ID";

                        BindCategories();
                        BindTypes();
                        BindActs();
                        BindFilterFrequencies();
                        BindIndustry();
                        BindEntityType();
                        BindNatureOfCompliance();
                        BindFrequencies();
                        BindDueDates();
                        BindDueDays();
                        BindIsForSecretarial();
                        txtIndustry.Attributes.Add("readonly", "readonly");
                        txtEntityType.Attributes.Add("readonly", "readonly");
                        txtIsForSecretarial.Attributes.Add("readonly", "readonly");
                        if (AuthenticationHelper.Role.Equals("SADMN") || (AuthenticationHelper.Role.Equals("UPDT") || AuthenticationHelper.Role.Equals("RREV")))
                        {
                            btnAddCompliance.Visible = true;
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
               
            }
        }
        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFrequencies()
        {
            try
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";


                ddlFrequency.DataSource = Enumerations.GetAll<EventBasedFrequency>();
                ddlFrequency.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindNatureOfCompliance()
        {
            try
            {
                ddlNatureOfCompliance.DataSource = Business.ComplianceManagement.GetAllComplianceNature();
                ddlNatureOfCompliance.DataTextField = "Name";
                ddlNatureOfCompliance.DataValueField = "ID";
                ddlNatureOfCompliance.DataBind();

                ddlNatureOfCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompliancesNew()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.Compliances
                                        join row1 in entities.Acts on row.ActID equals row1.ID
                                        where row.IsDeleted == false && row.EventFlag == true 
                                        select new
                                        {
                                            row.ID
                                                ,
                                            row.ActID
                                                ,
                                            ActName = row1.Name
                                                ,
                                            row.Description
                                                ,
                                            row.Sections
                                                ,
                                            row.ComplianceType
                                                ,
                                            row.UploadDocument
                                                ,
                                            row.NatureOfCompliance
                                                ,
                                            row.RequiredForms
                                                ,
                                            row.Frequency
                                                ,
                                            row.DueDate
                                                ,
                                            row.RiskType
                                                ,
                                            row.NonComplianceType
                                                ,
                                            row.NonComplianceEffects
                                                ,
                                            row.ShortDescription
                                                ,
                                            row.CreatedOn
                                                ,
                                            row1.ComplianceCategoryId
                                                ,
                                            row1.ComplianceTypeId
                                                ,
                                            row.EventID
                                                ,
                                            row.SubComplianceType
                                                ,
                                            row.FixedGap
                                                ,
                                            row.CheckListTypeID
                                                ,
                                            RISK = row.RiskType == 0 ? "High" :
                                                row.RiskType == 1 ? "Medium" :
                                                row.RiskType == 2 ? "Low" :
                                                row.RiskType == 3 ? "Critical" : "",

                                            FrequencyName = row.Frequency == 0 ? "Monthly" :
                                            row.Frequency == 1 ? "Quarterly" :
                                            row.Frequency == 2 ? "HalfYearly" :
                                            row.Frequency == 3 ? "Annual" :
                                            row.Frequency == 4 ? "FourMonthly" :
                                            row.Frequency == 5 ? "TwoYearly" :
                                            row.Frequency == 6 ? "SevenYearly" : ""
                                        });

                int risk = -1;
                if (tbxFilter.Text.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (tbxFilter.Text.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (tbxFilter.Text.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (tbxFilter.Text.ToUpper().Equals("CRICITAL"))
                    risk = 3;
                

                string frequency = tbxFilter.Text.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        compliancesQuery = compliancesQuery.Where(entry => entry.ID == a);
                    }
                    else
                    {
                        compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Sections.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.RequiredForms.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                    }
                }

                if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                {
                    int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == a);
                }
                if (Convert.ToInt32(ddlComplinceCatagory.SelectedValue) != -1)
                {
                    int b = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == b);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (Convert.ToInt32(ddlFilterFrequencies.SelectedValue) != -1)
                {
                    int c = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == c);
                }
                if (ddlAct1.SelectedValue != "")
                {
                    if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
                    {
                        int c = Convert.ToInt32(ddlAct1.SelectedValue);
                        compliancesQuery = compliancesQuery.Where(entry => entry.ActID == c);
                    }
                }
                var compliances = compliancesQuery.ToList();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderBy(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderBy(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderBy(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderBy(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdCompliances.DataSource = compliances;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
        }

        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void chbImprisonment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                divImprisonmentDetails.Visible = ((CheckBox)sender).Checked;
                if (!divImprisonmentDetails.Visible)
                {
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                    ddlMinimumYear.SelectedValue = ddlMaximumYear.SelectedValue = "-1";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                divSampleForm.Visible = false;

                divNatureOfCompliance.Visible = true;
                divNatureOfSubCompliance.Visible = true;
                divUploadDocument.Visible = true;
                divActionable.Visible = true;
                chkDocument.Checked = false;
                saveopo.Value = "false";
                //GComplianceID = 0;
                   //ViewState["ComplianceParameters"] = new List<ComplianceParameter>();
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;

                tbxRequiredForms.Text = tbxDescription.Text = tbxNonComplianceEffects.Text = tbxSections.Text = string.Empty;
                ddlNatureOfCompliance.SelectedIndex = ddlComplianceType.SelectedIndex = ddlAct.SelectedIndex =  ddlNonComplianceType.SelectedIndex = ddlRiskType.SelectedIndex = 0;
                rdoUploadDoc.Checked = true;
                rdoComplianceVisible.Checked = true;
                rdoNotUploadDoc.Checked = false;
                rdoNotComplianceVisible.Checked = false;
                txtSampleFormLink.Text = "";
                isOnline.Checked = false;
                chkFrequency.Checked = false;
                chkForcefulClosure.Checked = false;
                //lblSampleForm.Text = "< Not selected >";
                ddlNonComplianceType_SelectedIndexChanged(null, null);

                ddlComplianceType_SelectedIndexChanged(null, null);

                chkFrequency_CheckedChanged(null, null);
                divTimeBased.Visible = false;
                ddlWeekDueDay.SelectedValue = "-1";
                tbxStartDate.Text = string.Empty;

                txtReferenceMaterial.Text = string.Empty;
                txtPenaltyDescription.Text = string.Empty;
                txtShortDescription.Text = string.Empty;
                txtCompliancetag.Text = string.Empty;
                txtshortform.Text = string.Empty;
                tbxMinimumYears.Text = string.Empty;
                tbxMaximumYears.Text = string.Empty;
                ddlMaximumYear.SelectedValue = "-1";
                ddlMaximumYear.SelectedValue = "-1";
                txtIndustry.Text = "< Select >";

                ChkIsMapped.Checked = true;

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
                txtEntityType.Text = "< Select >";
                
                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    chkEntityType.Checked = false;
                    CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                    EntityTypeSelectAll.Checked = false;
                }
                txtIsForSecretarial.Text = "< Select >";

                foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                {
                    CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                    chkIsForSecretarial.Checked = false;
                    CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                    IsForSecretarialSelectAll.Checked = false;
                }
                upComplianceDetails.Update();

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool chkexists = false;
                if (ViewState["GComplianceID"] !=null)
                {
                    chkexists = Business.ComplianceManagement.ExistsSavedCompliance(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue), Convert.ToInt32(ViewState["GComplianceID"]));
                }
                else
                {
                    chkexists = Business.ComplianceManagement.ExistsR(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue));
                }

                Boolean chkIndustryFlag = false;
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    if (chkIndustry.Checked)
                    {
                        chkIndustryFlag = true;
                    }
                }

                if (chkIndustryFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one industry.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                Boolean chkLegalEntityFlag = false;
                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    if (chkEntityType.Checked)
                    {
                        chkLegalEntityFlag = true;
                    }
                }

                if (chkLegalEntityFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one Legal entity type.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                //if (ddlMinimumYear.SelectedItem.Text == "Year" && ddlMaximumYear.SelectedItem.Text == "Month")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    minvalue = minvalue * 12;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    if (minvalue >= maxvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum month greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}
                //else if (ddlMinimumYear.SelectedItem.Text == "Month" && ddlMaximumYear.SelectedItem.Text == "Year")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    maxvalue = maxvalue * 12;

                //    if (minvalue >= maxvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum year greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}
                //else if (ddlMinimumYear.SelectedItem.Text == "Month" && ddlMaximumYear.SelectedItem.Text == "Month")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    if (maxvalue <= minvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum month greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}

                int? DueDateType = null;
                if (isOnline.Checked == false)
                {
                    DueDateType = Convert.ToInt32(ddlDueDateType.SelectedValue);
                }

                if (chkexists == false)
                {
                    Boolean FileValidation = false;
                    if (chkDocument.Checked)
                    {
                        if (fuSampleFile.PostedFiles.Count() > 0)
                        {
                            if (fuSampleFile.FileName.Contains('&'))
                            {
                                FileValidation = true;
                            }
                        }
                    }
                    if (FileValidation == false)
                    {
                        Boolean UploadDoc = false;
                        if (rdoUploadDoc.Checked == true)
                        {
                            UploadDoc = true;
                        }

                        Boolean ComplianceVisible = false;
                        if (rdoComplianceVisible.Checked == true)
                        {
                            ComplianceVisible = true;
                        }

                        if (ddlComplianceType.SelectedValue == "0")
                        {
                            ComplianceVisible = true;
                        }

                        Business.Data.Compliance compliance = new Business.Data.Compliance()
                        {
                            ActID = Convert.ToInt32(ddlAct.SelectedValue),
                            Description = tbxDescription.Text,
                            Sections = tbxSections.Text,
                            ShortDescription = txtShortDescription.Text,
                            ShortForm=txtshortform.Text,
                            UploadDocument = chkDocument.Checked,
                            ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                            RequiredForms = tbxRequiredForms.Text,
                            SampleFormLink = txtSampleFormLink.Text,
                            RiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                            PenaltyDescription = txtPenaltyDescription.Text,
                            ReferenceMaterialText = txtReferenceMaterial.Text,
                            EventFlag = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            UpDocs = UploadDoc,
                            ComplinceVisible = ComplianceVisible,
                            UpdatedOn = DateTime.Now,
                            onlineoffline = isOnline.Checked,
                            duedatetype = DueDateType,
                            IsForSecretarial = false,
                            IsMapped = ChkIsMapped.Checked,
                        };
                        if (ChkIsForSecretarial.Checked)
                        {
                            compliance.IsForSecretarial = Convert.ToBoolean(true);
                        }
                        if (!string.IsNullOrEmpty(tbxStartDate.Text))
                            compliance.StartDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        if (compliance.ComplianceType == 0 || compliance.ComplianceType == 1)//function based or time based and Checklist
                        {
                            compliance.IsFrequencyBased = chkFrequency.Checked;
                            compliance.IsForcefulClosure = chkForcefulClosure.Checked;

                            if (chkFrequency.Checked == true)
                            {
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                compliance.TriggerNo = Convert.ToInt32(txtTriggerNo.Text);
                                if (compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    compliance.DueDate = null;
                                }
                                else
                                {
                                    compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                    compliance.DueWeekDay = null;
                                }
                                if (compliance.Frequency == 9)
                                {
                                    compliance.DueDate = null;
                                }
                            }
                            else
                            {
                                compliance.IsFrequencyBased = null;
                                compliance.DueWeekDay = null;
                                compliance.Frequency = null;
                                compliance.DueDate = null;
                                compliance.TriggerNo = null;
                            }
                         }
                            if (ddlNatureOfCompliance.SelectedValue != "")
                            {
                                if (ddlNatureOfCompliance.SelectedValue != "-1")
                                {
                                    compliance.NatureOfCompliance = Convert.ToByte(ddlNatureOfCompliance.SelectedValue);
                                    if (ddlComplianceSubType.SelectedValue != "")
                                    {
                                        if (ddlComplianceSubType.SelectedValue != "-1")
                                        {
                                            compliance.ComplianceSubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                                        }
                                    }
                                }
                            }

                            compliance.NonComplianceType = ddlNonComplianceType.SelectedValue == "-1" ? (byte?)null : Convert.ToByte(ddlNonComplianceType.SelectedValue);
                            compliance.NonComplianceEffects = tbxNonComplianceEffects.Text;

                            compliance.FixedMinimum = tbxFixedMinimum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMinimum.Text) : (double?)null;
                            compliance.FixedMaximum = tbxFixedMaximum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMaximum.Text) : (double?)null;
                            if (ddlPerDayMonth.SelectedValue == "0")
                            {
                                compliance.VariableAmountPerDay = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                                compliance.VariableAmountPerMonth = null;
                            }
                            else
                            {
                                compliance.VariableAmountPerMonth = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                                compliance.VariableAmountPerDay = null;
                            }
                            compliance.VariableAmountPerDayMax = tbxVariableAmountPerDayMax.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDayMax.Text) : (double?)null;
                        //compliance.VariableAmountPercent = tbxVariableAmountPercent.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercent.Text) : (double?)null;
                        //compliance.VariableAmountPercentMax = tbxVariableAmountPercentMaximum.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercentMaximum.Text) : (double?)null;
                        compliance.VariableAmountPercent =  Convert.ToString(tbxVariableAmountPercent.Text);
                        compliance.VariableAmountPercentMax =  Convert.ToString(tbxVariableAmountPercentMaximum.Text);

                        compliance.Imprisonment = chbImprisonment.Checked;
                            compliance.Designation = tbxDesignation.Text;
                            compliance.MinimumYears = tbxMinimumYears.Text.Length > 0 ? Convert.ToInt32(tbxMinimumYears.Text) : (int?)null;
                            compliance.MaximumYears = tbxMaximumYears.Text.Length > 0 ? Convert.ToInt32(tbxMaximumYears.Text) : (int?)null;

                            compliance.MinimumPeriodType = ddlMinimumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMinimumYear.SelectedValue);
                            compliance.MaximumPeriodType = ddlMaximumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMaximumYear.SelectedValue);


                        if (compliance.ComplianceType == 2)
                        {
                            compliance.triggerNoOfDays =Convert.ToInt32(txttriggerNoOfDays.Text);
                            compliance.IntervalDays  = Convert.ToInt32(txtIntervalDays.Text);
                        }
                       
                        ComplianceForm form = null;
                        List<ComplianceForm> ComplianceForms = new List<ComplianceForm>();
                        if (chkDocument.Checked)
                        {
                            if (fuSampleFile.PostedFiles.Count() > 0)
                            //if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                            {
                                HttpFileCollection fileCollection = Request.Files;
                                if (fileCollection.Count > 0)
                                {

                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = null;
                                        uploadfile = fileCollection[i];
                                        string fileName = uploadfile.FileName;

                                        if(!string.IsNullOrEmpty(fileName))
                                        {
                                            //fuSampleFile.SaveAs(Server.MapPath("~/ComplianceFiles/" + fileName));
                                            fileCollection[i].SaveAs(Server.MapPath("~/ComplianceFiles/" + fileName));
                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            form = new ComplianceForm()
                                            {
                                                Name = fileName,
                                                FileData = bytes,
                                                FilePath = "~/ComplianceFiles/" + fileName,
                                            };

                                            ComplianceForms.Add(form);
                                        }
                                    }
                                }
                            }
                        }
                        List<ComplianceParameter> parameters = new List<ComplianceParameter>(); //GetComplianceParameters();

                        if ((int)ViewState["Mode"] == 1)
                        {
                            compliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                        }

                        if ((int)ViewState["Mode"] == 0)
                        {
                            Business.ComplianceManagement.CreateMultiple(compliance, ComplianceForms);

                            /// Add compliance new filed in  ComplianceDetail table SACHIN 04 Aug 2017
                            ComplianceDetail compliancedetail = new ComplianceDetail()
                            {
                                ComplianceID = compliance.ID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };
                            Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);

                            //---------add Industry--------------------------------------------
                            List<int> IndustryIds = new List<int>();
                            foreach (RepeaterItem aItem in rptIndustry.Items)
                            {
                                CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                                if (chkIndustry.Checked)
                                {
                                    IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));


                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                    {
                                        ComplianceId = compliance.ID,
                                        IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),


                                    };
                                    Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                                }
                            }

                            //---------add Legal Entity type--------------------------------------------
                            List<int> EntityTypeIds = new List<int>();
                            foreach (RepeaterItem aItem in rptEntityType.Items)
                            {
                                CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                                if (chkEntityType.Checked)
                                {
                                    EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                                    LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                    {
                                        ComplianceId = compliance.ID,
                                        LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),

                                    };
                                    Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                                }
                            }

                            //----------------add IsForSecretarial----------------------------------------
                            List<int> IsForSecretarialIds = new List<int>();
                            foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                            {
                                CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                                if (chkIsForSecretarial.Checked)
                                {
                                    IsForSecretarialIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()));
                                    Compliance_SecretarialTagMapping SecretarialTagMapping = new Compliance_SecretarialTagMapping()
                                    {
                                        ComplianceID = compliance.ID,
                                        TagID = Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()),
                                        IsActive = true,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        CreatedOn = DateTime.UtcNow,
                                    };
                                    Business.ComplianceManagement.CreateSecreterialMapping(SecretarialTagMapping);
                                }
                            }
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                            if (chkexistsComplianceschedule == true)
                            {

                                Business.ComplianceManagement.UpdateIFInstanceIsCreatedEventCompliance(compliance, ComplianceForms);
                            }
                            else
                            {
                                Business.ComplianceManagement.UpdateEventCompliance(compliance, ComplianceForms);
                            }
                            /// Add compliance new field in  ComplianceDetail table SACHIN 04 Aug 2017
                            bool chkexistsComplianceDetail = Business.ComplianceManagement.ExistsComplianceDetail(Convert.ToInt64(ViewState["ComplianceID"]));
                            if (chkexistsComplianceDetail == false)
                            {
                                ComplianceDetail compliancedetail = new ComplianceDetail()
                                {
                                    ComplianceID = compliance.ID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };
                                Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);
                            }
                            else
                            {
                                Business.ComplianceManagement.UpdateComplianceDetail(compliance.ID, AuthenticationHelper.UserID);
                            }
                            //---------add Industry--------------------------------------------
                            List<int> IndustryIds = new List<int>();
                            Business.ComplianceManagement.UpdateIndustryMappedID(compliance.ID);
                            foreach (RepeaterItem aItem in rptIndustry.Items)
                            {
                                CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                                if (chkIndustry.Checked)
                                {
                                    IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                    {
                                        ComplianceId = compliance.ID,
                                        IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };

                                    Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                                }
                            }

                            //---------add Legal Entity type--------------------------------------------
                            List<int> EntityTypeIds = new List<int>();
                            Business.ComplianceManagement.UpdateLegalEntityMappedID(compliance.ID);
                            foreach (RepeaterItem aItem in rptEntityType.Items)
                            {
                                CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                                if (chkEntityType.Checked)
                                {
                                    EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                                    LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                    {
                                        ComplianceId = compliance.ID,
                                        LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),

                                    };
                                    Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                                }
                            }
                            //---------add Secreterial--------------------------------------------
                            List<int> IsForSecretarialIds = new List<int>();
                            Business.ComplianceManagement.UpdateSecretarialMappedID(compliance.ID);
                            foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                            {
                                CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                                if (chkIsForSecretarial.Checked)
                                {
                                    IsForSecretarialIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()));
                                    Compliance_SecretarialTagMapping SecretarialTagMapping = new Compliance_SecretarialTagMapping()
                                    {
                                        ComplianceID = compliance.ID,
                                        TagID = Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()),
                                        IsActive = true,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        CreatedOn = DateTime.UtcNow,
                                    };
                                    Business.ComplianceManagement.CreateSecreterialMapping(SecretarialTagMapping);
                                }
                            }
                        }

                        Business.ComplianceManagement.UpdateComplianceTagMappedID(compliance.ID);

                        if (txtCompliancetag.Text != "")
                        {
                            string[] arrFileTags = txtCompliancetag.Text.Split(',');

                            if (arrFileTags.Length > 0)
                            {
                                List<ComplianceDatatagMapping> lstFiletagdataMapping = new List<ComplianceDatatagMapping>();

                                for (int j = 0; j < arrFileTags.Length; j++)
                                {
                                    ComplianceDatatagMapping objFileTagMapping = new ComplianceDatatagMapping()
                                    {
                                        ComplianceID = compliance.ID,
                                        FileTag = arrFileTags[j].Trim(),
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        UpdatedOn = DateTime.Now,
                                        DocType = "E"
                                    };

                                    lstFiletagdataMapping.Add(objFileTagMapping);
                                }

                                if (lstFiletagdataMapping.Count > 0)
                                {
                                    Business.ComplianceManagement.CreateUpdate_ComplianceFileTagsMappingData(lstFiletagdataMapping);
                                }
                            }
                        }

                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close')", true);
                        BindCompliancesNew();
                        var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(compliance.ID);

                        if (complianceForm.Count > 0)
                        {
                            hdnFile.Value = Convert.ToString(compliance.ID);
                            divSampleForm.Visible = true;
                            grdSampleForm.DataSource = complianceForm;
                            grdSampleForm.DataBind();
                        }
                        else
                        {
                            divSampleForm.Visible = false;
                        }
                        saveopo.Value = "true";
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";

                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please remove '&' symbol from uploaded sample file";
                        saveopo.Value = "true";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Compliance Short Description and Detailed Description compliance allready present in System";
                    saveopo.Value = "true";
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    divSampleForm.Visible = false;
                    grdSampleForm.DataSource = null;
                    grdSampleForm.DataBind();
                    hdnFile.Value = "";
                    ClearSelection();
                    ViewState["GComplianceID"] = null;
                    ViewState["GComplianceID"] = Convert.ToInt32(e.CommandArgument);
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    lblErrorMassage.Text = "";
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    hdnFile.Value = Convert.ToString(complianceID);

                    var vGetIndustryMappedIDs = Business.ComplianceManagement.GetIndustryMappedID(complianceID);

                    if (compliance.IsMapped == true)
                    {
                        ChkIsMapped.Checked = true;
                    }
                    else
                    {
                        ChkIsMapped.Checked = false;
                    }

                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                      
                        for(int i=0; i <= vGetIndustryMappedIDs.Count -1 ; i ++)
                        {
                            if(((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }
                               
                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                        {
                            IndustrySelectAll.Checked = true;

                        }
                        else
                        {
                            IndustrySelectAll.Checked = false;

                        }
                    }

                    var vGetLegalEntityTypeMappedID = Business.ComplianceManagement.GetLegalEntityTypeMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        chkEntityType.Checked = false;
                        CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                        for (int i = 0; i <= vGetLegalEntityTypeMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetLegalEntityTypeMappedID[i].ToString())
                            {
                                chkEntityType.Checked = true;
                            }
                        }
                        if ((rptEntityType.Items.Count) == (vGetLegalEntityTypeMappedID.Count))
                        {
                            EntityTypeSelectAll.Checked = true;

                        }
                        else
                        {
                            EntityTypeSelectAll.Checked = false;

                        }
                    }

                    #region Secretrial tag
                    var vGetSecretrialMappedID = ActManagement.GetSecretrialTagMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                    {
                        CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                        chkIsForSecretarial.Checked = false;
                        CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                        for (int i = 0; i <= vGetSecretrialMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim() == vGetSecretrialMappedID[i].ToString())
                            {
                                chkIsForSecretarial.Checked = true;
                            }
                        }
                        if ((rptIsForSecretarial.Items.Count) == (vGetSecretrialMappedID.Count))
                        {
                            IsForSecretarialSelectAll.Checked = true;
                        }
                        else
                        {
                            IsForSecretarialSelectAll.Checked = false;
                        }
                    }

                    #endregion
                    txtIndustry.Text = "< Select >";
                    txtEntityType.Text = "< Select >";
                    txtIsForSecretarial.Text = "< Select >";
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                          
                    ddlAct.SelectedValue = compliance.ActID.ToString();

                    if (compliance.StartDate != null)
                        tbxStartDate.Text = Convert.ToDateTime(compliance.StartDate).ToString("dd-MM-yyyy");

                    if (vGetIndustryMappedIDs.Count <= 0)
                    {
                        ddlAct_SelectedIndexChanged(null, null);
                    }
                    if (compliance.IsForSecretarial == true)
                    {
                         ChkIsForSecretarial.Checked = true;
                        divSecretarial.Visible = true;
                    }
                    else
                    {
                        ChkIsForSecretarial.Checked = false;
                        divSecretarial.Visible = false;
                    }

                    txtShortDescription.Text = compliance.ShortDescription;
                    tbxDescription.Text = compliance.Description;
                    txtshortform.Text = compliance.ShortForm;
                    tbxSections.Text = compliance.Sections;
                    chkDocument.Checked = compliance.UploadDocument ?? false;
                    ddlComplianceType.SelectedValue = compliance.ComplianceType.ToString();
                    tbxRequiredForms.Text = compliance.RequiredForms;
                    txtSampleFormLink.Text = compliance.SampleFormLink; 
                    ddlRiskType.SelectedValue = compliance.RiskType.ToString();
                    ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();

                    ddlNatureOfCompliance_SelectedIndexChanged(null, null);

                    if (compliance.onlineoffline != null)
                        isOnline.Checked = (bool)compliance.onlineoffline;

                    if (compliance.duedatetype != null)
                        ddlDueDateType.SelectedValue = Convert.ToString(compliance.duedatetype);

                    ddlDueDateType.Enabled = true;
                    if (isOnline.Checked == true)
                    {
                        ddlDueDateType.Enabled = false;
                    }

                    try
                    {
                        if (compliance.ComplianceSubTypeID == null)
                        {
                            ddlComplianceSubType.SelectedValue = "-1";
                        }
                        else
                        {
                            ddlComplianceSubType.SelectedValue = compliance.ComplianceSubTypeID.ToString();

                        }
                    }
                    catch (Exception)
                    {
                    }

                    chkForcefulClosure.Checked = false;
                    chkForcefulClosure.Checked = (compliance.IsForcefulClosure ?? false);

                    if (compliance.IsFrequencyBased == true)
                    {
                        chkFrequency.Checked = true;
                        ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();
                        txtTriggerNo.Text = Convert.ToString(compliance.TriggerNo);

                        if (ddlFrequency.SelectedValue == "8" || compliance.Frequency == 9)
                        {
                            if (compliance.Frequency == 9)
                            {
                                vivDueDate.Visible = false;
                                ddlDueDate.SelectedValue = null;
                            }
                            else
                            {
                                ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                vivWeekDueDays.Visible = true;
                                vivDueDate.Visible = false;
                            }
                           
                        }
                        else
                        {
                            vivWeekDueDays.Visible = false;
                            vivDueDate.Visible = true;
                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                        }
                    }
                    else
                    {
                        chkFrequency.Checked = false;
                        ddlFrequency.SelectedValue = "-1";
                        ddlDueDate.SelectedValue = "1";
                        txtTriggerNo.Text = "";
                    }

                    if (compliance.ComplianceType == 2)
                    {
                        txttriggerNoOfDays.Text = Convert.ToString(compliance.triggerNoOfDays);
                        txtIntervalDays.Text = Convert.ToString(compliance.IntervalDays);
                    }
                    else
                    {
                        txttriggerNoOfDays.Text = "";
                        txtIntervalDays.Text = "";
                    }

                    if (complianceForm.Count > 0)
                    {
                        divSampleForm.Visible = true;
                        grdSampleForm.DataSource = complianceForm;
                        grdSampleForm.DataBind();
                    }
                    else
                    {
                        divSampleForm.Visible = false;
                    }

                    if ( Convert.ToString(compliance.PenaltyDescription) != null)
                    {
                        txtPenaltyDescription.Text = compliance.PenaltyDescription.ToString();
                    }
                    if (Convert.ToString(compliance.ReferenceMaterialText) != null)
                    {
                       txtReferenceMaterial.Text = compliance.ReferenceMaterialText.ToString();
                    }

                    rdoUploadDoc.Checked = compliance.UpDocs ?? false;
                    rdoComplianceVisible.Checked = compliance.ComplinceVisible ?? false;

                    if (rdoUploadDoc.Checked == false)
                    {
                        rdoUploadDoc.Checked = false;
                        rdoNotUploadDoc.Checked = true;
                    }
                    else
                    {
                        rdoUploadDoc.Checked = true;
                        rdoNotUploadDoc.Checked = false;
                    }

                    if (rdoComplianceVisible.Checked == false)
                    {
                        rdoComplianceVisible.Checked = false;
                        rdoNotComplianceVisible.Checked = true;
                    }
                    else
                    {
                        rdoComplianceVisible.Checked = true;
                        rdoNotComplianceVisible.Checked = false;
                    }

                    ddlComplianceType_SelectedIndexChanged(null, null);


                    var compliancetag = Business.ComplianceManagement.GetComplianceDatatagMapping(complianceID);
                    List<string> CIntertagfixvalue = new List<string>();

                    foreach (var tag in compliancetag)
                    {
                        string tag1 = tag.FileTag;
                        CIntertagfixvalue.Add(tag1);
                    }

                    string nameOfString = (string.Join(",", CIntertagfixvalue.Select(x => x.ToString()).ToArray()));
                    txtCompliancetag.Text = nameOfString;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);


                    //if (complianceForm != null)
                    //{
                    //    lblSampleForm.Text = complianceForm.Name;
                    //}
                    //else
                    //{
                    //    lblSampleForm.Text = "< Not selected >";
                    //}

                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2 )//function based or time based
                    {
                        ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();

                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }

                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);

                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else if (compliance.VariableAmountPerMonth.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "2";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerInstance.HasValue ? compliance.VariableAmountPerInstance.Value.ToString() : string.Empty;
                            }

                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.ToString();
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.ToString();

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;
                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }
                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;

                            ddlMinimumYear.SelectedValue = (compliance.MinimumPeriodType ?? -1).ToString();
                            ddlMaximumYear.SelectedValue = (compliance.MaximumPeriodType ?? -1).ToString();
                        }
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;
                    }
                    else if(compliance.ComplianceType ==1)
                    {
                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }
                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);
                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else if (compliance.VariableAmountPerMonth.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "2";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerInstance.HasValue ? compliance.VariableAmountPerInstance.Value.ToString() : string.Empty;
                            }
                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.ToString();
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.ToString();

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;
                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }
                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;

                            ddlMinimumYear.SelectedValue = (compliance.MinimumPeriodType ?? -1).ToString();
                            ddlMaximumYear.SelectedValue = (compliance.MaximumPeriodType ?? -1).ToString();
                        }
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;

                       
                    }
                  
                    upComplianceDetails.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == false)
                    {
                        int complianceID = Convert.ToInt32(e.CommandArgument);
                        Business.ComplianceManagement.Delete(complianceID);
                        BindCompliancesNew();
                    }
                }
                else if (e.CommandName.Equals("STATUS"))
                {
                    lblErrorMassage.Text = "";
                    lblDeactivate.Text = "";
                    string deactivateOn = "";
                    txtShortDescriptionStatus.Text = "";
                    tbxDescriptionStatus.Text = "";
                    tbxDescriptionStatus.Text = "";
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var compliancedeactive = Business.ComplianceManagement.GetComplianceDeactivate(complianceID);

                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    BindStatusActs();
                    ddlStatusAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescriptionStatus.Text = compliance.ShortDescription;
                    tbxDescriptionStatus.Text = compliance.Description;
                    tbxSectionsStatus.Text = compliance.Sections;

                    if (compliance.DeactivateOn != null)
                    {
                        deactivateOn = Convert.ToDateTime(compliance.DeactivateOn).ToString("dd-MM-yyyy");
                    }

                    txtDeactivateDate.Text = deactivateOn;

                    txtDeactivateDesc.Text = compliance.DeactivateDesc;

                    if (compliancedeactive != null)
                    {
                        lblDeactivate.Text = compliancedeactive.Name;
                    }
                    upComplianceStatusDetails.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceStatusDialog\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindIsForSecretarial()
        {
            try
            {
                rptIsForSecretarial.DataSource = CustomerBranchManagement.GetAllIsForSecretarial();
                rptIsForSecretarial.DataBind();

                foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                {
                    CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");

                    if (!chkIsForSecretarial.Checked)
                    {
                        chkIsForSecretarial.Checked = true;
                    }
                }
                CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                IsForSecretarialSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ComplianceActiveOrInActive(int ComplianceID)
        {
            try
            {
                string result = "Active";
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).FirstOrDefault();

                if (data.Status == "D" || data.Status == null)
                {
                    result = "Active";
                }
                else
                {
                    result = "DeActive";
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "DeActive";
        }

        private void BindStatusActs()
        {
            try
            {
                ddlStatusAct.DataTextField = "Name";
                ddlStatusAct.DataValueField = "ID";

                ddlStatusAct.DataSource = ActManagement.GetAllNVP();
                ddlStatusAct.DataBind();

                ddlStatusAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetailsStatus_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(ViewState["ComplianceID"]), Date, "D", txtDeactivateDesc.Text);
                if (FileUploadDeactivateDoc.FileBytes != null && FileUploadDeactivateDoc.FileBytes.LongLength > 0)
                {
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        FileUploadDeactivateDoc.SaveAs(ConfigurationManager.AppSettings["DriveUrl"] + "/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName);
                    }
                    else
                    {
                        FileUploadDeactivateDoc.SaveAs(Server.MapPath("~/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName));
                    }
                   
                    ComplianceDeactive Deact = new ComplianceDeactive()
                    {
                        ComplianceID = Convert.ToInt32(ViewState["ComplianceID"]),
                        Name = FileUploadDeactivateDoc.FileName,
                        FileData = FileUploadDeactivateDoc.FileBytes,
                        FilePath = "~/DeactiveComplianceFiles/" + FileUploadDeactivateDoc.FileName,
                    };
                    Business.ComplianceManagement.DeleteOldDeactiveDoc(Convert.ToInt32(ViewState["ComplianceID"]));
                    Business.ComplianceManagement.CreateDeactivateFile(Deact, true);
                }
                CreateLogForDeactivation(Convert.ToInt32(ViewState["ComplianceID"]));
                var compliancedeactive = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(ViewState["ComplianceID"]));
                if (compliancedeactive.Status == "D")
                {
                    string ReplyEmailAddressName = "Avantis";
                    string DeactivateDate = Convert.ToDateTime(compliancedeactive.DeactivateOn).ToString("dd-MM-yyyy");
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ComplainceDeactivate
                                        .Replace("@ShortDescription", compliancedeactive.ShortDescription)
                                        .Replace("@DetailDescription ", compliancedeactive.Description)
                                        .Replace("@DeactivateDate", DeactivateDate)
                                        .Replace("@Requestor ", AuthenticationHelper.User)
                                        .Replace("@DeactivateDescription ", compliancedeactive.DeactivateDesc)
                                        .Replace("@From", ReplyEmailAddressName);

                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "Compliance Deactivation Request.", message);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateLogForDeactivation(long complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Log_Deactivation log = new Log_Deactivation()
                {
                    ComplianceID = complianceid,
                    Flag = "A",
                    IsCompliance = "ECompliance",
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,

                };
                entities.Log_Deactivation.Add(log);
                entities.SaveChanges();
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliancesNew();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComplianceType.SelectedValue == "0")// Normal Event Based
                {
                    lblNature.Visible = true;
                    cmpValnature.Enabled = true;
                    divNatureOfCompliance.Visible = true;
                    divNatureOfSubCompliance.Visible = true;
                    divUploadDocument.Visible = true;
                    divActionable.Visible = false;
                    divFunctionBased.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divFreqCheckbox.Visible = true;
                    divTimeBased.Visible = false;

                }
                else if (ddlComplianceType.SelectedValue == "2")// Time Based
                {
                    lblNature.Visible = true;
                    cmpValnature.Enabled = true;
                    divNatureOfCompliance.Visible = true;
                    divNatureOfSubCompliance.Visible = true;
                    divUploadDocument.Visible = true;
                    divActionable.Visible = false;
                    divFunctionBased.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divFreqCheckbox.Visible = false;
                    chkFrequency.Checked = false;
                    divTimeBased.Visible = true;

                }
                else if (ddlComplianceType.SelectedValue == "1")//checklist
                {
                    lblNature.Visible = false;
                    cmpValnature.Enabled = false;
                    divNatureOfCompliance.Visible = true;
                    divNatureOfSubCompliance.Visible = true;
                    divUploadDocument.Visible = false;
                    divActionable.Visible = true;
                    divFunctionBased.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divFreqCheckbox.Visible = true;
                    //chkFrequency.Checked = false;
                    divTimeBased.Visible = false;
                }

                chkFrequency_CheckedChanged(null,null);

                if (ddlComplianceType.SelectedValue == "2")// Time Based
                {
                    divForcefulClosure.Visible = true;
                }
                    ddlNonComplianceType_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlNonComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divMonetary.Visible = ddlNonComplianceType.SelectedValue == "0" || ddlNonComplianceType.SelectedValue == "2";
                divNonMonetary.Visible = ddlNonComplianceType.SelectedValue == "1" || ddlNonComplianceType.SelectedValue == "2";

                if (!divMonetary.Visible)
                {
                    tbxFixedMinimum.Text = tbxFixedMaximum.Text = tbxVariableAmountPerDay.Text = tbxVariableAmountPerDayMax.Text = tbxVariableAmountPercent.Text = tbxVariableAmountPercentMaximum.Text = string.Empty;
                }

                if (!divNonMonetary.Visible)
                {
                    chbImprisonment.Checked = false;
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                    ddlMinimumYear.SelectedValue = ddlMaximumYear.SelectedValue = "-1";
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeEntityTypeList", string.Format("initializeJQueryUI('{0}', 'dvEntityType');", txtEntityType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideEntityTypeList", "$(\"#dvEntityType\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePickerStart", string.Format("initializeDatePickerStart();"), true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeSecretarialList", string.Format("initializeJQueryUI('{0}', 'dvIsForSecretarial');", txtIsForSecretarial.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideSecretarialTypeList", "$(\"#dvIsForSecretarial\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            //if (!chkDocument.Checked)
            //{
            //    lblSampleForm.Text = "< Not selected >";
            //    //divFunctionBased.Visible = false;
            //}
            //else
            //{
            //    divFunctionBased.Visible = true;
            //}
            //upComplianceDetails.Update();
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAllEventCompliances(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";
                    else if (complianceInfo.RiskType == 3)
                        risk = "Critical";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }
                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActCategoryWise(Convert.ToInt32(ddlComplinceCatagory.SelectedValue));
                //BindCompliancesNew();
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlComplinceCatagory.DataTextField = "Name";
                ddlComplinceCatagory.DataValueField = "ID";

                ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplinceCatagory.DataBind();

                ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {
                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = ActManagement.GetAllNVP();
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
                 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActCategoryWise(int CategoryID)
        {
            try
            {
                var actlist = ActManagement.GetActCategoryWise(CategoryID);
                
                ddlAct1.DataTextField = "Name";
                ddlAct1.DataValueField = "ID";

                ddlAct1.DataSource = actlist;
                ddlAct1.DataBind();

                ddlAct1.Items.Insert(0, new ListItem("< Select Act >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    LinkButton lnkStatus = (LinkButton)e.Row.FindControl("lnkStatus");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    lblDeactivate.Visible = false;
                    lnkStatus.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                        lblDeactivate.Visible = true;
                        lnkStatus.Visible = true;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        lblDeactivate.Visible = false;
                        lnkStatus.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("UPDT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        lblDeactivate.Visible = false;
                        lnkStatus.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("RPER"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = false;
                        lblDeactivate.Visible = false;
                        lnkStatus.Visible = false;
                        btnSave.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("RREV"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                        lblDeactivate.Visible = true;
                        lnkStatus.Visible = true;
                        btnSave.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
   
        private void BindIndustry()
        {
            try
            {

                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                        if (!chkIndustry.Checked)
                        {
                            chkIndustry.Checked = true;
                        }
                    }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;
               

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       

        private void BindEntityType()
        {
            try
            {

                rptEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                rptEntityType.DataBind();

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");

                    if (!chkEntityType.Checked)
                    {
                        chkEntityType.Checked = true;
                    }
                }
                CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                EntityTypeSelectAll.Checked = true;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
            //BindComplianceMatrix();
            //if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
            //{
            //    setDateToGridView();
            //}
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }


        public Boolean ButtonDisplayComplianceActiveOrInActive(int ComplianceID)
        {
            try
            {
                Boolean result = true;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).FirstOrDefault();

                if (data.Status == "D" || data.Status == null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return true;
        }

        private void BindComplianceSubType(int NatureOfComplianceID)
        {
            try
            {
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";

                ddlComplianceSubType.DataSource = ComplianceCategoryManagement.GetComplianceSubTypes(NatureOfComplianceID);
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("< Nature Of Compliance Sub Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlNatureOfCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceSubType(Convert.ToInt32(ddlNatureOfCompliance.SelectedValue));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct1_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
            {
                BindCompliancesNew();
            }

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        public void ClearSelection()
        {
            lblErrorMassage.Text = string.Empty;
            tbxRequiredForms.Text = tbxDescription.Text = tbxNonComplianceEffects.Text = tbxSections.Text = string.Empty;
            ddlNatureOfCompliance.SelectedIndex = ddlComplianceType.SelectedIndex = ddlAct.SelectedIndex =  ddlNonComplianceType.SelectedIndex = ddlRiskType.SelectedIndex = 0;
            //lblSampleForm.Text = "< Not selected >";
            txtPenaltyDescription.Text = string.Empty;
            txtReferenceMaterial.Text = string.Empty;
            txtShortDescription.Text = string.Empty;
            ddlComplianceSubType.SelectedIndex = -1;
            txtSampleFormLink.Text = string.Empty;
            tbxVariableAmountPerDay.Text = string.Empty;
            tbxVariableAmountPerDayMax.Text = string.Empty;
            tbxVariableAmountPercent.Text = string.Empty;
            tbxVariableAmountPercentMaximum.Text = string.Empty;
            tbxDesignation.Text = string.Empty;
            tbxMinimumYears.Text = string.Empty;
            tbxMaximumYears.Text = string.Empty;
            txtIndustry.Text = "< Select >";
            rdoUploadDoc.Checked = false;
            rdoNotUploadDoc.Checked = true;
            rdoComplianceVisible.Checked = false;
            rdoNotComplianceVisible.Checked = true;
        }

        protected void grdSampleForm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    var file = Business.ComplianceManagement.GetSelectedComplianceFileName(ID, complianceID);
                    //Response.Buffer = true;
                    //Response.Clear();
                    //Response.ClearContent();
                    //Response.ContentType = "application/octet-stream";
                    //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.FileData); // create the file

                    //HttpContext.Current.Response.Flush(); // send it to the client to download
                    //Response.End();
                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename= " + file.Name);
                    Response.BinaryWrite(file.FileData);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
                else if (e.CommandName.Equals("DeleleSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    Business.ComplianceManagement.DeleteComplianceForm(ID, complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    grdSampleForm.DataSource = complianceForm;
                    grdSampleForm.DataBind();
                    upCompliancesList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    Act act = Business.ComplianceManagement.GetStartDateFrAct(Convert.ToInt32(ddlAct.SelectedValue));

                    if (act != null && act.ShortForm != "" && act.ShortForm != null)
                        txtshortform.Text = act.ShortForm;
                    else
                        txtshortform.Text = string.Empty;

                    if (act != null && act.ID != -1 && act.StartDate != null)
                        tbxStartDate.Text = Convert.ToDateTime(act.StartDate).ToString("dd-MM-yyyy");
                    else
                        tbxStartDate.Text = string.Empty;

                    if (act != null && act.duedatetype != -1 && act.duedatetype != null)
                        ddlDueDateType.SelectedValue = act.duedatetype.Value.ToString();


                    #region Industry
                    var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(act.ID);

                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                                chkIndustry.Checked = true;
                        }

                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                            IndustrySelectAll.Checked = true;
                        else
                            IndustrySelectAll.Checked = false;
                    }
                    #endregion

                    act = null;
                    vGetIndustryMappedIDs = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ChkIsForSecretarial_Changed(object sender, EventArgs e)
        {
            try
            {
                divSecretarial.Visible = ((CheckBox)sender).Checked;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void chkFrequency_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkFrequency.Checked == true)
                {
                    divFrequencyDuedate.Visible = true;
                    cvddlFrequency.Enabled = true;
                    reqddlDueDate.Enabled = true;
                    reqtxtTriggerNo.Enabled = true;
                    divForcefulClosure.Visible = true;
                }
                else
                {
                    cvddlFrequency.Enabled = false;
                    reqddlDueDate.Enabled = false;
                    reqtxtTriggerNo.Enabled = false;

                    ddlDueDate.SelectedValue = "1";
                    txtTriggerNo.Text = "";
                    ddlFrequency.SelectedValue = "-1";
                    divFrequencyDuedate.Visible = false;
                    divForcefulClosure.Visible = false;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "8" || ddlFrequency.SelectedValue == "9")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}