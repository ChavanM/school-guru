﻿<%@ Page Title="Upload Master Data" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript"> 
        function showNew() {
            jQuery(window).load(function () {
                $('#updateProgress').show();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Event Wise Report" BorderStyle="None" ID="EventWiseReport" CssClass="Initial" runat="server"
                    OnClick="EventWiseReport_Click" />
                <asp:Button Text="Act Wise Report" BorderStyle="None" ID="btnActWiseReport" CssClass="Initial" runat="server"
                    OnClick="btnActWiseReport_Click" />
                <asp:Button Text="Untagged Compliance Report" BorderStyle="None" ID="btnUntaggedComplianceReport" CssClass="Initial" runat="server"
                    OnClick="btnUntaggedComplianceReport_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    Event Name :
                                    <asp:DropDownList runat="server" ID="ddlEventName" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlEventName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Act Name :
                                    <asp:DropDownList runat="server" ID="ddlActName" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlActName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    State :
                                    <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                   <td>
                                    Compliance Count :
                                     <asp:Label ID="lblComplianceCount" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <asp:Panel ID="Panel1" Width="100%" Height="430px" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdEventWiseReport" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="false" Width="100%"
                                    Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="State" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" Text='<%# Eval("State") %>' ToolTip='<%# Eval("State") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px"  ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ComplianceID" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ComplianceID") %>' ToolTip='<%# Eval("ComplianceID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="570px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 570px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </asp:View>
                     <asp:View ID="View2" runat="server">
                           <table width="100%">
                            <tr>
                                <td>
                                    Act Name :
                                    <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                     
                                </td>
                                <td>
                                    View along with Compliance :
                                  <asp:CheckBox ID="chkView" Checked="true" AutoPostBack="true" OnCheckedChanged="chkView_CheckedChanged" runat="server" />
                                </td>
                                   <td>
                                    Compliance Count :
                                     <asp:Label ID="lblcomplianceCount1" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <asp:Panel ID="pnlAll" Width="100%" Height="430px" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdActWiseDetail" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="false" Width="100%"
                                    Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="20px" HeaderStyle-Height="20px"  ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ParentEvent") %>' ToolTip='<%# Eval("ParentEvent") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ComplianceID" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ComplianceID") %>' ToolTip='<%# Eval("ComplianceID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="570px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 570px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>

                             <asp:Panel ID="pnlEvent" Width="100%" Height="430px" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdActEvent" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="false" Width="100%"
                                    Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="20px" HeaderStyle-Height="20px"  ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                     
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                         <table width="100%">
                            <tr>
                                <td>
                                       Tagged Compliance :
                                  <asp:CheckBox ID="chkTagged" Checked="true" AutoPostBack="true" OnCheckedChanged="chkTagged_CheckedChanged" runat="server" />
                                </td>
                                <td>
                                     
                                </td>
                                <td>
                               
                                </td>
                                   <td>
                                    Compliance Count :
                                     <asp:Label ID="lblTaggedCompliance" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                         <div style="width: 100%; float: left; margin-bottom: 15px">
                            <asp:Panel ID="Panel2" Width="100%" Height="430px" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdCompliance" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="false" Width="100%"
                                    Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px"  ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ComplianceID" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ComplianceID") %>' ToolTip='<%# Eval("ComplianceID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="570px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 570px;">
                                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                     
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>

   
</asp:Content>
