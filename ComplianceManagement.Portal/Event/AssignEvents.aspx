﻿<%@ Page Title="Event Assignments" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="AssignEvents.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.AssignEvents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        function Validate(sender, args) {
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked").length
            if (rowCheckBoxSelected == 0 || rowCheckBoxSelected == "undefined") {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
    margin: 0;
    padding: 0.3em;
    width: 260px;
   
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventInstanceList" runat="server" UpdateMode="Conditional"
        OnLoad="upEventInstanceList_Load">
        <ContentTemplate>
            <div style="width: 100%">

                <div runat="server" id="customerdiv" style="margin-left: -62px; margin-top: 5px; float: left;">
                    <label style="width: 140px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;margin-left: 78px;">
                        Select Customer:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 26px; width: 315px;margin-left: -37px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div id="FilterLocationdiv" runat="server" style="float: left; margin-left: 20px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 320px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="322px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>
                <div runat="server" id="divFilterUsers" style="margin-left: 20px; margin-top: 5px; float: left;">
                    <label style="width: 140px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;margin-left: -8px;margin-right: -67px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                
                <%--<div runat="server" id="customerdiv" style="margin-left: 20px; margin-top: 5px; float: left;">
                             
                            <label style="width: 140px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Customer:
                    </label>
                               
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 27px; width: 393px;margin-top: 10px;margin-left: 200px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventInstanceValidationGroup"
                                Display="None" />
                                
                </div>--%>
                <div style="float: right; margin-top: 10px; margin-right: 15px">
                    <asp:LinkButton runat="server" ID="btnAssigncompliances" OnClick="btnAddEvent_Click">Add New</asp:LinkButton>
                </div>
                <br />
                <br />
                <br />
                <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                    <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" GridLines="Vertical"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                        Font-Size="12px" DataKeyNames="EventInstanceID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Location" SortExpression="CustomerBranchName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UserName" HeaderText="User" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                            <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Role" />
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAssignEventDialog">
        <asp:UpdatePanel ID="upEventinstance" runat="server" UpdateMode="Conditional" OnLoad="upEventinstance_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="EventInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="EventInstanceValidationGroup" Display="None" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Event Classification</label>
                                <asp:DropDownList ID="ddlEventClassification" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlEventClassification_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Event Classification." ControlToValidate="ddlEventClassification"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventInstanceValidationGroup"
                                    Display="None" />
                            </div>

                        <div id="customerdrop" runat="server" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomerNew" Style="padding: 0px; margin: 0px; height: 27px; width: 394px;"
                            OnSelectedIndexChanged="ddlCustomerNew_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                        <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomerNew"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventInstanceValidationGroup"
                        Display="None" />

                    </div>

                            <div style="margin-bottom: 7px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" Height="200px" Width="392px"
                                        Style="overflow: auto;margin-left: 10px;" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                                 <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Location." ControlToValidate="tbxBranch"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select Event Owner</label>
                                <asp:DropDownList ID="ddlUsers" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Event Owner." ControlToValidate="ddlUsers"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <asp:DropDownList ID="ddlEventReviewer" Visible="false" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlEventReviewer_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Act Name
                                </label>
                                   <div>
                                       <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Please select at least one Act."
                                         ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="EventInstanceValidationGroup" Display="None"></asp:CustomValidator>
                                 </div>
                                <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 150px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                    <asp:Repeater ID="rptActList" runat="server">
                                        <HeaderTemplate>
                                            <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                <tr>
                                                    <td style="width: 100px;">
                                                        <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                    <td style="width: 282px;">
                                                        <asp:Button runat="server" ID="btnRefresh" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 20px;">
                                                    <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                                <td style="width: 200px;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                        <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div style="margin-bottom: 11px; margin-left: 10px;">
                                <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>--%>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Filter Location wise classification</label>
                                <asp:DropDownList ID="ddlLocationclassification" runat="server" Style="padding: 0px; margin: 0px; height: 27px; width: 393px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlLocationclassification_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div style="margin-bottom: 7px; margin-left: 10px;">
                                <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>--%>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Filter Event</label>
                                <asp:TextBox runat="server" ID="tbxFilter" Width="384px" style="height: 19px;" AutoPostBack="true"
                                    OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <asp:GridView runat="server" ID="grdEventInstance" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdEventInstance_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdEventInstance_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" OnRowDataBound="grdEventInstance__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdEventInstance_PageIndexChanging" OnRowCommand="grdEventInstance_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Event Name" SortExpression="Sections">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 640px;">
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkEventCheck" Text="Assign" runat="server" AutoPostBack="true" OnCheckedChanged="chkEventCheck_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEvent" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Event" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="ViewEvent" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="View Event" title="View Event" /></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 320px; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="EventInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignEventDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 40px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlUsers" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divNotAssignedCompliances">
        <asp:UpdatePanel ID="upNotAssignedCompliances" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    Please assign below compliance List and then assign event.
                </div>
                <div style="margin: 5px">
                    <asp:Panel ID="Panel4" Height="515px" ScrollBars="Auto" runat="server">
                        <asp:GridView runat="server" ID="grdNoAsignedComplinace" OnPageIndexChanging="grdNoAsignedComplinace_PageIndexChanging" AutoGenerateColumns="false" GridLines="Vertical"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                            Font-Size="12px">
                            <Columns>
                                <asp:TemplateField HeaderStyle-Height="22px" HeaderText="ID" SortExpression="ID">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100;">
                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="15px" SortExpression="EvnetName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("EvnetName") %>' ToolTip='<%# Eval("EvnetName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Compliance Name" ItemStyle-Height="22px" SortExpression="ComplianceName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                            <asp:Label ID="lblName1" runat="server" Text='<%# Eval("ComplianceName") %>' ToolTip='<%# Eval("ComplianceName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:Panel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 310px; margin-top: 10px; clear: both">
                        <asp:Button Text="Assign Compliance" runat="server" Width="166px" ID="btnComplianceList" CssClass="button" OnClick="btnComplianceList_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnDvNotAssignedClose" CssClass="button" OnClick="btnDvNotAssignedClose_Click" />
                    </div>
                </div>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divAssignedCompliances">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <table runat="server" width="80%">
                    <tr>
                        <td class="td1">
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Location:</label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="txtLocation" Enabled="false" Style="padding: 0px; margin: 0px; height: 22px; width: 387px;"
                                CssClass="txtbox" />
                            <asp:Label runat="server" ID="lblBranch" Visible="false" />
                        </td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Performer:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterPerformer" Style="padding: 0px; width: 250px; margin: 0px; height: 22px; width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Performer." ControlToValidate="ddlFilterPerformer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AssignComplaiceValidationGroup"
                                Display="None" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Reviewer:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Reviewer." ControlToValidate="ddlFilterReviewer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AssignComplaiceValidationGroup"
                                Display="None" />
                        </td>
                        <td class="td3">
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Approver:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterApprover" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Start Date:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" CssClass="StartDate" ID="tbxStartDate" Style="height: 16px; width: 150px;" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ErrorMessage="Please select Start date." ControlToValidate="tbxStartDate"
                                runat="server" ValidationGroup="AssignComplaiceValidationGroup" Display="None" />
                        </td>
                        <td class="td3">
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Department:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Event Name:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                        <td class="td3" colspan="2">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="AssignComplaiceValidationGroup" />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                ValidationGroup="AssignComplaiceValidationGroup" Display="None" />
                            <asp:Label ID="Label4" runat="server" Style="color: Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Panel ID="Panel2" Height="400px" Width="950px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="gvComplianceListAssign" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                                    Font-Size="12px">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="22px" HeaderText="ID" SortExpression="ID">
                                            <ItemTemplate>
                                                <%--<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">--%>
                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <%-- </div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="15px" SortExpression="EvnetName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("EvnetName") %>' ToolTip='<%# Eval("EvnetName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Compliance Name" ItemStyle-Height="22px" SortExpression="ComplianceName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                    <asp:Label ID="lblName1" runat="server" Text='<%# Eval("ComplianceName") %>' ToolTip='<%# Eval("ComplianceName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkComplianceCheck" Text="Assign" runat="server" AutoPostBack="true" OnCheckedChanged="chkComplianceCheck_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCompliance" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                            <br />
                            <asp:Button Text="Save" runat="server" ID="SaveComplianceList" OnClick="SaveComplianceList_Click" CssClass="button"
                                ValidationGroup="AssignComplaiceValidationGroup" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divEventDetail">
        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvParentGrid" GridLines="None" runat="server" AutoGenerateColumns="false"
                                ShowFooter="true" Width="900px" DataKeyNames="Type,SequenceID"
                                OnRowDataBound="gvParentGrid_OnRowDataBound">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                    src="../Images/plus.gif" alt="" /></a>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" />
                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%">
                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                        <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="95%" DataKeyNames="EventType,SequenceID"
                                                            AutoGenerateColumns="false" OnRowDataBound="gvParentToComplianceGrid_OnRowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                    <ItemTemplate>
                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                alt="" /></a>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" />
                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" />
                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEventComplianceDays" Text='<%# Eval("Days") %>' Width="50px" runat="server"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Please enter compliance trigger Days"
                                                                            ControlToValidate="txtEventComplianceDays" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="100%">
                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                        <asp:GridView ID="gvChildGrid" GridLines="None" runat="server" Width="95%"
                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                            OnRowDataBound="gvChildGrid_OnRowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                    <ItemTemplate>
                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                alt="" /></a>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" />
                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td colspan="100%">
                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound" runat="server" Width="95%" DataKeyNames="IntermediateEventID"
                                                                                        AutoGenerateColumns="false">
                                                                                        <Columns>
                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                <ItemTemplate>
                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                            alt="" /></a>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" />
                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="IntermediateEventID1" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <tr>
                                                                                                        <td colspan="100%">
                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                                    AutoGenerateColumns="false" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound">
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:TextBox ID="txtIntermediateSubEventComlianceDays" Text='<%# Eval("Days") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                                                <asp:Label ID="lblFrequency" Text='<%# GetFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server"></asp:Label>
                                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please enter compliance trigger days"
                                                                                                                                    ControlToValidate="txtIntermediateSubEventComlianceDays" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                </asp:GridView>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                    </asp:GridView>
                                                                                    <%--Complaince--%>
                                                                                    <asp:GridView ID="gvComplianceGrid" GridLines="None" runat="server" Width="95%"
                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID" OnRowDataBound="gvComplianceGrid_OnRowDataBound">
                                                                                        <Columns>
                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox ID="txtSubEventComlianceDays" Text='<%# Eval("Days") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                    <asp:Label ID="lblFrequency1" Text='<%# GetFrequency(Convert.ToInt32(Eval("ComplianceID")))%>' runat="server"></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="margin-bottom: 7px; float: right; margin-top: 10px;">
            <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
            <div style="float: left; color: #666666; padding: 9px;">
                Informative checklist.
            </div>
            <div style="height: 30px; width: 30px; background-color: SlateBlue; float: left; margin-right: 5px"></div>
            <div style="float: left; color: #666666; padding: 9px;">
                Actionable checklist.
            </div>

            <asp:Button Text="Save" runat="server" ID="Button1" Visible="false" OnClick="btnSave_Click" CssClass="button"
                ValidationGroup="ComplianceValidationGroup" />
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divAssignEventDialog').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Assign Events",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });


            $('#divGridRowLocation').dialog({
                width: 450,
                autoOpen: false,
                draggable: true,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            }).siblings('div.ui-dialog-titlebar').remove();

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
            $("#<%= ddlUsers.ClientID %>").combobox();
            $("#<%= ddlEventClassification.ClientID %>").combobox();
            $("#<%= ddlEventReviewer.ClientID %>").combobox();
            $("#<%= ddlEvent.ClientID %>").combobox();
            $("#<%= ddlFilterPerformer.ClientID %>").combobox();
            $("#<%= ddlFilterReviewer.ClientID %>").combobox();
            $("#<%= ddlFilterApprover.ClientID %>").combobox();
            $("#<%= ddlDepartment.ClientID %>").combobox();
        }

        $(function () {
            $('#divNotAssignedCompliances').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Not Assigned Complianes List",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

        });

        $(function () {
            $('#divAssignedCompliances').dialog({
                height: 650,
                width: 1000,
                autoOpen: false,
                draggable: true,
                title: "Assign Compliane List",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

        });

        $(function () {
            $('#divEventDetail').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Event Detail",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function hideAllLocationDiv(divID) {
            $(".hideLocation").hide();
        }


        function SelectheaderCheckboxes(headerchk) {
            debugger;
            var rolecolumn;
            var crosscolumn;
            var crossheader;
            var chkheaderid = headerchk.id.split("_");

            if (chkheaderid[2] == "chkEventCheck") {
                rolecolumn = 1;
            }
            else if (chkheaderid[2] == "chkEventCheck") {
                rolecolumn = 2;
            }

            var gvcheck = document.getElementById("<%=grdEventInstance.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {

                    if (rolecolumn == 1) {
                        crosscolumn = 2;
                        crossheader = "chkEventCheck";
                    }
                    else {
                        crosscolumn = 1;
                        crossheader = "chkEventCheck";
                    }
                    gvcheck.rows[0].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = false;
                    if (gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked == true && (gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].disabled == true)) {
                        gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = true;

                    } else {
                        gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = false;
                    }

                    gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }

            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].disabled == false)
                        gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            debugger;
            var rolecolumn;
            var crosscolumn;
            var crossheader;
            var chkheaderid = headerchk.id.split("_");

            if (chkheaderid[2] == "chkComplianceCheck") {
                rolecolumn = 1;
            }
            else if (chkheaderid[2] == "chkComplianceCheck") {
                rolecolumn = 2;
            }

            var gvcheck = document.getElementById("<%=grdEventInstance.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {

                    if (rolecolumn == 1) {
                        crosscolumn = 2;
                        crossheader = "chkComplianceCheck";
                    }
                    else {
                        crosscolumn = 1;
                        crossheader = "chkComplianceCheck";
                    }
                    gvcheck.rows[0].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = false;
                    if (gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked == true && (gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].disabled == true)) {
                        gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = true;

                    } else {
                        gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = false;
                    }

                    gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }

            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].disabled == false)
                        gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            debugger;
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdEventInstance.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");
            var rowcount = gvcheck.rows.length;

            if (chkheaderid[2] == "chkEventCheck") {
                rolecolumn = 1;
            }
            else if (chkheaderid[2] == "chkEventCheck") {
                rolecolumn = 2;
            }

            if (rolecolumn == 1) {
                crosscolumn = 2;
                crossheader = "chkEventCheck";
            }
            else {
                crosscolumn = 1;
                crossheader = "chkEventCheck";
            }

            for (i = 0; i < gvcheck.rows.length - 1; i++) {
                if ((gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked && gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].disabled != true) && gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].disabled == true) {
                    gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = true;
                }
                else if (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].checked && (gvcheck.rows[i + 1].cells[rolecolumn].getElementsByTagName("INPUT")[0].disabled != true)) {
                    gvcheck.rows[i + 1].cells[crosscolumn].getElementsByTagName("INPUT")[0].checked = false;
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }

    </script>
</asp:Content>
