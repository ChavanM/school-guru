﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.ViewPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
   <%-- <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>--%>

    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtRemindOn]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function postbackOnCheck() {
            var o = window.event.srcElement;
            if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
            { __doPostBack("", ""); }
        }

        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        function ClosemodalPopup() {
            $('#modalPopup').modal('hide');

            return true;
        }
        function ClosemodaelShortNotice() {
            $('#modalShortNotice').modal('hide');

            return true;
        }
        function openmodalPopup() {
            if (Displays() == true) {
                $('#modalPopup').modal('show');
            }
            return true;
        }

        $(document).ready(function () {
            fhead('Closed Events');
        });s

        function CloseMe() {
            window.parent.CloseMyReminderPopup();
        }
    </script>

    <style>

        .circle {
            background-color: green;
            width: 29px;
            height: 30px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 5px;
        }

        .ui-widget-header {
            border-bottom: 2px solid #dddddd;
            background: #999 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
        }

        table tr th 
        {
           color: white;
           font-size: 15px;
        }

        .Inforamative {
            color: Chocolate !important;
        }

        tr.Inforamative > td {
            color: Chocolate !important;
        }

        .Actionable {
            color: SlateBlue !important;
        }

        tr.Actionable > td {
            color: SlateBlue !important;
        }

    </style>

</head>
<body style="background: none !important;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>
            <%--<div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
            <%--<div class="modal-dialog" style="width: 883px">--%>
                <%--<div class="modal-content">--%>
                    <div class="modal-body" style="width: 863px;">
                        <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 4px;">
                                    <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div style="margin-bottom: 7px;margin-top: -19px;" runat="server" id="divSubEventmode">
                                                    <div style="z-index: 10" id="divSubevent">
                                                        <asp:gridview id="gvParentGrid" runat="server" autogeneratecolumns="false"
                                                            showfooter="true" width="790px" borderwidth="0px" gridlines="None" class="tablenew" datakeynames="Type,Date"
                                                            onrowdatabound="gvParentGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                    src="../Images/plus.gif" alt="" /></a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name"  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true"/>
                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                            <%-- <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>--%>
                                                          <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" style="border-radius: 9px;border-color: #c7c7cc; text-align :center" BackColor ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                       <ItemTemplate>
                                                            <asp:Button ID="BtnAddgvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleParentEventAddButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Add" />
                                                            <asp:Button ID="BtnUpdategvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                        <asp:GridView ID="gvParentToComplianceGrid" borderwidth="0px"  gridlines="None" style="color: #666666;font-size:14px;" runat="server" Width="98%" DataKeyNames="EventType,SequenceID" onrowdatabound="gvParentToComplianceGrid_OnRowDataBound"
                                                                            AutoGenerateColumns="false" CellSpacing="5" >
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblParentStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvChildGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvChildGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub event name" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtgvChildGridDate" CssClass="StartDate" style=" text-align :center; border-radius: 9px;border-color: #c7c7cc;" Visible='<%# visibleTxtgvChildGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  Width="100px" runat="server"></asp:TextBox>
                                                                                     </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="BtnAddgvChildGrid" runat="server" class="btn btn-search" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" class="btn btn-search" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvComplianceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvComplianceGrid_OnRowDataBound"
                                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center" >
                                                                                                            <ItemTemplate>
                                                                                                            <asp:Label ID="lblStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                           </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                        <%--<RowStyle BorderStyle="Solid" BorderColor="Black" BorderWidth="1px" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- Parent -> Intermediate Event -> Sub Event -> Compliance--%>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvIntermediateGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvIntermediateGrid_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="IntermediateEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Intermediate Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <%--Sub Event--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" borderwidth="0px" gridlines="None" class="tablenew"  OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound"
                                                                                                         runat="server" Width="98%" DataKeyNames="IntermediateEventID,ParentEventID"
                                                                                                        AutoGenerateColumns="false">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                <ItemTemplate>
                                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                                            alt="" /></a>
                                                                                                                  <%--  <asp:Label ID="lblIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate> 
                                                                                                                    <asp:TextBox ID="txtgvIntermediateSubEventGridDate" style="border-radius: 9px; border-color: #c7c7cc; text-align:center;"  Visible='<%# visibleTxtgvIntermediateSubEventGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="BtnAddIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleIntermediateSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="BtnUpdateIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td colspan="100%">
                                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound"
                                                                                                                                    AutoGenerateColumns="false">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                        <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                                                                        <ItemTemplate>
                                                                                                                                         <asp:Label ID="lblIntermediateStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                                                         </ItemTemplate>
                                                                                                                                       </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                    <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                                                </asp:GridView>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                            </asp:gridview>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 400px;">
                                                <div style="margin-bottom: 7px;" runat="server" id="div1111">
                                                    <label style="width: 390px; display: block; float: left; font-size: 16px; color: #333;">
                                                        Nature of event
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 400px;">
                                                <asp:TextBox ID="txtDescription" class="form-control" Rows="3" Width="802px" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <div class="clearfix"></div>
                                        <tr>
                                            <td>
                                                <table width="100%" id="eventnaturelegend" style="margin-top: 10px">
                                                    <tr>
                                                        <td>
                                                            <label style="width: 50px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Note :
                                                            </label>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div class="circle" style="float: left"></div>
                                                            <div style="float: left; color: #666666; padding: 9px; font-family: 'Roboto',sans-serif;">
                                                                Completed / Closed compliance
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px; color: #666666;">
                                                            <div style="height: 30px; width: 81px; border-radius: 14px; background-color: lightgreen; float: left; margin-right: 5px; padding: 9px;">DD-MM-YYYY </div>
                                                            <div style="float: left; padding: 9px;">
                                                                Completed steps
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
                                                            <div style="float: left; color: #666666; padding: 9px;">
                                                                Informative task no action required.
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:HiddenField ID="hdnTitle" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--  </div>--%>
                    </div>
 </div>
            </div>
                <%--</div>
                </div>
            </div>--%>
    </form>
</body>
</html>
