﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EventClose.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventClose" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

<style type="text/css">
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0px;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 30px;
            background: white;
            border: none;
            margin-left: -3px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

       .k-grid-pager {
            margin-top: -1px;
            //margin-right: -2px;
            border-width: 1px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -8px;
            margin-right: -1px;
            background-color: white;           
            //border-bottom: 0px;
        }
        .k-grouping-header {
            font-style: italic;
            background-color: white;
            border-right: 1px solid;
            //border-left: 1px solid;
            border-top: 1px solid;
            margin-right: -2px;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-grid-content, .k-grid-content-locked, .k-pager-wrap 
        {
           white-space: normal;
       }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>

    <script type="text/javascript">

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;


            if (locationlist.length > 0
                || ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventClassificationID", operator: "eq", value: parseInt($("#dropdownlistComplianceType1").val())
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        //function FilterGrid() {

        //    //location details
        //    var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
        //    var locationsdetails = [];
        //    $.each(list1, function (i, v) {
        //        locationsdetails.push({
        //            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        //        });
        //    });

        //    // type to Search
        //    var searchValue = $('#Searchfilter').val();

        //    //ComplianceType Details
        //    var Statusdetails = [];
        //    var liststatus = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
        //    $.each(liststatus, function (i, v) {
        //        Statusdetails.push({
        //            field: "Type", operator: "eq", value: v
        //        });
        //    });

        //    ////ComplianceType details
        //    //if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
        //    //    Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
        //    //}

        //    var dataSource = $("#grid").data("kendoGrid").dataSource;

        //    //two
        //    if (Statusdetails.length > 0
        //        && locationsdetails.length > 0) {
        //        dataSource.filter({
        //            logic: "and",
        //            filters: [
        //                {
        //                    logic: "or",
        //                    filters: Statusdetails
        //                },
        //                {
        //                    logic: "or",
        //                    filters: locationsdetails
        //                }
        //            ]
        //        });
        //    }
        //        //one
        //    else if (Statusdetails.length > 0) {
        //        dataSource.filter({
        //            logic: "and",
        //            filters: [
        //                {
        //                    logic: "or",
        //                    filters: Statusdetails
        //                }
        //            ]
        //        });
        //    }
        //    else if (locationsdetails.length > 0) {
        //        dataSource.filter({
        //            logic: "and",
        //            filters: [
        //                {
        //                    logic: "or",
        //                    filters: locationsdetails
        //                }
        //            ]
        //        });
        //    }
        
        //}


        $('#Searchfilter').on('input', function (e) {
            FilterGrid();
        });


        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            //$("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            //$('#Searchfilter').val('');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            //if (div == 'filterstatus') {
            //    $('#' + div).append('Compliance Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            //    $('#ClearfilterMain').css('display', 'block');
            //}
            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }


            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        //function OpenAdvanceSearchFilter(e) {
        //    $('#divAdvanceSearchFilterModel').modal('show');
        //    e.preventDefault();
        //    return false;
        //}

    </script>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        //function fclosebtn(tbn) {
        //    $('#' + tbn).css('display', 'none');
        //    $('#' + tbn).html('');
        //}

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                         read: {
                            url: '<% =Path%>Data/GetClosedEvent?Userid=<% =UId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetClosedEvent?Userid=<% =UId%>'

                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 15
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                //height: 523,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All',5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                columns: [

                    {
                        field: "CustomerBranchName", title: 'Location',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                    , {
                        field: "Name", title: 'Name',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EventDescription", title: 'Nature of event',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "UserName", title: 'User',
                        width: "15%;",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Role", title: 'Role',
                        width: "15%;",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" },
                            //{ name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true, width: "7%;",// width: 150,
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Overview";
                }
            });

            $('#grid').kendoTooltip({
                filter: "td[role=gridcell]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                position: 'down'
            }).data("kendoTooltip");

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                $('#divShowReminderDialog').modal('show');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                //alert(item.EventClassificationID);
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Event/ViewPage.aspx?ID=" + item.ID + "&EventScheduledOnID=" + item.EventScheduledOnId + "&Type=" + item.Type + "&CustomerBranchID=" + item.CustomerBranchID + "&EventClassificationID=" + item.EventClassificationID + "&UserID=" + item.UserID + "&CustomerID=" + item.CustomerID + "&EventDescription=" + item.EventDescription + "&ComplianceID=" + item.ComplianceID)

            });
        }


        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }

        function BindGridApply(e) {
            BindGrid();
            FilterAll();
            e.preventDefault();
        }
        $(document).ready(function () {
            BindGrid();

            $("#txtSearchfilter").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "CustomerBranchName" || x.field == "Name" || x.field == "EventDescription" || x.field == "UserName" || x.field == "Role") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);

            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                placeholder: "Select",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Event Type",
                autoClose: true,
                dataSource: [
                    { text: "Secretarial", value: "1" },
                    { text: "Non-Secretarial", value: "2" },
                ],
                index: 0,
                change: function (e) {
                    FilterAll();

                }
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {    
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            
        });

        function AddnewTest(e) {
            $('#divShowReminderDialog').modal('show');
            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Event/ViewPage.aspx?AccessID=0")
            e.preventDefault();
           }
    
        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }

      function CloseClearPopup() {
          $('#APIOverView').attr('src', "../Common/blank.html");
      }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        
            <div class="row">
                <div class="row">
                    <input id="dropdownlistComplianceType1" data-placeholder="Select" style="width: 190px; margin-right: 10px;" />
                    <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 341px; margin-right: 10px;"/>
                    <input id='txtSearchfilter' onkeydown="return(event.keyCode!=13);" class='k-textbox' placeholder="Type to Filter" style="width: 150px;"/>
                    <%--<button id="Applyfilter" style="margin-left: 1%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button> --%>
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>

            <div class="clearfix" style="height: 10px;"></div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filtersstoryboard">&nbsp;</div>
            <div id="grid"></div>
        </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 913px; margin-left: -122px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body" style="margin-left: 17px;">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="300px" style="/*width: 1092px;*/ height: 330px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <script>

            $(document).ready(function () {
                fhead('Closed Events');
            });

    </script>

</asp:Content>

