﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="FrmEventSequence.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.FrmEventSequence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">

        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlparentEvent.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="ComplianceValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
            </div>
            <div style="margin-bottom: 7px; float: right; margin-right: 520px; margin-top: 10px;">
                <asp:RadioButtonList runat="server" ID="rblRelationship" RepeatDirection="Horizontal" RepeatLayout="Flow"
                    OnSelectedIndexChanged="rblRelationship_SelectedIndexChanged" Font-Bold="true" AutoPostBack="true">
                   <%-- <asp:ListItem Text="Public" Value="0" Selected="True" />
                    <asp:ListItem Text="Private" Value="1" />
                    <asp:ListItem Text="Listed" Value="2" />
                    <asp:ListItem Text="Non-Secretarial" Value="3" />--%>
                    <asp:ListItem Text="Public" Value="1" Selected="True" />
                    <asp:ListItem Text="Private" Value="2" />
                    <asp:ListItem Text="Listed" Value="3" />
                    <asp:ListItem Text="Non-Secretarial" Value="4" />
                    <asp:ListItem Text="LLP" Value="5" />
                    <asp:ListItem Text="Public Section 8" Value="12" />
                    <asp:ListItem Text="Private Section 8" Value="13" />
                </asp:RadioButtonList>
            </div>
            <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
                <asp:DropDownList runat="server" ID="ddlparentEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlparentEvent_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                    CssClass="txtbox" />
                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select parent Event Name."
                    ControlToValidate="ddlparentEvent" runat="server" ValueToCompare="-1" Operator="NotEqual"
                    ValidationGroup="ComplianceValidationGroup" Display="None" />
            </div>
            <table width="100%" align="center">
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvParentGrid" GridLines="None" runat="server" AutoGenerateColumns="false"
                            ShowFooter="true" Width="900px" DataKeyNames="Type"
                            OnRowDataBound="gvParentGrid_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                            <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                src="../Images/plus.gif" alt="" /></a>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" />
                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" />

                                <%-- Parent -> Compliance--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="100%">
                                                <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                    <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="95%" DataKeyNames="EventType"
                                                        AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                            alt="" /></a>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" />
                                                            <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" />
                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="SequenceID" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtEventComplianceSequence" Text='<%# Eval("SequenceID") %>' Width="50px" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Please enter compliance Sequence ID"
                                                                        ControlToValidate="txtEventComplianceSequence" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                        <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- Parent -> Sub Event -> Compliance--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="100%">
                                                <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                    <asp:GridView ID="gvChildGrid" runat="server" Width="95%" GridLines="None"
                                                        AutoGenerateColumns="false" DataKeyNames="ParentEventID"
                                                        OnRowDataBound="gvChildGrid_OnRowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                            alt="" /></a>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" />
                                                            <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" />
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="SequenceID" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtSubEventSequence" Text='<%# Eval("SequenceID") %>' Width="50px" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Please enter subevent Sequence ID"
                                                                        ControlToValidate="txtSubEventSequence" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--Intermdaite Event--%>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td colspan="100%">
                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound" runat="server" Width="95%" DataKeyNames="intermediateEventID"
                                                                                    AutoGenerateColumns="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-Width="20px">
                                                                                            <ItemTemplate>
                                                                                                <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                    <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                        alt="" /></a>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" />
                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="SequenceID" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtIntermediateSubEventSequence" Text='<%# Eval("SequenceID") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Please enter intermediate sub event Sequence ID"
                                                                                                    ControlToValidate="txtIntermediateSubEventSequence" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td colspan="100%">
                                                                                                        <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                            <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                                AutoGenerateColumns="false">
                                                                                                                <Columns>
                                                                                                                    <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                    <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="SequenceID" ItemStyle-HorizontalAlign="Center">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:TextBox ID="txtIntermediateComplianceSequence" Text='<%# Eval("SequenceID") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Please enter intermediate compliance Sequence ID"
                                                                                                                                ControlToValidate="txtIntermediateComplianceSequence" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                            </asp:GridView>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                    <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                </asp:GridView>
                                                                                <%--Complaince--%>
                                                                                <asp:GridView ID="gvComplianceGrid" runat="server" Width="95%" GridLines="None"
                                                                                    AutoGenerateColumns="false">
                                                                                    <Columns>
                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="SequenceID" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtSubEventComlianceSequence" Text='<%# Eval("SequenceID") %>' Width="50px" runat="server"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please enter compliance Sequence ID"
                                                                                                    ControlToValidate="txtSubEventComlianceSequence" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        </Columns>
                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        </asp:GridView>
                    </td>
                </tr>
             <%--   <tr>
                    <%=DataString%>
                </tr>--%>
            </table>

        </asp:Panel>
        <div style="margin-bottom: 7px; float: right; margin-right: 750px; margin-top: 10px;">
            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                ValidationGroup="ComplianceValidationGroup" />
        </div>
    </div>
</asp:Content>

