﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventParentChildRelationship.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventParentChildRelationship" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlEvents.ClientID %>").combobox();
            $("#<%= ddlEventCompliance.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }


    </script>
    <style type="text/css">
        .td1 {
            width: 25%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 25%;
        }

        .td4 {
            width: 25%;
        }
    </style>
    <script type="text/javascript">

        function UncheckParentEventHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkParentEvent']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkParentEvent']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ParentEventSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function UncheckComplianceHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCompliance']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompliance']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ComplianceSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function UncheckSubEventHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkSubEvent']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkSubEvent']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='SubEventSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }


        function checkAllPE(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkParentEvent") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function checkAllEC(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompliance") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }


        function checkAllSE(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkSubEvent") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="ComplianceValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
            </div>
            <div runat="server" style="float: left; margin-left: 100px; margin-top: 5px; width: 100%">
                <table runat="server" width="70%">
                    <tr>
                        <td class="td1">
                            <div style="margin: 10px 20px 10px 294px">
                                <div>
                                    <%--  Relationship :--%>
                                    <asp:RadioButtonList runat="server" ID="rblRelationship" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        OnSelectedIndexChanged="rblRelationship_SelectedIndexChanged" Font-Bold="true" AutoPostBack="true">
                                        <asp:ListItem Text="Public" Value="0" Selected="True" />
                                        <asp:ListItem Text="Private" Value="1" />
                                        <asp:ListItem Text="Listed" Value="2" />
                                        <asp:ListItem Text="Non-Secretarial" Value="3" />
                                       <%-- <asp:ListItem Text="Unlisted" Value="3" />--%>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:MultiView ID="Multiview1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                       <%-- <td align="center">--%>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <asp:Label runat="server" ID="lblEventType" Text="Event :" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;"></asp:Label>
                                <asp:DropDownList runat="server" ID="ddlEvents" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 542px;"
                                    CssClass="txtbox" OnSelectedIndexChanged="ddlEvents_SelectedIndexChanged">
                                </asp:DropDownList>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                 <asp:CheckBox Font-Bold ="true" Text="Intermediate" runat="server" ID="chkIntermediate" CssClass="txtbox"/>
                                <asp:CompareValidator ErrorMessage="Please Select Event." ControlToValidate="ddlEvents" ID="rfEvents"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <asp:Label runat="server" ID="lblEventTypeList" Text="Event List :" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;"></asp:Label>
                                </label>
                                        <asp:TextBox runat="server" ID="txtParentEvent" Style="padding: 0px; margin: 0px; height: 22px; width: 950px;"
                                            CssClass="txtbox" />
                                <div style="margin-left: 212px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; width: 949px; height: 350px;" id="dvParentEvent">
                                    <asp:Repeater ID="rptParentEvent" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%;" class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                <tr>
                                                    <td style="width: 50px;">
                                                        <asp:CheckBox ID="parentEventSelectAll" Text="Select All" runat="server" onclick="checkAllPE(this)" /></td>
                                                    <td style="width: 282px;">
                                                        <asp:Button runat="server" ID="btnPERepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 20px;">
                                                    <asp:CheckBox ID="chkParentEvent" runat="server" onclick="UncheckParentEventHeader();" /></td>
                                                <td style="width: 200px;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 940px; padding-bottom: 5px;">
                                                        <asp:Label ID="lblParentEventID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblParentEventName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: right; margin-right: 865px; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                                    ValidationGroup="ComplianceValidationGroup" />
                            </div>
                       <%-- </td>--%>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                       <%-- <td align="center">--%>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                    ValidationGroup="ComplianceValidationGroup" />
                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                                <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <asp:Label runat="server" ID="Label2" Text="Event :" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;"></asp:Label>
                                <asp:DropDownList runat="server" ID="ddlEventCompliance" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 542px;"
                                    CssClass="txtbox" OnSelectedIndexChanged="ddlEventCompliance_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please Select Event." ControlToValidate="ddlEventCompliance" ID="CompareValidator1"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <asp:Label runat="server" ID="Label3" Text="Compliance List :" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;"></asp:Label>
                                </label>
                                        <asp:TextBox runat="server" ID="txtCompliance" Style="padding: 0px; margin: 0px; height: 22px; width: 550px;"
                                            CssClass="txtbox" />
                                <div style="margin-left: 212px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; width: 550px; height: 200px;" id="dvComplianceEvent">
                                    <asp:Repeater ID="rptCompliance" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%;" class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                <tr>
                                                    <td style="width: 50px;">
                                                        <asp:CheckBox ID="complianceSelectAll" Text="Select All" runat="server" onclick="checkAllEC(this)" /></td>
                                                    <td style="width: 282px;">
                                                        <asp:Button runat="server" ID="btnPERepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 20px;">
                                                    <asp:CheckBox ID="chkCompliance" runat="server" onclick="UncheckComplianceHeader();" /></td>
                                                <td style="width: 200px;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                        <asp:Label ID="lblComplianceID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblComplianceName" runat="server" Text='<%# Eval("ShortDescription")%>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: right; margin-right: 865px; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSaveCompliance" OnClick="btnSaveCompliance_Click" CssClass="button"
                                    ValidationGroup="ComplianceValidationGroup" />
                            </div>
                       <%-- </td>--%>
                    </asp:View>
                </asp:MultiView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
