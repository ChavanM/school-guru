﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EventDeactivation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventDeactivation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
          $(function () {

            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlEvent.ClientID %>").combobox();
            $("#<%= ddlEventNature.ClientID %>").combobox();
            $("#<%= ddlCustomer.ClientID %>").combobox();

        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
       <div id="divEventDialog" style="height: auto;">
        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional" OnLoad="upEvent_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="EventValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="EventValidationGroup" Display="None" />
                    </div>
                </div>
                <table runat="server" width="50%" align="center">
                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Customer 
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlCustomer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true"  Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Customer"
                                ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="EventValidationGroup" Display="None" />
                        </td>
                      
                    </tr>

                    <tr>
                        <td class="auto-style5">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Location</label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" AutoPostBack="true" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 342px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1"  OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="240px"
                                    Style="overflow: auto" ShowLines="true" >
                                </asp:TreeView>
                                <asp:CompareValidator ID="CompareValidator3" ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                                    Display="None" />
                            </div>
                        </td>
                    </tr>
                
                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Event Name</label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndesxChanged"  Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Event"
                                ControlToValidate="ddlEvent" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="EventValidationGroup" Display="None" />
                        </td>
                        <%-- <td class="td3" align="right"></td>
                        <td class="td4" align="left"></td>--%>
                    </tr>

                    <tr>
                        <td align="left" style="margin-bottom: 4px">
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Event Nature
                            </label>
                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:DropDownList runat="server" ID="ddlEventNature" OnSelectedIndexChanged="ddlEventNature_SelectedIndexChanged"  AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Event Nature"
                                ControlToValidate="ddlEventNature" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="EventValidationGroup" Display="None" />
                        </td>
                    
                    </tr>
                 
                 
                  
              </table>
                <br />
                <div style="margin-left:250px;">
                      <asp:Panel ID="Panel1" Width="100%" Height="300px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdEvent" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="1000"
                                    Font-Size="14px">

                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer Branch Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("CustomerBranch") %>' ToolTip='<%# Eval("CustomerBranch") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblScheduleOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduleOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>

                                                </div>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                </div>

                  <table>
                    <tr>
                         <td class="td1" align="left" style="margin-bottom: 4px">
                            <asp:Button ID="btndelete" runat="server"  OnClientClick="return confirm('Are you sure!! You want to Delete schedules ?');" OnClick="btndelete_Click" Style="width: 80px; height: 30px; margin-left: 520px; margin-bottom: 4px" Text="Delete" ValidationGroup="EventValidationGroup" />

                        </td>
                        <td class="td2" align="left" style="margin-bottom: 4px">
                            <asp:Button ID="btnKeep" runat="server"  OnClientClick="return confirm('Are you sure!! You want to Keep schedules ?');" OnClick="btnKeep_Click" Style="width: 80px; height: 30px; margin-left: 60px; margin-bottom: 4px" Text="Keep" ValidationGroup="EventValidationGroup" />
                        </td>
                    </tr>
                </table>
                 
                </ContentTemplate>
            </asp:UpdatePanel>
</div>
</asp:Content>
