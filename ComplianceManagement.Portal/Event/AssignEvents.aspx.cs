﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Configuration;
using System.Text;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class AssignEvents : System.Web.UI.Page
    {
        static int type = 0;
        protected int UserID = -1;
        protected int customerid = -1;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    customerdiv.Visible = true;
                    //lblcustomer.Visible = true;
                    customerdrop.Visible = true;
                    BindCustomers(UserID);
                    //BindCustomersDrop(UserID);
                }
                else
                {
                    customerdiv.Visible = false;
                    //lblcustomer.Visible = false;
                    customerdrop.Visible = false;
                    BindTreeView(tvFilterLocation);
                    BindLocation();
                    tbxFilterLocation.Text = "< Select >";
                }

                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "CustomerBranchName";

                //BindTreeView(tvFilterLocation);
                BindUsers(ddlFilterUsers, customerid);
                BindEventClassification();
                BindLocationClassification();
                BindEventInstances(10, customerid);
                BindAct();
                BindUsers(ddlUsers, customerid);
                BindUsers(ddlFilterPerformer, customerid);
                BindUsers(ddlFilterReviewer, customerid);
                BindApproverUsers(ddlFilterApprover);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            if ((!string.IsNullOrEmpty(ddlCustomer.SelectedValue)) && ddlCustomer.SelectedValue != "-1")
            {
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
            }
            BindTreeView(tvFilterLocation);
            BindUsers(ddlFilterUsers, customerID);
            BindUsers(ddlUsers, customerID);
            BindEventInstances(10, customerID);
            BindUsers(ddlFilterPerformer, customerID);
            BindUsers(ddlFilterReviewer, customerID);
            BindApproverUsers(ddlFilterApprover);
        }

        private void BindCustomers(int userid)
        {
            try
            {
                var Customedata = Assigncustomer.GetAllCustomer(userid);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = Customedata;
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));


                ddlCustomerNew.DataTextField = "Name";
                ddlCustomerNew.DataValueField = "ID";

                ddlCustomerNew.DataSource = Customedata;
                ddlCustomerNew.DataBind();

                ddlCustomerNew.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        private void BindDepartment()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                customerID = Convert.ToInt32(customerID);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.Items.Clear();
                var deptdetails = CompDeptManagement.FillDepartment(customerID);
                ddlDepartment.DataSource = deptdetails;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("< Select Department >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEvents(int type, int CustomerBranchID, int pageIndex)
        {
            try
            {
                grdEventInstance.DataSource = null;
                grdEventInstance.DataBind();
                List<long> actIds = new List<long>();

                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }

                if (actIds.Count != 0)
                {

                    if (pageIndex == null || pageIndex.ToString() == "")
                    {
                        pageIndex = 0;
                    }

                    long EventClassificationID = Convert.ToInt64(ddlEventClassification.SelectedValue);

                    long LocationClassificationID = Convert.ToInt64(ddlLocationclassification.SelectedValue);

                    grdEventInstance.DataSource = EventManagement.GetAllEventsWithAssignedDays(type, CustomerBranchID, EventClassificationID, LocationClassificationID, tbxFilter.Text, actIds);
                    grdEventInstance.PageIndex = pageIndex;
                    grdEventInstance.DataBind();
                }

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                upEventinstance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCheckdEvents(List<long> EventList)
        {
            try
            {
                ddlEvent.DataTextField = "Name";
                ddlEvent.DataValueField = "ID";

                ddlEvent.DataSource = EventManagement.GetCheckdEvent(EventList);
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, 0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindEventInstances(int EventRoleID, int customerID, int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                try
                {                    
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    else if (AuthenticationHelper.Role == "MGMT")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    else if (AuthenticationHelper.Role == "IMPT")
                    {
                        if (ddlCustomerNew.SelectedValue != null && ddlCustomerNew.SelectedValue != "-1")
                            customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                    }
                    else
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    int branchID = -1;
                    if ((!string.IsNullOrEmpty(tvFilterLocation.SelectedValue)))
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    int userID = -1;
                    if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                    }

                    if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                    {
                        userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    }

                    string Role = Convert.ToString(AuthenticationHelper.Role);

                    var dataSource = EventManagement.GetAllAssignedEventsForCompanyAdmin(branchID, userID, customerID);    //Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)

                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Ascending;
                    }

                    grdEventList.PageIndex = pageIndex;
                    grdEventList.DataSource = dataSource;
                    grdEventList.DataBind();

                    if (isBranchChanged)
                    {
                        if (branchID != -1)
                        {
                            BindUsers(ddlFilterUsers, customerID, dataSource.Select(entry => entry.UserID.Value).Distinct().ToList());
                        }
                        else
                        {
                            BindUsers(ddlFilterUsers, customerID);
                        }

                        ddlFilterUsers.SelectedValue = "-1";
                    }

                    upEventInstanceList.Update();
                    ForceCloseFilterBranchesTreeView();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue.Length > 0)
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if (ddlFilterUsers.SelectedValue.Length > 0)
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }

                var assignmentList = EventManagement.GetAllAssignedEventsForCompanyAdmin(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }

                grdEventList.DataSource = assignmentList;
                grdEventList.DataBind();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int customerID = -1;
            BindEventInstances(10, customerID, pageIndex: e.NewPageIndex);
        }

        private void BindAct()
        {
            try
            {                
                rptActList.DataSource = CustomerBranchManagement.BindAct(); ;
                rptActList.DataBind();

                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                    if (!chkAct.Checked)
                    {
                        chkAct.Checked = true;
                    }
                }
                CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                actSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
            grdEventInstance.PageIndex = 0;
            BindEvents(type, CustomerBranchID, 0);
        }

        protected void upEventinstance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upEventInstanceList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTreeView(TreeView tvLocation)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomer.SelectedValue != null && ddlCustomer.SelectedValue != "-1")
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                tbxFilterLocation.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                }

                tvLocation.ExpandAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindEventInstances(10, customerID);

        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, int customerID, List<long> ids = null)
        {
            try
            {
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }


                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                //users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select Event Owner >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindApproverUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                //users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select Approver >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindReviewer(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlEventReviewer.DataTextField = "Name";
                ddlEventReviewer.DataValueField = "ID";
                ddlEventReviewer.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                // users.Insert(0, new { ID = -1, Name = ddlUserList == ddlEventReviewer ? "< Select >" : "< All >" });

                ddlEventReviewer.DataSource = users;
                ddlEventReviewer.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            BindEventInstances(10, customerID);
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var EventList = EventManagement.GetAllAssignedInstancesByUser(Convert.ToInt32(ddlUsers.SelectedValue), Convert.ToInt64(UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue)).CustomerID));
                var UserEventList = EventList.GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();

                if (EventList.Count > 0)
                {
                    Session["EventLocation"] = null;
                    List<EventLocationProperty> EventLocationList = new List<EventLocationProperty>();
                    foreach (var eve in UserEventList)
                    {
                        var locationList = EventList.Where(entry => entry.ID == eve.ID).GroupBy(entry => entry.CustomerBranchID).Select(en => en.FirstOrDefault()).ToList();
                        string ids = string.Empty;
                        string names = string.Empty;
                        foreach (var stringnodes in locationList)
                        {
                            ids += stringnodes.CustomerBranchID + ",";
                            names += stringnodes.CustomerBranchName + ",";
                        }

                        EventLocationProperty EventLocation = new EventLocationProperty();
                        EventLocation.EventID = Convert.ToInt32(eve.ID);
                        EventLocation.BranchIds = ids;
                        EventLocation.BranchNames = names;
                        EventLocationList.Add(EventLocation);
                    }

                    Session["EventLocation"] = EventLocationList;
                }
                // BindEvents();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                //long customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                Boolean IsIndustryMapped = EventManagement.CheckIndustryMapping(branchID1);

                if (IsIndustryMapped == true)
                {
                    List<Tuple<int, bool, bool>> eventList = new List<Tuple<int, bool, bool>>();
                    SaveCheckedValues();
                    eventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;
                    List<long> EventList = new List<long>();
                    for (int i = 0; i < eventList.Count; i++)
                    {
                        long EventID = eventList[i].Item1;
                        int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        bool FlgCheck = false;
                        FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                        if (FlgCheck == true)
                        {
                            continue;
                        }
                        EventList.Add(EventID);
                    }

                    //int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                    var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);

                    Session["exceptComplianceIDs"] = exceptComplianceIDs;
                    Session["EventList"] = EventList;
                    if (exceptComplianceIDs.Count > 0)
                    {
                        List<long> EventList1 = new List<long>();
                        EventList1 = (List<long>)Session["EventList"];
                        List<long> EvtList = EventList1.Select(i => (long)i).ToList();
                        BindCheckdEvents(EvtList);

                        grdNoAsignedComplinace.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, exceptComplianceIDs, EventList, branchID1, -1);
                        grdNoAsignedComplinace.DataBind();
                        upNotAssignedCompliances.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);
                    }
                    else
                    {
                        List<string> eventlist = new List<string>();
                        for (int i = 0; i < eventList.Count; i++)
                        {
                            List<EventInstance> eventInstanceOwner = new List<EventInstance>();
                            List<EventAssignment> eventAssignmentOwner = new List<EventAssignment>();
                            int EventID = eventList[i].Item1;
                            long userID = Convert.ToInt64(ddlUsers.SelectedValue);
                            long ReviewerID = Convert.ToInt64(ddlEventReviewer.SelectedValue);
                            int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                            bool FlgCheck = false;
                            FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                            if (FlgCheck == true)
                            {
                                continue;
                            }

                            string eventname = EventManagement.GetEventName(EventID);

                            eventlist.Add(eventname);

                            EventInstance inst = new EventInstance();
                            inst.EventID = EventID;
                            inst.CustomerBranchID = branchID;
                            inst.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            inst.IsDeleted = false;
                            inst.StartDate = DateTime.Now;
                            inst.CreatedOn = DateTime.Now;
                            eventInstanceOwner.Add(inst);
                            Business.EventManagement.AddEventInstance(eventInstanceOwner);

                            EventAssignment assig = new EventAssignment();
                            assig.EventInstanceID = inst.ID;
                            assig.UserID = userID;
                            assig.Role = RoleManagement.GetByCode("EO").ID;
                            assig.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            assig.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            assig.UpdatedOn = DateTime.Now;
                            assig.IsDeleted = false;
                            assig.CreatedOn = DateTime.Now;
                            eventAssignmentOwner.Add(assig);

                            Business.EventManagement.AddEventAssignment(eventAssignmentOwner, branchID);

                        }

                        if (eventlist.Count > 0)
                        {
                            //int customerID = -1;
                            customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0;
                            string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                            var user = UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue));
                            string accessURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                accessURL = Urloutput.URL;
                            }
                            else
                            {
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            var EscalationEmail = Properties.Settings.Default.EMailTemplate_AssignedEvents
                                                         .Replace("@User", user.FirstName + ' ' + user.LastName)
                                                         .Replace("@URL", Convert.ToString(accessURL))
                                                         .Replace("@From", ReplyEmailAddressName);

                            //var EscalationEmail = Properties.Settings.Default.EMailTemplate_AssignedEvents
                            //                               .Replace("@User", user.FirstName + ' ' + user.LastName)
                            //                               .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                               .Replace("@From", ReplyEmailAddressName);
                            StringBuilder details = new StringBuilder();

                            for (int i = 0; i < eventlist.Count; i++)
                            {
                                details.AppendLine(Properties.Settings.Default.EMailTemplate_AssignedEventsRows
                                    .Replace("@Branch", tbxBranch.Text)
                                    .Replace("@eventName", eventlist[i].ToString()));

                            }
                            string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Reminder: List of 'ASSIGNED' events.", message);

                            var StatutoryChecklist = EventManagement.GetStatutoryChecklistCompliance(EventClassification, EventList, branchID1);

                            if(StatutoryChecklist.Count >0)
                            {
                                EventManagement.ChengeFlagGenerateSchedule(StatutoryChecklist, branchID1);
                            }
                        }

                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEventDialog\").dialog('close');", true);
                        BindEventInstances(10, customerID);
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please assign industry to selected location before event assignment";
                }
            }
            catch (Exception ex)
            {
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnDvNotAssignedClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if ((!string.IsNullOrEmpty(ddlCustomerNew.SelectedValue)) && ddlCustomerNew.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('close')", true);

                lblBranch.Text = tvBranches.SelectedNode.Value.ToString();
                txtLocation.Text = tbxBranch.Text;
                BindUsers(ddlFilterPerformer, customerID);
                BindUsers(ddlFilterReviewer, customerID);
                BindApproverUsers(ddlFilterApprover);
                BindDepartment();
                upCompliance.Update();

                List<long> lstComplaince = new List<long>();
                List<long> EventList = new List<long>();
                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);

                lstComplaince = (List<long>)Session["exceptComplianceIDs"];
                EventList = (List<long>)Session["EventList"];
                int EvetID = Convert.ToInt32(ddlEvent.SelectedValue);
                int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                gvComplianceListAssign.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, lstComplaince, EventList, branchID1, EvetID);
                gvComplianceListAssign.DataBind();
                upCompliance.Update();
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divAssignedCompliances", "$(\"#divAssignedCompliances\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        List<Tuple<int, string>> _valueList = new List<Tuple<int, string>>();
        private List<Tuple<int, string>> getCheckedNodes(TreeNodeCollection _parentNodeList)
        {
            foreach (TreeNode item in _parentNodeList)
            {
                if (item.Checked)
                {
                    _valueList.Add(new Tuple<int, string>(Convert.ToInt32(item.Value), item.Text));
                }

                if (item.ChildNodes.Count > 0)
                {
                    getCheckedNodes(item.ChildNodes);
                }
            }
            return _valueList;
        }

        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }
                //Session["EventLocation"] = null;
                ViewState["CHECKED_ITEMS"] = null;
                txtactList.Text = "< Select Act >";

                ddlFilterPerformer.SelectedValue = "-1";
                ddlFilterReviewer.SelectedValue = "-1";
                ddlFilterApprover.SelectedValue = "-1";
                tbxStartDate.Text = "";

                BindAct();
                BindUsers(ddlUsers, customerID);
                BindReviewer(ddlEventReviewer);
                tbxBranch.Text = "< Select Location>";

                grdEventInstance.DataSource = null;
                grdEventInstance.DataBind();

                ddlCustomerNew.SelectedValue = "-1";
                ddlUsers.SelectedValue = "-1";
                ddlEventReviewer.SelectedValue = "-1";

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEventDialog\").dialog('open');", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                DateTime date = DateTime.Now;
                ForceCloseFilterBranchesTreeView();
                upEventinstance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventInstance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();
                grdEventInstance.PageIndex = e.NewPageIndex;
                grdEventInstance.DataBind();
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, e.NewPageIndex);
                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<Tuple<int, bool, bool>> EventList = new List<Tuple<int, bool, bool>>();

                int index = -1;
                foreach (GridViewRow gvrow in grdEventInstance.Rows)
                {
                    index = Convert.ToInt32(grdEventInstance.DataKeys[gvrow.RowIndex].Value);

                    CheckBox chkEvent = ((CheckBox)gvrow.FindControl("chkEvent"));
                    if (ViewState["CHECKED_ITEMS"] != null)
                        EventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;

                    if (chkEvent.Checked)
                    {
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                        {
                            EventList.Remove(data);
                            EventList.Add(new Tuple<int, bool, bool>(index, chkEvent.Checked, chkEvent.Checked));
                        }
                        else
                        {
                            EventList.Add(new Tuple<int, bool, bool>(index, chkEvent.Checked, chkEvent.Checked));
                        }
                    }
                    else
                    {
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                            EventList.Remove(data);
                    }
                }
                if (EventList != null && EventList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = EventList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<Tuple<int, bool, bool>> EventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;
                if (EventList != null && EventList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdEventInstance.Rows)
                    {
                        int index = Convert.ToInt32(grdEventInstance.DataKeys[gvrow.RowIndex].Value);
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                        {
                            CheckBox eventchek = (CheckBox)gvrow.FindControl("chkEvent");
                            eventchek.Checked = data.Item2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEventInstance__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int branchID = -1;
                    if (!(tbxBranch.Text.Trim().Equals("< Select Location >")))
                    {
                        branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    }
                    long eventID = Convert.ToInt64(grdEventInstance.DataKeys[e.Row.RowIndex].Values[0].ToString());
                    List<EventAssignment> AssignedEvents = EventManagement.GetAssignedEventsByEvent(eventID, branchID);

                    CheckBox chkEvent = (CheckBox)e.Row.FindControl("chkEvent");
                    if (AssignedEvents != null)
                    {

                        EventAssignment AsOwner = AssignedEvents.Where(entry => entry.Role == 10).FirstOrDefault();
                        EventAssignment AsReviewer = AssignedEvents.Where(entry => entry.Role == 11).FirstOrDefault();

                        if (AsOwner != null)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(AsOwner.UserID));
                            chkEvent.Checked = true;
                            chkEvent.Enabled = false;
                            chkEvent.ToolTip = "This Eevent is already assigned to " + user.FirstName + " " + user.LastName + ".";
                        }
                    }
                    CheckBox headerchk = (CheckBox)grdEventInstance.HeaderRow.FindControl("chkEventCheck");
                    chkEvent.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "')");

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventInstance_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("LOCATION"))
                {
                    ViewState["EventID"] = e.CommandArgument;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenLocationDialog", "$(\"#divGridRowLocation\").dialog('open');", true);
                }
                if (e.CommandName.Equals("ViewEvent"))
                {
                    int eventID = Convert.ToInt32(e.CommandArgument);

                    ViewState["EventID"] = eventID;
                    BindParentEventData(Convert.ToInt32(eventID));
                    upEvent.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divEventDetail\").dialog('open');", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                var Event = Business.ComplianceManagement.GetEventType(ParentEventID);

                int Type = Convert.ToInt32(Event.Type);

                ViewState["Type"] = Type;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;

                Type = Convert.ToInt32(ViewState["Type"]);

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetEventData(ParentEventID, type, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();
            }
        }
        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                Type = Convert.ToInt32(ViewState["Type"]);

                Label lblIntermediateEventID = (Label)e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {
                    GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                    
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
                else
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int Type = 0;
                    Type = Convert.ToInt32(ViewState["Type"]);
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(ViewState["EventID"]);
                    var Compliance = entities.SP_GetIntermediateComplianceAssignDays(parentEventID, intermediateEventID, SubEventID, Type).ToList();
                    
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }
        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);


                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }
        protected string GetFrequency(int ComplianceID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                string result = "";
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).Distinct().FirstOrDefault();

                if (data.ComplianceType == 0)
                {
                    if (data.Frequency != null)
                    {
                        result = data.Frequency == 0 ? "Monthly" :
                                             data.Frequency == 1 ? "Quarterly" :
                                             data.Frequency == 2 ? "HalfYearly" :
                                             data.Frequency == 3 ? "Annual" :
                                             data.Frequency == 4 ? "FourMonthly" :
                                             data.Frequency == 5 ? "TwoYearly" :
                                             data.Frequency == 6 ? "SevenYearly" :
                                             data.Frequency == 9 ? "Fortnight" :
                                             data.Frequency == 8 ? "Weekly" : "";
                    }
                    else
                    {
                        result = "";
                    }
                }
                else if (data.ComplianceType == 2)
                {
                    result = "TimeBased";
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "";
        }

        protected void grdEventInstance_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceCatagoryID = Convert.ToInt32(grdEventInstance.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.GetByType(0, complianceCatagoryID);
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }


                foreach (DataControlField field in grdEventInstance.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdEventInstance.Columns.IndexOf(field);
                    }
                }
                SaveCheckedValues();
                grdEventInstance.DataSource = ComplianceRoleMatrixList;
                grdEventInstance.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventInstance_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                type = EventManagement.GetCompanyType(Convert.ToInt32(tvBranches.SelectedNode.Value));

                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select Location >";
                TreeviewTextHighlightrd(Convert.ToInt32(tvBranches.SelectedNode.Value));
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                grdEventInstance.PageIndex = 0;
                BindEvents(type, CustomerBranchID, 0);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void TreeviewTextHighlightrd(int? branch)
        {
            foreach (TreeNode node in tvBranches.Nodes)
            {
                var list = GetChildren(node);
                List<TreeNode> tn = list.Where(nd => nd.Text.Contains("<div")).ToList();
                if (tn != null)
                {
                    foreach (TreeNode t in tn)
                    {
                        t.Text = Regex.Replace(t.Text, "<.*?>", string.Empty);
                    }

                }

                TreeNode td = null;
                if (branch != null)
                    td = list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault();

                if (td != null)
                    list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault().Text = "<div style=\"font-weight:bold\">" + td.Text + "</div>";
            }

        }

        private void BindLocation()
        {
            try
            {
                tvBranches.Nodes.Clear();
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
               else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerNew.SelectedValue != null && ddlCustomerNew.SelectedValue != "-1")
                        customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvBranches.Nodes.Clear();
                tbxBranch.Text = string.Empty;
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForEvent(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tbxBranch.Text = "< Select Location >";
                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationNonSecretrial()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerNew.SelectedValue != null && ddlCustomerNew.SelectedValue != "-1")
                        customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvBranches.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForNonSecretrialEvent(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tbxBranch.Text = "< Select Location >";
                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlEventReviewer_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void chkEventCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdEventInstance.HeaderRow.FindControl("chkEventCheck");
            foreach (GridViewRow row in grdEventInstance.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkEvent");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
                int branchID = -1;
                if (!(tbxBranch.Text.Trim().Equals("< Select Location >")))
                {
                    branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }
                long eventID = Convert.ToInt64(grdEventInstance.DataKeys[row.RowIndex].Values[0].ToString());
                List<EventAssignment> AssignedEvents = EventManagement.GetAssignedEventsByEvent(eventID, branchID);
                CheckBox chkEvent = (CheckBox)row.FindControl("chkEvent");
                // CheckBox chkEventReviewer = (CheckBox)e.Row.FindControl("chkEventReviewer");

                if (AssignedEvents != null)
                {

                    EventAssignment AsOwner = AssignedEvents.Where(entry => entry.Role == 10).FirstOrDefault();
                    EventAssignment AsReviewer = AssignedEvents.Where(entry => entry.Role == 11).FirstOrDefault();

                    if (AsOwner != null)
                    {
                        User user = UserManagement.GetByID(Convert.ToInt32(AsOwner.UserID));
                        chkEvent.Checked = true;
                        chkEvent.Enabled = false;
                        chkEvent.ToolTip = "This Eevent is already assigned to " + user.FirstName + " " + user.LastName + ".";
                    }
                }
            }
        }

        protected void grdNoAsignedComplinace_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdNoAsignedComplinace.PageIndex = e.NewPageIndex;
            List<Tuple<int, bool, bool>> eventList = new List<Tuple<int, bool, bool>>();
            SaveCheckedValues();
            eventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;

            List<long> EventList = new List<long>();
            for (int i = 0; i < eventList.Count; i++)
            {
                int EventID = eventList[i].Item1;
                int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                bool FlgCheck = false;
                FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                if (FlgCheck == true)
                {
                    continue;
                }
                EventList.Add(EventID);
            }

            int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
            int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

            var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);
            grdNoAsignedComplinace.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, exceptComplianceIDs, EventList, branchID1, -1);
            grdNoAsignedComplinace.DataBind();
            upNotAssignedCompliances.Update();
        }
        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void SaveComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                foreach (GridViewRow item in gvComplianceListAssign.Rows)
                {
                    string ComplianceID = (item.FindControl("lblID") as Label).Text;

                    int DepartmentID = -1;
                    if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                    {
                        if (ddlDepartment.SelectedValue != "-1")
                        {
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        }
                    }

                    CheckBox chkAssign = (CheckBox)item.FindControl("chkCompliance");
                    if (chkAssign.Checked)
                    {
                        ComplianceInstance instance = new ComplianceInstance();
                        instance.ComplianceId = Convert.ToInt64(ComplianceID);
                        instance.CustomerBranchID = Convert.ToInt32(lblBranch.Text);
                        instance.ScheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        if (DepartmentID == -1 || DepartmentID == 0)
                        {
                            instance.DepartmentID = null;
                        }
                        else
                        {
                            instance.DepartmentID = DepartmentID;
                        }

                        if (ddlFilterPerformer.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment = new ComplianceAssignment();
                            assignment.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                            assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                        }
                        if (ddlFilterReviewer.SelectedValue.ToString() != "-1")
                        {

                            ComplianceAssignment assignment1 = new ComplianceAssignment();
                            assignment1.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                            assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                        }

                        if (ddlFilterApprover.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment2 = new ComplianceAssignment();
                            assignment2.UserID = Convert.ToInt32(ddlFilterApprover.SelectedValue);
                            assignment2.RoleID = RoleManagement.GetByCode("APPR").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment2));
                        }
                    }
                }
                if (assignments.Count != 0)
                {
                    Business.ComplianceManagement.CreateInstancesEventBased(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    GetAssignCompliance(-1);
                    ddlEvent.SelectedValue = "-1";
                    upCompliance.Update();

                    if (gvComplianceListAssign.Rows.Count <= 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignedCompliances\").dialog('close');", true);
                    }
                }

                ddlFilterPerformer.SelectedValue = "-1";
                ddlFilterReviewer.SelectedValue = "-1";
                ddlFilterApprover.SelectedValue = "-1";
                tbxStartDate.Text = "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);
                GetAssignCompliance(EventID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void GetAssignCompliance(int EventID)
        {
            try
            {
                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                var EventList = (List<long>)Session["EventList"];
                int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);

                Session["exceptComplianceIDs"] = exceptComplianceIDs;
                var lstComplaince = (List<long>)Session["exceptComplianceIDs"];

                gvComplianceListAssign.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, lstComplaince, EventList, branchID1, EventID);
                gvComplianceListAssign.DataBind();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkComplianceCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)gvComplianceListAssign.HeaderRow.FindControl("chkComplianceCheck");
            foreach (GridViewRow row in gvComplianceListAssign.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkCompliance");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }

        private void BindEventClassification()
        {
            try
            {
                ddlEventClassification.DataTextField = "Name";
                ddlEventClassification.DataValueField = "ID";
                ddlEventClassification.DataSource = EventManagement.GetEventClassification();
                ddlEventClassification.DataBind();
                ddlEventClassification.Items.Insert(0, new ListItem("< Select Classification >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationClassification()
        {
            try
            {
                ddlLocationclassification.DataTextField = "Name";
                ddlLocationclassification.DataValueField = "ID";
                ddlLocationclassification.DataSource = EventManagement.GetLocationClassification();
                ddlLocationclassification.DataBind();
                ddlLocationclassification.Items.Insert(0, new ListItem("< Select Classification >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlEventClassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindEventClassification();
                tvBranches.Nodes.Clear();
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlEventClassification.SelectedValue == "1")
                    {
                        BindLocation();
                    }
                    else
                    {
                        BindLocationNonSecretrial();
                    }
                }
              else if (AuthenticationHelper.Role == "MGMT")
                {
                    if (ddlEventClassification.SelectedValue == "1")
                    {
                        BindLocation();
                    }
                    else
                    {
                        BindLocationNonSecretrial();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, 0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLocationclassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, 0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCustomerNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = -1;
            if ((!string.IsNullOrEmpty(ddlCustomerNew.SelectedValue)) && ddlCustomerNew.SelectedValue != "-1")
            {
                customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
            }
            //BindLocation();

            if (ddlEventClassification.SelectedValue == "1")
            {
                BindLocation();
            }
            else
            {
                BindLocationNonSecretrial();
            }
             

            BindAct();
            BindUsers(ddlUsers, customerID);
            BindUsers(ddlFilterPerformer, customerID);
            BindUsers(ddlFilterReviewer, customerID);
            BindApproverUsers(ddlFilterApprover);
        }
    }
}