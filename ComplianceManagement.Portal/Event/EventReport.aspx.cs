﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventReport : System.Web.UI.Page
    {
        DataTable dtCompliance = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EventWiseReport_Click(sender, e);
            }
        }

        protected void EventWiseReport_Click(object sender, EventArgs e)
        {
            BindParentEvent();
            btnUntaggedComplianceReport.CssClass = "Initial";
            btnActWiseReport.CssClass = "Initial";
            EventWiseReport.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
        }
        protected void btnActWiseReport_Click(object sender, EventArgs e)
        {
            BindAct(-1, -1, -1, "2");
            EventWiseReport.CssClass = "Initial";
            btnUntaggedComplianceReport.CssClass = "Initial";
            btnActWiseReport.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
        }

        protected void btnUntaggedComplianceReport_Click(object sender, EventArgs e)
        {
            EventWiseReport.CssClass = "Initial";
            btnActWiseReport.CssClass = "Initial";
            btnUntaggedComplianceReport.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
            chkTagged_CheckedChanged(sender, e);
        }

        protected void ddlEventName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActStateData(Convert.ToInt32(ddlEventName.SelectedValue), -1,-1, "1");

                int  ActNameID = -1;
                if(ddlActName.SelectedValue != "")
                {
                    ActNameID = Convert.ToInt32(ddlActName.SelectedValue);
                }

                int StateID = -1;
                if (ddlState.SelectedValue != "")
                {
                    StateID = Convert.ToInt32(ddlState.SelectedValue);
                }
                BindEventWiseDetail(Convert.ToInt32(ddlEventName.SelectedValue), ActNameID, StateID, "1");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlActName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int ActNameID = -1;
                if (ddlActName.SelectedValue != "")
                {
                    ActNameID = Convert.ToInt32(ddlActName.SelectedValue);
                }

                int StateID = -1;
                if (ddlState.SelectedValue != "")
                {
                    StateID = Convert.ToInt32(ddlState.SelectedValue);
                }
                BindEventWiseDetail(Convert.ToInt32(ddlEventName.SelectedValue), ActNameID, StateID, "1");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int ActNameID = -1;
                if (ddlActName.SelectedValue != "")
                {
                    ActNameID = Convert.ToInt32(ddlActName.SelectedValue);
                }

                int StateID = -1;
                if (ddlState.SelectedValue != "")
                {
                    StateID = Convert.ToInt32(ddlState.SelectedValue);
                }
                BindEventWiseDetail(Convert.ToInt32(ddlEventName.SelectedValue), ActNameID, StateID, "1");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindParentEvent()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlEventName.DataTextField = "Name";
                ddlEventName.DataValueField = "ID";
                ddlEventName.DataSource = EventManagement.BindNonSecretrialEventData();
                ddlEventName.DataBind();
                ddlEventName.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindAct(int EventID, int ActID, int StateID, string flag)
        {
            try
            {
                var data = EventManagement.BindActData(EventID, ActID, StateID, flag);

                var actdata = data.OrderBy(entry => entry.ActName).Distinct().ToList();

                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlAct.DataTextField = "ActName";
                ddlAct.DataValueField = "ActID";
                ddlAct.DataSource = actdata;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActStateData(int EventID,int ActID,int StateID,string flag)
        {
            try
            {
               var data= EventManagement.BindActData(EventID, ActID, StateID, flag);

                var actdata = data.OrderBy(entry => entry.ActName).Distinct().ToList();

                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlActName.DataTextField = "ActName";
                ddlActName.DataValueField = "ActID";
                ddlActName.DataSource = actdata;
                ddlActName.DataBind();
                ddlActName.Items.Insert(0, new ListItem("< Select >", "-1"));

                var data1 = EventManagement.BindStateData(EventID, ActID, StateID, flag);
                var statedata = data1.OrderBy(entry => entry.State).Distinct().ToList();
                
                ddlState.DataTextField = "State";
                ddlState.DataValueField = "StateID";
                ddlState.DataSource = statedata;
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
     

        private void BindState()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = EventManagement.BindNonSecretrialEventData();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("< State Filter >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEventWiseDetail(int EventID, int ActID,int StateID, string flag)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                entities.Database.CommandTimeout = 180;
                var List = entities.SP_GetEventWiseDetail(EventID, ActID, StateID, flag).ToList();
              
                grdEventWiseReport.DataSource = List;
                grdEventWiseReport.DataBind();

                lblComplianceCount.Text = Convert.ToString(List.Count());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActWiseDetail(int EventID, int ActID, int StateID, string flag)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                entities.Database.CommandTimeout = 180;
                var List = entities.SP_GetEventWiseDetail(EventID, ActID, StateID, flag).ToList();

                grdActWiseDetail.DataSource = List;
                grdActWiseDetail.DataBind();

                lblcomplianceCount1.Text = Convert.ToString(List.Count());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTaggedUntaggedComplianceDetail(int flag)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                entities.Database.CommandTimeout = 180;
                var List = entities.SP_GetTagUntaggedEventCompliance(flag).ToList();

                grdCompliance.DataSource = List;
                grdCompliance.DataBind();

                lblTaggedCompliance.Text = Convert.ToString(List.Count());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActEvent(int ActID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                entities.Database.CommandTimeout = 180;
                var List = entities.SP_GetActEventDetail(ActID).ToList();

                grdActEvent.DataSource = List;
                grdActEvent.DataBind();

                lblcomplianceCount1.Text = Convert.ToString(List.Count());
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkView.Checked == true)
                {
                    int ActNameID = -1;
                    if (ddlAct.SelectedValue != "")
                    {
                        ActNameID = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                    BindActWiseDetail(-1, ActNameID, -1, "2");
                }
                else
                {
                    int ActNameID = -1;
                    if (ddlAct.SelectedValue != "")
                    {
                        ActNameID = Convert.ToInt32(ddlAct.SelectedValue);
                        BindActEvent(ActNameID);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void chkView_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkView.Checked == true)
                {
                    pnlAll.Visible = true;
                    pnlEvent.Visible = false;
                    int ActNameID = -1;
                    if (ddlAct.SelectedValue != "")
                    {
                        ActNameID = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                    BindActWiseDetail(-1, ActNameID, -1, "2");
                }
                else
                {
                    pnlAll.Visible = false;
                    pnlEvent.Visible = true;
                    int ActNameID = -1;
                    if (ddlAct.SelectedValue != "")
                    {
                        ActNameID = Convert.ToInt32(ddlAct.SelectedValue);
                        BindActEvent(ActNameID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkTagged_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                if(chkTagged.Checked == true)
                {
                    BindTaggedUntaggedComplianceDetail(2);
                }
                else
                {
                    BindTaggedUntaggedComplianceDetail(1);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
            
        }
    }

}