﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ClosedEvents.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.ClosedEvents" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        //$(document).on("click", "#ContentPlaceHolder1_upEventInstanceList", function (event) {
        //    debugger;
        //    if (event.target.id == "") {
        //        var idvid = $(event.target).closest('div');
        //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
        //            $("#ContentPlaceHolder1_divFilterLocation").show();
        //        } else {
        //            $("#ContentPlaceHolder1_divFilterLocation").hide();
        //        }
        //    }
        //    else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#ContentPlaceHolder1_divFilterLocation").hide();
        //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
        //        $("#ContentPlaceHolder1_divFilterLocation").show();
        //    } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');
        //        $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
        //            $("#ContentPlaceHolder1_divFilterLocation").toggle("blind", null, 500, function () { });
        //        });
        //    }
        //});

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function postbackOnCheck() {
            var o = window.event.srcElement;
            if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
            { __doPostBack("", ""); }
        }

        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        function ClosemodalPopup() {
            $('#modalPopup').modal('hide');

            return true;
        }
        function ClosemodaelShortNotice() {
            $('#modalShortNotice').modal('hide');

            return true;
        }
        function openmodalPopup() {
            if (Displays() == true) {
                $('#modalPopup').modal('show');
            }
            return true;
        }

    </script>

    <style>
        .circle {
            background-color: green;
            width: 29px;
            height: 30px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 5px;
        }

        .ui-widget-header {
            border-bottom: 2px solid #dddddd;
            background: #999 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
        }

            .ui-widget-header > th {
                color: #fff;
            }

        .ui-widget-header {
            border: none;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 17px;
            height: 23px;
        }

        .Inforamative {
            color: Chocolate !important;
        }

        tr.Inforamative > td {
            color: Chocolate !important;
        }

        .Actionable {
            color: SlateBlue !important;
        }

        tr.Actionable > td {
            color: SlateBlue !important;
        }
    </style>
    <script type="text/javascript">
        function DeleteItem() {
            if (confirm("Are you sure you want to delete ...?")) {
                return true;
            }
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 40px; margin-left: 40px;">
        <div style="margin-bottom: 4px">
        </div>
        <asp:UpdatePanel ID="upEventInstanceList" runat="server" UpdateMode="Conditional" OnLoad="upEventInstanceList_Load">
            <ContentTemplate>
                <div class="panel-body">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                            <div class="col-md-2 colpadding0 entrycount">
                                <div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left; margin-left: 10px;">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" Selected="True" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                                <asp:Button ID="btnClosedEvent" OnClick="btnClosedEvent_Click" Visible="false" runat="server" Text="Button" />
                                <a id="btn_Back" visible="false" runat="server" class="btn btn-search" href="../Common/EventDashboard.aspx?type=active" style="margin-top: -50px; margin-left: 920px;">Back</a>
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="float: left; margin-right: -2%; width: 60.667% !important">
                                <asp:DropDownList runat="server" ID="ddlCheckListType" AutoPostBack="true" OnSelectedIndexChanged="ddlCheckListType_SelectedIndexChanged" class="form-control m-bot15 search-select">
                                    <asp:ListItem Text="<Select>" Value="-1" />
                                    <asp:ListItem Text="Secretrial" Value="1" />
                                    <asp:ListItem Text="Non Secretrial" Value="2" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -50%; width: 31.667% !important">
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 280px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                    CssClass="txtbox" />
                                <div style="margin-left: 14%; position: absolute; z-index: 10;" id="divFilterLocation" runat="server">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="280px" NodeStyle-ForeColor="#8e8e93"
                                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </div>
                            <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -17%; width: 13.333333% !important;">
                                <asp:TextBox runat="server" Style="padding-left: 7px; width: 300px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                            </div>

                            <div class="col-md-2 colpadding0" style="width: 17.333333%; float: right">
                                <div class="col-md-6 colpadding0">
                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" Style="margin-left: 100px !important;" runat="server" Text="Apply" />
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="DivRecordsScrum" style="float: right;">

                            <p style="color: #999; padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>

                <asp:Panel ID="Panel3" Width="100%" runat="server">
                    <asp:GridView runat="server" ID="grdEventList1" AutoGenerateColumns="false" CssClass="table" GridLines="None" OnRowCancelingEdit="grdEventList_RowCancelingEdit"
                        OnRowEditing="grdEventList_RowEditing" OnRowUpdating="grdEventList_RowUpdating"
                        OnRowCreated="grdEventList_RowCreated"
                        AllowPaging="True" PageSize="5" OnSorting="grdEventList_Sorting"
                        DataKeyNames="ID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="22px" HeaderText="Location" SortExpression="CustomerBranchName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblAssignmnetId" runat="server" Visible="false" Text='<%# Eval("EventAssignmentID") %>'></asp:Label>
                                        <asp:Label ID="lblEventScheduleOnID" runat="server" Visible="false" Text='<%# Eval("EventScheduledOnId") %>'></asp:Label>
                                        <asp:Label ID="lblEventInstanceID" runat="server" Visible="false" Text='<%# Eval("EventInstanceID") %>'></asp:Label>
                                        <asp:Label ID="lblBranch" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                        <asp:Label ID="lblEventClassificationID" runat="server" Visible="false" Text='<%# Eval("EventClassificationID") %>'></asp:Label>
                                        <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Height="22px" SortExpression="Name">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                        <asp:Label ID="lbleventName" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nature of event" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lbleventDesc" runat="server" Text='<%# Eval("EventDescription") %>' ToolTip='<%# Eval("EventDescription") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Held On" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblHeldOn" runat="server" Text='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" SortExpression="UserName">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role" SortExpression="Role" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<asp:Button runat="server"  OnClientClick="return openmodalPopup()" ID="btnChangeStatus" CssClass="btnss"  CommandName="Update" CommandArgument='<%# Eval("ID") %>' /> --%>
                                    <asp:LinkButton ID="btn_Update" OnClientClick="return openmodalPopup()" runat="server" CommandName="Update" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon_new.png" alt="Edit Event"/></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerStyle HorizontalAlign="Right" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0">
                        </div>
                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 20px">
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 883px">
                <div class="modal-content">
                    <div class="modal-header" style="width: 861px">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 863px">
                        <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode">
                                                    <div style="z-index: 10" id="divSubevent">
                                                        <asp:gridview id="gvParentGrid" runat="server" autogeneratecolumns="false"
                                                            showfooter="true" width="790px" borderwidth="0px" gridlines="None" class="tablenew" datakeynames="Type,Date"
                                                            onrowdatabound="gvParentGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                    src="../Images/plus.gif" alt="" /></a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name"  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true"/>
                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                            <%-- <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>--%>
                                                          <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" style="border-radius: 9px;border-color: #c7c7cc; text-align :center" BackColor ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                       <ItemTemplate>
                                                            <asp:Button ID="BtnAddgvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleParentEventAddButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Add" />
                                                            <asp:Button ID="BtnUpdategvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                        <asp:GridView ID="gvParentToComplianceGrid" borderwidth="0px"  gridlines="None" style="color: #666666;font-size:14px;" runat="server" Width="98%" DataKeyNames="EventType,SequenceID" onrowdatabound="gvParentToComplianceGrid_OnRowDataBound"
                                                                            AutoGenerateColumns="false" CellSpacing="5" >
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblParentStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvChildGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvChildGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub event name" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtgvChildGridDate" CssClass="StartDate" style=" text-align :center; border-radius: 9px;border-color: #c7c7cc;" Visible='<%# visibleTxtgvChildGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  Width="100px" runat="server"></asp:TextBox>
                                                                                     </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="BtnAddgvChildGrid" runat="server" class="btn btn-search" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" class="btn btn-search" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvComplianceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvComplianceGrid_OnRowDataBound"
                                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center" >
                                                                                                            <ItemTemplate>
                                                                                                            <asp:Label ID="lblStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                           </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                        <%--<RowStyle BorderStyle="Solid" BorderColor="Black" BorderWidth="1px" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- Parent -> Intermediate Event -> Sub Event -> Compliance--%>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvIntermediateGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvIntermediateGrid_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="IntermediateEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Intermediate Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <%--Sub Event--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" borderwidth="0px" gridlines="None" class="tablenew"  OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound"
                                                                                                         runat="server" Width="98%" DataKeyNames="IntermediateEventID,ParentEventID"
                                                                                                        AutoGenerateColumns="false">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                <ItemTemplate>
                                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                                            alt="" /></a>
                                                                                                                  <%--  <asp:Label ID="lblIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate> 
                                                                                                                    <asp:TextBox ID="txtgvIntermediateSubEventGridDate" style="border-radius: 9px; border-color: #c7c7cc; text-align:center;"  Visible='<%# visibleTxtgvIntermediateSubEventGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="BtnAddIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleIntermediateSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="BtnUpdateIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td colspan="100%">
                                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound"
                                                                                                                                    AutoGenerateColumns="false">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                        <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                                                                        <ItemTemplate>
                                                                                                                                         <asp:Label ID="lblIntermediateStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                                                         </ItemTemplate>
                                                                                                                                       </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                    <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                                                </asp:GridView>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                            </asp:gridview>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 400px;">
                                                <div style="margin-bottom: 7px;" runat="server" id="div1111">
                                                    <label style="width: 390px; display: block; float: left; font-size: 16px; color: #333;">
                                                        Nature of event
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 400px;">
                                                <asp:TextBox ID="txtDescription" class="form-control" Rows="3" Width="785px" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <div class="clearfix"></div>
                                        <tr>
                                            <td>
                                                <table width="100%" id="eventnaturelegend" style="margin-top: 10px">
                                                    <tr>
                                                        <td>
                                                            <label style="width: 50px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Note :
                                                            </label>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div class="circle" style="float: left"></div>
                                                            <div style="float: left; color: #666666; padding: 9px; font-family: 'Roboto',sans-serif;">
                                                                Completed / Closed compliance
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px; color: #666666;">
                                                            <div style="height: 30px; width: 81px; border-radius: 14px; background-color: lightgreen; float: left; margin-right: 5px; padding: 9px;">DD-MM-YYYY </div>
                                                            <div style="float: left; padding: 9px;">
                                                                Completed steps
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
                                                            <div style="float: left; color: #666666; padding: 9px;">
                                                                Informative task no action required.
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:HiddenField ID="hdnTitle" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--  </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">


            $(document).on("click", "div#ContentPlaceHolder1_upEventInstanceList", function (event) {

                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                        $("#ContentPlaceHolder1_divFilterLocation").show();
                    } else {
                        $("#ContentPlaceHolder1_divFilterLocation").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#ContentPlaceHolder1_divFilterLocation").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                    $("#ContentPlaceHolder1_divFilterLocation").show();
                } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                    $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                        $("#ContentPlaceHolder1_divFilterLocation").toggle("blind", null, 500, function () { });
                    });
                }
            });


            $(document).ready(function () {
                fhead('Closed Events');
            });
        </script>
</asp:Content>
