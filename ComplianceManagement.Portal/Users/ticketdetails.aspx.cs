﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class ticketdetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    tbxFilter.Text = Convert.ToString(Request.QueryString["id"]);
                    Submit(null, null);
                }
            }

        }
        protected void Submit(object sender, EventArgs e)
        {
            try
            {
                Business.Data.User usr = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //System.Net.WebClient wcClient = new System.Net.WebClient();
                string strJson = System.Configuration.ConfigurationManager.AppSettings["ticketUrl"] + "get_status_api.php?ticket_number=" + tbxFilter.Text + "&apikey=" + System.Configuration.ConfigurationManager.AppSettings["ticketApi"] + "&email=" + usr.Email;
                //string response = wcClient.UploadString(System.Configuration.ConfigurationManager.AppSettings["ticketUrl"], "GET", strJson);
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strJson);

                request.Method = "GET";
                using (System.Net.WebResponse response = request.GetResponse())
                {
                    using (System.IO.Stream stream = response.GetResponseStream())
                    {
                        System.IO.StreamReader responseReader = new System.IO.StreamReader(stream);
                        string json = responseReader.ReadToEnd();
                        RootObjectTicket ticket = new RootObjectTicket();
                        ticket = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObjectTicket>(json);
                        txtDeptName.Text = ticket.dept_name;
                        txtTicDate.Text = ticket.created;
                        txtTicketID.Text = tbxFilter.Text;
                        txtTopic.Text = ticket.topic;
                        txtStatus.Text = ticket.status;
                        txtPriority.Text = ticket.priority;
                        // lblBody.Text = ticket.thread.        
                        table1.Visible = true;
                        for (int i=0;i<ticket.thread.Count;i++)
                        {
                            if (ticket.thread.Count - 1 == i)
                            {
                                lblBody.Text += "<div style='float:left;margin:10px; width:100%'>"+"Poster :"+  ticket.thread[i].poster + "</br>"+"Body :" + ticket.thread[i].body + "</br>"+"Created Date :" + ticket.thread[i].created + "</div><div style='float:left;height:5px;width:100%;'></div>";
                                
                            }
                            else
                            {
                                lblBody.Text += "<div style='float:left;margin:10px; width:100%'>" + "Poster :" + ticket.thread[i].poster + "</br>"+ "Body :" + ticket.thread[i].body + "</br>" + "Created Date :" + ticket.thread[i].created + "</div><div style='float:left;height:5px;width:100%;    border-top: 1px #e1e5e9 solid;'></div>";
                            }
                        }
                    }
                }
                //System.IO.StreamWriter requestWriter = new System.IO.StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);

                //requestWriter.Close();

                //try
                //{
                //    System.Net.WebResponse webResponse = request.GetResponse();
                //    System.IO.Stream webStream = webResponse.GetResponseStream();
                //    System.IO.StreamReader responseReader = new System.IO.StreamReader(webStream);
                //    string response = responseReader.ReadToEnd();
                //    Console.Out.WriteLine(response);
                //    responseReader.Close();
                //}
                //catch (Exception e1)
                //{
                //}
            }
            catch (Exception ex)
            {

            }
        }


    }

    public class ThreadTicket
    {
        public string body { get; set; }
        public string poster { get; set; }
        public string created { get; set; }
    }

    public class EventTicket
    {
        public string state { get; set; }
        public string data { get; set; }
        public string username { get; set; }
        public string timestamp { get; set; }
    }

    public class RootObjectTicket
    {
        public string status { get; set; }
        public string dept_name { get; set; }
        public string topic { get; set; }
        public string priority { get; set; }
        public string created { get; set; }
        public object duedate { get; set; }
        public List<ThreadTicket> thread { get; set; }
        public List<EventTicket> events { get; set; }
    }
}