﻿<%@ Page Title="News letters" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="NewsLetterList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.NewsLetterList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                            <asp:ListItem Text="5" Selected="True" />
                            <asp:ListItem Text="10" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                            </asp:DropDownList>                    
                                <%--<p style="color: #999; margin-top: 10px;">Entries</p>--%>
                            </div>
                            <div style="margin-bottom: 4px">          
                            <asp:GridView runat="server" ID="grdNewsLetterList" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true"
                            GridLines="none" CssClass="table" BorderWidth="0px" CellPadding="4"  OnPageIndexChanging="grdNewsLetterList_PageIndexChanging"
                            Width="100%" Font-Size="12px" DataKeyNames="ID">            
                            <columns>
                               <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                                <asp:BoundField DataField="Title" HeaderText="Title" />                                       
                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src="../NewsLetterImages/<%#Eval("Filename")%>" style="height:35px;width:70px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="30" HeaderText = "View">
                                    <ItemTemplate>
                                        <a class="view-pdf" data-toggle="modal" onclick="viewpdf(this);settracknew('Dashboard','Newsletter','Detailedview','');"   style="cursor:pointer" data-href="../NewsLetterImages/<%#Eval("Docfilename")%>" data-title="<%#Eval("Title")%>"><img src="../Images/view-icon-new.png" alt="View Details" title="View Detail"/></a>
                                    </ItemTemplate>    
                                </asp:TemplateField>                             
                            </columns>    
                            <RowStyle CssClass="clsROWgrid"   />
                            <HeaderStyle CssClass="clsheadergrid"    />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>         
                            </asp:GridView>  
                            </div>   
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                     </div>
                                 </div>
                                 <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                        <div class="table-paging-text">
                                            <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                 </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>     
    </asp:UpdatePanel>

      <script type="text/javascript">
/*
* This is the plugin
*/
          (function (a) { a.createModal = function (b) { defaults = { title: "", message: "Your Message Goes Here!", closeButton: true, scrollable: false }; var b = a.extend({}, defaults, b); var c = (b.scrollable === true) ? 'style="max-height: 420px;overflow-y: auto;"' : ""; html = '<div class="modal fade" id="myModal">'; html += '<div class="modal-dialog" style="width: 1000px;">'; html += '<div class="modal-content" style="width: 1100px;">'; html += '<div class="modal-header">'; html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'; if (b.title.length > 0) { html += '<h4 class="modal-title">' + b.title + "</h4>" } html += "</div>"; html += '<div class="modal-body" ' + c + ">"; html += b.message; html += "</div>"; html += '<div class="modal-footer">'; if (b.closeButton === true) { html += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>' } html += "</div>"; html += "</div>"; html += "</div>"; html += "</div>"; a("body").prepend(html); a("#myModal").modal().on("hidden.bs.modal", function () { a(this).remove() }) } })(jQuery);
    
/*
* Here is how you use it
*/
 
function viewpdf(obj)  {
var pdf_link = $(obj).attr('data-href');
//alert(pdf_link.indexOf('.png'));
var pdf_title = $(obj).attr('data-title');
if (pdf_link.indexOf('.png') >-1){
var iframe = '<div class="iframe-container"><iframe src="' + pdf_link + '" width="100%" height="500" border="0"></iframe></div>'
}else{
  //  var iframe = '<object data="../AvacomDocuments/Infrastructure.docx" type="application/winword"><embed src="../AvacomDocuments/Infrastructure.docx" /></object>'
   var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
}
$.createModal({
title: pdf_title,
message: iframe,
closeButton:true,
scrollable: false
});
return false;        
} 

        $(document).ready(function () {
        
            fhead('News Letters');
        });
	 </script>
    <script type="text/javascript">
        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {
            debugger;
         try {
             _paq.push(['trackEvent', e, t, n])
         } catch (t) { } return !0
     }
</script>
</asp:Content>
