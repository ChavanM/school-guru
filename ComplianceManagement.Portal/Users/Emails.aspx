﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="Emails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.Emails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                    <br />
                    <br />
                    <%--<p style="color: #999; margin-top: 10px; margin-left: 5px;">Entries</p>--%>
                </div>
        <div style="margin-bottom: 4px">          
        <asp:GridView runat="server" ID="grdEmail" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true"
            CssClass="table" GridLines="Horizontal" BorderWidth="0px" CellPadding="4"  OnPageIndexChanging="grdEmail_PageIndexChanging"
            Width="100%" Font-Size="12px" DataKeyNames="Id">            
                <columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                <asp:BoundField DataField="Email" HeaderText="From" ItemStyle-Height="20px" HeaderStyle-Height="20px" />
                <asp:BoundField DataField="Recepient" HeaderText="Recepient" />
                <asp:BoundField DataField="Subjecttext" HeaderText="Subject" />
                <asp:BoundField DataField="MessageBody" HeaderText="Message" />
                <asp:TemplateField HeaderText = "View" ItemStyle-Width="30">
            <ItemTemplate>
                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "~/MailDetails.aspx?Id={0}") %>'
                    Text="View" />
            </ItemTemplate>
        </asp:TemplateField>
                    </columns>    
             <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>         
            </asp:GridView>  
                    </div>   
                    <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 461px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>

                        </div>
                </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnDownload" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="ddlType" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
