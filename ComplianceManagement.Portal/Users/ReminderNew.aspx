﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReminderNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.ReminderNew" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(function () {
            $('#divUserRemindersDialog').dialog({
                height: 450,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "User Reminder",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtRemindOn]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                defaultDate: startDate,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function initializeCombobox() {
            $("#<%= ddlBranch.ClientID %>").combobox();
            $("#<%= ddlCompliance.ClientID %>").combobox();
            $("#<%= ddlRole.ClientID %>").combobox();
        }

        function openModal() {
            $('#Newaddremider').modal('show');
            return true;
        }

        function CloseWin() {
            $('#Newaddremider').modal('hide');
        };

        function caller() {
            setInterval(CloseWin, 3000);
        };

        function CloseMe() {
            window.parent.CloseMyReminderPopup();
        }
    </script>
</head>
<body style="background: none !important;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="upUserReminders" runat="server" UpdateMode="Conditional" OnLoad="upUserReminders_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-success" ValidationGroup="UserReminderValidationGroup" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-danger" ValidationGroup="NewUserReminderValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="UserReminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Compliance Type</label>
                            <asp:DropDownListChosen runat="server" ID="ddlComType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                class="form-control" Width="400px" DataValueField="ID" DataTextField="Name"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlComType_SelectedIndexChanged">
                                <asp:ListItem Text="Statutory" Value="-1" />
                                <asp:ListItem Text="Internal" Value="0" />
                            </asp:DropDownListChosen>
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Location</label>
                            <asp:DropDownListChosen runat="server" ID="ddlBranch" AllowSingleDeselect="false" DisableSearchThreshold="10"
                                class="form-control" Width="400px" DataValueField="ID" DataTextField="Name"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                            <asp:CompareValidator ErrorMessage="Please select Location." ControlToValidate="ddlBranch"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="NewUserReminderValidationGroup"
                                Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Compliance</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCompliance" AllowSingleDeselect="false" DisableSearchThreshold="10"
                                class="form-control" Width="400px" DataValueField="ID" DataTextField="Name"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Compliance."
                                ControlToValidate="ddlCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="NewUserReminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Role</label>
                            <asp:DropDownListChosen runat="server" ID="ddlRole" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                class="form-control" Width="400px" DataValueField="ID" DataTextField="Name">
                            </asp:DropDownListChosen>

                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="NewUserReminderValidationGroup"
                                Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; height: 20px; color: #666; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Due Date</label>
                            <asp:Literal ID="litScheduledOn" runat="server" />
                        </div>

                        <div style="margin-bottom: 7px; clear: both; margin-left: 20px;">
                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 14px; color: #666;">
                                Remind Me On</label>
                            <asp:TextBox runat="server" ID="tbxDate" Style="padding: 0px; margin: 0px; height: 28px; width: 150px; border-radius: 3px; border-color: #c7c7cc; color: #666;" />
                            <asp:RequiredFieldValidator ErrorMessage="Please select Remind Me On date." ControlToValidate="tbxDate"
                                runat="server" ValidationGroup="NewUserReminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 30px; margin-left: 200px; height: 30px; margin-top: 15px; margin-bottom: 10px;">
                            <div style="float: left">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;"
                                    ValidationGroup="NewUserReminderValidationGroup" />
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <%--    <button type="button" class="btn-search" data-dismiss="modal" style="border: 0px; width: 100px; height: 25px;">Close</button>--%>
                                <asp:Button Text="Close" runat="server" ID="btnClose" OnClientClick="CloseMe();" CssClass="btn-search" Style="border: 0px; width: 70px; height: 30px; border-radius: 3px;" OnClick="btnClose_Click" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>