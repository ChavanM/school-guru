﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class VerticalMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                BindVerticalData(customerID);
                bindPageNumber();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            BindVerticalData(customerID);
        }

        private void BindVerticalData(int customerID)
        {
            try
            {
                var Verti = UserManagementRisk.GetAllVertical(customerID);
                grdAuditor.DataSource = Verti;
                Session["TotalRows"] = Verti.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                //Reload the Grid
                BindVerticalData(customerID);
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                mst_Vertical objVerti = new mst_Vertical()
                {
                    VerticalName = txtFName.Text.Trim(),
                    CustomerID = customerID
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (UserManagementRisk.VerticalNameExist(objVerti))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Vertical with same name already exists.";
                    }
                    else
                    {
                        UserManagementRisk.CreateVerticalName(objVerti);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Vertical Name Save Successfully.";
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    objVerti.ID = Convert.ToInt32(ViewState["VertiID"]);
                    int verticalId = UserManagementRisk.GetVerticalIDByName(objVerti.VerticalName, customerID);
                    if (objVerti.ID != verticalId)
                    {
                        if (verticalId == 0)
                        {
                            UserManagementRisk.UpdateVerticalName(objVerti);
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Vertical Name Updated Successfully.";
                        }
                        else
                        {
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Vertical with same name already exists.";
                        }
                    }
                    else
                    {
                        UserManagementRisk.UpdateVerticalName(objVerti);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Vertical Name Updated Successfully.";
                    }
                }
                BindVerticalData(customerID);
                GetPageDisplaySummary();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "fclosepopup();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("Edit_Vertical"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["VertiID"] = ID;

                    mst_Vertical objVertical = UserManagementRisk.GetVerticalListByID(ID);

                    if (objVertical != null)
                        txtFName.Text = objVertical.VerticalName;

                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Vertical"))
                {
                    UserManagementRisk.DeleteVertical(ID);

                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    BindVerticalData(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
        }
    }
}