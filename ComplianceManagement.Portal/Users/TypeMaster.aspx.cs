﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class TypeMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCityData();
                // GetPageDisplaySummary();
            }
        }

        private void BindCityData()
        {
            try
            {
                var TypeList = CustomerBranchManagement.GetallType();
                grdAuditor.DataSource = TypeList;
                Session["TotalRows"] = TypeList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CompanyType objType = new CompanyType()
                {
                    Name = txtFName.Text
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    objType.ID = Convert.ToInt32(ViewState["TypeID"]);//Need to change DeptID
                }

                if ((int)ViewState["Mode"] == 0)
                {

                    if (CustomerBranchManagement.TypeNameExist(objType.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "City Already Exists";
                    }
                    CustomerBranchManagement.CreateType(objType);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "City Saved Successfully";
                    txtFName.Text = string.Empty;
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerBranchManagement.UpdateType(objType);
                    cvDuplicateLocation.ErrorMessage = "City Updated Successfully";
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorDialog\").dialog('close')", true);
                BindCityData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindCityData();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Type"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["TypeID"] = ID;
                    CompanyType objCity = CustomerBranchManagement.GetTypeByID(ID);                    
                    txtFName.Text = objCity.Name;
                    upPromotor.Update();                    

                }
                else if (e.CommandName.Equals("DELETE_Type"))
                {
                    CustomerBranchManagement.DeleteType(ID);
                    BindCityData();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid                           
                BindCityData();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid                
                //BindAuditorList(branchid);
                BindCityData();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}