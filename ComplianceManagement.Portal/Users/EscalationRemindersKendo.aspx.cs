﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class EscalationRemindersKendo : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static string RoleFlag;
        protected static string Path;
        protected static int ActID;
        protected static string Flag;
        protected static string RoleKey;
        protected static int type;
        protected static int risk;
        protected static string loc;
        protected static int UserRoleID;
        protected static int RoleID;
        protected static int PerformerFlagID;
        protected static int ReviewerFlagID;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

                PerformerFlagID = 0;
                ReviewerFlagID = 0;
                roles = Session["User_comp_Roles"] as List<int>;//get role for Performer and Reviewer
                if (roles.Contains(3))
                    PerformerFlagID = 1;
                if (roles.Contains(4))
                    ReviewerFlagID = 1;


                string InputRole = Request.QueryString["role"];//Perfomer,Reviewer
                if (!string.IsNullOrEmpty(InputRole))
                {
                    if (InputRole.Equals("Performer"))
                        UserRoleID = 3;
                    if (InputRole.Equals("Reviewer"))
                        UserRoleID = 4;
                }
                else
                {
                    if (roles.Contains(3))
                        UserRoleID = 3;
                    else if (roles.Contains(4))
                        UserRoleID = 4;
                }

                if (!IsPostBack)
                {
                    if (AuthenticationHelper.Role == "EXCT")
                    {
                        RoleFlag = "PRA";
                        var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);
                        if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                        {
                            if ((bool)UserDetails.IsHead)
                            {
                                RoleFlag = "DEPT";
                            }
                        }
                    }
                    else
                    {
                        RoleFlag = AuthenticationHelper.Role;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}