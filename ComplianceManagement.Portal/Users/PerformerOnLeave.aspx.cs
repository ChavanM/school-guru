﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class PerformerOnLeave : System.Web.UI.Page
    {
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                    
                    if (Session["User_comp_Roles"] != null)
                    {
                        roles = Session["User_comp_Roles"] as List<int>;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["User_comp_Roles"] = roles;
                    }
                    BindNewUserList(AuthenticationHelper.CustomerID, ddlNewPerformerUsers, ddlNewReviewerUsers, ddlNewEventOwnerUsers, roles, ddlNewPerformerUsersC, ddlNewReviewerUsersC, ddlNewEventOwnerUsersC,ddlNewPerformerUsersassign,ddlNewReviewerUsersassign,ddlNewEventOwnerUsersassign);
                    FillComplianceDocuments();
                    BindActs();
                    Tab1_Click1(sender, e);
                

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            FillComplianceDocuments();
        }



        protected void Tab1_Click1(object sender, EventArgs e)
        {
            try
            {
                performerdocuments.Visible = true;                
                liNotDone.Attributes.Add("class", "active");
                liSubmitted.Attributes.Add("class", "");                
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                HDN.Value = "AL";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Tab2_Click1(object sender, EventArgs e)
        {
            try
            {
                HDN.Value = "TA";
                performerdocuments.Visible = true;                
                liNotDone.Attributes.Add("class", "");
                liSubmitted.Attributes.Add("class", "active");                
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAssignSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    long oldperformerid = -1;
                    long newperformerid = -1;
                    long oldreviewerid = -1;
                    long newreviewerid = -1;
                    long oldeventownerid = -1;
                    long neweventownerid = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (perdiv.Visible && revdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsersassign.SelectedValue))
                        {
                            if (ddlNewPerformerUsersassign.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsersassign.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsersassign.SelectedValue))
                        {
                            if (ddlNewReviewerUsersassign.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsersassign.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsersassign.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsersassign.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible && revdiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = -1;
                        neweventownerid = -1;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsersassign.SelectedValue))
                        {
                            if (ddlNewPerformerUsersassign.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsersassign.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsersassign.SelectedValue))
                        {
                            if (ddlNewReviewerUsersassign.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = -1;
                        newreviewerid = -1;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsersassign.SelectedValue))
                        {
                            if (ddlNewPerformerUsersassign.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsersassign.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsersassign.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsersassign.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (revdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsersassign.SelectedValue))
                        {
                            if (ddlNewReviewerUsersassign.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsersassign.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsersassign.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsersassign.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = -1;
                        oldeventownerid = -1;
                        newreviewerid = -1;
                        neweventownerid = -1;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsersassign.SelectedValue))
                        {
                            if (ddlNewPerformerUsersassign.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (revdiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldeventownerid = -1;
                        neweventownerid = -1;
                        oldreviewerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsersassign.SelectedValue))
                        {
                            if (ddlNewReviewerUsersassign.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsersassign.SelectedValue);
                            }
                        }
                    }
                    else if (eveDiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldreviewerid = -1;
                        newreviewerid = -1;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsersassign.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsersassign.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsersassign.SelectedValue);
                            }
                        }
                    }
                    UsersOnLeave UOL = new UsersOnLeave()
                    {
                        OldPerformerID = oldperformerid,
                        NewPerformerID = newperformerid,
                        OldReviewerID = oldreviewerid,
                        NewReviewerID = newreviewerid,
                        OldEventOwnerID = oldeventownerid,
                        NewEventOwnerID = neweventownerid,
                        CreatedOn = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                        IsActive = false,
                        CustomerID = AuthenticationHelper.CustomerID,
                        CreatedByText = AuthenticationHelper.User,
                        IsProcessed=false,
                        flag = true,
                   };
                  
                    if (!string.IsNullOrEmpty(txtStartDateassign.Text) && !string.IsNullOrEmpty(txtEndDateassign.Text))
                    {
                        if (!string.IsNullOrEmpty(txtStartDateassign.Text))
                        {
                            UOL.LStartDate = DateTime.ParseExact(Convert.ToString(txtStartDateassign.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        if (!string.IsNullOrEmpty(txtEndDateassign.Text))
                        {
                            UOL.LEndDate = DateTime.ParseExact(Convert.ToString(txtEndDateassign.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }

                        DateTime fromDatePeroid = new DateTime(UOL.LStartDate.Year, UOL.LStartDate.Month, UOL.LStartDate.Day);
                        DateTime toDatePeroid = new DateTime(UOL.LEndDate.Year, UOL.LEndDate.Month, UOL.LEndDate.Day);
                        if (fromDatePeroid <= toDatePeroid)
                        {
                            if ((int)ViewState["Mode"] == 0)
                            {
                                if (Exist(UOL))
                                {
                                    cvDuplicateassign.IsValid = false;
                                    cvDuplicateassign.ErrorMessage = "Leave period already exists. or some task assigned to you in this period " + UOL.LStartDate.ToString("dd-MMM-yyyy") + " & " + UOL.LEndDate.ToString("dd-MMM-yyyy");
                                }
                                else
                                {
                                    string portalURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        portalURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    Create(UOL);

                                    #region[save checked compliance"]                                    
                                    //if (type == "P")
                                    //{
                                        string compliancetype = string.Empty;
                                        if (rblcompliancetype.SelectedValue == "0")
                                        {
                                            compliancetype = "S";
                                        }
                                        else
                                        {
                                            compliancetype = "I";
                                        }
                                        var complianceList = new List<ComplianceAssignmentLeave>();
                                        SaveCheckedValues();
                                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAssignmentLeave>;
                                        foreach (GridViewRow gvrow in grdassignedcompliance.Rows)
                                        {
                                            var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                                            if (checkbox.Checked)
                                            {
                                                var lblcomplianceID = gvrow.FindControl("lblComplianceID") as Label;
                                                var lblComplianceInstenaceID = gvrow.FindControl("lblComplianceInstenaceID") as Label;
                                                var lblcomplianceshortdescription = gvrow.FindControl("lblShortDescription") as Label;
                                                var lblPerformer = gvrow.FindControl("lblPerformer") as Label;
                                                var lblReviewer = gvrow.FindControl("lblReviewer") as Label;

                                                String shortdesc = String.Empty;
                                                ViewState["Shortdescription"] = shortdesc;
                                                long complianceid = Convert.ToInt32(lblcomplianceID.Text);
                                                var complianceinstanceid = Convert.ToInt32(lblComplianceInstenaceID.Text);
                                                if (lblPerformer.Text == AuthenticationHelper.User)
                                                {
                                                    UserLeaveInstance ULI = new UserLeaveInstance()
                                                    {
                                                        LeaveId = UOL.ID,
                                                        ComplianceInstanceID = Convert.ToInt32(complianceinstanceid),
                                                        IsStatutoryInternal = compliancetype,
                                                        RoleId = 3,
                                                        UserID = AuthenticationHelper.UserID
                                                    };
                                                    CreateComplianceLeaveMapping(ULI);
                                                }
                                                else if (lblReviewer.Text == AuthenticationHelper.User)
                                                {
                                                    UserLeaveInstance ULI = new UserLeaveInstance()
                                                    {
                                                        LeaveId = UOL.ID,
                                                        ComplianceInstanceID = Convert.ToInt32(complianceinstanceid),
                                                        IsStatutoryInternal = compliancetype,
                                                        RoleId = 4,
                                                        UserID = AuthenticationHelper.UserID
                                                    };
                                                    CreateComplianceLeaveMapping(ULI);
                                                }
                                            }
                                        }
                                    //}
                                    #endregion

                                    //GetStartAll(UOL.ID);
                                    if (newperformerid != -1 && newreviewerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var newperuser = UserManagement.GetByID(Convert.ToInt32(newperformerid));
                                        var oldrevuser = UserManagement.GetByID(Convert.ToInt32(oldreviewerid));
                                        string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                                           .Replace("@newPerUser", newperuser.FirstName + " " + newperuser.LastName)
                                                           .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@From", ReplyEmailAddressName)
                                                           .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                        //                    .Replace("@newPerUser", newperuser.FirstName + " " + newperuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newperuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);

                                        var newrevuser = UserManagement.GetByID(Convert.ToInt32(newreviewerid));
                                        string revmessage = Settings.Default.EmailTemplate_ReviewerOnLeave
                                                           .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                                           .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@From", ReplyEmailAddressName)
                                                           .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string revmessage = Settings.Default.EmailTemplate_ReviewerOnLeave
                                        //                    .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newrevuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", revmessage);
                                    }
                                    else if (newperformerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var Newperuser = UserManagement.GetByID(Convert.ToInt32(newperformerid));
                                        var revuser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                        string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                                          .Replace("@newPerUser", Newperuser.FirstName + " " + Newperuser.LastName)
                                                          .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@From", ReplyEmailAddressName)
                                                          .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                        //                    .Replace("@newPerUser", Newperuser.FirstName + " " + Newperuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { Newperuser.Email }).ToList(), (new string[] { revuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);
                                    }
                                    else if (newreviewerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var newrevuser = UserManagement.GetByID(Convert.ToInt32(newreviewerid));
                                        var oldrevuser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                        string message = Settings.Default.EmailTemplate_ReviewerOnLeave
                                                          .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                                          .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@From", ReplyEmailAddressName)
                                                          .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_ReviewerOnLeave
                                        //                    .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newrevuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);

                                    }
                                    cvDuplicateassign.IsValid = false;
                                    cvDuplicateassign.ErrorMessage = "Leave save successfully.";
                                }
                            }
                            else if ((int)ViewState["Mode"] == 1)
                            {
                                UOL.ID = Convert.ToInt32(ViewState["LeaveID"]);
                                #region["Update checked records"]

                                // ExistsComplianceLeaveMapping(UOL.ID);
                                var complianceList = new List<ComplianceAssignmentLeave>();
                                SaveCheckedValues();
                                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAssignmentLeave>;

                                List<UserLeaveInstance> master = new List<UserLeaveInstance>();
                                string compliancetypenew = string.Empty;
                                if (rblcompliancetype.SelectedValue == "0")
                                {
                                    compliancetypenew = "S";
                                }
                                else
                                {
                                    compliancetypenew = "I";
                                }
                                foreach (GridViewRow gvrow in grdassignedcompliance.Rows)
                                {
                                    var checkbox = gvrow.FindControl("Chkselection") as CheckBox;
                                    if (checkbox.Checked)
                                    {
                                        var lblcomplianceID = gvrow.FindControl("lblComplianceID") as Label;
                                        var lblComplianceInstenaceID = gvrow.FindControl("lblComplianceInstenaceID") as Label;
                                        var lblcomplianceshortdescription = gvrow.FindControl("lblShortDescription") as Label;
                                        var lblPerformer = gvrow.FindControl("lblPerformer") as Label;
                                        var lblReviewer = gvrow.FindControl("lblReviewer") as Label;
                                        string shortdesc = string.Empty;
                                        ViewState["Shortdescription"] = shortdesc;
                                        long complianceid = Convert.ToInt32(lblcomplianceID.Text);
                                        var complianceinstanceid = Convert.ToInt32(lblComplianceInstenaceID.Text);                                        
                                        if (lblPerformer.Text == AuthenticationHelper.User)
                                        {
                                            UserLeaveInstance ULI = new UserLeaveInstance()
                                            {
                                                LeaveId = UOL.ID,
                                                ComplianceInstanceID = Convert.ToInt32(complianceinstanceid),
                                                IsStatutoryInternal = compliancetypenew,
                                                RoleId = 3,
                                                UserID = AuthenticationHelper.UserID
                                            };
                                            master.Add(ULI);
                                            //CreateComplianceLeaveMapping(ULI);
                                        }
                                        else if (lblReviewer.Text == AuthenticationHelper.User)
                                        {
                                            UserLeaveInstance ULI = new UserLeaveInstance()
                                            {
                                                LeaveId = UOL.ID,
                                                ComplianceInstanceID = Convert.ToInt32(complianceinstanceid),
                                                IsStatutoryInternal = compliancetypenew,
                                                RoleId = 4,
                                                UserID = AuthenticationHelper.UserID
                                            };
                                            master.Add(ULI);
                                            //CreateComplianceLeaveMapping(ULI);
                                        }
                                    }
                                }
                                if (master.Count>0)
                                {
                                    CreateComplianceLeaveMapping(master, UOL.ID, compliancetypenew);
                                }                                
                                #endregion
                                var objQuery = (from row in entities.UsersOnLeaves
                                                where row.ID == UOL.ID
                                                && row.IsProcessed == false
                                                select row).FirstOrDefault();
                                if (objQuery != null)
                                {
                                    objQuery.LStartDate = UOL.LStartDate;
                                    objQuery.LEndDate = UOL.LEndDate;
                                    entities.SaveChanges();
                                    cvDuplicateassign.IsValid = false;
                                    cvDuplicateassign.ErrorMessage = "Leave updated successfully.";
                                }
                                else
                                {
                                    cvDuplicateassign.IsValid = false;
                                    cvDuplicateassign.ErrorMessage = "Leave already processed.";
                                }
                            }
                            FillComplianceDocuments();
                            upDocumentDownload.Update();
                            //upDocumentDownload.Update();
                        }
                        else
                        {
                            cvDuplicateassign.IsValid = false;
                            cvDuplicateassign.ErrorMessage = "From Date should be less than To Date.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 322", MethodBase.GetCurrentMethod().Name);
                cvDuplicateassign.IsValid = false;
                cvDuplicateassign.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    long oldperformerid = -1;
                    long newperformerid = -1;
                    long oldreviewerid = -1;
                    long newreviewerid = -1;
                    long oldeventownerid = -1;
                    long neweventownerid = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (perdiv.Visible && revdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsers.SelectedValue))
                        {
                            if (ddlNewPerformerUsers.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsers.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                        {
                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsers.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsers.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsers.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible && revdiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = -1;
                        neweventownerid = -1;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsers.SelectedValue))
                        {
                            if (ddlNewPerformerUsers.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsers.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                        {
                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = -1;
                        newreviewerid = -1;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsers.SelectedValue))
                        {
                            if (ddlNewPerformerUsers.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsers.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsers.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsers.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (revdiv.Visible && eveDiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldreviewerid = AuthenticationHelper.UserID;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                        {
                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsers.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsers.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsers.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (perdiv.Visible)
                    {
                        oldperformerid = AuthenticationHelper.UserID;
                        oldreviewerid = -1;
                        oldeventownerid = -1;
                        newreviewerid = -1;
                        neweventownerid = -1;
                        if (!string.IsNullOrEmpty(ddlNewPerformerUsers.SelectedValue))
                        {
                            if (ddlNewPerformerUsers.SelectedValue != "-1")
                            {
                                newperformerid = Convert.ToInt64(ddlNewPerformerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (revdiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldeventownerid = -1;
                        neweventownerid = -1;
                        oldreviewerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                        {
                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                            {
                                newreviewerid = Convert.ToInt64(ddlNewReviewerUsers.SelectedValue);
                            }
                        }
                    }
                    else if (eveDiv.Visible)
                    {
                        oldperformerid = -1;
                        newperformerid = -1;
                        oldreviewerid = -1;
                        newreviewerid = -1;
                        oldeventownerid = AuthenticationHelper.UserID;
                        if (!string.IsNullOrEmpty(ddlNewEventOwnerUsers.SelectedValue))
                        {
                            if (ddlNewEventOwnerUsers.SelectedValue != "-1")
                            {
                                neweventownerid = Convert.ToInt64(ddlNewEventOwnerUsers.SelectedValue);
                            }
                        }
                    }
                    UsersOnLeave UOL = new UsersOnLeave()
                    {
                        OldPerformerID = oldperformerid,
                        NewPerformerID = newperformerid,
                        OldReviewerID = oldreviewerid,
                        NewReviewerID = newreviewerid,
                        OldEventOwnerID = oldeventownerid,
                        NewEventOwnerID = neweventownerid,
                        CreatedOn = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                        IsActive = false,
                        CustomerID = AuthenticationHelper.CustomerID,
                        CreatedByText = AuthenticationHelper.User,
                        flag = false,                                      
                    };                                       
                    if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
                    {
                        if (!string.IsNullOrEmpty(txtStartDate.Text))
                        {
                            UOL.LStartDate = DateTime.ParseExact(Convert.ToString(txtStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        if (!string.IsNullOrEmpty(txtEndDate.Text))
                        {
                            UOL.LEndDate = DateTime.ParseExact(Convert.ToString(txtEndDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }

                        DateTime fromDatePeroid = new DateTime(UOL.LStartDate.Year, UOL.LStartDate.Month, UOL.LStartDate.Day);
                        DateTime toDatePeroid = new DateTime(UOL.LEndDate.Year, UOL.LEndDate.Month, UOL.LEndDate.Day);
                        if (fromDatePeroid <= toDatePeroid)
                        {
                            if ((int)ViewState["Mode"] == 0)
                            {
                                if (Exist(UOL))
                                {
                                    cvDuplicate.IsValid = false;
                                    cvDuplicate.ErrorMessage = "Leave period already exists. or some task assigned to you in this period " + UOL.LStartDate.ToString("dd-MMM-yyyy") + " & " + UOL.LEndDate.ToString("dd-MMM-yyyy");
                                }
                                else
                                {
                                    Create(UOL);
                                    string portalURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        portalURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }

                                    if (newperformerid != -1 && newreviewerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var newperuser = UserManagement.GetByID(Convert.ToInt32(newperformerid));
                                        var oldrevuser = UserManagement.GetByID(Convert.ToInt32(oldreviewerid));
                                        string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                                          .Replace("@newPerUser", newperuser.FirstName + " " + newperuser.LastName)
                                                          .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@From", ReplyEmailAddressName)
                                                          .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                        //                    .Replace("@newPerUser", newperuser.FirstName + " " + newperuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newperuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);

                                        var newrevuser = UserManagement.GetByID(Convert.ToInt32(newreviewerid));
                                        string revmessage = Settings.Default.EmailTemplate_ReviewerOnLeave
                                                    .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                    .Replace("@From", ReplyEmailAddressName)
                                                    .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string revmessage = Settings.Default.EmailTemplate_ReviewerOnLeave
                                        //                    .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newrevuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", revmessage);
                                    }
                                    else if (newperformerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var Newperuser = UserManagement.GetByID(Convert.ToInt32(newperformerid));
                                        var revuser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                        string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                                           .Replace("@newPerUser", Newperuser.FirstName + " " + Newperuser.LastName)
                                                           .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                           .Replace("@From", ReplyEmailAddressName)
                                                           .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_PerformerOnLeave
                                        //                    .Replace("@newPerUser", Newperuser.FirstName + " " + Newperuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { Newperuser.Email }).ToList(), (new string[] { revuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);
                                    }
                                    else if (newreviewerid != -1)
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(customerID);
                                        var newrevuser = UserManagement.GetByID(Convert.ToInt32(newreviewerid));
                                        var oldrevuser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                        string message = Settings.Default.EmailTemplate_ReviewerOnLeave
                                                          .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                                          .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                                          .Replace("@From", ReplyEmailAddressName)
                                                          .Replace("@PortalURL", Convert.ToString(portalURL));

                                        //string message = Settings.Default.EmailTemplate_ReviewerOnLeave
                                        //                    .Replace("@newRevUser", newrevuser.FirstName + " " + newrevuser.LastName)
                                        //                    .Replace("@fromdate", UOL.LStartDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@todate", UOL.LEndDate.ToString("dd-MMM-yyyy"))
                                        //                    .Replace("@From", ReplyEmailAddressName)
                                        //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newrevuser.Email }).ToList(), (new string[] { oldrevuser.Email }).ToList(), null, "AVACOM Notification :: Compliances reassignment.", message);

                                    }
                                    cvDuplicate.IsValid = false;
                                    cvDuplicate.ErrorMessage = "Leave save successfully.";
                                }
                            }                                               
                            FillComplianceDocuments();
                            upDocumentDownload.Update();
                        }
                        else
                        {
                            cvDuplicate.IsValid = false;
                            cvDuplicate.ErrorMessage = "From Date should be less than To Date.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 322", MethodBase.GetCurrentMethod().Name);
                cvDuplicate.IsValid = false;
                cvDuplicate.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        public static UsersOnLeave GetByID(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypes = (from row in entities.UsersOnLeaves
                                where row.ID == iD
                                && row.IsActive == false
                                select row).SingleOrDefault();
                return objTypes;
            }
        }
        public static void Update(UsersOnLeave obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objQuery = (from row in entities.UsersOnLeaves
                                where row.ID == obj.ID
                                && row.IsProcessed == false
                                select row).FirstOrDefault();

                objQuery.LStartDate = obj.LStartDate;
                objQuery.LEndDate = obj.LEndDate;
                entities.SaveChanges();
            }
        }
        public static bool Exist(UsersOnLeave obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Sp_CheckLeaveExists(AuthenticationHelper.UserID, obj.LStartDate, obj.LEndDate)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static void Delete(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypes = (from row in entities.UsersOnLeaves
                                where row.ID == iD
                                && row.IsProcessed == false
                                select row).FirstOrDefault();

                objTypes.IsActive = true;
                entities.SaveChanges();
            }
        }
        public static void Create(UsersOnLeave obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.UsersOnLeaves.Add(obj);
                entities.SaveChanges();
            }
        }
        public static object GetAllUserCustomerID(int customerID, string roleCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> s = new List<int>();
                s.Add(2);
                s.Add(8);
                s.Add(7);
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && s.Contains(row.RoleID)
                             && row.ID != AuthenticationHelper.UserID
                             select row);
                                
                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public static object GetAllUserZomato(int customerID, int PerformerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> s = new List<int>();
                s.Add(2);
                s.Add(8);
                s.Add(7);
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && s.Contains(row.RoleID)
                             && row.ID != AuthenticationHelper.UserID
                             && row.ID != PerformerID
                             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        private void BindNewUserList(long customerID, DropDownList ddlPlist, DropDownList ddlRlist, DropDownList ddlElist, 
            List<int> crols, DropDownList ddlPlistC, DropDownList ddlRlistC, DropDownList ddlElistC, 
            DropDownList ddlPassignlist, DropDownList ddlRassignlist, DropDownList ddlEassignlist)
        {
            try
            {

                var AllUsers = GetAllUserCustomerID((int)customerID, null);
                ddlPlist.DataTextField = "Name";
                ddlPlist.DataValueField = "ID";
                ddlPlist.DataSource = AllUsers;
                ddlPlist.DataBind();
                ddlPlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));

                //ddlRlist.DataTextField = "Name";
                //ddlRlist.DataValueField = "ID";
                //ddlRlist.DataSource = AllUsers;
                //ddlRlist.DataBind();
                //ddlRlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));

                ddlElist.DataTextField = "Name";
                ddlElist.DataValueField = "ID";
                ddlElist.DataSource = AllUsers;
                ddlElist.DataBind();
                ddlElist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));


                ddlPlistC.DataTextField = "Name";
                ddlPlistC.DataValueField = "ID";
                ddlPlistC.DataSource = AllUsers;
                ddlPlistC.DataBind();
                ddlPlistC.Items.Insert(0, new ListItem("Select User to Assign", "-1"));


                ddlRlistC.DataTextField = "Name";
                ddlRlistC.DataValueField = "ID";
                ddlRlistC.DataSource = AllUsers;
                ddlRlistC.DataBind();
                ddlRlistC.Items.Insert(0, new ListItem("Select User to Assign", "-1"));


                ddlElistC.DataTextField = "Name";
                ddlElistC.DataValueField = "ID";
                ddlElistC.DataSource = AllUsers;
                ddlElistC.DataBind();
                ddlElistC.Items.Insert(0, new ListItem("Select User to Assign", "-1"));

                ddlPassignlist.DataTextField = "Name";
                ddlPassignlist.DataValueField = "ID";
                ddlPassignlist.DataSource = AllUsers;
                ddlPassignlist.DataBind();
                ddlPassignlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));


                ddlRassignlist.DataTextField = "Name";
                ddlRassignlist.DataValueField = "ID";
                ddlRassignlist.DataSource = AllUsers;
                ddlRassignlist.DataBind();
                ddlRassignlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));


                ddlEassignlist.DataTextField = "Name";
                ddlEassignlist.DataValueField = "ID";
                ddlEassignlist.DataSource = AllUsers;
                ddlEassignlist.DataBind();
                ddlEassignlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));

                if (crols.Contains(3) && crols.Contains(4) && crols.Contains(10))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = true;
                    eveDiv.Visible = true;

                    perdivC.Visible = true;
                    revdivC.Visible = true;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = true;
                    
                }
                else if (crols.Contains(3) && crols.Contains(10))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = false;
                    eveDiv.Visible = true;

                    perdivC.Visible = true;
                    revdivC.Visible = false;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = true;
                }
                else if (crols.Contains(4) && crols.Contains(10))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = true;
                    eveDiv.Visible = true;

                    perdivC.Visible = false;
                    revdivC.Visible = true;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = false;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = true;
                }
                else if (crols.Contains(3) && crols.Contains(4))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = true;
                    eveDiv.Visible = false;

                    perdivC.Visible = true;
                    revdivC.Visible = true;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(3))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = false;
                    eveDiv.Visible = false;

                    perdivC.Visible = true;
                    revdivC.Visible = false;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(4))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = true;
                    eveDiv.Visible = false;

                    perdivC.Visible = false;
                    revdivC.Visible = true;
                    eveDivC.Visible = false;


                    perdivAssign.Visible = false;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(10))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = false;
                    eveDiv.Visible = true;

                    perdivC.Visible = false;
                    revdivC.Visible = false;
                    eveDivC.Visible = true;


                    perdivAssign.Visible = false;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = true;
                }
                else
                {
                    perdiv.Visible = false;
                    revdiv.Visible = false;
                    eveDiv.Visible = false;


                    perdivC.Visible = false;
                    revdivC.Visible = false;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = false;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindReviewerList(long customerID, DropDownList ddlRlist,List<int> crols,int PerformerID)
        {
            try
            {

                if (customerID == 914) // Zomato Customer
                {
                    var AllUsers = GetAllUserZomato((int)customerID, PerformerID);

                    ddlRlist.DataTextField = "Name";
                    ddlRlist.DataValueField = "ID";
                    ddlRlist.DataSource = AllUsers;
                    ddlRlist.DataBind();
                    ddlRlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));

                }
                else
                {
                    var AllUsers = GetAllUserCustomerID((int)customerID, null);

                    ddlRlist.DataTextField = "Name";
                    ddlRlist.DataValueField = "ID";
                    ddlRlist.DataSource = AllUsers;
                    ddlRlist.DataBind();
                    ddlRlist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));
                }

                if (crols.Contains(3) && crols.Contains(4) && crols.Contains(10))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = true;
                    eveDiv.Visible = true;

                    perdivC.Visible = true;
                    revdivC.Visible = true;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = true;

                }
                else if (crols.Contains(3) && crols.Contains(10))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = false;
                    eveDiv.Visible = true;

                    perdivC.Visible = true;
                    revdivC.Visible = false;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = true;
                }
                else if (crols.Contains(4) && crols.Contains(10))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = true;
                    eveDiv.Visible = true;

                    perdivC.Visible = false;
                    revdivC.Visible = true;
                    eveDivC.Visible = true;

                    perdivAssign.Visible = false;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = true;
                }
                else if (crols.Contains(3) && crols.Contains(4))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = true;
                    eveDiv.Visible = false;

                    perdivC.Visible = true;
                    revdivC.Visible = true;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(3))
                {
                    perdiv.Visible = true;
                    revdiv.Visible = false;
                    eveDiv.Visible = false;

                    perdivC.Visible = true;
                    revdivC.Visible = false;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = true;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(4))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = true;
                    eveDiv.Visible = false;

                    perdivC.Visible = false;
                    revdivC.Visible = true;
                    eveDivC.Visible = false;


                    perdivAssign.Visible = false;
                    revdivAssign.Visible = true;
                    eveDivAssign.Visible = false;
                }
                else if (crols.Contains(10))
                {
                    perdiv.Visible = false;
                    revdiv.Visible = false;
                    eveDiv.Visible = true;

                    perdivC.Visible = false;
                    revdivC.Visible = false;
                    eveDivC.Visible = true;


                    perdivAssign.Visible = false;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = true;
                }
                else
                {
                    perdiv.Visible = false;
                    revdiv.Visible = false;
                    eveDiv.Visible = false;


                    perdivC.Visible = false;
                    revdivC.Visible = false;
                    eveDivC.Visible = false;

                    perdivAssign.Visible = false;
                    revdivAssign.Visible = false;
                    eveDivAssign.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
                ddlNewPerformerUsers.ClearSelection();
                ddlNewReviewerUsers.ClearSelection();
                ddlNewEventOwnerUsers.ClearSelection();

                txtStartDate.Enabled = true;
                txtEndDate.Enabled = true;
                ddlNewPerformerUsers.Enabled = true;
                ddlNewReviewerUsers.Enabled = true;
                ddlNewEventOwnerUsers.Enabled = true;

                txtStartDateassign.Text = string.Empty;
                txtEndDateassign.Text = string.Empty;
                ddlNewPerformerUsersassign.ClearSelection();
                ddlNewReviewerUsersassign.ClearSelection();
                ddlNewEventOwnerUsersassign.ClearSelection();
                txtFilter.Text = string.Empty;

                txtStartDateassign.Enabled = true;
                txtEndDateassign.Enabled = true;
                ddlNewPerformerUsersassign.Enabled = true;
                ddlNewReviewerUsersassign.Enabled = true;
                ddlNewEventOwnerUsersassign.Enabled = true;

                foreach (GridViewRow gvrow in grdassignedcompliance.Rows)
                {
                    CheckBox chk = (CheckBox)(gvrow.FindControl("Chkselection"));
                    if (chk.Checked)
                    {
                        chk.Checked = false;
                    }
                }


                if (HDN.Value =="AL")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "openleavepopup();", true);
                }
                else 
                {
                    Panel1.Visible = true;
                    bindAssignedcompliance();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "openleaveAssignpopup();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 285", MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdLeave_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("Edit_Leave"))
                {                    
                    txtStartDate.Enabled = true;
                    txtEndDate.Enabled = true;
                    ddlNewPerformerUsers.Enabled = true;
                    ddlNewReviewerUsers.Enabled = true;
                    ddlNewEventOwnerUsers.Enabled = true;
                    txtStartDateassign.Enabled = true;
                    txtEndDateassign.Enabled = true;
                    ddlNewPerformerUsersassign.Enabled = true;
                    ddlNewReviewerUsersassign.Enabled = true;
                    ddlNewEventOwnerUsersassign.Enabled = true;
                    ViewState["Mode"] = 1;
                    ViewState["LeaveID"] = ID;

                    //var cominstanceid = GetComInstanceID(ID);
                    //int cinstanceid = Convert.ToInt32(cominstanceid);
                    //var newobj = GetByInstanceID(ID, cinstanceid);

                    var obj = GetByID(ID);
                    if (obj != null)
                    {
                        if (obj.flag == false)
                        {
                            txtStartDate.Text = obj.LStartDate != null ? obj.LStartDate.ToString("dd-MM-yyyy") : " ";
                            txtEndDate.Text = obj.LEndDate != null ? obj.LEndDate.ToString("dd-MM-yyyy") : " ";
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewPerformerID)))
                            {
                                if (obj.NewPerformerID != -1)
                                {
                                    ddlNewPerformerUsers.SelectedValue = Convert.ToString(obj.NewPerformerID);
                                }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewReviewerID)))
                            {
                                if (obj.NewReviewerID != -1)
                                {
                                    ddlNewReviewerUsers.SelectedValue = Convert.ToString(obj.NewReviewerID);
                                }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewEventOwnerID)))
                            {
                                if (obj.NewEventOwnerID != -1)
                                {
                                    ddlNewEventOwnerUsers.SelectedValue = Convert.ToString(obj.NewEventOwnerID);
                                }
                            }

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "openleavepopup();", true);
                        }
                        else if (obj.flag == true)
                        {
                            txtStartDateassign.Text = obj.LStartDate != null ? obj.LStartDate.ToString("dd-MM-yyyy") : " ";
                            txtEndDateassign.Text = obj.LEndDate != null ? obj.LEndDate.ToString("dd-MM-yyyy") : " ";
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewPerformerID)))
                            {
                                if (obj.NewPerformerID != -1)
                                {
                                    ddlNewPerformerUsersassign.SelectedValue = Convert.ToString(obj.NewPerformerID);
                                }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewReviewerID)))
                            {
                                if (obj.NewReviewerID != -1)
                                {
                                    ddlNewReviewerUsersassign.SelectedValue = Convert.ToString(obj.NewReviewerID);
                                }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(obj.NewEventOwnerID)))
                            {
                                if (obj.NewEventOwnerID != -1)
                                {
                                    ddlNewEventOwnerUsersassign.SelectedValue = Convert.ToString(obj.NewEventOwnerID);
                                }
                            }
                            //if (newobj.IsStatutoryInternal == "S")
                            //{
                            //    rblcompliancetype.Items[0].Selected = true;
                            //    rblcompliancetype.Items[1].Selected = false;
                            //}
                            //else if (newobj.IsStatutoryInternal == "I")
                            //{
                            //    rblcompliancetype.Items[0].Selected = false;
                            //    rblcompliancetype.Items[1].Selected = true;
                            //}
                            Panel1.Visible = true;
                            bindAssignedcompliance();
                            bindcheckedcomplianceList(ID);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "openleaveAssignpopup();", true);
                        }
                    }
                }
                else if (e.CommandName.Equals("Cancel_Leave"))
                {                  
                    ViewState["Mode"] = 1;
                    ViewState["LeaveID"] = ID;
                    var obj = GetByID(ID);
                    if (rbtnlstleave.SelectedValue.Equals("1"))
                    {
                        txtStartDateC.Enabled = false;
                        txtEndDateC.Enabled = false;
                        ddlNewPerformerUsersC.Enabled = false;
                        ddlNewReviewerUsersC.Enabled = false;
                        ddlNewEventOwnerUsersC.Enabled = false;
                    }
                    if (obj != null)
                    {
                        txtStartDateC.Text = obj.LStartDate != null ? obj.LStartDate.ToString("dd-MM-yyyy") : " ";
                        txtEndDateC.Text = obj.LEndDate != null ? obj.LEndDate.ToString("dd-MM-yyyy") : " ";
                        if (!string.IsNullOrEmpty(Convert.ToString(obj.NewPerformerID)))
                        {
                            if (obj.NewPerformerID != -1)
                            {
                                ddlNewPerformerUsersC.SelectedValue = Convert.ToString(obj.NewPerformerID);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(obj.NewReviewerID)))
                        {
                            if (obj.NewReviewerID != -1)
                            {
                                ddlNewReviewerUsersC.SelectedValue = Convert.ToString(obj.NewReviewerID);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(obj.NewEventOwnerID)))
                        {
                            if (obj.NewEventOwnerID != -1)
                            {
                                ddlNewEventOwnerUsersC.SelectedValue = Convert.ToString(obj.NewEventOwnerID);
                            }
                        }
                    }                    
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "opencancelleavepopup();", true);
                    upPromotorC.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 560", MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static object GetComInstanceID(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypes = (from row in entities.UserLeaveInstances
                                where row.LeaveId == iD
                                select row.ComplianceInstanceID).FirstOrDefault();
                return objTypes;
            }
        }

        public static UserLeaveInstance GetByInstanceID(int iD, int cominstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypes = (from row in entities.UserLeaveInstances
                                where row.LeaveId == iD && row.ComplianceInstanceID == cominstanceid
                                select row).SingleOrDefault();
                return objTypes;
            }
        }

        #region[checkbox assign compliance]

        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = GetAllNVP(AuthenticationHelper.UserID);
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<object> GetAllNVP(long userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ComplianceAssignments
                            join row1 in entities.ComplianceInstances
                            on row.ComplianceInstanceID equals row1.ID
                            join row2 in entities.Compliances
                            on row1.ComplianceId equals row2.ID
                            join row3 in entities.Acts
                            on row2.ActID equals row3.ID
                            where row3.IsDeleted == false
                            && row.UserID == userid
                            select new { ID = row3.ID, Name = row3.Name }).Distinct().OrderBy(entry => entry.Name).ToList<object>();                
                return acts;
            }
        }
        private void SaveCheckedValues()
        {
            try
            {
                List<ComplianceAssignmentLeave> complianceList = new List<ComplianceAssignmentLeave>();
                foreach (GridViewRow gvrow in grdassignedcompliance.Rows)
                {
                    ComplianceAssignmentLeave complianceProperties = new ComplianceAssignmentLeave();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdassignedcompliance.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("Chkselection")).Checked;
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAssignmentLeave>;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAssignmentLeave rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAssignmentLeave rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdassignedcompliance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();
                bindAssignedcompliance();
                grdassignedcompliance.PageIndex = e.NewPageIndex;
                grdassignedcompliance.DataBind();
                // PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        public static void CreateComplianceLeaveMapping(UserLeaveInstance LCMapping)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var leavedetails = (from row in entities.UsersOnLeaves
                                        join row1 in entities.UserLeaveInstances
                                        on row.ID equals LCMapping.LeaveId
                                        where row.IsProcessed == false
                                        && row1.ComplianceInstanceID == LCMapping.ComplianceInstanceID
                                        && row1.LeaveId==LCMapping.LeaveId
                                        select row).ToList();
                    if (leavedetails.Count == 0)
                    {
                        entities.UserLeaveInstances.Add(LCMapping);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateComplianceLeaveMapping(List<UserLeaveInstance> LCMappingList,long LeaveID,string IsStatutoryInternal)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var chkleavedetails = (from row in entities.UsersOnLeaves
                                        where row.IsProcessed == false
                                        && row.ID == LeaveID
                                        select row).FirstOrDefault();
                    if (chkleavedetails != null)
                    {
                        entities.UserLeaveInstances.RemoveRange(entities.UserLeaveInstances.Where(x => x.LeaveId == LeaveID && x.IsStatutoryInternal.ToUpper().Trim()== IsStatutoryInternal.ToUpper().Trim()));
                        entities.SaveChanges();


                        LCMappingList.ForEach(entry =>
                        {
                            entities.UserLeaveInstances.Add(entry);
                            entities.SaveChanges();

                        });                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindcheckedcomplianceList(long LeaveID)
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<long> checklist = new List<long>();
                    if (rblcompliancetype.SelectedValue == "0")
                    {
                        checklist = (from row in entities.UserLeaveInstances
                                     where row.LeaveId == LeaveID
                                     && row.IsStatutoryInternal == "S"
                                     select row.ComplianceInstanceID).ToList();
                    }
                    else
                    {
                        checklist = (from row in entities.UserLeaveInstances
                                     where row.LeaveId == LeaveID
                                     && row.IsStatutoryInternal == "I"
                                     select row.ComplianceInstanceID).ToList();
                    }

                    if (checklist != null)
                    {
                        foreach (GridViewRow gvrow in grdassignedcompliance.Rows)
                        {
                            int index = Convert.ToInt32(grdassignedcompliance.DataKeys[gvrow.RowIndex].Value);
                            var rmdata = checklist.Where(t => t == index).FirstOrDefault();
                            if (rmdata != null && rmdata != 0)
                            {
                                CheckBox CheckBox1 = (CheckBox)gvrow.FindControl("Chkselection");
                                CheckBox1.Checked = true;
                            }
                        }
                    }                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void bindAssignedcompliance()
        {
            try
            {
                string comtype = string.Empty;
                if (rblcompliancetype.SelectedValue == "0")
                {
                    comtype = "S";
                }
                else
                {
                    comtype = "I";
                }
                var complianceinfo = Compliancedetails(AuthenticationHelper.UserID, comtype);
                int actid = -1;
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (actid != -1)
                {
                    complianceinfo = complianceinfo.Where(entry => entry.ActID == actid).ToList();
                }
                if (!string.IsNullOrEmpty(txtFilter.Text))
                {
                    if (CheckInt(txtFilter.Text))
                    {
                        int a = Convert.ToInt32(txtFilter.Text.ToUpper());
                        complianceinfo = complianceinfo.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        complianceinfo = complianceinfo.Where(entry =>
                        entry.ShortDescription.ToUpper().Trim().Contains(txtFilter.Text.ToUpper().Trim())
                        || entry.Branch.ToUpper().Trim().Contains(txtFilter.Text.ToUpper().Trim())
                        || (entry.Perofrmer !=null && entry.Perofrmer.ToUpper().Trim().Contains(txtFilter.Text.ToUpper().Trim()))
                        || (entry.Reviewer != null && entry.Reviewer.ToUpper().Trim().Contains(txtFilter.Text.ToUpper().Trim()))
                        ).ToList();
                    }
                }
                grdassignedcompliance.DataSource = complianceinfo;
                grdassignedcompliance.DataBind();
                //uptempassign.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindAssignedcompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }            
        }


        protected void txtFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bindAssignedcompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bindAssignedcompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<sp_assignedcomplianceLeave_Result> Compliancedetails(int userid,string comtype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var ComplianceDetails = (from row in entities.sp_assignedcomplianceLeave(userid, comtype)
                                             select row).Distinct().ToList();
                    return ComplianceDetails;
                }
            }
            catch (Exception ex)
            {                
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        protected void rblcompliancetype_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindAssignedcompliance();
                long leaveid = -1;
                leaveid = Convert.ToInt32(ViewState["LeaveID"]);
                bindcheckedcomplianceList(leaveid);
          }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        #endregion
        protected void rbtnlstleave_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbtnlstleave.SelectedValue == "1")
                {
                    txtStartDateC.Enabled = false;
                    txtEndDateC.Enabled = false;
                    ddlNewPerformerUsersC.Enabled = false;
                    ddlNewReviewerUsersC.Enabled = false;
                    ddlNewEventOwnerUsersC.Enabled = false;
                }
                else
                {
                    txtStartDateC.Enabled = true;
                    txtEndDateC.Enabled = true;
                    ddlNewPerformerUsersC.Enabled = true;
                    ddlNewReviewerUsersC.Enabled = true;
                    ddlNewEventOwnerUsersC.Enabled = true;
                }                 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void grdLeave_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillComplianceDocuments();
        }

        public static object Getassigncompliance(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.UsersOnLeaves
                                         where row.CreatedBy==UserID
                                         && row.PartialAddLevaeFlag=="P"
                                         select new
                                         {
                                             row.ID,
                                             row.LStartDate,
                                             row.LEndDate,
                                             row.CreatedOn,
                                         }).ToList();
                return ComplianceDetails;
            }
        }
        public static List<Sp_GetLeaveDetails_Result> GetDetails(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.Sp_GetLeaveDetails(UserID)                                         
                                         select row).ToList();
                return ComplianceDetails;
            }
        }
        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = true;
                if (Flag !="NO" )
                {
                    result = false;
                }               
               
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        public void FillComplianceDocuments()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                Session["TotalRows"] = 0;
                var ComplianceDocs = GetDetails(AuthenticationHelper.UserID);
                grdLeave.DataSource = ComplianceDocs;
                grdLeave.DataBind();
                Session["TotalRows"] = ComplianceDocs.Count;
                grdLeave.Visible = true;
                GetPageDisplaySummary();                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdLeave.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdLeave.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdLeave.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdLeave.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;


                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";
                grdLeave.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdLeave.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                lTotalCount.Text = GetTotalPagesCount().ToString();
                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void ddlNewPerformerUsersassign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewPerformerUsersassign.SelectedValue != "-1")
                {
                    ddlNewPerformerUsersassign.BorderColor = Color.Empty;
                    int PerformerID = Convert.ToInt32(ddlNewPerformerUsersassign.SelectedValue);
                    BindReviewerList(AuthenticationHelper.CustomerID, ddlNewReviewerUsersassign, roles, PerformerID);

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 761", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewReviewerUsersassign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewReviewerUsersassign.SelectedValue != "-1")
                {
                    ddlNewReviewerUsersassign.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 775", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewEventOwnerUsersassign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewEventOwnerUsersassign.SelectedValue != "-1")
                {
                    ddlNewEventOwnerUsersassign.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 790", MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlNewPerformerUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewPerformerUsers.SelectedValue != "-1")
                {
                    ddlNewPerformerUsers.BorderColor = Color.Empty;
                    int PerformerID = Convert.ToInt32(ddlNewPerformerUsers.SelectedValue);
                    BindReviewerList(AuthenticationHelper.CustomerID, ddlNewReviewerUsers, roles, PerformerID);
                }
 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 761", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewReviewerUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewPerformerUsers.SelectedValue != "-1")
                {
                    ddlNewPerformerUsers.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 775", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewEventOwnerUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewEventOwnerUsers.SelectedValue != "-1")
                {
                    ddlNewEventOwnerUsers.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 790", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void linkbutton_onclick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Controls/frmUpcomingCompliancessNew.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Cancel Leave
        public  bool BulkUserLeaveInstanceInsert(List<UserLeaveInstance> ListIAT)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    ListIAT.ForEach(EachStep =>
                    {
                        var IndIDs = (from row in entities.UserLeaveInstances
                                      where row.LeaveId == EachStep.LeaveId
                                       && row.ComplianceInstanceID == EachStep.ComplianceInstanceID
                                       && row.RoleId == EachStep.RoleId
                                       && row.IsStatutoryInternal == EachStep.IsStatutoryInternal
                                      select row).FirstOrDefault();
                        if (IndIDs == null)
                        {
                            Count++;
                            entities.UserLeaveInstances.Add(EachStep);
                            if (Count >= 500)
                            {
                                entities.SaveChanges();
                                Count = 0;
                            }
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public  void StatutoryUpdateUserStatus(List<ComplianceAssignment> list, long newuserid, long leaveid, long RoleID, long olduserID)
        {
            try
            {
                List<UserLeaveInstance> masterlist = new List<UserLeaveInstance>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    list.ForEach(entry =>
                    {
                        var objQuery = (from row in entities.ComplianceAssignments
                                        where row.ID == entry.ID
                                        select row).FirstOrDefault();
                        Count++;
                        objQuery.UserID = newuserid;
                        if (Count >= 500)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }

                        UserLeaveInstance ulilist = new UserLeaveInstance()
                        {
                            LeaveId = leaveid,
                            ComplianceInstanceID = entry.ComplianceInstanceID,
                            IsStatutoryInternal = "S",
                            RoleId = RoleID,
                            UserID = olduserID,
                        };
                        masterlist.Add(ulilist);
                    });
                    entities.SaveChanges();
                    BulkUserLeaveInstanceInsert(masterlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 207 ", MethodBase.GetCurrentMethod().Name);
            }
        }
        public void StatutoryUpdateUserStatus1(string ISRole,string IsFlagSIT,long leaveid, long Newuserid,long Olduserid,string ispartial)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var objQuery = entities.Sp_LeaveCancelUpdate(ISRole,IsFlagSIT, leaveid, Newuserid, Olduserid);

                    if (ispartial=="A")
                    {
                        var lit = (from row in entities.UsersOnLeaves
                                   where row.ID == leaveid
                                   select row).FirstOrDefault();
                        if (lit != null)
                        {
                            lit.IsProcessed = true;
                            entities.SaveChanges();
                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 249 ", MethodBase.GetCurrentMethod().Name);
            }
        }        
        public  void InternalUpdateUserStatus(List<InternalComplianceAssignment> list, long newuserid, long leaveid, long RoleID, long olduserID)
        {
            try
            {
                List<UserLeaveInstance> masterlist = new List<UserLeaveInstance>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    list.ForEach(entry =>
                    {
                        var objQuery = (from row in entities.InternalComplianceAssignments
                                        where row.ID == entry.ID
                                        select row).FirstOrDefault();
                        Count++;
                        objQuery.UserID = newuserid;
                        if (Count >= 500)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                        UserLeaveInstance ulilist = new UserLeaveInstance()
                        {
                            LeaveId = leaveid,
                            ComplianceInstanceID = entry.InternalComplianceInstanceID,
                            IsStatutoryInternal = "I",
                            RoleId = RoleID,
                            UserID = olduserID,
                        };
                        masterlist.Add(ulilist);
                    });
                    entities.SaveChanges();
                    BulkUserLeaveInstanceInsert(masterlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 207 ", MethodBase.GetCurrentMethod().Name);
            }
        }        
        public  void TaskUpdateUserStatus(List<TaskAssignment> list, long newuserid, long leaveid, long RoleID, long olduserID)
        {
            try
            {
                List<UserLeaveInstance> masterlist = new List<UserLeaveInstance>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    list.ForEach(entry =>
                    {
                        var objQuery = (from row in entities.TaskAssignments
                                        where row.ID == entry.ID
                                        select row).FirstOrDefault();
                        Count++;
                        objQuery.UserID = newuserid;
                        if (Count >= 500)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                        UserLeaveInstance ulilist = new UserLeaveInstance()
                        {
                            LeaveId = leaveid,
                            ComplianceInstanceID = entry.TaskInstanceID,
                            IsStatutoryInternal = "T",
                            RoleId = RoleID,
                            UserID = olduserID,
                        };
                        masterlist.Add(ulilist);
                    });
                    entities.SaveChanges();
                    BulkUserLeaveInstanceInsert(masterlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 207 ", MethodBase.GetCurrentMethod().Name);
            }
        }        
        public  void EventUpdateUserStatus(List<EventAssignment> list, long newuserid, long leaveid, long RoleID, long olduserID)
        {
            try
            {
                List<UserLeaveInstance> masterlist = new List<UserLeaveInstance>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    list.ForEach(entry =>
                    {
                        var objQuery = (from row in entities.EventAssignments
                                        where row.ID == entry.ID
                                        select row).FirstOrDefault();
                        Count++;
                        objQuery.UserID = newuserid;
                        if (Count >= 500)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }

                        UserLeaveInstance ulilist = new UserLeaveInstance()
                        {
                            LeaveId = leaveid,
                            ComplianceInstanceID = entry.EventInstanceID,
                            IsStatutoryInternal = "E",
                            RoleId = RoleID,
                            UserID = olduserID,
                        };
                        masterlist.Add(ulilist);
                    });
                    entities.SaveChanges();
                    BulkUserLeaveInstanceInsert(masterlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 207 ", MethodBase.GetCurrentMethod().Name);
            }
        }        
        public  void GetStartAll(long LeaveID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Startmaster = (entities.SP_GetUserCancelLeaveDetails("SD", LeaveID)).ToList();
                    foreach (var Sitem in Startmaster)
                    {
                        if (Sitem.OldPerformerID != -1)
                        {
                            #region Statutory
                            var statutoryassignment = (from row in entities.ComplianceAssignments
                                                       where row.UserID == Sitem.OldPerformerID
                                                       && row.RoleID == 3
                                                       select row).ToList();

                            if (statutoryassignment != null && Sitem.NewPerformerID != -1)
                            {
                                StatutoryUpdateUserStatus(statutoryassignment, (long)Sitem.NewPerformerID, Sitem.ID, 3, (long)Sitem.OldPerformerID);
                            }
                            #endregion

                            #region Internal
                            var Internalassignment = (from row in entities.InternalComplianceAssignments
                                                      where row.UserID == Sitem.OldPerformerID
                                                      && row.RoleID == 3
                                                      select row).ToList();

                            if (Internalassignment != null && Sitem.NewPerformerID != -1)
                            {
                                InternalUpdateUserStatus(Internalassignment, (long)Sitem.NewPerformerID, Sitem.ID, 3, (long)Sitem.OldPerformerID);
                            }
                            #endregion

                            #region Task
                            var statutoryTaskassignment = (from row in entities.TaskAssignments
                                                           where row.UserID == Sitem.OldPerformerID
                                                            && row.RoleID == 3
                                                           select row).ToList();

                            if (statutoryTaskassignment != null && Sitem.NewPerformerID != -1)
                            {
                                TaskUpdateUserStatus(statutoryTaskassignment, (long)Sitem.NewPerformerID, Sitem.ID, 3, (long)Sitem.OldPerformerID);
                            }
                            #endregion
                        }
                        if (Sitem.OldReviewerID != -1)
                        {
                            #region Statutory
                            var statutoryassignment = (from row in entities.ComplianceAssignments
                                                       where row.UserID == Sitem.OldReviewerID
                                                       && row.RoleID == 4
                                                       select row).ToList();

                            if (statutoryassignment != null && Sitem.NewReviewerID != -1)
                            {
                                StatutoryUpdateUserStatus(statutoryassignment, (long)Sitem.NewReviewerID, Sitem.ID, 4, (long)Sitem.OldReviewerID);
                            }
                            #endregion

                            #region Internal
                            var Internalassignment = (from row in entities.InternalComplianceAssignments
                                                      where row.UserID == Sitem.OldReviewerID
                                                      && row.RoleID == 4
                                                      select row).ToList();

                            if (Internalassignment != null && Sitem.NewReviewerID != -1)
                            {
                                InternalUpdateUserStatus(Internalassignment, (long)Sitem.NewReviewerID, Sitem.ID, 4, (long)Sitem.OldReviewerID);
                            }
                            #endregion

                            #region Task
                            var statutoryTaskassignment = (from row in entities.TaskAssignments
                                                           where row.UserID == Sitem.OldReviewerID
                                                            && row.RoleID == 4
                                                           select row).ToList();

                            if (statutoryTaskassignment != null && Sitem.NewReviewerID != -1)
                            {
                                TaskUpdateUserStatus(statutoryTaskassignment, (long)Sitem.NewReviewerID, Sitem.ID, 4, (long)Sitem.OldReviewerID);
                            }
                            #endregion
                        }
                        if (Sitem.OldEventOwnerID != -1)
                        {
                            #region Event
                            var eventassignment = (from row in entities.EventAssignments
                                                   where row.UserID == Sitem.OldEventOwnerID
                                                   && row.Role == 10
                                                   select row).ToList();

                            if (eventassignment != null && Sitem.NewEventOwnerID != -1)
                            {
                                EventUpdateUserStatus(eventassignment, (long)Sitem.NewEventOwnerID, Sitem.ID, 10, (long)Sitem.OldEventOwnerID);
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 1777", MethodBase.GetCurrentMethod().Name);
                cvDuplicateC.IsValid = false;
                cvDuplicateC.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void GetEndAll(long LeaveID,string ispartial)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Endmaster = (entities.SP_GetUserCancelLeaveDetails("ED", LeaveID)).ToList();
                    foreach (var Sitem in Endmaster)
                    {
                        if (Sitem.NewPerformerID != -1)
                        {
                            #region Statutory                                                   
                            if (Sitem.OldPerformerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("PER", "S", Sitem.ID, (long)Sitem.NewPerformerID, (long)Sitem.OldPerformerID, ispartial);
                            }
                            #endregion

                            #region Internal                         
                            if (Sitem.OldPerformerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("PER", "I", Sitem.ID, (long)Sitem.NewPerformerID, (long)Sitem.OldPerformerID, ispartial);
                            }
                            #endregion

                            #region Task                           
                            if (Sitem.OldPerformerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("PER", "T", Sitem.ID, (long)Sitem.NewPerformerID, (long)Sitem.OldPerformerID, ispartial);
                            }
                            #endregion
                        }
                        if (Sitem.NewReviewerID != -1)
                        {
                            #region Statutory                            
                            if (Sitem.OldReviewerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("REV", "S", Sitem.ID, (long)Sitem.NewReviewerID, (long)Sitem.OldReviewerID, ispartial);
                            }

                            #endregion

                            #region Internal                            
                            if (Sitem.OldReviewerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("REV", "I", Sitem.ID, (long)Sitem.NewReviewerID, (long)Sitem.OldReviewerID, ispartial);
                            }
                            #endregion

                            #region Task                            
                            if (Sitem.OldReviewerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("REV", "T", Sitem.ID, (long)Sitem.NewReviewerID, (long)Sitem.OldReviewerID, ispartial);
                            }
                            #endregion
                        }
                        if (Sitem.NewEventOwnerID != -1)
                        {
                            #region Event                                                    
                            if (Sitem.OldReviewerID != -1)
                            {
                                //(string ISRole, string IsFlagSIT, long leaveid, long Newuserid, long Olduserid)
                                StatutoryUpdateUserStatus1("EVO", "E", Sitem.ID, (long)Sitem.NewEventOwnerID, (long)Sitem.OldEventOwnerID, ispartial);
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 1777", MethodBase.GetCurrentMethod().Name);
                cvDuplicateC.IsValid = false;
                cvDuplicateC.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnCancelSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((int)ViewState["Mode"] == 1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {                       
                        string typevalue = string.Empty;
                        if (rbtnlstleave.SelectedValue == "1")
                        {
                            typevalue = "A";
                            #region All
                            var leaveID = Convert.ToInt32(ViewState["LeaveID"]);
                            var objQuery = (from row in entities.UsersOnLeaves
                                            where row.ID == leaveID
                                            select row).FirstOrDefault();

                            if (objQuery != null)
                            {
                                if (objQuery.IsProcessed == false)
                                {
                                    objQuery.UpdatedOn = DateTime.Now;
                                    objQuery.UpdatedBy = AuthenticationHelper.UserID;                                   
                                    objQuery.Type = typevalue;
                                    objQuery.IsProcessed = true;
                                    entities.SaveChanges();

                                    var objQuerys = (from row in entities.UserLeaveInstances
                                                     where row.LeaveId == leaveID
                                                     select row).ToList();
                                    if (objQuerys.Count>0)
                                    {
                                        GetEndAll(leaveID, "A");
                                    }
                                    cvDuplicateC.IsValid = false;
                                    cvDuplicateC.ErrorMessage = "Leave cancelled successfully.";
                                }
                                else
                                {
                                    objQuery.UpdatedOn = DateTime.Now;
                                    objQuery.UpdatedBy = AuthenticationHelper.UserID;                                   
                                    objQuery.Type = typevalue;
                                    entities.SaveChanges();

                                    cvDuplicateC.IsValid = false;
                                    cvDuplicateC.ErrorMessage = "Leave cancelled successfully.";
                                    GetEndAll(leaveID, "A");
                                }
                                FillComplianceDocuments();
                                upDocumentDownload.Update();
                                // upDocumentDownload.Update();
                            }

                            #endregion
                        }
                        else
                        {
                            typevalue = "P";
                            #region Partial
                            var leaveID = Convert.ToInt32(ViewState["LeaveID"]);
                            var UOL = (from row in entities.UsersOnLeaves
                                       where row.ID == leaveID
                                       select row).FirstOrDefault();
                            if (UOL != null)
                            {

                                if (!string.IsNullOrEmpty(txtStartDateC.Text) && !string.IsNullOrEmpty(txtEndDateC.Text))
                                {
                                    if (!string.IsNullOrEmpty(txtStartDateC.Text))
                                    {
                                        UOL.LStartDate = DateTime.ParseExact(Convert.ToString(txtStartDateC.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    }
                                    if (!string.IsNullOrEmpty(txtEndDateC.Text))
                                    {
                                        UOL.LEndDate = DateTime.ParseExact(Convert.ToString(txtEndDateC.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    }

                                    DateTime fromDatePeroid = new DateTime(UOL.LStartDate.Year, UOL.LStartDate.Month, UOL.LStartDate.Day);
                                    DateTime toDatePeroid = new DateTime(UOL.LEndDate.Year, UOL.LEndDate.Month, UOL.LEndDate.Day);
                                    if (fromDatePeroid <= toDatePeroid)
                                    {                                       
                                        if (UOL.IsProcessed == false)
                                        {
                                            UOL.LStartDate = UOL.LStartDate;
                                            UOL.LEndDate = UOL.LEndDate;
                                            UOL.UpdatedOn = DateTime.Now;
                                            UOL.UpdatedBy = AuthenticationHelper.UserID;                                            
                                            UOL.Type = typevalue;
                                            UOL.IsProcessed = false;
                                            entities.SaveChanges();

                                            var objQuerys = (from row in entities.UserLeaveInstances
                                                             where row.LeaveId == leaveID
                                                             select row).ToList();
                                            if (objQuerys.Count > 0)
                                            {
                                                GetEndAll(leaveID,"P");
                                            }
                                            cvDuplicateC.IsValid = false;
                                            cvDuplicateC.ErrorMessage = "Leave updated successfully.";
                                        }
                                        else
                                        {
                                            GetEndAll(leaveID, "P");
                                            UOL.LStartDate = UOL.LStartDate;
                                            UOL.LEndDate = UOL.LEndDate;
                                            UOL.UpdatedOn = DateTime.Now;
                                            UOL.UpdatedBy = AuthenticationHelper.UserID;                                            
                                            UOL.Type = typevalue;
                                            UOL.IsProcessed = false;
                                            entities.SaveChanges();
                                            cvDuplicateC.IsValid = false;
                                            cvDuplicateC.ErrorMessage = "Leave updated successfully.";                                                                                       
                                        }                                          
                                        FillComplianceDocuments();
                                        upDocumentDownload.Update();
                                        //upDocumentDownload.Update();
                                    }
                                    else
                                    {
                                        cvDuplicateC.IsValid = false;
                                        cvDuplicateC.ErrorMessage = "From Date should be less than To Date.";
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 1777", MethodBase.GetCurrentMethod().Name);
                cvDuplicateC.IsValid = false;
                cvDuplicateC.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void ddlNewPerformerUsersC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewPerformerUsersC.SelectedValue != "-1")
                {
                    ddlNewPerformerUsersC.BorderColor = Color.Empty;
                    int PerformerID = Convert.ToInt32(ddlNewPerformerUsersC.SelectedValue);
                    BindReviewerList(AuthenticationHelper.CustomerID, ddlNewReviewerUsersC, roles, PerformerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 987", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewReviewerUsersC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewPerformerUsersC.SelectedValue != "-1")
                {
                    ddlNewPerformerUsersC.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 1001", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlNewEventOwnerUsersC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewEventOwnerUsersC.SelectedValue != "-1")
                {
                    ddlNewEventOwnerUsersC.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 1015", MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtncancelleave_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "opencancelleavepopup();", true);
                ViewState["Mode"] = 0;
                txtStartDateC.Text = string.Empty;
                txtEndDateC.Text = string.Empty;
                ddlNewPerformerUsersC.ClearSelection();
                ddlNewReviewerUsersC.ClearSelection();
                
                txtStartDateC.Enabled = true;
                txtEndDateC.Enabled = true;
                ddlNewPerformerUsersC.Enabled = true;
                ddlNewReviewerUsersC.Enabled = true;
                ddlNewEventOwnerUsersC.Enabled = true;
                upPromotorC.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 999", MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion
    }
}