﻿<%@ Page Title="Reminders List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="User_RemindersInternalCA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.User_RemindersInternalCA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserReminderList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUserReminder" OnClick="btnAddUserReminder_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdUserReminder" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdUserReminder_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdUserReminder_Sorting"
                Font-Size="12px" DataKeyNames="ComplianceReminderID" OnRowCommand="grdUserReminder_RowCommand"
                OnPageIndexChanging="grdUserReminder_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Location" SortExpression="Branch" ItemStyle-Width="300px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                <asp:Label runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="Description" ItemStyle-Width="400px">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                <asp:Label runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Role" />
                    <asp:TemplateField HeaderText="Due Date" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reminder On" ItemStyle-HorizontalAlign="Center" SortExpression="RemindOn">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("RemindOn")).ToString("dd-MMM-yyyy")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                        <ItemTemplate>
                            <%# ((com.VirtuosoITech.ComplianceManagement.Business.Data.ReminderStatus)Convert.ToByte(Eval("Status"))).ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" Visible="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_USER_REMINDER" CommandArgument='<%# Eval("ComplianceReminderID") %>'><img src="../Images/edit_icon.png" alt="Edit Reminder" title="Edit Reminder" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_USER_REMINDER" CommandArgument='<%# Eval("ComplianceReminderID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this reminder?');"><img src="../Images/delete_icon.png" alt="Delete Reminder" title="Delete Reminder" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUserRemindersDialog">
        <asp:UpdatePanel ID="upUserReminders" runat="server" UpdateMode="Conditional" OnLoad="upUserReminders_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="UserReminderValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="UserReminderValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <asp:DropDownList runat="server" ID="ddlBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataValueField="ID" DataTextField="Name"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select Location." ControlToValidate="ddlBranch"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserReminderValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance</label>
                        <asp:DropDownList runat="server" ID="ddlCompliance" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataValueField="ID" DataTextField="Name"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Compliance."
                            ControlToValidate="ddlCompliance" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserReminderValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Role</label>
                        <asp:DropDownList runat="server" ID="ddlRole" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataValueField="ID" DataTextField="Name" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserReminderValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; height: 20px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Scheduled On</label>
                        <asp:Literal ID="litScheduledOn" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px; clear: both">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Remind On</label>
                        <asp:TextBox runat="server" ID="tbxDate" Style="height: 16px; width: 390px;" />
                        <asp:RequiredFieldValidator ErrorMessage="Please select Remind On." ControlToValidate="tbxDate"
                            runat="server" ValidationGroup="UserReminderValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 160px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="UserReminderValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUserRemindersDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divUserRemindersDialog').dialog({
                height: 450,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "User Reminder",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });


        function initializeCombobox() {
            $("#<%= ddlBranch.ClientID %>").combobox();
             $("#<%= ddlCompliance.ClientID %>").combobox();
             $("#<%= ddlRole.ClientID %>").combobox();
         }

         function initializeDatePicker(date) {
             var startDate = new Date();
             $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                defaultDate: startDate,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

    </script>
</asp:Content>
