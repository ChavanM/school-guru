﻿<%@ Page Title="City Master" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="CityMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.CityMaster" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#divAuditorDialog').modal('show');
        }
    </script>

    <script type="text/javascript">

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('City Master');
            fhead('City Master');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlCountry"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;"  DataPlaceHolder="Country" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:DropDownListChosen runat="server" ID="ddlStateList"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;"  DataPlaceHolder="State" OnSelectedIndexChanged="ddlStateList_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:TextBox runat="server" ID="tbxSearchCity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;"  PlaceHolder="Search City" OnTextChanged="tbxSearchCity_TextChanged">
                                    </asp:TextBox>
                                </div>
            <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" />
                    </div>
            <div style="margin-bottom: 4px"> 
            <%--<asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdAuditor_RowDataBound"
                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"  DataKeyNames="CityID" OnRowCommand="grdAuditor_RowCommand">
                    <Columns>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:BoundField DataField="CityName" HeaderText="City" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="55%" />
                        <asp:BoundField DataField="StateName" HeaderText="State" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%" />                        
                        <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Edit_City" OnClientClick="fopenpopup()" data-toggle="tooltip" data-placement="top" ToolTip="Edit City Details"
                                    CommandArgument='<%# Eval("CityID") %>'><img src="../../Images/edit_icon_new.png" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_City" Visible="false" data-toggle="tooltip" data-placement="top" ToolTip="Delete City Details"
                                    CommandArgument='<%# Eval("CityID") %>' OnClientClick="return confirm('Are you certain you want to delete this City Details?');"><img src="../../Images/delete_icon_new.png"/></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />    
                      <PagerSettings Visible="false" />        
                    <PagerTemplate>
                                      <%--  <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                      </PagerTemplate>
                </asp:GridView>
                   <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0" style="float:right">
                            <div class="table-paging" style="margin-bottom: 10px;">
                                <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                <div class="table-paging-text" style="float: right;">
                                    <p>
                                        Page
                                      <%--  <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                    </p>
                                </div>
                                <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                                </div>
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divAuditorDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="AuditAssignment">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            City</label>
                                        <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="City Name can not be empty." ControlToValidate="txtFName"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="PromotorValidationGroup"
                                            ErrorMessage="Please enter a valid location" ControlToValidate="txtFName"
                                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            State</label>
                                        <asp:DropDownList runat="server" ID="ddlState" class="form-control m-bot15" Style="width: 250px;"
                                            AutoPostBack="true" />
                                        <%--      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="State Name can not be empty." ControlToValidate="ddlState"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />--%>
                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select State" ControlToValidate="ddlState"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
