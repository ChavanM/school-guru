﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Web;
using System.Web.Security;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class UserList : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        protected static string CustomerName;
        public static int userID;
        public bool MGM_KEy;
        protected static int customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN") {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }

                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {                        
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "Name";
                        userID = Convert.ToInt32(AuthenticationHelper.UserID);
                        BindCustomers(userID);
                        BindUsers();
                        Session["CurrentRole"] = AuthenticationHelper.Role;
                        Session["CurrentUserId"] = AuthenticationHelper.UserID;
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        divCustomerfilter.Visible = false;
                        btnChangeUserRole.Visible = true;
                    }
                    else if (AuthenticationHelper.Role == "MGMT")
                    {
                        divCustomerfilter.Visible = false;
                        btnChangeUserRole.Visible = true;
                    }
                    else
                    {
                        divCustomerfilter.Visible = true;
                        btnChangeUserRole.Visible = false;
                    }
                        if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                        {
                            btnAddUser.Visible = false;
                        }
                        ViewState["AssignedCompliancesID"] = null;
                        ViewState["AssignedTaskID"] = null;
                        chkEvent.Checked = false;
                    }
                   
                
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            udcInputForm.OnSaved += (inputForm, args) => { BindUsers(); };
        }

        #region user Detail
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role== "SADMN")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                int userid = Convert.ToInt32(AuthenticationHelper.UserID);
                if (AuthenticationHelper.Role == "IMPT")
                {
                    var uselist = Assigncustomer.GetAllNewUser(customerID, userid, tbxFilter.Text);
                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "FirstName")
                        {
                            uselist = uselist.OrderBy(entry => entry.FirstName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "LastName")
                        {
                            uselist = uselist.OrderBy(entry => entry.LastName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Email")
                        {
                            uselist = uselist.OrderBy(entry => entry.Email).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ContactNumber")
                        {
                            uselist = uselist.OrderBy(entry => entry.ContactNumber).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            uselist = uselist.OrderBy(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "FirstName")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.FirstName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "LastName")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.LastName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Email")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.Email).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ContactNumber")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.ContactNumber).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Ascending;
                    }
                    grdUser.DataSource = uselist;
                    grdUser.DataBind();
                    upUserList.Update();
                }
                else
                {
                    var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "FirstName")
                        {
                            uselist = uselist.OrderBy(entry => entry.FirstName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "LastName")
                        {
                            uselist = uselist.OrderBy(entry => entry.LastName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Email")
                        {
                            uselist = uselist.OrderBy(entry => entry.Email).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ContactNumber")
                        {
                            uselist = uselist.OrderBy(entry => entry.ContactNumber).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            uselist = uselist.OrderBy(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "FirstName")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.FirstName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "LastName")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.LastName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Email")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.Email).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ContactNumber")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.ContactNumber).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            uselist = uselist.OrderByDescending(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Ascending;
                    }
                    grdUser.DataSource = uselist;
                    grdUser.DataBind();
                    upUserList.Update();
                } 
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            chkCheckList.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
            {                
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
                else
                {
                    BindInternalComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
            }

        }
        protected void chkCheckList_CheckedChanged(object sender, EventArgs e)
        {
            chkEvent.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
            {
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
                else
                {
                    BindInternalComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
            }
        }
        public static List<User> GetAllUsers(int customerID,int UID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID != UID
                             select row).ToList();

                return query.ToList();
            }
        }
        //private void BindUserList(int UID)
        //{
        //    try
        //    {
        //        int customerID = -1;
        //        if (AuthenticationHelper.Role == "CADMN")
        //        {
        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }
        //        if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SADMN")
        //        {
        //            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
        //        }
        //        else
        //        {
        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        var lstAllUsers = GetAllUsers(customerID, UID);

        //        var lstUsers = (from row in lstAllUsers
        //                        select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();
                
        //        drpuser.DataTextField = "Name";
        //        drpuser.DataValueField = "ID";
        //        if (lstUsers != null)
        //        {
        //            drpuser.DataSource = lstUsers;
        //            drpuser.DataBind();
        //        }

        //        drpuser.Items.Insert(0, new ListItem("< Select user >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    udcInputForm.EditUserInformation(userID);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    if (UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (EventManagement.GetAllAssignedInstancesByUser(userID).Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventInform", "alert('Account can not be deleted. One or more Event are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (UserManagement.HasInternalCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                        else if(UserManagement.HasEventOwnerAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventOwnerInform", "alert('Account can not be deleted. One or more Event Owner are assigned to user, please re-assign to other user.');", true);
                    }
                     else if (UserManagement.HasTaskAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "TaskInform", "alert('Account can not be deleted. One or more Task are assigned to user, please re-assign to other user.');", true);
                    } 
                    else if (UserManagement.IsSelectedUserUniverse(userID))
                    {
                        txtuserid.Text = "";
                        txtuserid.Text = userID.ToString();
                        BindFolderData(userID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divassignefolderDialog\").dialog('open')", true);
                    }
                    else
                    {
                        UserManagement.Delete(userID);
                        UserManagementRisk.Delete(userID);
                        UserManagement.DeleteTempAssignmentTableInternal(userID);
                        UserManagement.DeleteTempAssignmentTable(userID);
                        UserManagement.DeleteTempAssignmentTableCheckList(userID);
                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.ToggleStatus(userID);
                        UserManagementRisk.ToggleStatus(userID);

                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    User user = UserManagement.GetByID(userID);
                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                    string passwordText = Util.CreateRandomPassword(10);
                    string encryptedPwd = Util.CalculateAESHash(passwordText);
                    user.Password = encryptedPwd;
                    mstuser.Password = encryptedPwd;

                    string ReplyEmailAddressName = "";
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else if (AuthenticationHelper.Role == "CADMN")
                    {

                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                    }
                    else if (AuthenticationHelper.Role == "MGMT")
                    {

                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                    }
                    string accessURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                    if (Urloutput != null)
                    {
                        accessURL = Urloutput.URL;
                    }
                    else
                    {
                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(accessURL), ReplyEmailAddressName);
                    bool result = UserManagement.ChangePassword(user);
                    bool result1 = UserManagementRisk.ChangePassword(mstuser);

                    //Added on 20072020 for TL_AVACOM
                    bool isRLCSUser = UserManagement.IsRLCSUser(userID);
                    if (isRLCSUser)
                    {
                        UserManagement.ChangePassword_RLCS(userID, encryptedPwd, "A");
                    }

                    if (result && result1)
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
                else if (e.CommandName.Equals("MODIFY_ASSIGNMENT"))
                {
                    chkEvent.Checked = false;
                    chkCheckList.Checked = false;
                    txtAssigmnetFilter.Text = string.Empty;
                    ViewState["UserID"] = userID;
                    User user = UserManagement.GetByID(userID);
                    litCurrentUser.Text = user.FirstName + " " + user.LastName;
                    BindNewUserList(user.CustomerID ?? -1, ddlNewUsers);
                    rbtSelectionType.SelectedValue = "0";

                    BindComplianceInstances(userID);
                    int IsIComplianceApp = 0;
                    Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    if (customer.IComplianceApplicable != null)
                    {
                        IsIComplianceApp = Convert.ToInt32(customer.IComplianceApplicable);
                    }
                    if (IsIComplianceApp.ToString() == "1")
                    {
                        divSelectionType.Visible = true;
                    }
                    else
                    {
                        divSelectionType.Visible = false;
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenAssignmentsDialog", "$(\"#divModifyAssignment\").dialog('open')", true);
                    upModifyAssignment.Update();
                }
                else if (e.CommandName.Equals("MODIFY_EVENT_ASSIGNMENT"))
                {
                    txtAssignEventUser.Text = string.Empty;
                    ViewState["UserID"] = userID;
                    User user = UserManagement.GetByID(userID);
                    lblEventAssignedUser.Text = user.FirstName + " " + user.LastName;
                    BindNewUserList(user.CustomerID ?? -1, ddlAllUsers);
                    BindEventInstances(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenAssignmentsDialog", "$(\"#divAssignEvent\").dialog('open')", true);
                    upAssignEvent.Update();
                }
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    UserManagement.WrongAttemptCountUpdate(userID);
                    UserManagementRisk.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                    BindUsers();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                var data = UserManagement.GetAllUserCustomerID(customerID, null);
                
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddlNewReviewerUsers.DataTextField = "Name";
                ddlNewReviewerUsers.DataValueField = "ID";
                if (data !=null)
                {                    
                    ddllist.DataSource = data;
                    ddllist.DataBind();
                   
                    ddlNewReviewerUsers.DataSource = data;
                    ddlNewReviewerUsers.DataBind();
                }
                
                ddllist.Items.Insert(0, new ListItem("< Select user >", "-1"));                
                ddlNewReviewerUsers.Items.Insert(0, new ListItem("< Select user >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upUserList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if(AuthenticationHelper.Role=="IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var users = UserManagement.GetAllUser(customerID, tbxFilter.Text);               
                if (direction == SortDirection.Ascending)
                {
                    users = users.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    users = users.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdUser.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
                    }
                }

                grdUser.DataSource = users;
                grdUser.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int id = Convert.ToInt32(((GridView) sender).DataKeys[e.Row.RowIndex].Value);

                    LinkButton lbtnDelete = (LinkButton) e.Row.FindControl("lbtnDelete");
                    LinkButton lbtnModifyAssignment = (LinkButton) e.Row.FindControl("lbtnModifyAssignment");
                    LinkButton lbtnChangeStatus = (LinkButton) e.Row.FindControl("lbtnChangeStatus");
                    LinkButton lbtnEventAssignment = (LinkButton) e.Row.FindControl("lbtnEventAssignment");

                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        lbtnDelete.Visible = false;
                    }
                    else if (AuthenticationHelper.Role == "CADMN")
                    {
                        lbtnDelete.Visible = false;
                    }
                    else
                    {
                        lbtnDelete.Visible = true;
                    }

                    if (UserManagement.GetByID(id).RoleID == 1)
                    {
                        lbtnDelete.Visible = false;
                        lbtnModifyAssignment.Visible = false;
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    if (id == AuthenticationHelper.UserID)
                    {
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    int a = 0;
                    int b = 0;
                    int c = 0;
                    int d = 0;

                    a = Business.ComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    b = Business.InternalComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    c = Business.ComplianceManagement.GetAllAssignedInstancesCheckList(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    d = Business.TaskManagment.GetAllTaskAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;

                    
                    if (a <= 0 && b <= 0 && c <= 0 && d <= 0)
                    {
                        lbtnModifyAssignment.Visible = false;
                    }
                   
                    if (EventManagement.GetAllAssignedInstancesByUser(id).Count <= 0)
                    {
                        lbtnEventAssignment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        #endregion

        private void BindComplianceInstances(int userID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                if (customerID != -1)
                {
                    grdTask.DataSource = null;
                    grdTask.DataBind();
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        List<Sp_ComplianceAssignedInstance_Result> datasource = new List<Sp_ComplianceAssignedInstance_Result>();
                        if (chkEvent.Checked == true)
                        {
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("Y", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                        }
                        else if (chkCheckList.Checked == true)
                        {
                            chkEvent.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("NA", customerID, userID, "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                        }
                        else
                        {
                            chkEvent.Checked = false;
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("N", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                        }
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else if (rbtSelectType.SelectedValue == "1")
                    {
                        grdComplianceInstances.DataSource = null;
                        grdComplianceInstances.DataBind();
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();

                        if (rbtSelectionType.SelectedValue == "0")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstance_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstance_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_Internal(customerID, userID,"S", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                        }
                        if (rbtSelectionType.SelectedValue == "1")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstance_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstance_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_Internal(customerID, userID, "I", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                        }
                    }
                    else
                    {
                        BindInternalComplianceInstances(userID);
                    }
                    upModifyAssignment.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindInternalComplianceInstances(int userID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
            else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                if (customerID != -1)
                {
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        if (chkCheckList.Checked == true)
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstances("Y", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                        }
                        else if (chkEvent.Checked == true)
                        {
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                            grdInternalComplianceInstances.DataSource = null;
                            grdInternalComplianceInstances.DataBind();
                        }
                        else
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstances("N", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                        }
                    }
                    else
                    {
                        BindComplianceInstances(userID);
                    }

                    upModifyAssignment.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
            {
                int customerid = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    udcInputForm.AddNewUser(Convert.ToInt32(ddlCustomerList.SelectedValue));
                }
                else
                {
                    udcInputForm.AddNewUser(customerid);
                }
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string[] confirmValue = Request.Form["confirm_value"].Split(',');

                    if (confirmValue[confirmValue.Length - 1] == "Yes")
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            if (rbtSelectionType.SelectedValue == "0")
                            {
                                #region Statutory
                                CheckBoxValueSaved();
                                List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
                                if (chkList != null)
                                {                                                                  
                                    string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                    if (action.Equals("Delete"))
                                    {
                                        #region Delete Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                     else if (AuthenticationHelper.Role == "MGMT")
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                        if (AuthenticationHelper.Role == "IMPT")
                                        {
                                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                                        }
                                        else
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                        if (customerID != -1)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
                                                {
                                                    if (chkList.Count > 0)
                                                    {
                                                        ComplianceExclude_Reassign.Statutory_DeleteComplianceAssignment(Convert.ToInt32(ViewState["UserID"]), chkList, customerID, AuthenticationHelper.UserID);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }

                                            //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                            //{                                                
                                            //    if (chkList.Count > 0)
                                            //    {
                                            //        var instancsids = chkList.Select(a=>a.Item1).ToList();         

                                            //        var compilancelist = (from CI in entities.ComplianceInstances
                                            //                          join C in entities.Compliances on CI.ComplianceId equals C.ID
                                            //                          join A in entities.Acts on C.ActID equals A.ID
                                            //                          join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                                            //                          where instancsids.Contains(CI.ID)
                                            //                          select new
                                            //                          {
                                            //                              Branch = CB.Name,
                                            //                              ComplianceID = C.ID,
                                            //                              ActName = A.Name,
                                            //                              ShortDescription = C.ShortDescription
                                            //                          }).ToList();


                                            //            string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                            //            var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeComplinces
                                            //                                       .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                            //                                       .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                                       .Replace("@From", ReplyEmailAddressName);
                                            //StringBuilder details = new StringBuilder();
                                            //var remindersummary = compilancelist.Distinct().ToList();

                                            //if (remindersummary.Count > 0)
                                            //{
                                            //    foreach (var row in remindersummary)
                                            //    {
                                            //        details.AppendLine(com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeTableRows
                                            //            .Replace("@Branch", row.Branch)
                                            //            .Replace("@ComplianceID", Convert.ToString(row.ComplianceID))
                                            //            .Replace("@ActName", row.ActName)
                                            //            .Replace("@Compliance", row.ShortDescription));

                                            //    }
                                            //    string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                                            //            // new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding compliances.", message); }).Start();

                                            //new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for excluding compliances.", message); }).Start();
                                            //        }//remindersummary.Count
                                            //    }//chkList.Count end
                                            //}
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Ressign Performer /Reviewer
                                        long pID = -1;
                                        long rID = -1;
                                        if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                        {
                                            if (ddlNewUsers.SelectedValue != "-1")
                                            {
                                                pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                        {
                                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                                            {
                                                rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                            }
                                        }
                                        if (pID == -1 && rID == -1)
                                        {
                                            CustomModifyAsignment.IsValid = false;
                                            CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                            return;
                                        }
                                        else
                                        {
                                            ComplianceExclude_Reassign.Statutory_ReplaceUserForComplianceAssignment(pID, rID,AuthenticationHelper.UserID, chkList);
                                            //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                            //var newPerformerUser = UserManagement.GetByID((int)pID);
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newPerformerUser.CustomerID)))
                                            //{
                                            //string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newPerformerUser.CustomerID)).Name;
                                            //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesNewOwner
                                            //                        .Replace("@User", newPerformerUser.FirstName + " " + newPerformerUser.LastName)
                                            //                        .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                        .Replace("@From", ReplyEmailAddressName);

                                            //    // new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newPerformerUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                            //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"],c, null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                            //}


                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                            //{
                                            //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                            //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesOldOwner
                                            //                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                            //                        .Replace("@newUser", newPerformerUser.FirstName + " " + newPerformerUser.LastName)
                                            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                        .Replace("@From", ReplyEmailAddressName);

                                            //    //new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                            //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                            //}                                            
                                        }
                                        #endregion
                                    }
                                    BindUsers();
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                    ViewState["AssignedCompliancesID"] = null;
                                   
                                }
                                else
                                {
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                                }
                                #endregion
                            }
                            else//internal
                            {
                                #region internal
                                InternalCheckBoxValueSaved();
                                List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];
                                if (chkIList != null)
                                {
                                    string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                    if (action.Equals("Delete"))
                                    {
                                        #region Delete Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                        if (AuthenticationHelper.Role == "MGMT")
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                        if (AuthenticationHelper.Role == "IMPT")
                                            {
                                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                                        }
                                        else
                                        {
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        }
                                        if (customerID != -1)
                                        {                                            
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
                                                {
                                                    if (chkIList.Count > 0)
                                                    {
                                                        ComplianceExclude_Reassign.Internal_DeleteComplianceAssignment(Convert.ToInt32(ViewState["UserID"]), chkIList, customerID, AuthenticationHelper.UserID);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }


                                            //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                            //{
                                            //    if (chkIList.Count > 0)
                                            //    {
                                            //        var instancsids = chkIList.Select(a => a.Item1).ToList();

                                            //        var compilancelist = (from ICI in entities.InternalComplianceInstances
                                            //                              join IC in entities.InternalCompliances on ICI.InternalComplianceID equals IC.ID                                                                          
                                            //                              join CB in entities.CustomerBranches on ICI.CustomerBranchID equals CB.ID
                                            //                              where instancsids.Contains(ICI.ID)
                                            //                              select new
                                            //                              {
                                            //                                  Branch = CB.Name,
                                            //                                  ComplianceID = IC.ID,                                                                              
                                            //                                  ShortDescription = IC.IShortDescription
                                            //                              }).ToList();


                                            //        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                            //        var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeInternalComplinces
                                            //                                   .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                            //                                   .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                                   .Replace("@From", ReplyEmailAddressName);
                                            //        StringBuilder details = new StringBuilder();
                                            //        var remindersummary = compilancelist.Distinct().ToList();

                                            //        if (remindersummary.Count > 0)
                                            //        {
                                            //            foreach (var row in remindersummary)
                                            //            {
                                            //                details.AppendLine(com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeInternalTableRows
                                            //                    .Replace("@Branch", row.Branch)
                                            //                    .Replace("@ComplianceID", Convert.ToString(row.ComplianceID))                                                                
                                            //                    .Replace("@Compliance", row.ShortDescription));

                                            //            }
                                            //            string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                                            //            // new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding compliances.", message); }).Start();                                                       
                                            //            new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for excluding compliances.", message); }).Start();
                                            //        }//remindersummary.Count
                                            //    }//chkList.Count end                                               
                                            //}
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Ressign Performer /Reviewer
                                        long pID = -1;
                                        long rID = -1;
                                        if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                        {
                                            if (ddlNewUsers.SelectedValue != "-1")
                                            {
                                                pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                        {
                                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                                            {
                                                rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                            }
                                        }
                                        if (pID == -1 && rID == -1)
                                        {
                                            CustomModifyAsignment.IsValid = false;
                                            CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                            return;
                                        }
                                        else
                                        {
                                            ComplianceExclude_Reassign.Internal_ReplaceUserForComplianceAssignment(pID, rID,AuthenticationHelper.UserID, chkIList);
                                            //var newUser = UserManagement.GetByID((int)pID);
                                            //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                            //{
                                            //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                            //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesNewOwner
                                            //                        .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                            //                        .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                        .Replace("@From", ReplyEmailAddressName);

                                            //    //new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                            //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                                
                                            //}
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                            //{
                                            //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                            //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesOldOwner
                                            //                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                            //                        .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                        .Replace("@From", ReplyEmailAddressName);

                                            //    //new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                            //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                                
                                            //}                                           
                                        }
                                        #endregion
                                    }

                                    BindUsers();
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                    ViewState["AssignedInternalCompliancesID"] = null;
                                }
                                else
                                {
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                                }
                                #endregion
                            }
                        }//Task
                        else
                        {
                            #region Task
                            Task_CheckBoxValueSaved();
                            List<Tuple<long, int>> chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];
                            string action = Convert.ToString(rbtModifyAction.SelectedValue);
                            if (action.Equals("Delete"))
                            {
                                #region Delete Assignment
                                int customerID = -1;
                                if (AuthenticationHelper.Role == "CADMN")
                                {
                                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                }
                            else if (AuthenticationHelper.Role == "MGMT")
                                {
                                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                }
                                if (AuthenticationHelper.Role == "IMPT")
                                {
                                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                                }
                                else
                                {
                                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                }
                                if (customerID != -1)
                                {
                                    try
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
                                        {
                                            if (chkList.Count > 0)
                                            {
                                                bool taskresult = ComplianceExclude_Reassign.DeleteTaskAssignmentTask(Convert.ToInt32(ViewState["UserID"]), chkList, customerID, AuthenticationHelper.UserID);
                                                if (taskresult)
                                                {
                                                    //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                                    //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                                    //{
                                                    //if (chkList.Count > 0)
                                                    //{
                                                    //    var instancsids = chkList.Select(a => a.Item1).ToList();                                           
                                                    //    var compilancelist = (from TI in entities.TaskInstances
                                                    //                          join TK in entities.Tasks on TI.TaskId equals TK.ID
                                                    //                          join TCM in entities.TaskComplianceMappings on TK.MainTaskID equals TCM.TaskID
                                                    //                          join C in entities.Compliances on TCM.ComplianceID equals C.ID
                                                    //                          join CB in entities.CustomerBranches on TI.CustomerBranchID equals CB.ID
                                                    //                          where instancsids.Contains(TI.ID)
                                                    //                          select new
                                                    //                          {
                                                    //                              Branch = CB.Name,
                                                    //                              TaskID = TI.TaskId,
                                                    //                              TaskTitle=TK.Title,
                                                    //                              ShortDescription = C.ShortDescription
                                                    //                          }).ToList();
                                                    //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                                    //    var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeTask
                                                    //                               .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                    //                               .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                    //                               .Replace("@From", ReplyEmailAddressName);
                                                    //    StringBuilder details = new StringBuilder();
                                                    //    var remindersummary = compilancelist.Distinct().ToList();
                                                    //    if (remindersummary.Count > 0)
                                                    //    {
                                                    //        foreach (var row in remindersummary)
                                                    //        {
                                                    //            details.AppendLine(com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeTaskTableRows 
                                                    //                .Replace("@Branch", row.Branch)
                                                    //                .Replace("@TaskID", Convert.ToString(row.TaskID))
                                                    //                .Replace("@TaskTitle", Convert.ToString(row.TaskTitle))
                                                    //                .Replace("@Compliance", row.ShortDescription));
                                                    //        }
                                                    //        string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                                                    //        //new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding Task.", message); }).Start();
                                                    //        new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for excluding Task.", message); }).Start();
                                                    //    }//remindersummary.Count
                                                    //}//chkList.Count end 

                                                    ////string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                                    ////string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeTask
                                                    ////                    .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                    ////                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                    ////                    .Replace("@From", ReplyEmailAddressName);                          
                                                    //}
                                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                                    ViewState["AssignedTaskID"] = null;
                                                }
                                                else
                                                {
                                                    ViewState["AssignedTaskID"] = null;
                                                    CustomModifyAsignment.IsValid = false;
                                                    CustomModifyAsignment.ErrorMessage = "Subtask is exist.";
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }

                                   
                                }
                                #endregion
                            }
                            else
                            {
                                #region Ressign Performer /Reviewer
                                long pID = -1;
                                long rID = -1;
                                if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                {
                                    if (ddlNewUsers.SelectedValue != "-1")
                                    {
                                        pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                    }
                                }
                                if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                {
                                    if (ddlNewReviewerUsers.SelectedValue != "-1")
                                    {
                                        rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                    }
                                }
                                if (pID == -1 && rID == -1)
                                {
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                    return;
                                }
                                else
                                {
                                    ComplianceExclude_Reassign.ReassignTask(pID, rID,AuthenticationHelper.UserID, chkList);
                                    //var newUser = UserManagement.GetByID((int)pID);
                                    //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                    //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                    //{
                                    //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                    //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskNewOwner
                                    //                        .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                    //                        .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                    //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                        .Replace("@From", ReplyEmailAddressName);

                                    //    //  new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                    //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                    //}


                                    //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                    //{
                                    //    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                    //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_Task_ReassignTaskOldOwner
                                    //                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                    //                        .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                    //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                        .Replace("@From", ReplyEmailAddressName);

                                    //    //new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                    //    new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"],c, null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                    //}
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);                                   
                                }                              
                                BindUsers();
                                #endregion
                            }
                            #endregion
                        }
                    }
                    else if (confirmValue[confirmValue.Length - 1] == "No")
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                    }
                    else
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Please select user and compliance for proceed.";                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region[Export to excel]
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                
                bool CustFlag = false;
                if(AuthenticationHelper.Role != "CADMN" && ddlCustomerList.SelectedValue != "-1")
                {
                    CustFlag = true;
                }
                else
                {
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        CustFlag = true;
                    }
                    else
                    {
                        CustFlag = false;
                    }
                }
              
                if (AuthenticationHelper.Role != "MGMT" && ddlCustomerList.SelectedValue != "-1")
                {
                    CustFlag = true;
                }
                else
                {
                    if (AuthenticationHelper.Role == "MGMT")
                    {
                        CustFlag = true;
                    }
                    else
                    {
                        CustFlag = false;
                    }
                }

                if (CustFlag == true)
                {

                    //this.Validate(btnExport.ValidationGroup);
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("UserDetails");
                        DataTable ExcelData = null;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                            int customerID = -1;
                            if (AuthenticationHelper.Role == "CADMN")
                            {
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                            else if (AuthenticationHelper.Role == "IMPT")
                            {
                                customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                            }
                            else
                            {
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }

                            var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                            string filterdata = "";
                            filterdata = tbxFilter.Text;

                            var Data = (from row in entities.Userdetails(customerID)
                                        select row).ToList();

                            if (filterdata != "")
                            {
                                Data = Data.Where(entry => entry.UserName.Contains(filterdata) || entry.Email.Contains(filterdata) || entry.ContactNumber.Contains(filterdata)).ToList();
                            }


                            DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                            List<Userdetails_Result> cb = new List<Userdetails_Result>();

                            ExcelData = view.ToTable("Selected", false, "ID", "UserName", "Email", "ContactNumber", "Role", "IsActive");

                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Value = "Customer Name:";

                            // exWorkSheet.Cells["B2"].Merge = true;
                            exWorkSheet.Cells["B2"].Value = cname;

                            exWorkSheet.Cells["A3:F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A3:F3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);



                            exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].Value = "User ID";
                            exWorkSheet.Cells["A3"].AutoFitColumns(25);

                            exWorkSheet.Cells["B3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B3"].Value = "User Name";
                            exWorkSheet.Cells["B3"].AutoFitColumns(40);

                            exWorkSheet.Cells["C3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C3"].Value = "Email";
                            exWorkSheet.Cells["C3"].AutoFitColumns(40);

                            exWorkSheet.Cells["D3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D3"].Value = "Contact No";
                            exWorkSheet.Cells["D3"].AutoFitColumns(25);


                            exWorkSheet.Cells["E3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E3"].Value = "Role";
                            exWorkSheet.Cells["E3"].AutoFitColumns(25);

                            exWorkSheet.Cells["F3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F3"].Value = "Status";
                            exWorkSheet.Cells["F3"].AutoFitColumns(25);



                        }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 3 + ExcelData.Rows.Count, 6])
                        {

                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=UserDetails.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                }
                else
                {
                    //cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "Please select customer";
                    //Response.Write("alert('Please select customer');");
                    CustomValidator2.IsValid = false;
                    CustomValidator2.ErrorMessage = "Please select customer";



                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        #endregion
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerList.SelectedItem.Text == "< Select Customer >")
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        protected void btnChangeRole_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divChengeRolesDialog\").dialog('open')", true);
        }
        protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {               
                BindUserData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxUserFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindUserData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindUserData()
        {
            try
            {
                grdUserRole.DataSource = null;
                grdUserRole.DataBind();               
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int ddlModulevalue = Convert.ToInt32(ddlModule.SelectedValue);                
                var uselist = UserManagement.GetUserRollWise(customerID, ddlModulevalue, tbxUserFilter.Text);
                grdUserRole.DataSource = uselist;
                grdUserRole.DataBind();
        
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void ddlHeaderRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string ddlHeader = (((DropDownList)sender).SelectedValue);
                for (int i = 0; i < grdUserRole.Rows.Count; i++)
                {
                    DropDownList ddlRole = (DropDownList)grdUserRole.Rows[i].FindControl("ddlRole");                                   
                    ddlRole.SelectedValue = (((DropDownList)sender).SelectedValue);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        private void BindUserRoles(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                var roles = RoleManagement.GetAllRoles();

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                ddlUserList.DataSource = roles.OrderBy(entry => entry.Name);
                ddlUserList.DataBind();
                ddlUserList.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void GrdUserRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DropDownList ddlHeardeRole = (DropDownList)e.Row.Cells[1].FindControl("ddlHeaderRole");
                BindUserRoles(ddlHeardeRole);
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlRole = (DropDownList)e.Row.FindControl("ddlRole");               
                BindUserRoles(ddlRole);
            }
            

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int ddlModuleValue = Convert.ToInt32(ddlModule.SelectedValue);
                string ddlModuleName = Convert.ToString(ddlModule.SelectedItem);
                if (grdUserRole.Rows.Count > 0)
                {
                    foreach (GridViewRow row in grdUserRole.Rows)
                    {
                        int UserID1 = Convert.ToInt32(((Label)row.FindControl("lblID")).Text);                        
                        int ddlRoles = Convert.ToInt32(((DropDownList)row.FindControl("ddlRole")).SelectedValue);
                        if (ddlRoles != -1)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                            var UserRecordToUpdate = (from rows in entities.Users
                                                      where rows.ID == UserID1
                                                      select rows).FirstOrDefault();

                            if (UserRecordToUpdate != null)
                            {
                                if (ddlRoles != -1)
                                {
                                    if (ddlModuleValue == 1)
                                    {
                                        UserRecordToUpdate.RoleID = ddlRoles;
                                    }
                                    else if (ddlModuleValue == 2)
                                    {
                                        UserRecordToUpdate.LitigationRoleID = ddlRoles;
                                    }
                                    else if (ddlModuleValue == 3)
                                    {
                                        UserRecordToUpdate.ContractRoleID = ddlRoles;
                                    }
                                    else if (ddlModuleValue == 4)
                                    {
                                        UserRecordToUpdate.LicenseRoleID = ddlRoles;
                                    }
                                    else
                                    {
                                        UserRecordToUpdate.LitigationRoleID = null;
                                        UserRecordToUpdate.ContractRoleID = null;
                                        UserRecordToUpdate.LicenseRoleID = null;
                                    }
                                    entities.SaveChanges();
                                    saveSuccess = true;
                                }
                            }
                        }
                    }else
                       {
                        BindUserData();
                        saveSuccess = false;
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Role";
                        CustomValidator4.CssClass = "alert alert-danger";
                    }
                }
                }
                
                if(saveSuccess)
                {
                    BindUserData();
                    CustomValidator2.IsValid = false;
                    CustomValidator2.ErrorMessage = "Roles assigned Successfully";
                    CustomValidator4.CssClass = "alert alert-success";
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divChengeRolesDialog\").dialog('close')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator2.IsValid = false;
                CustomValidator2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomers(int UserID)
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

               
                if(AuthenticationHelper.Role=="IMPT")
                {
                    ddlCustomerList.DataSource = CustomerManagement.GetAllCustomer(UserID, "");
                }
                else
                {
                    int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomerList.DataSource = Assigncustomer.GetAllCustomerData(customerID);
                    ddlCustomerList.SelectedValue = Convert.ToString(customerID);
                    ddlCustomerList.DataBind();
                }

                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));                
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    if (ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
                if (AuthenticationHelper.Role == "MGMT")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    if (ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Statutory
        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdComplianceInstances.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
                {
                    BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (customerID != -1)
                {
                    List<Sp_ComplianceAssignedInstance_Result> assignmentList = new List<Sp_ComplianceAssignedInstance_Result>();
                    if (chkEvent.Checked == true)
                    {
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("Y", customerID, Convert.ToInt32(ViewState["UserID"]), "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    else if (chkCheckList.Checked == true)
                    {
                        chkEvent.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("NA", customerID, Convert.ToInt32(ViewState["UserID"]), "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        chkEvent.Checked = false;
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("N", customerID, Convert.ToInt32(ViewState["UserID"]), "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    if (direction == SortDirection.Ascending)
                    {
                        assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }
                    foreach (DataControlField field in grdComplianceInstances.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["InstancesSortIndex"] = grdComplianceInstances.Columns.IndexOf(field);
                        }
                    }
                    grdComplianceInstances.DataSource = assignmentList;
                    grdComplianceInstances.DataBind();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Something went wrong, Please check customer selection & try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void CheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;
                //string role = ((Label) gvrow.FindControl("lblRole")).Text;
                string role = "Performer";
                if (ViewState["AssignedCompliancesID"] != null)
                    chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedCompliancesID"] = chkList;
        }
        private void AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    //string role = ((Label) gvrow.FindControl("lblRole")).Text;
                    string role = "Performer";
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        #endregion
              
        protected void rbtModifyAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string action = Convert.ToString(rbtModifyAction.SelectedValue);
                if (action.Equals("Delete"))
                {
                    divNewUser.Visible = false;
                }
                else
                {
                    divNewUser.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void txtAssigmnetFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                int userID = Convert.ToInt32(ViewState["UserID"].ToString());
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceInstances(userID);
                }
                else
                {
                    BindInternalComplianceInstances(userID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Event
        private void BindEventInstances(int userID)
        {
            try
            {
                grdAssignedEventInstance.DataSource = EventManagement.GetAllAssignedInstancesByUser(userID, filter: txtAssignEventUser.Text).OrderBy(entry => entry.CustomerBranchName).Distinct().ToList();
                grdAssignedEventInstance.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upAssignEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdAssignedEventInstance_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var assignmentList = EventManagement.GetAllAssignedInstancesByUser(Convert.ToInt32(ViewState["UserID"])).OrderBy(entry => entry.CustomerBranchName).ToList();
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAssignedEventInstance.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["InstancesSortIndex"] = grdAssignedEventInstance.Columns.IndexOf(field);
                    }
                }

                grdAssignedEventInstance.DataSource = assignmentList;
                grdAssignedEventInstance.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAssignedEventInstance_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAssignedEventInstance_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                EventCheckBoxValueSaved();
                grdAssignedEventInstance.PageIndex = e.NewPageIndex;
                BindEventInstances(Convert.ToInt32(ViewState["UserID"]));
                EventSavedCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void EventCheckBoxValueSaved()
        {
            List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdAssignedEventInstance.Rows)
            {
                index = Convert.ToInt32(grdAssignedEventInstance.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox) gvrow.FindControl("chkEvent")).Checked;
                int role = Convert.ToInt32(((Label) gvrow.FindControl("lblRoleId")).Text);
                
                if (ViewState["AssignedEventID"] != null)
                    chkList = (List<Tuple<int, int>>) ViewState["AssignedEventID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2 == role).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, int>(index, role));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedEventID"] = chkList;
        }
        private void EventSavedCheckBoxexValue()
        {

            List<Tuple<int, int>> chkList = (List<Tuple<int, int>>) ViewState["AssignedEventID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdAssignedEventInstance.Rows)
                {
                    int index = Convert.ToInt32(grdAssignedEventInstance.DataKeys[gvrow.RowIndex].Value);
                    int role = Convert.ToInt32(((Label) gvrow.FindControl("lblRoleId")).Text);
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2 == role).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkEvent");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void btnAssignEvents_Click(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    EventCheckBoxValueSaved();
                    
                    List<Tuple<int, int>> chkList = (List<Tuple<int, int>>) ViewState["AssignedEventID"];
                    if (chkList != null)
                    {
                            string action = Convert.ToString(rbtEventModifyAssignment.SelectedValue);
                            if (action.Equals("Delete"))
                            {

                                Business.EventManagement.DeleteEventAssignment(Convert.ToInt32(ViewState["UserID"]), chkList);
                                var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                            string accessURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(oldUser.CustomerID));
                            if (Urloutput != null)
                            {
                                accessURL = Urloutput.URL;
                            }
                            else
                            {
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                {
                                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeEvents
                                                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                        .Replace("@URL", Convert.ToString(accessURL))
                                                        .Replace("@From", ReplyEmailAddressName);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding events.", message); }).Start();
                                }
                            }
                            else
                            {
                                EventManagement.ReassignEvents(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlAllUsers.SelectedValue), chkList, AuthenticationHelper.UserID);
                                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlAllUsers.SelectedValue));
                                var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                string accessURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(newUser.CustomerID));
                                if (Urloutput != null)
                                {
                                    accessURL = Urloutput.URL;
                                }
                                else
                                {
                                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                            if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                {
                                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignEventNewOwner
                                                        .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                                        .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                                        .Replace("@URL", Convert.ToString(accessURL))
                                                        .Replace("@From", ReplyEmailAddressName);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                }


                                if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                {
                                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignEventOldOwner
                                                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                        .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                                        .Replace("@URL", Convert.ToString(accessURL))
                                                        .Replace("@From", ReplyEmailAddressName);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
                                }
                            }


                            BindUsers();
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                            ViewState["AssignedEventID"] = null;
                        
                    }
                    else
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Please select at list one event for proceed.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtAssignEventsFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                int userID = Convert.ToInt32(ViewState["UserID"].ToString());
                BindEventInstances(userID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void rbtEventModifyAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string action = Convert.ToString(rbtEventModifyAssignment.SelectedValue);
                if (action.Equals("Delete"))
                {
                    divEventNewUser.Visible = false;
                }
                else
                {
                    divEventNewUser.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rbtSelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ViewState["UserID"].ToString());
            if (rbtSelectionType.SelectedValue == "0")
            {
                BindComplianceInstances(userID);
            }
            else
            {
                BindInternalComplianceInstances(userID);
            }
        }
        #endregion

        
        #region  internal
        private void InternalCheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkIList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox) gvrow.FindControl("chkICompliances")).Checked;
                //string role = ((Label) gvrow.FindControl("lblIRole")).Text;
                string role = "Performer";

                if (ViewState["AssignedInternalCompliancesID"] != null)
                    chkIList = (List<Tuple<long, string>>) ViewState["AssignedInternalCompliancesID"];

                var checkedData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkIList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkIList.Remove(checkedData);
            }
            if (chkIList != null && chkIList.Count > 0)
                ViewState["AssignedInternalCompliancesID"] = chkIList;
        }
        private void InternalAssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>) ViewState["AssignedInternalCompliancesID"];
            if (chkIList != null && chkIList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    //string role = ((Label) gvrow.FindControl("lblIRole")).Text;
                    string role = "Performer";
                    var checkedIData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkIList.Contains(checkedIData))
                    {
                        CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkICompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdInternalComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                InternalCheckBoxValueSaved();
                grdInternalComplianceInstances.PageIndex = e.NewPageIndex;                
                BindInternalComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                InternalAssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdInternalComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {                    
                    int sortColumnIndex = Convert.ToInt32(ViewState["InternalInstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdInternalComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (customerID != -1)
                {
                    if (chkCheckList.Checked == true)
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstances("Y", customerID,Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();

                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }

                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }

                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstances("N",customerID, Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }
                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }
                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void rbtSelectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ViewState["UserID"].ToString());
            if (rbtSelectionType.SelectedValue == "0")
            {
                if (rbtSelectType.SelectedValue == "0")
                {
                    divEventBase.Visible = true;
                    BindComplianceInstances(userID);
                }
                else
                {
                    divEventBase.Visible = false;
                    BindComplianceInstances(userID);
                }
            }
            else
            {
                BindInternalComplianceInstances(userID);
            }

            
        }
        #region Task
        private void Task_CheckBoxValueSaved()
        {
            List<Tuple<long, int>> chkList = new List<Tuple<long, int>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdTask.Rows)
            {
                index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkTask")).Checked;                
                int role = 3;                
                if (ViewState["AssignedTaskID"] != null)
                    chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, int>(index, Convert.ToInt32(role)));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedTaskID"] = chkList;
        }
        private void Task_AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedTaskID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdTask.Rows)
                {
                    int index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);                    
                    int role = 3;
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkTask");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdTask_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void grdTask_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }
        protected void grdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Task_CheckBoxValueSaved();
                grdTask.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["UserID"])))
                {
                    BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
                }
                Task_AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void Btnsaveuniversaldoc_Click(object sender, EventArgs e)
        {
            bool flagvalidation = true;
            if (gridviewfolder != null)
            {
                for (int i = 0; i < gridviewfolder.Rows.Count; i++)
                {
                    if (gridviewfolder.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        DropDownList ddluser = (DropDownList)gridviewfolder.Rows[i].FindControl("ddlUserlist");
                        if (ddluser.SelectedValue == "" || ddluser.SelectedValue == "-1" || ddluser.SelectedValue == "0")
                        {
                            flagvalidation = false;
                        }
                    }
                }
            }
            if (flagvalidation)
            {
                bool success = false;
                int UID = Convert.ToInt32(txtuserid.Text);
                for (int i = 0; i < gridviewfolder.Rows.Count; i++)
                {
                    if (gridviewfolder.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        Label txtboxfolderid = (Label)gridviewfolder.Rows[i].FindControl("lblFolderID");
                        DropDownList ddlNewuser = (DropDownList)gridviewfolder.Rows[i].FindControl("ddlUserlist");
                        int NewUID = -1;
                        int FolderID = -1;
                        if (ddlNewuser.SelectedValue != "" && ddlNewuser.SelectedValue != "-1" && ddlNewuser.SelectedValue != "0")
                        {
                            NewUID = Convert.ToInt32(ddlNewuser.SelectedValue);
                        }
                        if (txtboxfolderid.Text != null && txtboxfolderid.Text != "")
                        {
                            FolderID = Convert.ToInt32(txtboxfolderid.Text);
                        }
                        if (FolderID > 0 && NewUID > 0)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                #region Folder id update
                                var AllFolderDetail = (from row in entities.sp_AllFolder((int)FolderID)
                                                       select row).ToList();

                                foreach (var item1 in AllFolderDetail)
                                {
                                    Mst_FolderMaster obj1 = (from row in entities.Mst_FolderMaster
                                                             where row.CreatedBy == UID
                                                             && row.ID == item1
                                                             select row).FirstOrDefault();
                                    if (obj1 != null)
                                    {
                                        if (obj1.CreatedBy == UID)
                                            obj1.CreatedBy = NewUID;

                                        if (obj1.UpdatedBy == UID)
                                            obj1.UpdatedBy = NewUID;

                                        entities.SaveChanges();
                                    }

                                    var obj2 = (from row in entities.UserFolderPermissions
                                                where row.FolderID == item1
                                                select row).ToList();
                                    foreach (var item3 in obj2)
                                    {
                                        UserFolderPermission obj12 = (from row in entities.UserFolderPermissions
                                                                      where row.ID == item3.ID
                                                                      select row).FirstOrDefault();
                                        if (obj12 != null)
                                        {
                                            if (obj12.CreatedBy == UID)
                                            {
                                                obj12.CreatedBy = NewUID;
                                            }
                                            if (obj12.UserID == UID)
                                            {
                                                obj12.UserID = NewUID;
                                            }
                                            if (obj12.SubShareUserID == UID)
                                            {
                                                obj12.SubShareUserID = NewUID;
                                            }
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion

                                #region File Id Update
                                List<long> FileIDs = (from row in entities.sp_AllfilesofFolder((int)FolderID)
                                                      select (long)row).ToList();
                                foreach (var itemFDs in FileIDs)
                                {
                                    FolderFileData c = (from row in entities.FolderFileDatas
                                                        where row.ID == itemFDs
                                                        select row).FirstOrDefault();
                                    if (c != null)
                                    {
                                        if (c.CreatedBy == UID)
                                        {
                                            c.CreatedBy = NewUID;
                                        }
                                        if (c.UpdatedBy == UID)
                                        {
                                            c.UpdatedBy = NewUID;
                                        }
                                        entities.SaveChanges();
                                    }
                                }
                                #endregion

                                UniverseUserAssignment ass = new UniverseUserAssignment()
                                {
                                    OldUserID = UID,
                                    NewUserID = NewUID,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now,
                                    FolderID = FolderID
                                };
                                entities.UniverseUserAssignments.Add(ass);
                                entities.SaveChanges();
                                success = true;
                            }
                        }
                    }
                }
                if (success)
                {                    
                    UserManagement.Delete(UID);
                    UserManagementRisk.Delete(UID);

                    BindUsers();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fclosepopup1();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. please select user for re-assign to other user.');", true);
            }
        }

        public void BindFolderData(int UID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SADMN")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    gridviewfolder.DataSource = null;
                    gridviewfolder.DataBind();


                    var MainFolderDetail = (from row in entities.Mst_FolderMaster
                                            where row.IsUniverse == 1
                                            && row.ParentID == null
                                            && row.CreatedBy == UID
                                            && row.CustomerID == customerID
                                            select row).ToList();

                    gridviewfolder.DataSource = MainFolderDetail;
                    gridviewfolder.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridviewfolder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DropDownList ddlHeaderUser = (DropDownList)e.Row.Cells[1].FindControl("ddlHeaderUser");
                BindUserList(ddlHeaderUser);
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlUserlist = (DropDownList)e.Row.FindControl("ddlUserlist");
                BindUserList(ddlUserlist);
            }
        }

        protected void ddlHeaderUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string ddlHeader = (((DropDownList)sender).SelectedValue);
                for (int i = 0; i < gridviewfolder.Rows.Count; i++)
                {
                    DropDownList ddlUserlist = (DropDownList)gridviewfolder.Rows[i].FindControl("ddlUserlist");
                    ddlUserlist.SelectedValue = (((DropDownList)sender).SelectedValue);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindUserList(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SADMN")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var lstAllUsers = GetAllUsers(customerID, Convert.ToInt32(txtuserid.Text));

                var lstUsers = (from row in lstAllUsers
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                if (lstUsers != null)
                {
                    ddlUserList.DataSource = lstUsers;
                    ddlUserList.DataBind();
                }

                ddlUserList.Items.Insert(0, new ListItem("< Select user >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}