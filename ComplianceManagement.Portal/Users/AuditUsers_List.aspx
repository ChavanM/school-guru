﻿<%@ Page Title="User List" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
    CodeBehind="AuditUsers_List.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.AuditUsers_List" %>

<%@ Register Src="~/Controls/AuditUserDetailsControl.ascx" TagName="AuditUserDetailsControl"
    TagPrefix="vit" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">

        function remoDisabledclass() {
            $('a.aspNetDisabled').removeClass('aspNetDisabled');
        };

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").unbind('click');
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").click(function () {

                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });
        function txtclick() {

            $("#divBranches").toggle("blind", null, 500, function () { });

        }

        function CloseEvent() {
            $('#divReAssign').modal('hide');
            window.location.reload();
        };


        $(document).ready(function () {
            fhead('Users');
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <%-- <section class="panel">--%>
                <div style="margin: 5px">
                    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional" OnLoad="upUserList_Load">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" style="width: 23%">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" class="form-control m-bot15"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please select Location."
                                    ControlToValidate="ddlCustomerList" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                    Display="None" />
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;" runat="server" id="divCustomerfilter">
                                <div class="col-md-2 colpadding0 entrycount">
                                    <label style="font-size: 13px; color: #999; margin-top: 5px;">&nbsp;</label>
                                </div>
                                <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="200px" OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="text-align: right">
                                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" AutoPostBack="false" Style="margin-top: 5px;" OnClientClick="openModalControl();" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                            </div>
                            <div class="panel" style="margin-bottom: 4px;">
                                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true"
                                    CssClass="table" GridLines="none" OnRowCreated="grdUser_RowCreated" PageSize="5" OnSorting="grdUser_Sorting" Width="100%"
                                    DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging" OnRowDataBound="grdUser_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />

                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />

                                        <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ContactNumber" HeaderText="Contact" ItemStyle-Width="100px" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                        <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Width="100px" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                                    ToolTip="Click to toggle the status..." data-toggle="tooltip" data-placement="top" OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Modify Assignment">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:LinkButton ID="lbtnReassignment" runat="server" CommandName="MODIFY_AUDIT_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>' OnClientClick="OpendivReAssign();"
                                                        ToolTip="Click to Modify assignments for this user..." data-toggle="tooltip" data-placement="top">Re-Assignment</asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                    <asp:LinkButton runat="server" ToolTip="Change user details" data-toggle="tooltip" data-placement="top" CommandName="EDIT_USER" OnClientClick="openModalControl()" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon_new.png"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="DELETE_USER" ToolTip="Remove user" data-toggle="tooltip" data-placement="top" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                                        OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../Images/delete_icon_new.png"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                                        OnClientClick="return confirm('Are you certain you want to reset password for this user?');" ToolTip="Change user password" data-toggle="tooltip" data-placement="top"><img src="../Images/reset_password_new.png"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="UNLOCK_USER" ToolTip="Unlock user" data-toggle="tooltip" data-placement="top" CommandArgument='<%# Eval("ID") %>'
                                                        Visible='<%# IsLocked((string)Eval("Email")) %>'><img src="../Images/permissions_icon.png"/></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                </asp:GridView>
                                <div style="float: right;">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
         
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" style="width: 650px; height: 850px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <vit:AuditUserDetailsControl runat="server" ID="udcInputForm" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- </section>--%>
            </div>
        </div>
    </div>

    <div id="divReAssign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 750px; height: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">×</button>
                </div>

                <div class="modal-body">
                    <asp:UpdatePanel ID="ReAssign" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 ">
                                <%--<section class="panel"> --%>
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="VSReassignSummary" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ReAssignValidationSummary" />
                                     <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                    ValidationGroup="ReAssignValidationSummary" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 142px; margin-top: 10px;">
                                    <label style="width: 15px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>

                                    <asp:RadioButtonList ID="rbtEventReAssignment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtEventReAssignment_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Process" Value="Process" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Implementation" Value="Implementation"></asp:ListItem>
                                        <asp:ListItem Text="ICFR" Value="Internal Financial Control"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div style="margin-bottom: 10px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Current User</label>
                                    <asp:Label ID="lblReAssign" runat="server" Style="color: #333;" />
                                </div>
                                <div style="margin-bottom: 7px" id="div1" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        New User</label>
                                    <asp:DropDownList runat="server" ID="ddlREAssignUser" Style="width: 200px;" AutoPostBack="false"
                                        CssClass="form-control m-bot15" />
                                    <asp:CompareValidator ErrorMessage="Please select New User." ControlToValidate="ddlREAssignUser"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ReAssignValidationSummary"
                                        Display="None" />
                                </div>
                                <div style="margin-bottom: 7px" id="div5" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Process</label>
                                    <asp:DropDownList runat="server" ID="ddlProcess" Style="width: 200px;" AutoPostBack="true" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged"
                                        CssClass="form-control m-bot15" />
                                </div>
                                <div style="margin-bottom: 5px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                                    <label style="font-size: 13px; width: 150px; color: #333; display: block; float: left;">
                                        Filter
                                    </label>
                                    <asp:TextBox runat="server" ID="txtReAssignFilter" Width="200px" MaxLength="50" AutoPostBack="true" CssClass="form-control" OnTextChanged="txtReAssignFilter_TextChanged" />
                                </div>
                                <div style="margin-bottom: 5px; margin-right: 10%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                                    <label style="font-size: 13px; width: 50px; color: #333; display: block; float: left; margin-right: 100px;">
                                        Show
                                    </label>
                                    <asp:DropDownList runat="server" ID="ddlPageSizeReAssign" class="form-control m-bot15" Style="width: 70px; float: left" OnSelectedIndexChanged="ddlPageSizeReAssign_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                                <div runat="server" id="div2" style="margin-bottom: 7px; width: 100%; height: 300px; overflow-y: auto;">
                                    <asp:GridView runat="server" ID="grdReassign" AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                                        GridLines="Horizontal" AllowSorting="true" Width="100%" CssClass="table" DataKeyNames="ProcessId" ShowFooter="false"
                                        OnPageIndexChanging="grdReassign_PageIndexChanging" OnRowCreated="grdReassign_RowCreated" OnSorting="grdReassign_Sorting">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkEventHeader" runat="server" onclick="javascript:SelectheaderCheckboxesReAssging(this)" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkEvent" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CustomerBranchID" Visible="false">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                        <asp:Label ID="lblBrachID" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBranchID") %>' ToolTip='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                        <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Financial Year">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                        <asp:Label ID="Label4" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                        <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                        <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0" style="margin: 5px">
                                    <div class="col-md-6 colpadding0" style="margin: auto; text-align: center;">
                                        <asp:Button Text="Save" runat="server" ID="btnReassign" OnClick="btnReassign_Click"
                                            CssClass="btn btn-primary" ValidationGroup="ReAssignValidationSummary" OnClientClick="ConfirmEvent();" />
                                        <asp:Button Text="Close" runat="server" ID="btnReassignClose" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseEvent()" />
                                    </div>

                                    <div class="col-md-6 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:ImageButton ID="lBPreviousReAssign" CssClass="table-paging-left" runat="server"
                                                ImageUrl="~/img/paging-left-active.png" OnClick="lBPreviousReAssign_Click" />
                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNoReAssign" runat="server" Text=""></asp:Label>/
                                                    <asp:Label ID="lTotalCountReAssign" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNextReAssign" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNextReAssign_Click" />
                                            <asp:HiddenField ID="TotalRowsReAssign" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>


                                <%-- 
            </section>--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        //$(document).ready(function () {
        //    setactivemenu('Users');
        //    fhead('Users');
        //});

        function openModalControl() {
            $('#divCustomersDialog').modal('show');
            return true;
        }


        function caller() {
            setInterval(CloseWin, 3000);
        };

        function OpendivAssignEvent() {
            $('#divAssignEvent').modal('show');
        }

        function OpendivReAssign() {
            $('#divReAssign').modal('show');
        }



        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

      

        function SelectheaderCheckboxesReAssging(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");

            var gvcheck = null;
            if (chkheaderid[1] == "grdReassign")
                gvcheck = document.getElementById("<%=grdReassign.ClientID %>");
            else
                gvcheck = document.getElementById("<%=grdReassign.ClientID %>");

            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }


        function ConfirmEvent() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_Event_value";
            var oldUser = $("#<%=lblReAssign.ClientID%>").html();
            var newUser = $("#<%=ddlREAssignUser.ClientID%> option:selected").text();
            var type = $("#<%=rbtEventReAssignment.ClientID%> input:checked").val();

            if (type == "Reassign") {
                if (newUser != "< Select user >") {
                    if (confirm("Are you sure you want reassign selected Event to " + newUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
            } else {
                if (confirm("Are you sure you want exclude selected Event to " + oldUser + "?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
            }

            document.forms[0].appendChild(confirm_value);
        }

    </script>
</asp:Content>
