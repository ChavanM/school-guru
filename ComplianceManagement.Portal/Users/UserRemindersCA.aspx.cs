﻿using System;
using System.Web.UI;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class UserRemindersCA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindUserReminders();
                BindInputLists();
                tbxDate.Attributes.Add("readonly", "readonly");
            }
           
        }

        protected void upUserReminders_Load(object sender, EventArgs e)
        {
            try
            {
                //if (ViewState["AssignedCompliances"] != null)//commented by Manisha
                //{
                    List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];

                    DateTime date = DateTime.MinValue;
                    if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    }

                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindInputLists()
        {
            try
            {
                List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                //ViewState["AssignedCompliances"] = assignedCompliances;//commented by manisha

                //if ((assignedCompliances != null) && (assignedCompliances.Count > 0))
                //{
                //    ViewState["AssignedCompliances"] = assignedCompliances;//added by Manisha and commented by Manisha
                //}

                ddlBranch.DataSource = assignedCompliances.Select(entry => new { ID = entry.CustomerBranchID, Name = entry.Branch }).Distinct().OrderBy(entry => entry.Name).ToList();
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlBranch_SelectedIndexChanged(null, null);

                ////--------added by Manisha-------------------------------------------------------------------------
                //ddlCompliance.DataSource = assignedCompliances.Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlCompliance.DataBind();
                //ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));

                //ddlCompliance_SelectedIndexChanged(null, null);
                ////---------added by Manisha-----------------------------------------------------------
                //ddlRole.DataSource = assignedCompliances.Select(entry => new { ID = entry.RoleID, Name = entry.Role }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlRole.DataBind();
                //ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));
                              

                //----------------------------------------------------------------------------------
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ViewState["AssignedCompliances"] != null)//commented by Manisha
                //{
                    //List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                    ddlCompliance.ClearSelection();
                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    ddlCompliance.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription })
                        .OrderBy(entry => entry.Name).Distinct().ToList();
                    ddlCompliance.DataBind();

                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlCompliance_SelectedIndexChanged(null, null);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];
                List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                ddlRole.ClearSelection();
                ddlRole.DataSource = null;
                ddlRole.DataBind();

                //if (ViewState["AssignedCompliances"] != null)//added by manisha
                if (assignedCompliances != null)//added by manisha
                {
                    if (assignedCompliances.Count > 0)//added by manisha
                    {
                        if ((ddlCompliance.SelectedValue != null) && (ddlCompliance.SelectedValue != string.Empty) && (ddlCompliance.SelectedValue != "-1")) //added by manisha
                        {
                            ddlRole.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue)).Select(entry => new { ID = entry.RoleID, Name = entry.Role })
                                .OrderBy(entry => entry.Name).Distinct().ToList();
                            ddlRole.DataBind();

                            ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                            DateTime scheduledOn = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => entry.ScheduledOn).FirstOrDefault();
                            if (scheduledOn != DateTime.MinValue)
                            {
                                litScheduledOn.Text = scheduledOn == null ? string.Empty : scheduledOn.ToString("dd-MM-yyyy");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        private void BindUserReminders()
        {
            try
            {
                grdUserReminder.DataSource = Business.ComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();
                grdUserReminder.DataBind();

                upUserReminderList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserReminder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userReminderID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER_REMINDER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["UserReminderID"] = userReminderID;

                    var userReminder = Business.ComplianceManagement.GetComplianceReminderNotificationsByID(userReminderID);

                    ddlBranch.SelectedValue = userReminder.CustomerBranchID.ToString();
                    ddlBranch_SelectedIndexChanged(null, null);

                    ddlCompliance.SelectedValue = userReminder.ComplianceID.ToString();
                    ddlCompliance_SelectedIndexChanged(null, null);

                    ddlRole.SelectedValue = userReminder.RoleID.ToString();
                    tbxDate.Text = userReminder.RemindOn.ToString("dd-MM-yyyy");

                    ////---------added by Manisha------------------------------------
                    //litScheduledOn.Text = userReminder.ScheduledOn.ToString("dd-MM-yyyy");
                    ////-------------------------------------------------------------

                    upUserReminders.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserRemindersDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_USER_REMINDER"))
                {
                    UserManagement.DeleteUserReminder(userReminderID);
                    BindUserReminders();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserReminder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUserReminder.PageIndex = e.NewPageIndex;
                BindUserReminders();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddUserReminder_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
              
                ddlBranch.SelectedValue = "-1";
                ddlBranch_SelectedIndexChanged(null, null);

                ddlCompliance.SelectedValue = "-1";
                ddlCompliance_SelectedIndexChanged(null, null);

                tbxDate.Text = string.Empty;
                litScheduledOn.Text = string.Empty;
                upUserReminders.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUserRemindersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUserReminder.PageIndex = 0;
                BindUserReminders();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];
                List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                var assignment = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.RoleID == Convert.ToInt32(ddlRole.SelectedValue)).FirstOrDefault();

                ComplianceReminder reminder = new ComplianceReminder()
                {
                    ComplianceDueDate = (DateTime)assignment.ScheduledOn,
                    RemindOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    ComplianceAssignmentID = assignment.ComplianceAssignmentID
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    reminder.ID = Convert.ToInt32(ViewState["UserReminderID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    Business.ComplianceManagement.CreateReminder(reminder);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    Business.ComplianceManagement.UpdateReminder(reminder);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divUserRemindersDialog\").dialog('close')", true);
                BindUserReminders();

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdUserReminder_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var reminderList = Business.ComplianceManagement.GetComplianceReminderNotificationsByUserID(AuthenticationHelper.UserID, tbxFilter.Text).ToList();
                if (direction == SortDirection.Ascending)
                {
                    reminderList = reminderList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    reminderList = reminderList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdUserReminder.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUserReminder.Columns.IndexOf(field);
                    }
                }

                grdUserReminder.DataSource = reminderList;
                grdUserReminder.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserReminder_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

    }
}