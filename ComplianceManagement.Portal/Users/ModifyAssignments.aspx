﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ModifyAssignments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.ModifyAssignments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function fstatus(e) {
             
            window.open("/Users/PerformerReviewerStatus.aspx?CID=" + $(e).attr('Data-attr'), " mywindow", "resizable=1,width=650,height=250,top=300");
        }
        
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function UncheckHeaderCustomer() {
            var rowCheckBox = $("#RepeaterSubTable input[id*='chkApprover']");
            var rowCheckBoxSelected = $("#RepeaterSubTable input[id*='chkApprover']:checked");
            var rowCheckBoxHeader = $("#RepeaterSubTable input[id*='ApproverSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllApprover(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkApprover") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function initializeJQueryUI1(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
     function OnTreeClick(evt) {            
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            <%--var oldUser = $("#<%=litCurrentUser.ClientID%>").html();--%>            
            var oldUser = $("#<%=ddlUserList.ClientID%> option:selected").text();
            var newUser = $("#<%=ddlNewUsers.ClientID%> option:selected").text();
            var ReviewerUser = $("#<%=ddlNewReviewerUsers.ClientID%> option:selected").text();
            <%--var ApproverUser = $("#<%=ddlNewApproverUsers.ClientID%> option:selected").text();--%>
            var type = $("#<%=rbtModifyAction.ClientID%> input:checked").val();
            var Comtype = $("#<%=rbtSelectType.ClientID%> input:checked").val();           
            //if (Comtype == "Compliance") {
            if (Comtype == 0) {
                if (type == "Reassign") {
                    if (newUser != "< Select user >") {                        
                        if (confirm("Are you sure you want reassign selected compliances to " + newUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                    else if (ReviewerUser != "< Select user >") {                        
                        if (confirm("Are you sure you want reassign selected compliances to " + ReviewerUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                    else if (ApproverUser != "< Select user >") {
                        if (confirm("Are you sure you want reassign selected compliances to " + ApproverUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                } else {

                    if (confirm("Are you sure you want exclude selected compliances for " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
                document.forms[0].appendChild(confirm_value);
            }
            else {
                if (type == "Reassign") {
                    if (newUser != "< Select user >") {
                        
                        if (confirm("Are you sure you want reassign selected compliance tasks to " + newUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                    else if (ReviewerUser != "< Select user >") {                        
                        if (confirm("Are you sure you want reassign selected compliances to " + ReviewerUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                    else if (ApproverUser != "< Select user >") {
                        if (confirm("Are you sure you want reassign selected compliances to " + ApproverUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                } else {

                    if (confirm("Are you sure you want exclude selected compliance tasks for " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
                document.forms[0].appendChild(confirm_value);
            }
        }

        function SelectTaskheaderCheckboxes(chkTaskHeader) {
            var rolecolumn;
            var chkheaderid = chkTaskHeader.id.split("_");

            var Taskgvcheck = null;
            if (chkheaderid[1] == "grdTask")
                Taskgvcheck = document.getElementById("<%=grdTask.ClientID %>");
            else
                Taskgvcheck = document.getElementById("<%=grdTask.ClientID %>");

            var i;

            if (chkTaskHeader.checked) {
                for (i = 0; i < Taskgvcheck.rows.length; i++) {
                    Taskgvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = chkTaskHeader.checked;
                }
            }
            else {
                for (i = 0; i < Taskgvcheck.rows.length; i++) {
                    Taskgvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = chkTaskHeader.checked;
                }
            }
        }

        function SelectheaderICheckboxes(Iheaderchk) {
             var rolecolumn;
             var chkIheaderid = Iheaderchk.id.split("_");

             var Igvcheck = null;
             if (chkIheaderid[1] == "grdInternalComplianceInstances")
                 Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");
            else
                Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");

            var i;

            if (Iheaderchk.checked) {
                for (i = 0; i < Igvcheck.rows.length; i++) {
                    Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                }
            }

            else {
                for (i = 0; i < Igvcheck.rows.length; i++) {
                    Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                }
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            var gvcheck = null;
            gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
            <%--if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
             else
                 gvcheck = document.getElementById("<%=grdAssignedEventInstance.ClientID %>");--%>

             var i;
             if (headerchk.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                 }
             }
         }
        
    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any(one or more) child not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upModifyAssignment" runat="server" UpdateMode="Conditional">
            <ContentTemplate> 

      <div style="margin: 5px">
                    <table style="width:100%;">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="ModifyAsignmentValidationGroup" />
                                    <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                        ValidationGroup="ModifyAsignmentValidationGroup" Display="None" />
                                </div>
                            </td>
                        </tr>
                           <tr>
                            <td colspan="2">
                                <div id="DivType" style="margin-bottom: 7px; margin-left: 0px;" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtSelectType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtSelectType_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Compliance" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Task" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                            <div style="margin-bottom: 7px;" runat="server" id="divEventBase">                                
                                <td style="width:35%;" colspan="2">
                                    <div style="float: left;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Event Based</label>
                                        <asp:CheckBox runat="server" ID="chkEvent" AutoPostBack="true" OnCheckedChanged="chkEvent_CheckedChanged" style="margin-left: -21px;" />

                                    </div>

                                     <div style="float: left;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;margin-left: 15px;">
                                            CheckList</label>
                                        <asp:CheckBox runat="server" ID="chkCheckList" AutoPostBack="true" OnCheckedChanged="chkCheckList_CheckedChanged" style="margin-left: -37px;" />

                                    </div>
                                </td>
                            </div>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div id="divSelectionType" style="margin-bottom: 7px; margin-left: 0px;" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtSelectionType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtSelectionType_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Statutory" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Internal" Value="1" style="margin-left: 13px;" ></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="margin-bottom: 7px; margin-left: 0px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtModifyAction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtModifyAction_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Reassign" Value="Reassign" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Exclude" Value="Delete" style="margin-left: 15px;"></asp:ListItem>
                                        <asp:ListItem Text="Approver Exclude" Value="ApproverDelete" style="margin-left: 15px;"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                            <td></td>
                         
                        </tr>

                            <tr>
                            <div style="margin-bottom: 7px;" id="div1" runat="server">
                                <td style="width:15%;">
                             <div id="lblcustomer" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Customer
                            </label>
                                 </div>
                        </td>
                        <td style="width:35%;">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please Select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        Select Location</label>
                                </td>
                                <td style="width:35%;">
                                   <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 18px; width: 384px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation"  ShowCheckBoxes="All" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true"   onclick="OnTreeClick(event)"  >
                                </asp:TreeView>
                               
                            </div> <asp:Button ID="btnlocation" runat="server" onclick="tvFilterLocation_SelectedNodeChanged" Text="Select"/> 
                                </td>
                            </div>
                        </tr>

                             <tr>
                            <div style="margin-bottom: 7px;" id="div2" runat="server">
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        Select User</label>
                                </td>
                                <td style="width:35%;">
                                      <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlUserList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" 
                                           OnSelectedIndexChanged="ddlUserList_SelectedIndexChanged"
                                        CssClass="txtbox" />
                                </td>
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Filter  </label>                                     
                                </td>
                                <td style="width:35%;">
                                  
                                    <asp:TextBox runat="server" ID="txtAssigmnetFilter" Width="380px" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="txtAssigmnetFilter_TextChanged" />
                                </td>
                            </div>
                        </tr>
                         <div style="margin-bottom: 7px;" id="divNewUser" runat="server">
                           <tr>
                           
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        New Performer User</label>
                                </td>
                                <td style="width:35%;">
                                    <asp:DropDownList runat="server" ID="ddlNewUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" />
                                </td>
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        New Reviewer User</label>
                                </td>
                                <td style="width:35%;">
                                    <asp:DropDownList runat="server" ID="ddlNewReviewerUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 388px;"
                                        CssClass="txtbox" />
                                </td>
                           
                        </tr>
                        <tr>
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                       Select Compliance</label>
                                </td>
                                <td style="width:35%;">
                                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlCompliance" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                           OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged"
                                        CssClass="txtbox" />
                                </td>     

                                <td style="width: 15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        New Approver User</label>
                                </td>
                                <td style="width: 35%;">
                                   <%-- <asp:DropDownList runat="server" ID="ddlNewApproverUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 388px;"
                                        CssClass="txtbox" />--%>
                                      <div style="margin-bottom: 7px">
                      
                         
                        <asp:TextBox runat="server" ID="txtapprover" Style="padding: 0px; margin: 0px; height: 22px; width: 388px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 7px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; width: 300px;" id="dvapprover">
                            <asp:Repeater ID="rptApprover" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterSubTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ApproverSelectAll" Text=" All"
                                                    runat="server" onclick="checkAllApprover(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeatersub" Text="Ok" Style="float: left"  /></td>
                                   </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkApprover" runat="server" onclick="UncheckHeaderCustomer();" /></td>
                                        <td style="width: 560px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px; padding-bottom: 5px;">
                                                <asp:Label ID="lblapproverid" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblapproverName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                                </td>
                           
                           
                        </tr>
                        </div>


                        <tr>
                            <td colspan="2" style="margin-bottom: 7px;"></td>
                            <td>
                                <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSaveAssignment_Click" style="margin-bottom: -13px;margin-top: 19px;"
                                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" OnClientClick="Confirm();" />
                            </td>
                        </tr>
                        
                    </table>
                </div>

                <%-- --%>
                
                <div style="margin-bottom: 7px; float: right; margin-right: 205px; margin-top: 10px;">
                    
                </div>

                <div runat="server" id="div3" style="margin-bottom: 7px;">
                    <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                        <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="false" PageSize="1000"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdComplianceInstances_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                            DataKeyNames="ComplianceInstanceID" OnSorting="grdComplianceInstances_Sorting">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkCompliancesHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCompliances" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ComplianceId" HeaderText="Compliance ID" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" SortExpression="ComplianceId"
                                    HeaderStyle-Height="20px" />
                                <asp:BoundField DataField="Branch" HeaderText="Location" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label111" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Approver" SortExpression="Approver" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprover1" runat="server" Text='<%# Eval("Approver") %>' ToolTip='<%# Eval("Approver") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="LicenseNo" HeaderText="License No" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" SortExpression="LicenseNo"
                                    HeaderStyle-Height="20px" />
                                  <asp:TemplateField HeaderText="Label" SortExpression="SequenceID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID") %>' ToolTip='<%# Eval("SequenceID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="ComplianceInstanceId" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprover21" CssClass="lblstatus" runat="server" Data-attr='<%# Eval("ComplianceInstanceId")+","+"S"+","+ Eval("CustomerID")%>' style="cursor:pointer;" onclick="fstatus(this);" >Status</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:GridView runat="server" ID="grdInternalComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdInternalComplianceInstances_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdInternalComplianceInstances_OnPageIndexChanging"
                            DataKeyNames="InternalComplianceInstanceID" OnSorting="grdInternalComplianceInstances_Sorting">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkICompliancesHeader" runat="server" onclick="javascript:SelectheaderICheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkICompliances" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InternalComplianceID" ItemStyle-Width="8%" HeaderText="Compliance ID" ItemStyle-VerticalAlign="Top" SortExpression="InternalComplianceID"
                                    HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Branch" HeaderStyle-HorizontalAlign="Left" HeaderText="Location" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" />

                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="200px" SortExpression="IShortDescription">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                 <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Approver" SortExpression="Approver" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprover2" runat="server" Text='<%# Eval("Approver") %>' ToolTip='<%# Eval("Approver") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:BoundField DataField="LicenseNo" ItemStyle-Width="8%" HeaderText="License No" ItemStyle-VerticalAlign="Top" SortExpression="LicenseNo"
                                    HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" />
                                  <asp:TemplateField HeaderText="Label" SortExpression="SequenceID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID") %>' ToolTip='<%# Eval("SequenceID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Status" SortExpression="InternalComplianceInstanceID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprover211" CssClass="lblstatus" runat="server"  Data-attr='<%# Eval("InternalComplianceInstanceID") +","+"I"+","+ Eval("CustomerID")%>' style="cursor:pointer;" onclick="fstatus(this);" >Status</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:GridView runat="server" ID="grdTask" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdTask_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdTask_PageIndexChanging"
                            DataKeyNames="TaskInstanceID" OnSorting="grdTask_Sorting">                            
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkTaskHeader" runat="server" onclick="javascript:SelectTaskheaderCheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkTask" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Branch" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" />
                                <asp:TemplateField HeaderText="Task Title" ItemStyle-Width="200px" SortExpression="TaskTitle" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px" >
                                            <asp:Label ID="Label131" runat="server" Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%--<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">--%>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        <%--</div>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Text='<%# Eval("RoleID") %>' Visible="false" ToolTip='<%# Eval("RoleID") %>'></asp:Label>                                        
                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                 <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Approver" SortExpression="Approver" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprover3" runat="server" Text='<%# Eval("Approver") %>' ToolTip='<%# Eval("Approver") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                    </asp:Panel>
                </div>
    
                </ContentTemplate>
        </asp:UpdatePanel>

</asp:Content>
