﻿<%@ Page Title="User List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="UserList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.UserList" %>

<%@ Register Src="~/Controls/UserDetailsControl.ascx" TagName="UserDetailsControl"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Newjs/fullcalendar.min.js"></script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .custom-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 258px;
        }
    </style>
    <script type="text/javascript">
        function initializeDatePickerFunctionEndDate(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtStartDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
        function initializeDatePickerFunctionEndDate1(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtEndDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });


        }
        function initializeDatePickerFunctionEndDate2(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtperiodStartDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
        function initializeDatePickerFunctionEndDate3(FunctionEndDate) {
            var startDate = new Date();
            $('#BodyContent_udcInputForm_txtperiodEndDate').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional" OnLoad="upUserList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                 <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="CustomerValidationGroup" />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                        ValidationGroup="CustomerValidationGroup" Display="None" />
                                </div>
                            </td>
                        </tr>
                <tr>
                    <td style="vertical-align: bottom">
                        <div id="divCustomerfilter" runat="server" style="margin-left: 300px">
                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: White; margin-bottom: -5px;">
                                Select Customer :</label>
                            <div style="width: 150px; float: left; margin-top: -15px; margin-left: 109px;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td align="right" style="width: 65%; padding-right: 60px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" style="width: 5%; padding-right: 60px;">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                            </asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                    <td align="right" style="width: 10%;">
                          <asp:LinkButton Text="Change Role" runat="server" ID="btnChangeUserRole" OnClick="btnChangeRole_Click" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdUser_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdUser_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging"
                    OnRowDataBound="grdUser_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="FirstName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="ContactNumber" HeaderText="Contact No." ItemStyle-HorizontalAlign="Center" SortExpression="ContactNumber" />
                        <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                        <asp:TemplateField ItemStyle-Width="60px" HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                    ToolTip="Click to toggle the status..." OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnModifyAssignment" runat="server" CommandName="MODIFY_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                    ToolTip="Modify assignments for this user...">Modify Assignments</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="130px" HeaderText="Assignments" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEventAssignment" runat="server" CommandName="MODIFY_EVENT_ASSIGNMENT" CommandArgument='<%# Eval("ID") %>'
                                    ToolTip="Modify event assignments for this user...">Modify Events</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit User" title="Edit User" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                    OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../Images/delete_icon.png" alt="Delete User" title="Delete User" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to reset password for this user?');"><img src="../Images/reset_password.png" alt="Reset Password" title="Reset Password" /></asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="UNLOCK_USER" CommandArgument='<%# Eval("ID") %>'
                                    Visible='<%# IsLocked((string)Eval("Email")) %>'><img src="../Images/permissions_icon.png" alt="Unblock User" title="Unblock User" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <vit:UserDetailsControl runat="server" ID="udcInputForm" />
    <div id="divModifyAssignment">
        <asp:UpdatePanel ID="upModifyAssignment" runat="server" UpdateMode="Conditional" OnLoad="upModifyAssignment_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width:100%;">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="ModifyAsignmentValidationGroup" />
                                    <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                        ValidationGroup="ModifyAsignmentValidationGroup" Display="None" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Current User</label>
                                    <asp:Label ID="litCurrentUser" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div id="DivType" style="margin-bottom: 7px; margin-left: 142px;" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtSelectType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtSelectType_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Compliance" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Task" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div id="divSelectionType" style="margin-bottom: 7px; margin-left: 142px;" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtSelectionType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtSelectionType_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Statutory" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Internal" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 7px; margin-left: 142px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:RadioButtonList ID="rbtModifyAction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtModifyAction_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Text="Reassign" Value="Reassign" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Exclude" Value="Delete"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px;" id="divNewUser" runat="server">
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        New Performer User</label>
                                </td>
                                <td style="width:35%;">
                                    <asp:DropDownList runat="server" ID="ddlNewUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" />
                                   <%-- <asp:CompareValidator ErrorMessage="Please select New Performer User." ControlToValidate="ddlNewUsers"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup"
                                        Display="None" />--%>
                                </td>
                                <td style="width:15%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                        New Reviewer User</label>
                                </td>
                                <td style="width:35%;">
                                    <asp:DropDownList runat="server" ID="ddlNewReviewerUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" />
                                    <%--<asp:CompareValidator ErrorMessage="Please select New Reviewer User." ControlToValidate="ddlNewReviewerUsers"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup"
                                        Display="None" />--%>
                                </td>
                            </div>
                        </tr>
                        <tr>
                            <td colspan="4" style="margin-bottom:7px;"></td>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px;" runat="server" id="divEventBase">
                                <td style="width:15%;"></td>
                                <td style="width:35%;">
                                    <div style="float: left;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Event Based</label>
                                        <asp:CheckBox runat="server" ID="chkEvent" AutoPostBack="true" OnCheckedChanged="chkEvent_CheckedChanged" />

                                    </div>

                                     <div style="float: left;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            CheckList</label>
                                        <asp:CheckBox runat="server" ID="chkCheckList" AutoPostBack="true" OnCheckedChanged="chkCheckList_CheckedChanged" />

                                    </div>
                                </td>
                                <td style="width:15%;">
                                   
                                </td>
                                <td style="width:35%;">
                                    <label style="font-size: 13px;">
                                        Filter :</label>
                                    <asp:TextBox runat="server" ID="txtAssigmnetFilter" Width="240px" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="txtAssigmnetFilter_TextChanged" />
                                </td>
                            </div>
                        </tr>
                    </table>
                </div>

                <%-- --%>
                <div runat="server" id="div3" style="margin-bottom: 7px;">
                    <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                        <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdComplianceInstances_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                            DataKeyNames="ComplianceInstanceID" OnSorting="grdComplianceInstances_Sorting">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkCompliancesHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCompliances" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ComplianceId" HeaderText="ComplianceId" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" SortExpression="ComplianceId"
                                    HeaderStyle-Height="20px" />
                                <asp:BoundField DataField="Branch" HeaderText="Location" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="Description">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label111" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Label" SortExpression="SequenceID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID") %>' ToolTip='<%# Eval("SequenceID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:GridView runat="server" ID="grdInternalComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdInternalComplianceInstances_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdInternalComplianceInstances_OnPageIndexChanging"
                            DataKeyNames="InternalComplianceInstanceID" OnSorting="grdInternalComplianceInstances_Sorting">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkICompliancesHeader" runat="server" onclick="javascript:SelectheaderICheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkICompliances" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InternalComplianceID" ItemStyle-Width="50px" HeaderText="InternalComplianceID" ItemStyle-VerticalAlign="Top" SortExpression="InternalComplianceId"
                                    HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Branch" HeaderStyle-HorizontalAlign="Left" HeaderText="Location" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" />

                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="200px" SortExpression="ShortDescription">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                 <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Label" SortExpression="SequenceID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID") %>' ToolTip='<%# Eval("SequenceID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                        <asp:GridView runat="server" ID="grdTask" AutoGenerateColumns="false" AllowPaging="true" PageSize="100"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdTask_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnPageIndexChanging="grdTask_PageIndexChanging"
                            DataKeyNames="TaskInstanceID" OnSorting="grdTask_Sorting">                            
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkTaskHeader" runat="server" onclick="javascript:SelectTaskheaderCheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkTask" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Branch" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" SortExpression="Branch"
                                    HeaderStyle-Height="20px" />
                                <asp:TemplateField HeaderText="Task Title" ItemStyle-Width="200px" SortExpression="TaskTitle" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px" >
                                            <asp:Label ID="Label131" runat="server" Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Text='<%# Eval("RoleID") %>' Visible="false" ToolTip='<%# Eval("RoleID") %>'></asp:Label>                                        
                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                 <asp:TemplateField HeaderText="Perofrmer" SortExpression="Perofrmer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPerofrmer" runat="server" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        </asp:GridView>
                    </asp:Panel>
                </div>
                <div style="margin-bottom: 7px; float: right; margin-right: 205px; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSaveAssignment" OnClick="btnSaveAssignment_Click"
                        CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" OnClientClick="Confirm();" />
                    <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divModifyAssignment').dialog('close');" />
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divAssignEvent">
        <asp:UpdatePanel ID="upAssignEvent" runat="server" UpdateMode="Conditional" OnLoad="upAssignEvent_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="AssignEventValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="AssignEventValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Current User</label>
                        <asp:Label ID="lblEventAssignedUser" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 142px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:RadioButtonList ID="rbtEventModifyAssignment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtEventModifyAssignment_SelectedIndexChanged"
                            RepeatDirection="Horizontal" RepeatLayout="Table">
                            <asp:ListItem Text="Reassign" Value="Reassign" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Exclude" Value="Delete"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px" id="divEventNewUser" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            New User</label>
                        <asp:DropDownList runat="server" ID="ddlAllUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select New User." ControlToValidate="ddlAllUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AssignEventValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 5px; margin-left: 564px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="txtAssignEventUser" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="txtAssignEventsFilter_TextChanged" />
                    </div>
                    <div runat="server" id="div4" style="margin-bottom: 7px;">
                        <asp:GridView runat="server" ID="grdAssignedEventInstance" AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdAssignedEventInstance_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnSorting="grdAssignedEventInstance_Sorting"
                            DataKeyNames="EventAssignmentID" OnPageIndexChanging="grdAssignedEventInstance_OnPageIndexChanging">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkEventHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEvent" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CustomerBranchName" HeaderText="Location" ItemStyle-VerticalAlign="Top" SortExpression="CustomerBranchName"
                                    HeaderStyle-Height="20px" />
                                <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="200px" SortExpression="Name">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="Label1a" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Text='<%# Eval("RoleID") %>' Visible="false" ToolTip='<%# Eval("RoleID") %>'></asp:Label>
                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 205px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnAssignEvents" OnClick="btnAssignEvents_Click"
                            CssClass="button" ValidationGroup="AssignEventValidationGroup" OnClientClick="ConfirmEvent();" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divAssignEvent').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divChengeRolesDialog">
        <asp:UpdatePanel ID="upIComplianceType" runat="server">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="vdsummary" ValidationGroup="ChengeRolesValidationGroup" 
                            class="alert alert-block alert-danger fade in"/>
                        <asp:CustomValidator ID="CustomValidator4" runat="server" EnableClientScript="False"
                            ValidationGroup="ChengeRolesValidationGroup" Display="None" class="alert alert-block alert-danger fade in"/>
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                         <div style="margin-bottom: 7px">
                            <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 70px; display: block; float: left; font-size: 13px; color: #333;">
                            Modules</label>
                        <asp:DropDownList runat="server" ID="ddlModule" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged"  class="form-control">
                            <asp:ListItem Text="< Select Module >" Value="-1" />
                           <%-- <asp:ListItem Text="Compliance" Value="1" />--%>
                            <asp:ListItem Text="Litigation" Value="2" />
                            <asp:ListItem Text="Contract" Value="3" />
                            <asp:ListItem Text="License" Value="4" />
                        </asp:DropDownList>
                        
                        <label style="color: #333;"">Filter :</label>
                        <asp:TextBox runat="server" ID="tbxUserFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxUserFilter_TextChanged" />
                   
                       
                             </div>
                        <div style="margin-bottom: 7px; margin-top: 20px" id="divSampleForm" runat="server">
                            <asp:Panel ID="Panel3" Width="100%" Height="30%" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdUserRole" AutoGenerateColumns="false"
                                    GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" ShowHeaderWhenEmpty="true"
                                    BorderWidth="1px" CellPadding="4" ForeColor="Black" OnRowDataBound="GrdUserRole_RowDataBound"
                                    Width="100%" Font-Size="12px">
                                    <Columns>
                                        <%--<asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" />--%>
                                        <asp:TemplateField HeaderText="UserID" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
                                            <ItemTemplate >
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="FirstName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                         <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                         
                                        <asp:TemplateField HeaderText="Role" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="ddlHeaderRole" name="ddlHeaderRole" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlHeaderRole_SelectedIndexChanged" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 130px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:DropDownList ID="ddlRole" name="ddlRole" runat="server" Style="width: 100%;">
                                                    </asp:DropDownList>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>

                    </div>

                    <div style="margin-bottom: 7px; margin-left: 35%;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ChengeRolesValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divChengeRolesDialog').dialog('close');RefreshPage();" />
                    </div>

                </div>
                <%--               <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>

                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divassignefolderDialog">
        <asp:UpdatePanel ID="upIComplianceFolder" runat="server">
              <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                      
                    </div>
                    <div style="margin-bottom: 7px;    margin-top: 30px;
">
                          <div style="margin-bottom: 7px; margin-top: 20px" id="div1" runat="server">
                            <asp:Panel ID="Panel4" Width="100%" Height="30%" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="gridviewfolder" AutoGenerateColumns="false"
                                    GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" ShowHeaderWhenEmpty="true"
                                    BorderWidth="1px" CellPadding="4" ForeColor="Black" OnRowDataBound="gridviewfolder_RowDataBound"
                                    Width="100%" Font-Size="12px">
                                    <Columns>
                                        <%--<asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" />--%>
                                        <asp:TemplateField HeaderText="ID" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" Visible="false">
                                            <ItemTemplate >
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblFolderID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Name" HeaderText="Folder Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                       
                                        <asp:TemplateField HeaderText="User" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="ddlHeaderUser" name="ddlHeaderUser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlHeaderUser_SelectedIndexChanged" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="overflow: hidden; width: 130px; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:DropDownList ID="ddlUserlist" name="ddlUserlist" runat="server" Style="width: 100%;">
                                                    </asp:DropDownList>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                        <div style="display:none;">
                          <asp:TextBox runat="server" ID="txtuserid" Width="250px" MaxLength="50" />
                            </div>
                        <div style="margin-bottom: 7px;margin-left: 19%;margin-top: 46px;">
                            <asp:Button Text="Save" runat="server" ID="Btnsaveuniversaldoc" OnClick="Btnsaveuniversaldoc_Click" CssClass="button"
                                ValidationGroup="ChengeRolesValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="Button4" CssClass="button" OnClientClick="$('#divassignefolderDialog').dialog('close');RefreshPage();" />
                        </div>
                    </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        function fclosepopup1() {
            alert("Deleted Succesfully.");
            $('#divassignefolderDialog').dialog('close');
            //window.location.href = window.location.href;
        }

        $(function () {
            $('#divModifyAssignment').dialog({
                height: 650,
                width: 1064,
                autoOpen: false,
                draggable: true,
                title: "Assignments",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divAssignEvent').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Event Assignments",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        $(function () {
            $('#divChengeRolesDialog').dialog({
                height: 600,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Bulk Role Change",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        $(function () {
            $('#divassignefolderDialog').dialog({
                height: 500,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Reassignment of universal document",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        
        function RefreshPage() {
            window.location.reload(true);
        }
        function initializeComboboxForAssignmentsDialog() {
            $("#<%= ddlNewUsers.ClientID %>").combobox();
            $("#<%= ddlCustomerList.ClientID %>").combobox();
            $("#<%= ddlAllUsers.ClientID %>").combobox();
            $("#<%= ddlNewReviewerUsers.ClientID %>").combobox();
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            var gvcheck = null;
            if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
            else
                gvcheck = document.getElementById("<%=grdAssignedEventInstance.ClientID %>");
            var i;
            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function SelectheaderICheckboxes(Iheaderchk) {
            var rolecolumn;
            var chkIheaderid = Iheaderchk.id.split("_");

            var Igvcheck = null;
            if (chkIheaderid[1] == "grdInternalComplianceInstances")
                Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");
             else
                 Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");

             var i;

             if (Iheaderchk.checked) {
                 for (i = 0; i < Igvcheck.rows.length; i++) {
                     Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                 }
             }

             else {
                 for (i = 0; i < Igvcheck.rows.length; i++) {
                     Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                 }
             }
         }

         function SelectTaskheaderCheckboxes(chkTaskHeader) {
             var rolecolumn;
             var chkheaderid = chkTaskHeader.id.split("_");

             var Taskgvcheck = null;
             if (chkheaderid[1] == "grdTask")
                 Taskgvcheck = document.getElementById("<%=grdTask.ClientID %>");
            else
                Taskgvcheck = document.getElementById("<%=grdTask.ClientID %>");

            var i;

            if (chkTaskHeader.checked) {
                for (i = 0; i < Taskgvcheck.rows.length; i++) {
                    Taskgvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = chkTaskHeader.checked;
                }
            }
            else {
                for (i = 0; i < Taskgvcheck.rows.length; i++) {
                    Taskgvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = chkTaskHeader.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = null;
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
            else
                gvcheck = document.getElementById("<%=grdAssignedEventInstance.ClientID %>");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

        function Confirm() {
            debugger;
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var oldUser = $("#<%=litCurrentUser.ClientID%>").html();
            var newUser = $("#<%=ddlNewUsers.ClientID%> option:selected").text();
            var newUser1 = $("#<%=ddlNewReviewerUsers.ClientID%> option:selected").text();
            var type = $("#<%=rbtModifyAction.ClientID%> input:checked").val();
            var Comtype = $("#<%=rbtSelectType.ClientID%> input:checked").val();
            //if (Comtype == "Compliance") {
            debugger;
            if (Comtype == 0) {
                if (type == "Reassign") {
                    if (newUser != "< Select user >") {
                        //Comtype
                        if (confirm("Are you sure you want reassign selected compliances to " + newUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                    else if (newUser1 != "< Select user >") {
                        if (confirm("Are you sure you want reassign selected compliances to " + newUser1 + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                } else {
                    if (confirm("Are you sure you want exclude selected compliances for " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
                if (confirm_value != '' && confirm_value != null) {
                    document.forms[0].appendChild(confirm_value);
                }
                //document.forms[0].appendChild(confirm_value);
            }
            else {
                if (type == "Reassign") {
                    if (newUser != "< Select user >") {
                        //Comtype
                        if (confirm("Are you sure you want reassign selected compliance tasks to " + newUser + "?")) {
                            confirm_value.value = "Yes";
                        } else {
                            confirm_value.value = "No";
                        }
                    }
                } else {

                    if (confirm("Are you sure you want exclude selected compliance tasks for " + oldUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
                if (confirm_value != '' && confirm_value != null) {
                    document.forms[0].appendChild(confirm_value);
                }

            }
        }

        function ConfirmEvent() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_Event_value";
            var oldUser = $("#<%=lblEventAssignedUser.ClientID%>").html();
            var newUser = $("#<%=ddlAllUsers.ClientID%> option:selected").text();
            var type = $("#<%=rbtEventModifyAssignment.ClientID%> input:checked").val();

            if (type == "Reassign") {
                if (newUser != "< Select user >") {
                    if (confirm("Are you sure you want reassign selected Event to " + newUser + "?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                }
            } else {
                if (confirm("Are you sure you want exclude selected Event to " + oldUser + "?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
            }

            document.forms[0].appendChild(confirm_value);
        }

    </script>
</asp:Content>
