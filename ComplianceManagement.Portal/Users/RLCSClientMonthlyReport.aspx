﻿<%@ Page Title="TeamLease-Client(s) Monthly Report" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCSClientMonthlyReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.RLCSClientMonthlyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .custom-combobox-input{
            width:250px;
        }
    </style>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlMonth.ClientID %>").combobox();
            $("#<%= ddlYear.ClientID %>").combobox();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upRLCSClientReport" runat="server" UpdateMode="Conditional" OnLoad="upRLCSClientReport_Load">
        <ContentTemplate>
            <center>
                <div class="row">
                    <b>TeamLease-Client(s) Monthly Report</b>
                </div>
                <div style="margin: 10px" class="row">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="6" align="left">
                                <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 15%; height: 10%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select Month</label>
                            </td>
                            <td style="width: 15%; height: 10%;">
                                <asp:DropDownList runat="server" ID="ddlMonth" class="form-control m-bot15" Style="width: 180px; float: left"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="January" Value="01" />
                                    <asp:ListItem Text="February" Value="02" />
                                    <asp:ListItem Text="March" Value="03" />
                                    <asp:ListItem Text="April" Value="04" />
                                    <asp:ListItem Text="May" Value="05" />
                                    <asp:ListItem Text="June" Value="06" />
                                    <asp:ListItem Text="July" Value="07" />
                                    <asp:ListItem Text="August" Value="08" />
                                    <asp:ListItem Text="September" Value="09" />
                                    <asp:ListItem Text="October" Value="10" />
                                    <asp:ListItem Text="November" Value="11" />
                                    <asp:ListItem Text="December" Value="12" />
                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%; height: 10%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 130px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select Year</label>
                            </td>
                            <td style="width: 15%; height: 10%;">
                                <asp:DropDownList runat="server" ID="ddlYear" class="form-control m-bot15" Style="width: 180px; float: left"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="2017" Value="2017" />
                                    <asp:ListItem Text="2018" Value="2018" />
                                    <asp:ListItem Text="2019" Value="2019" />
                                    <asp:ListItem Text="2020" Value="2020" />
                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%; height: 10%;">
                                <asp:Button ID="btnExport" CausesValidation="false" class="btn btn-search" CssClass="button" runat="server" Text="Export to Excel" OnClick="btnExport_Click" />
                            </td>
                        </tr>
                    </table>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-10 colpadding0" style="text-align: right; float: left">
                            <div class="col-md-4 colpadding0">
                            </div>
                            <div class="col-md-4 colpadding0">
                            </div>
                            <div class="col-md-4 colpadding0">
                            </div>
                        </div>
                    </div>
                </div>
            </center>
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnExport" EventName="Click" />--%>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
