﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderUserMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderUserMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
    .k-pager-wrap > .k-link > .k-icon {
    margin-top: 6px;
    color: inherit;
}
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
    <div class="col-sm-12">
                     <div class="col-sm-12" >
                <div class="toolbar">  
                 <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
                  <button style="float: right;margin-right: 10px;"  id="button1" type="button" class="btn btn-primary"  onclick="OpenAdvanceImport(event)" >Import User</button> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:4%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>    
        <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                <form  class="form-inline" method="POST" id="fcontact" required  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:30px;">
            <div class="form-group required col-sm-6 " style="padding:0">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">First Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="firstname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="First Name" autocomplete="off"  required></span>
            </div>
            <div class="form-group required  col-sm-6  " style="padding:0">
                <label class="col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Last Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="lastname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Last Name" autocomplete="off" ></span>
            </div>
            <div class="form-group required col-sm-6  " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Mobile:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="mobile" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Mobile" autocomplete="off"required ></span>
            </div>
            <div class="form-group required col-sm-6   " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Email:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="email" type="email" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off" required ></span>
            </div>
            <div class="form-group required col-sm-6  " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1" required>Role: &nbsp; </label>
                <input  id="dropdownRole" data-placeholder="Role" style="width:40%;">
            </div>
            <div class="form-group required col-sm-6   " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1" required>Status: &nbsp; </label>
                <input  id="dropdownStatus" data-placeholder="Role" style="width:40%;">
            </div>
                
                 <div class="form-group required col-sm-6   " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1" required>Email Trigger: &nbsp; </label>
                <input  id="dropdownEmailTrigger" data-placeholder="Role" style="width:40%;">
            </div>

            </div>
            
            
          

                 
                
                   
           
            
            </div>
            <button id="saveBtn" style="margin-top:50px;" type="submit" onclick="saveDetails(event)" class="btn btn-primary">Save</button>
                                        
          </form>
                      </div>
           </div>
        <input type="hidden" id="editFlag" value="0" />
    <div id="divAdvanceImportModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton11" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton12" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                       <form  class="form-inline" method="POST" id="fcontactimport" required  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:30px;">
             <div class="form-group required col-sm-6   " style="padding:0px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1" required>User Name: &nbsp; </label>
                <input  id="dropdownNameImport" data-placeholder="Name" style="width:55%;">
            </div>
            <div class="form-group required col-sm-6   " style="padding:0;">
                <label class="col-sm-3 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Email:</label>
                <span class="col-sm-9 k-widget k-textbox" style="width: 55%;"><input id="emailImport" type="email" disabled style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off" required  ></span>
            </div>
            <div class="form-group required col-sm-6  " style="padding:0px;padding-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="sel1" required>Role: &nbsp; </label>
                <input  id="dropdownRoleImport" data-placeholder="Role" style="width:55%;">
            </div>

            </div>    
 
            <button id="saveImportBtn" style="margin-top:60px;" type="submit" onclick="importDetails(event)" class="btn btn-primary">Import</button>
                                        
          </form>
                      </div>
           </div>
        
        <script type="text/javascript">
       
        $(document).ready(function () {
            $("#pagetype").text("User Master");
            $("#grid").kendoGrid({

                dataSource: {
                    transport: {
                        read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getuser/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                        },
                    schema: {
                        data: function (response) {
                            return response.data;
                        },
                        total: function (response) {
                            return response.total;
                        },
                        model: {
                            fields: {
                                FirstName: { type: "string" },
                                LastName: { type: "string" },
                                Email: { type: "string" },
                                ContactNumber:{ type: "string"},
                                RoleID: { type: "string" },
                                role1: { type: "string" },
                            }
                        }
                    },
                    pageSize: 10
                },
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                //height: 550,
                //toolbar: ["search"],
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{

                    //field: "FirstName",
                    //field:"LastName",
                    title: "Name",
                    template:"<div>#=FirstName# #=LastName# </div>",
                    width: 150

                } ,{

                        field: "Email",
                    title: "Email",
                    width: 150

                },
                {

                    field: "ContactNumber",
                    title: "Mobile",
                    width: 150

                },
                {

                        field: "role1",
                    title: "RoleID",
                    width: 150

                },
                     {

                         field: "IsActive",
                         title: "Status",
                         //template:"# if(IsActive == true){Active } else {In Active }#",
                        width: 150

                    },{
                        template: "<div class='customer-photo'" +
                            "style='background-image: url(../../Images/edit_icon.png);'" + " onClick='OpenAdvanceSearch(#= JSON.stringify(data) #)' >",
                        field: "",
                        title: "Action",
                        width: 150
                    }]
            });
        });
    </script>
<script>
    function OpenAdvanceImport(dd) {
        if (dd.role1 == 'HDCS') {
            $("#dropdownRole").data("kendoDropDownList").value(29);
        } else {
            $("#dropdownRole").data("kendoDropDownList").value(38);
        }
        
       
        var myWindowAdv = $("#divAdvanceImportModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "55%",
            height: "30%",
            title: "Import User",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
            close: onClose
        });
        $("#divAdvanceImportModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        //   $("#firstname").val(firstName);
        //  $('#lastname').val(lastName);
        //$('#mobile').val();
        // $('#email').val(email);
        //$('#dropdownRole').val(role);
        //e.preventDefault();
        return false;
    }
    
    function OpenAdvanceSearch(dd) {
        //alert(JSON.stringify(dd));
        if (dd.FirstName != undefined) {
            //alert('Flag not zero')
            $('#editFlag').val(dd.ID);
        } else {
           // alert('Flag  zero')
            $('#editFlag').val(0);
        }
        $('#firstname').val(dd.FirstName);
        $('#lastname').val(dd.LastName);
        $('#email').val(dd.Email);
        $('#mobile').val(dd.ContactNumber);
        if (dd.role1 == 'HDCS') {
            $("#dropdownRole").data("kendoDropDownList").value(29);
        } else {
            $("#dropdownRole").data("kendoDropDownList").value(38);
        }
        if (dd.IsActive == 'Active') {
            $("#dropdownStatus").data("kendoDropDownList").value(1);
        } else {
            $("#dropdownStatus").data("kendoDropDownList").value(0);
        }
       
        var myWindowAdv = $("#divAdvanceSearchModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "65%",
            height: "45%",
            title: "Add New/Edit User",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
            close: onClose
        });
        $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
     //   $("#firstname").val(firstName);
      //  $('#lastname').val(lastName);
        //$('#mobile').val();
       // $('#email').val(email);
        //$('#dropdownRole').val(role);
        //e.preventDefault();
        return false;
    }

    function OpenAdvanceSearchFilter(e) {
        $('#divAdvanceSearchFilterModel').modal('show');
        e.preventDefault();
        return false;
    }
        </script>
    
    <script type="text/javascript">
        $("#dropdownRole").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Designated Person", value: "38" },
                { text: "Head Company Secretary", value: "29" },
            ],
            index: 0,
            change: function (e) {
                

            }

        });
        $("#dropdownStatus").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Active", value: "1" },
                { text: "In Active", value: "0" },
            ],
            index: 0,
            change: function (e) {


            }

        });
        $("#dropdownEmailTrigger").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Yes", value: "1" },
                { text: "No", value: "2" },
            ],
            index: 0,
            change: function (e) {


            }

        });
    </script>
     <script type="text/javascript">
        $("#dropdownRoleImport").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Designated Person", value: "38" },
                { text: "Head Company Secretary", value: "29" },
            ],
            index: 0,
            change: function (e) {
                

            }

        });
        $("#dropdownStatus").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Active", value: "1" },
                { text: "In Active", value: "0" },
            ],
            index: 0,
            change: function (e) {


            }

        });

        $("#dropdownEmailTriggerImport").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "Yes", value: "1" },
                { text: "No", value: "2" },
            ],
            index: 0,
            change: function (e) {


            }

        });
      
        $("#dropdownNameImport").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getalluser/?customer_id="+<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                        },
                    }
                },
            change: function (e) {
               // alert(e.sender.value());
                $.ajax({
                    type: 'Get',
                    url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getalluserlist/?customer_id="+<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+"&id="+e.sender.value(),
                    
                    success: function (data) {
                       // alert(data[0]['Email'])
                        $('#emailImport').val(data[0]['Email']);

                    },
                    error: function (e, t) { alert('something went wrong'); }
                });
                    
                }
        });

        
    </script>
    
        <script type="text/javascript">
            function saveDetails(e) {
                e.preventDefault();
               // alert('hi');
                confirm("Are you sure want to add details?");
                var editFlag = $("#editFlag").val();
                var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
                var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
                var detailcreated_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
                var firstname = $("#firstname").val();
                var lastname = $('#lastname').val();
                var mobile = $('#mobile').val();
                var email = $('#email').val();
                var is_active = $('#dropdownStatus').val();
                var emailTrigger =$('#dropdownEmailTrigger').val();
                var dropdownRole = $('#dropdownRole').val();
                var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
                //var pdata = '{ "user_id":' + user_id + ' ,"detailcreated_id":' + detailcreated_id + ' ,"customer_id":' + customer_id + ' ,"firstname":' + firstname + ' ,"lastname":' + lastname + ' ,"mobile":' + mobile + ' ,"dropdownRole":' + dropdownRole + ' ,"created_by":' + created_by  + '   }';

               // alert(is_active);
                //pdata = JSON.parse(pdata);
                $.ajax({
                    type: 'post',
                    url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createuser/',
                    data: { "user_id": user_id, "email": email, "customer_id": customer_id, "firstname": firstname, "lastname": lastname, "mobile": mobile, "role": dropdownRole, "created_by": created_by,"editflag":editFlag,"emailTrigger":emailTrigger,"is_active":is_active },
                    success: function (result) {
                       alert(result.msg);
                       
                        location.reload();

                    },
                    error: function (e, t) { alert('something went wrong'); }
                });

                return false;
            }
        </script>
         <script type="text/javascript">
             function importDetails(e) {
                e.preventDefault();
                confirm("Are you sure want to import user?");
                var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
                var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
                var detailcreated_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
                var email = $("#emailImport").val();dropdownNameImport
                var edit=$("#dropdownNameImport").val();
                var dropdownRole = $('#dropdownRoleImport').val();
                var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
                //var pdata = '{ "user_id":' + user_id + ' ,"detailcreated_id":' + detailcreated_id + ' ,"customer_id":' + customer_id + ' ,"firstname":' + firstname + ' ,"lastname":' + lastname + ' ,"mobile":' + mobile + ' ,"dropdownRole":' + dropdownRole + ' ,"created_by":' + created_by  + '   }';

                //alert(is_active);
                //pdata = JSON.parse(pdata);
                $.ajax({
                    type: 'post',
                    url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>importuser/',
                    data: { "user_id": user_id, "email": email, "customer_id": customer_id, "role": dropdownRole, "created_by": created_by,'edit':edit },
                    success: function (result) {
                       alert(result.msg);
                       
                        location.reload();

                    },
                    error: function (e, t) { alert('something went wrong'); }
                });

                return false;
            }
        </script>
    </asp:Content>
