﻿<%@ Page Title="Vendor Master:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="VendorMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.VendorMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters/ Vendor');
        });

        function OpenVendorDetailPopup(a) {
            $('#AddVendorDetailsPop').modal('show');
            $('#ContentPlaceHolder1_IframePartyDetial').attr('src', "/ContractProduct/Masters/AddVendor.aspx?VendorId=" + a);
        }

        function ClosePopVendor() {
            $('#AddVendorDetailsPop').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

        function RefreshParent() {
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="row">
                <asp:ValidationSummary ID="vsVendorPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="VendorPageValidationGroup" />
                <asp:CustomValidator ID="cvVendorPage" runat="server" EnableClientScript="False"
                    ValidationGroup="VendorPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-6 colpadding0">
                    <asp:TextBox runat="server" AutoComplete="off" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" AutoPostBack="true"
                        OnTextChanged="tbxFilter_TextChanged" Width="70%" />
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                </div>

                <div class="col-md-2 colpadding0 text-right">
                    <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"
                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Vendor">
                        <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                  
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdVendor" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnRowCreated="grdVendor_RowCreated"
                    PageSize="10" AllowPaging="true" AutoPostBack="true"
                    OnRowDataBound="grdVendor_RowDataBound" CssClass="table" OnSorting="grdVendor_Sorting" GridLines="none" Width="100%" DataKeyNames="ID" OnRowCommand="grdVendor_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- SortExpression="Type"--%>
                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Convert.ToInt32(Eval("Type")) != 1 ? "Corporate" : "Individual" %>' ToolTip='<%# Convert.ToInt32(Eval("Type")) != 1 ? "Corporate" : "Individual" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StateID") %>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CityID") %>' ToolTip='<%# Eval("CityID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Person" SortExpression="ContactPerson" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContactPerson") %>' ToolTip='<%# Eval("ContactPerson") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Number" SortExpression="ContactNumber" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContactNumber") %>' ToolTip='<%# Eval("ContactNumber") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Vendor" ToolTip="Edit" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>'>
                                        <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Vendor" ToolTip="Delete" data-toggle="tooltip" Visible="true"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Vendor Details?');">
                                        <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>

                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Previous" CssClass="btn btn-primary" ToolTip="Go to Previous Master (User)" data-toggle="tooltip" OnClick="lnkPreviousSwitch_Click" runat="server" ID="LnkbtnPrevious" Width="100%" />
                        </div>
                        <div class="col-md-10 colpadding0">
                        </div>
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-10 colpadding0">
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Next" CssClass="btn btn-primary" ToolTip="Go to Next Master (Contract Type)" data-toggle="tooltip" runat="server" OnClick="lnkNextSwitch_Click" ID="LnkbtnNext" Style="float: right; width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddVendorDetailsPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblName" runat="server" class="modal-header-custom">
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframePartyDetial" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
