﻿using System;
using System.Web;
using System.Web.UI;

using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Collections.Generic;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddNewDepartment : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DepartmentID"]))
                {
                    int DeptID = Convert.ToInt32(Request.QueryString["DepartmentID"]);
                    Department RPD = CompDeptManagement.DepartmentMasterGetByID(DeptID, CustomerID);//, CustomerID
                    txtFName.Text = RPD.Name;
                    if (!string.IsNullOrEmpty(RPD.Color))
                    {
                        favcolor.Value = RPD.Color;
                        Txtboxcolor.Text = RPD.Color;
                    }
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = DeptID;
                }
                else
                {
                    ViewState["Mode"] = 0;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string value = Txtboxcolor.Text;
                if (!string.IsNullOrEmpty(txtFName.Text.Trim()))
                {
                    Department objDeptcompliance = new Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)CustomerID,
                        Color = Txtboxcolor.Text
                    };
                    mst_Department objDeptaudit = new mst_Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)CustomerID,
                        Color = Txtboxcolor.Text
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                        objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (CompDeptManagement.GetDepartmentExists(objDeptcompliance))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists";
                        }
                        else
                        {
                            int newDeptID = 0;
                            newDeptID = CompDeptManagement.CreateComplianceDepartmentMaster(objDeptcompliance);

                            if (newDeptID > 0)
                            {
                                //List<Cont_tbl_UserDeptMapping> lstDeptMapping_ToSave = new List<Cont_tbl_UserDeptMapping>();
                                //Cont_tbl_UserDeptMapping _DeptMappingRecord = new Cont_tbl_UserDeptMapping()
                                //{
                                //    UserID = AuthenticationHelper.UserID,
                                //    CustomerID = AuthenticationHelper.CustomerID,
                                //    DeptID = newDeptID,
                                //    IsActive = true,
                                //    CreatedBy = (AuthenticationHelper.UserID),
                                //    CreatedOn = DateTime.Now,
                                //    IsContractOwner = true
                                //};
                                //lstDeptMapping_ToSave.Add(_DeptMappingRecord);
                                //bool saveDeptSuccess = ContractManagement.CreateUpdate_DeptUserMapping(lstDeptMapping_ToSave);


                                cvDeptPopup.IsValid = false;
                                cvDeptPopup.ErrorMessage = "Department Name Saved Successfully.";
                                vsAddEditDept.CssClass = "alert alert-success";
                                txtFName.Text = string.Empty;
                                Session["DID"] = newDeptID;
                            }
                        }

                        if (CompDeptManagement.GetAuditDepartmentExists(objDeptaudit))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists.";
                        }
                        else
                        {
                            CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Name Saved Successfully.";
                            vsAddEditDept.CssClass = "alert alert-success";
                            txtFName.Text = string.Empty;
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (CompDeptManagement.GetDepartmentExists(objDeptcompliance))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists.";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Name Updated Successfully.";
                            vsAddEditDept.CssClass = "alert alert-success";
                        }

                        if (CompDeptManagement.GetAuditDepartmentExists(objDeptaudit))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department already Exists.";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Name Updated Successfully.";
                            vsAddEditDept.CssClass = "alert alert-success";
                        }
                    }

                }
                else
                {
                    cvDeptPopup.IsValid = false;
                    cvDeptPopup.ErrorMessage = "Please Enter Department Name.";
                    vsAddEditDept.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        [WebMethod]
        public static string getDepatmentID()
        {
            string depatmentID = string.Empty;
            try
            {

                depatmentID = ContractMastersManagement.GetDepartmenID();
                return depatmentID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return depatmentID;
            }
        }
        protected void btnCancelDeptPopUp_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}