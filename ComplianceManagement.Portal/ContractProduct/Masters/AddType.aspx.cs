﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Contract.Masters
{
    public partial class AddType : System.Web.UI.Page
    {

        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            divsuccessmsgaCType.Visible = false;
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["ContractTypeId"]))
                    {
                        long typeID = Convert.ToInt64(Request.QueryString["ContractTypeId"]);
                        Cont_tbl_TypeMaster objContractTypeRecord = ContractTypeMasterManagement.GetContractTypeDetailByID(typeID, Convert.ToInt32(CustomerID));

                        if (objContractTypeRecord != null)
                        {
                            tbxContType.Text = objContractTypeRecord.TypeName;
                            ViewState["Mode"] = 1;
                            ViewState["ContTypeID"] = typeID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    divsuccessmsgaCType.Visible = false;
                    cvAddEditContractType.IsValid = false;
                    cvAddEditContractType.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxContType.Text.Trim()))
                {
                    Cont_tbl_TypeMaster objCase = new Cont_tbl_TypeMaster()
                    {
                        TypeName = tbxContType.Text,
                        CustomerID = (int)CustomerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTypeMasterManagement.ExistsContractType(objCase, 0))
                        {
                            divsuccessmsgaCType.Visible = false;
                            cvAddEditContractType.IsValid = false;
                            cvAddEditContractType.ErrorMessage = "Contract Type with Same Name already exists.";
                        }
                        else
                        {
                            long newTypeID = 0;

                            newTypeID = ContractTypeMasterManagement.CreateContractTypeDetails(objCase);

                            if (newTypeID > 0)
                            {
                                //cvAddEditContractType.IsValid = false;
                                //cvAddEditContractType.ErrorMessage = "Contract Type Save Successfully.";
                                //vsAddEditContractType.CssClass = "alert alert-success";
                                if (cvAddEditContractType.IsValid)
                                {
                                    divsuccessmsgaCType.Visible = true;
                                    successmsgaCType.Text = "Contract Type Saved Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCType.ClientID + "').style.display = 'none' },2000);", true);
                                }
                                tbxContType.Text = string.Empty;

                                Session["TypeID"] = newTypeID;
                            }
                            else
                            {
                                divsuccessmsgaCType.Visible = false;
                                cvAddEditContractType.IsValid = false;
                                cvAddEditContractType.ErrorMessage = "Something went wrong, Please try again.";

                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        objCase.ID = Convert.ToInt64(ViewState["ContTypeID"]);

                        if (ContractTypeMasterManagement.ExistsContractType(objCase, objCase.ID))
                        {
                            divsuccessmsgaCType.Visible = false;
                            cvAddEditContractType.IsValid = false;
                            cvAddEditContractType.ErrorMessage = "Contract Type with same Name already exist.";
                            cvAddEditContractType.CssClass = "alert alert-danger";
                        }
                        else
                        {
                            objCase.UpdatedBy = AuthenticationHelper.UserID;
                            objCase.UpdatedOn = DateTime.Now;

                            bool saveSuccess = ContractTypeMasterManagement.UpdateContractTypeDetails(objCase);

                            if (saveSuccess)
                            {
                                //cvAddEditContractType.IsValid = false;
                                //cvAddEditContractType.ErrorMessage = "Contract Type Updated Successfully.";
                                //vsAddEditContractType.CssClass = "alert alert-success";
                                if (cvAddEditContractType.IsValid)
                                {
                                    divsuccessmsgaCType.Visible = true;
                                    successmsgaCType.Text = "Contract Type Updated Successfully.";

                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCType.ClientID + "').style.display = 'none' },2000);", true);
                                }
                            }
                            else
                            {
                                divsuccessmsgaCType.Visible = false;
                                cvAddEditContractType.IsValid = false;
                                cvAddEditContractType.ErrorMessage = "Something went wrong, Please try again";
                                cvAddEditContractType.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
                else
                {
                    divsuccessmsgaCType.Visible = false;
                    cvAddEditContractType.IsValid = false;
                    cvAddEditContractType.ErrorMessage = "Please Enter Contract Type.";

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                divsuccessmsgaCType.Visible = false;
                cvAddEditContractType.IsValid = false;
                cvAddEditContractType.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        [WebMethod]
        public static string getTypeID()
        {
            string TypeID = string.Empty;
            try
            {
                TypeID = ContractTypeMasterManagement.GetContTypeID();
                return TypeID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return TypeID;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}