﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class VendorMaster : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected Cont_tbl_PageAuthorizationMaster authpage;
        protected int pageid = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String key = "ContractAuthenticate" + AuthenticationHelper.UserID;
                if (Cache.Get(key) != null)
                {
                    var Records = (List<Cont_tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                    if (Records.Count > 0)
                    {
                        var query = (from row in Records
                                     where row.PageID == pageid
                                     select row).FirstOrDefault();
                        authpage = query;
                    }
                }
                if (authpage != null)
                {
                    btnAddNew.Visible = false;
                    if (authpage.Addval == true)
                    {
                        btnAddNew.Visible = true;
                    }
                }
                else
                    btnAddNew.Visible = true;

                BindData();
                flag = false;
                bindPageNumber();
                ViewState["Mode"] = 0;
                                if (AuthenticationHelper.Role.Equals("CADMN") || AuthenticationHelper.Role.Equals("MGMT"))
                    LnkbtnPrevious.Visible = true;
                else
                    LnkbtnPrevious.Visible = false;
            }
        }

        private void BindData()
        {
            try
            {
                var VendorList = VendorDetails.GetVendorDetails_Paging(Convert.ToInt32(CustomerID), tbxtypeTofilter.Text.Trim().ToString());
                
                if (VendorList.Count > 0)
                {
                    VendorList = VendorList.OrderBy(entry => entry.VendorName).ToList();
                    direction = SortDirection.Ascending;
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            VendorList = VendorList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            VendorList = VendorList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (VendorList.Count > 0)
                {
                    grdVendor.DataSource = VendorList;
                    Session["TotalRows"] = VendorList.Count;
                    grdVendor.DataBind();
                }
                else
                {
                    grdVendor.DataSource = VendorList;
                    grdVendor.DataBind();
                }
                lblStartRecord.Text = DropDownListPageNo.SelectedValue;
                if ((string.IsNullOrEmpty(DropDownListPageNo.SelectedValue)))
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdVendor.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdVendor.PageIndex = 0;
                BindData();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvVendorPage.IsValid = false;
                cvVendorPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                var VendorList = VendorDetails.GetVendorDetails_Paging(Convert.ToInt32(CustomerID), tbxtypeTofilter.Text.Trim().ToString());               
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    VendorList = VendorList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    VendorList = VendorList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                
                foreach (DataControlField field in grdVendor.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdVendor.Columns.IndexOf(field);
                    }
                }
                flag = true;
                grdVendor.DataSource = VendorList;
                Session["TotalRows"] = VendorList.Count;
                grdVendor.DataBind();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdVendor_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdVendor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            string ID = null;
            lblName.InnerText = "Add New Vendor";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenVendorDetailPopup('" + ID + "');", true);
            BindData();
            bindPageNumber();
        }

        protected void grdVendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lblName.InnerText = "Edit Vendor Details";
                if (e.CommandName.Equals("EDIT_Vendor"))
                {
                    long vendorID = Convert.ToInt64(e.CommandArgument);

                    ViewState["Mode"] = 1;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenVendorDetailPopup('" + vendorID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_Vendor"))
                {
                    long vendorID = Convert.ToInt64(e.CommandArgument);
                    if (VendorDetails.CheckVendorMapping(vendorID))
                    {
                        bool deleteSuccess = VendorDetails.DeleteVendor(vendorID, Convert.ToInt32(CustomerID));

                        if (deleteSuccess)
                        {
                            BindData();
                            bindPageNumber();

                            cvVendorPage.IsValid = false;
                            cvVendorPage.ErrorMessage = "Vendor Deleted Successfully";
                        }
                    }
                    else
                    {
                        cvVendorPage.IsValid = false;
                        cvVendorPage.ErrorMessage = "Vendor is already mapped with some Contract, please remove contract mapping.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdVendor.PageIndex = chkSelectedPage - 1;
            grdVendor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData(); SetShowingRecords();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                DropDownListPageNo.DataBind();
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                if (!string.IsNullOrEmpty(Session["TotalRows"].ToString())) /*&& Session["TotalRows"].ToString() != "0"*/
                {
                    int PageSize = 0;
                    int PageNumber = 0;

                    if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                        PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                    if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                    var EndRecord = 0;
                    var TotalRecord = 0;
                    var TotalValue = PageSize * PageNumber;

                    TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                    if (TotalRecord < TotalValue)
                    {
                        EndRecord = TotalRecord;
                    }
                    else
                    {
                        EndRecord = TotalValue;
                    }

                    if (TotalRecord != 0)
                        lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                    else
                        lblStartRecord.Text = "0";

                    lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                    lblTotalRecord.Text = TotalRecord.ToString();
                }
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }

        protected void upLCPanel_Load(object sender, EventArgs e)
        {

        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContractUser_List.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkNextSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContractType.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton2");
                LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton1");
                LinkButton2.Visible = true;
                LinkButton1.Visible = true;

                if (authpage != null)
                {
                    LinkButton2.Visible = false;
                    LinkButton1.Visible = false;

                    if (authpage.Modify == true)
                    {
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Deleteval == true)
                    {
                        LinkButton1.Visible = true;
                    }
                }
            }
        }
    }
}